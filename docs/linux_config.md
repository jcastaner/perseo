
## Instalación del servidor sobre Red Hat Linux 7

Instalamos las últimas versiones de Apache, MariaDB, Php, .Net Core

### Apache

Instalamos Apache
```shell
sudo yum install httpd
```

### MariaDB 10

Añadimos el repositorio de MariaDB
```shell
sudo nano /etc/yum.repos.d/MariaDB.repo
```
Añadimos el siguiente contenido en el archivo (MariaDB.repo)

```shell
[mariadb]
name = MariaDB
baseurl = http://yum.mariadb.org/10.1/rhel7-amd64
gpgkey=https://yum.mariadb.org/RPM-GPG-KEY-MariaDB
gpgcheck=1
```
Instalamaos MariaDB

```shell
sudo yum install MariaDB-server MariaDB-client
```

Securizamos la instalación

```shell
mysql_secure_installation
```

### Php 7.2.7  (opcional)

Agregamos los repositorios epel y remi

```shell
sudo yum install https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
sudo yum install http://rpms.remirepo.net/enterprise/remi-release-7.rpm
sudo yum install yum-utils
sudo subscription-manager repos --enable=rhel-7-server-optional-rpms
sudo yum-config-manager --enable remi-php72
sudo yum update
```
Instalamos Php 7 con sus módulos típicos

```shell
sudo yum search php72 | more
sudo yum install php72 php72-php php72-php-fpm php72-php-gd php72-php-json php72-php-mbstring php72-php-mysqlnd php72-php-xml php72-php-xmlrpc php72-php-opcache mod_php72w
```

### .NET Core runtime y ASP.NET Core runtime 2.2

Agregamos repositorio de Microsoft

```shell
yum install https://packages.microsoft.com/config/rhel/7/packages-microsoft-prod.rpm
```

Instalamos el runtime

```shell
yum install aspnetcore-runtime-2.2
```

## Configuración del servidor

### Configuración de Apache

Editamos la configuración

```shell
sudo nano /etc/httpd/conf/httpd.conf
```

Lo securizamos (httpd.conf)

```shell
ServerTokens Prod
ServerSignature off
FileETag None
TraceEnable off
Header always append X-Frame-Options SAMEORIGIN
Header edit Set-Cookie ^(.*)$ $1;HttpOnly;Secure
Header set X-XSS-Protection "1; mode=block"
```

Añadimos compresión GZIP (httpd.conf)

```shell
LoadModule deflate_module modules/mod_deflate.so
LoadModule filter_module modules/mod_filter.so
<IfModule mod_deflate.c>
    SetOutputFilter DEFLATE
    AddOutputFilterByType DEFLATE text/plain
    AddOutputFilterByType DEFLATE text/html
    AddOutputFilterByType DEFLATE text/xml
    AddOutputFilterByType DEFLATE text/gml
    AddOutputFilterByType DEFLATE text/css
    AddOutputFilterByType DEFLATE application/json
    AddOutputFilterByType DEFLATE application/xml
    AddOutputFilterByType DEFLATE application/xhtml+xml
    AddOutputFilterByType DEFLATE application/rss+xml
    AddOutputFilterByType DEFLATE application/javascript
    AddOutputFilterByType DEFLATE application/x-javascript
    <files *.html>
        SetOutputFilter DEFLATE
    </files>
    <files *.xml>
        SetOutputFilter DEFLATE
    </files>
    <files *.gml>
        SetOutputFilter DEFLATE
    </files>
    <files *.json>
        SetOutputFilter DEFLATE
    </files>
    <files *.js>
        SetOutputFilter DEFLATE
    </files>
    <files *.css>
        SetOutputFilter DEFLATE
    </files>
</IfModule>
```

### SSL con certificado autofirmado

Genermos los certificados

```shell
openssl genrsa -out ca.key 2048 
openssl req -new -key ca.key -out ca.csr
openssl x509 -req -days 365 -in ca.csr -signkey ca.key -out ca.crt

cp ca.crt /etc/pki/tls/certs
cp ca.key /etc/pki/tls/private/ca.key
cp ca.csr /etc/pki/tls/private/ca.csr
```

Editamos la configuración de Apache

```shell
sudo nano +/SSLCertificateFile /etc/httpd/conf.d/ssl.conf
```

Cambiamos los paths de los certificados (ssl.conf)

```shell
SSLCertificateFile /etc/pki/tls/certs/ca.crt
SSLCertificateKeyFile /etc/pki/tls/private/ca.key
```

### Configurar Apache como proxy de Kestrel


```shell
sudo nano /etc/httpd/conf.d/webapp.conf
```

```shell
<Location />
    Order allow,deny
    Allow from all
    <LimitExcept POST GET>
        Deny from all
    </LimitExcept>
</Location>

<VirtualHost *:*>
    RequestHeader set X-Forwarded-Proto "https" early
</VirtualHost>

<VirtualHost *:80>
    RewriteEngine On
    RewriteCond %{REQUEST_METHOD} !^(GET|POST)
    RewriteRule .* - [R=405,L]
    RewriteCond %{HTTPS} !=on
    RewriteRule ^/?(.*) https://%{SERVER_NAME}/$1 [R,L]
</VirtualHost>

<VirtualHost *:443>
    RewriteEngine On
    RewriteCond %{REQUEST_METHOD} !^(GET|POST)
    RewriteRule .* - [R=405,L]
    ProxyPreserveHost On
    ProxyPass / http://127.0.0.1:5000/
    ProxyPassReverse / http://127.0.0.1:5000/
    ServerName 172.22.243.105
    ServerAlias 172.22.243.105
    ErrorLog /var/log/httpd/webapp.log
    CustomLog /var/log/httpd/webapp-access.log common
    SSLEngine on
    SSLProtocol all -SSLv2 -SSLv3
    SSLCipherSuite ALL:!ADH:!EXPORT:!SSLv2:!RC4+RSA:+HIGH:+MEDIUM:!LOW:!RC4
    SSLCertificateFile /etc/pki/tls/certs/ca.crt
    SSLCertificateKeyFile /etc/pki/tls/private/ca.key
</VirtualHost>
```

Salvar y probar configuración de apache
```shell
sudo service httpd configtest
```
Reiniciar Apache y hacer que arranque al inicio
```shell
sudo systemctl restart httpd
sudo systemctl enable httpd
```


## Despliegue de la aplicación


Copiar los archivos del resultado de "Publish"

```shell
\var\webapp
```

Probar la aplicación (salir una vez probada)

```shell
sudo dotnet Bpo.Web.dll

Ctrl+C (salir)
```

### Configurar dotnet/kestrell como servicio

Creamos el archivo de configuración del servicio

```shell
sudo nano /etc/systemd/system/kestrel-webapp.service
```

```shell
[Unit]
Description=Opsait Webapp

[Service]
WorkingDirectory=/var/webapp
ExecStart=/usr/bin/dotnet /var/webapp/Bpo.Web.dll
Restart=always
# Restart service after 10 seconds if the dotnet service crashes:
RestartSec=10
KillSignal=SIGINT
SyslogIdentifier=kestrel-webapp
User=finapp
Environment=ASPNETCORE_ENVIRONMENT=Production 

[Install]
WantedBy=multi-user.target
```

Guardamos y hacemos que el servicio se ejecute al arrancar

```shell
sudo systemctl enable kestrel-helloapp.service
```

Arrancamos el servicio y verificamos status

```shell
sudo systemctl start kestrel-helloapp.service
sudo systemctl status kestrel-helloapp.service
```

Servidor

MADPPVBPOFINAPP
 
Nombre máquina  MADPPVBPOFINAPP
Dirección IP    172.22.243.105
Usuario  Root
Responsable técnico     Cancio Gomez, Aaron
SO      Red Hat 7.5
Parcheo/Política        SMT
Plataforma      VMware Proyectos
Backup (retención)      Si, 1 mes
Usuario adicional: finapp