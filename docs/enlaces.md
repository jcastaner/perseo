# Enlaces a doumentación de referencia

## Documentación oficial
- [Documentación ASP.Net Core 2.1](https://docs.microsoft.com/es-es/aspnet/core/?view=aspnetcore-2.1)
- [Documentación C#](https://docs.microsoft.com/es-es/dotnet/csharp/)
- [Documentación Entity Framework Core](https://docs.microsoft.com/es-es/ef/#pivot=efcore)
- [Documentación Dapper Micro ORM](https://github.com/StackExchange/Dapper)
- [Documentación MariaDB 10](https://mariadb.com/kb/en/library/training-tutorials/)
- [Documentación jQuery 3.3](https://api.jquery.com/)
- [Documentación Bootstrap 4.1](https://getbootstrap.com/docs/4.1/getting-started/introduction/)

## Tutoriales y documentación no oficial
- [Tutorial Dapper](http://dapper-tutorial.net/dapper)
- [Tutorial Entity Framework Core](https://www.learnentityframeworkcore.com/)


## Otra información
- [Arquitectura MVC en ASP](https://www.c-sharpcorner.com/article/mvc-architecture-its-pipeline4/)
- [Arquitectura 3-Tier en ASP](https://www.c-sharpcorner.com/article/three-tier-architecture-in-asp-net-with-example/)
- [Arquitectura Onion en ASP](https://www.c-sharpcorner.com/article/onion-architecture-in-asp-net-core-mvc/)
- [Buenas prócticas C#](https://docs.microsoft.com/es-es/dotnet/csharp/programming-guide/inside-a-program/coding-conventions)
- [Patrones de diseño de software](https://es.wikipedia.org/wiki/Patr%C3%B3n_de_dise%C3%B1o)
- [Inyección de dependencias I](https://msdn.microsoft.com/es-es/communitydocs/net-dev/csharp/patrones-de-diseno)
- [Inyección de dependencias II](https://itblogsogeti.com/2015/10/29/inyeccion-de-dependencias-vs-inversion-de-control-eduard-moret-sogeti/)
- [Inyección de dependencias III](https://danielggarcia.wordpress.com/2014/01/15/inversion-de-control-e-inyeccion-de-dependencias/)
- [Git](https://git-scm.com/book/es/v1/Empezando)