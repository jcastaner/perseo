# ROADMAP DE DESARROLLO

## ACCESO, CONFIGURACIÓN Y ADMINISTRACIÓN  (v0.1)
- Gestión de usuarios (autentificación y autorización)
- Gestión de roles y permisos
- Panel de administración
- Configuración de servicios (sedes, horario, calendario)
- Configuración de tareas/items de servicio, temo, kpi, sla, precios, facturación

## GESTIÓN DE PERSONAL (v0.2)
- Control de personal, (información personal básica y profesional completa)
- Control jerárquico, funcional y de roles
- Control y trazabilidad de asignaciones, cesiones y dedicación
- Control horario (turnos, asistencia, vacaciones y ausencias)
- Gestión de ETTs (inventario de contratos, control de horas, facturación)
- Cuadro horas imputadas vs control asistencia RRHH
- Gestión de peticiones (vacaciones, permisos…)

## PREVISIONES, DIMENSIONAMIENTO Y AGENDADO (v0.3)
- Carga y/o cálculo de previsiones de llamadas/tareas
- Cálculo de dimensionamiento de recursos.
- Cálculo y asignación de las pausas y descansos

## GESTIÓN DE EQUIPOS E INFRAESTRUCTURAS (v0.4)
- Inventario equipos, pantallas, cascos, licencias
- Inventario de puestos de trabajo
- Inventario de incidencias técnicas

## GESTIÓN DE SERVICIOS (v1.0)
- Repositorio  de volumetrías y niveles de servicio
- Previsiones económicas y proyección de recursos
- Cuadro de mando
- Informes de niveles de servicio 
