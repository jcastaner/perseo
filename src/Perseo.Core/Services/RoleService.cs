using System.Collections.Generic;
 
namespace Perseo.Core
{
    public class RoleService : Service<Role>, IRoleService
    {
        private IRoleRepository RoleRepository;
 
        public RoleService(IRoleRepository roleRepository) : base (roleRepository)
        {
            RoleRepository = roleRepository;
        }

        public Role FindByName(string normalizedRoleName)
        {
            return RoleRepository.FindByName(normalizedRoleName);
        }

        public IEnumerable<Role> All()
        {
            return RoleRepository.GetMany();
        }

    }
}