using System.Collections.Generic;
 
namespace Perseo.Core
{
    public class JobService : Service<Job>, IJobService
    {
        private readonly IJobRepository JobRepository;

        public JobService(IJobRepository jobRepository) : base(jobRepository)
        {
            JobRepository = jobRepository;
        }

        public IEnumerable<Job> GetJobsFromAreas(IEnumerable<Area> areas)
        {
            return JobRepository.GetJobsFromAreas(areas);
        }
    }
}