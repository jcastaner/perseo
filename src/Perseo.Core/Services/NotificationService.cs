using System.Collections.Generic;
 
namespace Perseo.Core
{
    public class NotificationService : Service<Notification>, INotificationService
    {
        private readonly INotificationRepository NotificationRepository;
        private readonly IUserRepository UserRepository;

        public NotificationService(INotificationRepository notificationRepository, IUserRepository userRepository) : base (notificationRepository)
        {
            NotificationRepository = notificationRepository;
            UserRepository = userRepository;
        }

        public IEnumerable<Notification> GetAllFromUser(int id, bool active = true, NotificationType? type = null)
        {
            return NotificationRepository.GetAllFromUser(id, active, type);
        }

        public bool Dismiss(Notification notification)
        {
            notification.Active = false;
            NotificationRepository.Update(notification);
            if (notification.Id > 0)
            {
                User user = UserRepository.Get(notification.UserId);
                switch (notification.Type)
                {
                    case NotificationType.alert: user.Alerts--; break;
                    case NotificationType.task: user.Tasks--; break;
                    case NotificationType.message: user.Messages--; break;
                }
                UserRepository.Update(user);
                return true;
            }
            return false;
        }
    }
}