using System.Collections.Generic;
using System.Linq;

namespace Perseo.Core
{
    public class ScheduleService : Service<ScheduleDate>, IScheduleService
    {
        private readonly IScheduleRepository ScheduleRepository;
        private readonly IJobRepository JobRepository;
        private readonly IUserRepository UserRepository;

        public ScheduleService(
            IScheduleRepository scheduleRepository,
            IJobRepository jobRepository, 
            IUserRepository userRepository) : base (scheduleRepository)
        {
            ScheduleRepository = scheduleRepository;
            JobRepository = jobRepository;
            UserRepository = userRepository;
        }

        public IEnumerable<ScheduleDate> GetUserSchedule(int year, int userId)
        {
            return ScheduleRepository.GetUserSchedule(year, userId);
        }

        public List<UserDedication> GetUserDedications(int userId, int year, int? month = null)
        {
            List<int> firstTerms = new List<int>();
            List<UserDedication> dedications = ScheduleRepository.GetUserDedications(userId, year, month).ToList();
            for (int i = 0; i < dedications.Count; i++)
            {
                double hours = System.Math.Round(dedications[i].TermHours / dedications[i].TermCount, 2);
                if (dedications[i].TermCount > 1)
                {
                    if (!firstTerms.Contains(dedications[i].TermId))
                    {
                        firstTerms.Add(dedications[i].TermId);
                        hours = dedications[i].TermHours - ((dedications[i].TermCount - 1) * (hours));
                    }
                }
                dedications[i].JobHours = hours;
            }
            return dedications;
        }

        public Dictionary<int, Dictionary<int, UserScheduleSummary>> GetUserScheduleSummary(int userId, int year, int? month = null)
        {
            Dictionary<int, Dictionary<int, UserScheduleSummary>> summary = new Dictionary<int, Dictionary<int, UserScheduleSummary>>();
            summary.Add(0, new Dictionary<int, UserScheduleSummary>());
            int start_month = 1;
            int end_month = 12;
            if (month != null) start_month = end_month = month.Value;
            for (int i = start_month; i <= end_month; i++)
            {
                summary.Add(i , new Dictionary<int, UserScheduleSummary>());
            }
            List<int> firstTerms = new List<int>();
            List<UserDedication> dedications = ScheduleRepository.GetUserDedications(userId, year, month).ToList();

            for (int e = start_month; e <= end_month; e++)
            {
                var dedications_month = dedications.Where(x => x.DateMonth == e);
                dedications.Where(x => x.DateMonth == e).ToList().ForEach(dedication_month =>
                {
                    double hours = System.Math.Round(dedication_month.TermHours / dedication_month.TermCount, 2);
                    if (dedication_month.TermCount > 1)
                    {
                        if (!firstTerms.Contains(dedication_month.TermId))
                        {
                            firstTerms.Add(dedication_month.TermId);
                            hours = dedication_month.TermHours - ((dedication_month.TermCount - 1) * (hours));
                        }
                    }
                    dedication_month.JobHours = hours;

                    if (!summary[e].ContainsKey(dedication_month.JobId))
                    {
                        summary[e][dedication_month.JobId] = new UserScheduleSummary()
                        {
                            Days = 1,
                            Hours = System.Math.Round(dedication_month.JobHours, 2),
                            JobName = dedication_month.JobName,
                            JobType = dedication_month.JobType
                        };
                    }
                    else
                    {
                        summary[e][dedication_month.JobId].Days++;
                        summary[e][dedication_month.JobId].Hours = System.Math.Round(summary[e][dedication_month.JobId].Hours + dedication_month.JobHours, 2);
                    }

                    if (!summary[0].ContainsKey(dedication_month.JobId))
                    {
                        summary[0][dedication_month.JobId] = new UserScheduleSummary()
                        {
                            Days = 0,
                            Hours = 0,
                            JobName = dedication_month.JobName,
                            JobType = dedication_month.JobType
                        };
                    }
                    summary[0][dedication_month.JobId].Days++;
                    summary[0][dedication_month.JobId].Hours = System.Math.Round(summary[0][dedication_month.JobId].Hours + dedication_month.JobHours, 2);

                });
                
            }
            return summary;
        }

        public IEnumerable<Job> GetGeneralJobs(int Year){
            return JobRepository.GetGeneralJobs(Year);
        }


        public ScheduleDateTerm GetTerm(params object[] key)
        {
            return ScheduleRepository.GetTerm(key);
        }

        public int AddTerm(ScheduleDateTerm entity)
        {
            return ScheduleRepository.AddTerm(entity);
        }

        public int AddTermRange(IEnumerable<ScheduleDateTerm> entityList)
        {
            return ScheduleRepository.AddTermRange(entityList);
        }

        public int UpdateTerm(ScheduleDateTerm entity)
        {
            return ScheduleRepository.UpdateTerm(entity);
        }

        public int UpdateTermRange(IEnumerable<ScheduleDateTerm> entityList)
        {
            return ScheduleRepository.UpdateTermRange(entityList);
        }

        public int RemoveTerm(ScheduleDateTerm entity)
        {
            return ScheduleRepository.RemoveTerm(entity);
        }

        public int RemoveTermRange(IEnumerable<ScheduleDateTerm> entityList)
        {
            return ScheduleRepository.RemoveTermRange(entityList);
        }



        public ScheduleDateTermJob GetTermJob(params object[] key)
        {
            return ScheduleRepository.GetTermJob(key);
        }

        public int AddTermJob(ScheduleDateTermJob entity)
        {
            return ScheduleRepository.AddTermJob(entity);
        }

        public int AddTermJobRange(IEnumerable<ScheduleDateTermJob> entityList)
        {
            return ScheduleRepository.AddTermJobRange(entityList);
        }

        public int UpdateTermJob(ScheduleDateTermJob entity)
        {
            return ScheduleRepository.UpdateTermJob(entity);
        }

        public int UpdateTermJobRange(IEnumerable<ScheduleDateTermJob> entityList)
        {
            return ScheduleRepository.UpdateTermJobRange(entityList);
        }

        public int RemoveTermJob(ScheduleDateTermJob entity)
        {
            return ScheduleRepository.RemoveTermJob(entity);
        }

        public int RemoveTermJobRange(IEnumerable<ScheduleDateTermJob> entityList)
        {
            return ScheduleRepository.RemoveTermJobRange(entityList);
        }

    }
}