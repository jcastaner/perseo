using System;
using System.Collections.Generic;
 
namespace Perseo.Core
{
    public class AreaService : Service<Area>, IAreaService
    {
        private readonly IAreaRepository AreaRepository;

        public new int Insert(Area entity)
        {
            return Add(entity);
        }

        public new int Add(Area entity)
        {
            AreaRepository.Add(entity);
            AreaRepository.Attach(entity);
            AreaRepository.LoadProperty(entity, x => x.Parent);
            Area parentArea = entity.Parent;
            entity.Lineage = parentArea != null ? parentArea.Lineage + entity.Id + "-" : "-" + entity.Id + "-";
            entity.Depth = parentArea != null ? parentArea.Depth + 1 : 0;
            return AreaRepository.Update(entity);
        }

        public new int Update(Area entity)
        {
            entity.Lineage = entity.ParentId != null ? entity.Parent.Lineage + entity.Id + "-" : "-" + entity.Id + "-";
            entity.Depth = entity.ParentId != null ? entity.Parent.Depth + 1 : 0;
            return AreaRepository.Update(entity);
        }

        public AreaService(IAreaRepository areaRepository) : base( areaRepository)
        {
            AreaRepository = areaRepository;
        }

        public IEnumerable<Area> GetAncestors(int id)
        {
            return AreaRepository.GetAncestors(id);
        }

        public IEnumerable<Area> GetChildren(int id)
        {
            return AreaRepository.GetChildren(id);
        }

        public IEnumerable<AreaListNode> GetTree(int id)
        {
            return AreaRepository.GetTree(id);
        }

        public IEnumerable<AreaListNode> GetOwnedAreaList(User user, bool? Active = null, List<AreaType> Types = null)
        {
            return AreaRepository.GetOwnedAreaList(user, Active, Types);
        }

        public IEnumerable<Area> GetAreasToSchedule(List<int> ids)
        {
            return AreaRepository.GetAreasToSchedule(ids);
        }

        public PagedResult<AreaListNode> GetOwnedAreaListPaged(User user, bool? Active = null, List<AreaType> Types = null, int? page = null, int? pageSize = null, string Search = null, string OrderField = null, string OrderDirection = "asc")
        {
            return AreaRepository.GetOwnedAreaListPaged(user, Active, Types, page, pageSize, Search, OrderField, OrderDirection);
        }

        public PagedResult<AreaListNode> ListPaged(List<int> areaFilterIds, ActiveType? active, List<AreaType> types = null, int? page = null, int? pageSize = null, string search = null, string order = null)
        {
            bool? Active = active.ToBool();
            order = order ?? "Lineage ASC, Depth ASC";
            return AreaRepository.QueryPaged(AreaListNode.Projection,
               x => areaFilterIds.Contains(x.Id) && types.Contains(x.Type) && (Active != null ? x.Active == Active : true), null,
               order, page, pageSize);
        }
    }
}