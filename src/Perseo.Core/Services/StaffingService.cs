using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
 
namespace Perseo.Core
{
    public class StaffingService : IStaffingService
    {
        private readonly double multi_skill_efficiency = 0.05;
        private readonly double period_in_seconds = 1800;
        private readonly double front_unproductivity = 0.13;
        private readonly double back_unproductivity = 0.10;
        private readonly double availability_adjustment = 0.50;

        private readonly IAreaRepository areaRepository;
        private IScheduleRepository scheduleRepository;
        private IJobRepository jobRepository;
        private IJobForecastRepository jobForecastRepository;
        private readonly IExcellService excellService;

        public StaffingService(
            IAreaRepository AreaRepository,
            IScheduleRepository ScheduleRepository,
            IJobRepository JobRepository,
            IJobForecastRepository JobForecastRepository,
            IExcellService ExcellService)
        {
            areaRepository = AreaRepository;
            scheduleRepository = ScheduleRepository;
            jobRepository = JobRepository;
            jobForecastRepository = JobForecastRepository;
            excellService = ExcellService;
        }

        public double CalculateRequiredAgents(
            double wait_time,
            double service_level,
            double inbound_calls,
            double average_talk_time,
            double period_in_seconds = 1800,
            double max_queue_length = 999)
        {
            return ErlangC.CalculateRequiredAgents(wait_time, service_level, inbound_calls,
                average_talk_time, period_in_seconds, max_queue_length);
        }

        public double CalculateAverageWaitTime(
            double number_of_agents,
            double inbound_calls,
            double average_talk_time,
            double period_in_seconds = 1800,
            double max_queue_length = 999)
        {
            return ErlangC.CalculateAverageWaitTime(number_of_agents, inbound_calls,
                average_talk_time, period_in_seconds, max_queue_length);
        }

        public double CalculateTimeOverSla(
            double number_of_agents,
            double wait_time,
            double inbound_calls,
            double average_talk_time,
            double period_in_seconds = 1800,
            double max_queue_length = 999)
        {
            return ErlangC.CalculateTimeOverSla(number_of_agents, wait_time, inbound_calls,
                average_talk_time, period_in_seconds, max_queue_length);
        }

        public double CalculateServiceLevel(
            double number_of_agents,
            double wait_time,
            double inbound_calls,
            double average_talk_time,
            double period_in_seconds = 1800,
            double max_queue_length = 999)
        {
            return ErlangC.CalculateServiceLevel(number_of_agents, wait_time, inbound_calls,
                average_talk_time, period_in_seconds, max_queue_length);
        }

        public double CalculateOccupancy(
           double number_of_agents,
           double inbound_calls,
           double average_talk_time,
           double period_in_seconds = 1800)
        {
            return ErlangC.CalculateOccupancy(number_of_agents, inbound_calls,
                average_talk_time, period_in_seconds);
        }

        public double CalculateAvailability(
            double number_of_agents,
            double inbound_calls,
            double average_talk_time,
            double period_in_seconds = 1800)
        {
            return ErlangC.CalculateAvailability(number_of_agents, inbound_calls,
               average_talk_time, period_in_seconds);
        }

        public IList<string> GetTimeSlots()
        {
            return new List<string> {
                "00:00-00:30", "00:30-01:00", "01:00-01:30", "01:30-02:00", "02:00-02:30", "02:30-03:00",
                "03:00-03:30", "03:30-04:00", "04:00-04:30", "04:30-05:00", "05:00-05:30", "05:30-06:00",
                "06:00-06:30", "06:30-07:00", "07:00-07:30", "07:30-08:00", "08:00-08:30", "08:30-09:00",
                "09:00-09:30", "09:30-10:00", "10:00-10:30", "10:30-11:00", "11:00-11:30", "11:30-12:00",
                "12:00-12:30", "12:30-13:00", "13:00-13:30", "13:30-14:00", "14:00-14:30", "14:30-15:00",
                "15:00-15:30", "15:30-16:00", "16:00-16:30", "16:30-17:00", "17:00-17:30", "17:30-18:00",
                "18:00-18:30", "18:30-19:00", "19:00-19:30", "19:30-20:00", "20:00-20:30", "20:30-21:00",
                "21:00-21:30", "21:30-22:00", "22:00-22:30", "22:30-23:00", "23:00-23:30", "23:30-00:00" };
        }

        public IEnumerable<dynamic> GetJobForecastList(IList<int> jobsIds)
        {
            return jobForecastRepository.GetJobForecastList(jobsIds);
        }

        public IList<JobConfig> GetJobsConfiguration(IList<int> jobsIds, int year, int month)
        {
            return jobRepository.GetJobConfiguration(jobsIds, year, month).ToList();
        }

        public IList<JobSla> GetJobsSla(IList<int> jobsIds, int year, int month)
        {
            return jobRepository.GetJobSla(jobsIds, year, month).ToList();
        }

        public IDictionary<int, List<double>> GetActualAgents(IList<int> jobsIds, int year, int month)
        {
            IDictionary<int, List<double>> actualAgents = new Dictionary<int, List<double>>();
            var groupedDayPatterns = scheduleRepository.GetWorkPatterns(jobsIds, year, month).ToList().GroupBy(x => x.Day);
            foreach (IGrouping<int, StaffingDayPattern> dayPatterns in groupedDayPatterns)
            {
                int gDay = dayPatterns.Key;
                List<double> dayScheduledJobSum = Enumerable.Repeat(0.0, 48).ToList();
                dayPatterns.ToList().ForEach(g =>
                {
                    double dedication = g.Dedication;
                    List<double> b = g.Pattern.Select(c => double.Parse(c.ToString()) * dedication).ToList();
                    dayScheduledJobSum = dayScheduledJobSum.Zip(b, (x, y) => x + y).ToList();
                });
                actualAgents[gDay] = dayScheduledJobSum;
            }
            return actualAgents;
        }

        public IDictionary<int, List<double>> GetAvailableAgents(IList<int> jobsIds, int year, int month)
        {
            IDictionary<int, List<double>> availableAgents = new Dictionary<int, List<double>>();

            var groupedDayPatterns = scheduleRepository.GetWorkPatterns(jobsIds, year, month).ToList().GroupBy(x => x.Day);
            List<JobConfig> jobConfigs = GetJobsConfiguration(jobsIds, year, month).ToList();

            foreach (IGrouping<int, StaffingDayPattern> dayPatterns in groupedDayPatterns)
            {
                int gDay = dayPatterns.Key;
                List<double> dayScheduledJobSum = Enumerable.Repeat(0.0, 48).ToList();
                dayPatterns.ToList().ForEach(g =>
                {
                    double dedication = g.Dedication;
                    double productivity = 0;
                    if (jobConfigs.SingleOrDefault(c => c.JobId == g.JobId) != null)
                    {
                        TaskType job_type = jobConfigs.SingleOrDefault(c => c.JobId == g.JobId).TaskType;
                        if (job_type == TaskType.FrontOffice) { productivity = 1 - front_unproductivity; }
                        else if (job_type == TaskType.BackOffice) { productivity = 1 - back_unproductivity; }
                    }
                    List<double> b = g.Pattern.Select(c => double.Parse(c.ToString()) * dedication * productivity).ToList();
                    dayScheduledJobSum = dayScheduledJobSum.Zip(b, (x, y) => x + y).ToList();
                });
                availableAgents[gDay] = dayScheduledJobSum;
            }

            return availableAgents;
        }

        public (int start, int end) GetRealTimeSlots(IDictionary<int, List<double>> data)
        {
            List<double> slots_sum = Enumerable.Repeat(0.0, 48).ToList();
            for (int slot = 0; slot < 48; slot++)
            {
                foreach (var d in data)
                {
                    slots_sum[slot] += Math.Abs(d.Value[slot]);
                }
            }
            int start_slot = slots_sum.FindIndex(item => item > 0);
            int end_slot = slots_sum.FindLastIndex(item => item > 0);
            start_slot = start_slot >= 0 && start_slot < 47 ? start_slot : 0;
            end_slot = end_slot >= start_slot && end_slot < 48 ? end_slot : 48;
            return (start_slot, end_slot);
        }

        public IDictionary<int, List<double>> GetRequiredEvents(IList<int> jobsIds, int year, int month)
        {
            IDictionary<int, List<double>> requiredEvents = new Dictionary<int, List<double>>();
            var groupedDayForecast = jobForecastRepository.GetJobForecast(jobsIds, year, month).ToList().GroupBy(x => x.Day);
            foreach (IGrouping<int, JobForecast> dayForecast in groupedDayForecast)
            {
                int fDay = dayForecast.Key;
                List<double> dayForecastJobSum = Enumerable.Repeat(0.0, 48).ToList();
                dayForecast.ToList().ForEach(f =>
                {
                    dayForecastJobSum = dayForecastJobSum.Zip(f.Events, (x, y) => x + y).ToList();
                });
                requiredEvents[fDay] = dayForecastJobSum;
            }

            for (int i = 0; i<48; i++)
            {
                double slot = 0;
                foreach(var d in requiredEvents)
                {
                    slot += d.Value[i];
                }
            }
            return requiredEvents;
        }

        public IDictionary<int, List<double>> GetRequiredAgents(IList<int> jobsIds, int year, int month)
        {
            List<JobConfig> jobConfigs = GetJobsConfiguration(jobsIds, year, month).ToList();
            List<JobSla> jobSlas = GetJobsSla(jobsIds, year, month).ToList();

            double totalEvents = 0;
            
            IDictionary<int, List<double>> requiredAgents = new Dictionary<int, List<double>>();

            double efficiency = jobsIds.Count() > 1 ? 1 - (jobsIds.Count() * multi_skill_efficiency) : 1;

            foreach (int jobId in jobsIds)
            {
                double wait_time = 30;
                double service_level = 0.8;
                double average_talk_time = 180;
                double max_queue_length = 999;
                TaskType job_type = TaskType.General;

                IDictionary<int, List<double>> requiredEvents = GetRequiredEvents(new List<int>(){ jobId }, year, month);
                JobConfig jobConfig = jobConfigs.Where(x => x.JobId == jobId && x.Active).FirstOrDefault();
                JobSla jobSla = jobSlas.Where(x => x.JobId == jobId && x.Active).FirstOrDefault();
                double events = requiredEvents.Select(x => x.Value.Sum()).ToList().Sum();
                totalEvents += events;
                if (jobConfig != null) {
                    job_type = jobConfig.TaskType;
                }
                if (jobSla != null) {
                    wait_time = jobSla.Wait ?? 30;
                    service_level = jobSla.Sla ?? 0.8;
                    average_talk_time = jobSla.Tmo ?? 180;
                    max_queue_length = jobSla.Queue ?? 999;
                }

                foreach (var item in requiredEvents)
                {
                    if (!requiredAgents.ContainsKey(item.Key))
                    {
                        requiredAgents[item.Key] = Enumerable.Repeat(0.0, 48).ToList();
                    }
                    for (int i = 0; i < 48; i++) 
                    {
                        if (item.Value[i] >= 1)
                        {
                            double r = 0;
                            if (job_type == TaskType.FrontOffice)
                            {
                                r = CalculateRequiredAgents(wait_time, service_level, item.Value[i], average_talk_time, period_in_seconds, max_queue_length);
                            }
                            else if (job_type == TaskType.BackOffice)
                            {
                                r = (item.Value[i] * average_talk_time * (1 + back_unproductivity)) / period_in_seconds;
                            }
                            requiredAgents[item.Key][i] += (efficiency < 1 ) ? r * efficiency : r;
                        }
                    }
                }
            }

            return requiredAgents;
        }

        public IDictionary<int, List<double>> GetCapacity(IList<int> jobsIds, int year, int month, bool? full = true)
        {
            IDictionary<int, List<double>> requiredAgents = GetRequiredAgents(jobsIds, year, month);
            IDictionary<int, List<double>> availableAgents = GetAvailableAgents(jobsIds, year, month);
            IDictionary<int, List<double>> capacity = new Dictionary<int, List<double>>();

            for (int j = 1; j <= DateTime.DaysInMonth(year, month); j++)
            {
                if (requiredAgents.ContainsKey(j) || availableAgents.ContainsKey(j))
                {
                    capacity[j] = Enumerable.Repeat(0.0, 48).ToList();
                    for (int i = 0; i < 48; i++)
                    {
                        if (requiredAgents.ContainsKey(j))
                        {
                            if (availableAgents.ContainsKey(j))
                            {
                                capacity[j][i] = availableAgents[j][i] + requiredAgents[j][i];
                            }
                            else
                            {
                                capacity[j][i] = 0 + requiredAgents[j][i];
                            }
                        }
                        else
                        {
                            if (availableAgents.ContainsKey(j))
                            {
                                capacity[j][i] = availableAgents[j][i];
                            }
                        }
                        if ((full == false) && (capacity[j][i] > 0))
                        {
                            capacity[j][i] = capacity[j][i] * availability_adjustment;
                        }
                    }
                }
                
            }
            return capacity;
        }

        public IDictionary<int, Dictionary<int, List<double>>> GetCapacityByJob(IList<int> jobsIds, int year, int month, bool? full = true)
        {
            IDictionary<int, Dictionary<int, List<double>>> result = new Dictionary<int, Dictionary<int, List<double>>>();

            foreach (int jobid in jobsIds)
            {
                IDictionary<int, List<double>> requiredAgents = GetRequiredAgents(new List<int>() { jobid }, year, month);
                IDictionary<int, List<double>> availableAgents = GetAvailableAgents(new List<int>() { jobid }, year, month);
                Dictionary<int, List<double>> capacity = new Dictionary<int, List<double>>();

                for (int j = 1; j <= DateTime.DaysInMonth(year, month); j++)
                {
                    if (requiredAgents.ContainsKey(j) || availableAgents.ContainsKey(j))
                    {
                        capacity[j] = Enumerable.Repeat(0.0, 48).ToList();
                        for (int i = 0; i < 48; i++)
                        {
                            if (requiredAgents.ContainsKey(j))
                            {
                                if (availableAgents.ContainsKey(j))
                                {
                                    capacity[j][i] = availableAgents[j][i] + requiredAgents[j][i];
                                }
                                else
                                {
                                    capacity[j][i] = 0 + requiredAgents[j][i];
                                }
                            }
                            else
                            {
                                if (availableAgents.ContainsKey(j))
                                {
                                    capacity[j][i] = availableAgents[j][i];
                                }
                            }
                            if ((full == false) && (capacity[j][i] > 0))
                            {
                                capacity[j][i] = capacity[j][i] * availability_adjustment;
                            }
                        }
                    }
                }
                result[jobid] = capacity;
            }
            return result;
        }

        public void LoadForeacast (FileStream file, int sheetNumber, int jobId, int year, int month)
        {
            DataTable dt = TransposeDataTable(excellService.ReadSheet(file, sheetNumber));
            List<JobForecast> jobForecastRange= new List<JobForecast>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow row = dt.Rows[i];
                var detail = row.ItemArray;
                List<double> events = detail.OfType<object>().Select(a => Convert.ToDouble(a)).ToList();
                JobForecast jobForecast = new JobForecast()
                {
                    Date = new DateTime(year, month, i + 1),
                    Year = year,
                    Month = month,
                    Day = i + 1,
                    JobId = jobId,
                    Events = events
                };
                jobForecastRange.Add(jobForecast);
            }
            jobForecastRepository.AddRange(jobForecastRange);
        }
        
        public void SaveForeacast(IDictionary<int, List<double>> forecast, int jobId, int year, int month)
        {
            List <JobForecast> jobForecastRange = new List<JobForecast>();
            foreach (var item in forecast)
            {
                List<double> events = item.Value;
                JobForecast jobForecast = new JobForecast()
                {
                    Date = new DateTime(year, month, item.Key),
                    Year = year,
                    Month = month,
                    Day = item.Key,
                    JobId = jobId,
                    Events = events
                };

                jobForecastRange.Add(jobForecast);
            }
            jobForecastRepository.AddRange(jobForecastRange);
        }
        
        public IDictionary<int, List<double>> GetRequiredEventsFromFile(FileStream file, int sheetNumber, int jobId, int year, int month)
        {
            IDictionary<int, List<double>> requiredEvents = new Dictionary<int, List<double>>();

            DataTable dt = TransposeDataTable(excellService.ReadSheet(file, sheetNumber));
            List<JobForecast> jobForecastRange = new List<JobForecast>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow row = dt.Rows[i];
                var detail = row.ItemArray;
                List<double> events = detail.OfType<object>().Select(a => Convert.ToDouble(a)).Skip(1).ToList();
                requiredEvents[i + 1] = events;
            }
            return requiredEvents;
        }

        public IDictionary<int, List<double>> GetRequiredEventsMonthPattern(
            int Year,
            int Month,
            List<double> WeekDistribution = null,
            List<double> DayDistribution = null)
        {
            WeekDistribution = WeekDistribution ?? GetRequiredEventsWeekPattern();
            DayDistribution = DayDistribution ?? GetRequiredEventsDayPattern();

            DateTime dt = new DateTime(Year, Month, 1);
            int DaysOfMonth = DateTime.DaysInMonth(Year, Month);
            int FirstWeekDayOfMonth = (int)dt.AddDays(-1).DayOfWeek;

            double MonthSum = 0;
            for (var i = FirstWeekDayOfMonth; i < DaysOfMonth + FirstWeekDayOfMonth; i++)
            {
                MonthSum += WeekDistribution[i % 7];
            }

            IDictionary<int, List<double>> Events = new Dictionary<int, List<double>>();
            int e = 1;
            for (var i = FirstWeekDayOfMonth; i < DaysOfMonth + FirstWeekDayOfMonth; i++)
            {
                var DayEvents = WeekDistribution[i % 7];
                Events[e] = DayDistribution.Select(d => Math.Round((d * DayEvents) / MonthSum , 4)).ToList();
                e++;
            }
            return Events;
        }

        public List<double> GetRequiredEventsWeekPattern(
            int StartDay = 0,
            int EndDay = 6,
            List<double> WeekDistribution = null)
        {

            List<double> WeekDistributionBase = new List<double>() { 0.18, 0.172, 0.168, 0.162, 0.158, 0.09, 0.06 };

            int DaysNumber = EndDay - StartDay + 1;
            int DaysTrail = 6 - EndDay;

            WeekDistribution = WeekDistribution ?? WeekDistributionBase.GetRange(StartDay, DaysNumber);

            double WeekDistributionTotal = WeekDistribution.Sum(a => a);

            List<double> WeekDistributionFinal = WeekDistribution.Select(d => d / WeekDistributionTotal).ToList();

            for (int i = 0; i < StartDay; i++) { WeekDistributionFinal.Insert(0, 0.0); }
            for (int i = 0; i < DaysTrail; i++) { WeekDistributionFinal.Add(0.0); }

            return WeekDistributionFinal;
        }

        public List<double> GetRequiredEventsDayPattern(
            int StartTimeRange = 0,
            int EndTimeRange = 47,
            List<double> DayDistribution = null)
        {
            List<double> DayDistributionBase = new List<double>() { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
              0.003619, 0.004222, 0.004825, 0.005428, 0.010856, 0.023522, 0.043426, 0.052473, 0.057901,
              0.055489, 0.054282, 0.050663, 0.052473, 0.052473, 0.043426, 0.033172, 0.033172, 0.037998,
              0.041013, 0.043426, 0.041013, 0.038601, 0.041013, 0.041013, 0.033776, 0.027141, 0.018094,
              0.013269, 0.010856, 0.009047, 0.007238, 0.004222, 0.003619, 0.003016, 0.002413, 0.001809 };

            int TimeRangeNumber = EndTimeRange - StartTimeRange + 1;
            int TimeRangeTrail = 47 - EndTimeRange;

            DayDistribution = DayDistribution ?? DayDistributionBase.GetRange(StartTimeRange, TimeRangeNumber);

            double DayDistributionTotal = DayDistribution.Sum(a => a);

            List<double> DayDistributionFinal = DayDistribution.Select(d => d / DayDistributionTotal).ToList();

            for (int i = 0; i < StartTimeRange; i++)
            {
                DayDistributionFinal.Insert(0, 0.0);
            }
            for (int i = 0; i < TimeRangeTrail; i++)
            {
                DayDistributionFinal.Add(0.0);
            }
            return DayDistributionFinal;
        }

        private DataTable TransposeDataTable(DataTable dt)
        {
            DataTable dtNew = new DataTable();
            for (int i = 0; i <= dt.Rows.Count; i++)
            {
                dtNew.Columns.Add(i.ToString());
            }
            dtNew.Columns[0].ColumnName = " ";
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dtNew.Columns[i + 1].ColumnName = dt.Rows[i].ItemArray[0].ToString();
            }
            for (int k = 1; k < dt.Columns.Count; k++)
            {
                DataRow r = dtNew.NewRow();
                r[0] = dt.Columns[k].ToString();
                for (int j = 1; j <= dt.Rows.Count; j++)
                    r[j] = dt.Rows[j - 1][k];
                dtNew.Rows.Add(r);
            }
            return dtNew;
        }

    }
}