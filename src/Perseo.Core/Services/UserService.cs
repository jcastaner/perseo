using System;
using System.Collections.Generic;
using System.Linq;

namespace Perseo.Core
{
    public class UserService : Service<User>, IUserService
    {
        private readonly IUserRepository UserRepository;
        private readonly IAreaRepository AreaRepository;
        private readonly IJobRepository JobRepository;

        public UserService(IUserRepository userRepository, 
            IAreaRepository areaRepository,
            IJobRepository jobRepository) : base (userRepository)
        {
            UserRepository = userRepository;
            AreaRepository = areaRepository;
            JobRepository = jobRepository;
        }

        public User GetFull(int id) {
            return UserRepository.GetFull(id);
        }

        public IEnumerable<User> GetAllUserSchedule(int year, int month, List<int> areasIds, bool? Active = null, List<int> officeFilterIds = null)
        {
            return UserRepository.GetAllUserSchedule(year, month, areasIds, Active, officeFilterIds);
        }

        public User GetUserSchedule(int year, int userId)
        {
            return UserRepository.GetUserSchedule(year, userId);
        }

        public IEnumerable<Area> GetOwnedAreas(User user, DateTime? StartDate = null, DateTime? EndDate = null)
        {
            return AreaRepository.GetOwnedAreas(user);
            /*
            List<int> userAreasIds = user.UserArea.Where(x => x.Owner.GetValueOrDefault() == true
                && ( StartDate.HasValue ? x.StartDate <= StartDate.Value : true)
                && ( EndDate.HasValue ? x.EndDate >= EndDate : !x.EndDate.HasValue))
                .Select(b=>b.AreaId).ToList();
            return areaRepository.GetAllChildren(userAreasIds);
            */
        }

        public IEnumerable<Job> GetOwnedJobs(User user, DateTime? StartDate = null, DateTime? EndDate = null)
        {
            IEnumerable<Area> areas = GetOwnedAreas(user, StartDate, EndDate);
            return JobRepository.GetJobsFromAreas(areas).Distinct();
        }
        
        public IEnumerable<User> GetUsersFromAreas(IEnumerable<int> AreaIds)
        {
            return UserRepository.GetUsersFromAreas(AreaIds);
        }

        public IEnumerable<UserListNode> GetUserListFromAreas(IEnumerable<int> AreaIds, bool? Active = null)
        {
            return UserRepository.GetUserListFromAreas(AreaIds, Active);
        }

        //public PagedResult<UserListNode> GetUserListFromAreasPaged(IEnumerable<int> AreaIds, bool? Active = null, int? page = null, int? pageSize = null,
        //    string Search = null, string OrderField = null, string OrderDirection = "asc")
        //{
        //    return UserRepository.GetUserListFromAreasPaged(AreaIds, Active, page, pageSize, Search, OrderField, OrderDirection);
        //}

        #region Crud

        public User GetAuthUser(int id)
        {
            User user = UserRepository.GetAuthUser(id);
            user.OwnedAreas = AreaRepository.GetOwnedAreas(user).ToList();
            user.OwnedJobs = JobRepository.GetJobsFromAreas(user.OwnedAreas).ToList();
            return user;
        }

        public User GetAuthUser(string userName)
        {
            User user = UserRepository.GetAuthUser(userName);
            user.OwnedAreas = AreaRepository.GetOwnedAreas(user).ToList();
            user.OwnedJobs = JobRepository.GetJobsFromAreas(user.OwnedAreas).ToList();
            return user;
        }

        public IEnumerable<User> GetAllReadOnly()
        {
            return UserRepository.GetAllReadOnly();
        }

        #endregion

        #region Identity

        public User FindByNormalizedUserName(string normalizedUserName)
        {
            return UserRepository.FindByNormalizedUserName(normalizedUserName);
        }

        public User FindByNormalizedEmail(string normalizedEmail)
        {
            return UserRepository.FindByNormalizedEmail(normalizedEmail);
        }

        public IEnumerable<User> All()
        {
            return UserRepository.GetMany();
        }

        public void AddRole(int userId, string roleName)
        {
            UserRepository.AddRole(userId, roleName);
        }

        public void RemoveRole(int userId, string roleName)
        {
            UserRepository.RemoveRole(userId, roleName);
        }

        public IEnumerable<string> GetRoleNamesByUserId(int userId)
        {
            return UserRepository.GetRoleNamesByUserId(userId);
        }

        public IEnumerable<User> GetUsersByRoleName(string roleName)
        {
            return UserRepository.GetUsersByRoleName(roleName);
        }

        #endregion

        public bool ExistsUserName(string userName)
        {
            return UserRepository.ExistsUserName(userName);
        }

        public bool ExistsEmail(string email)
        {
            return UserRepository.ExistsEmail(email);
        }

        public bool ExistsEmployeeId(int employeeId)
        {
            return UserRepository.ExistsEmployeeId(employeeId);
        }

        public IEnumerable<User> GetSubordinates(int UserId, bool Direct = false, bool Owner = false)
        {
            return UserRepository.GetSubordinates(UserId, Direct, Owner);
        }

        //**************************************

        public PagedResult<UserListNode> ListPaged(List<int> areaFilterIds, ActiveType? active, int? page = null, int? pageSize = null, string search = null, string order = null, List<int> officeFilterIds = null)
        {
            bool? Active = active.ToBool();
            order = order ?? "MainSurname ASC";
            return UserRepository.QueryPaged( UserListNode.Projection, 
                x => x.UserArea.Any(y => y.Active == true 
                    && y.Owner != true && y.Authorized != true 
                    && areaFilterIds.Contains(y.AreaId)) 
                    && (Active != null ? x.Active == Active : true) 
                    && (officeFilterIds != null ? (officeFilterIds.Any(i => i.Equals(0)) ? (x.UserOffice.Any(z => z.Active && officeFilterIds.Contains(z.OfficeId))) || !(x.UserOffice.Any()) : (x.UserOffice.Any(z => z.Active && officeFilterIds.Contains(z.OfficeId)))) : true)
                    , null, order, page, pageSize);
        }

    }
}