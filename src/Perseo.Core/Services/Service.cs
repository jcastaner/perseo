using System;
using System.Collections.Generic;
 
namespace Perseo.Core
{
    public class Service<T> : IService<T> where T : class
    {
        protected IRepository<T> Repository;
        
        public Service(IRepository<T> repository)
        {
            Repository = repository;
        }

        public T Find(params object[] key)
        {
            return Repository.Find(key);
        }

        public T Get(params object[] key)
        {
            return Repository.Find(key);
        }

        public T GetOne(System.Linq.Expressions.Expression<Func<T, bool>> where, string include = null)
        {
            return Repository.GetOne(where, include);
        }

        public IEnumerable<T> GetMany(System.Linq.Expressions.Expression<Func<T, bool>> where = null, string order = null, bool tracking = true)
        {
            return Repository.GetMany(where, order, tracking);
        }

        public PagedResult<T> GetManyPaged(
           System.Linq.Expressions.Expression<Func<T, bool>> where = null,
           string order = null, int? page = null, int? pageSize = null)
        {
            return Repository.GetManyPaged(where, order, page, pageSize);
        }

        public PagedResult<Q> QueryPaged<Q>(
            System.Linq.Expressions.Expression<Func<T, Q>> select,
            System.Linq.Expressions.Expression<Func<T, bool>> where = null,
            System.Linq.Expressions.Expression<Func<Q, bool>> filter = null,
            string order = null, int? page = null, int? pageSize = null)
        {
            return Repository.QueryPaged(select, where, filter, order, page, pageSize);
        }

        public int Insert(T entity)
        {
            return Repository.Insert(entity);
        }

        public int Add(T entity)
        {
            return Repository.Add(entity);
        }

        public int AddRange(IEnumerable<T> entityList)
        {
            return Repository.AddRange(entityList);
        }

        public int Update(T entity)
        {
            return Repository.Update(entity);
        }

        public int UpdateRange(IEnumerable<T> entityList)
        {
            return Repository.UpdateRange(entityList);
        }

        public int Delete(T entity)
        {
            Repository.Remove(entity);
            return Repository.SaveChanges();
        }

        public int Delete(params object[] key)
        {
            return Repository.Delete(key);
        }

        public int DeleteRange(IEnumerable<T> entityList)
        {
            return Repository.DeleteRange(entityList);
        }

        public int DeleteRange(IEnumerable<int> idList)
        {
            return Repository.DeleteRange(idList);
        }

        public int Remove(T entity)
        {
            return Repository.Remove(entity);
        }

        public int RemoveRange(IEnumerable<T> entityList)
        {
            return Repository.DeleteRange(entityList);
        }

    }
}