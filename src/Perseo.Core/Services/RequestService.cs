using System.Collections.Generic;
 
namespace Perseo.Core
{
    public class RequestService : Service<Request>, IRequestService
    {
        private readonly IRequestRepository RequestRepository;

        public PagedResult<RequestListNode> GetRequestsFromAreasPaged(List<int> AreaIds, List<RequestType> Types = null, List<RequestState> States = null, int? year = null, int? month = null,
            int? page = null, int? pageSize = null, string Search = null, string OrderField = null, string OrderDirection = "asc")
        {
            return RequestRepository.GetRequestsFromAreasPaged(AreaIds, Types, States, year, month, page, pageSize, Search, OrderField, OrderDirection);
        }

        public RequestService(IRequestRepository requestRepository) : base (requestRepository)
        {
            RequestRepository = requestRepository;
        }

        public IEnumerable<RequestListNode> GetRequestsFromAreas(List<int> AreaIds, List<RequestType> Types = null, List<RequestState> States = null)
        {
            return RequestRepository.GetRequestsFromAreas(AreaIds, Types, States);
        }

        public IEnumerable<RequestListNode> GetRequestsCreatedByUser(int UserId, List<RequestType> Types = null, List<RequestState> States = null)
        {
            return RequestRepository.GetRequestsCreatedByUser(UserId, Types, States);
        }

        public IEnumerable<RequestListNode> GetRequestsAssignedToUser(int UserId, List<RequestType> Types = null, List<RequestState> States = null)
        {
            return RequestRepository.GetRequestsAssignedToUser(UserId, Types, States);
        }

        public IEnumerable<RequestHistoryListNode> GetRequestHistory(int id)
        {
            return RequestRepository.GetRequestHistory(id);
        }

    }
}