﻿
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;


namespace Perseo.Core
{
    public interface IFileService
    {
        string GetRootPath();

        Stream OpenFile(string FileName);

        MemoryStream LoadFile(string FileName);

        byte[] ReadFile(string FileName);

        void SaveFile(Stream Stream, string FileName);

        void SaveFile(FileStream FileStream, string FileName);

        void SaveFileAsync(Stream Stream, string FileName);

        void SaveFileAsync(FileStream FileStream, string FileName);  

        void DeleteFile(string FileName);

        void SaveBase64Image(string Base64Image, string FileName);

        void SaveBase64ImageAsync(string Base64Image, string FileName);

        bool ExistFile(string FileName);

        Image Base64ToImage(string base64String);

        string ImageToBase64(Image image, ImageFormat format);
        
    }
}
