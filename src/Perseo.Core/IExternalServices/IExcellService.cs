using System.Collections.Generic;
using System.Data;
using System.IO;

namespace Perseo.Core
{

    public interface IExcellService
    {

        DataTable ReadSheet(FileStream file, int sheetNumber);

        Stream Create(DataTable SourceTable);

        Stream Create(IEnumerable<object> SourceData, IDictionary<string, string> Fields = null);

    }
}