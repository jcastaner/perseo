﻿using System.Threading.Tasks;

namespace Perseo.Core
{
    public interface IEmailService
    {
        //Task SendHtmlAsync(string to, string subject, string templateView, object templateModel, int retryCount = 2);
        Task SendAsync(string to, string subject, string bodyText = null, string bodyHtml = null, int retryCount = 2);
        void Send(string to, string subject, string bodyText = null, string bodyHtml = null, int retryCount = 2);
    }
}
