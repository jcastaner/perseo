﻿namespace Perseo.Core
{
	public interface IEntity
	{
        int Id { get; set; }
        bool Active { get; set; }

        bool IsTransient();
        bool Equals(object obj);
        int GetHashCode();
    }
}
