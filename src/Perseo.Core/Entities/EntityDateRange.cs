using System;  
namespace Perseo.Core
{  
    public abstract class EntityDateRange : Entity
    {  
        public DateTime StartDate { get; set; }  

        public DateTime? EndDate {
            get
            {
                return _EndDate;
            }
            set
            {
                _EndDate = value;
            }
        }
        private DateTime? _EndDate { get; set; }

        public new bool Active
        {
            get
            {
                if (EndDate.HasValue) _Active = false;
                return _Active;
            }
            set
            {
                if (!EndDate.HasValue && value == false) _EndDate = DateTime.Now;
                _Active = value;
            }
        }

        public EntityDateRange() : base()
        {
            Active = true;
        }
    }  
}  