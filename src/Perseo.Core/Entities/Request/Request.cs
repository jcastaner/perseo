﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Perseo.Core
{
    public class Request : Entity
    {
        public Request() : base()
        {
            RequestHistory = new HashSet<RequestHistory>();
        }

        public RequestType Type { get; set; }

        public DateTime RequestDate { get; set; }

        public PriorityType Priority { get; set; }
        public string Summary { get; set; }
        public string Description { get; set; }

        public int UserId { get; set; }
        public virtual User User { get; set; }

        public int? AssignedUserId { get; set; }
        public virtual User AssignedUser { get; set; }

        public string AdditionalData { get; set; }

        public virtual ICollection<RequestHistory> RequestHistory { get; set; }

        public virtual RequestState State
        {
            get
            {
                if (RequestHistory.Count > 0)
                {
                    return RequestHistory.OrderByDescending(x => x.EntryDate).FirstOrDefault().State;
                }
                return RequestState.Created;
            }
        }

    }
}
