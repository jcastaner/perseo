﻿using System;

namespace Perseo.Core
{
    public class RequestHistory : Entity
    {

        public RequestHistory() : base()
        {
        }
        public DateTime EntryDate { get; set; }

        public int RequestId { get; set; }
        public virtual Request Request { get; set; }

        public string Comment { get; set; }

        public RequestState State { get; set; }

        public int UserId { get; set; }
        public virtual User User { get; set; }

    }
}
