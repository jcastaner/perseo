using System.Collections.Generic;

namespace Perseo.Core
{  
    public class QualityExamSection : Entity
    {
        public new int? Id { get; set; }
        public int Order { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public double Weighing { get; set; }

        public int ExamId { get; set; }
        public virtual QualityExam Exam { get; set; }

        public virtual ICollection<QualityExamSectionField> Fields { get; set; }

        public QualityExamSection() : base()
        {
            Fields = new HashSet<QualityExamSectionField>();
            Weighing = 1;
        }

    }  
}  