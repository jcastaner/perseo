using System.Collections.Generic;

namespace Perseo.Core
{  
    public class QualityAuditSection : Entity
    {
        public new int? Id { get; set; }
        public int Order { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public double Weighing { get; set; }

        public int AuditId { get; set; }
        public virtual QualityAudit Audit { get; set; }

        public virtual ICollection<QualityAuditSectionField> Fields { get; set; }

        public QualityAuditSection() : base()
        {
            Fields = new HashSet<QualityAuditSectionField>();
            Weighing = 1;
        }

    }  
}  