using System.Collections.Generic;

namespace Perseo.Core
{  
    public class QualityExamOwner : Entity
    {
        public int ExamId { get; set; }
        public virtual QualityExam Exam { get; set; }

        public int UserId { get; set; }
        public virtual User User { get; set; }

        public virtual ICollection<QualityExamUser> Users { get; set; }

        public QualityExamOwner() : base()
        {
            Users = new HashSet<QualityExamUser>();
        }
    }
}  