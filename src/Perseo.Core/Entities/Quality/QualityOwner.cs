using System.Collections.Generic;

namespace Perseo.Core
{  
    public class QualityOwner : Entity
    {
        public int QualityId { get; set; }
        public virtual Quality Quality { get; set; }

        public int OwnerId { get; set; }
        public virtual User Owner { get; set; }

        public QualityOwner() : base()
        {

        }
    }
}  