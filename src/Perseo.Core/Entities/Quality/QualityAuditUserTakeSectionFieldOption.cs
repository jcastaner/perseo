namespace Perseo.Core
{  
    public class QualityAuditUserTakeSectionFieldOption : Entity
    {
        new int? Id { get; set; }
        public bool? Mark { get; set; }
        
        public int AuditOptionId { get; set; }
        public virtual QualityAuditSectionFieldOption AuditOption{ get; set; }

        public int FieldId { get; set; }
        public virtual QualityAuditUserTakeSectionField Field { get; set; }

        public QualityAuditUserTakeSectionFieldOption() : base()
        {

        }
    }  
}  