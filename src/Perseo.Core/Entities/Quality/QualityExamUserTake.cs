using System.Collections.Generic;

namespace Perseo.Core
{  
    public class QualityExamUserTake : EntityDateRange
    {
        public int ExamUserId { get; set; }
        public virtual QualityExamUser ExamUser { get; set; }

        public int Attempt { get; set; }
        public double Mark { get; set; }
        public bool? Pass { get; set; }

        public string Content { get; set; }

        public virtual ICollection<QualityExamUserTakeSection> Sections { get; set; }

        public QualityExamUserTake() : base()
        {
            Sections = new HashSet<QualityExamUserTakeSection>();
        }

    }
}  