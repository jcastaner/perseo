using System.Collections.Generic;

namespace Perseo.Core
{  
    public class QualityExamUser : Entity
    {
        public int ExamId { get; set; }
        public virtual QualityExam Exam { get; set; }

        public int UserId { get; set; }
        public virtual User User { get; set; }

        public int ExamOwnerId { get; set; }
        public virtual QualityExamOwner ExamOwner { get; set; }

        public virtual ICollection<QualityExamUserTake> Takes { get; set; }

        public QualityExamUser() : base()
        {
            Takes = new HashSet<QualityExamUserTake>();
        }
    }
}  