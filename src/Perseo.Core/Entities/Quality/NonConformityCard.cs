﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Perseo.Core
{
    public partial class NonConformityCard : EntityDateRange
    {
        public string InternalReference { get; set; }
       // public int? State { get; set; }
        public bool External { get; set; }
        public string ExternalStaff { get; set; }

        public int OriginUserId { get; set; }
        public virtual User OriginUser { get; set; }

        public int JobId { get; set; }
        public virtual Job Job { get; set; }

        public int TargetUserId { get; set; }
        public virtual User TargetUser { get; set; }

        public string Description { get; set; }
        public int? Quantityincidences { get; set; }

        public IncidentTypology? IncidentTypology { get; set; }
        public MinuteCost? MinuteCost { get; set; }
        public HumanErrorSubtype? HumanErrorSubtype { get; set; }
        public RequestState? State { get; set; }


        public string Cause { get; set; }
        public string ImmediateAction { get; set; }
        public string DefinitiveAction { get; set; }
        public string MonitoringEfficiency{ get; set; }


        public virtual ICollection<NonConformityCardSign> NonConformityCardSign { get; set; }

        public NonConformityCard() : base()
        {
            NonConformityCardSign = new HashSet<NonConformityCardSign>();
            External = false;
            
        }
    }
}
