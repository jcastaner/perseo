using System.Collections.Generic;

namespace Perseo.Core
{  
    public class QualityUser : Entity
    {
        public int QualityId { get; set; }
        public virtual Quality Quality { get; set; }

        public int UserId { get; set; }
        public virtual User User { get; set; }

        public bool QualityCompleted { get; set; }

        public QualityUser() : base()
        {

        }

    }
}  