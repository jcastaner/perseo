using System.Collections.Generic;

namespace Perseo.Core
{  
    public class QualityAuditUserTakeSectionField : Entity
    {
        new int? Id { get; set; }
        public double Mark { get; set; }
        public string Comment { get; set; }

        public int AuditFieldId { get; set; }
        public virtual QualityAuditSectionField AuditField { get; set; }

        public int SectionId { get; set; }
        public virtual QualityAuditUserTakeSection Section { get; set; }

        public virtual ICollection<QualityAuditUserTakeSectionFieldOption> Options { get; set; }

        public QualityAuditUserTakeSectionField() : base()
        {
            Options = new HashSet<QualityAuditUserTakeSectionFieldOption>();
        }
    }  
}  