using System.Collections.Generic;

namespace Perseo.Core
{  
    public class QualityAuditOwner : Entity
    {
        public int AuditId { get; set; }
        public virtual QualityAudit Audit { get; set; }

        public int UserId { get; set; }
        public virtual User User { get; set; }

        public virtual ICollection<QualityAuditUser> Users { get; set; }

        public QualityAuditOwner() : base()
        {
            Users = new HashSet<QualityAuditUser>();
        }
    }
}  