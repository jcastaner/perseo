using System.Collections.Generic;

namespace Perseo.Core
{  
    public class QualityExamUserTakeSectionFieldOption : Entity
    {
        public bool? Mark { get; set; }
        
        public int ExamOptionId { get; set; }
        public virtual QualityExamSectionFieldOption ExamOption{ get; set; }

        public int FieldId { get; set; }
        public virtual QualityExamUserTakeSectionField Field { get; set; }

        public QualityExamUserTakeSectionFieldOption() : base()
        {

        }

    }  
}  