using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Perseo.Core
{  
    public class QualityExam : EntityDateRange
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public double PassMark { get; set; }
        public int Attempts { get; set; }
        public int? Time { get; set; }

        public int QualityId { get; set; }
        public virtual Quality Quality { get; set; }

        public virtual ICollection<QualityExamOwner> Owners { get; set; }
        public virtual ICollection<QualityExamUser> Users { get; set; }

        public virtual ICollection<QualityExamSection> Sections { get; set; }

        public QualityExam() : base()
        {
            Owners = new HashSet<QualityExamOwner>();
            Users = new HashSet<QualityExamUser>();
            Sections = new HashSet<QualityExamSection>();
            Attempts = 1;
        }
    }  
}  