using System.Collections.Generic;

namespace Perseo.Core
{  
    public class QualityAuditSectionField : Entity
    {
        public new int? Id { get; set; }
        public AuditFieldType Type { get; set; }

        public int Order { get; set; }
        public string Name { get; set; }

        public bool? Reasoned { get; set; }
        public double Weighing { get; set; }

        public int SectionId { get; set; }
        public virtual QualityAuditSection Section { get; set; }

        public virtual ICollection<QualityAuditSectionFieldOption> Options { get; set; }

        public QualityAuditSectionField() : base()
        {
            Options = new HashSet<QualityAuditSectionFieldOption>();
            Order = 0;
            Weighing = 0;
        }

    }  
}  