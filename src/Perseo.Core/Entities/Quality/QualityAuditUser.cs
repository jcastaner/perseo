using System.Collections.Generic;

namespace Perseo.Core
{  
    public class QualityAuditUser : Entity
    {
        public int AuditId { get; set; }
        public virtual QualityAudit Audit { get; set; }

        public int UserId { get; set; }
        public virtual User User { get; set; }

        public int AuditOwnerId { get; set; }
        public virtual QualityAuditOwner AuditOwner { get; set; }

        public virtual ICollection<QualityAuditUserTake> Takes { get; set; }

        public QualityAuditUser() : base()
        {
            Takes = new HashSet<QualityAuditUserTake>();
        }
    }
}  