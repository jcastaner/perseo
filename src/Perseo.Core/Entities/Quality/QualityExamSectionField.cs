using System.Collections.Generic;

namespace Perseo.Core
{  
    public class QualityExamSectionField : Entity
    {
        public new int? Id { get; set; }
        public AuditFieldType Type { get; set; }
        public int Order { get; set; }
        public string Name { get; set; }
        public string Content { get; set; }
        public bool? Reasoned { get; set; }
        public double Weighing { get; set; }

        public int SectionId { get; set; }
        public virtual QualityExamSection Section { get; set; }

        public virtual ICollection<QualityExamSectionFieldOption> Options { get; set; }

        public QualityExamSectionField() : base()
        {
            Options = new HashSet<QualityExamSectionFieldOption>();
            Order = 0;
            Weighing = 0;
        }

    }  
}  