﻿namespace Perseo.Core
{
    public class NonConformityCardSign : EntityDateRange
    {
        public int NonConformityCardId { get; set; }
        public virtual NonConformityCard NonConformityCard { get; set; }
        public string Commentary { get; set; }
        public int UserId { get; set; }
        public virtual User User { get; set; }
    }
}
