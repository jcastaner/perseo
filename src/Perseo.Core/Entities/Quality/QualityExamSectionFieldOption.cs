using System.Collections.Generic;

namespace Perseo.Core
{  
    public class QualityExamSectionFieldOption : Entity
    {
        public new int? Id { get; set; }
        public int Order { get; set; }
        public string Content { get; set; }
        public bool? Mark { get; set; }
        public double Value { get; set; }
        

        public int FieldId { get; set; }
        public virtual QualityExamSectionField Field { get; set; }

        public QualityExamSectionFieldOption() : base()
        {
            Value = 0;
            Order = 0;
        }
    }  
}  