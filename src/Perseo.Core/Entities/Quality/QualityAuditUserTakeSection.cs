using System.Collections.Generic;

namespace Perseo.Core
{  
    public class QualityAuditUserTakeSection : Entity
    {
        new int? Id { get; set; }
        public double Mark { get; set; }

        public int AuditSectionId { get; set; }
        public virtual QualityAuditSection AuditSection { get; set; }

        public int TakeId { get; set; }
        public virtual QualityAuditUserTake Take { get; set; }

        public virtual ICollection<QualityAuditUserTakeSectionField> Fields { get; set; }

        public QualityAuditUserTakeSection() : base()
        {
            Fields = new HashSet<QualityAuditUserTakeSectionField>();
        }
    }  
}  