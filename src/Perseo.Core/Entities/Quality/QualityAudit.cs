using System.Collections.Generic;

namespace Perseo.Core
{  
    public class QualityAudit : EntityDateRange
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public double PassMark { get; set; }

        public int QualityId { get; set; }
        public virtual Quality Quality { get; set; }

        public virtual ICollection<QualityAuditOwner> Owners { get; set; }
        public virtual ICollection<QualityAuditUser> Users { get; set; }

        public virtual ICollection<QualityAuditSection> Sections { get; set; }

        public QualityAudit() : base()
        {
            Owners = new HashSet<QualityAuditOwner>();
            Users = new HashSet<QualityAuditUser>();
            Sections = new HashSet<QualityAuditSection>();
            PassMark = 0;
        }
    }  
}  