using System;
using System.Collections.Generic;

namespace Perseo.Core
{  
    public class Quality : EntityDateRange
    {
        public CampaignType Type { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public int AreaId { get; set; }
        public virtual Area Area { get; set; }

        public int CreatorId { get; set; }
        public virtual User Creator { get; set; }

        public virtual ICollection<QualityOwner> Owners { get; set; }
        public virtual ICollection<QualityUser> Users { get; set; }

        public virtual ICollection<QualityAudit> Audits { get; set; }
        public virtual ICollection<QualityExam> Exams { get; set; }

        public Quality() : base()
        {
            Owners = new HashSet<QualityOwner>();
            Users = new HashSet<QualityUser>();
            Audits = new HashSet<QualityAudit>();
            Exams = new HashSet<QualityExam>();
        }
    }  
}  