using System.Collections.Generic;

namespace Perseo.Core
{  
    public class QualityExamUserTakeSection : Entity
    {
        public double Mark { get; set; }

        public int ExamSectionId { get; set; }
        public virtual QualityExamSection ExamSection { get; set; }

        public int TakeId { get; set; }
        public virtual QualityExamUserTake Take { get; set; }

        public virtual ICollection<QualityExamUserTakeSectionField> Fields { get; set; }

        public QualityExamUserTakeSection() : base()
        {
            Fields = new HashSet<QualityExamUserTakeSectionField>();
        }

    }  
}  