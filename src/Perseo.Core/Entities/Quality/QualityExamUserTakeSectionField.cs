using System.Collections.Generic;

namespace Perseo.Core
{  
    public class QualityExamUserTakeSectionField : Entity
    {
        public double Mark { get; set; }
        public string Answer { get; set; }

        public int ExamFieldId { get; set; }
        public virtual QualityExamSectionField ExamField { get; set; }
        
        public int SectionId { get; set; }
        public virtual QualityExamUserTakeSection Section { get; set; }

        public virtual ICollection<QualityExamUserTakeSectionFieldOption> Options { get; set; }

        public QualityExamUserTakeSectionField() : base()
        {
            Options = new HashSet<QualityExamUserTakeSectionFieldOption>();
        }

    }  
}  