using System.Collections.Generic;

namespace Perseo.Core
{  
    public class QualityAuditUserTake : EntityDateRange
    {
        public int AuditUserId { get; set; }
        public virtual QualityAuditUser AuditUser { get; set; }

        public int AuditorId { get; set; }
        public virtual User Auditor { get; set; }

        public double Mark { get; set; }
        public bool? Pass { get; set; }

        public string Content { get; set; }

        public virtual ICollection<QualityAuditUserTakeSection> Sections { get; set; }

        public QualityAuditUserTake() : base()
        {
            Sections = new HashSet<QualityAuditUserTakeSection>();
        }
    }
}  