
namespace Perseo.Core
{  
    public class CalendarDay:Entity
    {          
        public int CalendarId { get; set; }
        public virtual Calendar Calendar { get; set; }

        public int Year { get; set; } 
        public int Month { get; set; } 
        public int Day { get; set; }

        public string Name { get; set; }
        public DayType Type { get; set; }

        public CalendarDay() : base() { }
    }  
}  