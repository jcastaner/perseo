namespace Perseo.Core
{
    public class UserRate:EntityDateRange
    {
        public int UserId { get; set; }
        public virtual User User { get; set; }
        public int RateId { get; set; }
        public virtual Rate Rate { get; set; }  

        public UserRate() : base() { }
    } 

}