using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Perseo.Core
{
    public partial class User
    {
        public string FullName
        {
            get
            {
                string fn = FirstName + " " + MainSurname + " " + SecondSurname;
                return Regex.Replace(fn, @"\s+", " ");
            }
        }

        public string FullFamilyName
        {
            get
            {
                string fn = MainSurname + " " + SecondSurname + ", " + FirstName;
                return fn.Trim();
            }
        }

        private ICollection<Area> _OwnedAreas;

        public virtual ICollection<Area> OwnedAreas
        {

            get
            {
                if (_OwnedAreas != null) return _OwnedAreas;
                IEnumerable<Area> areas = Enumerable.Empty<Area>().ToList();
                if (UserArea.Any())
                {
                    UserArea.Where(x => !x.EndDate.HasValue &&
                    (x.Owner.GetValueOrDefault() == true || x.Authorized.GetValueOrDefault() == true)).ToList()
                    .ForEach(x =>
                    {
                        areas = areas.Concat(x.Area.FlatChildren);
                    });
                }

                return areas.Distinct().ToList();
            }
            set
            {
                _OwnedAreas = value;
            }    
            
        }

        private ICollection<Job> _OwnedJobs;

        public virtual ICollection<Job> OwnedJobs
        {
            get
            {
                if (_OwnedJobs != null) return _OwnedJobs;
                IEnumerable<Job> jobs = Enumerable.Empty<Job>().ToList();
                //if (_OwnedAreas.Any())
                //{
                //    _OwnedAreas.ToList().ForEach(x => {
                //        jobs = jobs.Concat(x.Job.Where(a => !a.EndDate.HasValue).ToList());
                //    });
                //}else 
                if (OwnedAreas.Any())
                {
                    OwnedAreas.ToList().ForEach(x => {
                        jobs = jobs.Concat(x.Job.Where(a => !a.EndDate.HasValue).ToList());
                    });
                }
                return jobs.Distinct().ToList();
            }
            set
            {
                _OwnedJobs = value;
            }
        }

        public virtual ICollection<Role> Roles
        {
            get
            {
                IList<Role> roles = Enumerable.Empty<Role>().ToList();
                if (UserRole.Any())
                {
                    UserRole.ToList().ForEach(x => {
                        roles.Add(x.Role);
                    });
                }
                return roles;
            }
        }

        public virtual ICollection<UserArea> OwnedUserAreas
        {
            get
            {
                if (this.UserArea.Any())
                {
                    return this.UserArea.Where(x => !x.EndDate.HasValue &&
                    (x.Owner.GetValueOrDefault() == true || x.Authorized.GetValueOrDefault() == true)).ToList();
                }
                else
                {
                    return null;
                }
            }
        }

        public virtual UserArea LastUserArea
        {
            get
            {
                return this.UserArea.Where(x => x.Owner.GetValueOrDefault() == false && x.Authorized.GetValueOrDefault() == false
                    && !x.EndDate.HasValue)
                    .FirstOrDefault();
            }
        }
        
        public virtual UserOffice LastUserOffice
        {
            get
            {
                return this.UserOffice.Where(x => !x.EndDate.HasValue).FirstOrDefault();
            }
        }

        public virtual UserPosition LastUserPosition
        {
            get
            {
                return this.UserPosition.Where(x => !x.EndDate.HasValue).FirstOrDefault();
            }
        }

        public virtual UserRate LastUserRate
        {
            get
            {
                return this.UserRate.Where(x => !x.EndDate.HasValue).FirstOrDefault();
            }
        }

        public virtual UserSalary LastUserSalary
        {
            get
            {
                return this.UserSalary.Where(x => !x.EndDate.HasValue).FirstOrDefault();
            }
        }

        public virtual UserCollective LastUserCollective
        {
            get
            {
                return this.UserCollective.Where(x => !x.EndDate.HasValue).FirstOrDefault();
            }
        }

        public virtual UserCollectiveCategory LastUserCollectiveCategory
        {
            get
            {
                return this.UserCollectiveCategory.Where(x => !x.EndDate.HasValue).FirstOrDefault();
            }
        }

        public virtual UserCategory LastUserCategory
        {
            get
            {
                return this.UserCategory.Where(x => !x.EndDate.HasValue).FirstOrDefault();
            }
        }

        public virtual UserShift LastUserShift
        {
            get
            {
                return this.UserShift.Where(x => !x.EndDate.HasValue).FirstOrDefault();
            }
        }

        public virtual UserContract LastUserContract
        {
            get
            {
                return this.UserContract.Where(x => !x.EndDate.HasValue).FirstOrDefault();
            }
        }

        public virtual UserWorkConditions LastUserWorkConditions
        {
            get
            {
                return this.UserWorkConditions.Where(x => !x.EndDate.HasValue).FirstOrDefault();
            }
        }

        public virtual User Supervisor
        {
            get
            {
                try
                {
                    Area area = LastUserArea.Area;
                    while(area.Owner == null)
                    {
                        area = area.Parent;
                    }
                    return area.Owner;
                }
                catch { }
                return null;
            }
        }


        public bool IsInRole(params string[] roleNames)
        {
            return Roles.Select(x => x.Name).Distinct().Intersect(roleNames).Any();
        }

    }
}  