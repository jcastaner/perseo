namespace Perseo.Core
{  
    public class UserSalary:EntityDateRange
    {  
        public float Salary { get; set; }  
        public float? Variable { get; set; } 
        public string Currency { get; set; }

        public int UserId { get; set; }
        public virtual User User { get; set; }

        public UserSalary() : base() { }
    }  
}  