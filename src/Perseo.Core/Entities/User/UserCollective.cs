namespace Perseo.Core
{
    public class UserCollective:EntityDateRange
    {
        public int UserId { get; set; }
        public virtual User User { get; set; }
        public int CollectiveId { get; set; }
        public virtual Collective Collective { get; set; }

        public UserCollective() : base() { }
    } 

}