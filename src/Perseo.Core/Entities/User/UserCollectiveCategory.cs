namespace Perseo.Core
{  
    public class UserCollectiveCategory:EntityDateRange
    {  
        public string Category { get; set; }

        public int UserId { get; set; }
        public virtual User User { get; set; }

        public UserCollectiveCategory() : base() { }
    }  
}  