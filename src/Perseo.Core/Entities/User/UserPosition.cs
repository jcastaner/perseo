namespace Perseo.Core
{  
    public class UserPosition:EntityDateRange
    {
        public int UserId { get; set; }
        public virtual User User { get; set; }
        public string Name { get; set; }

        public UserPosition() : base() { }
    }  
}  