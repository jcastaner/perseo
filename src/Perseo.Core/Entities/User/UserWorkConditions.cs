namespace Perseo.Core
{
    public class UserWorkConditions:EntityDateRange
    {

        public UserWorkConditions() : base()
        {
        }

        public int UserId { get; set; }
        public virtual User User { get; set; }

        public double WeekWorkHours { get; set; }
        public double YearWorkHours { get; set; }
        public double YearDoctorHours { get; set; }
        public double YearVacationDays { get; set; }
        public double YearSpareDays { get; set; }

    }

}