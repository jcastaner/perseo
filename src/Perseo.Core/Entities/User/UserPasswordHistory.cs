namespace Perseo.Core
{  
    public class UserPasswordHistory : EntityDateRange
    {
        public int UserId { get; set; }
        public virtual User User { get; set; }
        public string PasswordHash { get; set; }

        public UserPasswordHistory() : base() {
        }
    }  
}  