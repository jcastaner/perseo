using System;
using System.Collections.Generic;

namespace Perseo.Core
{
    public partial class User : Entity
    {
        private string _UserName;
        private string _Email;
        private string _NormalizedUserName;
        private string _NormalizedEmail;
        private string _Avatar;

        public string UserName {
            get {
                return _UserName;
            }
            set {
                _UserName = value;
                _NormalizedUserName = value.ToUpper();
            }
        }
        public string Email {
            get
            {
                return _Email;
            }
            set
            {
                _Email = value;
                _NormalizedEmail = value.ToUpper();
            }
        }
        public string NormalizedUserName {
            get
            {
                return _NormalizedUserName;
            }
            set
            {
                _NormalizedUserName = value;
            }
        }
        public string NormalizedEmail {
            get
            {
                return _NormalizedEmail;
            }
            set
            {
                _NormalizedEmail = value;
            }
        }

        public string PhoneNumber { get; set; }
        public string MobileNumber { get; set; }
        public string NetworkUser { get; set; }
        public int? EmployeeId { get; set; }
        public string FirstName { get; set; }
        public string MainSurname { get; set; }
        public string SecondSurname { get; set; }
        public string PasswordHash { get; set; }
        public DateTime? LastPasswordChangedDate { get; set; }
        public DateTime? LastLoginDate { get; set; }

        public AcademicDegree? AcademicDegree { get; set; }
        public string Nationality { get; set; }
        public string NationalId { get; set; }
        public DateTime? BirthDate { get; set; }
        public string BirthPlace { get; set; }
        public string PersonalPhoneNumber { get; set; }
        public string PersonalMobileNumber { get; set; }
        public string PersonalEmail { get; set; }
        public string Address { get; set; }
        public string Zip { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }

        public bool EmailConfirmed { get; set; }
        public int AccessFailedCount { get; set; }
        public bool LockoutEnabled { get; set; }
        public DateTimeOffset? LockoutEnd { get; set; }
        public string ConcurrencyStamp { get; set; }
        public string SecurityStamp { get; set; }
        public bool? Flag { get; set; }
        public int Alerts { get; set; }
        public int Messages { get; set; }
        public int Tasks { get; set; }
        public bool? RefreshCookie { get; set; }
        public bool? ChangePassword { get; set; }
        public string Avatar {
            get
            {
                return string.IsNullOrEmpty(_Avatar) ? "0.jpg" : _Avatar;
            }
            set
            {
                    _Avatar = value;          
            }
        }
        
        public virtual ICollection<UserPasswordHistory> UserPasswordHistory { get; set; }
        public virtual ICollection<UserRole> UserRole { get; set; }
        public virtual ICollection<UserArea> UserArea { get; set; }
        public virtual ICollection<UserOffice> UserOffice { get; set; }
        public virtual ICollection<UserPosition> UserPosition { get; set; }
        public virtual ICollection<UserRate> UserRate { get; set; }
        public virtual ICollection<UserSalary> UserSalary { get; set; }
        public virtual ICollection<UserCollective> UserCollective { get; set; }
        public virtual ICollection<UserCollectiveCategory> UserCollectiveCategory { get; set; }
        public virtual ICollection<UserCategory> UserCategory { get; set; }
        public virtual ICollection<UserShift> UserShift { get; set; }
        public virtual ICollection<UserOperational> UserOperational { get; set; }
        public virtual ICollection<ScheduleDate> ScheduleDate { get; set; }
        public virtual ICollection<UserContract> UserContract { get; set; }
        public virtual ICollection<UserWorkConditions> UserWorkConditions { get; set; }
        public virtual ICollection<Bookmark> Bookmark { get; set; }
        public virtual ICollection<TrainingUser> TrainingUser { get; set; }
        public virtual ICollection<QualityOwner> QualityOwner { get; set; }
        public virtual ICollection<QualityUser> QualityUser { get; set; }
        public virtual ICollection<QualityAuditUser> QualityAuditUser { get; set; }
        public virtual ICollection<QualityAuditOwner> QualityAuditOwner { get; set; }
        public virtual ICollection<QualityExamUser> QualityExamUser { get; set; }
        public virtual ICollection<QualityExamOwner> QualityExamOwner { get; set; }
        public virtual ICollection<NonConformityCard> NonConformityCardTarget { get; set; }
        public virtual ICollection<NonConformityCard> NonConformityCardOrigin { get; set; }
        public virtual ICollection<NonConformityCardSign> NonConformityCardSign { get; set; }

        public User(): base()
        {
            UserPasswordHistory = new HashSet<UserPasswordHistory>();
            UserRole = new HashSet<UserRole>();
            UserArea = new HashSet<UserArea>();
            UserOffice = new HashSet<UserOffice>();
            UserPosition = new HashSet<UserPosition>();
            UserRate = new HashSet<UserRate>();
            UserSalary = new HashSet<UserSalary>();
            UserCollective = new HashSet<UserCollective>();
            UserCollectiveCategory = new HashSet<UserCollectiveCategory>();
            UserCategory = new HashSet<UserCategory>();
            UserShift = new HashSet<UserShift>();
            UserOperational = new HashSet<UserOperational>();
            ScheduleDate = new HashSet<ScheduleDate>();
            UserContract = new HashSet<UserContract>();
            UserWorkConditions = new HashSet<UserWorkConditions>();
            Bookmark = new HashSet<Bookmark>();
            TrainingUser = new HashSet<TrainingUser>();
            QualityOwner = new HashSet<QualityOwner>();
            QualityUser = new HashSet<QualityUser>();
            QualityAuditUser = new HashSet<QualityAuditUser>();
            QualityAuditOwner = new HashSet<QualityAuditOwner>();
            QualityExamUser = new HashSet<QualityExamUser>();
            QualityExamOwner = new HashSet<QualityExamOwner>();
            NonConformityCardTarget = new HashSet<NonConformityCard>();
            NonConformityCardOrigin = new HashSet<NonConformityCard>();
            NonConformityCardSign = new HashSet<NonConformityCardSign>();
        }

    }
}  