using System.Collections.Generic;

namespace Perseo.Core
{  
    public class Category:Entity
    {          
        public Category () : base()
        {
            UserCategory = new HashSet<UserCategory>();
        }
        public int Level { get; set; } 
        public string Name { get; set; } 

        public virtual ICollection<UserCategory> UserCategory { get; set; }
    }  
}  