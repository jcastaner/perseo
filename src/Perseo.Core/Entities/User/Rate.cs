using System.Collections.Generic;

namespace Perseo.Core
{
    public class Rate : Entity
    {
        public int Year { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public double Value { get; set; }
        public string Currency { get; set; }
        public virtual ICollection<UserRate> UserRate { get; set; }

        public Rate() : base()
        {
            UserRate = new HashSet<UserRate>();
        }
    } 

}
