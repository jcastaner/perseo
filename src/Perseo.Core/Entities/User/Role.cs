﻿using System.Collections.Generic;

namespace Perseo.Core
{
    public class Role : Entity
    {
        public string ConcurrencyStamp { get; set; }
        public string Name { get; set; }
        public string NormalizedName { get; set; }

        public virtual ICollection<UserRole> UserRole { get; set; }

        public Role() : base()
        {
            UserRole = new HashSet<UserRole>();
        }
    }
}
