using System.Collections.Generic;

namespace Perseo.Core
{  
    public class Calendar:Entity
    {          
        public Calendar() : base()
        {
            Days = new HashSet<CalendarDay>();
            Office = new HashSet<Office>();
        }
        public string Name { get; set; } 
        public string Description { get; set; } 

        public virtual ICollection<CalendarDay> Days { get; set; }
        public virtual ICollection<Office> Office { get; set; }
    }  
}  