using System.Collections.Generic;

namespace Perseo.Core
{
    public class Company : Entity
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public bool External { get; set; }

        public virtual ICollection<UserContract> UserContract { get; set; }

        public Company() : base()
        {
            this.UserContract = new HashSet<UserContract>();
        }
    } 

}
