namespace Perseo.Core
{
    public class UserContract:EntityDateRange
    {

        public int UserId { get; set; }
        public virtual User User { get; set; }

        public virtual Company Company { get; set; }
        public int CompanyId { get; set; }

        public bool External { get; set; }

        public ContractPeriodType ContractPeriod { get; set; }

        public bool? WorkLocalHolydays { get; set; }
        public bool? WorkNationalHolidays { get; set; }
        public bool? WorkWeekends { get; set; }

        public UserContract() : base()
        {
        }

    }

}