namespace Perseo.Core
{  
    public class UserOffice:EntityDateRange
    {
        public int UserId { get; set; }
        public virtual User User { get; set; }
        public int OfficeId { get; set; }
        public virtual Office Office { get; set; } 

        public UserOffice() : base() { }
    }  
}  