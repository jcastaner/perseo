using System.Collections.Generic;

namespace Perseo.Core
{  
    public class Office:Entity
    {          

        public string Name { get; set; } 

        public virtual Calendar Calendar { get; set; }
        public int? CalendarId { get; set; }
        public virtual ICollection<UserOffice> UserOffice { get; set; }

        public Office() : base()
        {
            UserOffice = new HashSet<UserOffice>();
        }
    }  
}  