using System;

namespace Perseo.Core
{  
    public class UserOperational:Entity
    {
        public string Key { get; set; }
        public string Value { get; set; }
        public int UserId { get; set; }
        public virtual User User { get; set; }

        public UserOperational() : base() { }
    }  
}  