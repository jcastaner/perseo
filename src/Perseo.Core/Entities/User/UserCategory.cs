namespace Perseo.Core
{  
    public class UserCategory:EntityDateRange
    {
        public int UserId { get; set; }
        public virtual User User { get; set; }
        public int CategoryId { get; set; }
        public virtual Category Category { get; set; } 

        public UserCategory() : base() { }
        
    }  
}  