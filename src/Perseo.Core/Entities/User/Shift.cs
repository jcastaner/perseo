using System.Collections.Generic;

namespace Perseo.Core
{  
    public class Shift:Entity
    {
        public int Week { get; set; }
        public int WeekDay { get; set; }
        public string Pattern { get; set; }
        public int UserShiftId { get; set; }
        public virtual UserShift UserShift { get; set; }
        
        public Shift() : base() { }
    }  
}  