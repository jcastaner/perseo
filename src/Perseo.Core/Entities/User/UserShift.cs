using System.Collections.Generic;

namespace Perseo.Core
{  
    public class UserShift:EntityDateRange
    {
        public string Description { get; set; }
        public int UserId { get; set; }
        public virtual User User { get; set; }
        public virtual ICollection<Shift> Shift { get; set; }

        public UserShift() : base()
        {
            Shift = new HashSet<Shift>();
        }
    }  
}  