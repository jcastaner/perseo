namespace Perseo.Core
{  
    public class UserArea : EntityDateRange
    {
        public bool? Owner { get; set; }
        public bool? Authorized { get; set; }
        public int UserId { get; set; }
        public virtual User User { get; set; }
        public int AreaId { get; set; }
        public virtual Area Area { get; set; }

        public UserArea() : base() {
            Owner = false;
            Authorized = false;
        }
    }  
}  