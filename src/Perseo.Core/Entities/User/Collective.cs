using System.Collections.Generic;

namespace Perseo.Core
{
    public class Collective:EntityDateRange
    {  
        public string Name { get; set; }
        public double YearWorkHours { get; set; }
        public double YearDoctorHours { get; set; }
        public double YearVacationDays { get; set; }
        public double YearSpareDays { get; set; }
        public int Other { get; set; }

        public virtual ICollection<UserCollective> UserCollective { get; set; }

        public Collective(): base()
        {
            this.UserCollective = new HashSet<UserCollective>();
        }
    } 

}