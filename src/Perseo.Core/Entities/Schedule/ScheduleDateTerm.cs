using System;
using System.Collections.Generic;

namespace Perseo.Core
{
    public class ScheduleDateTerm : Entity
    {

        public string Pattern { get; set; }
        public double Hours { get; set; }
        public WorkHoursType? HoursType { get; set; }

        public int ScheduleDateId { get; set; }

        public virtual ScheduleDate ScheduleDate { get; set; }
        public virtual ICollection<ScheduleDateTermJob> ScheduleDateTermJob { get; set; }

        public ScheduleDateTerm() : base()
        {
            ScheduleDateTermJob = new HashSet<ScheduleDateTermJob>();
        }
    } 
}
