
namespace Perseo.Core
{  
    public class ScheduleDateTermJob : Entity
    {
        public int ScheduleDateTermId { get; set; }
        public int JobId { get; set; }
        public virtual ScheduleDateTerm ScheduleDateTerm { get; set; }
        public virtual Job Job { get; set; }   

        public ScheduleDateTermJob() : base() { }
    }  
}  