﻿using System;
using System.Collections.Generic;

namespace Perseo.Core
{
    public class Notification : EntityDateRange
    {
        public Notification() : base()
        {
        }

        public int UserId { get; set; }
        public virtual User User { get; set; }
      
        public NotificationType Type { get; set; }
        public bool Priority { get; set; }
        public string Summary { get; set; }
        public string Description { get; set; }
        public string Url { get; set; }
    }
}
