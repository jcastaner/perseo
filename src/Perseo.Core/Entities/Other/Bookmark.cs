using System.Collections.Generic;

namespace Perseo.Core
{  
    public class Bookmark : Entity
    {          
        public string Name { get; set; }
        public string Url { get; set; }
        public bool Local { get; set; }
        public bool Group { get; set; }
        public int Order { get; set; }

        public int? ParentId { get; set; }
        public virtual Bookmark Parent { get; set; }

        public virtual ICollection<Bookmark> Children { get; set; }

        public int UserId { get; set; }
        public virtual User User { get; set; }

        public Bookmark() : base()
        {
            Children = new HashSet<Bookmark>();
            Order = 0;
        }
        
    }  
}  