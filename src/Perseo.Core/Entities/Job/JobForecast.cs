﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Perseo.Core
{
    public class JobForecast : Entity
    {
        public DateTime? Date { get; set; }

        public int Year { get; set; }
        public int Month { get; set; }
        public int Day { get; set; }

        public double Forecast { get; set; }

        public int JobId { get; set; }
        public virtual Job Job { get; set; }
        public IEnumerable<double> Events { get; set; }
        
    }
}
