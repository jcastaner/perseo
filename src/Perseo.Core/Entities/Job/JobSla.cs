﻿

namespace Perseo.Core
{
    public class JobSla : EntityDateRange
    {        
        public string SlaDescription { get; set; }
        public char SlaComparator { get; set; }
        public double? Sla { get; set; }
        public double? Rate { get; set; }
        public double? Tmo { get; set; }
//        public double? Aht { get; set; }
        public double? Wait { get; set; }
        public double? Queue { get; set; }

        public int JobId { get; set; }
        public virtual Job Job { get; set; }
    }
}
