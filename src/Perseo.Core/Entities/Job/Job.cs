using System;
using System.Collections.Generic;
using System.Linq;

namespace Perseo.Core
{  
    public class Job:EntityDateRange
    {          

        public string Name { get; set; }
        public string Description { get; set; }
        public JobType Type { get; set; }
        public int AreaId { get; set; }
        public bool? Default { get; set; }
        public string Acronym { get; set; }

        public virtual Area Area { get; set; }

        public virtual ICollection<JobConfig> JobConfig { get; set; }
        public virtual ICollection<JobSla> JobSla { get; set; }        
        public virtual ICollection<JobForecast> JobForecast { get; set; }
        public virtual ICollection<JobKpi> JobKpi { get; set; }
        public virtual ICollection<ScheduleDateTermJob> ScheduleDateTermJob { get; set; }
        public virtual ICollection<Forecast> Forecast { get; set; }
        public virtual ICollection<NonConformityCard> NonConformityCard { get; set; }
        //public virtual ICollection<AreaCodeWbs> AreaCodeWbs { get; set; }

        public Job()
        {
            JobConfig = new HashSet<JobConfig>();            
            JobSla = new HashSet<JobSla>();
            JobForecast = new HashSet<JobForecast>();
            JobKpi = new HashSet<JobKpi>();
            ScheduleDateTermJob = new HashSet<ScheduleDateTermJob>();
            Forecast = new HashSet<Forecast>();
            NonConformityCard = new HashSet<NonConformityCard>();
        //this.AreaCodeWbs = new HashSet<AreaCodeWbs>();
    }

        public virtual JobSla LastJobSla
        {
            get
            {
                return this.JobSla.Where (x => !x.EndDate.HasValue).FirstOrDefault();
            }
        }
    }  
}  