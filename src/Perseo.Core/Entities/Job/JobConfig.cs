﻿
namespace Perseo.Core
{
    public class JobConfig : EntityDateRange
    {
        public BillingType BillingType { get; set; }
        public TaskType TaskType { get; set; }

        public int JobId { get; set; }
        public virtual Job Job { get; set; }
    }
}
