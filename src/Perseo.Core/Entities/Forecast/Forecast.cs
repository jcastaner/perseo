﻿using System;

namespace Perseo.Core
{
    public class Forecast : Entity
    {
        public DateTime Date { get; set; }

        public int Year { get; set; }
        public int Month { get; set; }

        public double Events { get; set; }

        public int JobId { get; set; }
        public virtual Job Job { get; set; }
        
    }
}
