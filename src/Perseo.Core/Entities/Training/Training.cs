using System;
using System.Collections.Generic;

namespace Perseo.Core
{  
    public class Training : EntityDateRange
    {
        public string Name { get; set; }
        public string Description { get; set; }

        public int AreaId { get; set; }

        public virtual Area Area { get; set; }

        public virtual ICollection<TrainingUser> Users { get; set; }

        public virtual ICollection<TrainingTest> Tests { get; set; }

        public Training() : base()
        {
            Users = new HashSet<TrainingUser>();
            Tests = new HashSet<TrainingTest>();
        }
    }  
}  