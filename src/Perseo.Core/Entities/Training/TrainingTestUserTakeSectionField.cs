using System.Collections.Generic;

namespace Perseo.Core
{  
    public class TrainingTestUserTakeSectionField : Entity
    {
        public double Mark { get; set; }
        public string Answer { get; set; }

        public int TestFieldId { get; set; }
        public virtual TrainingTestSectionField TestField { get; set; }
        
        public int SectionId { get; set; }
        public virtual TrainingTestUserTakeSection Section { get; set; }

        public virtual ICollection<TrainingTestUserTakeSectionFieldOption> Options { get; set; }

        public TrainingTestUserTakeSectionField() : base()
        {
            Options = new HashSet<TrainingTestUserTakeSectionFieldOption>();
        }
    }  
}  