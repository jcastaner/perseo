using System.Collections.Generic;

namespace Perseo.Core
{  
    public class TrainingTestUserTake : EntityDateRange
    {
        public int TestUserId { get; set; }
        public virtual TrainingTestUser TestUser { get; set; }

        public int Attempt { get; set; }
        public double Mark { get; set; }
        public bool? Pass { get; set; }

        public string Content { get; set; }

        public virtual ICollection<TrainingTestUserTakeSection> Sections { get; set; }

        public TrainingTestUserTake() : base()
        {
            Sections = new HashSet<TrainingTestUserTakeSection>();
        }
    }
}  