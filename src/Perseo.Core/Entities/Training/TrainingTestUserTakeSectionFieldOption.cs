using System.Collections.Generic;

namespace Perseo.Core
{  
    public class TrainingTestUserTakeSectionFieldOption : Entity
    {
        public bool? Mark { get; set; }
        
        public int TestOptionId { get; set; }
        public virtual TrainingTestSectionFieldOption TestOption{ get; set; }

        public int FieldId { get; set; }
        public virtual TrainingTestUserTakeSectionField Field { get; set; }

        public TrainingTestUserTakeSectionFieldOption() : base()
        {

        }

    }  
}  