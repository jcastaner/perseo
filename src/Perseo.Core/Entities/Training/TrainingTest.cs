using System;
using System.Collections.Generic;

namespace Perseo.Core
{  
    public class TrainingTest : EntityDateRange
    {
        public string Name { get; set; }
        public string Description { get; set; }

        public double PassMark { get; set; }
        public int Attempts { get; set; }
        public int? Time { get; set; }

        public int TrainingId { get; set; }
        public virtual Training Training { get; set; }

        public virtual ICollection<TrainingTestOwner> Owners { get; set; }
        public virtual ICollection<TrainingTestUser> Users { get; set; }

        public virtual ICollection<TrainingTestSection> Sections { get; set; }

        public TrainingTest() : base()
        {
            Owners = new HashSet<TrainingTestOwner>();
            Users = new HashSet<TrainingTestUser>();
            Sections = new HashSet<TrainingTestSection>();
            Attempts = 1;
        }
    }  
}  