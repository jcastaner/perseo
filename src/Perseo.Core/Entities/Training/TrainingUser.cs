using System;
using System.Collections.Generic;

namespace Perseo.Core
{  
    public class TrainingUser : Entity
    {
        public int TrainingId { get; set; }

        public virtual Training Training { get; set; }

        public int UserId { get; set; }
        public virtual User User { get; set; }

        public bool Trainer { get; set; }
        public bool Attendant { get; set; }
        public bool TrainingCompleted { get; set; }

        public TrainingUser() : base() { }
    }  
}  