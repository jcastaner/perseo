using System.Collections.Generic;

namespace Perseo.Core
{  
    public class TrainingTestOwner : Entity
    {
        public int TestId { get; set; }
        public virtual TrainingTest Test { get; set; }

        public int UserId { get; set; }
        public virtual User User { get; set; }

        public TrainingTestOwner() : base() { }
    }
}  