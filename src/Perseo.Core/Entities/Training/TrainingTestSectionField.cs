using System.Collections.Generic;

namespace Perseo.Core
{  
    public class TrainingTestSectionField : Entity
    {
        public new int? Id { get; set; }
        public AuditFieldType Type { get; set; }
        public int Order { get; set; }
        public string Name { get; set; }
        public string Content { get; set; }
        public bool? Reasoned { get; set; }
        public double Weighing { get; set; }

        public int SectionId { get; set; }
        public virtual TrainingTestSection Section { get; set; }

        public virtual ICollection<TrainingTestSectionFieldOption> Options { get; set; }

        public TrainingTestSectionField() : base()
        {
            Options = new HashSet<TrainingTestSectionFieldOption>();
            Order = 0;
            Weighing = 0;
        }
    }  
}  