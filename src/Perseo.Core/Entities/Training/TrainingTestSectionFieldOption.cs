using System.Collections.Generic;

namespace Perseo.Core
{  
    public class TrainingTestSectionFieldOption : Entity
    {
        public new int? Id { get; set; }
        public int Order { get; set; }
        public string Content { get; set; }
        public bool? Mark { get; set; }
        public double Value { get; set; }
        

        public int FieldId { get; set; }
        public virtual TrainingTestSectionField Field { get; set; }

        public TrainingTestSectionFieldOption() : base()
        {
            Value = 0;
            Order = 0;
        }
    }  
}  