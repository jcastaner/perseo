using System.Collections.Generic;

namespace Perseo.Core
{  
    public class TrainingTestUserTakeSection : Entity
    {
        public double Mark { get; set; }

        public int TestSectionId { get; set; }
        public virtual TrainingTestSection TestSection { get; set; }

        public int TakeId { get; set; }
        public virtual TrainingTestUserTake Take { get; set; }

        public virtual ICollection<TrainingTestUserTakeSectionField> Fields { get; set; }

        public TrainingTestUserTakeSection() : base()
        {
            Fields = new HashSet<TrainingTestUserTakeSectionField>();
        }

    }  
}  