using System.Collections.Generic;

namespace Perseo.Core
{  
    public class TrainingTestSection : Entity
    {
        public new int? Id { get; set; }
        public int Order { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public double Weighing { get; set; }

        public int TestId { get; set; }
        public virtual TrainingTest Test { get; set; }

        public virtual ICollection<TrainingTestSectionField> Fields { get; set; }

        public TrainingTestSection() : base()
        {
            Fields = new HashSet<TrainingTestSectionField>();
            Weighing = 1;
        }
    }  
}  