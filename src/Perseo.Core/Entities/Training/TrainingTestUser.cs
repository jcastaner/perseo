using System.Collections.Generic;

namespace Perseo.Core
{  
    public class TrainingTestUser : Entity
    {
        public int TestId { get; set; }
        public virtual TrainingTest Test { get; set; }

        public int UserId { get; set; }
        public virtual User User { get; set; }

        public virtual ICollection<TrainingTestUserTake> Takes { get; set; }

        public TrainingTestUser() : base()
        {
            Takes = new HashSet<TrainingTestUserTake>();
        }
    }
}  