﻿using System;  
using System.Collections.Generic;  
using System.Threading.Tasks;  
  
namespace Perseo.Core
{
	public abstract class Entity : IEntity
	{
        public Entity()
        {
            Active = true;
        }

		protected int? _requestedHashCode;

        public int Id { get; set; }

        protected bool _Active;
        public bool Active {
            get
            {
                return _Active;
            }
            set
            {
                _Active = value;
            }
        }

        public bool IsTransient()
		{
			return this.Id == default(Int32);
		}

		public override bool Equals(object obj)
		{
			if (obj == null || !(obj is Entity))
				return false;
			if (Object.ReferenceEquals(this, obj))
				return true;
			if (this.GetType() != obj.GetType())
				return false;
			Entity item = (Entity)obj;
			if (item.IsTransient() || this.IsTransient())
				return false;
			else
				return item.Id == this.Id;
		}

		public override int GetHashCode()
		{
			if (!IsTransient())
			{
				if (!_requestedHashCode.HasValue)
					_requestedHashCode = this.Id.GetHashCode() ^ 31; 
				return _requestedHashCode.Value;
			}
			else
				return base.GetHashCode();
		}
		public static bool operator ==(Entity left, Entity right)
		{
			if (Object.Equals(left, null))
				return (Object.Equals(right, null)) ? true : false;
			else
				return left.Equals(right);
		}
		public static bool operator !=(Entity left, Entity right)
		{
			return !(left == right);
		}
	}
}
