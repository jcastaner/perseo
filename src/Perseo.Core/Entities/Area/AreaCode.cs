﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Perseo.Core
{
    public class AreaCode : EntityDateRange
    {

        public AreaCode() : base()
        {
            AreaCodeWbs = new HashSet<AreaCodeWbs>();
        }

        public string Name { get; set; }
        public string Description { get; set; }

        public int AreaId { get; set; }
        public virtual Area Area { get; set; }

        public virtual ICollection<AreaCodeWbs> AreaCodeWbs { get; set; }
    }
}
