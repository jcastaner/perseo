using System;
using System.Collections.Generic;
using System.Linq;

namespace Perseo.Core
{
    public class Area : EntityDateRange
    {
        public Area() : base()
        {
            Children = new HashSet<Area>();
            Job = new HashSet<Job>();
            AreaCode = new HashSet<AreaCode>();
            UserArea = new HashSet<UserArea>();
            AreaCodeWbs = new HashSet<AreaCodeWbs>();
            Job = new HashSet<Job>();
            Quality = new HashSet<Quality>();
            Depth = 0;
        }

        public string Name { get; set; }
        public string Description { get; set; }
        public AreaType Type{ get; set; }

        public int? ParentId { get; set; }
        public string Lineage { get; set; }
        public int Depth { get; set; }

        public virtual Area Parent { get; set; }
        public virtual ICollection<Area> Children { get; set; }

        public virtual ICollection<Job> Job { get; set; }
        public virtual ICollection<UserArea> UserArea { get; set; }
        public virtual ICollection<AreaCode> AreaCode { get; set; }
        public virtual ICollection<AreaCodeWbs> AreaCodeWbs { get; set; }

        public virtual ICollection<Quality> Quality { get; set; }

        public virtual User Owner
        {
            get
            {
                try
                {
                    return UserArea.FirstOrDefault(o => o.Owner == true && o.EndDate == null).User;
                }
                catch  {}
                return null;
                //return new User();
              
            }
        }
        public virtual AreaCode Code
        {
            get
            {
                if (Type != AreaType.Organizational)
                {
                    try
                    {
                        AreaCode Code = AreaCode.FirstOrDefault(x => x.EndDate == null);
                        if (Code != null)
                        {
                            return Code;
                        }
                        else if (Wbs != null)
                        {
                            return Wbs.AreaCode;
                        }
                    }
                    catch { }
                }
                return null;
            }
        }
        public virtual AreaCodeWbs Wbs
        {
            get
            {
                try
                {
                    AreaCodeWbs Wbs = AreaCodeWbs.FirstOrDefault(x => x.EndDate == null);
                    if (Wbs != null)
                    {
                        return Wbs;
                    }
                }
                catch { }
                return null;
            }
        }
        public virtual String FullWbs
        {
            get
            {
                if (Type != AreaType.Organizational && Wbs != null)
                {
                    return Wbs.Parent != null ? Wbs.Parent.Name + Wbs.Name : Wbs.Name;
                }
                return null;
            }
        }
        public virtual String FullCode
        {
            get
            {
                if (Type != AreaType.Organizational && Code != null)
                {
                    return Code.Name + (Wbs != null ? "/" + FullWbs : "");
                }
                return null;
            }
        }
        public virtual IEnumerable<Area> FlatChildren
        { 
            get
            {
                return Flatten(this).ToList();
            }

        }
        private IEnumerable<Area> Flatten(Area node)
        {
            yield return node;
            if (node.Children.Any())
            {
                foreach (var child in node.Children)
                    foreach (var descendant in Flatten(child))
                        yield return descendant;
            }
        }


    }  
}  