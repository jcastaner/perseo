﻿using System.Collections.Generic;
using System.Linq;

namespace Perseo.Core
{
    public class AreaCodeWbs : EntityDateRange
    {

        public AreaCodeWbs() : base()
        {
            Children = new HashSet<AreaCodeWbs>();
        }
        public string Name { get; set; }
        public string Description { get; set; }

        public int? ParentId { get; set; }

        public virtual AreaCodeWbs Parent { get; set; }
        public virtual ICollection<AreaCodeWbs> Children { get; set; }

        public bool? HasChildren { get; set; }

        public int AreaCodeId { get; set; }
        public virtual AreaCode AreaCode { get; set; }

        public int? AreaId { get; set; }
        public virtual Area Area { get; set; }

        public virtual IEnumerable<AreaCodeWbs> FlatChildren
        {
            get
            {
                return Flatten(this).ToList();
            }

        }
        private IEnumerable<AreaCodeWbs> Flatten(AreaCodeWbs node)
        {
            yield return node;
            if (node.Children.Any())
            {
                foreach (var child in node.Children)
                    foreach (var descendant in Flatten(child))
                        yield return descendant;
            }
        }
    }
}
