﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Perseo.Core
{
    public class UserScheduleSummary
    {
        public int Days { get; set; }
        public double Hours { get; set; }
        public string JobName { get; set; }
        public JobType JobType { get; set; }
        public string AreaName { get; set; }

        public UserScheduleSummary() 
        { }

    }
}

