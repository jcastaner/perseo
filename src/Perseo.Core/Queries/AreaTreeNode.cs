﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Perseo.Core
{
    public class AreaTreeNode
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool Active { get; set; }
        public bool? Dedication { get; set; }
        public bool? Productive { get; set; }
        public int? ParentId { get; set; }
        public int? OwnerId { get; set; }
        public int? Users { get; set; }
        public string Code { get; set; }
        public int? CodeId { get; set; }
        public string Wbs { get; set; }
        public int? WbsId { get; set; }
        public AreaType Type { get; set; }
        public string OwnerFullName { get; set; }
        public string Lineage { get; set; }
        public int Depth { get; set; }

        public string FullCode {
            get {
                if (Code != null)
                {
                    if (Wbs != null)
                    {
                        return Code + "/" + Wbs;
                    }
                    return Code;
                }
                return null;
            }
            private set { }
            }

        public string OwnerAvatar
        {
            get
            {
                return string.IsNullOrEmpty(_OwnerAvatar) ? "0.jpg" : _OwnerAvatar;
            }
            set
            {
                _OwnerAvatar = value;
            }
        }
        private string _OwnerAvatar;


        public static Expression<Func<Area, AreaTreeNode>> Projection
        {
            get
            {
                return x => new AreaTreeNode()
                {
                    Id = x.Id,
                    Name = x.Name,
                    Description = x.Description,
                    Active = x.Active,
                    Dedication = true,
                    Productive = true,
                    ParentId = x.ParentId,
                    OwnerId = x.UserArea.FirstOrDefault(o => o.Owner == true && o.EndDate == null).UserId,
                    Users = x.UserArea.Count,
                    Code = x.AreaCode.FirstOrDefault(y => y.EndDate == null).Name,
                    CodeId = x.AreaCode.FirstOrDefault(y => y.EndDate == null).Id,
                    Wbs = "",
                    WbsId = 1,
                    Type = x.Type,
                    Depth = x.Depth,
                    Lineage = x.Lineage,
                    OwnerFullName = x.UserArea.Where(o => o.Owner == true && o.EndDate == null).Select( e => e.User.FirstName + " " + e.User.MainSurname ).FirstOrDefault(),
                    OwnerAvatar = x.UserArea.FirstOrDefault(o => o.Owner == true && o.EndDate == null).User.Avatar

                };
            }
        }

    }
}

