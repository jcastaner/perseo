﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Perseo.Core
{
    public class UserListNode
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public int? EmployeeId { get; set; }
        public string FirstName { get; set; }
        public string MainSurname { get; set; }
        public string SecondSurname { get; set; }
        public string Area { get; set; }
        public int AreaId { get; set; }
        public string Avatar
        {
            get
            {
                return string.IsNullOrEmpty(_Avatar) ? "0.jpg" : _Avatar;
            }
            set
            {
                _Avatar = value;
            }
        }
        private string _Avatar;

        public static Expression<Func<User, UserListNode>> Projection => x => new UserListNode()
        {
            Id = x.Id,
            UserName = x.UserName,
            Email = x.Email,
            EmployeeId = x.EmployeeId,
            FirstName = x.FirstName,
            MainSurname = x.MainSurname,
            SecondSurname = x.SecondSurname,
            Area = x.UserArea.FirstOrDefault(y => y.Owner != true && y.Authorized != true && !y.EndDate.HasValue).Area.Name,
            AreaId = x.UserArea.FirstOrDefault(y => y.Owner != true && y.Authorized != true && !y.EndDate.HasValue).AreaId,
            Avatar = x.Avatar
        };
    }
}

