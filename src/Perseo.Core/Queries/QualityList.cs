﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Perseo.Core
{
    public class QualityList
    {
        public int Id { get; set; }//quality
        public string Description { get; set; }
        public string AreaName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int CountAudit { get; set; }
        public int CountAuditForm { get; set; }
        public int CountUserQualityAuditForm { get; set; }
        public int CountUserQualityAudit { get; set; }
    }
}

