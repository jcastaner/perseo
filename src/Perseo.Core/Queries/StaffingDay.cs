﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Perseo.Core
{
    public class StaffingDay
    {
        public StaffingDay () 
        {
            this.Events = new HashSet<double>();
            this.RequiredAgents = new HashSet<double>();
            this.ActualAgents = new HashSet<double>();
        }

        public DateTime? Date { get; set; }
        public int Year { get; set; }
        public int Month { get; set; }
        public int Day { get; set; }
        public IEnumerable<double> Events { get; set; }
        public IEnumerable<double> RequiredAgents { get; set; }
        public IEnumerable<double> ActualAgents { get; set; }
    }
}

