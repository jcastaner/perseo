﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Perseo.Core
{
    public class UserDedication
    {
        public double TermHours { get; set; }
        public int DateYear { get; set; }
        public int DateMonth { get; set; }
        public int DateDay { get; set; }
        public double DateHours { get; set; }
        public string JobName { get; set; }
        public JobType JobType { get; set; }
        public bool JobDefault { get; set; }
        public string AreaName { get; set; }
        public AreaType AreaType { get; set; }
        public string AreaCodeName { get; set; }
        public string AreaCodeWbsName { get; set; }
        public int TermId { get; set; }
        public int UserId { get; set; }
        public int DateId { get; set; }
        public int AreaId { get; set; }
        public int? AreaParentId { get; set; }
        public int JobId { get; set; }
        public int? AreaCodeId { get; set; }
        public int? AreaCodeWbsId { get; set; }
        public int TermCount { get; set; }
        public double JobHours { get; set; }
        public UserDedication() 
        { }

    }
}

