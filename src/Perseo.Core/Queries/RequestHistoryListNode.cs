﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Perseo.Core
{
    public class RequestHistoryListNode
    {

        public int Id { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy hh:mm}")]
        public DateTime EntryDate { get; set; }

        public string Comment { get; set; }

        public RequestState State { get; set; }

        public string UserName { get; set; }

    }
}
