﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Perseo.Core
{
    public class StaffingDayPattern
    {
        public int Day { get; set; }
        public string Pattern { get; set; }
        public int JobId { get; set; }
        public double Dedication { get; set; }
}
}

