﻿using System;
using System.Collections.Generic;

namespace Perseo.Core
{
    public class RequestListNode
    {

        public int Id { get; set; }

        public bool? Active { get; set; }

        public RequestType Type { get; set; }

        public virtual RequestState State { get; set; }

        public DateTime CreationDate { get; set; }

        public DateTime LastChangeDate { get; set; }

        public PriorityType Priority { get; set; }

        public string Summary { get; set; }

        public string Description { get; set; }

        public int UserId { get; set; }

        public string UserName { get; set; }

        public int AssignedUserId { get; set; }

        public string AssignedUserName { get; set; }

    }
}
