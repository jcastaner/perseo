﻿using System.ComponentModel.DataAnnotations;

namespace Perseo.Core
{
    public enum PriorityType
    {
        [Display(Name = "PriorityType_Low", ResourceType = typeof(Resources.EnumResources))]
        Low = 1,
        [Display(Name = "PriorityType_Medium", ResourceType = typeof(Resources.EnumResources))]
        Medium = 2,
        [Display(Name = "PriorityType_High", ResourceType = typeof(Resources.EnumResources))]
        High = 3,
        [Display(Name = "PriorityType_Critical", ResourceType = typeof(Resources.EnumResources))]
        Critical = 4,
    
    }

}
