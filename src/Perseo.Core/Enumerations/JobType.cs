﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Perseo.Core
{
    [Flags]
    public enum JobType
    {
        [Display(Name = "JobType_Productive", ResourceType = typeof(Resources.EnumResources))]
        Productive = 1,
        [Display(Name = "JobType_General", ResourceType = typeof(Resources.EnumResources))]
        General = 2,
        [Display(Name = "JobType_Organizational", ResourceType = typeof(Resources.EnumResources))]
        Organizational = 4,
        [Display(Name = "JobType_Project", ResourceType = typeof(Resources.EnumResources))]
        Project = 8,
        [Display(Name = "JobType_Hollydays", ResourceType = typeof(Resources.EnumResources))]
        Hollydays = 16,
        [Display(Name = "JobType_LastYearHollydays", ResourceType = typeof(Resources.EnumResources))]
        LastYearHollydays = 32,
        [Display(Name = "JobType_FreeDays", ResourceType = typeof(Resources.EnumResources))]
        FreeDays = 64,
        [Display(Name = "JobType_UnionHours", ResourceType = typeof(Resources.EnumResources))]
        UnionHours = 128,
        [Display(Name = "JobType_SickLeave", ResourceType = typeof(Resources.EnumResources))]
        SickLeave = 256,
        [Display(Name = "JobType_DoctorHours", ResourceType = typeof(Resources.EnumResources))]
        DoctorHours = 512,
        [Display(Name = "JobType_PaidLeave", ResourceType = typeof(Resources.EnumResources))]
        PaidLeave = 1024,
        [Display(Name = "JobType_UnjustifiedAbsence", ResourceType = typeof(Resources.EnumResources))]
        UnjustifiedAbsence = 2048,

        [Display(Name = "JobType_LabourHours", ResourceType = typeof(Resources.EnumResources))]
        LabourHours = Productive | LastYearHollydays | PaidLeave | UnionHours | SickLeave | DoctorHours | FreeDays | UnjustifiedAbsence,
        [Display(Name = "JobType_Absenteeism", ResourceType = typeof(Resources.EnumResources))]
        Absenteeism = SickLeave | DoctorHours | UnjustifiedAbsence,
    }

}
