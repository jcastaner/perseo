﻿using System.ComponentModel.DataAnnotations;

namespace Perseo.Core
{
    public enum HumanErrorSubtype
    {
        [Display(Name = "HumanErrorSubtype_Confusion", ResourceType = typeof(Resources.EnumResources))]
        Confusion = 1,
        [Display(Name = "HumanErrorSubtype_Distraction", ResourceType = typeof(Resources.EnumResources))]
        Distraction = 2,
    }
}
