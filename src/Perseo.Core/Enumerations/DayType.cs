﻿using System.ComponentModel.DataAnnotations;

namespace Perseo.Core
{
    public enum DayType
    {
        [Display(Name = "DayType_WorkingDay", ResourceType = typeof(Resources.EnumResources))]
        WorkingDay = 1,
        [Display(Name = "DayType_NationalHoliday", ResourceType = typeof(Resources.EnumResources))]
        NationalHoliday = 2,
        [Display(Name = "DayType_RegionalHoliday", ResourceType = typeof(Resources.EnumResources))]
        RegionalHoliday = 3,
        [Display(Name = "DayType_LocalHoliday", ResourceType = typeof(Resources.EnumResources))]
        LocalHoliday = 4,
        [Display(Name = "DayType_Other", ResourceType = typeof(Resources.EnumResources))]
        Other = 99
    }
}