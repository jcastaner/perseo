﻿
using System.ComponentModel.DataAnnotations;

namespace Perseo.Core
{
    public enum IncidentTypology
    {
        [Display(Name = "IncidentTypology_Human", ResourceType = typeof(Resources.EnumResources))]
        Human = 1,
        [Display(Name = "IncidentTypology_Sofware", ResourceType = typeof(Resources.EnumResources))]
        Sofware = 2,
        [Display(Name = "IncidentTypology_IncorrectSupport", ResourceType = typeof(Resources.EnumResources))]
        IncorrectSupport = 3,
        [Display(Name = "IncidentTypology_CircuitWithoutVerification", ResourceType = typeof(Resources.EnumResources))]
        CircuitWithoutVerification = 4,
        [Display(Name = "IncidentTypology_LegalBreach", ResourceType = typeof(Resources.EnumResources))]
        LegalBreach = 5,
        [Display(Name = "IncidentTypology_NotApplicable", ResourceType = typeof(Resources.EnumResources))]
        NotApplicable = 6,
        [Display(Name = "IncidentTypology_Other", ResourceType = typeof(Resources.EnumResources))]
        Other = 7,
       
    }
}
