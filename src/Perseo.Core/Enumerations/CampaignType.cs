﻿using System.ComponentModel.DataAnnotations;

namespace Perseo.Core
{
    public enum CampaignType
    {
        [Display(Name = "CampaignType_PerformanceEvaluation", ResourceType = typeof(Resources.EnumResources))]
        PerformanceEvaluation = 1,
        [Display(Name = "CampaignType_KnowledgeEvaluation", ResourceType = typeof(Resources.EnumResources))]
        KnowledgeEvaluation = 2,
    }

}
