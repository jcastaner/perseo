﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Perseo.Core
{
    public enum BillingType
    {
        [Display(Name = "BillingType_Na", ResourceType = typeof(Resources.EnumResources))]
        Na = 0,
        [Display(Name = "BillingType_Event", ResourceType = typeof(Resources.EnumResources))]
        Event = 1,
        [Display(Name = "BillingType_Person", ResourceType = typeof(Resources.EnumResources))]
        Person = 2,
        [Display(Name = "BillingType_Time", ResourceType = typeof(Resources.EnumResources))]
        Time = 3,
        [Display(Name = "BillingType_Fixed", ResourceType = typeof(Resources.EnumResources))]
        Fixed = 4,
    }

}
