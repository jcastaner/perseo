﻿using System.ComponentModel.DataAnnotations;

namespace Perseo.Core
{
    public enum AuditFieldType
    {
        [Display(Name = "AuditFieldType_Assessment", ResourceType = typeof(Resources.EnumResources))]
        Assessment = 1,
        [Display(Name = "AuditFieldType_SingleChoice", ResourceType = typeof(Resources.EnumResources))]
        SingleChoice = 2,
        [Display(Name = "AuditFieldType_MultipleChoice", ResourceType = typeof(Resources.EnumResources))]
        MultipleChoice = 3
    }

}
