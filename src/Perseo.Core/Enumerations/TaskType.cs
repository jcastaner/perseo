﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Perseo.Core
{
    public enum TaskType
    {
        [Display(Name = "TaskType_General", ResourceType = typeof(Resources.EnumResources))]
        General = 1,
        [Display(Name = "TaskType_FrontOffice", ResourceType = typeof(Resources.EnumResources))]
        FrontOffice = 2,
        [Display(Name = "TaskType_BackOffice", ResourceType = typeof(Resources.EnumResources))]
        BackOffice = 3,
        [Display(Name = "TaskType_Mixed", ResourceType = typeof(Resources.EnumResources))]
        Mixed = 4,
    }

}
