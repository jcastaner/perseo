﻿using System.ComponentModel.DataAnnotations;

namespace Perseo.Core
{
    public enum WorkHoursType
    {
        [Display(Name = "WorkHoursType_Normal", ResourceType = typeof(Resources.EnumResources))]
        Normal = 1,
        [Display(Name = "WorkHoursType_Extra", ResourceType = typeof(Resources.EnumResources))]
        Extra = 2,
    }

}
