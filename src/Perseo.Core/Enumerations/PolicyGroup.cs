﻿using System.ComponentModel.DataAnnotations;

namespace Perseo.Core
{
    public enum PolicyGroup
    {
        [Display(Name = "PolicyGroup_Administration", ResourceType = typeof(Resources.EnumResources))]
        Administration = 1,
        [Display(Name = "PolicyGroup_TopManagement", ResourceType = typeof(Resources.EnumResources))]
        TopManagement = 2,
        [Display(Name = "PolicyGroup_UpperManagement", ResourceType = typeof(Resources.EnumResources))]
        UpperManagement = 3,
        [Display(Name = "PolicyGroup_MiddleManagement", ResourceType = typeof(Resources.EnumResources))]
        MiddleManagement = 4,
        [Display(Name = "PolicyGroup_LowerManagement", ResourceType = typeof(Resources.EnumResources))]
        LowerManagement = 5,
        [Display(Name = "PolicyGroup_SupportTeam", ResourceType = typeof(Resources.EnumResources))]
        SupportTeam = 6,
        [Display(Name = "PolicyGroup_Staff", ResourceType = typeof(Resources.EnumResources))]
        Staff = 7,      
    }

}
