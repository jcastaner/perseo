﻿using System.ComponentModel.DataAnnotations;
namespace Perseo.Core
{
    public enum MinuteCost
    {
        [Display(Name = "30")]
        Min30 = 1,
        [Display(Name = "60")]
        Min60 = 2,
        [Display(Name = "90")]
        Min90 = 3,
        [Display(Name = "120")]
        Min120 = 4,
        [Display(Name = "150")]
        Min150 = 5,
        [Display(Name = "180")]
        Min180 = 6,
        [Display(Name = "210")]
        Min210 = 7,
        [Display(Name = "240")]
        Min240 = 8,
    }
}
