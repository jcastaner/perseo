﻿using System.ComponentModel.DataAnnotations;

namespace Perseo.Core
{
    public enum RequestType
    {
        [Display(Name = "RequestType_Hollydays", ResourceType = typeof(Resources.EnumResources))]
        Hollydays = 1,
        [Display(Name = "RequestType_SpecialLeave", ResourceType = typeof(Resources.EnumResources))]
        SpecialLeave = 2,
        [Display(Name = "RequestType_CompensationHours", ResourceType = typeof(Resources.EnumResources))]
        CompensationHours = 3,
        [Display(Name = "RequestType_PayClaim", ResourceType = typeof(Resources.EnumResources))]
        PayClaim = 4,
        [Display(Name = "RequestType_ScheduleChange", ResourceType = typeof(Resources.EnumResources))]
        ScheduleChange = 5,
        [Display(Name = "RequestType_PositionChange", ResourceType = typeof(Resources.EnumResources))]
        PositionChange = 6,
        [Display(Name = "RequestType_Other", ResourceType = typeof(Resources.EnumResources))]
        Other = 99,
    }
}
