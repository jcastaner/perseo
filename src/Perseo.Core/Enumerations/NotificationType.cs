﻿using System.ComponentModel.DataAnnotations;

namespace Perseo.Core
{

    public enum NotificationType
    {
        [Display(Name = "NotificationType_alert", ResourceType = typeof(Resources.EnumResources))]
        alert = 1,
        [Display(Name = "NotificationType_task", ResourceType = typeof(Resources.EnumResources))]
        task = 2,
        [Display(Name = "NotificationType_message", ResourceType = typeof(Resources.EnumResources))]
        message = 3
    }

}
