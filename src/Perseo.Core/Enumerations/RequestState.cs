﻿using System.ComponentModel.DataAnnotations;

namespace Perseo.Core
{
    public enum RequestState
    {
        [Display(Name = "RequestState_Created", ResourceType = typeof(Resources.EnumResources))]
        Created = 1,
        [Display(Name = "RequestState_Pending", ResourceType = typeof(Resources.EnumResources))]
        Pending = 2,
        [Display(Name = "RequestState_InProgress", ResourceType = typeof(Resources.EnumResources))]
        InProgress = 3,
        [Display(Name = "RequestState_Returned", ResourceType = typeof(Resources.EnumResources))]
        Returned = 4,
        [Display(Name = "RequestState_Accepted", ResourceType = typeof(Resources.EnumResources))]
        Accepted = 5,
        [Display(Name = "RequestState_Rejected", ResourceType = typeof(Resources.EnumResources))]
        Rejected = 6,
        [Display(Name = "RequestState_Closed", ResourceType = typeof(Resources.EnumResources))]
        Closed = 7,
    }
}
