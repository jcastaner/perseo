﻿using System.ComponentModel.DataAnnotations;

namespace Perseo.Core
{
    public enum QuestionsType
    {
        [Display(Name = "QuestionsType_Simple", ResourceType = typeof(Resources.EnumResources))]
        Simple = 1,
        [Display(Name = "QuestionsType_Multiple", ResourceType = typeof(Resources.EnumResources))]
        Multiple = 2,
    }

}
