﻿using System.ComponentModel.DataAnnotations;

namespace Perseo.Core
{
    public enum ActiveType
    {
        [Display(Name = "ActiveType_Active", ResourceType = typeof(Resources.EnumResources))]
        Active = 1,
        [Display(Name = "ActiveType_Inactive", ResourceType = typeof(Resources.EnumResources))]
        Inactive = 2,
        [Display(Name = "ActiveType_All", ResourceType = typeof(Resources.EnumResources))]
        All = 3,

    }

    static class ActiveTypeExtension
    {
        public static bool? ToBool(this ActiveType? type)
        {
            switch (type)
            {
                case ActiveType.Active: return true;
                case null: return true;
                case ActiveType.Inactive: return false;
                default: return null;
            }
        }
    }



}
