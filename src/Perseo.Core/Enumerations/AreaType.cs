﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Perseo.Core
{
    public enum AreaType
    {
        [Display(Name = "AreaType_Organizational", ResourceType = typeof(Resources.EnumResources))]
        Organizational = 1,
        [Display(Name = "AreaType_Project", ResourceType = typeof(Resources.EnumResources))]
        Project = 2,
        [Display(Name = "AreaType_Productive", ResourceType = typeof(Resources.EnumResources))]
        Productive = 4,
        [Display(Name = "AreaType_General", ResourceType = typeof(Resources.EnumResources))]
        General = 8,
    }

}
