﻿using System.ComponentModel.DataAnnotations;

namespace Perseo.Core
{
    public enum AcademicDegree
    {
        [Display(Name = "AcademicDegree_Basic", ResourceType = typeof(Resources.EnumResources))]
        Basic = 1,
        [Display(Name = "AcademicDegree_Primary", ResourceType = typeof(Resources.EnumResources))]
        Primary = 2,
        [Display(Name = "AcademicDegree_LowerSecondary", ResourceType = typeof(Resources.EnumResources))]
        LowerSecondary = 3,
        [Display(Name = "AcademicDegree_UpperSecondary", ResourceType = typeof(Resources.EnumResources))]
        UpperSecondary = 4,
        [Display(Name = "AcademicDegree_HigherDiploma", ResourceType = typeof(Resources.EnumResources))]
        HigherDiploma = 5,
        [Display(Name = "AcademicDegree_Bachellor", ResourceType = typeof(Resources.EnumResources))]
        Bachellor = 6,
        [Display(Name = "AcademicDegree_Master", ResourceType = typeof(Resources.EnumResources))]
        Master = 7,
        [Display(Name = "AcademicDegree_Doctor", ResourceType = typeof(Resources.EnumResources))]
        Doctor = 8,
    }

}
