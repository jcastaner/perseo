﻿using System.ComponentModel.DataAnnotations;

namespace Perseo.Core
{
    public enum RoleType
    {
        [Display(Name = "RoleType_Admin", ResourceType = typeof(Resources.EnumResources))]
        Admin = 1,
        [Display(Name = "RoleType_SuperUser", ResourceType = typeof(Resources.EnumResources))]
        SuperUser = 2,
        [Display(Name = "RoleType_Director", ResourceType = typeof(Resources.EnumResources))]
        Director = 3,
        [Display(Name = "RoleType_UpperManager", ResourceType = typeof(Resources.EnumResources))]
        UpperManager = 4,
        [Display(Name = "RoleType_MiddleManager", ResourceType = typeof(Resources.EnumResources))]
        MiddleManager = 5,
        [Display(Name = "RoleType_LowerManager", ResourceType = typeof(Resources.EnumResources))]
        LowerManager = 6,
        [Display(Name = "RoleType_TeamManger", ResourceType = typeof(Resources.EnumResources))]
        TeamManger = 7,
        [Display(Name = "RoleType_SupportStaff", ResourceType = typeof(Resources.EnumResources))]
        SupportStaff = 8,
        [Display(Name = "RoleType_ReportingStaff", ResourceType = typeof(Resources.EnumResources))]
        ReportingStaff = 9,
        [Display(Name = "RoleType_QualityStaff", ResourceType = typeof(Resources.EnumResources))]
        QualityStaff = 10,
        [Display(Name = "RoleType_TrainingStaff", ResourceType = typeof(Resources.EnumResources))]
        TrainingStaff = 11,
        [Display(Name = "RoleType_ItStaff", ResourceType = typeof(Resources.EnumResources))]
        ItStaff = 12,
        [Display(Name = "RoleType_Staff", ResourceType = typeof(Resources.EnumResources))]
        Staff = 13
    }

}
