﻿using System.ComponentModel.DataAnnotations;

namespace Perseo.Core
{
    public enum ContractPeriodType
    {
        [Display(Name = "ContractPeriodType_Permanent", ResourceType = typeof(Resources.EnumResources))]
        Permanent = 1,
        [Display(Name = "ContractPeriodType_Temporary", ResourceType = typeof(Resources.EnumResources))]
        Temporary = 2,
        [Display(Name = "ContractPeriodType_Service", ResourceType = typeof(Resources.EnumResources))]
        Service = 3,
        [Display(Name = "ContractPeriodType_External", ResourceType = typeof(Resources.EnumResources))]
        External = 4
    }
}
