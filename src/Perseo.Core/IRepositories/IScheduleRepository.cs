using System;
using System.Collections.Generic;  

namespace Perseo.Core
{
	public interface IScheduleRepository : IRepository<ScheduleDate>
    {

        IEnumerable<ScheduleDate> GetUserSchedule(int year, int userId);

        IEnumerable<UserDedication> GetUserDedications(int userId, int year, int? month = null);

        ScheduleDateTerm GetTerm(params object[] key);

        int AddTerm(ScheduleDateTerm entity);

        int AddTermRange(IEnumerable<ScheduleDateTerm> entityList);

        int UpdateTerm(ScheduleDateTerm entity);

        int UpdateTermRange(IEnumerable<ScheduleDateTerm> entityList);

        int RemoveTerm(ScheduleDateTerm entity);

        int RemoveTermRange(IEnumerable<ScheduleDateTerm> entityList);


        ScheduleDateTermJob GetTermJob(params object[] key);

        int AddTermJob(ScheduleDateTermJob entity);

        int AddTermJobRange(IEnumerable<ScheduleDateTermJob> entityList);

        int UpdateTermJob(ScheduleDateTermJob entity);

        int UpdateTermJobRange(IEnumerable<ScheduleDateTermJob> entityList);

        int RemoveTermJob(ScheduleDateTermJob entity);

        int RemoveTermJobRange(IEnumerable<ScheduleDateTermJob> entityList);


        IEnumerable<StaffingDayPattern> GetWorkPatterns(IList<int> jobsIds, int year, int month, int? week = null, int? day = null);
    }	
}

