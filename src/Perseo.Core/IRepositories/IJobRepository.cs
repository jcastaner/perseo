using System.Collections.Generic;  

namespace Perseo.Core
{
	public interface IJobRepository : IRepository<Job> 
    {
        IEnumerable<JobConfig> GetJobConfiguration(IList<int> jobsIds, int year, int month, int? week = null, int? day = null);

        IEnumerable<JobSla> GetJobSla(IList<int> jobsIds, int year, int month, int? week = null, int? day = null);

        IEnumerable<Job> GetJobsFromAreas(IEnumerable<Area> areas);

        IEnumerable<Job> GetGeneralJobs(int Year);
    }	
}

