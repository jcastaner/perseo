using System;
using System.Collections.Generic;  

namespace Perseo.Core
{
    public interface IAreaRepository : IRepository<Area>
    {
        IEnumerable<Area> GetAncestors(int Id);

        IEnumerable<Area> GetChildren(int Id);

        IEnumerable<Area> GetAllChildren(IEnumerable<int> Id);

        IEnumerable<AreaListNode> GetTree(int id);

        IEnumerable<Area> GetOwnedAreas(User user);

        IEnumerable<AreaListNode> GetOwnedAreaList(User user, bool? Active = null, List<AreaType> Types = null);

        IEnumerable<Area> GetAreasToSchedule(List<int> ids);

        PagedResult<AreaListNode> GetOwnedAreaListPaged(User user, bool? Active = null, List<AreaType> Types = null, int? page = null, int? pageSize = null, string Search = null, string OrderField = null, string OrderDirection = "asc");

    }
}

