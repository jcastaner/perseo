
using System.Collections.Generic;

namespace Perseo.Core
{
	public interface INotificationRepository : IRepository<Notification>
    {

        IEnumerable<Notification> GetAllFromUser(int id, bool active = true, NotificationType? type = null);

    }	
}

