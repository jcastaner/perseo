﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Perseo.Core
{
    public interface IUserRepository : IRepository<User>
    {
        User GetFull(int id);

        IEnumerable<User> GetUsersFromAreas(IEnumerable<int> AreaIds);

        IEnumerable<UserListNode> GetUserListFromAreas(IEnumerable<int> AreaIds, bool? Active = null);

        IEnumerable<User> GetAllUserSchedule(int year, int month, List<int> areasIds, bool? Active = null, List<int> officeFilterIds = null);

        User GetUserSchedule(int year, int userId);

        User GetAuthUser(int id);

        User GetAuthUser(string UserName);

        User GetWithAreas(int id);

        User GetWithAreasAndJobs(int id);

        User Get(string UserName);

        IEnumerable<User> GetAllReadOnly();

        // IDENTITY

        IEnumerable<User> All();

        User FindByNormalizedUserName(string normalizedUserName);

        User FindByNormalizedEmail(string normalizedEmail);

        void AddRole(int UserId, string roleName);

        void RemoveRole(int userId, string roleName);

        IEnumerable<string> GetRoleNamesByUserId(int userId);

        IEnumerable<User> GetUsersByRoleName(string roleName);

        bool ExistsUserName(string userName);

        bool ExistsEmail(string email);

        bool ExistsEmployeeId(int employeeId);

        IEnumerable<User> GetSubordinates(int UserId, bool Direct = false, bool Owner = false, bool Strict = false, bool? Active = true );
    }
}
