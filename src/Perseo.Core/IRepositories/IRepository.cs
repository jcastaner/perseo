﻿using System;
using System.Collections.Generic;  

namespace Perseo.Core
{
	public interface IRepository<T> where T : class
    {
        T Find(params object[] key);

        T Get(params object[] key);

        T GetOne(System.Linq.Expressions.Expression<Func<T, bool>> where, string include = null);

        IEnumerable<T> GetMany(System.Linq.Expressions.Expression<Func<T, bool>> where = null, string order = null, bool tracking = true);

        PagedResult<T> GetManyPaged(
           System.Linq.Expressions.Expression<Func<T, bool>> where = null,
           string order = null, int? page = null, int? pageSize = null);

        PagedResult<Q> QueryPaged<Q>(
            System.Linq.Expressions.Expression<Func<T, Q>> select,
            System.Linq.Expressions.Expression<Func<T, bool>> where = null,
            System.Linq.Expressions.Expression<Func<Q, bool>> filter = null,
            string order = null, int? page = null, int? pageSize = null);

        int Insert(T entity);

        int Add(T entity);

        int AddRange(IEnumerable<T> entityList);

        int Update(T entity);

        int UpdateRange(IEnumerable<T> entityList);

        int Delete(T entity);

        int Delete(params object[] key);

        int DeleteRange(IEnumerable<T> entityList);

        int DeleteRange(IEnumerable<int> idList);

        int Remove(T entity);

        int RemoveRange(IEnumerable<T> entityList);

        int SaveChanges();

        void Reload(T entity);

        void Attach(T entity);

        void LoadProperty(T entity, System.Linq.Expressions.Expression<Func<T, Object>> related);


    }
}
