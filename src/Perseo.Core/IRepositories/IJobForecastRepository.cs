using System.Collections.Generic;  

namespace Perseo.Core
{
	public interface IJobForecastRepository : IRepository<JobForecast> 
    {
        IEnumerable<JobForecast> GetJobForecast(IList<int> jobsIds, int year, int month, int? week = null, int? day = null);

        IEnumerable<dynamic> GetJobForecastList(IList<int> jobsIds);
    }	
}

