﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Perseo.Core
{
    public interface IRoleRepository : IRepository<Role>
    {
        Role FindByName(string roleName);

    }

}
