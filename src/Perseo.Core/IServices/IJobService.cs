using System.Collections.Generic;

namespace Perseo.Core
{
    public interface IJobService : IService<Job>
    {

        IEnumerable<Job> GetJobsFromAreas(IEnumerable<Area> areas);

    }

}

