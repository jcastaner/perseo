using System.Collections.Generic;

namespace Perseo.Core
{
    public interface IRequestService : IService<Request>
    {

        PagedResult<RequestListNode> GetRequestsFromAreasPaged(List<int> AreaIds, List<RequestType> Types = null, List<RequestState> States = null,
           int? year = null, int? month = null, int? page = null, int? pageSize = null, string Search = null, string OrderField = null, string OrderDirection = "asc");

        IEnumerable<RequestListNode> GetRequestsFromAreas(List<int> AreaIds, List<RequestType> Types = null, List<RequestState> States = null);

        IEnumerable<RequestListNode> GetRequestsCreatedByUser(int UserId, List<RequestType> Types = null, List<RequestState> States = null);

        IEnumerable<RequestListNode> GetRequestsAssignedToUser(int UserId, List<RequestType> Types = null, List<RequestState> States = null);

        IEnumerable<RequestHistoryListNode> GetRequestHistory(int id);
    }
}

