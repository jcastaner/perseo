using System.Collections.Generic;

namespace Perseo.Core
{
    public interface INotificationService : IService<Notification>
    {
        IEnumerable<Notification> GetAllFromUser(int id, bool active = true, NotificationType? type = null);

        bool Dismiss(Notification notification);
    }
}

