using System;
using System.Collections.Generic;

namespace Perseo.Core
{
    public interface IAreaService : IService<Area>
    {
        IEnumerable<Area> GetAncestors(int id);

        IEnumerable<Area> GetChildren(int id);

        IEnumerable<AreaListNode> GetTree(int id);

        IEnumerable<AreaListNode> GetOwnedAreaList(User user, bool? Active = null, List<AreaType> Types = null);

        IEnumerable<Area> GetAreasToSchedule(List<int> ids);

        PagedResult<AreaListNode> GetOwnedAreaListPaged(User user, bool? Active = null, List<AreaType> Types = null, int? page = null, int? pageSize = null, string Search = null, string OrderField = null, string OrderDirection = "asc");

        PagedResult<AreaListNode> ListPaged(List<int> areaFilterIds, ActiveType? active, List<AreaType> types = null, int? page = null, int? pageSize = null, string search = null, string order = null);
    }
}

