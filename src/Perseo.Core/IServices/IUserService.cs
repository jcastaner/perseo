using System;
using System.Collections.Generic;

namespace Perseo.Core
{
    public interface IUserService : IService<User>
    {

        User GetFull(int id);

        IEnumerable<User> GetUsersFromAreas(IEnumerable<int> AreaIds);

        IEnumerable<UserListNode> GetUserListFromAreas(IEnumerable<int> AreaIds, bool? Active = null);

        IEnumerable<User> GetAllUserSchedule(int year, int month, List<int> areasIds, bool? Active = null, List<int> officeFilterIds = null);

        User GetUserSchedule(int year, int userId);

        IEnumerable<User> GetAllReadOnly();

        User GetAuthUser(int id);

        User GetAuthUser(string userName);

        IEnumerable<Area> GetOwnedAreas(User user, DateTime? StartDate = null, DateTime? EndDate = null);

        IEnumerable<Job> GetOwnedJobs(User user, DateTime? StartDate = null, DateTime? EndDate = null);


        void AddRole(int UserId, string roleName);

        void RemoveRole(int userId, string roleName);

        User FindByNormalizedUserName(string normalizedUserName);

        User FindByNormalizedEmail(string normalizedEmail);

        IEnumerable<string> GetRoleNamesByUserId(int userId);

        IEnumerable<User> GetUsersByRoleName(string roleName);

        bool ExistsUserName(string userName);

        bool ExistsEmail(string email);

        bool ExistsEmployeeId(int employeeId);

        IEnumerable<User> GetSubordinates(int UserId, bool Direct = false, bool Owner = false);

        //***************************************************************

        PagedResult<UserListNode> ListPaged(List<int> areaFilterIds, ActiveType? active, int? page = null, int? pageSize = null, string search = null, string order = null, List<int> officeFilterIds = null);
    }
}

