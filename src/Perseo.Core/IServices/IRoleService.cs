using System.Collections.Generic;

namespace Perseo.Core
{
    public interface IRoleService : IService<Role>
    {
        IEnumerable<Role> All();

        Role FindByName(string normalizedRoleName);

    }


}

