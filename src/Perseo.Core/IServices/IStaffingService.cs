using System.Collections.Generic;
using System.IO;

namespace Perseo.Core
{
    public interface IStaffingService
    {
        double CalculateRequiredAgents(
           double wait_time,
           double service_level,
           double inbound_calls,
           double average_talk_time,
           double period_in_seconds = 1800,
           double max_queue_length = 999);

        double CalculateAverageWaitTime(
            double number_of_agents,
            double inbound_calls,
            double average_talk_time,
            double period_in_seconds = 1800,
            double max_queue_length = 999);

        double CalculateTimeOverSla(
            double number_of_agents,
            double wait_time,
            double inbound_calls,
            double average_talk_time,
            double period_in_seconds = 1800,
            double max_queue_length = 999);

        double CalculateServiceLevel(
            double number_of_agents,
            double wait_time,
            double inbound_calls,
            double average_talk_time,
            double period_in_seconds = 1800,
            double max_queue_length = 999);

        double CalculateOccupancy(
           double number_of_agents,
           double inbound_calls,
           double average_talk_time,
           double period_in_seconds = 1800);

        double CalculateAvailability(
            double number_of_agents,
            double inbound_calls,
            double average_talk_time,
            double period_in_seconds = 1800);

        IList<string> GetTimeSlots();

        IEnumerable<dynamic> GetJobForecastList(IList<int> jobsIds);

        IList<JobConfig> GetJobsConfiguration(IList<int> jobsIds, int year, int month);

        IDictionary<int, List<double>> GetActualAgents(IList<int> jobsIds, int year, int month);

        IDictionary<int, List<double>> GetAvailableAgents(IList<int> jobsIds, int year, int month);

        IDictionary<int, List<double>> GetRequiredEvents(IList<int> jobsIds, int year, int month);

        IDictionary<int, List<double>> GetRequiredAgents(IList<int> jobsIds, int year, int month);

        IDictionary<int, List<double>> GetCapacity(IList<int> jobsIds, int year, int month, bool? full = true);

        IDictionary<int, Dictionary<int, List<double>>> GetCapacityByJob(IList<int> jobsIds, int year, int month, bool? full = true);

        void SaveForeacast(IDictionary<int, List<double>> forecast, int jobId, int year, int month);

        void LoadForeacast(FileStream file, int sheetNumber, int jobId, int year, int month);

        IDictionary<int, List<double>> GetRequiredEventsFromFile(FileStream file, int sheetNumber, int jobId, int year, int month);

        IDictionary<int, List<double>> GetRequiredEventsMonthPattern(
            int Year,
            int Month,
            List<double> WeekDistribution = null,
            List<double> DayDistribution = null);

        List<double> GetRequiredEventsWeekPattern(
            int StartDay = 0,
            int EndDay = 6,
            List<double> WeekDistribution = null);

        List<double> GetRequiredEventsDayPattern(
            int StartTimeRange = 0,
            int EndTimeRange = 47,
            List<double> DayDistribution = null);

        (int start, int end) GetRealTimeSlots(IDictionary<int, List<double>> data);
    }
}

