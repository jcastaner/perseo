using Perseo.Core.Resources;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Resources;

namespace Microsoft.AspNetCore.Mvc
{
    public static class EnumExtensions
    {
        public static string GetDisplayName(this Enum e)
        {
            if (e != null)
            {
                try
                {
                    string resourceDisplayName = new ResourceManager(typeof(EnumResources)).GetString(e.GetType().Name + "_" + e);
                    return !string.IsNullOrWhiteSpace(resourceDisplayName) ? resourceDisplayName : e.GetType()?
                                                                                                    .GetMember(e.ToString())?
                                                                                                    .FirstOrDefault()?
                                                                                                    .GetCustomAttribute<DisplayAttribute>()?
                                                                                                    .Name;
                }
                catch (Exception)
                {
                    return e.GetType().Name;
                }
            }
            return "";
        }

        //public static string GetDisplayName(this Enum enumValue)
        //{
        //    if (enumValue != null)
        //    {
        //        return enumValue.GetType()?
        //                    .GetMember(enumValue.ToString())?
        //                    .FirstOrDefault()?
        //                    .GetCustomAttribute<DisplayAttribute>()?
        //                    .Name;
        //    }
        //    return "";
        //}



    }
}
