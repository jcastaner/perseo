using System;
 
namespace Perseo.Core
{
    //Definition of primary variables:
    //inbound_calls - (incoming calls: default value is calls/period defined)
    //average_talk_time - (Average talk time in seconds)
    //period_in_seconds - (Length of period in seconds ( 3600 = hour, 1800 = 30 min, 1200 = 20 min, etc.)
    //max_queue_length - set at 999, but should be the max queue length
    //silent - supress all error messages

    public static class ErlangC
    {

        //Function to get the probability ratio of a delay given the number of agents specified
        private static double CalcDelay( 
            double number_of_agents,
            double inbound_calls,
            double average_talk_time,
            double max_queue_length = 999)
        {
            //More calls than can possibly be handled
            if (inbound_calls / average_talk_time >= number_of_agents)
            {
                return 1;
            }
            //Load Defaults
            double total_load = 0;
            double temp_load = 1;
            //We need the maximum number of customers possible
            double max_queue = number_of_agents + max_queue_length;
            //calculate the load based on the number of agents
            for (int x = 1; x < number_of_agents; x++)
            {
                //Load gets smaller as agents increase
                temp_load = temp_load * inbound_calls / (x * average_talk_time);
                total_load += temp_load;
            }
            //Add a buffer to the total Load
            double agent_load = total_load + 1;
            //calculate load for waiting clients which cannot be handled by agents
            for (int x = Convert.ToInt32(number_of_agents); x <= max_queue; x++)
            {
                temp_load = temp_load * inbound_calls / (number_of_agents * average_talk_time);
                if (temp_load <= 0)
                {
                    break;
                }
                total_load += temp_load;
            }
            //Add another buffer to the total load
            total_load++;
            //get the load ratio
            double ratio = 1 / total_load;
            //return the value
            return 1 - ratio * agent_load;
        }

        //Calcualte the service level based on the number of agents and wait time in seconds
        private static double CalcServiceLevel(
            double number_of_agents, 
            double wait_time,
            double inbound_calls,
            double average_talk_time,
            double max_queue_length = 999
            )
        {
            //Return numeric service Level between 1 and 0;
            return Math.Max(0, 1.0 - (CalcDelay(number_of_agents, inbound_calls, average_talk_time, max_queue_length) 
                / Math.Exp((number_of_agents * average_talk_time - inbound_calls) * wait_time))
                );
        }

        //Calcualte the service level based on the number of agents and wait time in seconds
        public static double CalculateServiceLevel(
            double number_of_agents,
            double wait_time,
            double inbound_calls,
            double average_talk_time,
            double period_in_seconds = 1800,
            double max_queue_length = 999
            )
        {
            inbound_calls = inbound_calls / period_in_seconds;
            average_talk_time = period_in_seconds / average_talk_time / period_in_seconds;

            //Return numeric service Level between 1 and 0;
            return Math.Max(0, 1.0 - (CalcDelay(number_of_agents, inbound_calls, average_talk_time, max_queue_length)
                / Math.Exp((number_of_agents * average_talk_time - inbound_calls) * wait_time))
                );
        }

        //calculate the required agents based on wait time in seconds and service level ratio
        public static double CalculateRequiredAgents(
            double wait_time, 
            double service_level,
            double inbound_calls,
            double average_talk_time,
            double period_in_seconds = 1800,
            double max_queue_length = 999)
        {
            if (inbound_calls == 2) Console.WriteLine("*************"+ wait_time + " " + service_level + " " + average_talk_time);
            inbound_calls = inbound_calls / period_in_seconds;
            average_talk_time = period_in_seconds / average_talk_time / period_in_seconds;

            //We need a service level between 1 and 0
            if (service_level < 0 || service_level > 1)
            {
                return 999999999999;
            }
            //If there are no calls
            if (inbound_calls <= 0)
            {
                return 0;
            }
            //If there is no service the number is HUGE
            if (average_talk_time == 0)
            {
                return 999999999999;
            }
            
            //Check to see if it can be handled by 1 agent
            if (CalcServiceLevel(1, wait_time, 
                inbound_calls, average_talk_time, max_queue_length) >= service_level)
            {
                return 1;
            }
            //Calculcate the Load on the agents
            double load = inbound_calls / average_talk_time;
            //Perform a binary search on the most-convenient ratio of service level to agents
            double value_1 = Math.Floor(load);
            double value_2 = Math.Ceiling(load + Math.Sqrt(load));
            while (CalcServiceLevel(value_2, wait_time, 
                inbound_calls, average_talk_time, max_queue_length) < service_level)
            {
                value_1 = value_2;
                value_2 = Math.Max(value_2 + 1, value_2 + Math.Sqrt(load));
            }
            double value = 1;
            while (value_2 - value_1 > 1)
            {
                double val = (value_1 + value_2) / 2;
                value = Math.Round(val);
                //check the service level of the current value
                double check_service_level = CalcServiceLevel(value, wait_time,
                    inbound_calls, average_talk_time, max_queue_length);
                //If the service level of val is better than the minimum requirement, we use it
                if (check_service_level >= service_level)
                {
                    value_2 = value;
                }
                else
                {
                    value_1 = value;
                }
            }
            return value_2;
            //return Math.Floor(value_2);
        }

        //Calculate the average wait time based on the number of agents
        public static double CalculateAverageWaitTime(
            double number_of_agents,
            double inbound_calls,
            double average_talk_time,
            double period_in_seconds = 1800,
            double max_queue_length = 999)
        {
            inbound_calls = inbound_calls / period_in_seconds;
            average_talk_time = period_in_seconds / average_talk_time / period_in_seconds;

            //It is impossible for this number of agents to process the incoming calls
            if (inbound_calls >= average_talk_time * number_of_agents)
            {
                return 9999999999;
            }
            //return the Average Wait time
            return CalcDelay(number_of_agents, inbound_calls, average_talk_time,
                max_queue_length) / (average_talk_time * number_of_agents - inbound_calls);
        }

        //Calculate the excess time, i.e. the average amount of time over the SLA wait limit
        public static double CalculateTimeOverSla(
            double number_of_agents, 
            double wait_time,
            double inbound_calls,
            double average_talk_time,
            double period_in_seconds = 1800,
            double max_queue_length = 999)
        {
            inbound_calls = inbound_calls / period_in_seconds;
            average_talk_time = period_in_seconds / average_talk_time / period_in_seconds;
            //It is impossible for this number of agents to process the incoming calls
            if (inbound_calls >= average_talk_time * number_of_agents)
            {
                return 9999999999;
            }
            //Return the Excess wait time
            return CalcDelay(number_of_agents, inbound_calls, average_talk_time,
                max_queue_length) / Math.Exp(average_talk_time * number_of_agents * wait_time - inbound_calls * wait_time) 
                / (average_talk_time * number_of_agents - inbound_calls);
        }

        public static double CalculateOccupancy(
            double number_of_agents, 
            double inbound_calls,
            double average_talk_time,
            double period_in_seconds = 1800)
        {
            return (period_in_seconds * average_talk_time)/(number_of_agents * period_in_seconds);
        }

        public static double CalculateAvailability(
            double number_of_agents,
            double inbound_calls,
            double average_talk_time,
            double period_in_seconds = 1800)
        {
            return 1 - (period_in_seconds * average_talk_time) / (number_of_agents * period_in_seconds);
        }

    }
}