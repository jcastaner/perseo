﻿using System;
using System.Collections.Generic;

namespace Perseo.Core
{ 
    public abstract class PagedResultBase
    {

        public const int DEFAULTPAGESIZE = 10;
        public const int MINFORPAGINATION = 100;

        public int CurrentPage { get; set; }
        public int PageCount { get; set; }
        public int PageSize { get; set; }
        public int RowCount { get; set; }
        public int FilteredRowCount { get; set; }

        public int FirstRowOnPage
        {
            get { return (CurrentPage - 1) * PageSize + 1; }
        }

        public int LastRowOnPage
        {
            get { return Math.Min(CurrentPage * PageSize, RowCount); }
        }

    }

    public class PagedResult<T> : PagedResultBase 
    {
        public readonly int MinForPagination = MINFORPAGINATION;
        public readonly int DefaultPageSize = DEFAULTPAGESIZE;

        public IEnumerable<T> Results { get; set; }

        public PagedResult()
        {
            Results = new HashSet<T>();
        }

        public bool ServerSidePagination
        {
            get { return RowCount > MinForPagination; }
        }
    }
}
