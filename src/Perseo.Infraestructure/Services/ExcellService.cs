using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using NPOI.XSSF.UserModel;
using Perseo.Core;

namespace Perseo.Infraestructure
{

    public class ExcellService : IExcellService
    {

        public DataTable ReadSheet(FileStream file, int sheetNumber)
        {
            IWorkbook workbook = WorkbookFactory.Create(file);
            //var workbook = new HSSFWorkbook(file);
            ISheet sheet = workbook.GetSheetAt(sheetNumber);
            //ISheet sheet = book.GetSheet("Hoja1");
            DataTable dt = new DataTable(sheet.SheetName);
            IRow headerRow = sheet.GetRow(0);
            int colCount = headerRow.LastCellNum;
            int rowCount = sheet.LastRowNum;
            foreach (ICell headerCell in headerRow)
            {
                dt.Columns.Add(headerCell.ToString());
            }
            int rowIndex = 0;
            foreach (IRow row in sheet)
            {
                if (rowIndex++ == 0) continue;
                DataRow dataRow = dt.NewRow();
                dataRow.ItemArray = row.Cells.Select(c => c.ToString()).ToArray();
                dt.Rows.Add(dataRow);
            }
            return dt;
            /*
            for (int row = 0; row <= rowCount; row++)
            {
                if (sheet.GetRow(row) != null) //null is when the row only contains empty cells 
                {
                    //sheet.GetRow(row).GetCell(0).StringCellValue);
                }
            }
            */
        }

        public Stream Create(DataTable SourceTable)
        {
            XSSFWorkbook workbook = null;
            NpoiMemoryStream ms = null;
            ISheet sheet = null;
            XSSFRow headerRow = null;
            try
            {
                workbook = new XSSFWorkbook();
                ms = new NpoiMemoryStream();
                sheet = workbook.CreateSheet();
                headerRow = (XSSFRow)sheet.CreateRow(0);

                var font = workbook.CreateFont();
                font.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.Bold;

                foreach (DataColumn column in SourceTable.Columns)
                {
                    var cell = headerRow.CreateCell(column.Ordinal);
                    cell.SetCellValue(column.ColumnName);
                    cell.CellStyle = workbook.CreateCellStyle();
                    cell.CellStyle.SetFont(font);
                }
                int rowIndex = 1;

                IDataFormat dataFormatCustom = workbook.CreateDataFormat();

                foreach (DataRow row in SourceTable.Rows)
                {
                    XSSFRow dataRow = (XSSFRow)sheet.CreateRow(rowIndex);
                    foreach (DataColumn column in SourceTable.Columns)
                    {
                        var cell = dataRow.CreateCell(column.Ordinal);
                        var value = row[column];

                        if (value is DateTime)
                        {
                            cell.CellStyle = workbook.CreateCellStyle();
                            var dt = (DateTime)value;
                            if (dt.TimeOfDay.TotalSeconds == 0)
                            {
                                cell.CellStyle.DataFormat = dataFormatCustom.GetFormat("dd/MM/yyyy");
                            }
                            else if (dt.Date.DayOfYear == 0)
                            {
                                cell.CellStyle.DataFormat = dataFormatCustom.GetFormat("HH:mm:ss");
                            }
                            else
                            {
                                cell.CellStyle.DataFormat = dataFormatCustom.GetFormat("dd/MM/yyyy hh:mm:ss");
                            }
                            cell.SetCellType(CellType.Numeric);
                            cell.SetCellValue(dt);
                        }
                        else if (value is sbyte|| value is byte || value is short || value is ushort  || value is int
                        || value is uint || value is long || value is ulong || value is float || value is double  
                        || value is decimal)
                        {
                            cell.SetCellType(CellType.Numeric);
                            cell.SetCellValue(Convert.ToDouble(value)); 
                    }
                        else if (value is string)
                        {
                            cell.SetCellType(CellType.String);
                            cell.SetCellValue(value.ToString());
                        }
                        else
                        {
                            cell.SetCellValue(value.ToString());
                        }
                    }
                    ++rowIndex;
                }
                for (int i = 0; i <= SourceTable.Columns.Count; ++i)
                    sheet.AutoSizeColumn(i);
                ms.AllowClose = false;
                workbook.Write(ms);
                ms.Flush();
                ms.Seek(0, SeekOrigin.Begin);
                ms.AllowClose = true;
            }
            catch
            {
                return null;
            }
            finally
            {
                sheet = null;
                headerRow = null;
                workbook = null;
            }
            return ms;
        }

        public Stream Create(IEnumerable<object> SourceData, IDictionary<string,string> Fields = null)
        {
            XSSFWorkbook workbook = null;
            NpoiMemoryStream ms = null;
            ISheet sheet = null;
            XSSFRow headerRow = null;
            List<string> properties = null;
            List<string> headerFields = null;

            if (Fields != null)
            {
                properties = Fields.Keys.ToList();
                headerFields = Fields.Values.ToList();
            }
            else
            {
                properties = SourceData.First().GetType().GetProperties()
                .Where(x => x.GetAccessors().Any(a => !a.IsVirtual))
                .Select(p => p.Name).ToList();
                headerFields = properties;
            }

            workbook = new XSSFWorkbook();
            ms = new NpoiMemoryStream();
            sheet = workbook.CreateSheet();
            headerRow = (XSSFRow)sheet.CreateRow(0);

            var font = workbook.CreateFont();
            font.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.Bold;

            int hederColIndex = 0;
            foreach (var column in headerFields)
            {
                var cell = headerRow.CreateCell(hederColIndex);
                cell.SetCellValue(column);
                cell.CellStyle = workbook.CreateCellStyle();
                cell.CellStyle.SetFont(font);
                ++hederColIndex;
            }

            int rowIndex = 1;

            IDataFormat dataFormatCustom = workbook.CreateDataFormat();

            foreach (var row in SourceData)
            {
                XSSFRow dataRow = (XSSFRow)sheet.CreateRow(rowIndex);
                int colIndex = 0;
                foreach (var column in properties)
                {
                    var cell = dataRow.CreateCell(colIndex);
                    var value = row.GetType().GetProperty(column).GetValue(row, null);

                    if (value is DateTime)
                    {
                        cell.CellStyle = workbook.CreateCellStyle();
                        var dt = (DateTime)value;
                        if (dt.TimeOfDay.TotalSeconds == 0)
                        {
                            cell.CellStyle.DataFormat = dataFormatCustom.GetFormat("dd/MM/yyyy");
                        }
                        else if (dt.Date.DayOfYear == 0)
                        {
                            cell.CellStyle.DataFormat = dataFormatCustom.GetFormat("HH:mm:ss");
                        }
                        else
                        {
                            cell.CellStyle.DataFormat = dataFormatCustom.GetFormat("dd/MM/yyyy hh:mm:ss");
                        }
                        cell.SetCellType(CellType.Numeric);
                        cell.SetCellValue(dt);
                    }
                    else if (value is sbyte || value is byte || value is short || value is ushort || value is int
                    || value is uint || value is long || value is ulong || value is float || value is double
                    || value is decimal)
                    {
                        cell.SetCellType(CellType.Numeric);
                        cell.SetCellValue(Convert.ToDouble(value));
                    }
                    else if (value is string)
                    {
                        cell.SetCellType(CellType.String);
                        cell.SetCellValue(value.ToString());
                    }
                    else if (value is null)
                    {
                        cell.SetCellType(CellType.Blank);
                    }
                    else
                    {
                        cell.SetCellValue(value.ToString());
                    }
                    ++colIndex;
                }
                ++rowIndex;
            }
            for (int i = 0; i < properties.Count; ++i) sheet.AutoSizeColumn(i);
            ms.AllowClose = false;
            workbook.Write(ms);
            ms.Flush();
            ms.Seek(0, SeekOrigin.Begin);
            ms.AllowClose = true;

            return ms;
        }

    }

    public class NpoiMemoryStream : MemoryStream
    {
        public NpoiMemoryStream()
        {
            // We always want to close streams by default to
            // force the developer to make the conscious decision
            // to disable it.  Then, they're more apt to remember
            // to re-enable it.  The last thing you want is to
            // enable memory leaks by default.  ;-)
            AllowClose = true;
        }

        public bool AllowClose { get; set; }

        public override void Close()
        {
            if (AllowClose)
                base.Close();
        }
    }


}