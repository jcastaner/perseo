﻿using System;
using MimeKit;
using MailKit.Security;
using MailKit.Net.Smtp;
using Microsoft.Extensions.Options;
using System.Threading.Tasks;
using Perseo.Core;

namespace Perseo.Infraestructure
{

    public class EmailService : IEmailService
    {
        //private readonly IViewRenderService ViewRenderService;

        public EmailSettings _emailSettings { get; }

        public EmailService(IOptions<EmailSettings> emailSettings)
        {
            _emailSettings = emailSettings.Value;
            //ViewRenderService = viewRenderService;
        }

        //public async Task SendHtmlAsync(string to, string subject, string templateView, object templateModel, int retryCount = 2)
        //{
        //    var mailBodyTask = ViewRenderService.RenderToStringAsync(templateView, new object());
        //    var body = await mailBodyTask;
        //    await SendAsync(to, subject, "", body);
        //}

        public async Task SendAsync(string to, string subject, string bodyText = null, string bodyHtml = null, int retryCount = 2)
        {
            var message = new MimeMessage();
            message.From.Add(new MailboxAddress(_emailSettings.Name, _emailSettings.UsernameEmail));
            message.To.Add(new MailboxAddress(to, to));
            message.Subject = subject;
            var builder = new BodyBuilder();
            if (bodyHtml != null){  builder.HtmlBody = bodyHtml;
            } else {   builder.TextBody = bodyText;  }
            message.Body = builder.ToMessageBody();
        
            for (var count = 1; count <= retryCount; count++)
            {
                try
                {
                    using (var client = new SmtpClient())
                    {
                        await client.ConnectAsync(_emailSettings.PrimaryDomain, _emailSettings.PrimaryPort, SecureSocketOptions.StartTlsWhenAvailable).ConfigureAwait(false);
                        await client.AuthenticateAsync(_emailSettings.Username, _emailSettings.UsernamePassword);
                        await client.SendAsync(message).ConfigureAwait(false);
                        await client.DisconnectAsync(true).ConfigureAwait(false);
                        return;
                    }
                }
                catch (Exception exception)
                {
                    if (retryCount >= 0)
                    {
                        throw exception;
                    }
                    await Task.Delay(count * 1000);
                }
            }
        }


        public void Send(string to, string subject, string bodyText = null, string bodyHtml = null, int retryCount = 2)
        {
            var message = new MimeMessage();
            message.From.Add(new MailboxAddress(_emailSettings.Name, _emailSettings.UsernameEmail));
            message.To.Add(new MailboxAddress(to, to));
            message.Subject = subject;
            var builder = new BodyBuilder();
            if (bodyHtml != null)
            {
                builder.HtmlBody = bodyHtml;
            }
            else { builder.TextBody = bodyText; }
            message.Body = builder.ToMessageBody();
            for (var count = 1; count <= retryCount; count++)
            {
                try
                {
                    using (var client = new SmtpClient())
                    {
                        client.Connect(_emailSettings.PrimaryDomain, _emailSettings.PrimaryPort, SecureSocketOptions.StartTlsWhenAvailable);
                        client.Authenticate(_emailSettings.Username, _emailSettings.UsernamePassword);
                        client.Send(message);
                        client.Disconnect(true);
                        return;
                    }
                }
                catch (Exception exception)
                {
                    if (retryCount >= 0)
                    {
                        throw exception;
                    }
                    Task.Delay(1000).Wait();
                }
            }
        }
    }

    public class EmailSettings
    {
        public string PrimaryDomain { get; set; }
        public int PrimaryPort { get; set; }
        public string SecondaryDomain { get; set; }
        public int SecondaryPort { get; set; }
        public string Name { get; set; }
        public string Username { get; set; }
        public string UsernameEmail { get; set; }
        public string UsernamePassword { get; set; }
        public string FromEmail { get; set; }
        public string ToEmail { get; set; }
        public string CcEmail { get; set; }

    }

}


/*
// In order to reference selfie.jpg from the html text, we'll need to add it
// to builder.LinkedResources and then use its Content-Id value in the img src.
var image = builder.LinkedResources.Add(@"C:\Users\Joey\Documents\Selfies\selfie.jpg");
image.ContentId = MimeUtils.GenerateMessageId();

// Set the html version of the message text
builder.HtmlBody = string.Format(@"<p>Hey Alice,<br>
<p>What are you up to this weekend? Monica is throwing one of her parties on
Saturday and I was hoping you could make it.<br>
<p>Will you be my +1?<br>
<p>-- Joey<br>
<center><img src=""cid:{0}""></center>", image.ContentId);

// We may also want to attach a calendar event for Monica's party...
builder.Attachments.Add(@"C:\Users\Joey\Documents\party.ics");
 */
