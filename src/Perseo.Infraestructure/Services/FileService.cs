﻿using Perseo.Core;
using Microsoft.Extensions.FileProviders;
using System;
using System.Drawing;
using System.IO;

namespace Perseo.Infraestructure
{
    public class FileService : IFileService
    {
        private readonly string RootPath;
        readonly PhysicalFileProvider Provider;

        public FileService(string rootPath)
        {
            RootPath = rootPath;
            Provider = new PhysicalFileProvider(RootPath);
        }

        public string GetRootPath()
        {
            return RootPath;
        }

        public Stream OpenFile(string FileName)
        {
            var filename = Path.Combine(RootPath, FileName);
            if (File.Exists(filename))
            {
                return File.OpenRead(filename);
            }
            return null;
        }

        public MemoryStream LoadFile(string FileName)
        {
            MemoryStream data = new MemoryStream();
            var filename = Path.Combine(RootPath, FileName);
            if (File.Exists(filename))
            {
                File.OpenRead(filename).CopyTo(data);
                data.Seek(0, SeekOrigin.Begin);
                return data;
            }
            return null;
        }

        public byte[] ReadFile(string FileName)
        {
            MemoryStream data = new MemoryStream();
            var filename = Path.Combine(RootPath, FileName);
            if (File.Exists(filename))
            {
                File.OpenRead(filename).CopyTo(data);
                data.Seek(0, SeekOrigin.Begin);
                byte[] buf = new byte[data.Length];
                data.Read(buf, 0, buf.Length);
                return buf;
            }
            return null;
        }

        public void SaveFile(Stream Stream, string FileName)
        {
            using (var Target = new FileStream(Path.Combine(RootPath, FileName), FileMode.Create))
            {
                Stream.Seek(0, SeekOrigin.Begin);
                Stream.CopyTo(Target);
            }
        }

        public void SaveFile(FileStream FileStream, string FileName)
        {
            using (var Target = new FileStream(Path.Combine(RootPath, FileName), FileMode.Create))
            {
                FileStream.Seek(0, SeekOrigin.Begin);
                FileStream.CopyTo(Target);
            }
        }

        public void SaveFileAsync(Stream Stream, string FileName)
        {
            using (var Target = new FileStream(Path.Combine(RootPath, FileName), FileMode.Create))
            {
                Stream.Seek(0, SeekOrigin.Begin);
                Stream.CopyToAsync(Target);
            }
        }

        public void SaveFileAsync(FileStream FileStream, string FileName)
        {
            using (var Target = new FileStream(Path.Combine(RootPath, FileName), FileMode.Create))
            {
                FileStream.Seek(0, SeekOrigin.Begin);
                FileStream.CopyToAsync(Target);
            }
        }

        public void DeleteFile(string FileName)
        {
            var filename = Path.Combine(RootPath, FileName);
            if (File.Exists(filename))
            {
                File.Delete(filename);
            }
        }

        public void SaveBase64Image(string Base64Image, string FileName)
        {
            File.WriteAllBytes(Path.Combine(RootPath, FileName), Convert.FromBase64String(Base64Image));
        }

        public void SaveBase64ImageAsync(string Base64Image, string FileName)
        {
            File.WriteAllBytesAsync(Path.Combine(RootPath, FileName), Convert.FromBase64String(Base64Image));
        }

        public bool ExistFile(string FileName)
        {
            return File.Exists(Path.Combine(RootPath, FileName));
        }

        public Image Base64ToImage(string base64String)
        {
            byte[] imageBytes = Convert.FromBase64String(base64String);
            using (var ms = new MemoryStream(imageBytes, 0, imageBytes.Length))
            {
                Image image = Image.FromStream(ms, true);
                return image;
            }
        }

        public string ImageToBase64(Image image, System.Drawing.Imaging.ImageFormat format)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                image.Save(ms, format);
                byte[] imageBytes = ms.ToArray();
                string base64String = Convert.ToBase64String(imageBytes);
                return base64String;
            }
        }

    }
}
