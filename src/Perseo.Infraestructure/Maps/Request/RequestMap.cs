using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Perseo.Core;
 
namespace Perseo.Infraestructure
{
    public class RequestMap
    {
        public RequestMap(EntityTypeBuilder<Request> entityBuilder)
        {
            entityBuilder.Property(t => t.Type).HasConversion<string>();
            entityBuilder.HasOne(u => u.User).WithMany().HasForeignKey(x => x.UserId);
            entityBuilder.HasOne(u => u.AssignedUser).WithMany().HasForeignKey(x => x.AssignedUserId);
            entityBuilder.HasMany(u => u.RequestHistory).WithOne(r => r.Request);
        }
    }
}