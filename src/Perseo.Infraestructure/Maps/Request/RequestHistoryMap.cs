using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Perseo.Core;
 
namespace Perseo.Infraestructure
{
    public class RequestHistoryMap
    {
        public RequestHistoryMap(EntityTypeBuilder<RequestHistory> entityBuilder)
        {
            entityBuilder.Property(t => t.State).HasConversion<string>();
            entityBuilder.HasOne(u => u.Request).WithMany(r => r.RequestHistory).HasForeignKey(x => x.RequestId);
            entityBuilder.HasOne(u => u.User).WithMany().HasForeignKey(x => x.UserId);
        }
    }
}