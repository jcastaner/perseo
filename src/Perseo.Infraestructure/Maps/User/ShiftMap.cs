using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Perseo.Core;
 
namespace Perseo.Infraestructure
{
    public class ShiftMap
    {
        public ShiftMap(EntityTypeBuilder<Shift> entityBuilder)
        {
            entityBuilder.HasOne(u => u.UserShift).WithMany(s => s.Shift).HasForeignKey(x => x.UserShiftId);
        }
    }
}