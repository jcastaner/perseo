using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Perseo.Core;
 
namespace Perseo.Infraestructure
{
    public class CategoryMap
    {
        public CategoryMap(EntityTypeBuilder<Category> entityBuilder)
        {
            entityBuilder.Property(t => t.Name).IsRequired();
            entityBuilder.Property(t => t.Active).IsRequired();

            /*
            entityBuilder.HasData(
               new Category { Id = 1, Name = "Producción 1", Level = 1, Active = true },
               new Category { Id = 2, Name = "Producción 2", Level = 2, Active = true },
               new Category { Id = 3, Name = "Producción 3", Level = 3, Active = true },
               new Category { Id = 4, Name = "Producción 4", Level = 4, Active = true },
               new Category { Id = 5, Name = "Producción 5", Level = 5, Active = true },
               new Category { Id = 6, Name = "Producción 6", Level = 6, Active = true },
               new Category { Id = 7, Name = "Supervisor 1", Level = 7, Active = true },
               new Category { Id = 8, Name = "Supervisor 2", Level = 8, Active = true },
               new Category { Id = 9, Name = "Supervisor 3", Level = 9, Active = true },
               new Category { Id = 10, Name = "Gerente 1", Level = 10, Active = true },
               new Category { Id = 11, Name = "Gerente 2", Level = 11, Active = true },
               new Category { Id = 12, Name = "Gerente 3", Level = 12, Active = true },
               new Category { Id = 13, Name = "Director 1", Level = 13, Active = true },
               new Category { Id = 14, Name = "Director 2", Level = 14, Active = true },
               new Category { Id = 15, Name = "Director 3", Level = 15, Active = true }
           );*/

        }
    }
}