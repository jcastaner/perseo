using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Perseo.Core;
 
namespace Perseo.Infraestructure
{
    public class UserShiftMap
    {
        public UserShiftMap(EntityTypeBuilder<UserShift> entityBuilder)
        {
            entityBuilder.HasOne(u => u.User).WithMany(s => s.UserShift).HasForeignKey(x => x.UserId);
            entityBuilder.HasMany(u => u.Shift).WithOne(s => s.UserShift).HasForeignKey(x => x.UserShiftId);
        }
    }
}