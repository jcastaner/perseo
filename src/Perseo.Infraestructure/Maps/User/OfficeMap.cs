using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Perseo.Core;
 
namespace Perseo.Infraestructure
{
    public class OfficeMap
    {
        public OfficeMap(EntityTypeBuilder<Office> entityBuilder)
        {
            entityBuilder.Property(t => t.Name).IsRequired();
            entityBuilder.HasOne(t => t.Calendar).WithMany(x => x.Office).HasForeignKey(k => k.CalendarId);
            /*
            entityBuilder.HasData(
                new Office { Id = 1, Name = "Sant Joan Desp� (BCN)", Active = true },
                new Office { Id = 2, Name = "Alcal� (MAD)", Active = true },
                new Office { Id = 3, Name = "Bollullos (SEV)", Active = true },
                new Office { Id = 4, Name = "Cordovilla (PAM)", Active = true }
            );*/
        }
    }
}