using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Perseo.Core;
 
namespace Perseo.Infraestructure
{
    public class UserCollectiveCategoryMap
    {
        public UserCollectiveCategoryMap(EntityTypeBuilder<UserCollectiveCategory> entityBuilder)
        {
            entityBuilder.HasKey(p => new { p.Id, p.UserId });
            entityBuilder.HasOne(u => u.User).WithMany(s => s.UserCollectiveCategory).HasForeignKey(x => x.UserId);
        }
    }
}