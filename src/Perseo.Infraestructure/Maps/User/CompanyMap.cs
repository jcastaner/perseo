using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Perseo.Core;

namespace Perseo.Infraestructure
{
    public class CompanyMap
    {
        public CompanyMap(EntityTypeBuilder<Company> entityBuilder)
        {
            entityBuilder.HasKey(u => u.Id);
            entityBuilder.HasMany(u => u.UserContract).WithOne(s => s.Company).HasForeignKey(x => x.CompanyId);
        }
    }
}