using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Perseo.Core;

namespace Perseo.Infraestructure
{
    public class UserWorkConditionsMap
    {
        public UserWorkConditionsMap(EntityTypeBuilder<UserWorkConditions> entityBuilder)
        {
            entityBuilder.HasOne(u => u.User).WithMany(s => s.UserWorkConditions).HasForeignKey(x => x.UserId);
        }
    }
}