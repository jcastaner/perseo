using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Perseo.Core;
using Microsoft.EntityFrameworkCore;

namespace Perseo.Infraestructure
{
    public class UserRoleMap
    {
        public UserRoleMap(EntityTypeBuilder<UserRole> entityBuilder)
        { 
            
            entityBuilder.Property(t => t.RoleId);
            entityBuilder.Property(t => t.UserId);
            entityBuilder.HasIndex(t => t.RoleId);
            entityBuilder.HasIndex(t => t.UserId);
            entityBuilder.HasKey(t => new { t.UserId, t.RoleId });

            entityBuilder.HasOne( t => t.Role).WithMany(r => r.UserRole).HasForeignKey(a => a.RoleId).OnDelete(DeleteBehavior.Cascade);
            entityBuilder.HasOne( t => t.User).WithMany(r => r.UserRole).HasForeignKey(a => a.UserId).OnDelete(DeleteBehavior.Cascade);
           
            /*
            entityBuilder.HasData(
              new UserRole { UserId = 1, RoleId = 1 },
              new UserRole { UserId = 1, RoleId = 2 },
              new UserRole { UserId = 1, RoleId = 3 }, 
              new UserRole { UserId = 1, RoleId = 4 },
              new UserRole { UserId = 1, RoleId = 5 },
              new UserRole { UserId = 1, RoleId = 6 },
              new UserRole { UserId = 1, RoleId = 7 },
              new UserRole { UserId = 1, RoleId = 8 },
              new UserRole { UserId = 1, RoleId = 9 },
              new UserRole { UserId = 1, RoleId = 10 },
              new UserRole { UserId = 1, RoleId = 11 },
              new UserRole { UserId = 1, RoleId = 12 },
              new UserRole { UserId = 1, RoleId = 13 },
              new UserRole { UserId = 1, RoleId = 14 },
              new UserRole { UserId = 1, RoleId = 15 },
              new UserRole { UserId = 1, RoleId = 16 },
              new UserRole { UserId = 1, RoleId = 17 }
           
              new UserRole { UserId = 2, RoleId = 3 },
              new UserRole { UserId = 2, RoleId = 4 },
              new UserRole { UserId = 2, RoleId = 5 },
              new UserRole { UserId = 2, RoleId = 6 },
              new UserRole { UserId = 2, RoleId = 7 },
              new UserRole { UserId = 2, RoleId = 8 },
              new UserRole { UserId = 2, RoleId = 9 },
              new UserRole { UserId = 2, RoleId = 10 },
              new UserRole { UserId = 2, RoleId = 11 },
              new UserRole { UserId = 2, RoleId = 12 },
              new UserRole { UserId = 2, RoleId = 13 },
              new UserRole { UserId = 2, RoleId = 14 },
              new UserRole { UserId = 2, RoleId = 15 },
              new UserRole { UserId = 2, RoleId = 16 },
              new UserRole { UserId = 2, RoleId = 17 },

              new UserRole { UserId = 3, RoleId = 5 },
              new UserRole { UserId = 3, RoleId = 6 },
              new UserRole { UserId = 3, RoleId = 7 },
              new UserRole { UserId = 3, RoleId = 8 },
              new UserRole { UserId = 3, RoleId = 9 },
              new UserRole { UserId = 3, RoleId = 10 },
              new UserRole { UserId = 3, RoleId = 11 },
              new UserRole { UserId = 3, RoleId = 12 },
              new UserRole { UserId = 3, RoleId = 13 },
              new UserRole { UserId = 3, RoleId = 15 },
              new UserRole { UserId = 3, RoleId = 17 },

              new UserRole { UserId = 4, RoleId = 7 },
              new UserRole { UserId = 4, RoleId = 8 },
              new UserRole { UserId = 4, RoleId = 9 },
              new UserRole { UserId = 4, RoleId = 10 },
              new UserRole { UserId = 4, RoleId = 11 },
              new UserRole { UserId = 4, RoleId = 12 },
              new UserRole { UserId = 4, RoleId = 13 },
              new UserRole { UserId = 4, RoleId = 17 },


              new UserRole { UserId = 5, RoleId = 9 },
              new UserRole { UserId = 5, RoleId = 10 },
              new UserRole { UserId = 5, RoleId = 11 },
              new UserRole { UserId = 5, RoleId = 17 },

              new UserRole { UserId = 6, RoleId = 9 },
              new UserRole { UserId = 6, RoleId = 11 },
              new UserRole { UserId = 6, RoleId = 12 },
              new UserRole { UserId = 6, RoleId = 17 },

              new UserRole { UserId = 7, RoleId = 17 }
             
            );*/

        }
    }
}