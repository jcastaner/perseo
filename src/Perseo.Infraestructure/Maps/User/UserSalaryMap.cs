using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Perseo.Core;
 
namespace Perseo.Infraestructure
{
    public class UserSalaryMap
    {
        public UserSalaryMap(EntityTypeBuilder<UserSalary> entityBuilder)
        {
            entityBuilder.HasKey(p => new { p.Id, p.UserId });
            entityBuilder.HasOne(u => u.User).WithMany(s => s.UserSalary).HasForeignKey(x => x.UserId);
        }
    }
}