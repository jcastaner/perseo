using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Perseo.Core;
using Microsoft.EntityFrameworkCore;
using System;

namespace Perseo.Infraestructure
{
    public partial class UserMap
    {
        public UserMap(EntityTypeBuilder<User> entityBuilder)
        {
            entityBuilder.Property(u => u.FirstName); 
            entityBuilder.Property(u => u.MainSurname);
            entityBuilder.Property(u => u.SecondSurname);
            entityBuilder.Property(p => p.NetworkUser);
            entityBuilder.Property(u => u.Flag).HasDefaultValue(true);
            entityBuilder.Property(u => u.Alerts).HasDefaultValue(0);
            entityBuilder.Property(u => u.Messages).HasDefaultValue(0);
            entityBuilder.Property(u => u.Tasks).HasDefaultValue(0);
            entityBuilder.Property(t => t.AcademicDegree).HasConversion<string>();

            entityBuilder.Ignore(u => u.FullName);
            entityBuilder.Ignore(u => u.FullFamilyName);
            entityBuilder.Ignore(u => u.OwnedAreas);
            entityBuilder.Ignore(u => u.OwnedJobs);
            entityBuilder.Ignore(u => u.Roles);
            entityBuilder.Ignore(u => u.LastUserOffice);
            entityBuilder.Ignore(u => u.LastUserPosition);
            entityBuilder.Ignore(u => u.LastUserRate);
            entityBuilder.Ignore(u => u.LastUserSalary);
            entityBuilder.Ignore(u => u.LastUserCollective);
            entityBuilder.Ignore(u => u.LastUserCategory);
            entityBuilder.Ignore(u => u.LastUserShift);
            entityBuilder.Ignore(u => u.LastUserArea);
            entityBuilder.Ignore(u => u.LastUserContract);
            entityBuilder.Ignore(u => u.LastUserWorkConditions);
            entityBuilder.Ignore(u => u.OwnedUserAreas);
            entityBuilder.Ignore(u => u.Supervisor);


            entityBuilder.HasMany(p => p.UserPasswordHistory).WithOne(a => a.User).HasForeignKey(u => u.UserId);
            entityBuilder.HasMany(p => p.UserArea).WithOne(a => a.User).HasForeignKey(u => u.UserId);
            entityBuilder.HasMany(p => p.UserOffice).WithOne(o => o.User).HasForeignKey(u => u.UserId);
            entityBuilder.HasMany(p => p.UserPosition).WithOne(s => s.User).HasForeignKey(u => u.UserId);
            entityBuilder.HasMany(p => p.UserRate).WithOne(r => r.User).HasForeignKey(u => u.UserId);
            entityBuilder.HasMany(p => p.UserSalary).WithOne(y => y.User).HasForeignKey(u => u.UserId);
            entityBuilder.HasMany(p => p.UserCollective).WithOne(c => c.User).HasForeignKey(u => u.UserId);
            entityBuilder.HasMany(p => p.UserCollectiveCategory).WithOne(c => c.User).HasForeignKey(u => u.UserId);
            entityBuilder.HasMany(p => p.UserOperational).WithOne(c => c.User).HasForeignKey(u => u.UserId);
            entityBuilder.HasMany(p => p.UserContract).WithOne(c => c.User).HasForeignKey(u => u.UserId);
            entityBuilder.HasMany(p => p.UserWorkConditions).WithOne(c => c.User).HasForeignKey(u => u.UserId);

            entityBuilder.Property(u => u.AccessFailedCount).HasDefaultValue(0);
            entityBuilder.Property(u => u.ConcurrencyStamp).IsConcurrencyToken();
            entityBuilder.Property(u => u.Email).HasMaxLength(256);
            entityBuilder.Property(u => u.EmailConfirmed);
            entityBuilder.Property(u => u.LockoutEnabled);
            entityBuilder.Property(u => u.LockoutEnd);
            entityBuilder.Property(u => u.NormalizedEmail).HasMaxLength(256);
            entityBuilder.Property(u => u.NormalizedUserName).HasMaxLength(256);
            entityBuilder.Property(u => u.PasswordHash);
            entityBuilder.Property(u => u.PhoneNumber);
            entityBuilder.Property(u => u.SecurityStamp);
            entityBuilder.Property(u => u.UserName).HasMaxLength(256);
            entityBuilder.HasIndex(u => u.NormalizedEmail).HasName("EmailIndex");
            entityBuilder.HasIndex(u => u.NormalizedUserName).IsUnique().HasName("UserNameIndex");

            entityBuilder.HasMany(u => u.UserRole).WithOne(a => a.User);
   
            entityBuilder.ToTable("User");

        }
    
    }
}