using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Perseo.Core;
using Microsoft.EntityFrameworkCore;

namespace Perseo.Infraestructure
{
    public class UserOfficeMap
    {
        public UserOfficeMap(EntityTypeBuilder<UserOffice> entityBuilder)
        {
            entityBuilder.HasKey(p => new { p.Id, p.UserId, p.OfficeId });
            entityBuilder.HasOne(u => u.User).WithMany(s => s.UserOffice).HasForeignKey(x => x.UserId);
            entityBuilder.HasOne(u => u.Office).WithMany(s => s.UserOffice).HasForeignKey(x => x.OfficeId);
        }
    }
}