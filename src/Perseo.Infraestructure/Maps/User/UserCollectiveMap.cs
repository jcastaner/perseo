using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Perseo.Core;
 
namespace Perseo.Infraestructure
{
    public class UserCollectiveMap
    {
        public UserCollectiveMap(EntityTypeBuilder<UserCollective> entityBuilder)
        {
            entityBuilder.HasKey(p => new { p.Id, p.UserId, p.CollectiveId });
            entityBuilder.HasOne(u => u.User).WithMany(s => s.UserCollective).HasForeignKey(x => x.UserId);
            entityBuilder.HasOne(u => u.Collective).WithMany(s => s.UserCollective).HasForeignKey(x => x.CollectiveId);
        }
    }
}