using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Perseo.Core;
 
namespace Perseo.Infraestructure
{
    public class CollectiveMap
    {
        public CollectiveMap(EntityTypeBuilder<Collective> entityBuilder)
        { 
            entityBuilder.Property(t => t.Name).IsRequired();
            entityBuilder.HasMany(p => p.UserCollective).WithOne(a => a.Collective).HasForeignKey(u => u.CollectiveId);
        }
    }
}