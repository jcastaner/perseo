using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Perseo.Core;
using Microsoft.EntityFrameworkCore;

namespace Perseo.Infraestructure
{
    public class UserAreaMap
    {
        public UserAreaMap(EntityTypeBuilder<UserArea> entityBuilder)
        {
            entityBuilder.Property(u => u.Owner).HasDefaultValue(false);
            entityBuilder.Property(u => u.Authorized).HasDefaultValue(false);
            entityBuilder.HasKey(p => new { p.Id, p.UserId, p.AreaId });
            entityBuilder.HasOne(u => u.User).WithMany(s => s.UserArea).HasForeignKey(x => x.UserId);
            entityBuilder.HasOne(u => u.Area).WithMany(s => s.UserArea).HasForeignKey(x => x.AreaId);
            /*
            entityBuilder.HasData(
               new UserArea { Id = 1, StartDate = new System.DateTime(1900, 1, 1), Owner = false, Authorized = false, UserId = 1, AreaId = 1 }
             
               new UserArea { Id = 2, StartDate = new System.DateTime(1900, 1, 1), Owner = true, Authorized = false, UserId = 1, AreaId = 2 },

               new UserArea { Id = 3, StartDate = new System.DateTime(1900, 1, 1), Owner = false, Authorized = false, UserId = 2, AreaId = 3 },
               new UserArea { Id = 4, StartDate = new System.DateTime(1900, 1, 1), Owner = false, Authorized = false, UserId = 3, AreaId = 4 },
               new UserArea { Id = 5, StartDate = new System.DateTime(1900, 1, 1), Owner = false, Authorized = false, UserId = 4, AreaId = 10 },
               new UserArea { Id = 6, StartDate = new System.DateTime(1900, 1, 1), Owner = false, Authorized = false, UserId = 5, AreaId = 13 },

               new UserArea { Id = 7, StartDate = new System.DateTime(1900, 1, 1), Owner = true, Authorized = false, UserId = 2, AreaId = 4 },
               new UserArea { Id = 8, StartDate = new System.DateTime(1900, 1, 1), Owner = true, Authorized = false, UserId = 3, AreaId = 10 },
               new UserArea { Id = 9, StartDate = new System.DateTime(1900, 1, 1), Owner = true, Authorized = false, UserId = 4, AreaId = 13 },
               new UserArea { Id = 10, StartDate = new System.DateTime(1900, 1, 1), Owner = true, Authorized = false, UserId = 5, AreaId = 14 },

               new UserArea { Id = 11, StartDate = new System.DateTime(1900, 1, 1), Owner = false, Authorized = true, UserId = 6, AreaId = 14 },
               new UserArea { Id = 12, StartDate = new System.DateTime(1900, 1, 1), Owner = false, UserId = 7, AreaId = 14 }
           
               );*/
        }
    }
}