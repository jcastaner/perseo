using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Perseo.Core;
using Microsoft.EntityFrameworkCore;

namespace Perseo.Infraestructure
{
    public class UserOperationalMap
    {
        public UserOperationalMap(EntityTypeBuilder<UserOperational> entityBuilder)
        {
            entityBuilder.HasKey(p => new { p.Id, p.UserId });
            entityBuilder.HasOne(u => u.User).WithMany(s => s.UserOperational).HasForeignKey(x => x.UserId);
        }
    }
}