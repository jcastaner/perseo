using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Perseo.Core;
 
namespace Perseo.Infraestructure
{
    public class CalendarMap
    {
        public CalendarMap(EntityTypeBuilder<Calendar> entityBuilder)
        {
            entityBuilder.Property(c => c.Name).IsRequired();

            entityBuilder.HasMany(c => c.Days).WithOne(d => d.Calendar).HasForeignKey(d => d.CalendarId);
            entityBuilder.HasMany(c => c.Office).WithOne(o => o.Calendar).HasForeignKey(o => o.CalendarId);
        }
    }
}