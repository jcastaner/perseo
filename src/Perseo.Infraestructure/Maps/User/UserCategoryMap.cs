using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Perseo.Core;
 
namespace Perseo.Infraestructure
{
    public class UserCategoryMap
    {
        public UserCategoryMap(EntityTypeBuilder<UserCategory> entityBuilder)
        {
            entityBuilder.HasKey(p => new { p.Id, p.UserId, p.CategoryId });
            entityBuilder.HasOne(u => u.User).WithMany(s => s.UserCategory).HasForeignKey(x => x.UserId);
            entityBuilder.HasOne(u => u.Category).WithMany(s => s.UserCategory).HasForeignKey(x => x.CategoryId);
        }
    }
}