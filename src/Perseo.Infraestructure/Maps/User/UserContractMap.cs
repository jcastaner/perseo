using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Perseo.Core;

namespace Perseo.Infraestructure
{
    public class UserContractMap
    {
        public UserContractMap(EntityTypeBuilder<UserContract> entityBuilder)
        {
            entityBuilder.Property(t => t.ContractPeriod).HasConversion<string>();
            entityBuilder.HasOne(u => u.User).WithMany(s => s.UserContract).HasForeignKey(x => x.UserId);
            entityBuilder.HasOne(u => u.Company).WithMany(s => s.UserContract).HasForeignKey(x => x.CompanyId);
        }
    }
}