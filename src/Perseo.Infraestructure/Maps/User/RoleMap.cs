using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Perseo.Core;
using Microsoft.EntityFrameworkCore;

namespace Perseo.Infraestructure
{
    public class RoleMap
    {
        public RoleMap(EntityTypeBuilder<Role> entityBuilder)
        { 
            entityBuilder.Property(t => t.ConcurrencyStamp).IsConcurrencyToken();
            entityBuilder.Property(t => t.Name).HasMaxLength(256);
            entityBuilder.Property(t => t.NormalizedName).HasMaxLength(256);

            entityBuilder.HasIndex(t => t.NormalizedName).IsUnique().HasName("RoleNameIndex");
            entityBuilder.HasMany(t => t.UserRole).WithOne(a => a.Role);
            /*
            entityBuilder.HasData(
                new Role { Id  = 1, Name = "Administration", NormalizedName = "ADMINISTRATION" },
                new Role { Id  = 2, Name = "Maintenance", NormalizedName = "MAINTENANCE"},
                new Role { Id  = 3, Name = "EconomicalManagementShow", NormalizedName = "ECONOMICALMANAGEMENTSHOW" },                                       
                new Role { Id  = 4, Name = "EconomicalManagementEdit", NormalizedName = "ECONOMICALMANAGEMENTEDIT" },              
                new Role { Id  = 5, Name = "TecnicalManagementShow", NormalizedName = "TECNICALMANAGEMENTSHOW" },       
                new Role { Id  = 6, Name = "TecnicalManagementEdit", NormalizedName = "TECNICALMANAGEMENTEDIT" },                       
                new Role { Id  = 7, Name = "OperationalManagementShow", NormalizedName = "OPERATIONALMANAGEMENTSHOW" },                     
                new Role { Id  = 8, Name = "OperationalManagementEdit", NormalizedName = "OPERATIONALMANAGEMENTEDIT" },
                new Role { Id  = 9, Name = "QualityManagementShow", NormalizedName = "QUALITYMANAGEMENTSHOW" },
                new Role { Id = 10, Name = "QualityManagementEdit", NormalizedName = "QUALITYMANAGEMENTEDIT" },
                new Role { Id = 11, Name = "TrainingManagementShow", NormalizedName = "TRAININGMANAGEMENTSHOW" },
                new Role { Id = 12, Name = "TrainingManagementEdit", NormalizedName = "TRAININGMANAGEMENTEDIT" },
                new Role { Id = 13, Name = "ItManagementShow", NormalizedName = "ITMANAGEMENTSHOW" },
                new Role { Id = 14, Name = "ItManagementEdit", NormalizedName = "ITMANAGEMENTEDIT" },
                new Role { Id = 15, Name = "HHRRManagementShow", NormalizedName = "HHRRMANAGEMENTSHOW" },
                new Role { Id = 16, Name = "HHRRManagementEdit", NormalizedName = "HHRRMANAGEMENTEDIT" },
                new Role { Id = 17, Name = "Operation", NormalizedName = "OPERATION" });*/

        }
    }
}