using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Perseo.Core;
 
namespace Perseo.Infraestructure
{
    public class UserPositionMap
    {
        public UserPositionMap(EntityTypeBuilder<UserPosition> entityBuilder)
        {
            entityBuilder.HasKey(p => new { p.Id, p.UserId });
        }
    }
}