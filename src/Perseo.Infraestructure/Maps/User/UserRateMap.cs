using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Perseo.Core;
 
namespace Perseo.Infraestructure
{
    public class UserRateMap
    {
        public UserRateMap(EntityTypeBuilder<UserRate> entityBuilder)
        {
            entityBuilder.HasKey(p => new { p.Id, p.UserId, p.RateId });
            entityBuilder.HasOne(u => u.User).WithMany(s => s.UserRate).HasForeignKey(x => x.UserId);
            entityBuilder.HasOne(u => u.Rate).WithMany(s => s.UserRate).HasForeignKey(x => x.RateId);
        }
    }
}