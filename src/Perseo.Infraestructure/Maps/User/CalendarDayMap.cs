using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Perseo.Core;
 
namespace Perseo.Infraestructure
{
    public class CalendarDayMap
    {
        public CalendarDayMap(EntityTypeBuilder<CalendarDay> entityBuilder)
        {
            entityBuilder.Property(d => d.Name).IsRequired();
            entityBuilder.Property(d => d.Year).IsRequired();
            entityBuilder.Property(d => d.Month).IsRequired();
            entityBuilder.Property(d => d.Day).IsRequired();
            entityBuilder.Property(d => d.Type).IsRequired();

            entityBuilder.HasOne(d => d.Calendar).WithMany(c => c.Days).HasForeignKey(d => d.CalendarId);
        }
    }
}