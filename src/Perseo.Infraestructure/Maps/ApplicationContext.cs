using Perseo.Core;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Text.RegularExpressions;

namespace Perseo.Infraestructure
{
    public class ApplicationContext : DbContext
    {
       
        public ApplicationContext(DbContextOptions<ApplicationContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            
            new CalendarMap(modelBuilder.Entity<Calendar>());
            new CategoryMap(modelBuilder.Entity<Category>());
            new CollectiveMap(modelBuilder.Entity<Collective>());
            
            new OfficeMap(modelBuilder.Entity<Office>());
            new RateMap(modelBuilder.Entity<Rate>());
            new ShiftMap(modelBuilder.Entity<Shift>());
            new CompanyMap(modelBuilder.Entity<Company>());

            #region AreaMaps

            new AreaCodeWbsMap(modelBuilder.Entity<AreaCodeWbs>());
            new AreaCodeMap(modelBuilder.Entity<AreaCode>());
            new AreaMap(modelBuilder.Entity<Area>());

            #endregion

            #region JobMaps

            new JobMap(modelBuilder.Entity<Job>());
            new JobConfigMap(modelBuilder.Entity<JobConfig>());
            new JobForecastMap(modelBuilder.Entity<JobForecast>());
            new JobKpiMap(modelBuilder.Entity<JobKpi>());
            new ForecastMap(modelBuilder.Entity<Forecast>());

            #endregion

            #region ScheduleMaps

            new ScheduleDateMap(modelBuilder.Entity<ScheduleDate>());
            new ScheduleDateTermMap(modelBuilder.Entity<ScheduleDateTerm>());
            new ScheduleDateTermJobMap(modelBuilder.Entity<ScheduleDateTermJob>());

            #endregion

            #region UserMaps

            new UserMap(modelBuilder.Entity<User>());
            new UserAreaMap(modelBuilder.Entity<UserArea>());
            new UserOperationalMap(modelBuilder.Entity<UserOperational>());
            new UserCategoryMap(modelBuilder.Entity<UserCategory>());
            new UserCollectiveMap(modelBuilder.Entity<UserCollective>());
            new UserCollectiveCategoryMap(modelBuilder.Entity<UserCollectiveCategory>());
            new UserOfficeMap(modelBuilder.Entity<UserOffice>());
            new UserOperationalMap(modelBuilder.Entity<UserOperational>());
            new UserPositionMap(modelBuilder.Entity<UserPosition>());
            new UserRateMap(modelBuilder.Entity<UserRate>());
            new UserSalaryMap(modelBuilder.Entity<UserSalary>());
            new UserShiftMap(modelBuilder.Entity<UserShift>());
            new UserContractMap(modelBuilder.Entity<UserContract>());
            new UserWorkConditionsMap(modelBuilder.Entity<UserWorkConditions>());

            #endregion

            #region IdentityMaps

            new RoleMap(modelBuilder.Entity<Role>());
            new UserRoleMap(modelBuilder.Entity<UserRole>());

            #endregion

			#region TrainingMaps

            new TrainingMap(modelBuilder.Entity<Training>());

            new TrainingTestMap(modelBuilder.Entity<TrainingTest>());
            new TrainingTestOwnerMap(modelBuilder.Entity<TrainingTestOwner>());
            new TrainingTestSectionMap(modelBuilder.Entity<TrainingTestSection>());
            new TrainingTestSectionFieldMap(modelBuilder.Entity<TrainingTestSectionField>());
            new TrainingTestSectionFieldOptionMap(modelBuilder.Entity<TrainingTestSectionFieldOption>());
            new TrainingTestUserMap(modelBuilder.Entity<TrainingTestUser>());
            new TrainingTestUserTakeMap(modelBuilder.Entity<TrainingTestUserTake>());
            new TrainingTestUserTakeSectionMap(modelBuilder.Entity<TrainingTestUserTakeSection>());
            new TrainingTestUserTakeSectionFieldMap(modelBuilder.Entity<TrainingTestUserTakeSectionField>());
            new TrainingTestUserTakeSectionFieldOptionMap(modelBuilder.Entity<TrainingTestUserTakeSectionFieldOption>());
            new TrainingUserMap(modelBuilder.Entity<TrainingUser>());

            #endregion
			
            #region NonConformityCardMaps

            new NonConformityCardMap(modelBuilder.Entity<NonConformityCard>());
            new NonConformityCardSignMap(modelBuilder.Entity<NonConformityCardSign>());

            #endregion

            #region Quality

            new QualityMap(modelBuilder.Entity<Quality>());
            new QualityOwnerMap(modelBuilder.Entity<QualityOwner>());
            new QualityUserMap(modelBuilder.Entity<QualityUser>());


            new QualityAuditMap(modelBuilder.Entity<QualityAudit>());
            new QualityAuditSectionMap(modelBuilder.Entity<QualityAuditSection>());
            new QualityAuditSectionFieldMap(modelBuilder.Entity<QualityAuditSectionField>());
            new QualityAuditSectionFieldOptionMap(modelBuilder.Entity<QualityAuditSectionFieldOption>());

            new QualityAuditOwnerMap(modelBuilder.Entity<QualityAuditOwner>());
            new QualityAuditUserMap(modelBuilder.Entity<QualityAuditUser>());
            new QualityAuditUserTakeMap(modelBuilder.Entity<QualityAuditUserTake>());
            new QualityAuditUserTakeSectionMap(modelBuilder.Entity<QualityAuditUserTakeSection>());
            new QualityAuditUserTakeSectionFieldMap(modelBuilder.Entity<QualityAuditUserTakeSectionField>());
            new QualityAuditUserTakeSectionFieldOptionMap(modelBuilder.Entity<QualityAuditUserTakeSectionFieldOption>());


            new QualityExamMap(modelBuilder.Entity<QualityExam>());
            new QualityExamSectionMap(modelBuilder.Entity<QualityExamSection>());
            new QualityExamSectionFieldMap(modelBuilder.Entity<QualityExamSectionField>());
            new QualityExamSectionFieldOptionMap(modelBuilder.Entity<QualityExamSectionFieldOption>());

            new QualityExamOwnerMap(modelBuilder.Entity<QualityExamOwner>());
            new QualityExamUserMap(modelBuilder.Entity<QualityExamUser>());
            new QualityExamUserTakeMap(modelBuilder.Entity<QualityExamUserTake>());

            

            #endregion


            new NotificationMap(modelBuilder.Entity<Notification>());

            new RequestMap(modelBuilder.Entity<Request>());
            new RequestHistoryMap(modelBuilder.Entity<RequestHistory>());

            new BookmarkMap(modelBuilder.Entity<Bookmark>());



            foreach (var entityType in modelBuilder.Model.GetEntityTypes()
                .Where(t => t.ClrType.IsSubclassOf(typeof(Entity))))
            {
                modelBuilder.Entity(
                    entityType.Name,
                    x => {
                        x.Property("Id").ValueGeneratedOnAdd();
                        if (entityType.GetKeys().Count() < 1)
                        {
                            x.HasKey("Id");
                        }
                        else
                        {
                            x.HasAlternateKey("Id");
                        }
                    }
                );
            }

            foreach (var entity in modelBuilder.Model.GetEntityTypes())
            {
  
                entity.Relational().TableName = entity.Relational().TableName.ToSnakeCase();
        
                foreach (var property in entity.GetProperties())
                {
                    property.Relational().ColumnName = property.Relational().ColumnName.ToSnakeCase();
                }

                foreach (var key in entity.GetKeys())
                {
                    key.Relational().Name = key.Relational().Name.ToSnakeCase();
                }

                foreach (var key in entity.GetForeignKeys())
                {
                    key.Relational().Name = key.Relational().Name.ToSnakeCase();
                }

                foreach (var index in entity.GetIndexes())
                {
                    index.Relational().Name = index.Relational().Name.ToSnakeCase();
                }
            }
            
        }
    }

    public static class StringExtensions
    {
        public static string ToSnakeCase(this string input)
        {
            if (string.IsNullOrEmpty(input)) { return input; }

            var startUnderscores = Regex.Match(input, @"^_+");
            return startUnderscores + Regex.Replace(input, @"([a-z0-9])([A-Z])", "$1_$2").ToLower();
        }
    }
}