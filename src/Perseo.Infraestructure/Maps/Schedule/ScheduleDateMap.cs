using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Perseo.Core;
 
namespace Perseo.Infraestructure
{
    public class ScheduleDateMap
    {
        public ScheduleDateMap(EntityTypeBuilder<ScheduleDate> entityBuilder)
        {
            entityBuilder.Property(u => u.Acronym).HasMaxLength(2);
            entityBuilder.HasOne(u => u.User).WithMany(s => s.ScheduleDate).HasForeignKey(x => x.UserId);
            entityBuilder.HasMany(u => u.ScheduleDateTerm).WithOne(s => s.ScheduleDate).HasForeignKey(x => x.ScheduleDateId);
            entityBuilder.HasIndex(p => new { p.UserId, p.Year, p.Month, p.Day }).IsUnique();

        }
    }
}