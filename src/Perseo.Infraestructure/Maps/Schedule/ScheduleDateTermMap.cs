using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Perseo.Core;
 
namespace Perseo.Infraestructure
{
    public class ScheduleDateTermMap
    {
        public ScheduleDateTermMap(EntityTypeBuilder<ScheduleDateTerm> entityBuilder)
        {
            entityBuilder.Property(u => u.Id).ValueGeneratedOnAdd();
            entityBuilder.Property(u => u.Pattern).HasMaxLength(48);
            entityBuilder.Property(t => t.HoursType).HasConversion<string>().HasMaxLength(16);
            entityBuilder.HasOne(u => u.ScheduleDate).WithMany(s => s.ScheduleDateTerm).HasForeignKey(x => x.ScheduleDateId);
        }
    }
}