using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Perseo.Core;
 
namespace Perseo.Infraestructure
{
    public class ScheduleDateTermJobMap
    {
        public ScheduleDateTermJobMap(EntityTypeBuilder<ScheduleDateTermJob> entityBuilder)
        {
            entityBuilder.HasKey(x => new { x.ScheduleDateTermId, x.JobId });
            entityBuilder.HasOne(z => z.ScheduleDateTerm).WithMany(c => c.ScheduleDateTermJob).HasForeignKey(d => d.ScheduleDateTermId);
            entityBuilder.HasOne(y => y.Job).WithMany(b => b.ScheduleDateTermJob).HasForeignKey(a => a.JobId);
        }
    }
}