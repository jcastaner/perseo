using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Perseo.Core;
 
namespace Perseo.Infraestructure
{
    public class BookmarkMap
    {
        public BookmarkMap(EntityTypeBuilder<Bookmark> entityBuilder)
        {
            entityBuilder.Property(t => t.Name).IsRequired();
            entityBuilder.HasOne(u => u.User).WithMany(s => s.Bookmark).HasForeignKey(k => k.UserId);
            entityBuilder.HasOne(u => u.Parent).WithMany(s => s.Children).HasForeignKey(k => k.ParentId);
        }
    }
}