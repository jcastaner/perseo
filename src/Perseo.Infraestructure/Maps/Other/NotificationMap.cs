using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Perseo.Core;
 
namespace Perseo.Infraestructure
{
    public class NotificationMap
    {
        public NotificationMap(EntityTypeBuilder<Notification> entityBuilder)
        {
            entityBuilder.Property(t => t.Type).HasConversion<int>();
            entityBuilder.HasOne(u => u.User).WithMany().HasForeignKey(x => x.UserId);
        }
    }
}