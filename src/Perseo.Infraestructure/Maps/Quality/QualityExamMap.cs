using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Perseo.Core;
using Microsoft.EntityFrameworkCore;

namespace Perseo.Infraestructure
{
    public class QualityExamMap
    {
        public QualityExamMap(EntityTypeBuilder<QualityExam> entityBuilder)
        {

            entityBuilder.HasOne(a => a.Quality).WithMany(t => t.Exams).HasForeignKey(x => x.QualityId);

            entityBuilder.HasMany(a => a.Owners).WithOne(t => t.Exam).HasForeignKey(x => x.ExamId);
            entityBuilder.HasMany(a => a.Users).WithOne(t => t.Exam).HasForeignKey(x => x.ExamId);
            entityBuilder.HasMany(a => a.Sections).WithOne(t => t.Exam).HasForeignKey(x => x.ExamId);
        }
    }
}