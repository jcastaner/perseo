using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Perseo.Core;
using Microsoft.EntityFrameworkCore;

namespace Perseo.Infraestructure
{
    public class QualityExamUserMap
    {
        public QualityExamUserMap(EntityTypeBuilder<QualityExamUser> entityBuilder)
        {
            entityBuilder.HasOne(a => a.Exam).WithMany(t => t.Users).HasForeignKey(t => t.ExamId);
            entityBuilder.HasOne(a => a.ExamOwner).WithMany(t => t.Users).HasForeignKey(t => t.ExamOwnerId);
            entityBuilder.HasOne(a => a.User).WithMany(s => s.QualityExamUser).HasForeignKey(t => t.UserId);
            entityBuilder.HasMany(a => a.Takes);
        }
    }
}