using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Perseo.Core;
using Microsoft.EntityFrameworkCore;

namespace Perseo.Infraestructure
{
    public class QualityExamSectionMap
    {
        public QualityExamSectionMap(EntityTypeBuilder<QualityExamSection> entityBuilder)
        {

            entityBuilder.HasOne(a => a.Exam).WithMany(t => t.Sections).HasForeignKey(t => t.ExamId);
            entityBuilder.HasMany(a => a.Fields).WithOne(t => t.Section).HasForeignKey(t => t.SectionId);
        }
    }
}