using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Perseo.Core;
using Microsoft.EntityFrameworkCore;

namespace Perseo.Infraestructure
{
    public class QualityExamUserTakeMap
    {
        public QualityExamUserTakeMap(EntityTypeBuilder<QualityExamUserTake> entityBuilder)
        {
            entityBuilder.HasOne(a => a.ExamUser).WithMany(x => x.Takes).HasForeignKey(t => t.ExamUserId);
        }
    }
}