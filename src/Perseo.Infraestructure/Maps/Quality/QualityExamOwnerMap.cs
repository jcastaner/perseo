using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Perseo.Core;
using Microsoft.EntityFrameworkCore;

namespace Perseo.Infraestructure
{
    public class QualityExamOwnerMap
    {
        public QualityExamOwnerMap(EntityTypeBuilder<QualityExamOwner> entityBuilder)
        {
            entityBuilder.HasOne(a => a.Exam).WithMany(t => t.Owners).HasForeignKey(t => t.ExamId);
            entityBuilder.HasOne(a => a.User).WithMany().HasForeignKey(t => t.ExamId);
        }
    }
}