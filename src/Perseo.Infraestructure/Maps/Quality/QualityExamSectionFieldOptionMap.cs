using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Perseo.Core;
using Microsoft.EntityFrameworkCore;

namespace Perseo.Infraestructure
{
    public class QualityExamSectionFieldOptionMap
    {
        public QualityExamSectionFieldOptionMap(EntityTypeBuilder<QualityExamSectionFieldOption> entityBuilder)
        {
            entityBuilder.HasOne(a => a.Field).WithMany(t => t.Options).HasForeignKey(t => t.FieldId);
        }
  
    }
}