using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Perseo.Core;
using Microsoft.EntityFrameworkCore;

namespace Perseo.Infraestructure
{
    public class QualityAuditSectionMap
    {
        public QualityAuditSectionMap(EntityTypeBuilder<QualityAuditSection> entityBuilder)
        {

            entityBuilder.HasOne(a => a.Audit).WithMany(t => t.Sections).HasForeignKey(t => t.AuditId);
            entityBuilder.HasMany(a => a.Fields).WithOne(t => t.Section).HasForeignKey(t => t.SectionId);
        }
    }
}