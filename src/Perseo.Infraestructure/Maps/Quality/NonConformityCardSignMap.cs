using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Perseo.Core;
 
namespace Perseo.Infraestructure
{
    public class NonConformityCardSignMap
    {
        public NonConformityCardSignMap(EntityTypeBuilder<NonConformityCardSign> entityBuilder)
        {
           
            
            entityBuilder.HasOne(u => u.User).WithMany(s => s.NonConformityCardSign).HasForeignKey(u => u.UserId);
            entityBuilder.HasOne(u => u.NonConformityCard).WithMany(s => s.NonConformityCardSign).HasForeignKey(u => u.NonConformityCardId);
                       
           
        }
    }
}