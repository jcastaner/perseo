using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Perseo.Core;
using Microsoft.EntityFrameworkCore;

namespace Perseo.Infraestructure
{
    public class QualityUserMap
    {
        public QualityUserMap(EntityTypeBuilder<QualityUser> entityBuilder)
        {
            entityBuilder.HasOne(u => u.User).WithMany(s => s.QualityUser).HasForeignKey(x => x.UserId);
            entityBuilder.HasOne(u => u.Quality).WithMany(s => s.Users).HasForeignKey(x => x.QualityId);
        }
    }
}