using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Perseo.Core;
using Microsoft.EntityFrameworkCore;

namespace Perseo.Infraestructure
{
    public class QualityMap
    {
        public QualityMap(EntityTypeBuilder<Quality> entityBuilder)
        {

            entityBuilder.HasOne(a => a.Area).WithMany(q => q.Quality).HasForeignKey(t => t.AreaId);
            entityBuilder.HasOne(a => a.Creator).WithMany().HasForeignKey(t => t.CreatorId);

            entityBuilder.HasMany(a => a.Owners).WithOne(q => q.Quality).HasForeignKey(q => q.QualityId);
            entityBuilder.HasMany(a => a.Users).WithOne(q => q.Quality).HasForeignKey(q => q.QualityId);
            entityBuilder.HasMany(a => a.Audits).WithOne(q => q.Quality).HasForeignKey(q => q.QualityId);
            entityBuilder.HasMany(a => a.Exams).WithOne(q => q.Quality).HasForeignKey(q => q.QualityId);
        }
    }
}