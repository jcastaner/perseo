using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Perseo.Core;
using Microsoft.EntityFrameworkCore;

namespace Perseo.Infraestructure
{
    public class QualityAuditUserTakeMap
    {
        public QualityAuditUserTakeMap(EntityTypeBuilder<QualityAuditUserTake> entityBuilder)
        {

            entityBuilder.HasOne(a => a.Auditor).WithMany().HasForeignKey(t => t.AuditorId);
            entityBuilder.HasOne(a => a.AuditUser).WithMany(x=>x.Takes).HasForeignKey(t => t.AuditUserId);
            entityBuilder.HasMany(a => a.Sections).WithOne(x=>x.Take);
        }
    }
}