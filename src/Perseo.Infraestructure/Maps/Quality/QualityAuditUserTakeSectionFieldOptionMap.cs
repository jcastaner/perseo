using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Perseo.Core;
using Microsoft.EntityFrameworkCore;

namespace Perseo.Infraestructure
{  
    public class QualityAuditUserTakeSectionFieldOptionMap
    {
        public QualityAuditUserTakeSectionFieldOptionMap(EntityTypeBuilder<QualityAuditUserTakeSectionFieldOption> entityBuilder)
        {
            entityBuilder.HasOne(a => a.Field).WithMany(x=>x.Options).HasForeignKey(t => t.FieldId);
            entityBuilder.HasOne(a => a.AuditOption).WithMany().HasForeignKey(t => t.AuditOptionId);
        }

    }  
}  