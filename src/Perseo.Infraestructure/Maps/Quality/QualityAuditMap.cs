using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Perseo.Core;
using Microsoft.EntityFrameworkCore;

namespace Perseo.Infraestructure
{
    public class QualityAuditMap
    {
        public QualityAuditMap(EntityTypeBuilder<QualityAudit> entityBuilder)
        {
            entityBuilder.HasOne(a => a.Quality).WithMany(t => t.Audits).HasForeignKey(x => x.QualityId);

            entityBuilder.HasMany(a => a.Owners).WithOne(t => t.Audit).HasForeignKey(x =>x.AuditId);
            entityBuilder.HasMany(a => a.Users).WithOne(t => t.Audit).HasForeignKey(x => x.AuditId);
            entityBuilder.HasMany(a => a.Sections).WithOne(t => t.Audit).HasForeignKey(x => x.AuditId);
        }
    }
}