using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Perseo.Core;
using Microsoft.EntityFrameworkCore;

namespace Perseo.Infraestructure
{
    public class QualityAuditOwnerMap
    {
        public QualityAuditOwnerMap(EntityTypeBuilder<QualityAuditOwner> entityBuilder)
        {
            entityBuilder.HasOne(a => a.Audit).WithMany(t => t.Owners).HasForeignKey(t => t.AuditId);
//            entityBuilder.HasOne(a => a.User).WithMany().HasForeignKey(t => t.UserId);
        }
    }
}