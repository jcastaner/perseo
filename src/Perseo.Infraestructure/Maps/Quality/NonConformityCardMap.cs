using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Perseo.Core;
 
namespace Perseo.Infraestructure
{
    public class NonConformityCardMap
    {
        public NonConformityCardMap(EntityTypeBuilder<NonConformityCard> entityBuilder)
        {
            entityBuilder.Property(t => t.IncidentTypology).HasConversion<string>();
            entityBuilder.Property(t => t.MinuteCost).HasConversion<string>();
            entityBuilder.Property(t => t.HumanErrorSubtype).HasConversion<string>();
            entityBuilder.Property(t => t.State).HasConversion<string>();

            entityBuilder.HasOne(u => u.TargetUser).WithMany(s => s.NonConformityCardTarget).HasForeignKey(u => u.TargetUserId);
            entityBuilder.HasOne(u => u.OriginUser).WithMany(s => s.NonConformityCardOrigin).HasForeignKey(u => u.OriginUserId);
            entityBuilder.HasOne(u => u.Job).WithMany(s => s.NonConformityCard).HasForeignKey(u => u.JobId);
            entityBuilder.HasMany(u => u.NonConformityCardSign).WithOne(s => s.NonConformityCard);
                       
           
        }
    }
}