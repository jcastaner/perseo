using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Perseo.Core;
using Microsoft.EntityFrameworkCore;

namespace Perseo.Infraestructure
{  
    public class QualityAuditUserTakeSectionFieldMap 
    {
        public QualityAuditUserTakeSectionFieldMap(EntityTypeBuilder<QualityAuditUserTakeSectionField> entityBuilder)
        {
            entityBuilder.HasMany(a => a.Options).WithOne(x => x.Field);
            entityBuilder.HasOne(a => a.Section).WithMany(x => x.Fields).HasForeignKey(t => t.SectionId);
            entityBuilder.HasOne(a => a.AuditField).WithMany().HasForeignKey(t => t.AuditFieldId);
        }

    }  
}  