using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Perseo.Core;
using Microsoft.EntityFrameworkCore;

namespace Perseo.Infraestructure
{
    public class QualityAuditUserMap
    {
        public QualityAuditUserMap(EntityTypeBuilder<QualityAuditUser> entityBuilder)
        {
            entityBuilder.HasOne(a => a.Audit).WithMany(t => t.Users).HasForeignKey(t => t.AuditId);
            entityBuilder.HasOne(a => a.AuditOwner).WithMany(t => t.Users).HasForeignKey(t => t.AuditOwnerId);
            entityBuilder.HasOne(a => a.User).WithMany(s => s.QualityAuditUser).HasForeignKey(t => t.UserId);
            entityBuilder.HasMany(a => a.Takes);
        }
    }
}