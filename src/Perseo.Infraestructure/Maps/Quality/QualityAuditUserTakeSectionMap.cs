using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Perseo.Core;
using Microsoft.EntityFrameworkCore;

namespace Perseo.Infraestructure
{  
    public class QualityAuditUserTakeSectionMap
    {
        public QualityAuditUserTakeSectionMap(EntityTypeBuilder<QualityAuditUserTakeSection> entityBuilder)
        {
            entityBuilder.HasMany(a => a.Fields).WithOne(x => x.Section);
            entityBuilder.HasOne(a => a.AuditSection).WithMany().HasForeignKey(t => t.AuditSectionId);
        }

    }  
}  