using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Perseo.Core;
using Microsoft.EntityFrameworkCore;

namespace Perseo.Infraestructure
{
    public class QualityOwnerMap
    {
        public QualityOwnerMap(EntityTypeBuilder<QualityOwner> entityBuilder)
        {
            entityBuilder.HasOne(u => u.Owner).WithMany(s => s.QualityOwner).HasForeignKey(x => x.OwnerId);
            entityBuilder.HasOne(u => u.Quality).WithMany(s => s.Owners).HasForeignKey(x => x.QualityId);
        }
    }
}