using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Perseo.Core;
using Microsoft.EntityFrameworkCore;
using System;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using System.Collections.Generic;
using System.Linq;

namespace Perseo.Infraestructure
{
    public class ForecastMap
    {
        public ForecastMap(EntityTypeBuilder<Forecast> entityBuilder)
        {
            entityBuilder.HasKey(p => p.Id);
            entityBuilder.HasOne(u => u.Job).WithMany(t => t.Forecast).HasForeignKey(x => x.JobId);
        }
    }
}