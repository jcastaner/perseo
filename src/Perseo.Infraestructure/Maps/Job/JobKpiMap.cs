using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Perseo.Core;
 
namespace Perseo.Infraestructure
{
    public class JobKpiMap
    {
        public JobKpiMap(EntityTypeBuilder<JobKpi> entityBuilder)
        {
            entityBuilder.Property(u => u.Id).ValueGeneratedOnAdd();
            entityBuilder.HasKey(p => p.Id);
            entityBuilder.HasOne(u => u.Job).WithMany(s => s.JobKpi).HasForeignKey(x => x.JobId);
        }
    }
}