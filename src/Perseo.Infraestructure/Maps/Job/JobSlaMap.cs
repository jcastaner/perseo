using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Perseo.Core;
 
namespace Perseo.Infraestructure
{
    public class JobSlaMap
    {
        public JobSlaMap(EntityTypeBuilder<JobSla> entityBuilder)
        {
            entityBuilder.Property(u => u.Id).ValueGeneratedOnAdd();
            entityBuilder.HasKey(p => p.Id);
            entityBuilder.HasOne(u => u.Job).WithMany(s => s.JobSla).HasForeignKey(x => x.JobId);
        }
    }
}