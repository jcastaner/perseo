using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Perseo.Core;
 
namespace Perseo.Infraestructure
{
    public class JobConfigMap
    {
        public JobConfigMap(EntityTypeBuilder<JobConfig> entityBuilder)
        {
            entityBuilder.Property(u => u.Id).ValueGeneratedOnAdd();
            entityBuilder.HasKey(p => p.Id);
            entityBuilder.HasOne(u => u.Job).WithMany(s => s.JobConfig).HasForeignKey(x => x.JobId);
        }
    }
}