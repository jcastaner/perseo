using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Perseo.Core;
using Microsoft.EntityFrameworkCore;
using System;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using System.Collections.Generic;
using System.Linq;

namespace Perseo.Infraestructure
{
    public class JobForecastMap
    {
        public JobForecastMap(EntityTypeBuilder<JobForecast> entityBuilder)
        {
            var splitStringConverter = new ValueConverter<IEnumerable<double>, string> (
                v => string.Join(";", v.Select(x => x.ToString())), 
                v => v.Split(new[] { ';' }).Select(a => Convert.ToDouble(a))
                );
            entityBuilder.Property(u => u.Events).HasConversion(splitStringConverter);

            entityBuilder.Property(u => u.Id).ValueGeneratedOnAdd();
            entityBuilder.HasKey(p => p.Id);
            entityBuilder.HasOne(u => u.Job).WithMany(s => s.JobForecast).HasForeignKey(x => x.JobId);

            //entityBuilder.Ignore(u => u.Events);
            //entityBuilder.Property(u => u._Events).HasColumnName("events");

        }
    }
}