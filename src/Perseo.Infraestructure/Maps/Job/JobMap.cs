using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Perseo.Core;
 
namespace Perseo.Infraestructure
{
    public class JobMap
    {
        public JobMap(EntityTypeBuilder<Job> entityBuilder)
        {
            entityBuilder.Property(t => t.Type).HasConversion<string>();
            entityBuilder.Property(t => t.Name).IsRequired();
            entityBuilder.HasOne(u => u.Area).WithMany(s => s.Job);
            entityBuilder.HasMany(u => u.JobConfig).WithOne(s => s.Job);
            entityBuilder.HasMany(u => u.JobSla).WithOne(s => s.Job);
            entityBuilder.HasMany(u => u.JobForecast).WithOne(s => s.Job);
            entityBuilder.HasMany(u => u.JobKpi).WithOne(s => s.Job);
            entityBuilder.Property(u => u.Acronym).HasMaxLength(2);

            /*
            entityBuilder.HasData(
                new Job { Id = 1, Name = "Indra", Description = "Indra", Type = JobType.Work, AreaId = 1, Default = true, Acronym = "T", StartDate = new System.DateTime(1900, 1, 1) },
                new Job { Id = 2, Name = "Minsait", Description = "Minsait", Type = JobType.Work, AreaId = 2, Default = true, Acronym = "T", StartDate = new System.DateTime(1900, 1, 1) },
                new Job { Id = 3, Name = "Opsait", Description = "Opsait", Type = JobType.Work, AreaId = 3, Default = true, Acronym = "T", StartDate = new System.DateTime(1900, 1, 1) },
                new Job { Id = 4, Name = "Banca", Description = "Servicios Bancarios", Type = JobType.Work, AreaId = 4, Default = true, Acronym = "T", StartDate = new System.DateTime(1900, 1, 1) },
                new Job { Id = 5, Name = "Jur�dico", Description = "Servicios Jur�dicos", Type = JobType.Work, AreaId = 5, Default = true, Acronym = "T", StartDate = new System.DateTime(1900, 1, 1) },
                new Job { Id = 6, Name = "Telco", Description = "Telco y Media", Type = JobType.Work, AreaId = 6, Default = true, Acronym = "T", StartDate = new System.DateTime(1900, 1, 1) },
                new Job { Id = 7, Name = "AAPP", Description = "AAPP", Type = JobType.Work, AreaId = 7, Default = true, Acronym = "T", StartDate = new System.DateTime(1900, 1, 1) },
                new Job { Id = 8, Name = "Energ�a ", Description = "Energ�a", Type = JobType.Work, AreaId = 8, Default = true, Acronym = "T", StartDate = new System.DateTime(1900, 1, 1) },
                new Job { Id = 9, Name = "Documental", Description = "Documental", Type = JobType.Work, AreaId = 9, Default = true, Acronym = "T", StartDate = new System.DateTime(1900, 1, 1) },
                new Job { Id = 10, Name = "Caixabank", Description = "Servicios Caixabank", Type = JobType.Work, AreaId = 10, Default = true, Acronym = "T", StartDate = new System.DateTime(1900, 1, 1) },
                new Job { Id = 11, Name = "Sabadell", Description = "Servicios Banc Sabadell", Type = JobType.Work, AreaId = 11, Default = true, Acronym = "T", StartDate = new System.DateTime(1900, 1, 1) },
                new Job { Id = 12, Name = "Medios de Pago", Description = "Medios de Pago", Type = JobType.Work, AreaId = 12, Default = true, Acronym = "T", StartDate = new System.DateTime(1900, 1, 1) },
                new Job { Id = 13, Name = "Servicios Especializados", Description = "Servicios Especializados", Type = JobType.Work, AreaId = 13, Default = true, Acronym = "T", StartDate = new System.DateTime(1900, 1, 1) },
                new Job { Id = 14, Name = "Factoring", Description = "Factoring", Type = JobType.Work, AreaId = 14, Default = true, Acronym = "T", StartDate = new System.DateTime(1900, 1, 1) },
                new Job { Id = 15, Name = "Confirming", Description = "Confirming", Type = JobType.Work, AreaId = 15, Default = true, Acronym = "T", StartDate = new System.DateTime(1900, 1, 1) },
                new Job { Id = 16, Name = "Card Factory", Description = "Card Factory", Type = JobType.Work, AreaId = 16, Default = true, Acronym = "T", StartDate = new System.DateTime(1900, 1, 1) },
                new Job { Id = 17, Name = "Intercambios", Description = "Intercambios", Type = JobType.Work, AreaId = 17, Default = true, Acronym = "T", StartDate = new System.DateTime(1900, 1, 1) },

                new Job { Id = 18, Name = "Vacaciones", Description = "Vacaciones", Type = JobType.Hollydays, AreaId = 1, Default = false, Acronym = "V", StartDate = new System.DateTime(2019, 1, 1), EndDate = new System.DateTime(2019, 12, 31) },
                new Job { Id = 19, Name = "Permiso Especial", Description = "Permiso Especial", Type = JobType.SpecialLeave, AreaId = 1, Default = false, Acronym = "E", StartDate = new System.DateTime(2019, 1, 1), EndDate = new System.DateTime(2019, 12, 31) },
                new Job { Id = 20, Name = "Horas M�dicas", Description = "Horas M�dicas", Type = JobType.DoctorHours, AreaId = 1, Default = false, Acronym = "D", StartDate = new System.DateTime(2019, 1, 1), EndDate = new System.DateTime(2019, 12, 31) },
                new Job { Id = 21, Name = "Baja M�dica", Description = "Baja M�dica", Type = JobType.SickLeave, AreaId = 1, Default = false, Acronym = "B", StartDate = new System.DateTime(2019, 1, 1), EndDate = new System.DateTime(2019, 12, 31) },
                new Job { Id = 22, Name = "Ausencia Injustificada", Description = "Ausencia Injustificada", Type = JobType.UnjustifiedAbsence, AreaId = 1, Default = false, Acronym = "A", StartDate = new System.DateTime(2019, 1, 1), EndDate = new System.DateTime(2019, 12, 31) },
                new Job { Id = 23, Name = "Ausencia Remunerada", Description = "Ausencia Remunerada", Type = JobType.JustifiedAbsence, AreaId = 1, Default = false, Acronym = "R", StartDate = new System.DateTime(2019, 1, 1), EndDate = new System.DateTime(2019, 12, 31) },
                new Job { Id = 24, Name = "Horas Sindicales", Description = "Horas Sindicales", Type = JobType.UnionHours, AreaId = 1, Default = false, Acronym = "S", StartDate = new System.DateTime(2019, 1, 1), EndDate = new System.DateTime(2019, 12, 31) }

                );
                */
        }
    }
}