using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Perseo.Core;
using Microsoft.EntityFrameworkCore;

namespace Perseo.Infraestructure
{
    public class TrainingTestMap
    {
        public TrainingTestMap(EntityTypeBuilder<TrainingTest> entityBuilder)
        {
            entityBuilder.HasOne(a => a.Training).WithMany(t => t.Tests).HasForeignKey(x => x.TrainingId);

            entityBuilder.HasMany(a => a.Owners).WithOne(t => t.Test).HasForeignKey(x => x.TestId);
            entityBuilder.HasMany(a => a.Users).WithOne(t => t.Test).HasForeignKey(x => x.TestId);
            entityBuilder.HasMany(a => a.Sections).WithOne(t => t.Test).HasForeignKey(x => x.TestId);
        }
    }
}