using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Perseo.Core;
using Microsoft.EntityFrameworkCore;

namespace Perseo.Infraestructure
{
    public class TrainingTestSectionFieldMap
    {
        public TrainingTestSectionFieldMap(EntityTypeBuilder<TrainingTestSectionField> entityBuilder)
        {
            entityBuilder.HasOne(a => a.Section).WithMany(t => t.Fields).HasForeignKey(t => t.SectionId);
            entityBuilder.HasMany(a => a.Options).WithOne(t => t.Field).HasForeignKey(t => t.FieldId);
        }
    }
}