using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Perseo.Core;
using Microsoft.EntityFrameworkCore;

namespace Perseo.Infraestructure
{  
    public class TrainingTestUserTakeSectionFieldOptionMap
    {
        public TrainingTestUserTakeSectionFieldOptionMap(EntityTypeBuilder<TrainingTestUserTakeSectionFieldOption> entityBuilder)
        {
            entityBuilder.HasOne(a => a.Field).WithMany(x=>x.Options).HasForeignKey(t => t.FieldId);
            entityBuilder.HasOne(a => a.TestOption).WithMany().HasForeignKey(t => t.TestOptionId);
        }

    }  
}  