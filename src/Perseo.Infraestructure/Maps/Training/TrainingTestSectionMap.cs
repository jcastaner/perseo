using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Perseo.Core;
using Microsoft.EntityFrameworkCore;

namespace Perseo.Infraestructure
{
    public class TrainingTestSectionMap
    {
        public TrainingTestSectionMap(EntityTypeBuilder<TrainingTestSection> entityBuilder)
        {
            entityBuilder.HasOne(a => a.Test).WithMany(t => t.Sections).HasForeignKey(t => t.TestId);
            entityBuilder.HasMany(a => a.Fields).WithOne(t => t.Section).HasForeignKey(t => t.SectionId);
        }
    }
}