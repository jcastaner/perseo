using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Perseo.Core;
using Microsoft.EntityFrameworkCore;

namespace Perseo.Infraestructure
{
    public class TrainingTestUserTakeMap
    {
        public TrainingTestUserTakeMap(EntityTypeBuilder<TrainingTestUserTake> entityBuilder)
        {
//            entityBuilder.HasOne(a => a.Trainer).WithMany().HasForeignKey(t => t.TrainerId);
            entityBuilder.HasOne(a => a.TestUser).WithMany(x=>x.Takes).HasForeignKey(t => t.TestUserId);
            entityBuilder.HasMany(a => a.Sections).WithOne(x=>x.Take);
        }
    }
}