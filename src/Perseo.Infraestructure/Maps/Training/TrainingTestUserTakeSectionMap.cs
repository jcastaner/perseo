using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Perseo.Core;
using Microsoft.EntityFrameworkCore;

namespace Perseo.Infraestructure
{  
    public class TrainingTestUserTakeSectionMap
    {
        public TrainingTestUserTakeSectionMap(EntityTypeBuilder<TrainingTestUserTakeSection> entityBuilder)
        {
            entityBuilder.HasMany(a => a.Fields).WithOne(x => x.Section);
            entityBuilder.HasOne(a => a.TestSection).WithMany().HasForeignKey(t => t.TestSectionId);
        }

    }  
}  