using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Perseo.Core;
 
namespace Perseo.Infraestructure
{
    public class TrainingUserMap
    {
        public TrainingUserMap(EntityTypeBuilder<TrainingUser> entityBuilder)
        {
            entityBuilder.HasOne(u => u.User).WithMany(s => s.TrainingUser).HasForeignKey(x => x.UserId);
            entityBuilder.HasOne(u => u.Training).WithMany(s => s.Users).HasForeignKey(x => x.TrainingId);
        }
    }
}