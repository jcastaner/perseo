using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Perseo.Core;
using Microsoft.EntityFrameworkCore;

namespace Perseo.Infraestructure
{
    public class TrainingTestOwnerMap
    {
        public TrainingTestOwnerMap(EntityTypeBuilder<TrainingTestOwner> entityBuilder)
        {
            entityBuilder.HasOne(a => a.Test).WithMany(t => t.Owners).HasForeignKey(t => t.TestId);
            entityBuilder.HasOne(a => a.User).WithMany().HasForeignKey(t => t.UserId);
        }
    }
}