using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Perseo.Core;
using Microsoft.EntityFrameworkCore;

namespace Perseo.Infraestructure
{
    public class TrainingTestUserMap
    {
        public TrainingTestUserMap(EntityTypeBuilder<TrainingTestUser> entityBuilder)
        {
            entityBuilder.HasOne(a => a.Test).WithMany(t => t.Users).HasForeignKey(t => t.TestId);
            entityBuilder.HasOne(a => a.User).WithMany().HasForeignKey(t => t.UserId);
            entityBuilder.HasMany(a => a.Takes);
        }
    }
}