using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Perseo.Core;
using Microsoft.EntityFrameworkCore;

namespace Perseo.Infraestructure
{  
    public class TrainingTestUserTakeSectionFieldMap 
    {
        public TrainingTestUserTakeSectionFieldMap(EntityTypeBuilder<TrainingTestUserTakeSectionField> entityBuilder)
        {
            entityBuilder.HasMany(a => a.Options).WithOne(x => x.Field);
            entityBuilder.HasOne(a => a.Section).WithMany(x => x.Fields).HasForeignKey(t => t.SectionId);
            entityBuilder.HasOne(a => a.TestField).WithMany().HasForeignKey(t => t.TestFieldId);
        }

    }  
}  