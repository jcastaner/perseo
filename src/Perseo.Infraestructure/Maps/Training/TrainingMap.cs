using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Perseo.Core;
 
namespace Perseo.Infraestructure
{
    public class TrainingMap
    {
        public TrainingMap(EntityTypeBuilder<Training> entityBuilder)
        {
            entityBuilder.HasOne(t => t.Area).WithMany().HasForeignKey(t => t.AreaId);

            entityBuilder.HasMany(u => u.Users).WithOne(t => t.Training).HasForeignKey(t => t.TrainingId);
            entityBuilder.HasMany(e => e.Tests).WithOne(t => t.Training).HasForeignKey(t => t.TrainingId);
        }
    }
}