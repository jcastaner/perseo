using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Perseo.Core;
using Microsoft.EntityFrameworkCore;

namespace Perseo.Infraestructure
{
    public class AreaCodeMap
    {
        public AreaCodeMap(EntityTypeBuilder<AreaCode> entityBuilder)
        {
            entityBuilder.Property(a => a.Name).IsRequired();
            entityBuilder.Property(a => a.Description);
            entityBuilder.HasOne(a => a.Area).WithMany(t => t.AreaCode).HasForeignKey(t => t.AreaId);
        }
    }
}