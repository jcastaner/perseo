using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Perseo.Core;
using Microsoft.EntityFrameworkCore;

namespace Perseo.Infraestructure
{
    public class AreaMap
    {
        public AreaMap(EntityTypeBuilder<Area> entityBuilder)
        {
            entityBuilder.Property(a => a.Name).IsRequired(); 
            entityBuilder.Property(t => t.Type).HasConversion<string>();

            entityBuilder.Ignore(u => u.Owner);
            entityBuilder.Ignore(u => u.Code);
            entityBuilder.Ignore(u => u.Wbs);
            entityBuilder.Ignore(u => u.FlatChildren);
            

            entityBuilder.HasOne(a => a.Parent).WithMany(a => a.Children).HasForeignKey(a => a.ParentId);
            entityBuilder.HasMany(a => a.Children).WithOne(t => t.Parent);
            entityBuilder.HasMany(a => a.Job).WithOne(t => t.Area).HasForeignKey(t => t.AreaId);
            entityBuilder.HasMany(a => a.UserArea).WithOne(t => t.Area).HasForeignKey(t => t.AreaId);
            entityBuilder.HasMany(a => a.AreaCode).WithOne(t => t.Area).HasForeignKey(t => t.AreaId);
            entityBuilder.HasMany(a => a.AreaCodeWbs).WithOne(t => t.Area).HasForeignKey(t => t.AreaId);
            entityBuilder.HasMany(a => a.Quality).WithOne(t => t.Area).HasForeignKey(t => t.AreaId);

            /*
            entityBuilder.HasData(

                new Area { Id = 4, Name = "Producci�n", Description = "Producci�n", Active = true, Type = AreaType.OrganizationalArea, ParentId = 3 },
                new Area { Id = 5, Name = "Transverales", Description = "Transverales", Active = true, Type = AreaType.OrganizationalArea, ParentId = 3 },
               
                new Area { Id = 6, Name = "Servicios Financieros I", Description = "Servicios Financieros I", Active = true, Type = AreaType.OrganizationalArea, ParentId = 4 },
                new Area { Id = 7, Name = "Servicios Financieros II", Description = "Servicios Financieros II", Active = true, Type = AreaType.OrganizationalArea, ParentId = 4 },
                new Area { Id = 8, Name = "Servicios Jur�dicos", Description = "Servicios Jur�dicos", Active = true, Type = AreaType.OrganizationalArea, ParentId = 4 },
                new Area { Id = 9, Name = "Activos Inmobiliarios", Description = "Activos Inmobiliarios", Active = true, Type = AreaType.OrganizationalArea, ParentId = 4 },
                new Area { Id = 10, Name = "Hipotecario", Description = "Hipotecario", Active = true, Type = AreaType.OrganizationalArea, ParentId = 4 },
                new Area { Id = 11, Name = "CRM", Description = "CRM", Active = true, Type = AreaType.OrganizationalArea, ParentId = 4 },
                new Area { Id = 12, Name = "Energ�a", Description = "Energ�a", Active = true, Type = AreaType.OrganizationalArea, ParentId = 4 },
                new Area { Id = 13, Name = "Telecomunicaciones", Description = "Telecomunicaciones", Active = true, Type = AreaType.OrganizationalArea, ParentId = 4 },
                new Area { Id = 14, Name = "Administraciones P�blicas", Description = "Administraciones P�blicas", Active = true, Type = AreaType.OrganizationalArea, ParentId = 4 },
                new Area { Id = 15, Name = "Servicios Documentales", Description = "Servicios Documentales", Active = true, Type = AreaType.OrganizationalArea, ParentId = 4 },
                
                new Area { Id = 16, Name = "Recursos Humanos", Description = "Recursos Humanos", Active = true, Type = AreaType.OrganizationalArea, ParentId = 5 },
                new Area { Id = 17, Name = "Desarrollo de Negocio", Description = "Desarrollo de Negocio", Active = true, Type = AreaType.OrganizationalArea, ParentId = 5 },
                new Area { Id = 18, Name = "Ingenier�a de Operaciones", Description = "Ingenier�a de Operaciones", Active = true, Type = AreaType.OrganizationalArea, ParentId = 5 },
                new Area { Id = 19, Name = "Tecnolog�a", Description = "Tecnolog�a", Active = true, Type = AreaType.OrganizationalArea, ParentId = 5 },

                new Area { Id = 20, Name = "CaixaBank", Description = "CaixaBank", Active = true, Type = AreaType.OrganizationalArea, ParentId = 7 },
                new Area { Id = 21, Name = "Banc Sabadell", Description = "Banc Sabadell", Active = true, Type = AreaType.OrganizationalArea, ParentId = 7 },

                new Area { Id = 22, Name = "Medios de Pago", Description = "Medios de Pago", Active = true, Type = AreaType.Project, ParentId = 20 },
                new Area { Id = 23, Name = "Card Factory", Description = "Card Factory", Active = true, Type = AreaType.ProductiveArea, ParentId = 22 }
                );
                */
        }
    }
}