using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Perseo.Core;
using Microsoft.EntityFrameworkCore;

namespace Perseo.Infraestructure
{
    public class AreaCodeWbsMap
    {
        public AreaCodeWbsMap(EntityTypeBuilder<AreaCodeWbs> entityBuilder)
        {
            entityBuilder.Property(a => a.Name).IsRequired();
            entityBuilder.Property(a => a.Description);
            entityBuilder.Ignore(u => u.FlatChildren);
            entityBuilder.HasMany(a => a.Children).WithOne(t => t.Parent);
            entityBuilder.HasOne(a => a.Parent).WithMany(a => a.Children).HasForeignKey(a => a.ParentId);
            entityBuilder.HasOne(u => u.AreaCode).WithMany(s => s.AreaCodeWbs).HasForeignKey(p => p.AreaCodeId);
            entityBuilder.HasOne(a => a.Area).WithMany(a => a.AreaCodeWbs).HasForeignKey(a => a.AreaId);

        }
    }
}