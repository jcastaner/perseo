﻿using Perseo.Core;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;

namespace Perseo.Infraestructure
{
    public class RoleRepository : Repository<Role>, IRoleRepository, IRepository<Role>
    {
        public RoleRepository(ApplicationContext context, IConfiguration configuration) : base(context, configuration) { }

        public IEnumerable<Role> All()
        {
            return Query<Role>(
                sql: "SELECT * FROM role"
            );
        }

        public Role FindByName(string roleName)
        {
            return QuerySingleOrDefault<Role>(
                sql: "SELECT * FROM role WHERE name = @roleName",
                param: new { roleName }
            );
        }
    }
}
