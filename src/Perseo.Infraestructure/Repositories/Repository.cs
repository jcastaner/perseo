using Perseo.Core;
using Dapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Dynamic;

namespace Perseo.Infraestructure
{
    public class Repository<T> : IRepository<T> where T : Entity
    {
        protected IDbConnection Connection;
        protected readonly ApplicationContext context;
        protected DbSet<T> entities;
        readonly string errorMessage = string.Empty;
 
        public Repository(ApplicationContext context, IConfiguration configuration)
        {
            Connection = new MySqlConnection(configuration.GetValue<string>("Data:ConnectionString"));
            //Connection = new MySqlConnection("Server=localhost;Database=Bpo;User=Bpo;Password=2018Bpo.opB8102;");
            DefaultTypeMap.MatchNamesWithUnderscores = true;
            this.context = context;
            entities = context.Set<T>();
        }

        // ENTITY FRAMEWORK

        public T Find(params object[] key)
        {
            return entities.Find(key);
        }

        public T Get(params object[] key)
        {
            return entities.Find(key);
        }

        //public T GetOne(System.Linq.Expressions.Expression<Func<T, bool>> where)
        //{
        //    return entities.FirstOrDefault(where);
        //}

        public T GetOne(System.Linq.Expressions.Expression<Func<T, bool>> where, string include = null)
        {
            if (include == null) return entities.FirstOrDefault(where);

            try
            {
                string[] multi = include.Split(new Char[] { ',', ';', ' ' });
                var query = entities.Where(where).AsQueryable();
                foreach (string item in multi)
                {
                    query = query.Include(item);
                }
                return query.FirstOrDefault();
            }
            catch (Exception)
            {
                return null;
            }
        }

        public IEnumerable<T> GetMany(System.Linq.Expressions.Expression<Func<T, bool>> where = null, string order = null, bool tracking = true)
        {
            if (where != null)
            {
                if (order != null)
                {
                    if (tracking)
                    {
                        return entities.Where(where).OrderBy(order).AsEnumerable();
                    }
                    else
                    {
                        return entities.Where(where).OrderBy(order).AsNoTracking().AsEnumerable();
                    }
                }
                else
                {
                    if (tracking)
                    {
                        return entities.Where(where).AsEnumerable();
                    }
                    else
                    {
                        return entities.Where(where).AsNoTracking().AsEnumerable();
                    }
                }
            }
            else
            {
                if (order != null)
                {
                    if (tracking)
                    {
                        return entities.OrderBy(order).AsEnumerable();
                    }
                    else
                    {
                        return entities.OrderBy(order).AsNoTracking().AsEnumerable();
                    }
                }
                else
                {
                    if (tracking)
                    {
                        return entities.AsEnumerable();
                    }
                    else
                    {
                        return entities.AsNoTracking().AsEnumerable();
                    }
                }
            }
        }

        public PagedResult<T> GetManyPaged(
           System.Linq.Expressions.Expression<Func<T, bool>> where = null,
           string order = null, int? page = null, int? pageSize = null)
        {

            PagedResult<T> result = new PagedResult<T>
            {
                Results = new HashSet<T>()
            };

            IQueryable<T> query;

            if (where != null)
            {
                if (order != null)
                {
                    query = entities.Where(where).OrderBy(order);
                }
                else
                {
                    query = entities.Where(where);
                }
            }
            else
            {
                if (order != null)
                {
                    query = entities.OrderBy(order);
                }
                else
                {
                    query = entities;
                }

            }

            result.RowCount = query.Count();

            bool paginate = result.RowCount > PagedResultBase.MINFORPAGINATION;

            page = page ?? 1;
            pageSize = pageSize ?? PagedResultBase.DEFAULTPAGESIZE;

            result.CurrentPage = page.Value;
            result.PageSize = pageSize.Value;

            var pageCount = (double)result.RowCount / pageSize;
            result.PageCount = (int)Math.Ceiling(pageCount.Value);

            var skip = (page.Value - 1) * pageSize.Value;

            result.Results = paginate ? query.Skip(skip).Take(pageSize.Value).AsEnumerable() : query.AsEnumerable();

            return result;
        }

        public PagedResult<Q> QueryPaged<Q>(
            System.Linq.Expressions.Expression<Func<T, Q>> select,
            System.Linq.Expressions.Expression<Func<T, bool>> where = null,
            System.Linq.Expressions.Expression<Func<Q, bool>> filter = null,
            string order = null, int? page = null, int? pageSize = null)
        {

            PagedResult<Q> result = new PagedResult<Q>
            {
                Results = new HashSet<Q>()
            };

            IQueryable<Q> query; ;

            if (where != null)
            {
                if (order != null)
                {
                    if (filter != null)
                    {
                        query = entities.Where(where).Select(select).Where(filter).OrderBy(order);
                    }
                    else
                    {
                        query = entities.Where(where).Select(select).OrderBy(order);
                    }
                }
                else
                {
                    if (filter != null)
                    {
                        query = entities.Where(where).Select(select).Where(filter);
                    }
                    else
                    {
                        query = entities.Where(where).Select(select);
                    }
                }
            }
            else
            {
                if (order != null)
                {
                    if (filter != null)
                    {
                        query = entities.Select(select).Where(filter).OrderBy(order);
                    }
                    else
                    {
                        query = entities.Select(select).OrderBy(order);
                    }
                }
                else
                {
                    if (filter != null)
                    {
                        query = entities.Select(select).Where(filter);
                    }
                    else
                    {
                        query = entities.Select(select);
                    }
                }

            }

            result.RowCount = query.Count();

            bool paginate = result.RowCount > PagedResultBase.MINFORPAGINATION;

            page = page ?? 1;
            pageSize = pageSize ?? PagedResultBase.DEFAULTPAGESIZE;

            result.CurrentPage = page.Value;
            result.PageSize = pageSize.Value;

            var pageCount = (double)result.RowCount / pageSize;
            result.PageCount = (int)Math.Ceiling(pageCount.Value);

            var skip = (page.Value - 1) * pageSize.Value;

            result.Results = paginate ? query.Skip(skip).Take(pageSize.Value).AsEnumerable() : query.AsEnumerable();

            return result;
        }

        public int Insert(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }
            entities.Add(entity);
            return context.SaveChanges();
        }

        public int Add(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }
            entities.Add(entity);
            return context.SaveChanges();
        }

        public int AddRange(IEnumerable<T> entityList)
        {
            if (entityList == null)
            {
                throw new ArgumentNullException("entity");
            }
            entities.AddRange(entityList);
            return context.SaveChanges();
        }

        public int Update(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }
            entities.Update(entity);
            return context.SaveChanges();
        }

        public int UpdateRange(IEnumerable<T> entityList)
        {
            if (entityList == null)
            {
                throw new ArgumentNullException("entity");
            }
            entities.UpdateRange(entityList);
            return context.SaveChanges();
        }

        public int Delete(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }
            entities.Remove(entity);
            return context.SaveChanges();
        }

        public int Delete(params object[] key)
        {
            entities.Remove(entities.Find(key));
            return context.SaveChanges();
        }

        public int DeleteRange(IEnumerable<T> entityList)
        {
            if (entityList == null)
            {
                throw new ArgumentNullException("entity");
            }
            entities.RemoveRange(entityList);
            return context.SaveChanges();
        }

        public int DeleteRange(IEnumerable<int> keyList)
        {
            if (keyList == null)
            {
                throw new ArgumentNullException("entity");
            }
            entities.RemoveRange(GetMany(x=> keyList.Contains(x.Id)));
            return context.SaveChanges();
        }

        public int Remove(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }
            entities.Remove(entity);
            return context.SaveChanges();
        }

        public int RemoveRange(IEnumerable<T> entityList)
        {
            if (entityList == null)
            {
                throw new ArgumentNullException("entity");
            }
            entities.RemoveRange(entityList);
            return context.SaveChanges();
        }

        public int SaveChanges()
        {
            return context.SaveChanges();
        }

        public void Reload(T entity)
        {
            context.Entry(entity).Reload();
        }

        public void Attach(T entity)
        {
            entities.Attach(entity);
        }

        public void LoadProperty(T entity, System.Linq.Expressions.Expression<Func<T, Object>> related)
        {
            context.Entry(entity).Reference(related).Load();
        }

        // DAPPER

        protected Q ExecuteScalar<Q>(string sql, object param)
        {
            return Connection.ExecuteScalar<Q>(sql, param);
        }

        protected Q QuerySingleOrDefault<Q>(string sql, object param)
        {
            return Connection.QuerySingleOrDefault<Q>(sql, param);
        }

        protected IEnumerable<Q> Query<Q>(string sql, object param = null)
        {
            return Connection.Query<Q>(sql, param);
        }

        protected void Execute(string sql, object param)
        {
            Connection.Execute(sql, param);
        }

    }
}