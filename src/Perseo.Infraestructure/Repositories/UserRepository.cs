﻿using Perseo.Core;
using Dapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Perseo.Infraestructure
{
    public class UserRepository : Repository<User>, IUserRepository, IRepository<User>
    {
        public UserRepository(ApplicationContext context, IConfiguration configuration) : base(context, configuration) { }

        public IEnumerable<User> GetAllWithAreas()
        {
            return entities.Include(x => x.UserArea)
                    .ThenInclude(a => a.Area)
                    .AsEnumerable();
        }

        public User Get(string UserName)
        {
            return entities.SingleOrDefault(x => x.NormalizedUserName == UserName.ToUpper());
        }

        public User GetFull(int id)
        {
            return entities
                    .Include(x => x.UserContract)
                    .Include(x => x.UserOperational)
                    .Include(x => x.UserSalary)
                    .Include(x => x.UserWorkConditions)
                    .Include(x => x.UserPosition)
                    .Include(x => x.UserCategory).ThenInclude(a => a.Category)
                    .Include(x => x.UserCollective).ThenInclude(a => a.Collective)
                    .Include(x => x.UserOffice).ThenInclude(a => a.Office)
                    .Include(x => x.UserRate).ThenInclude(a => a.Rate)
                    .Include(x => x.UserShift).ThenInclude(a => a.Shift)
                    .Include(x => x.UserRole).ThenInclude(a => a.Role)
                    .Include(x => x.UserArea).ThenInclude(a => a.Area).ThenInclude(a => a.Job)
                    .Include(x => x.UserPasswordHistory)
                    .SingleOrDefault(s => s.Id == id);
        }

        public IEnumerable<User> GetAllReadOnly(){

            return entities.AsNoTracking().AsEnumerable();
        }

        public User GetWithAreas(int id)
        {
            return entities.Include(x => x.UserArea)
                    .ThenInclude(a => a.Area)
                    .SingleOrDefault(s => s.Id == id);
        }

        public User GetWithAreasAndJobs(int id)
        {
            
            return entities.Include(x => x.UserArea)
                    .ThenInclude(a => a.Area)
                        .ThenInclude(a => a.Job)
                    .SingleOrDefault(s => s.Id == id);
        }

        public User GetAuthUser(int id)
        {
            return entities
                    .Include(x => x.UserRole)
                        .ThenInclude(a => a.Role)
                    .Include(x => x.UserArea)
                        .ThenInclude(a => a.Area)
                            .ThenInclude(a => a.Job)
                        .SingleOrDefault(s => s.Id == id);
        }

        public User GetAuthUser(string userName)
        {
            return entities
                    .Include(x => x.UserRole)
                        .ThenInclude(a => a.Role)
                    .Include(x => x.UserArea)
                        .ThenInclude(a => a.Area)
                            .ThenInclude(a => a.Job)
                        .SingleOrDefault(s => s.NormalizedUserName == userName.ToUpper());
        }

        public IEnumerable<User> GetAllUserSchedule(int year, int month, List<int> areasIds, bool? Active = null, List<int> officeIds = null)
        {
            string ActiveSql = !Active.HasValue ? " " : Active.Value ? " AND u.active = TRUE " : " AND u.active = FALSE ";
            string OfficeSql = officeIds != null && officeIds.Count() > 0 ? (officeIds.Any(i => i.Equals(0)) ? " AND ( o.office_id IN @officeIds OR o.office_id IS NULL) " : " AND o.office_id IN @officeIds " ) : " ";

            var userDictionary = new Dictionary<int, User>();
            return Connection.Query<User, ScheduleDate, User>(
                    sql: "SELECT u.*, s.* FROM user AS u " +
                    "LEFT JOIN user_area AS a ON a.user_id = u.id " +
                    "LEFT JOIN user_office AS o ON o.user_id = u.id " +
                    "LEFT JOIN schedule_date AS s ON s.user_id = u.id AND `s`.`year`= @year AND `s`.`month`= @month " +
                    "WHERE a.owner != true AND a.authorized != true AND a.end_date IS NULL " + ActiveSql + OfficeSql +
                    "AND a.area_id IN @areasIds AND (NOT o.active = false OR o.active IS NULL)",
                    map: (user, scheduledate) =>
                    {
                        if (!userDictionary.TryGetValue(user.Id, out User userEntry))
                        {
                            userEntry = user;
                            userEntry.ScheduleDate = new List<ScheduleDate>();
                            userDictionary.Add(userEntry.Id, userEntry);
                        }
                        userEntry.ScheduleDate.Add(scheduledate);
                        return userEntry;
                    },
                    splitOn: "id",
                    param: new { year, month, areasIds, officeIds }
                ).Distinct().ToList(); 
        }

        public User GetUserSchedule(int year, int userId)
        {
            
            var userDictionary = new Dictionary<int, User>();
            return Connection.Query<User, ScheduleDate, User>(
                sql: "SELECT u.*, s.* FROM user AS u " +
                "LEFT JOIN schedule_date AS s ON s.user_id = u.id " +
                "WHERE u.Id = @userId AND `s`.`year`= @year ",
                map: (user, scheduledate) =>
                {
                    if (!userDictionary.TryGetValue(user.Id, out User userEntry))
                    {
                        userEntry = user;
                        userEntry.ScheduleDate = new List<ScheduleDate>();
                        userDictionary.Add(userEntry.Id, userEntry);
                    }
                    userEntry.ScheduleDate.Add(scheduledate);
                    return userEntry;

                },
                splitOn: "id",
                param: new { userId, year }
            ).FirstOrDefault();
        }

        public IEnumerable<User> GetUsersFromAreas(IEnumerable<int> AreaIds)
        {
            return entities.Where(u => u.UserArea.Any(ua => ua.Active && AreaIds.Contains(ua.AreaId)));
        }

        public IEnumerable<UserListNode> GetUserListFromAreas(IEnumerable<int> AreaIds, bool? Active = null)
        {
            string ActiveSql = !Active.HasValue ? " " : Active.Value ? " AND u.active = TRUE " : " AND u.active = FALSE ";
            return Connection.Query<UserListNode>(
                    sql: "SELECT u.id, u.user_name, u.email, u.employee_id, u.first_name, u.main_surname, u.second_surname, " +
                    "a.name AS area, u.Avatar " +
                    "FROM user AS u " +
                    "LEFT JOIN user_area AS ua ON ua.user_id = u.id " +
                    "LEFT JOIN area AS a ON a.id = ua.area_id " +
                    "WHERE ua.owner != true AND ua.authorized != true AND ua.end_date IS NULL " + ActiveSql +
                    "AND ua.area_id IN @AreaIds",
                    param: new { AreaIds }
                ).ToList();
        }
         
        public IEnumerable<User> All()
        {
            return Query<User>(
                sql: "SELECT * FROM user"
            );
        }

        public User FindByNormalizedEmail(string normalizedEmail)
        {
            return QuerySingleOrDefault<User>(
                sql: "SELECT * FROM user WHERE normalized_email = @normalizedEmail",
                param: new { normalizedEmail }
            );
        }

        public User FindByNormalizedUserName(string normalizedUserName)
        {
            return QuerySingleOrDefault<User>(
                sql: "SELECT * FROM user WHERE normalized_user_name = @normalizedUserName",
                param: new { normalizedUserName }
            );
        }

        public void AddRole(int userId, string roleName)
        {
            Execute(
                sql: @"
                    INSERT INTO user_role(user_id, role_id)
                    SELECT TOP 1 @userId, id FROM role
                    WHERE normalized_name = @roleName",
                param: new { userId, roleName }
            );
        }

        public IEnumerable<string> GetRoleNamesByUserId(int userId)
        {
            return Query<string>(
                sql: @"
                    SELECT r.name
                    FROM user_role ur INNER JOIN
                        role r ON ur.role_id = r.id
                    WHERE ur.user_id = @userId
                ",
                param: new { userId }
            );
        }

        public IEnumerable<User> GetUsersByRoleName(string roleName)
        {
            return Query<User>(
                sql: @"
                    SELECT u.*
                    FROM user_role ur INNER JOIN
	                    role r ON ur.role_id = r.id INNER JOIN
	                    user u ON ur.user_id = u.id
                    WHERE r.normalized_name = @roleName
                ",
                param: new { roleName });
        }

        public void RemoveRole(int userId, string roleName)
        {
            Execute(
                sql: @"
                    DELETE ur
                    FROM user_role ur INNER JOIN
                        role r ON ur.role_id = r.id
                    WHERE r.normalized_name = @roleName
                ",
                param: new { userId, roleName }
            );
        }

        public bool ExistsUserName(string userName)
        {
            userName = userName.ToUpper();
            return ExecuteScalar<bool>(
               sql: "SELECT count(1) FROM user where normalized_user_name=@userName",
               param: new { userName }
           );
        }

        public bool ExistsEmail(string email)
        {
            email = email.ToUpper();
            return ExecuteScalar<bool>(
               sql: "SELECT count(1) FROM user where normalized_email=@email",
               param: new { email}
           );
        }

        public bool ExistsEmployeeId(int employeeId)
        {
            return ExecuteScalar<bool>(
               sql: "SELECT count(1) FROM user where employee_id=@employeeId",
               param: new { employeeId }
           );
        }

        public IEnumerable<User> GetSubordinates(int UserId, bool Direct = false, bool Owner = false, bool Strict = false, bool? Active = true )
        {
            string ActiveSql = !Active.HasValue ? " " : Active.Value ? " AND active = TRUE " : " AND active = FALSE ";
            string sql = "SELECT area_id FROM user_area WHERE user_id = @UserId " + ActiveSql;
            sql += Strict ? " AND owner = true;" : " AND (owner = true OR authorized = true); ";
            var ids = Query<int>(
                sql: sql,
                param: new { UserId }).ToList() as IList<int>;
            if (ids.Any())
            {
                string ownerString = Owner ? " AND user_area.owner = true " : " ";
                if (Direct)
                {
                    return Query<User>(
                        sql: "SELECT DISTINCT users.* " +
                        "FROM user_area WHERE user_area.area_id IN @ids ) " + ownerString + " AND user_area.end_date IS NULL " +
                        "JOIN user AS users ON user_area.user_id = users.id;",
                        param: new { ids }
                    );
                }
                else
                {
                    return Query<User>(
                        sql: "SELECT DISTINCT users.* " +
                        "FROM ( WITH RECURSIVE area_children (id, name, description, type, active, parent_id) " +
                        "AS ( SELECT area.id, area.name, area.description, area.type, area.active, area.parent_id " +
                        "FROM area WHERE id IN @ids " +
                        "UNION ALL SELECT area.id, area.name, area.description, area.type, area.active, area.parent_id " +
                        "FROM area_children, area WHERE area.parent_id = area_children.id ) " +
                        "SELECT id, name, description, type, active, parent_id FROM area_children ORDER BY id) AS areas " +
                        "LEFT JOIN user_area ON areas.id = user_area.area_id " + ownerString + " AND user_area.end_date IS NULL " +
                        "JOIN user AS users ON user_area.user_id = users.id;",
                        param: new { ids }
                    );
                }
                
            }
            return Enumerable.Empty<User>();
        }
    }
}
