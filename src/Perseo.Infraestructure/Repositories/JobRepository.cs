﻿using Perseo.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Perseo.Infraestructure
{
    public class JobRepository : Repository<Job>, IJobRepository, IRepository<Job>
    {

        public JobRepository(ApplicationContext context, IConfiguration configuration) : base(context, configuration) { }

        public IEnumerable<JobConfig> GetJobConfiguration(IList<int> jobsIds, int year, int month, int? week, int? day)
        {
            DateTime sd = new DateTime(year, month, 1, 0, 0, 0);
            DateTime ed = new DateTime(year, month, DateTime.DaysInMonth(year, month), 0, 0, 0);
            DbSet<JobConfig> JobConfigEntities = context.Set<JobConfig>();
            return JobConfigEntities
                    .Where( a => jobsIds.Contains(a.JobId) &&
                            (  a.StartDate <= sd && (a.EndDate == null || a.EndDate >= ed)  ) ||   ( sd <= a.StartDate && a.StartDate <= ed )
                          ).AsEnumerable();
        }

        public IEnumerable<JobSla> GetJobSla(IList<int> jobsIds, int year, int month, int? week, int? day)
        {
            DateTime sd = new DateTime(year, month, 1, 0, 0, 0);
            DateTime ed = new DateTime(year, month, DateTime.DaysInMonth(year, month), 0, 0, 0);
            DbSet<JobSla> JobSlaEntities = context.Set<JobSla>();
            return JobSlaEntities
                    .Where(a => jobsIds.Contains(a.JobId) &&
                           (a.StartDate <= sd && (a.EndDate == null || a.EndDate >= ed)) || (sd <= a.StartDate && a.StartDate <= ed)
                          ).AsEnumerable();
        }

        public IEnumerable<Job> GetJobsFromAreas(IEnumerable<Area> areas)
        {
            if (areas.Any())
            {
                List<int> ids = areas.Select(a => a.Id).ToList();
                return Query<Job>(
                    sql: "SELECT id, name, description, `type`, area_id, `default`, start_date, end_date " +
                    "FROM job WHERE area_id IN @ids",
                    param: new { ids }
                );
            }
            return Enumerable.Empty<Job>();
        }

        public IEnumerable<Job> GetGeneralJobs(int Year)
        {
            string year = Year + "-01-01";
            var productive = JobType.Productive.ToString();
            var general = JobType.General.ToString();
            var organizational = JobType.Organizational.ToString();
            var project = JobType.Project.ToString();
            return Query<Job>(
                sql: "SELECT id, name, description, `type`, area_id, `default`, start_date, end_date, acronym " +
                "FROM job WHERE `type` != @productive AND `type` != @productive AND `type` != @organizational  AND `type` != @general " +
                "AND `type` != @project  AND @year >= start_date AND ( @year <= end_date OR end_date IS NULL )",
                param: new { productive, organizational, general, project, year }
            );
        }

    }
}
