﻿using Perseo.Core;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Perseo.Infraestructure
{
    public class RequestRepository : Repository<Request>, IRequestRepository, IRepository<Request>
    {
        public RequestRepository(ApplicationContext context, IConfiguration configuration) : base(context, configuration) { }

        public PagedResult<RequestListNode> GetRequestsFromAreasPaged(List<int> AreaIds, List<RequestType> Types = null, List<RequestState> States = null, int? year = null, int? month = null, int? page = null, int? pageSize = null, string Search = null, string OrderField = null, string OrderDirection = "asc")
        {
            PagedResult<RequestListNode> PagedResult = new PagedResult<RequestListNode>();

            string TypeSql = Types != null ? " AND request.type IN @TypeNames " : " ";
            string StateSql = States != null ? " AND st.state IN @StateNames " : " ";
            List<string> TypeNames = Types != null ? Types.Select(x => x.ToString("G")).ToList() : new List<string>();
            List<string> StateNames = States != null ? States.Select(x => x.ToString("G")).ToList() : new List<string>();

            string DateSql = year != null && month != null ? " AND MONTH(request.request_date) = " + month + " AND YEAR(request.request_date) = " + year + " " : " ";
            
            string SearchSql = Search != null ? @" AND ( request.description LIKE @Search 
                                OR request.summary LIKE @Search OR creator.first_name LIKE @Search
                                OR creator.main_surname LIKE @Search OR assigned.first_name LIKE @Search OR assigned.main_surname LIKE @Search
                                OR request.type LIKE @Search ) " : " ";

            Search = "%" + Search + "%";

            string OrderSql = OrderField != null ? $@" ORDER BY {OrderField} {OrderDirection} " : $@" ORDER BY creation_date DESC ";

            var count = QuerySingleOrDefault<int>(
                sql: $@"SELECT COUNT(DISTINCT request.id) 
                    FROM request 
                    LEFT JOIN user as creator ON creator.id = request.user_id 
                    LEFT JOIN user as assigned ON assigned.id = request.assigned_user_id 
                    INNER JOIN user_area ON user_area.user_id = creator.id 
                    AND user_area.owner IS FALSE AND user_area.end_date IS NULL AND user_area.area_id IN @AreaIds 
                    INNER JOIN  request_history as st ON st.request_id = request.id 
                    AND st.entry_date = (SELECT MAX(entry_date) FROM request_history WHERE request_id = request.id) {StateSql}
                    WHERE 1 {TypeSql} {DateSql}",
                param: new { AreaIds, TypeNames, StateNames }
            );

            int rows = pageSize ?? (count > PagedResult.MinForPagination ? PagedResult.DefaultPageSize : count);

            PagedResult.PageSize = pageSize ?? PagedResult.DefaultPageSize;
            PagedResult.CurrentPage = page ?? 1;
            PagedResult.RowCount = count;
            PagedResult.PageCount = (int)Math.Ceiling((double)count / PagedResult.PageSize);

            int offset = PagedResult.FirstRowOnPage - 1;

            PagedResult.Results = Query<RequestListNode>(
                sql: $@"SELECT request.id, request.active, request.type, request.request_date AS creation_date, 
                    request.priority, request.summary, request.description, creator.id AS user_id,  
                    CONCAT(creator.first_name, ' ', creator.main_surname) AS user_name, 
                    assigned.id AS assigned_user_id, CONCAT(assigned.first_name, ' ', assigned.main_surname) AS assigned_user_name, 
                    st.state, st.entry_date as last_change_date 
                    FROM request 
                    LEFT JOIN user as creator ON creator.id = request.user_id 
                    LEFT JOIN user as assigned ON assigned.id = request.assigned_user_id 
                    INNER JOIN user_area ON user_area.user_id = creator.id 
                    AND user_area.owner IS FALSE 
                    AND user_area.end_date IS NULL 
                    AND user_area.area_id IN @AreaIds 
                    INNER JOIN  request_history as st 
                    ON st.request_id = request.id 
                    AND st.entry_date = (SELECT MAX(entry_date) FROM request_history WHERE request_id = request.id) {StateSql}
                    WHERE 1 {TypeSql} {DateSql} {SearchSql} {OrderSql} LIMIT @offset, @rows ",

                param: new { AreaIds, TypeNames, StateNames, Search, OrderField, OrderDirection, offset, rows }
            );

            return PagedResult;

        }

        public IEnumerable<RequestListNode> GetRequestsFromAreas(List<int> AreaIds, List<RequestType> Types = null, List<RequestState> States = null)
        {
            string TypeSql = Types != null ? " AND request.type IN @TypeNames " : " ";
            string StateSql = States != null ? " AND st.state IN @StateNames " : " ";
            List<string> TypeNames = Types != null ? Types.Select(x => x.ToString("G")).ToList() : new List<string>();
            List<string> StateNames = States != null ? States.Select(x => x.ToString("G")).ToList() : new List<string>();
            if (AreaIds.Any())
            {
                return Query<RequestListNode>(
                    sql: "SELECT request.id, request.active, request.type, request.request_date AS creation_date, " +
                    "request.priority, request.summary, request.description, creator.id AS user_id, " +
                    "CONCAT(creator.first_name, ' ', creator.main_surname) AS user_name, " +
                    "assigned.id AS assigned_user_id, CONCAT(assigned.first_name, ' ', assigned.main_surname) AS assigned_user_name, " +
                    "st.state, st.entry_date as last_change_date " +
                    "FROM request " +
                    "LEFT JOIN user as creator ON creator.id = request.user_id " +
                    "LEFT JOIN user as assigned ON assigned.id = request.assigned_user_id " +
                    "INNER JOIN user_area ON user_area.user_id = creator.id " +
                    "AND user_area.owner IS FALSE " +
                    "AND user_area.end_date IS NULL " +
                    "AND user_area.area_id IN @AreaIds " +
                    "INNER JOIN  request_history as st " +
                    "ON st.request_id = request.id " +
                    "AND st.entry_date = (SELECT MAX(entry_date) FROM request_history WHERE request_id = request.id)" + StateSql + 
                    "WHERE 1 " + TypeSql + " ORDER BY creation_date DESC",
                    param: new { AreaIds, TypeNames, StateNames }
                );
            }
            return Enumerable.Empty<RequestListNode>();
        }

        public IEnumerable<RequestListNode> GetRequestsCreatedByUser(int UserId, List<RequestType> Types = null, List<RequestState> States = null)
        {
            string TypeSql = Types != null ? " AND request.type IN @TypeNames " : " ";
            string StateSql = States != null ? " AND st.state IN @StateNames " : " ";
            List<string> TypeNames = Types != null ? Types.Select(x => x.ToString("G")).ToList() : new List<string>();
            List<string> StateNames = States != null ? States.Select(x => x.ToString("G")).ToList() : new List<string>();
            return Query<RequestListNode>(
                   sql: "SELECT request.id, request.active, request.type, request.request_date AS creation_date, " +
                   "request.priority, request.summary, request.description, creator.id AS user_id, " +
                   "CONCAT(creator.first_name, ' ', creator.main_surname) AS user_name, " +
                   "assigned.id AS assigned_user_id, CONCAT(assigned.first_name, ' ', assigned.main_surname) AS assigned_user_name, " +
                   "st.state, st.entry_date as last_change_date " +
                   "FROM request " +
                   "LEFT JOIN user as creator ON creator.id = request.user_id " +
                   "LEFT JOIN user as assigned ON assigned.id = request.assigned_user_id " +
                   "INNER JOIN user_area ON user_area.user_id = creator.id " +
                   "AND user_area.owner IS FALSE " +
                   "AND user_area.end_date IS NULL " +
                   "INNER JOIN  request_history as st " +
                   "ON st.request_id = request.id " +
                   "AND st.entry_date = (SELECT MAX(entry_date) FROM request_history WHERE request_id = request.id)" + StateSql +
                   "WHERE creator.id = @UserId " + TypeSql + " ORDER BY creation_date DESC",
                   param: new { UserId, TypeNames, StateNames }
               );
        }

        public IEnumerable<RequestListNode> GetRequestsAssignedToUser(int UserId, List<RequestType> Types = null, List<RequestState> States = null)
        {
            string TypeSql = Types != null ? " AND request.type IN @TypeNames " : " ";
            string StateSql = States != null ? " AND st.state IN @StateNames " : " ";
            List<string> TypeNames = Types != null ? Types.Select(x => x.ToString("G")).ToList() : new List<string>();
            List<string> StateNames = States != null ? States.Select(x => x.ToString("G")).ToList() : new List<string>();
            return Query<RequestListNode>(
                sql: "SELECT request.id, request.active, request.type, request.request_date AS creation_date, " +
                "request.priority,  request.summary, request.description, creator.id AS user_id, " +
                "CONCAT(creator.first_name, ' ', creator.main_surname) AS user_name, " +
                "assigned.id AS assigned_user_id, CONCAT(assigned.first_name, ' ', assigned.main_surname) AS assigned_user_name, " +
                "st.state, st.entry_date as last_change_date " +
                "FROM request " +
                "LEFT JOIN user as creator ON creator.id = request.user_id " +
                "LEFT JOIN user as assigned ON assigned.id = request.assigned_user_id " +
                "INNER JOIN user_area ON user_area.user_id = creator.id " +
                "AND user_area.owner IS FALSE " +
                "AND user_area.end_date IS NULL " +
                "RIGHT JOIN " +
                "(SELECT rh.state, rh.entry_date, rh.request_id FROM request_history rh INNER JOIN (SELECT request_id, MAX(entry_date) AS MaxEntryDate FROM request_history GROUP BY request_id) grouped_rh ON rh.request_id = grouped_rh.request_id AND rh.entry_date = grouped_rh.MaxEntryDate) st " +
                "ON st.request_id = request.id " + StateSql +
                "WHERE assigned.id = @UserId " + TypeSql,
                param: new { UserId, TypeNames, StateNames }
            );
        }

        public IEnumerable<RequestHistoryListNode> GetRequestHistory(int id)
        {
            return Query<RequestHistoryListNode>(
                sql: "SELECT request_history.id, request_history.entry_date, request_history.comment, " +
                "request_history.state, CONCAT(user.first_name, ' ', user.main_surname) as user_name " +
                "FROM request_history " +
                "LEFT JOIN user ON user.id = request_history.user_id " +
                "WHERE request_history.request_id = @id",
                param: new { id, }
            );
        }

    }
}
