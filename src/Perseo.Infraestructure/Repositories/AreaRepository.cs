﻿using Perseo.Core;
using Dapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Perseo.Infraestructure
{
    public class AreaRepository : Repository<Area>, IAreaRepository, IRepository<Area>
    {

        public AreaRepository(ApplicationContext context, IConfiguration configuration) : base(context, configuration){ }

        public IEnumerable<Area> GetAncestors(int id)
        {
            return Query<Area>(
                sql: "WITH RECURSIVE area_ancestors (id, name, description, type, active, parent_id) " +
                "AS ( SELECT area.id, area.name, area.description, area.type, area.active, " +
                "area.parent_id FROM area WHERE id = @id " +
                "UNION ALL SELECT area.id, area.name, area.description, area.type, " +
                "area.active, area.parent_id FROM area_ancestors, area " +
                "WHERE area.id = area_ancestors.parent_id ) " +
                "SELECT id, name, description, type, active, parent_id FROM area_ancestors ORDER BY id;",
                param: new { id }
            );
        }

        public IEnumerable<Area> GetChildren(int id)
        {
            return entities.Where(a => a.Lineage.Contains("-" + id + "-"));
        }

        public IEnumerable<AreaListNode> GetTree(int id)
        {
            return Query<AreaListNode>(
                sql: "(SELECT areas.*, users.id AS owner_id, CONCAT(users.first_name, ' ', users.main_surname) AS owner_full_name, " +
                "users.avatar AS owner_avatar, area_code.name As code, area_code.id As code_id, " +
                "area_code_wbs.name As wbs, area_code_wbs.id As wbs_id " +
                "FROM ( WITH RECURSIVE area_ancestors (id, name, description, type, active, parent_id)  " +
                "AS ( SELECT area.id, area.name, area.description, area.type, area.active,  area.parent_id " +
                "FROM area WHERE id = @id " +
                "UNION ALL SELECT area.id, area.name, area.description, area.type,  " +
                "area.active, area.parent_id FROM area_ancestors, area  " +
                "WHERE area.id = area_ancestors.parent_id )  " +
                "SELECT id, name, description, type, active, parent_id FROM area_ancestors ORDER BY id) AS areas " +
                "LEFT JOIN area_code ON areas.id = area_code.area_id AND area_code.end_date IS NULL  " +
                "LEFT JOIN area_code_wbs ON areas.id = area_code_wbs.area_id AND area_code_wbs.end_date IS NULL  " +
                "LEFT JOIN user_area ON areas.id = user_area.area_id AND user_area.owner = TRUE AND user_area.end_date IS NULL  " +
                "LEFT JOIN user AS users ON user_area.user_id = users.id) " +
                "UNION " +
                "(SELECT areas2.*, users.id AS user_id, CONCAT(users.first_name, ' ', users.main_surname) AS user_full_name, " +
                "users.avatar AS owner_avatar, area_code.name As code, area_code.id As code_id, " +
                "area_code_wbs.name As wbs, area_code_wbs.id As wbs_id " +
                "FROM ( WITH RECURSIVE area_children (id, name, description, type, active, parent_id) " +
                "AS ( SELECT area.id, area.name, area.description, area.type, area.active, area.parent_id " +
                "FROM area WHERE id = @id " +
                "UNION ALL SELECT area.id, area.name, area.description, area.type,  " +
                "area.active, area.parent_id FROM area_children, area " +
                "WHERE area.parent_id = area_children.id ) " +
                "SELECT id, name, description, type, active, parent_id FROM area_children ORDER BY id) AS areas2 " +
                "LEFT JOIN area_code ON areas2.id = area_code.area_id AND area_code.end_date IS NULL  " +
                "LEFT JOIN area_code_wbs ON areas2.id = area_code_wbs.area_id AND area_code_wbs.end_date IS NULL  " +
                "LEFT JOIN user_area ON areas2.id = user_area.area_id AND user_area.owner = TRUE AND user_area.end_date IS NULL " +
                "LEFT JOIN user AS users ON user_area.user_id = users.id)",
                param: new { id }
            );
        }

        public IEnumerable<Area> GetAllChildren(IEnumerable<int> ids)
        {
            if (ids.Any())
            {
                return Query<Area>(
                    sql: "SELECT DISTINCT * FROM " +
                    "(WITH RECURSIVE area_children (id, name, description, type, active, parent_id) " +
                    "AS ( SELECT area.id, area.name, area.description, area.type, area.active, " +
                    "area.parent_id FROM area WHERE id IN @idS " +
                    "UNION ALL SELECT area.id, area.name, area.description, area.type, " +
                    "area.active, area.parent_id FROM area_children, area " +
                    "WHERE area.parent_id = area_children.id ) " +
                    "SELECT id, name, description, type, active, parent_id FROM area_children ORDER BY id) AS a " +
                    "WHERE 1; ",
                    param: new { ids }
                );
            }
            return Enumerable.Empty<Area>();
        }

        public IEnumerable<Area> GetOwnedAreas(User user)
        {
            int id = user.Id;
            var lineages = Query<string>(
              sql: @"SELECT area.lineage FROM user_area 
              JOIN area ON user_area.area_id = area.id
              WHERE user_id = @id AND (owner = true OR authorized = true) ",
              param: new { id }).ToList() as IList<string>;

            if (lineages.Any())
            {
                var lineagesql = " '" + String.Join("(.*)|", lineages) + "(.*)' ";

                return Query<Area>(
                    sql: $@"SELECT DISTINCT area.* FROM area WHERE lineage REGEXP {lineagesql}"
                );
            }
            return Enumerable.Empty<Area>();
        }

        public IEnumerable<AreaListNode> GetOwnedAreaList(User user, bool? Active = null, List<AreaType> Types = null)
        {
            string ActiveSql = !Active.HasValue ? " " : Active.Value ? " AND area.active = TRUE " : " AND area.active = FALSE ";
            string ActiveSql2 = !Active.HasValue ? " " : Active.Value ? " AND area_children.active = TRUE " : " AND area_children.active = FALSE ";
            List<string> TypeNames = Types != null ? Types.Select(x => x.ToString("G")).ToList() : new List<string>();
            string TypeSql = Types != null ? " AND area_children.type IN @TypeNames " : " ";
            int id = user.Id;
            var ids = Query<int>(
                sql: "SELECT area_id FROM user_area " +
                "JOIN area ON user_area.area_id = area.id " +
                "WHERE user_id = @id AND (owner = true OR authorized = true) " + ActiveSql,
                param: new { id }).ToList() as IList<int>;
            if (ids.Any())
            {
                //"users.avatar AS owner_avatar, area_code.name As code, area_code.id AS code_id, " +
                return Query<AreaListNode>(
                    sql: "SELECT DISTINCT areas.*, users.id AS owner_id, CONCAT(users.first_name, ' ', users.main_surname) AS owner_full_name, " +
                    "users.avatar AS owner_avatar, " +
                    "IFNULL(area_code.name, area_code_parent.name) As code, IFNULL(area_code.id, area_code_parent.id) AS code_id, " +
                    "area_code_wbs.name As wbs, area_code_wbs.id As wbs_id, " +
                    "(SELECT COUNT(*) FROM user_area WHERE user_area.area_id = areas.id " +
                    "AND user_area.end_date IS NULL AND user_area.owner = FALSE LIMIT 1) AS users FROM " +
                    "(WITH RECURSIVE area_children (id, name, description, type, active, parent_id) " +
                    "AS ( SELECT area.id, area.name, area.description, area.type, area.active, " +
                    "area.parent_id FROM area WHERE id IN @idS " +
                    "UNION ALL SELECT area.id, area.name, area.description, area.type, " +
                    "area.active, area.parent_id FROM area_children, area " +
                    "WHERE area.parent_id = area_children.id ) " +
                    "SELECT id, name, description, active, type, parent_id FROM area_children WHERE 1 " + TypeSql + ActiveSql2 + " ORDER BY id) AS areas " +
                    "LEFT JOIN area_code ON areas.id = area_code.area_id AND area_code.end_date IS NULL  " +
                    "LEFT JOIN area_code_wbs ON areas.id = area_code_wbs.area_id AND area_code_wbs.end_date IS NULL  " +
                    "LEFT JOIN area_code AS area_code_parent ON area_code_wbs.area_code_id = area_code_parent.id " +
                    "LEFT JOIN user_area ON areas.id = user_area.area_id AND user_area.owner = TRUE AND user_area.end_date IS NULL  " +
                    "LEFT JOIN user AS users ON user_area.user_id = users.id ",
                    param: new { ids, TypeNames }
                );
            }
            return Enumerable.Empty<AreaListNode>();
        }

        public IEnumerable<Area> GetAreasToSchedule(List<int> ids)
        {
            return entities
                    .Include(a => a.Job)
                    .Include(a => a.UserArea)
                        .ThenInclude(ua => ua.User)
                            .ThenInclude(u => u.UserContract)
                    .Include(a => a.UserArea)
                        .ThenInclude(ua => ua.User)
                            .ThenInclude(u => u.UserOffice)
                    .Include(a => a.UserArea)
                        .ThenInclude(ua => ua.User)
                            .ThenInclude(u => u.ScheduleDate)
                                .ThenInclude(sd => sd.ScheduleDateTerm)
                                    .ThenInclude(sdt => sdt.ScheduleDateTermJob)
                                        .ThenInclude(sdtj => sdtj.ScheduleDateTerm)
                    .Where(a => ids.Contains(a.Id));
        }

        public PagedResult<AreaListNode> GetOwnedAreaListPaged(User user, bool? Active = null, List<AreaType> Types = null, int? page = null, int? pageSize = null, 
            string Search = null, string OrderField = null, string OrderDirection = "asc")
        {
            PagedResult<AreaListNode> PagedResult = new PagedResult<AreaListNode>();

            string ActiveSql = !Active.HasValue ? " " : Active.Value ? " AND area.active = TRUE " : " AND area.active = FALSE ";

            string SearchSql = Search != null ? @" AND ( area.name LIKE @Search OR area.description LIKE @Search OR area.active LIKE @Search
                                OR area.type LIKE @Search OR users.first_name LIKE @Search OR users.main_surname LIKE @Search
                                OR area_code.name LIKE @Search OR area_code_parent.name LIKE @Search OR area_code_wbs.name LIKE @Search ) " : " ";

            Search = "%" + Search + "%";

            string OrderSql = OrderField != null ? $@" ORDER BY {OrderField} {OrderDirection} " : $@" ORDER BY lineage ASC ";

            List<string> TypeNames = Types != null ? Types.Select(x => x.ToString("G")).ToList() : new List<string>();

            string TypeSql = Types != null ? " AND area.type IN @TypeNames " : " ";

            int id = user.Id;

            var lineages = Query<string>(
                sql: "SELECT area.lineage FROM user_area JOIN area ON user_area.area_id = area.id WHERE user_id = @id AND (owner = true OR authorized = true) " + ActiveSql,
                param: new { id }).ToList() as IList<string>;

            if (lineages.Any())
            {
                var lineagesql = " '" + String.Join("(.*)|", lineages) + "(.*)' ";

                var count = QuerySingleOrDefault<int>(
                    sql: $@"SELECT COUNT(DISTINCT area.id) FROM area WHERE lineage REGEXP {lineagesql} {TypeSql} {ActiveSql} ",
                    param: new { lineagesql, TypeNames }
                );

                int rows = pageSize ?? (count > PagedResult.MinForPagination ? PagedResult.DefaultPageSize : count);

                PagedResult.PageSize = pageSize ?? PagedResult.DefaultPageSize;
                PagedResult.CurrentPage = page ?? 1;
                PagedResult.RowCount = count;
                PagedResult.PageCount = (int)Math.Ceiling((double)count / PagedResult.PageSize);

                int offset = PagedResult.FirstRowOnPage-1;

                PagedResult.Results = Query<AreaListNode>(
                    sql: $@"SELECT area.id, area.name, area.description, area.active, area.type, area.parent_id,
                    users.id AS owner_id, 
                    CONCAT(users.first_name, ' ', users.main_surname) AS owner_full_name,
                    users.avatar AS owner_avatar, 
                    IFNULL(area_code.name, area_code_parent.name) As code, 
                    IFNULL(area_code.id, area_code_parent.id) AS code_id, 
                    area_code_wbs.name As wbs, 
                    area_code_wbs.id As wbs_id 
                    FROM area
                    LEFT JOIN area_code ON area.id = area_code.area_id AND area_code.end_date IS NULL   
                    LEFT JOIN area_code_wbs ON area.id = area_code_wbs.area_id AND area_code_wbs.end_date IS NULL 
                    LEFT JOIN area_code AS area_code_parent ON area_code_wbs.area_code_id = area_code_parent.id 
                    LEFT JOIN user_area ON area.id = user_area.area_id AND user_area.owner = TRUE AND user_area.end_date IS NULL 
                    LEFT JOIN user AS users ON user_area.user_id = users.id 
                    WHERE lineage REGEXP {lineagesql} {TypeSql} {ActiveSql} {SearchSql} {OrderSql} LIMIT @offset, @rows ",
                    param: new { TypeNames, Search, OrderField, OrderDirection, offset, rows }
                );
            }
            return PagedResult;

        }

    }
}
