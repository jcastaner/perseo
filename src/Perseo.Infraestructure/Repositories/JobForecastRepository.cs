﻿using Perseo.Core;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Configuration;

namespace Perseo.Infraestructure
{
    public class JobForecastRepository : Repository<JobForecast>, IJobForecastRepository, IRepository<JobForecast>
    {

        public JobForecastRepository(ApplicationContext context, IConfiguration configuration) : base(context, configuration) { }

        public IEnumerable<JobForecast> GetJobForecast(IList<int> jobsIds, int year, int month, int? week, int? day)
        {
            return entities
                    .Where(a => jobsIds.Contains(a.JobId)
                            && a.Year == year
                            && a.Month == month)
                    .AsEnumerable();
        }

        public IEnumerable<dynamic> GetJobForecastList(IList<int> jobsIds)
        {
            return Query<dynamic>(
                sql: "select job_forecast.year, job_forecast.month, job_forecast.job_id, job.name as job, area.name as area " +
                "from job_forecast join job ON job_forecast.job_id = job.id join area ON job.area_id = area.id " +
                "WHERE 1 GROUP BY year, month"
            );
        }

}

}
