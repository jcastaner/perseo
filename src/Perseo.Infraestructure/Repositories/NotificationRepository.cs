﻿using Perseo.Core;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Linq;

namespace Perseo.Infraestructure
{
    public class NotificationRepository : Repository<Notification>, INotificationRepository, IRepository<Notification>
    {
        public NotificationRepository(ApplicationContext context, IConfiguration configuration) : base(context, configuration) { }

        public IEnumerable<Notification> GetAllFromUser(int id, bool active = true, NotificationType? type = null )
        {
            if (type != null)
            {
                return entities.Where(s => s.UserId == id && s.Active == true && s.Type == type).AsEnumerable();
            }
            else
            {
                return entities.Where(s => s.UserId == id && s.Active == true).AsEnumerable();
            }
        }
    }
}
