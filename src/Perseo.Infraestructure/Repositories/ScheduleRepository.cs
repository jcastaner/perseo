﻿using Perseo.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;

namespace Perseo.Infraestructure
{
    public class ScheduleRepository: Repository<ScheduleDate>, IScheduleRepository, IRepository<ScheduleDate>
    {
        protected DbSet<ScheduleDateTerm> terms;
        protected DbSet<ScheduleDateTermJob> termJobs;

        public ScheduleRepository(ApplicationContext context, IConfiguration configuration) : base(context, configuration) {
            terms = context.Set<ScheduleDateTerm>();
            termJobs = context.Set<ScheduleDateTermJob>();
        }

        public IEnumerable<ScheduleDate> GetUserSchedule(int year, int userId)
        {
            return Query<ScheduleDate>(
                sql: "SELECT s.* FROM schedule_date AS s " +
                "WHERE s.user_id = @userId AND `s`.`year`= @year ",
                param: new { year, userId }
            );
        }

        public IEnumerable<UserDedication> GetUserDedications (int userId, int year, int? month = null)
        {
            var monthsql ="";
            int monthvalue = 0;
            if (month.HasValue)
            {   monthsql = " AND `sd`.`month` = @monthvalue";
                monthvalue = month.Value;
            }
            return Query<UserDedication>(
                sql: "SELECT sdt.hours as term_hours, sd.year as date_year, sd.month as date_month, sd.day as date_day, " +
                " sd.total_hours as date_hours, j.name as job_name, j.`type` as job_type, j.`default` as job_default," +
                " a.name as area_name, a.type as area_type, " +
                " ac.name as area_code_name, wbs.name as area_code_wbs_name, sdt.id as term_id, sd.user_id as user_id," +
                " sd.id as date_id, a.id as area_id, a.parent_id as area_parent_id, j.id as job_id,  ac.id as area_code_id," +
                " wbs.id as area_code_wbs_id, " +
                " (SELECT COUNT(*) FROM schedule_date_term_job WHERE schedule_date_term_job.schedule_date_term_id = sdt.id) as term_count" +
                " FROM schedule_date_term_job AS sdtj" +
                " LEFT JOIN schedule_date_term AS sdt ON sdtj.schedule_date_term_id = sdt.id" +
                " LEFT JOIN schedule_date AS sd ON sdt.schedule_date_id = sd.id" +
                " LEFT jOIN job AS j ON sdtj.job_id = j.id" +
                " LEFT JOIN `area` AS a ON j.area_id = a.id" +
                " LEFT JOIN area_code_wbs AS wbs ON wbs.area_id = a.id " +
                " LEFT JOIN area_code AS ac ON ac.id = wbs.area_code_id " +
                " WHERE sd.user_id = @userId AND `sd`.`year`= @year " + monthsql + " ORDER BY month, day",
                param: new { userId, year, monthvalue }
            );
        }

        #region ScheduleDateTerm

        public ScheduleDateTerm GetTerm(params object[] key)
        {
            return terms.Find(key);
        }

        public int AddTerm(ScheduleDateTerm entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }
            terms.Add(entity);
            return context.SaveChanges();
        }

        public int AddTermRange(IEnumerable<ScheduleDateTerm> entityList)
        {
            if (entityList == null)
            {
                throw new ArgumentNullException("entity");
            }
            terms.AddRange(entityList);
            return context.SaveChanges();
        }

        public int UpdateTerm(ScheduleDateTerm entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }
            terms.Update(entity);
            return context.SaveChanges();
        }

        public int UpdateTermRange(IEnumerable<ScheduleDateTerm> entityList)
        {
            if (entityList == null)
            {
                throw new ArgumentNullException("entity");
            }
            terms.UpdateRange(entityList);
            return context.SaveChanges();
        }

        public int RemoveTerm(ScheduleDateTerm entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }
            terms.Remove(entity);
            return context.SaveChanges();
        }

        public int RemoveTermRange(IEnumerable<ScheduleDateTerm> entityList)
        {
            if (entityList == null)
            {
                throw new ArgumentNullException("entity");
            }
            terms.RemoveRange(entityList);
            return context.SaveChanges();
        }

        #endregion

        #region ScheduleDateTermJob

        public ScheduleDateTermJob GetTermJob(params object[] key)
        {
            return termJobs.Find(key);
        }

        public int AddTermJob(ScheduleDateTermJob entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }
            termJobs.Add(entity);
            return context.SaveChanges();
        }

        public int AddTermJobRange(IEnumerable<ScheduleDateTermJob> entityList)
        {
            if (entityList == null)
            {
                throw new ArgumentNullException("entity");
            }
            termJobs.AddRange(entityList);
            return context.SaveChanges();
        }

        public int UpdateTermJob(ScheduleDateTermJob entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }
            termJobs.Update(entity);
            return context.SaveChanges();
        }

        public int UpdateTermJobRange(IEnumerable<ScheduleDateTermJob> entityList)
        {
            if (entityList == null)
            {
                throw new ArgumentNullException("entity");
            }
            termJobs.UpdateRange(entityList);
            return context.SaveChanges();
        }

        public int RemoveTermJob(ScheduleDateTermJob entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }
            termJobs.Remove(entity);
            return context.SaveChanges();
        }

        public int RemoveTermJobRange(IEnumerable<ScheduleDateTermJob> entityList)
        {
            if (entityList == null)
            {
                throw new ArgumentNullException("entity");
            }
            termJobs.RemoveRange(entityList);
            return context.SaveChanges();
        }

        #endregion

        //********

        public IEnumerable<StaffingDayPattern> GetWorkPatterns(IList<int> jobsIds, int year, int month, int? week, int? day)
        {
            return Query<StaffingDayPattern>(
                    sql: "SELECT schedule_date.day, schedule_date_term.pattern, job.id as job_id, " +
                            "1/( SELECT COUNT(*) FROM schedule_date_term_job " +
                            "WHERE schedule_date_term_job.schedule_date_term_id = schedule_date_term.id ) AS dedication " +
                        "FROM schedule_date_term " +
                        "JOIN schedule_date ON schedule_date_term.schedule_date_id = schedule_date.id " +
                        "JOIN schedule_date_term_job ON schedule_date_term_job.schedule_date_term_id = schedule_date_term.id " +
                        "JOIN job ON schedule_date_term_job.job_id = job.id " +
                        "WHERE schedule_date.year = @year AND schedule_date.month = @month AND job.id IN @jobsIds",
                    param: new { year, month, jobsIds }
                );
        }
    }
}
