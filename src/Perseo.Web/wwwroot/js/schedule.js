﻿var show_schedule = function (Year, Month) {

    $(".cal").contextmenu(function () {
        return false;
    });

    $(function () {
        var isMouseDown = false, isHighlighted;
        $(".content-cols.row.no-gutters").find(".col").not(".d-lg-none")
            .mousedown(function (event) {
                switch (event.which) {
                    case 1:
                        break;
                    case 3:
                        isMouseDown = true;
                        $(this).toggleClass("highlighted");
                        isHighlighted = $(this).hasClass("highlighted");
                        return false;
                }
            })
            .mouseover(function () {
                if (isMouseDown) {
                    $(this).toggleClass("highlighted", isHighlighted);
                }
            })
            .bind("selectstart", function () {
                return false;
            });

        $(document)
            .mouseup(function () {
                isMouseDown = false;
            });
        $(".content-cols.row.no-gutters").find(".col").not(".d-lg-none").click(function () {
            var id_array = new Array();
            var user_dict = new Object();
            function edit_day(selected_day) {
                var id = selected_day.attr("data-id");
                if (typeof id !== typeof undefined && id !== false && id > 0) {
                    id_array.push(id);
                } else {
                    var dif = selected_day.parent(".row").find(".col").not(".d-lg-none").first().index();
                    var day = selected_day.index() - dif + 1;
                    var user_id = selected_day.parent().attr("data-id");
                    if (!(user_id in user_dict)) {
                        user_dict[user_id] = new Array();
                    }
                    user_dict[user_id].push(day);
                }
            }
            if ($(this).hasClass("highlighted")) {
                $(".cal .highlighted").each(function () {
                    edit_day($(this));
                });

            } else {
                edit_day($(this));
            }
            var json_id_array = JSON.stringify(id_array);
            var json_day_dict = JSON.stringify(user_dict);
            var data = { "Year": Year, "Month": Month, "Ids": json_id_array, "Days": json_day_dict };
            var url = "/schedule/edit?" + $.param(data, true);
            var options = { keyboard: true };
            $.ajaxGetModal(url, "#global-modal-lg", options);
        });
        $("#closbtn").click(function () {
            $("#global-modal-lg").modal("hide");
        });
    });
};

var edit_schedule = function (DayPattern) {

    var post_data = {
        year: 0,
        month: 0,
        hours: 0,
        acronym: "",
        comment: "",
        multiple: false,
        ids: new Array(),
        days: new Array(),
        edit: new Array(),
        add: new Array(),
        remove: new Array()
    };

    var multiselect_options = {
        enableFiltering: true,
        enableCaseInsensitiveFiltering: true,
        filterPlaceholder: 'Buscar',
        maxHeight: 300,
        buttonWidth: '100%',
        buttonClass: 'form-control text-left',
        nonSelectedText: 'Seleccionar tarea',
        onChange: function (options, checked) {
            var select = $(options).closest('select');
            var term = $(options).closest('.term');
            var selectHours = $(term).find('select').last();
            var termindex = $(term).data('term');
            var myclass = $(options).data('class');
            $(term).find('input[name=term]').prop('checked', true);
            $(term).find('.input-group-text').attr('class', 'input-group-text ' + myclass);
            $(term).find('.input-group-text').attr('data-class', myclass);
            $('.row.no-gutters.trs-hours').children('div[data-term*=' + termindex + ']').attr('class', 'col ' + myclass);
            if ($(select).find('option:selected').length === 0) {
                $('.row.no-gutters.trs-hours').children('div[data-term*=' + termindex + ']').attr('class', 'col');
                $('.row.no-gutters.trs-hours').children('div[data-term*=' + termindex + ']').attr('data-term', '');
            }
            var $option = $(options);
            var $otheroptions;
            var $options;
            if ($option.length === 1) {
                var $group = $option.parent('optgroup');
                if ($group.hasClass('production')) {
                    $othergroup = $('.general');
                    $otheroptions = $('option', $othergroup);
                    $options = $('option', $group);
                    $options = $options.filter(':selected');
                    if (checked && $options.length >= 1) {
                        $otheroptions.each(function () {
                            $(select).multiselect('deselect', $(this).val());
                        });
                    }
                    $(selectHours).show();
                } else {
                    $othergroup = $('.production');
                    $otheroptions = $('option', $othergroup);
                    $options = $('option', $group);
                    $options = $options.filter(':selected');
                    if (checked && $options.length >= 1) {
                        if (checked && $options.length > 1) {
                            $options.each(function () {
                                $(select).multiselect('deselect', $(this).val());
                            });
                            $(select).multiselect('select', $option.val());
                        } else {
                            $otheroptions.each(function () {
                                $(select).multiselect('deselect', $(this).val());
                            });
                        }
                    }
                    $(selectHours).hide();
                }
            }
        }
    };

    var update_hours = function () {
        var term_array = new Array();
        $(".trs-hours .col").not(".trs-blank").each(function () {
            term_array.push($(this).attr("data-term"));
        });
        var total_hours = 0;
        $(".term").each(function () {
            var termindex = $(this).attr("data-term");
            var pattern = new Array();
            for (var e = 0; e < 48; e++) {
                if (term_array[e] === termindex) pattern[e] = 1; else pattern[e] = 0;
            }
            var term_hours = pattern.reduce(function (a, b) { return a + b; }, 0) / 2;
            total_hours += term_hours;
            term_hours = term_hours > 0 ? term_hours : "";
            $(this).find(".input-group").find("input[name='hours']").val(term_hours);
        });
        $("input[name='total-hours']").val(total_hours + " horas");
    };

    var range_selector = function () {
        var isMouseDown = false, isHighlighted;
        var termindex = 0;
        var termclass = "";
        $(".trs-hours .col").not(".trs-blank")
            .mousedown(function (event) {
                termindex = $('.term').index($('input[name=term]:checked').closest('.term'));
                termclass = $('input[name=term]:checked').parent().attr('data-class');
                if (termindex >= 0) {
                    switch (event.which) {
                        case 3:
                            break;
                        case 1:
                            isMouseDown = true;
                            var classes = $(this).attr("class");
                            var i = classes.indexOf("cal-day-");
                            if (i !== -1) {
                                var clase = classes.substring(i, i + 9);
                                if (clase !== termclass) $(this).removeClass(clase);
                            }
                            $(this).toggleClass(termclass);
                            isHighlighted = $(this).hasClass(termclass);
                            if (isHighlighted) {
                                $(this).attr('data-term', termindex);
                            } else {
                                $(this).removeAttr('data-term');
                            }
                            return false;
                    }
                }
            })
            .mouseover(function () {
                if (isMouseDown) {
                    if (termindex >= 0) {
                        var classes = $(this).attr("class");
                        var i = classes.indexOf("cal-day-");
                        if (i !== -1) {
                            var clase = classes.substring(i, i + 9);
                            if (clase !== termclass) $(this).removeClass(clase);
                        }
                        $(this).toggleClass(termclass, isHighlighted);
                        if (isHighlighted) {
                            $(this).attr('data-term', termindex);
                        } else {
                            $(this).removeAttr('data-term');
                        }
                    }
                }
            })
            .mouseup(update_hours)
            .bind("selectstart", function () {
                return false;
            });

        $(document).mouseup(function () {
            if (isMouseDown) update_hours();
            isMouseDown = false;
        });
    };

    var save = function () {

        var year = Number($("input[name='year']").val());
        var month = Number($("input[name='month']").val());
        var multiple = $("input[name='multiple']").val() === "true" ? true : false;
        var ids = JSON.parse($("input[name='ids']").val());
        var days = JSON.parse($("input[name='days']").val());

        post_data.year = year;
        post_data.month = month;
        post_data.multiple = multiple;
        post_data.ids = ids;
        post_data.days = days;

        var i = 0;
        var term_array = new Array();
        $(".trs-hours .col").not(".trs-blank").each(function () {
            term_array[i] = $(this).attr("data-term");
            i++;
        });
        var acronym = "";
        var total_hours = 0;
        var max_hours = 0;
        var error = false;
        $(".term").each(function () {
            var term_index = $(this).attr("data-term");
            var term_id = $(this).attr("data-id");
            var job_id = $(this).find("select.jobs").val();
            var hours_type = $(this).find(".work-hours-type").val();
            acronym = $(this).find(".input-group-text").attr("data-class");
            acronym = acronym.slice(-1);
            acronym = isNaN(acronym) ? acronym : "T";
            var pattern = new Array();
            for (i = 0; i < 48; i++) {
                if (term_array[i] === term_index) pattern[i] = 1; else pattern[i] = 0;
            }
            var job_pattern = pattern.slice(-14).concat(pattern.slice(0, 34));
            var term_hours = job_pattern.reduce(function (a, b) { return a + b; }, 0) / 2;
            if ((job_id == "" || job_id == null) && term_hours > 0) {
                toastr.error("Existen períodos asignados sin tarea definida");
                error = true;
            }
            else {
                total_hours += term_hours;
                if (term_id !== "" && typeof term_id !== 'undefined') {
                    if (term_hours > 0) {
                        if (term_hours > max_hours) {
                            max_hours = term_hours;
                            post_data.acronym = acronym;
                        }
                        post_data.edit.push({ term_id: Number(term_id), job_id: job_id, pattern: job_pattern.join(""), hours: term_hours, hours_type: hours_type });
                    } else {
                        post_data.remove.push({ term_id: Number(term_id) });
                    }
                } else {
                    if (term_hours > 0) {
                        if (term_hours > max_hours) {
                            max_hours = term_hours;
                            post_data.acronym = acronym;
                        }
                        post_data.add.push({ job_id: job_id, pattern: job_pattern.join(""), hours: term_hours, hours_type: hours_type });
                    }
                }
            }
        });
        if (error) { return; }
        post_data.hours = total_hours;
        post_data.comment = $("textarea[name='comment']").val();
        var data = JSON.stringify(post_data);
        $.ajaxPostModal("/schedule/edit", data, function (data) {
            data.content.forEach(function (element) {
                var day = $(".row.no-gutters.content-cols[data-id='" + element.user_id + "']")
                    .find(".col").not(".d-lg-none").eq(element.day - 1);
                day.attr("class", "col cal-day-" + element.acronym);
                day.attr("data-id", element.id);
                day.attr("title", element.total_hours);
                day.text(element.acronym);
                if (day.find(".comment").length) {
                    if (element.comment === "false") {
                        day.find(".comment").remove();
                    }
                } else {
                    if (element.comment === "true") {
                        day.append("<div class='comment'></div>");
                    }
                }
            });
            if ($('#calendar').length > 0) {
                var year = $('#calendar').data('calendar').getYear();
                $('#calendar').data('calendar').setYear(year);
            }
        });
    };

    var add_term = function () {
        if (term_array.length >= 1) {
            var termindex = term_array.pop();
            var term = term_html(termindex);
            $(".modal-content .row").last().before(term);
            var newterm = $(".row.term").last();
            $(newterm).find(".delete-button").click(delete_term);
            $(newterm).find('.jobs').multiselect(multiselect_options);
            $('i.glyphicon.glyphicon-remove-circle').removeClass('glyphicon glyphicon-remove-circle').addClass('icon-close');
        } else {
            alert("no more");
        }
    };

    var delete_term = function () {
        var term = $(this).closest(".row.term");
        var termindex = $(term).attr("data-term");
        var term_id = $(term).attr('data-id');
        if (typeof term_id !== typeof undefined && term_id !== false) {
            post_data.remove.push({ term_id: Number(term_id) });
        }
        term_array.push(termindex);
        $(term).prev('hr').remove();
        $(term).remove();
        $(".trs-hours .col").not(".trs-blank").each(function () {
            if ($(this).attr("data-term") === termindex) {
                $(this).removeAttr("data-term");
                $(this).attr("class", "col");
            }
        });
    };

    var hide_modal = function () {
        $("#global-modal-lg").modal("hide");
    };

    var fast_assignment = function () {

        var dayPattern = DayPattern;
        if (dayPattern.length == 0) {
            toastr.error("No hay turno definido para este día");
            return;
        }

        $(".delete-button").each(delete_term);
        add_term();

        var termclass = $(this).find('button').attr('termclass');
        var jobvalue = $(this).find('button').val();
        var term = $(".row.term").last();

        $(term).find('.jobs').multiselect('select', jobvalue);
        $(term).find('input[type="radio"]').prop('checked', true);
        $(term).find('.input-group-text').attr('class', 'input-group-text ' + termclass);
        $(term).find('.input-group-text').attr('data-class', termclass);

        if (termclass == "cal-day-0") {
            $(term).find('.work-hours-type').attr('style', '');
        }

        var termindex = $(term).data('term');

        $(".trs-hours .col").not(".trs-blank").each(function (index) {
            var patternChar = dayPattern.charAt((index + 14) % 48);
            if (patternChar == "1") {
                $(this).toggleClass(termclass);
                $(this).attr('data-term', termindex)
            }
        });

        update_hours();
    };

    range_selector();
    $('.term .jobs').multiselect(multiselect_options);
    $('i.glyphicon.glyphicon-remove-circle').removeClass('glyphicon glyphicon-remove-circle').addClass('icon-close');
    $('#save').click(save);
    $('#add-button').click(add_term);
    $(".delete-button").click(delete_term);
    $("#closbtn").click(hide_modal);
    $(".dropdown-item").click(fast_assignment);
    update_hours();

};