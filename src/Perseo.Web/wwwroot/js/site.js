﻿var __language = document.documentElement.lang;

function breadcrumb() {
    var args = arguments.length;
    var items = '<li class="breadcrumb-item"><a href="/">Home</a></li>';
    var url = "";
    for (var i = 0; i < args; i++) {
        var name = arguments[i].name;
        url += '/' + arguments[i].url;
        if (i === args - 1) {
            items += '<li class="breadcrumb-item active">' + name + '</li>';
        } else {
            items += '<li class="breadcrumb-item"><a href="' + url + '">' + name + '</a></li>';
        }
    }
    $('.breadcrumb-item').remove();
    $('.breadcrumb').prepend(items);
}

function mobile_tabs() {
    $('#main-content .nav-tabs').each(function (e) {
        if ($(this).data('active-tab')) {
            $(this).dropdown_navtabs($(this).data('active-tab'));
        } else if ($(".nav-tabs .nav-link[data-target='#" + $('.tab-content div.active').attr('id') + "']")) {
            $(this).dropdown_navtabs($(".nav-tabs .nav-link[data-target='#" + $('.tab-content div.active').attr('id') + "']").attr('id'));
        } else if ($(".nav-tabs .nav-link[id='#" + $('.tab-content div.active').attr('id') + "']")) {
            $(this).dropdown_navtabs($('.tab-content div.active').attr('id'));
        } else {
            $(this).dropdown_navtabs($('.nav-tabs .nav-link.active').attr('id'));
        }
    });
}

function update_main_nav() {
    var url = window.location.pathname;
    var controller = url.split("/")[1];
    $(".sidebar .nav-dropdown .nav-item .nav-link").removeClass("active");
    if (controller.length > 1) {
        $(".sidebar .nav-dropdown .nav-item .nav-link[href^='/" + controller + "']").each(function () {
            $(this).addClass("active");
            $(this).closest(".nav-dropdown").addClass("open");
        });
    }
}

function titleCase(str) {
    var splitStr = str.toLowerCase().split(' ');
    for (var i = 0; i < splitStr.length; i++) {
        if (splitStr[i].length > 1) splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
    }
    return splitStr.join(' ');
}

function ajaxContentLoaded() {
    update_main_nav();
    document.title = "Perseo - " + $("#header-title").text();
    mobile_tabs();
}

/* AJAX */
var ajax_call_on = false;
$.extend({
    cachedScript: function (url, options) {
        options = $.extend(options || {}, {
            dataType: "script",
            async: false,
            cache: true,
            url: url
        });
        return $.ajax(options);
    },
    ajaxGet: function (url, callback, push_history) {
        if (typeof push_history === "undefined") {
            push_history = true;
        }
        if (ajax_call_on) return false;
        ajax_call_on = true;
        var ajaxLoadTimeout = setTimeout(function () { $("#ajax-loader").show(200); }, 800);
        toastr.clear();
        $.ajax({
            type: "GET",
            url: url,
            defaultErrors: true,
            success: function (response) {
                if (push_history) {
                    var stateObj = { prev_url: url };
                    history.pushState(stateObj, "", url);
                    breadcrumb();
                }
                if (callback) {
                    callback(response);
                } else {
                    $('#main-content').replaceWith('<div id ="main-content" class="animated fadeIn">' + response + "</div");
                }
                ajaxContentLoaded();
            },
            complete: function () {
                clearTimeout(ajaxLoadTimeout);
                $("#ajax-loader").hide(200);
                ajax_call_on = false;
            }
        });
        return false;
    },
    ajaxGetModal: function (url, modal_id, modal_options, callback) {
        if (ajax_call_on) return false;
        ajax_call_on = true;
        if (modal_id === null || modal_id === undefined) modal_id = "#global-modal";
        toastr.clear();
        $.ajax({
            type: "GET",
            defaultErrors: true,
            url: url,
            success: function (data) {
                if (callback) {
                    callback(data);
                } else {
                    $(modal_id + "-content").html(data);
                    if (modal_options !== null && modal_options !== undefined) $(modal_id).modal(modal_options);
                    $(modal_id).modal("show");
                }
            },
            complete: function () {
                ajax_call_on = false;
            }
        });
    },
    ajaxPost: function (url, data, callback) {
        if (ajax_call_on) return false;
        ajax_call_on = true;
        var ajaxLoadTimeout = setTimeout(function () { $("#ajax-loader").show(300); }, 800);
        var contentType = "application/x-www-form-urlencoded; charset=UTF-8";
        var token = $("input[name='__RequestVerificationToken'").val();
        if (token != null) var headers = { 'RequestVerificationToken': token }
        if (typeof data === 'string') {
            try {
                if (typeof $.parseJSON(data) === 'object') {
                    contentType = "application/json; charset=utf-8";
                }
            } catch (ex) { console.log(ex); }
        }
        toastr.clear();
        $.ajax({
            type: "POST",
            url: url,
            contentType: contentType,
            defaultErrors: true,
            headers: headers,
            data: data,
            success: function (resp) {
                ajax_call_on = false;
                if (callback) {
                    callback(resp);
                } else {
                    JSONresponse = resp.responseJSON;
                    if (typeof (JSONresponse) !== 'undefined') {
                        if (JSONresponse.result) {
                            if (close_modal) $('[id^=global-modal]').modal('hide');
                            toastr.success("La petición se ha procesado con éxito");
                        } else {
                            toastr.error(JSONresponse.response);
                        }
                    } 
                }
            },
            error: function (resp) {
                var JSONresponse = resp.responseJSON;
                if (typeof (JSONresponse) !== 'undefined' && typeof JSONresponse.content === 'object' && JSONresponse.content !== null) {
                    JSONresponse.content.forEach(function (item, index, array) {
                        var field = $('[data-valmsg-for="' + item.key + '"]');
                        if ($(field).length) {
                            $(field).removeClass('field-validation-valid');
                            $(field).addClass('field-validation-error');
                            $('[data-valmsg-for="' + item.key + '"]').html('<span id="' + item.key + '-error" class="">' + item.errors + '</span>');
                        }
                    });
                }
            },
            complete: function () {
                clearTimeout(ajaxLoadTimeout);
                $("#ajax-loader").hide(100);
                ajax_call_on = false;
            }
        });
        return false;
    },
    ajaxPostModal: function (url, data, callback, close_modal) {
        if (ajax_call_on) return false;
        if (close_modal === null || close_modal === undefined) close_modal = true;
        ajax_call_on = true;
        var contentType = "application/x-www-form-urlencoded; charset=UTF-8";
        var token = $("input[name='__RequestVerificationToken'").val();
        if (token != null) var headers = { 'RequestVerificationToken': token }
        if (typeof data === 'string') {
            try {
                if (typeof $.parseJSON(data) === 'object') {
                    contentType = "application/json; charset=utf-8";
                }
            } catch (ex) { console.log(ex); }
        }
        toastr.clear();
        $.ajax({
            type: "POST",
            url: url,
            contentType: contentType,
            headers: headers,
            data: data,
            defaultErrors: true,
            success: function (resp) {
                ajax_call_on = false;
                if (callback) {
                    callback(resp);
                    if (close_modal) $('[id^=global-modal]').modal('hide');
                    toastr.success("La petición se ha procesado con éxito");
                } else {
                    JSONresponse = resp.responseJSON;
                    if (typeof (JSONresponse) !== 'undefined') {
                        if (JSONresponse.result) {
                            if (close_modal) $('[id^=global-modal]').modal('hide');
                            toastr.success("La petición se ha procesado con éxito");
                        } else {
                            toastr.error(JSONresponse.response);
                        }
                    }
                }
            },
            error: function (resp) {
                JSONresponse = resp.responseJSON;
                if (typeof (JSONresponse) !== 'undefined' && typeof JSONresponse.content === 'object' && JSONresponse.content !== null) {
                    JSONresponse.content.forEach(function (item, index, array) {
                        var field = $('[data-valmsg-for="' + item.key + '"]');
                        if ($(field).length) {
                            $(field).removeClass('field-validation-valid');
                            $(field).addClass('field-validation-error');
                            $('[data-valmsg-for="' + item.key + '"]').html('<span id="' + item.key + '-error" class="">' + item.errors + '</span>');
                        }
                    });
                }
            },
            complete: function () {
                ajax_call_on = false;
            }
        });
    }
});

$(document).ajaxError(function (event, jqXHR, ajaxSettings, thrownError) {
    if (ajaxSettings.defaultErrors) {
        if (ajaxSettings.type === "GET") {
            if (jqXHR.status === 404) {
                toastr.error("No se ha encontrado el recurso solicitado " + jqXHR.status, "OOOPS! ALGO SALIÓ MAL " );
            } else if (jqXHR.status === 401) {
                window.location.assign(encodeURI("/account/login?ReturnUrl=" + window.location.pathname));
            } else if (jqXHR.status === 403) {
                toastr.error("No tiene los privilegios para acceder al recurso solicitado " + jqXHR.status, "OOOPS! ALGO SALIÓ MAL " );
            } else if (jqXHR.status === 500) {
                toastr.error("Se ha producido un error interno del servidor " + jqXHR.status, "OOOPS! ALGO SALIÓ MAL ");
            } else if (jqXHR.status >0) {
                toastr.error("Se ha producido un error " + jqXHR.status, "OOOPS! ALGO SALIÓ MAL ");
            } else {
                toastr.error("Se ha producido un error inesperado", "OOOPS! ALGO SALIÓ MAL " );
            }
        } else if (ajaxSettings.type === "POST") {
            if (jqXHR.status === 404) {
                toastr.error("No se ha encontrado el recurso solicitado " + jqXHR.status, "OOOPS! ALGO SALIÓ MAL " );
            } else if (jqXHR.status === 401) {
                window.location.assign(encodeURI("/account/login?ReturnUrl=" + window.location.pathname));
            } else if (jqXHR.status === 403) {
                toastr.error("No tiene los privilegios para modificar el recurso solicitado " + jqXHR.status, "OOOPS! ALGO SALIÓ MAL " );
            } else if (jqXHR.status === 500) {
                toastr.error("Se ha producido un error interno del servidor " + jqXHR.status, "OOOPS! ALGO SALIÓ MAL " );
            } else if (jqXHR.status > 0) {
                toastr.error("Se ha producido un error " + jqXHR.status, "OOOPS! ALGO SALIÓ MAL ");
            } else {
                toastr.error("Se ha producido un error inesperado", "OOOPS! ALGO SALIÓ MAL ");
            }
        }
    }
});

$(document).ajaxComplete(function (response, status, xhr) {
    $("#ajax-loader").hide();
//    toastr.clear();
});
/*
$(document).ajaxSuccess(function (response, status, xhr) {});
*/

$(window).on('popstate', function (e) {
    var state = e.originalEvent.state;
    if (state !== null) {
        var ajaxLoadTimeout = setTimeout(function () { $("#ajax-loader").show(200); }, 800);
        toastr.clear();
        $.ajax({
            type: "GET",
            url: state.prev_url,
            defaultErrors: true,
            success: function (response) {
                breadcrumb();
                $('#main-content').replaceWith('<div id ="main-content" class="animated fadeIn">' + response + "</div");
                ajaxContentLoaded();
            },
            complete: function () {
                clearTimeout(ajaxLoadTimeout);
                $("#ajax-loader").hide(200);
            }
        });
    }
});

/* DATA TABLES DEFAULTS*/

//$(document).on('preInit.dt', function (e, settings) {
//    var api = new $.fn.dataTable.Api(settings);
//    var page_len = $(window).height() > 790 ? 20 : 10;
//    api.page.len(10);
//});

$.fn.dt = function (options) {
   
    //var defaults = $.fn.dataTable.defaults;
    //var options = $.extend(defaults, options);
    //Here you can create your extended functions, just like a base plugin.

    if ($(this).is('[class*=select]')) {
        $(this).find('thead tr, tfoot tr').prepend("<th class='checkbox'></th>");
        $(this).find('tbody tr').prepend("<td class='checkbox'></td>");
    }
    if ($(this).is('[class*=responsive]')) {
        $(this).find('thead tr, tfoot tr').prepend("<th class='control'></th>");
        $(this).find('tbody tr').prepend("<td class='control'></td>");
    }

    //return this.each(function () {
        //Execute your normal plugin
        return $(this).DataTable(options);
        //Here you can put your additional logic, define additional events, etc
    //});
};

var datatables_lang = [];

datatables_lang["en"] = {
    "sEmptyTable": "No data available in table",
    "sInfo": "_START_ to _END_ of _TOTAL_ entries",
    "sInfoEmpty": "No entries to show",
    "sInfoFiltered": "(filtered from _MAX_ total entries)",
    "sInfoPostFix": "",
    "sInfoThousands": ",",
    "sLengthMenu": "Show _MENU_ entries",
    "sLoadingRecords": "Loading...",
    "sProcessing": "Processing...",
    "sSearch": "Search",
    "sZeroRecords": "No matching records found",
    "oPaginate": {
        "sFirst": "First",
        "sLast": "Last",
        "sNext": "Next",
        "sPrevious": "Previous"
    },
    "oAria": {
        "sSortAscending": ": activate to sort column ascending",
        "sSortDescending": ": activate to sort column descending"
    },
    "select": {
        "rows": {
            "_": "",
        }
    }
}

datatables_lang["pt"] = {
    "sEmptyTable": "Nenhum registro encontrado",
    "sProcessing": "A processar...",
    "sLengthMenu": "Mostrar _MENU_ registos",
    "sZeroRecords": "Não foram encontrados resultados",
    "sInfo": "_START_ até  _END_ de _TOTAL_ registros",
    "sInfoEmpty": "Não foram encontrados resultados",
    "sInfoFiltered": "(filtrado de _MAX_ registos no total)",
    "sInfoPostFix": "",
    "sSearch": "Procurar",
    "sUrl": "",
    "oPaginate": {
        "sFirst": "Primeiro",
        "sPrevious": "Anterior",
        "sNext": "Seguinte",
        "sLast": "Último"
    },
    "oAria": {
        "sSortAscending": ": Ordenar colunas de forma ascendente",
        "sSortDescending": ": Ordenar colunas de forma descendente"
    },
    "select": {
        "rows": {
            "_": "",
        }
    }
}

datatables_lang["es"]  = {
    "sProcessing": "Procesando...",
    "sLengthMenu": "Mostrar _MENU_ registros",
    "sZeroRecords": "No se encontraron resultados",
    "sEmptyTable": "No hay ningún registro",
    "sInfo": "_START_ a _END_ de _TOTAL_ registros",
    "sInfoEmpty": "No hay ningún registro",
    "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
    "sInfoPostFix": "",
    "sSearch": "Buscar",
    "sUrl": "",
    "sInfoThousands": ",",
    "sLoadingRecords": "Cargando...",
    "oPaginate": {
        "sFirst": "Primera",
        "sLast": "Última",
        "sNext": "Siguiente",
        "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending": ": Ordenar columnas de forma ascendente",
        "sSortDescending": ": Ordenar columnas de forma descendente"
    },
    "select": {
        "rows": {
            "_": "",
        }
    }
}

$.extend(true, $.fn.dataTable.defaults, {
    searching: true,
    ordering: true,
    lengthChange: true,
    lengthMenu: [[10, 20, 50, 100], [10, 20, 50, 100]],
    responsive: true,
    language: datatables_lang[__language],
    responsive: { details: { type: 'column' } },
    //select: { style: 'multi', selector: 'th.select-checkbox'},
    columnDefs: [
        { targets: 'control', orderable: false, defaultContent: "", className: "control", targets: 0, data: null },
        { targets: 'main', responsivePriority: 2 },
        { targets: 'checkbox', className: "checkbox", orderable: false, checkboxes: { 'selectRow': true }, responsivePriority: 1 },
        //{ targets: 'select-checkbox', orderable: false, className: 'select-checkbox',  },
        { targets: 'actions', responsivePriority: 3, orderable: false, defaultContent: "", className: "actions",
            render: function (data, type, row, meta) {
                var action = $("#" + meta.settings.sTableId + " thead tr th:last div").clone();
                if (action.length > 0) {
                    var id = typeof (row.DT_RowData) === "undefined" ? $("#" + meta.settings.sTableId + " tbody tr:nth-child(" + (meta.row + 1) + ")").data("id") : row.DT_RowData.id;
                    $(action).find("a").each(function () {
                        var link = $(this).attr("href");
                        $(this).attr("href", link + "/" + id);
                    });
                    return $(action).html();
                } else return data;
            }
        },
        { targets: 'not-order', orderable: false },
        { targets: 'not-search', searchable: false },
        { targets: 'hidden', orderable: false, visible: false, searchable: false }],
    buttons: [
        { extend: 'copy', text: '<i class="fa fa-files-o"></i>', titleAttr: 'Copiar', title: '', exportOptions: { columns: ":not(:empty):not('.control'):not('.actions'):not('.not-export')"} },
        { extend: 'excel', charset: 'UTF-8', text: '<i class="fa fa-file-excel-o"></i>', titleAttr: 'Excel', title: '',
            exportOptions: {
                columns: ":not(:empty):not('.control'):not('.actions'):not('.not-export')",
                format: {
                    body: function (data, row, column, node) {
                        data = $('<p>' + data + '</p>').text();
                        data = $.isNumeric(data.replace(',', '.')) ? parseFloat(data.replace(',', '.')) : data;
                        return data;
                    }
                }
            }
        },
        { extend: 'csv', charset: 'UTF-8', bom: true, text: '<i class="fa fa-file-text-o"></i>', titleAttr: 'CSV', title: '', exportOptions: { columns: ":not(:empty):not('.control'):not('.actions'):not('.not-export')" } },
    ],
    fnDrawCallback: function (oSettings) {
        if (oSettings._iDisplayLength > oSettings.fnRecordsDisplay()) {
            $(oSettings.nTableWrapper).find('.dataTables_paginate').hide();
            $(oSettings.nTableWrapper).find('.dataTables_info').hide();
        } else {
            $(oSettings.nTableWrapper).find('.dataTables_paginate').show();
            $(oSettings.nTableWrapper).find('.dataTables_info').show();
        }
    },
    preInitComplete: function () {
        /*
        $(this.api().table().header()).find('tr').prepend("<th class='control'></th>");
        this.api().table().rows().nodes().to$().prepend("<td class='control'></td>");
        $(this.api().table().footer()).find('tr').prepend("<th class='control'></th>");
        */
    },
    initComplete: function () {
        var input = $(this.api().table().container()).find('.dataTables_filter input').first();
        var label = $(this.api().table().container()).find('.dataTables_filter label').first();
        $(input).attr("placeholder", $(label).text());
        $(label).contents().filter(function () { return (this.nodeType == 3); }).remove();
        if (($(this.api().table().node()).hasClass('dt-filter'))) {
            this.api().cells(null, ":not(:first)").every(function () { if (this.data() == "") this.data("-"); });
            this.api().columns(":not('.control'):not('.actions'):not('.not-filter')").every(function () {
                var column = this;
                var select = $('<select><option value=""></option></select>')
                    .appendTo($(column.footer()).empty())
                    .on('change', function () {
                        var val = $.fn.dataTable.util.escapeRegex($(this).val());
                        column.search(val ? '^' + val + '$' : '', true, false).draw();
                    });
                column.data().unique().sort().each(function (d, j) {
                    select.append('<option value="' + d + '">' + d + '</option>')
                });
            });
        } 
        if ($(this.api().table().node()).data('row-link') && !$(this.api().table().node()).is('[class*=select]')) {
            var url = $(this.api().table().node()).data('row-link');
            $(this.api().table().container()).on('click', "tr td:not('.control'):not('.checkbox'):not('.actions'):not('.dataTables_empty')",
                function () {
                    var id = $(this).parent().data("id");
                    if (url.indexOf("$id") >= 0) url = url.replace("$id", id);
                    else url = url + id;
                    $.ajaxGet(url);
                });
        }
        //$(this.api().table().container()).attr('style', 'border-collapse: collapse !important');
        if (($(this.api().table().node()).hasClass('select-multi'))) {
            this.api().table().select();
            this.api().table().select.style('multi');
            this.api().table().select.info(false);
        }else if (($(this.api().table().node()).hasClass('select-single'))) {
            this.api().table().select();
            this.api().table().select.style('single');
            this.api().table().select.info(false);
        } else if (($(this.api().table().node()).hasClass('select'))) {
            this.api().table().select();
            this.api().table().select.style('os');
            this.api().table().select.info(false);
        }
        if (($(this.api().table().node()).hasClass('dt-export'))) this.api().buttons().container().appendTo('.dataTables_filter');
        if ($('.dt-hidden-container').length) $('.dt-hidden-container').removeClass('dt-hidden-container');
    }
});

/* HIERARCHY MUTLIPLE DROPDOWN */

$.fn.treeselect = function (options) {

    var treeselect_defaults = {
        type: 'button',
        id: '',
        cssclass: '',
        div: '<div class="dropdown bts_dropdown"></div>',
        form_div: '<div class="dropdown bts_dropdown bts_form w-100"></div>',
        buttontext: 'Elegir',
        button: '<button class="btn btn-outline-secondary dropdown-toggle" type="button" data-toggle="dropdown"><span></span> <i class="caret"></i></button>',
        form_button: '<button class="form-control w-100 pr-1 text-left dropdown-toggle" type="button" data-toggle="dropdown"><span class="w-100"></span></button>',
        ul: '<ul class="dropdown-menu"></ul>',
        li: '<li><label></label></li>'
    };

    var $select = $(this);

    var settings = $.extend(treeselect_defaults, options);

    var isform = settings.type == 'form' ? true : false;
    var $div = isform ? $(settings.form_div) : $(settings.div);
    var $button = isform ? $(settings.form_button) : $(settings.button);
    var _buttontext = isform ? "" : settings.buttontext;
    var id = settings.id == '' ? 'treeselect-' + $select.attr('id') : settings.id; 
    var $ul = $(settings.ul).click(function (e) {
        e.stopPropagation();
    });

    initialize();

    function initialize() {
        $select.after($div);
        $div.append($button);
        $div.append($ul);
        createList();
        updateButtonText();
        $select.remove();
    }

    function createStructure(selector) {
        var options = [];

        $select.children(selector).each(function (i, el) {
            $el = $(el);
            options.push({
                value: $el.val(),
                text: $el.text(),
                data: $el.data(),
                checked: $el.attr('selected') ? true : false,
                children: $el.val().trim() != '' ? createStructure('option[data-parent=' + $el.val() + ']') : ""
            });
        });

        return options;
    }

    function createListItem(option) {
        var $li = $(settings.li);
        $label = $li.children('label');
        if (option.text == '') $label.html("&nbsp;");
        else $label.text(option.text);

        var data_string = ' ';
        $.each(option.data, function (index, value) {
            data_string += 'data-' + index + '="' + value + '" ';
        });

        //$input = $('<input type="checkbox"' + data_string + ' name="' + $select.attr('name').replace('[]', '') + '[]" value="' + option.value + '">');

        //if ($select.attr('multiple'))
        var option_display = isform ? ' style="display:none;" ' : "";

        if ($select.attr('data-multiple')) {
            $input = $('<input type="checkbox"' + data_string + ' name="' + $select.attr('name').replace('[]', '') + '[]" value="' + option.value + '">');
        } else {
            $input = $('<input type="radio"' + data_string + ' name="' + $select.attr('name') + '" value="' + option.value +'"' + option_display + '>');
        }

        if (option.checked)
            $input.attr('checked', 'checked');
        $label.prepend($input);

        $input.change(function () {
            $(this).parent().siblings('ul')
                .find("input[type='checkbox']")
                .prop('checked', this.checked);
            updateButtonText();
            if (!$select.attr('data-multiple')) {
                $ul.removeClass('show');
            }
        });

        if (option.children.length > 0) {
            $(option.children).each(function (i, child) {
                $childul = $('<ul></ul>').appendTo($li);
                $childul.append(createListItem(child));
            });
        }

        return $li;
    }

    function createList() {
        $(createStructure('option:not([data-parent]), option[data-parent=""]')).each(function (i, option) {
            $li = createListItem(option);
            $ul.append($li);
            
        });
    }

    function updateButtonText() {
        if ($select.attr('data-multiple')) {
            $button.children('span').html(_buttontext);
        } else {
            buttontext = [];

            $div.find('input').each(function (i, el) {
                $checkbox = $(el);
                if ($checkbox.is(':checked')) {
                    buttontext.push($checkbox.parent().text());
                }
            });

            if (buttontext.length > 0) {
                if (buttontext.length < 4) {
                    $button.children('span').text(buttontext.join(', '));
                } else if ($div.find('input').length == buttontext.length) {
                    $button.children('span').text('Alle items geselecteerd');
                } else {
                    $button.children('span').text(buttontext.length + ' items geselecteerd');
                }
            } else {
                $button.children('span').text(_buttontext);
            }
        }
        
    }
};

$.fn.dropdown_navtabs = function (active) {
    var active_tab = $(this).find("a[id='" + active + "'], a[data-target='#" + active + "'], a[href='#" + active + "']").first();
    $(active_tab).tab('show');
    $(this).addClass('d-sm-down-none');
    var html = '<div id="drop-nav-tab" class="dropdown d-md-none">';
    html += '<button id="drop-nav-tab-btn" class="btn btn-square btn-outline-secondary rounded-top dropdown-toggle text-right" style="height: 39px; border-bottom-color: #fff;" type="button" data-toggle="dropdown" aria-haspopup="true"> Menu</button >';
    html += '<div class="dropdown-menu" aria-labelledby="dropdownMenuButton" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 35px, 0px);">';
    html += '<div class="nav">';
    $(this).find('li.nav-item a').each(function (index, element) {
        var target = $(this).attr('data-target') !== null ? $(this).attr('data-target') : $(this).attr('href');
        var id = $(this).attr('id') !== null ? 'drop-tab-' + $(this).attr('id') : 'drop-tab-' + target.substring(1);
        if ($(this).attr('href').charAt(0) === '#') {
            html += '<a class="dropdown-item" data-toggle="tab" href="#" data-target="' + target + '" id="' + id + '">' + $(this).html() + '</a>';
        } else {
            html += '<a class="dropdown-item" href="' + $(this).attr('href') + '" id="' + id + '">' + $(this).html() + '</a>';
        }
    });
    html += '</div></div></div>';
    $(this).after(html);
    $('#drop-nav-tab-btn').html($(this).find('a.active').first().html());
    $('#drop-tab-' + active).hide();
    var _this = this;
    $('#drop-nav-tab a').on('click', function (e) {
        if ($(this).attr('href').charAt(0) === '#') {
            $('#drop-nav-tab-btn').html($(this).html());
            $(e.target).hide();
            $(e.target).siblings().show();
            var ind = $(this).index();
            $(_this).find('li a').each(function (index, element) {
                if (index === ind) $(element).addClass('active'); else $(element).removeClass('active');
            });
        }
    });
    $(this).on('click', 'li a', function (e) {
        $('#drop-nav-tab-btn').html($(this).html());
        $('#drop-nav-tab a.dropdown-item').eq($(this).parent().index()).hide();
    });
};

/* MAIN TEMPLATE */

$(document).ready(function () {

    $(".aside-menu .tab-content").find("a").click(function () {
        if ($(this).attr("target") === "undefined") {
            event.preventDefault();
            var url = $(this).attr('href');
            $('.aside-menu-toggler').click();
            if (url != "#") setTimeout(function () { return $.ajaxGet(url); }, 200);
        }
    });

    $(document).click(function (e) {
        if ($(e.target).closest('.aside-menu').length == 0) {
            if ($('body').hasClass('aside-menu-show')) {
                $('.aside-menu-toggler').click();
            }
        }
    });

    $(document).on("click", "a[href^='#']", function (event) {
        event.preventDefault();
    });
    $(document).on("click", "a:not([href^=\"#\"]):not([target]):not(.aside-menu .tab-content a)", function (event) {
        event.preventDefault();
        if ($(this).hasClass('history-back')) history.back(); 
        else return $.ajaxGet($(this).attr('href'));
    });

    $("#help-button").on('click', '[data-toggle="modal"]', function () {
        $($(this).data("target") + ' #help-modal-content').load($(this).data("remote"));
    });

    $(".notification").on('click', function (event) {
        event.preventDefault();
        if ($(this).children('span').first().html() !== "") {
            $('#notification-modal').modal('toggle');
            $($(this).data("target") + ' #notification-modal-content').load($(this).data("remote"));
        } else {
            return false;
        }
    });

    $(document).on('click', '[data-toggle="modal"]', function (event) {
        if ($(this).data("remote").length && $($(this).data("target")).length) {
            var $modal = $($(this).data("target"));
            var $modal_content = $($(this).data("target") + ' ' + $(this).data("target") + '-content');
            $.ajax({ suppressErrors: true, type: "GET", url: $(this).data("remote"),
                success: function (response) {
                    $modal_content.append(response);
                },
                error: function (jqXHR, ajaxSettings, thrownError) {
                    var error = jqXHR;
                    if (jqXHR.status === 404) {
                        error = "No se ha encontrado el recurso solicitado " + jqXHR.status;
                    } else if (jqXHR.status === 401) {
                        error = "No está autorizado a acceder al recurso solicitado " + jqXHR.status;
                    } else if (jqXHR.status === 403) {
                        error = "No tiene los privilegios para acceder al recurso solicitado " + jqXHR.status;
                    } else if (jqXHR.status === 500) {
                        error = "Se ha producido un error interno del servidor " + jqXHR.status;
                    } else if (jqXHR.status > 0) {
                        error = "Se ha producido un error " + jqXHR.status;
                    } else {
                        error = "Se ha producido un error inesperado";
                    }
                    $modal_content.html("<div class='modal-header pb-2 pt-2 bg-danger'><h4 class='modal-title text-white'>Ooops! Algo salió mal </h4><button class='close text-white' type='button' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>×</span></button ></div><div class='modal-body'>" + error +"</div>");
                }
            });
        }
    });
    
    $(document).on('click', '[data-toggle="custom-modal"]', function (event) {
        event.preventDefault();
        if ($(this).data("remote").length && $(this).data("target").length) {
            var url = $(this).data("remote");
            var modal = $(this).data("target");
            var $modal = $(modal);
            var $modal_content = $(modal + ' ' + $(this).data("target") + '-content');
            $.ajax({ type: "GET", url: url,
                success: function (response) {
                    $modal_content.append(response);
                    $modal.modal("show");
                }
            });
        }
    });

    $('#global-modal').on('hidden.bs.modal', function (e) {
        $('#global-modal-content').empty();
    });

    $('#global-modal-lg').on('hidden.bs.modal', function (e) {
        $('#global-modal-lg-content').empty();
    });

    $('#help-modal').on('hidden.bs.modal', function (e) {
        $('#help-modal-content').empty();
    });

    $('#notification-modal').on('hidden.bs.modal', function (e) {
        $('#notification-modal-content').empty();
    });

    update_main_nav();
    mobile_tabs();

    function queryNotifications() {
        $.get("/notification/query", function (response) {
            if (response.result) {
                if (response.content.tasks >= 1) {
                    $("#authuser-task").html(response.content.tasks);
                    $("#authuser-task-alt").html(response.content.tasks);
                }
                if (response.content.messages >= 1) {
                    $("#authuser-message").html(response.content.messages);
                    $("#authuser-message-alt").html(response.content.messages);
                }
                if (response.content.alerts >= 1) {
                    $("#authuser-alert").html(response.content.alerts);
                    $("#authuser-alert-alt").html(response.content.alerts);
                }
            }
        },'json');
    }
    var queryNotificationsInterval = setInterval(queryNotifications, 600000);
});
