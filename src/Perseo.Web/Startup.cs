﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Perseo.Web.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Perseo.Core;
using Perseo.Infraestructure;
using Microsoft.EntityFrameworkCore;
using Perseo.Web.Services;
using System.Globalization;
using System;
using Perseo.Web.Resources;
using Microsoft.AspNetCore.Localization;
using AutoMapper;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Primitives;
using System.Threading.Tasks;
using System.Net;
using System.Linq;

namespace Perseo.Web
{
    public class Startup
    {
        public IHostingEnvironment HostingEnvironment { get; private set; }

        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration, IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true);
            //if (env.IsDevelopment()) {  builder.AddUserSecrets();  }
            builder.AddEnvironmentVariables();
            Configuration = builder.Build();
            HostingEnvironment = env;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<EmailSettings>(options => Configuration.GetSection("EmailSettings").Bind(options));
            services.Configure<DataSettings>(options => Configuration.GetSection("Data").Bind(options));

            services.Configure<CookiePolicyOptions>(options =>
            {
                options.CheckConsentNeeded = context => false;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.AddIdentity<ApplicationUser, ApplicationRole>(options => {
                    options.Password.RequireDigit = true;
                    options.Password.RequiredLength = 8;
                    options.Password.RequireNonAlphanumeric = false;
                    options.Password.RequireUppercase = true;
                    options.Password.RequireLowercase = true;
                    options.Password.RequiredUniqueChars = 4;
                    options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(5);
                    options.Lockout.MaxFailedAccessAttempts = 5;
                    options.Lockout.AllowedForNewUsers = true; })
                .AddCustomStores()
                .AddDefaultTokenProviders();

            services.ConfigureApplicationCookie(options =>
            {
                options.LogoutPath = "/account/logout";
                options.LoginPath = "/account/login";
                options.AccessDeniedPath = "/account/accessdenied";
                options.SlidingExpiration = true;
                options.Cookie.HttpOnly = true;
                options.ExpireTimeSpan = TimeSpan.FromMinutes(240);
            });

            services.AddLocalization(opts => opts.ResourcesPath = "Resources");

            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new ModelMappingProfile());
                mc.AddProfile(new ViewModelMappingProfile());
            });
            mappingConfig.CompileMappings();

            IMapper mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
                .AddDataAnnotationsLocalization(options => options.DataAnnotationLocalizerProvider = (type, factory) => factory.Create(typeof(SharedResources)))
                .AddJsonOptions(options => {
                    options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
                    options.SerializerSettings.Converters.Add(new Newtonsoft.Json.Converters.StringEnumConverter());
                });

            services.Configure<TokenManagement>(Configuration.GetSection("tokenManagement"));
            var token = Configuration.GetSection("tokenManagement").Get<TokenManagement>();
            var secret = Encoding.ASCII.GetBytes(token.Secret);

            services.AddHttpContextAccessor();

            services.AddDbContextPool<ApplicationContext>(  
                options => options
                    .UseMySql(Configuration["Data:ConnectionString"], 
                        mysqlOptions =>  {
                            mysqlOptions.ServerVersion(Configuration["Data:ServerVersion"]);
                        })
                    .UseLazyLoadingProxies()                           
            );

            
            services.AddAuthentication()
                .AddJwtBearer(options => {
                    options.RequireHttpsMetadata = false;
                    options.SaveToken = true;
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(token.Secret)),
                        ValidIssuer = token.Issuer,
                        ValidAudience = token.Audience,
                        ValidateIssuer = false,
                        ValidateAudience = false
                    };
                    options.Events = new JwtBearerEvents
                    {
                        OnMessageReceived = (context) => {
                            if (!context.Request.Query.TryGetValue("token", out StringValues values))
                            {
                                return Task.CompletedTask;
                            }
                            if (values.Count > 1)
                            {
                                context.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                                context.Fail( "Only one 'token' parameter can be defined" );
                                return Task.CompletedTask;
                            }
                            var _token = values.Single();
                            if (String.IsNullOrWhiteSpace(_token))
                            {
                                context.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                                context.Fail( "The 'token' parameter has no value" );
                                return Task.CompletedTask;
                            }
                            context.Token = _token;
                            return Task.CompletedTask;
                        }
                    };

                });
            //services.AddAuthorization();

            services.AddTransient<IFileService, FileService>(s => new FileService(HostingEnvironment.ContentRootPath));
            services.AddTransient<IStaticFileService, StaticFileService>();
            services.AddTransient<IEmailService, EmailService>();
            services.AddTransient<IExcellService, ExcellService>();
            services.AddTransient<HighCharts.IHighCharts, HighCharts.HighCharts>();
            services.AddTransient<Charts.ICharts, Charts.Charts>();

            services.AddTransient<IViewRenderService, ViewRenderService>();
            services.AddTransient<SharedViewLocalizer>();

            services.AddScoped(typeof(IRepository<>), typeof(Repository<>));
            services.AddTransient(typeof(IService<>), typeof(Service<>));

            services.AddScoped<IUserRepository, UserRepository>();
            services.AddTransient<IUserService, UserService>();

            services.AddScoped<IRoleRepository, RoleRepository>();
            services.AddTransient<IRoleService, RoleService>();

            services.AddScoped<IScheduleRepository, ScheduleRepository>();
            services.AddTransient<IScheduleService, ScheduleService>();

            services.AddTransient<IAreaRepository, AreaRepository>();
            services.AddTransient<IAreaService, AreaService>();

            services.AddTransient<IJobRepository, JobRepository>();
            services.AddTransient<IJobService, JobService>();

            services.AddTransient<IJobForecastRepository, JobForecastRepository>();
            services.AddTransient<IStaffingService, StaffingService>();

            services.AddScoped<IRequestRepository, RequestRepository>();
            services.AddTransient<IRequestService, RequestService>();

            services.AddScoped<INotificationRepository, NotificationRepository>();
            services.AddTransient<INotificationService, NotificationService>();

            services.AddRouting(options => options.LowercaseUrls = true);

        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            var supportedCultures = new[]
            {
                new CultureInfo("es"),
                new CultureInfo("en"),
                new CultureInfo("pt"),
            };

            app.UseRequestLocalization(new RequestLocalizationOptions
            {
                DefaultRequestCulture = new RequestCulture("es"),
                SupportedCultures = supportedCultures,
                SupportedUICultures = supportedCultures
            });

            app.Use((context, next) =>
            {
                context.Request.Scheme = "https";
                return next();
            });

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Error/Get");
                app.UseHsts();
            }

            app.UseStaticFiles();
            app.UseCookiePolicy();
            app.UseAuthentication();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "areaRoute",
                    template: "{area:exists}/{controller=Home}/{action=Index}/{id?}");
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
