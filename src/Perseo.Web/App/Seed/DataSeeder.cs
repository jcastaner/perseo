﻿using Perseo.Core;
using Perseo.Infraestructure;
using System.Linq;

namespace Perseo.Web
{
    public class DataSeeder
    {
        private readonly ApplicationContext _context;

        public DataSeeder(ApplicationContext context)
        {
            _context = context;
        }

        private void AddData<T>(T data) where T : Entity
        {
            var entities = _context.Set<T>();
            var existingData = entities.FirstOrDefault(d => d.Id == data.Id);
            if (existingData == null)
            {
                entities.Add(data);
            }
        }

        public void SeedData()
        {
            //SeedRole();
            //SeedCategory();
            //SeedCollective();
            //SeedCompany();
            //SeedArea();
            //SeedJob();
            //SeedOffice();
            //SeedUser();
            //SeedUserRole();
            //SeedUserArea();

            //_context.SaveChanges();
        }

        public void SeedDebugData()
        {
            //SeedDebugArea();
            //SeedDebugJob();
            //SeedDebugUser();
            //SeedDebugUserArea();


            //_context.SaveChanges();
        }

       
        private void SeedRole()
        {
            AddData<Role>(new Role { Id = 1, Name = "Administration", NormalizedName = "ADMINISTRATION" });
            AddData<Role>(new Role { Id = 2, Name = "Maintenance", NormalizedName = "MAINTENANCE" });
            AddData<Role>(new Role { Id = 3, Name = "EconomicalManagementShow", NormalizedName = "ECONOMICALMANAGEMENTSHOW" });
            AddData<Role>(new Role { Id = 4, Name = "EconomicalManagementEdit", NormalizedName = "ECONOMICALMANAGEMENTEDIT" });
            AddData<Role>(new Role { Id = 5, Name = "TecnicalManagementShow", NormalizedName = "TECNICALMANAGEMENTSHOW" });
            AddData<Role>(new Role { Id = 6, Name = "TecnicalManagementEdit", NormalizedName = "TECNICALMANAGEMENTEDIT" });
            AddData<Role>(new Role { Id = 7, Name = "OperationalManagementShow", NormalizedName = "OPERATIONALMANAGEMENTSHOW" });
            AddData<Role>(new Role { Id = 8, Name = "OperationalManagementEdit", NormalizedName = "OPERATIONALMANAGEMENTEDIT" });
            AddData<Role>(new Role { Id = 9, Name = "QualityManagementShow", NormalizedName = "QUALITYMANAGEMENTSHOW" });
            AddData<Role>(new Role { Id = 10, Name = "QualityManagementEdit", NormalizedName = "QUALITYMANAGEMENTEDIT" });
            AddData<Role>(new Role { Id = 11, Name = "TrainingManagementShow", NormalizedName = "TRAININGMANAGEMENTSHOW" });
            AddData<Role>(new Role { Id = 12, Name = "TrainingManagementEdit", NormalizedName = "TRAININGMANAGEMENTEDIT" });
            AddData<Role>(new Role { Id = 13, Name = "ItManagementShow", NormalizedName = "ITMANAGEMENTSHOW" });
            AddData<Role>(new Role { Id = 14, Name = "ItManagementEdit", NormalizedName = "ITMANAGEMENTEDIT" });
            AddData<Role>(new Role { Id = 15, Name = "HHRRManagementShow", NormalizedName = "HHRRMANAGEMENTSHOW" });
            AddData<Role>(new Role { Id = 16, Name = "HHRRManagementEdit", NormalizedName = "HHRRMANAGEMENTEDIT" });
            AddData<Role>(new Role { Id = 17, Name = "Operation", NormalizedName = "OPERATION" });
        }
        private void SeedOffice()
        {
            AddData<Office>(new Office { Id = 1, Name = "Sant Joan Despí (BCN)", Active = true });
            AddData<Office>(new Office { Id = 2, Name = "Alcalá (MAD)", Active = true });
            AddData<Office>(new Office { Id = 3, Name = "Bollullos (SEV)", Active = true });
            AddData<Office>(new Office { Id = 4, Name = "Cordovilla (PAM)", Active = true });
        }
        private void SeedCompany()
        {
            AddData<Company>(new Company { Id = 1, Name = "Minsait", Description = "Minsait", Active = true, External = false });
            AddData<Company>(new Company { Id = 2, Name = "Indra Bpo", Description = "Indra Bpo - Opsait", Active = true, External = false });
            AddData<Company>(new Company { Id = 3, Name = "Indra Bpo Servicios", Description = "Indra Bpo Servicios - Opsait", Active = true, External = false });
        }
        private void SeedCollective()
        {
            AddData<Collective>(new Collective { Id = 1, Name = "Consultoría (XVII)", StartDate = new System.DateTime(2018, 1, 1), Active = true });
        }
        private void SeedCategory()
        {
            AddData<Category>(new Category { Id = 1, Name = "Producción 1", Level = 1, Active = true });
            AddData<Category>( new Category { Id = 2, Name = "Producción 2", Level = 2, Active = true });
            AddData<Category>(new Category { Id = 3, Name = "Producción 3", Level = 3, Active = true });
            AddData<Category>(new Category { Id = 4, Name = "Producción 4", Level = 4, Active = true });
            AddData<Category>(new Category { Id = 5, Name = "Producción 5", Level = 5, Active = true });
            AddData<Category>(new Category { Id = 6, Name = "Producción 6", Level = 6, Active = true });
            AddData<Category>(new Category { Id = 7, Name = "Supervisor 1", Level = 7, Active = true });
            AddData<Category>(new Category { Id = 8, Name = "Supervisor 2", Level = 8, Active = true });
            AddData<Category>(new Category { Id = 9, Name = "Supervisor 3", Level = 9, Active = true });
            AddData<Category>(new Category { Id = 10, Name = "Gerente 1", Level = 10, Active = true });
            AddData<Category>(new Category { Id = 11, Name = "Gerente 2", Level = 11, Active = true });
            AddData<Category>(new Category { Id = 12, Name = "Gerente 3", Level = 12, Active = true });
            AddData<Category>(new Category { Id = 13, Name = "Director 1", Level = 13, Active = true });
            AddData<Category>(new Category { Id = 14, Name = "Director 2", Level = 14, Active = true });
            AddData<Category>(new Category { Id = 15, Name = "Director 3", Level = 15, Active = true });
        }
        private void SeedArea()
        {
            AddData<Area>(new Area { Id = 1, Name = "Indra", Description = "Indra", Active = true, Type = AreaType.Organizational});
                AddData<Area>(new Area { Id = 2, Name = "Minsait", Description = "Minsait", Active = true, Type = AreaType.Organizational, ParentId = 1 });
                    AddData<Area>(new Area { Id = 3, Name = "Opsait", Description = "Opsait", Active = true, Type = AreaType.Organizational, ParentId = 2 });
                        AddData<Area>(new Area { Id = 4, Name = "Servicios Financieros", Description = "Servicios Financieros", Active = true, Type = AreaType.Organizational, ParentId = 3 });
                            AddData<Area>(new Area { Id = 5, Name = "CaixaBank", Description = "Servicios CaixaBank", Active = true, Type = AreaType.Organizational, ParentId = 4 });
                                AddData<Area>(new Area { Id = 6, Name = "Medios de Pago", Description = "Medios de Pago", Active = true, Type = AreaType.Project, ParentId = 5 }); //jv
                                    AddData<Area>(new Area { Id = 7, Name = "Isoc", Description = "Isoc", Active = true, Type = AreaType.Productive, ParentId = 6 }); // xenia
                                    AddData<Area>(new Area { Id = 8, Name = "Payments", Description = "Payments", Active = true, Type = AreaType.Productive, ParentId = 6 }); // carles
                                    AddData<Area>(new Area { Id = 9, Name = "Comercia", Description = "Comercia", Active = true, Type = AreaType.Productive, ParentId = 6 }); // ..
                        AddData<Area>(new Area { Id = 10, Name = "OMC", Description = "Oficina Téncia y de Mejora Continua", Active = true, Type = AreaType.Project, ParentId = 3 }); // ..
                                        AddData<Area>(new Area { Id = 11, Name = "Card Factory", Description = "Card Factory", Active = true, Type = AreaType.Productive, ParentId = 8 }); // MjCamacho

            AddData<Area>(new Area { Id = 12, Name = "General", Description = "General", Active = true, Type = AreaType.General, ParentId = 1 });
                AddData<Area>(new Area { Id = 13, Name = "Vacaciones", Description = "Vacaciones", Active = true, Type = AreaType.General, ParentId = 12 });
                    AddData<Area>(new Area { Id = 17, Name = "Vacaciones", Description = "Vacaciones", Active = true, Type = AreaType.General, ParentId = 13 });
                    AddData<Area>(new Area { Id = 18, Name = "Vacaciones Año Pasado", Description = "Vacaciones Año Pasado", Active = true, Type = AreaType.General, ParentId = 13 });
                    AddData<Area>(new Area { Id = 19, Name = "Ajuste de Horas por Vacaciones", Description = "Ajuste de Horas por Vacaciones", Active = true, Type = AreaType.General, ParentId = 13 });
                AddData<Area>(new Area { Id = 14, Name = "Libre Disposición", Description = "Libre Disposición", Active = true, Type = AreaType.General, ParentId = 12 });
                    AddData<Area>(new Area { Id = 20, Name = "Día Libre Disposición", Description = "Día Libre Disposición", Active = true, Type = AreaType.General, ParentId = 14 });
                    AddData<Area>(new Area { Id = 21, Name = "Horas Libre Disposición", Description = "Horas Libre Disposición", Active = true, Type = AreaType.General, ParentId = 14 });
                AddData<Area>(new Area { Id = 15, Name = "Asuntos Sindicales", Description = "Asuntos Sindicales", Active = true, Type = AreaType.General, ParentId = 12 });
                    AddData<Area>(new Area { Id = 22, Name = "Asuntos Sindicales", Description = "Día Libre Disposición", Active = true, Type = AreaType.General, ParentId = 15 });
                    AddData<Area>(new Area { Id = 23, Name = "Otros Asuntos Sindicales", Description = "Horas Libre Disposición", Active = true, Type = AreaType.General, ParentId = 15 });
                AddData<Area>(new Area { Id = 16, Name = "Permiso Remunerado", Description = "Permiso Remunerado", Active = true, Type = AreaType.General, ParentId = 12 });
                    AddData<Area>(new Area { Id = 24, Name = "Baja Médica", Description = "Baja Médica", Active = true, Type = AreaType.General, ParentId = 16 });
                    AddData<Area>(new Area { Id = 25, Name = "Visita Médica", Description = "Visita Médica", Active = true, Type = AreaType.General, ParentId = 16 });
                    AddData<Area>(new Area { Id = 26, Name = "Enfermedad sin Baja", Description = "Enfermedad sin Baja", Active = true, Type = AreaType.General, ParentId = 16 });
                    AddData<Area>(new Area { Id = 27, Name = "Nacimiento", Description = "Nacimiento", Active = true, Type = AreaType.General, ParentId = 16 });
                    AddData<Area>(new Area { Id = 28, Name = "Lactancia", Description = "Lactancia", Active = true, Type = AreaType.General, ParentId = 16 });
                    AddData<Area>(new Area { Id = 29, Name = "Maternidad", Description = "Maternidad", Active = true, Type = AreaType.General, ParentId = 16 });
                    AddData<Area>(new Area { Id = 30, Name = "Paternidad", Description = "Paternidad", Active = true, Type = AreaType.General, ParentId = 16 });
                    AddData<Area>(new Area { Id = 31, Name = "Maternidad Extendida", Description = "Maternidad Extendida", Active = true, Type = AreaType.General, ParentId = 16 });
                    AddData<Area>(new Area { Id = 32, Name = "Maternidad Acumulada", Description = "Maternidad Acumulada", Active = true, Type = AreaType.General, ParentId = 16 });
                    AddData<Area>(new Area { Id = 33, Name = "Maternidad Combinada", Description = "Maternidad Combinada", Active = true, Type = AreaType.General, ParentId = 16 });
                    AddData<Area>(new Area { Id = 34, Name = "Pa/Maternidad Parcial", Description = "Pa/Maternidad Parcial", Active = true, Type = AreaType.General, ParentId = 16 });
                    AddData<Area>(new Area { Id = 35, Name = "Matrimonio", Description = "Matrimonio", Active = true, Type = AreaType.General, ParentId = 16 });
                    AddData<Area>(new Area { Id = 36, Name = "Matrimonio Familiar", Description = "Matrimonio Familiar", Active = true, Type = AreaType.General, ParentId = 16 });
                    AddData<Area>(new Area { Id = 37, Name = "Fallecimiento Familiar", Description = "Fallecimiento Familiar", Active = true, Type = AreaType.General, ParentId = 16 });
                    AddData<Area>(new Area { Id = 38, Name = "Enfermedad Familiar", Description = "Enfermedad Familiar", Active = true, Type = AreaType.General, ParentId = 16 });
                    AddData<Area>(new Area { Id = 39, Name = "Mudanza", Description = "Mudanza", Active = true, Type = AreaType.General, ParentId = 16 });
                    AddData<Area>(new Area { Id = 40, Name = "Exámenes", Description = "Exámenes", Active = true, Type = AreaType.General, ParentId = 16 });
                    AddData<Area>(new Area { Id = 41, Name = "Deber Judicial/Público", Description = "Deber Judicial/Público", Active = true, Type = AreaType.General, ParentId = 16 });
                    AddData<Area>(new Area { Id = 42, Name = "Reconocimiento Médico Empresa", Description = "Reconocimiento Médico Empresa", Active = true, Type = AreaType.General, ParentId = 16 });
                    AddData<Area>(new Area { Id = 43, Name = "Permiso sin sueldo", Description = "Permiso sin sueldo", Active = true, Type = AreaType.General, ParentId = 16 });
                    AddData<Area>(new Area { Id = 44, Name = "Suspensión sin sueldo", Description = "Suspensión sin sueldo", Active = true, Type = AreaType.General, ParentId = 16 });
                    AddData<Area>(new Area { Id = 45, Name = "Suspensión de empleo y sueldo", Description = "Suspensión de empleo y sueldo", Active = true, Type = AreaType.General, ParentId = 16 });
        }
        private void SeedJob()
        {
            AddData<Job>(new Job { Id = 1, Name = "Indra", Description = "Indra", Type = JobType.Organizational, AreaId = 1, Default = true, Acronym = "T", StartDate = new System.DateTime(1900, 1, 1) });
            AddData<Job>(new Job { Id = 2, Name = "Minsait", Description = "Minsait", Type = JobType.Organizational, AreaId = 2, Default = true, Acronym = "T", StartDate = new System.DateTime(1900, 1, 1) });
            AddData<Job>(new Job { Id = 3, Name = "Opsait", Description = "Opsait", Type = JobType.Organizational, AreaId = 3, Default = true, Acronym = "T", StartDate = new System.DateTime(1900, 1, 1) });
            AddData<Job>(new Job { Id = 4, Name = "Servicios Financieros", Description = "Servicios Financieros", Type = JobType.Organizational, AreaId = 4, Default = true, Acronym = "T", StartDate = new System.DateTime(1900, 1, 1) });
            AddData<Job>(new Job { Id = 5, Name = "CaixaBank", Description = "CaixaBank", Type = JobType.Organizational, AreaId = 5, Default = true, Acronym = "T", StartDate = new System.DateTime(1900, 1, 1) });
            AddData<Job>(new Job { Id = 6, Name = "Medios de Pago", Description = "Medios de Pago", Type = JobType.Project, AreaId = 6, Default = true, Acronym = "T", StartDate = new System.DateTime(1900, 1, 1) });
            AddData<Job>(new Job { Id = 7, Name = "Isoc", Description = "Isoc", Type = JobType.Productive, AreaId = 7, Default = true, Acronym = "T", StartDate = new System.DateTime(1900, 1, 1) });
            AddData<Job>(new Job { Id = 8, Name = "Payments", Description = "Payments", Type = JobType.Productive, AreaId = 8, Default = true, Acronym = "T", StartDate = new System.DateTime(1900, 1, 1) });
            AddData<Job>(new Job { Id = 9, Name = "Comercia", Description = "Comercia", Type = JobType.Productive, AreaId = 9, Default = true, Acronym = "T", StartDate = new System.DateTime(1900, 1, 1) });
            AddData<Job>(new Job { Id = 10, Name = "OMC", Description = "Oficina Téncia y de Mejora Continua", Type = JobType.Project, AreaId = 10, Default = true, Acronym = "T", StartDate = new System.DateTime(1900, 1, 1) });
            AddData<Job>(new Job { Id = 11, Name = "Card Factory", Description = "Card Factory", Type = JobType.Productive, AreaId = 11, Default = true, Acronym = "T", StartDate = new System.DateTime(1900, 1, 1) });

            AddData<Job>(new Job { Id = 12, Name = "General", Description = "General", Active = true, Type = JobType.Organizational, AreaId = 12, Default = true, Acronym = "", StartDate = new System.DateTime(1900, 1, 1) });
                AddData<Job>(new Job { Id = 13, Name = "Vacaciones", Description = "Vacaciones", Active = true, Type = JobType.General, AreaId = 13, Default = true, Acronym = "", StartDate = new System.DateTime(1900, 1, 1) });
                    AddData<Job>(new Job { Id = 17, Name = "Vacaciones", Description = "Vacaciones", Active = true, Type = JobType.Hollydays, AreaId = 17, Default = true, Acronym = "V", StartDate = new System.DateTime(1900, 1, 1) });
                    AddData<Job>(new Job { Id = 18, Name = "Vacaciones Año Pasado", Description = "Vacaciones Año Pasado", Active = true, Type = JobType.LastYearHollydays, AreaId = 18, Default = true, Acronym = "Y", StartDate = new System.DateTime(1900, 1, 1) });
                    AddData<Job>(new Job { Id = 19, Name = "Ajuste de Horas por Vacaciones", Description = "Ajuste de Horas por Vacaciones", Active = true, Type = JobType.LastYearHollydays, AreaId = 19, Default = true, Acronym = "Y", StartDate = new System.DateTime(1900, 1, 1) });
                AddData<Job>(new Job { Id = 14, Name = "Libre Disposición", Description = "Libre Disposición", Active = true, Type = JobType.General, AreaId = 14, Default = true, Acronym = "", StartDate = new System.DateTime(1900, 1, 1) });
                    AddData<Job>(new Job { Id = 20, Name = "Día Libre Disposición", Description = "Día Libre Disposición", Active = true, Type = JobType.FreeDays, AreaId = 20, Default = true, Acronym = "L", StartDate = new System.DateTime(1900, 1, 1) });
                    AddData<Job>(new Job { Id = 21, Name = "Horas Libre Disposición", Description = "Horas Libre Disposición", Active = true, Type = JobType.FreeDays, AreaId = 21, Default = true, Acronym = "L", StartDate = new System.DateTime(1900, 1, 1) });
                AddData<Job>(new Job { Id = 15, Name = "Asuntos Sindicales", Description = "Asuntos Sindicales", Active = true, Type = JobType.General, AreaId = 15, Default = true, Acronym = "", StartDate = new System.DateTime(1900, 1, 1) });
                    AddData<Job>(new Job { Id = 22, Name = "Asuntos Sindicales", Description = "Asuntos Sindicales", Active = true, Type = JobType.UnionHours, AreaId = 22, Default = true, Acronym = "S", StartDate = new System.DateTime(1900, 1, 1) });
                    AddData<Job>(new Job { Id = 23, Name = "Otros Asuntos Sindicales", Description = "Otros Asuntos Sindicales", Active = true, Type = JobType.UnionHours, AreaId = 23, Default = true, Acronym = "S", StartDate = new System.DateTime(1900, 1, 1) });
                AddData<Job>(new Job { Id = 16, Name = "Permiso Remunerado", Description = "Permiso Remunerado", Active = true, Type = JobType.General, AreaId = 16, Default = true, Acronym = "", StartDate = new System.DateTime(1900, 1, 1) });
                    AddData<Job>(new Job { Id = 24, Name = "Baja por Enfermedad", Description = "Baja por Enfermedad", Active = true, Type = JobType.SickLeave, AreaId = 24, Default = true, Acronym = "B", StartDate = new System.DateTime(1900, 1, 1) });
                    AddData<Job>(new Job { Id = 25, Name = "Visita Médica", Description = "Visita Médica", Active = true, Type = JobType.DoctorHours, AreaId = 25, Default = true, Acronym = "M", StartDate = new System.DateTime(1900, 1, 1) });
                    AddData<Job>(new Job { Id = 26, Name = "Enfermedad sin Baja", Description = "Enfermedad sin Baja", Active = true, Type = JobType.PaidLeave, AreaId = 26, Default = true, Acronym = "P", StartDate = new System.DateTime(1900, 1, 1) });
                    AddData<Job>(new Job { Id = 27, Name = "Nacimiento", Description = "Nacimiento", Active = true, Type = JobType.PaidLeave, AreaId = 27, Default = true, Acronym = "P", StartDate = new System.DateTime(1900, 1, 1) });
                    AddData<Job>(new Job { Id = 28, Name = "Lactancia", Description = "Lactancia", Active = true, Type = JobType.PaidLeave, AreaId = 28, Default = true, Acronym = "P", StartDate = new System.DateTime(1900, 1, 1) });
                    AddData<Job>(new Job { Id = 29, Name = "Maternidad", Description = "Maternidad", Active = true, Type = JobType.PaidLeave, AreaId = 20, Default = true, Acronym = "P", StartDate = new System.DateTime(1900, 1, 1) });
                    AddData<Job>(new Job { Id = 30, Name = "Paternidad", Description = "Paternidad", Active = true, Type = JobType.PaidLeave, AreaId = 30, Default = true, Acronym = "P", StartDate = new System.DateTime(1900, 1, 1) });
                    AddData<Job>(new Job { Id = 31, Name = "Maternidad Extendida", Description = "Maternidad Extendida", Active = true, Type = JobType.PaidLeave, AreaId = 31, Default = true, Acronym = "P", StartDate = new System.DateTime(1900, 1, 1) });
                    AddData<Job>(new Job { Id = 32, Name = "Maternidad Acumulada", Description = "Maternidad Acumulada", Active = true, Type = JobType.PaidLeave, AreaId = 32, Default = true, Acronym = "P", StartDate = new System.DateTime(1900, 1, 1) });
                    AddData<Job>(new Job { Id = 33, Name = "Maternidad Combinada", Description = "Maternidad Combinada", Active = true, Type = JobType.PaidLeave, AreaId = 33, Default = true, Acronym = "P", StartDate = new System.DateTime(1900, 1, 1) });
                    AddData<Job>(new Job { Id = 34, Name = "Pa/Maternidad Parcial", Description = "Pa/Maternidad Parcial", Active = true, Type = JobType.PaidLeave, AreaId = 34, Default = true, Acronym = "P", StartDate = new System.DateTime(1900, 1, 1) });
                    AddData<Job>(new Job { Id = 35, Name = "Matrimonio", Description = "Matrimonio", Active = true, Type = JobType.PaidLeave, AreaId = 35, Default = true, Acronym = "P", StartDate = new System.DateTime(1900, 1, 1) });
                    AddData<Job>(new Job { Id = 36, Name = "Matrimonio Familiar", Description = "Matrimonio Familiar", Active = true, Type = JobType.PaidLeave, AreaId = 36, Default = true, Acronym = "P", StartDate = new System.DateTime(1900, 1, 1) });
                    AddData<Job>(new Job { Id = 37, Name = "Fallecimiento Familiar", Description = "Fallecimiento Familiar", Active = true, Type = JobType.PaidLeave, AreaId = 37, Default = true, Acronym = "P", StartDate = new System.DateTime(1900, 1, 1) });
                    AddData<Job>(new Job { Id = 38, Name = "Enfermedad Familiar", Description = "Enfermedad Familiar", Active = true, Type = JobType.PaidLeave, AreaId = 38, Default = true, Acronym = "P", StartDate = new System.DateTime(1900, 1, 1) });
                    AddData<Job>(new Job { Id = 39, Name = "Mudanza", Description = "Mudanza", Active = true, Type = JobType.PaidLeave, AreaId = 39, Default = true, Acronym = "P", StartDate = new System.DateTime(1900, 1, 1) });
                    AddData<Job>(new Job { Id = 40, Name = "Exámenes", Description = "Exámenes", Active = true, Type = JobType.PaidLeave, AreaId = 40, Default = true, Acronym = "P", StartDate = new System.DateTime(1900, 1, 1) });
                    AddData<Job>(new Job { Id = 41, Name = "Deber Judicial/Público", Description = "Deber Judicial/Público", Active = true, Type = JobType.PaidLeave, AreaId = 41, Default = true, Acronym = "P", StartDate = new System.DateTime(1900, 1, 1) });
                    AddData<Job>(new Job { Id = 42, Name = "Reconocimiento Médico Empresa", Description = "Reconocimiento Médico Empresa", Active = true, Type = JobType.PaidLeave, AreaId = 42, Default = true, Acronym = "P", StartDate = new System.DateTime(1900, 1, 1) });
                    AddData<Job>(new Job { Id = 43, Name = "Permiso sin sueldo", Description = "Permiso sin sueldo", Active = true, Type = JobType.PaidLeave, AreaId = 43, Default = true, Acronym = "P", StartDate = new System.DateTime(1900, 1, 1) });
                    AddData<Job>(new Job { Id = 44, Name = "Suspensión sin sueldo", Description = "Suspensión sin sueldo", Active = true, Type = JobType.PaidLeave, AreaId = 44, Default = true, Acronym = "P", StartDate = new System.DateTime(1900, 1, 1) });
                    AddData<Job>(new Job { Id = 45, Name = "Suspensión de empleo y sueldo", Description = "Suspensión de empleo y sueldo", Active = true, Type = JobType.PaidLeave, AreaId = 45, Default = true, Acronym = "P", StartDate = new System.DateTime(1900, 1, 1) });
                AddData<Job>(new Job { Id = 46, Name = "Ausencia Injustificada", Description = "Ausencia Injustificada", Active = true, Type = JobType.UnjustifiedAbsence, AreaId = 12, Default = false, Acronym = "A", StartDate = new System.DateTime(1900, 1, 1) });

            //AddData<Job>(new Job { Id = 12, Name = "Vacaciones", Description = "Vacaciones", Type = JobType.Hollydays, AreaId = 1, Default = false, Acronym = "V", StartDate = new System.DateTime(2019, 1, 1), EndDate = new System.DateTime(2019, 12, 31) });
            //AddData<Job>(new Job { Id = 13, Name = "Vacaciones Año Pasado", Description = "Vacaciones Año Pasado", Type = JobType.LastYearHollydays, AreaId = 1, Default = false, Acronym = "Y", StartDate = new System.DateTime(2019, 1, 1), EndDate = new System.DateTime(2019, 12, 31) });
            //AddData<Job>(new Job { Id = 14, Name = "Permiso Especial", Description = "Permiso Especial", Type = JobType.SpecialLeave, AreaId = 1, Default = false, Acronym = "E", StartDate = new System.DateTime(2019, 1, 1), EndDate = new System.DateTime(2019, 12, 31) });
            //AddData<Job>(new Job { Id = 15, Name = "Horas Médicas", Description = "Horas Médicas", Type = JobType.DoctorHours, AreaId = 1, Default = false, Acronym = "D", StartDate = new System.DateTime(2019, 1, 1), EndDate = new System.DateTime(2019, 12, 31) });
            //AddData<Job>(new Job { Id = 16, Name = "Baja Médica", Description = "Baja Médica", Type = JobType.SickLeave, AreaId = 1, Default = false, Acronym = "B", StartDate = new System.DateTime(2019, 1, 1), EndDate = new System.DateTime(2019, 12, 31) });
            //AddData<Job>(new Job { Id = 17, Name = "Ausencia Injustificada", Description = "Ausencia Injustificada", Type = JobType.UnjustifiedAbsence, AreaId = 1, Default = false, Acronym = "A", StartDate = new System.DateTime(2019, 1, 1), EndDate = new System.DateTime(2019, 12, 31) });
            //AddData<Job>(new Job { Id = 18, Name = "Ausencia Remunerada", Description = "Ausencia Remunerada", Type = JobType.JustifiedAbsence, AreaId = 1, Default = false, Acronym = "R", StartDate = new System.DateTime(2019, 1, 1), EndDate = new System.DateTime(2019, 12, 31) });
            //AddData<Job>(new Job { Id = 19, Name = "Horas Sindicales", Description = "Horas Sindicales", Type = JobType.UnionHours, AreaId = 1, Default = false, Acronym = "S", StartDate = new System.DateTime(2019, 1, 1), EndDate = new System.DateTime(2019, 12, 31) });
        }

        private void SeedUser()
        {
            AddData<User>(new User
            {
                Id = 1,
                UserName = "Admin",
                NormalizedUserName = "ADMIN",
                Email = "bpoadmin@indrabpo.es",
                NormalizedEmail = "BPOADMIN@INDRABPO.ES",
                PasswordHash = "AQAAAAEAACcQAAAAEHd+gYvi9R3n+68kb5BoeypOIVvAW9BxGvVYQk5Uyt/mp7Ytl37Wh2IhzQ+Y8uQDKw==",
                SecurityStamp = "a5cc793f-8e53-47db-9ad5-303fba7b4c33",
                EmailConfirmed = true,
                LockoutEnabled = false,
                FirstName = "Admin",
                MainSurname = "",
                SecondSurname = "",
                Avatar = "1.jpg"
            });
            AddData<User>(new User
            {
                Id = 2,
                UserName = "acancio",
                NormalizedUserName = "ACANCIO",
                Email = "acancio@indrabpo.es",
                NormalizedEmail = "ACANCIO@INDRABPO.ES",
                PasswordHash = "AQAAAAEAACcQAAAAEHd+gYvi9R3n+68kb5BoeypOIVvAW9BxGvVYQk5Uyt/mp7Ytl37Wh2IhzQ+Y8uQDKw==",
                SecurityStamp = "a5cc793f-8e53-47db-9ad5-303fba7b4c33",
                EmailConfirmed = true,
                LockoutEnabled = false,
                FirstName = "Aarón",
                MainSurname = "Cancio",
                SecondSurname = "Gómez",
                Avatar = "2.jpg"
            });
        }
        private void SeedUserRole()
        {
            AddData<UserRole>(new UserRole { Id = 1, UserId = 1, RoleId = 1 });
            AddData<UserRole>(new UserRole { Id = 2, UserId = 1, RoleId = 2 });
            AddData<UserRole>(new UserRole { Id = 3, UserId = 1, RoleId = 3 });
            AddData<UserRole>(new UserRole { Id = 4, UserId = 1, RoleId = 4 });
            AddData<UserRole>(new UserRole { Id = 5, UserId = 1, RoleId = 5 });
            AddData<UserRole>(new UserRole { Id = 6, UserId = 1, RoleId = 6 });
            AddData<UserRole>(new UserRole { Id = 7, UserId = 1, RoleId = 7 });
            AddData<UserRole>(new UserRole { Id = 8, UserId = 1, RoleId = 8 });
            AddData<UserRole>(new UserRole { Id = 9, UserId = 1, RoleId = 9 });
            AddData<UserRole>(new UserRole { Id = 10, UserId = 1, RoleId = 10 });
            AddData<UserRole>(new UserRole { Id = 11, UserId = 1, RoleId = 11 });
            AddData<UserRole>(new UserRole { Id = 12, UserId = 1, RoleId = 12 });
            AddData<UserRole>(new UserRole { Id = 13, UserId = 1, RoleId = 13 });
            AddData<UserRole>(new UserRole { Id = 14, UserId = 1, RoleId = 14 });
            AddData<UserRole>(new UserRole { Id = 15, UserId = 1, RoleId = 15 });
            AddData<UserRole>(new UserRole { Id = 16, UserId = 1, RoleId = 16 });
            AddData<UserRole>(new UserRole { Id = 17, UserId = 1, RoleId = 17 });

            AddData<UserRole>(new UserRole { Id = 18, UserId = 2, RoleId = 1 });
            AddData<UserRole>(new UserRole { Id = 19, UserId = 2, RoleId = 2 });
            AddData<UserRole>(new UserRole { Id = 20, UserId = 2, RoleId = 3 });
            AddData<UserRole>(new UserRole { Id = 21, UserId = 2, RoleId = 4 });
            AddData<UserRole>(new UserRole { Id = 22, UserId = 2, RoleId = 5 });
            AddData<UserRole>(new UserRole { Id = 23, UserId = 2, RoleId = 6 });
            AddData<UserRole>(new UserRole { Id = 24, UserId = 2, RoleId = 7 });
            AddData<UserRole>(new UserRole { Id = 25, UserId = 2, RoleId = 8 });
            AddData<UserRole>(new UserRole { Id = 26, UserId = 2, RoleId = 9 });
            AddData<UserRole>(new UserRole { Id = 27, UserId = 2, RoleId = 10 });
            AddData<UserRole>(new UserRole { Id = 28, UserId = 2, RoleId = 11 });
            AddData<UserRole>(new UserRole { Id = 29, UserId = 2, RoleId = 12 });
            AddData<UserRole>(new UserRole { Id = 30, UserId = 2, RoleId = 13 });
            AddData<UserRole>(new UserRole { Id = 31, UserId = 2, RoleId = 14 });
            AddData<UserRole>(new UserRole { Id = 32, UserId = 2, RoleId = 15 });
            AddData<UserRole>(new UserRole { Id = 33, UserId = 2, RoleId = 16 });
            AddData<UserRole>(new UserRole { Id = 34, UserId = 2, RoleId = 17 });

        }
        private void SeedUserArea()
        {
            AddData<UserArea>(new UserArea { Id = 1, StartDate = new System.DateTime(1900, 1, 1), Owner = false, Authorized = false, UserId = 1, AreaId = 1 });
            AddData<UserArea>(new UserArea { Id = 2, StartDate = new System.DateTime(1900, 1, 1), Owner = false, Authorized = true, UserId = 1, AreaId = 2 });
            AddData<UserArea>(new UserArea { Id = 3, StartDate = new System.DateTime(1900, 1, 1), Owner = false, Authorized = false, UserId = 2, AreaId = 10 });
            AddData<UserArea>(new UserArea { Id = 4, StartDate = new System.DateTime(1900, 1, 1), Owner = false, Authorized = true, UserId = 2, AreaId = 2 });
        }

        private void SeedDebugArea()
        {
            
        }
        private void SeedDebugJob()
        {
            
           
        }
        private void SeedDebugUser()
        {
            
            AddData<User>(new User
            {
                Id = 3,
                UserName = "aondarra",
                NormalizedUserName = "AONDARRA",
                Email = "aondarra@indrabpo.es",
                NormalizedEmail = "AONDARRA@INDRABPO.ES",
                PasswordHash = "AQAAAAEAACcQAAAAEHd+gYvi9R3n+68kb5BoeypOIVvAW9BxGvVYQk5Uyt/mp7Ytl37Wh2IhzQ+Y8uQDKw==",
                SecurityStamp = "a5cc793f-8e53-47db-9ad5-303fba7b4c33",
                EmailConfirmed = true,
                LockoutEnabled = false,
                FirstName = "Aargoitz",
                MainSurname = "Ondarra",
                SecondSurname = "Vega",
                Avatar = "3.jpg"
            });
            AddData<User>(new User
            {
                Id = 4,
                UserName = "ajfernandezf",
                NormalizedUserName = "AJFERNANDEZF",
                Email = "ajfernandezf@indrabpo.es",
                NormalizedEmail = "AJFERNANDEZF@INDRABPO.ES",
                PasswordHash = "AQAAAAEAACcQAAAAEHd+gYvi9R3n+68kb5BoeypOIVvAW9BxGvVYQk5Uyt/mp7Ytl37Wh2IhzQ+Y8uQDKw==",
                SecurityStamp = "a5cc793f-8e53-47db-9ad5-303fba7b4c33",
                EmailConfirmed = true,
                LockoutEnabled = false,
                FirstName = "Alberto",
                MainSurname = "Fernández",
                SecondSurname = "Fuentes",
                Avatar = "4.jpg"
            });
            AddData<User>(new User
            {
                Id = 5,
                UserName = "mpineda",
                NormalizedUserName = "MPINEDA",
                Email = "mpineda@indrabpo.es",
                NormalizedEmail = "MPINEDA@INDRABPO.ES",
                PasswordHash = "AQAAAAEAACcQAAAAEHd+gYvi9R3n+68kb5BoeypOIVvAW9BxGvVYQk5Uyt/mp7Ytl37Wh2IhzQ+Y8uQDKw==",
                SecurityStamp = "a5cc793f-8e53-47db-9ad5-303fba7b4c33",
                EmailConfirmed = true,
                LockoutEnabled = false,
                FirstName = "María",
                MainSurname = "Pineda",
                SecondSurname = "Ros",
                Avatar = "5.jpg"
            });
            AddData<User>(new User
            {
                Id = 6,
                UserName = "sramon",
                NormalizedUserName = "SRAMON",
                Email = "sramon@indrabpo.es",
                NormalizedEmail = "SRAMON@INDRABPO.ES",
                PasswordHash = "AQAAAAEAACcQAAAAEHd+gYvi9R3n+68kb5BoeypOIVvAW9BxGvVYQk5Uyt/mp7Ytl37Wh2IhzQ+Y8uQDKw==",
                SecurityStamp = "a5cc793f-8e53-47db-9ad5-303fba7b4c33",
                EmailConfirmed = true,
                LockoutEnabled = false,
                FirstName = "Santiago",
                MainSurname = "Ramón",
                SecondSurname = "y Cajal",
                Avatar = "6.jpg"
            });
            AddData<User>(new User
            {
                Id = 7,
                UserName = "sochoa",
                NormalizedUserName = "SOCHOA",
                Email = "sochoa@indrabpo.es",
                NormalizedEmail = "SOCHOA@INDRABPO.ES",
                PasswordHash = "AQAAAAEAACcQAAAAEHd+gYvi9R3n+68kb5BoeypOIVvAW9BxGvVYQk5Uyt/mp7Ytl37Wh2IhzQ+Y8uQDKw==",
                SecurityStamp = "a5cc793f-8e53-47db-9ad5-303fba7b4c33",
                EmailConfirmed = true,
                LockoutEnabled = false,
                FirstName = "Severo",
                MainSurname = "Ochoa",
                SecondSurname = "de Albornoz",
                Avatar = "7.jpg"
            });
        }
        private void SeedDebugUserArea()
        {
            AddData<UserArea>(new UserArea { Id = 5, StartDate = new System.DateTime(1900, 1, 1), Owner = false, Authorized = false, UserId = 3, AreaId = 10 });
            AddData<UserArea>(new UserArea { Id = 6, StartDate = new System.DateTime(1900, 1, 1), Owner = false, Authorized = false, UserId = 4, AreaId = 10 });
            AddData<UserArea>(new UserArea { Id = 7, StartDate = new System.DateTime(1900, 1, 1), Owner = false, Authorized = false, UserId = 5, AreaId = 10 });
            AddData<UserArea>(new UserArea { Id = 8, StartDate = new System.DateTime(1900, 1, 1), Owner = false, Authorized = false, UserId = 6, AreaId = 10 });
            AddData<UserArea>(new UserArea { Id = 9, StartDate = new System.DateTime(1900, 1, 1), Owner = false, Authorized = false, UserId = 7, AreaId = 10 });
        }
        
    }
}
