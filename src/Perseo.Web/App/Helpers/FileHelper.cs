﻿using Perseo.Core;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Drawing;
using System.IO;

namespace Perseo.Web
{
    public static class FileHelper
    {
        public static void SaveFile(IHostingEnvironment enviroment, IFormFile File, String FileName)
        {
            string staticPath = enviroment.ContentRootPath + "/App/Static/";
            var filename = Path.Combine(staticPath, FileName);
            using (var stream = new FileStream(filename, FileMode.Create))
            {
                File.CopyToAsync(stream);
            }
        }

        public static void SaveAvatar(IHostingEnvironment enviroment, string Base64Image, String FileName)
        {
            var filename = Path.Combine(enviroment.ContentRootPath, "App", "Static", "Avatars", FileName);
            //Image image = Base64ToImage(Base64Image);
            //image.Save(filename, System.Drawing.Imaging.ImageFormat.Jpeg);
            File.WriteAllBytes(filename, Convert.FromBase64String(Base64Image));
        }

        public static void DeleteAvatar(IHostingEnvironment enviroment, String FileName)
        {
            var filename = Path.Combine(enviroment.ContentRootPath, "App", "Static","Avatars", FileName);
            if (File.Exists(filename))
            {
                File.Delete(filename);
            }
        }

        public static bool ExistAvatar(IHostingEnvironment enviroment, String FileName)
        {
            var filename = Path.Combine(enviroment.ContentRootPath, "App", "Static", "Avatars", FileName);
            return File.Exists(filename);
        }

        public static Image Base64ToImage(string base64String)
        {
            byte[] imageBytes = Convert.FromBase64String(base64String);
            using (var ms = new MemoryStream(imageBytes, 0, imageBytes.Length))
            {
                Image image = Image.FromStream(ms, true);
                return image;
            }
        }

        public static string ImageToBase64(Image image, System.Drawing.Imaging.ImageFormat format)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                image.Save(ms, format);
                byte[] imageBytes = ms.ToArray();
                string base64String = Convert.ToBase64String(imageBytes);
                return base64String;
            }
        }

        public static FileStreamResult DonwloadExcell(Stream stream, string FileName)
        {
            return new FileStreamResult(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            {
                FileDownloadName = FileName + ".xlsx"
            };
        }
    }


    public interface IStaticFileService
    {
        void SaveFormFile(IFormFile FormFile, string FileName);

        void SaveAvatar(string Base64Image, string FileName);

        void DeleteAvatar(string FileName);
        
        bool ExistAvatar(string FileName);

        FileStreamResult DonwloadExcell(Stream stream, string FileName);
    }


    public class StaticFileService : IStaticFileService
    {
        private readonly string StaticPath;
        private readonly IFileService FileService;

        public StaticFileService(IFileService fileService)
        {
            FileService = fileService;
            StaticPath = "/App/Static/";
        }

        public void SaveFormFile(IFormFile FormFile, string FileName)
        {
            var filename = Path.Combine(StaticPath, FileName);
            using (Stream Stream = new MemoryStream())
            {
                FormFile.CopyTo(Stream);
                FileService.SaveFile(Stream, filename);
            }
        }

        public void SaveAvatar(string Base64Image, string FileName)
        {
            FileService.SaveBase64ImageAsync(Base64Image, Path.Combine(StaticPath, "Avatars", FileName));

        }

        public void DeleteAvatar(string FileName)
        {
            FileService.DeleteFile(Path.Combine(StaticPath, "Avatars", FileName));
        }

        public bool ExistAvatar(string FileName)
        {
            return FileService.ExistFile(Path.Combine(StaticPath, "Avatars", FileName));
        }

        public FileStreamResult DonwloadExcell(Stream stream, string FileName)
        {
            return new FileStreamResult(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            {
                FileDownloadName = FileName + ".xlsx"
            };
        }

        //public static Image Base64ToImage(string base64String)
        //{
        //    byte[] imageBytes = Convert.FromBase64String(base64String);
        //    using (var ms = new MemoryStream(imageBytes, 0, imageBytes.Length))
        //    {
        //        Image image = Image.FromStream(ms, true);
        //        return image;
        //    }
        //}

        //public static string ImageToBase64(Image image, System.Drawing.Imaging.ImageFormat format)
        //{
        //    using (MemoryStream ms = new MemoryStream())
        //    {
        //        image.Save(ms, format);
        //        byte[] imageBytes = ms.ToArray();
        //        string base64String = Convert.ToBase64String(imageBytes);
        //        return base64String;
        //    }
        //}

    }
}

