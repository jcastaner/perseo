﻿namespace Perseo.Web.Helpers
{
    public class JsonResponse
    {
        public bool Result { get; set; }
        public string Response { get; set; }
        public dynamic Content { get; set; }
    }
}

