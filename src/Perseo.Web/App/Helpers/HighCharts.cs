﻿using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.AspNetCore.Html;
using Microsoft.Extensions.Localization;
using Newtonsoft.Json;

namespace Perseo.Web.HighCharts
{
    public interface IHighCharts
    {
        IHtmlContent RenderScript();

        LineChart LineChart(string name);

        AreaChart AreaChart(string name);

        ColumnsChart ColumnsChart(string name);

        BarsChart BarsChart(string name);

        ColumnsAndLineChart ColumnsAndLineChart(string name);

        AreaSplineChart AreaSplineChart(string name);

        GaugeChart GaugeChart(string name);

        TorusChart TorusChart(string name);

        PieChart PieChart(string name);
    }

    public class HighCharts : IHighCharts
    {
        private Dictionary<string, BaseChart> Charts;
        private List<string> ScriptIncludes;
        protected readonly IStringLocalizer<SharedResources> Localizer;

        public HighCharts(IStringLocalizer<SharedResources> localizer)
        {
            Localizer = localizer;
            Charts = new Dictionary<string, BaseChart>();
            ScriptIncludes = new List<string>();
        }

        public IHtmlContent RenderScript()
        {
            string scriptIncludes = "";
            ScriptIncludes = ScriptIncludes.Distinct().ToList();
            foreach (var incl in ScriptIncludes)
            {
                scriptIncludes += $@"$.cachedScript('/lib/highcharts/code/modules/{incl}');";
            }

            string script = $@"if (typeof Highcharts === 'undefined' || Highcharts === null) {{
                        $.cachedScript('/lib/highcharts/code/highcharts.js');
                        $.cachedScript('/lib/highcharts/code/highcharts-more.js');
                        {scriptIncludes}
                        Highcharts.setOptions({{ lang: {{
                            resetZoom: '{Localizer["Reiniciar zoom"]}',
                            drillUpText: '{Localizer["Volver"]}'
                        }}, credits: {{ enabled: false }} }}); }}";

            script += $@"
                        (function (AddTooltipDelay) {{
                            AddTooltipDelay.wrap(AddTooltipDelay.Tooltip.prototype, 'refresh', function(proceed, point, mouseEvent) {{
                                var tooltip = this;
                                if (this.options.delay && tooltip.isHidden) {{
                                    if (point !== this.queuePoint) {{
                                        clearTimeout(this.showTimer);
                                    }}
                                    this.queuePoint = point;
                                    this.showTimer = setTimeout(function () {{
                                        proceed.call(tooltip, point, mouseEvent);
                                    }}, this.options.delay);
                                }} else {{
                                    proceed.call(this, point, mouseEvent);
                                }}
                            }});    
                        }}(Highcharts));
                      ";

            var serializerSettings = new JsonSerializerSettings
            {
                ContractResolver = new Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver()
            };
            foreach (var chart in Charts)
            {
                script += $@"$('#{chart.Key}').highcharts(" + JsonConvert.SerializeObject(chart.Value, serializerSettings) + ");";
            }
            script = $@"<script>{script}</script>";
            return new HtmlString(script.Replace(Environment.NewLine, string.Empty));
        }

        public LineChart LineChart(string name)
        {
            LineChart chart = new LineChart(name);
            ScriptIncludes.Add("drilldown.js");
            Charts.Add(name, chart);
            return chart;
        }

        public AreaChart AreaChart(string name)
        {
            AreaChart chart = new AreaChart(name);
            ScriptIncludes.Add("drilldown.js");
            Charts.Add(name, chart);
            return chart;
        }

        public ColumnsChart ColumnsChart(string name)
        {
            ColumnsChart chart = new ColumnsChart(name);
            ScriptIncludes.Add("drilldown.js");
            Charts.Add(name, chart);
            return chart;
        }

        public BarsChart BarsChart(string name)
        {
            BarsChart chart = new BarsChart(name);
            ScriptIncludes.Add("drilldown.js");
            Charts.Add(name, chart);
            return chart;
        }

        public PieChart PieChart(string name)
        {
            PieChart chart = new PieChart(name);
            ScriptIncludes.Add("drilldown.js");
            Charts.Add(name, chart);
            return chart;
        }

        public ColumnsAndLineChart ColumnsAndLineChart(string name)
        {
            ColumnsAndLineChart chart = new ColumnsAndLineChart(name);
            ScriptIncludes.Add("drilldown.js");
            Charts.Add(name, chart);
            return chart;
        }

        public AreaSplineChart AreaSplineChart(string name)
        {
            AreaSplineChart chart = new AreaSplineChart(name);
            ScriptIncludes.Add("drilldown.js");
            Charts.Add(name, chart);
            return chart;
        }

        public GaugeChart GaugeChart(string name)
        {
            GaugeChart chart = new GaugeChart(name);
            ScriptIncludes.Add("solid-gauge.js");
            Charts.Add(name, chart);
            return chart;
        }

        public TorusChart TorusChart(string name)
        {
            TorusChart chart = new TorusChart(name);
            ScriptIncludes.Add("solid-gauge.js");
            Charts.Add(name, chart);
            return chart;
        }
    }

    // ***********************************************

    public abstract class BaseChart
    {
        protected string Html;
        protected string Script;
        protected string ScriptInclude;
        protected string Name;

        public BaseChart()
        {
            Script = "";
            ScriptInclude = "";
            Html = "";
        }

        public IHtmlContent Render()
        {
            Html = $@"<div id='{Name}'></div>";
            return new HtmlString(Html.Replace(Environment.NewLine, string.Empty));
        }
    }

    public class LineChart : BaseChart
    {
        public Title Title { get; set; }
        public Subtitle Subtitle { get; set; }
        public Chart Chart { get; set; }
        public Exporting Exporting { get; set; }
        public List<Axis> XAxis { get; set; }
        public List<Axis> YAxis { get; set; }
        public Tooltip Tooltip { get; set; }
        public Legend Legend { get; set; }
        public PlotOptions PlotOptions { get; set; }
        public List<Serie> Series { get; set; }
        public Drilldown Drilldown { get; set; }

        public LineChart(string name)
        {
            Script = "";
            Html = "";
            Name = name;
            Title = new Title();
            Subtitle = new Subtitle();
            Chart = new Chart() {
                Height = "200",
                ZoomType = "xy",
                ResetZoomButton = new ButtonOptions()
                {
                    Theme = new Theme() { Fill = "white", Stroke = Constants.color_gray, R = 0, States = new States() { Hover = new Hover() { Fill = Constants.color_gray, Style = new Style() { Color = "white" } } } }
                }
            };
            Exporting = new Exporting();
            XAxis = new List<Axis> { new Axis() { PlotLines = new List<PlotLine>() } };
            YAxis = new List<Axis> { new Axis() { Title = new Title(), PlotLines = new List<PlotLine>() } };
            Tooltip = new Tooltip() { Shared = true, Delay = 500};
            Legend = new Legend()
            {
                Layout = "horizontal",
                Align = "right",
                X = 16,
                VerticalAlign = "top",
                Y = -16,
                Floating = true,
                BackgroundColor = "rgba(255,255,255,0.1)"
            };
            PlotOptions = new PlotOptions() {
                Series = new SeriesOptions() { Stacking = false, StickyTracking = false, Marker = new Marker() { Enabled = false } },
                Spline = new SplineOptions() { },
                Column = new ColumnOptions() { PointPadding = 0, GroupPadding = 0.05 },
                Bar = new BarOptions() { PointPadding = 0, GroupPadding = 0.05 },
                Area = new AreaOptions() { FillOpacity = 0.5 },
                Areaspline = new AreaSplineOptions() { FillOpacity = 0.5 },
                Pie = new PieOptions()
                {
                    Tooltip = new Tooltip()
                    {
                        HeaderFormat = "",
                        PointFormat = "<span style='color:{point.color}'>\u25CF</span> {point.name}: <b>{point.y:.1f}</b>"
                    },
                    DataLabels = new DataLabels() { Enabled = false },
                    ShowInLegend = true,
                    ColorByPoint = true
                }
            };
            Series = new List<Serie>();
            Drilldown = new Drilldown()
            {
                ActiveAxisLabelStyle = new ActiveAxisLabelStyle { TextDecoration = "none", FontWeight ="none", Color = "#333333" },
                DrillUpButton = new ButtonOptions()
                {
                    Theme = new Theme() { Fill = "white", Stroke = Constants.color_gray, R = 0, States = new States() { Hover = new Hover() { Fill = Constants.color_gray, Style = new Style() { Color = "white" } } } }
                }
            };
        }

        public void AddLabels(List<string> labels)
        {
            XAxis.First().Categories = labels.ToList();
        }

        public void AddData(string name, List<double> data, string color = null, string type = null)
        {
            type = type ?? Chart.Type;

            List<Data> datalist = new List<Data>();
            foreach (double value in data) { datalist.Add(new Data() { Y = value }); }

            Series.Add(new Serie() { Name = name, Type = type, Data = datalist, Color = color});
        }

        public void AddComplexData(string name, List<Data> data, string color = null)
        {
            XAxis.First().Type = "category";

            List<Data> datalist = new List<Data>();
            foreach (Data dataobject in data) { datalist.Add(new Data() { Name= dataobject.Name, Color = dataobject.Color, Y = dataobject.Y, Drilldown = dataobject.Drilldown }); }
                       
            Series.Add(new Serie() { Name = name, Data = datalist, Color = color });
        }
        public void AddDrilldown(string id, List<Data> data, string name = null, string color = null)
        {
            Drilldown.Series.Add(new Serie() { Id = id, Data = data.ToList(), Name = name, Color = color });
        }

        public void Stack(bool stack)
        {
            PlotOptions.Series.Stacking = stack;
        }
    }

    public class AreaChart : LineChart
    {
        public AreaChart(string name) : base(name) { Chart.Type = "area"; }
    }
    
    public class ColumnsChart : LineChart
    {
        public ColumnsChart(string name) : base(name) { Chart.Type = "column"; }
    }
    
    public class BarsChart : LineChart
    {
        public BarsChart(string name) : base(name) { Chart.Type = "bar"; }
    }

    public class PieChart : LineChart
    {
        public PieChart(string name) : base(name)
        {
            Chart.Type = "pie";
            Legend = new Legend()
            {
                BackgroundColor = "rgba(255,255,255,0.1)",
            };
        }

        public void AddData(string name, double data, string color = null)
        {
            if (Series.Count() < 1) { Series.Add(new Serie() { }); }
            Series.First().Data.Add(new Data() { Name = name, Color = color, Y = data });
        }
    }

    public class ColumnsAndLineChart : LineChart
    {
        public ColumnsAndLineChart(string name) : base(name)
        {
            YAxis = new List<Axis> {
                new Axis(){
                    Labels = new Labels(){ Enabled = true, Format = "{value}", Style = new Style(){ Color ="black" } },
                    Title = new Title()
                },
                new Axis(){
                    Labels = new Labels(){ Enabled = true, Style = new Style(){ Color ="black" } },
                    Title = new Title(),
                    Visible = false,
                    Opposite = true
                }
            };
        }

        public void AddColumnData(string name, List<double> data, string color = Constants.color_dark_gray)
        {
            AddData(name, data, color, "column");
            Series.First().YAxis = 1;
        }

        public void AddLineData(string name, List<double> data, string color = Constants.color_dark_gray)
        {
            AddData(name, data, color, "spline");
        }
    }

    public class AreaSplineChart : LineChart
    {
        public AreaSplineChart(string name) : base(name) { Chart.Type = "areaspline"; }

        public void SetReducedFormat()
        {
            Chart.Height = "70";
            Chart.ZoomType = "none";
            Chart.Margin = new List<int> { 0, -30, 0, -30 };
            Chart.BackgroundColor = "rgba(255,255,255,0)";
            Chart.PlotBackgroundColor = "rgba(255,255,255,0)";
            YAxis.First().Max = (int)Series.First().Data.Select(d => d.Y).Max() + 1;
            YAxis.First().Min = Series.First().Data.Select(d => d.Y).Min() == 0 ? 0 : (int)Series.First().Data.Select(d => d.Y).Min() - 1;
            XAxis.First().Categories.Add("");
            XAxis.First().Categories.Insert(0, "");
            Series.First().Data.Add(new Data() { Y = 0 });
            Series.First().Data.Insert(0, new Data() { Y = 0 });
            Legend.Enabled = false;
            Tooltip.Outside = true;
            PlotOptions.Series.Marker.Enabled = true;
            PlotOptions.Series.Marker.Radius = 3;
            PlotOptions.Areaspline.FillOpacity = 0.7;
        }
    }

    public class GaugeChart : BaseChart
    {
        public Title Title { get; set; }
        public Subtitle Subtitle { get; set; }
        public Chart Chart { get; set; }
        public Tooltip Tooltip { get; set; }
        public Exporting Exporting { get; set; }
        public Pane Pane { get; set; }
        public PlotOptions PlotOptions { get; set; }
        public Axis YAxis { get; set; }
        public List<Serie> Series { get; set; }

        public GaugeChart(string name)
        {
            Script = "";
            Html = "";
            Name = name;
            Title = new Title();
            Subtitle = new Subtitle();
            Chart = new Chart("gauge")
            {
                Height = "200",
                Margin = new List<int> { 0, 0, 0, 0 }
            };
            Tooltip = new Tooltip() { ValueSuffix = "%" };
            Exporting = new Exporting();
            Pane = new Pane()
            {
                Center = new List<string>() { "50%", "75%" },
                Size = "120%",
                StartAngle = -90,
                EndAngle = 90,
                Background = null
            };
            PlotOptions = new PlotOptions()
            {
                Gauge = new GaugeOptions() { DataLabels = new DataLabels() { Enabled = false } }
            };
            YAxis = new Axis()
            {
                Labels = new Labels() { Enabled = false },
                MinorTickLength = 0,
                Min = 0,
                Max = 100,
                TickLength = 0,
                PlotBands = new List<PlotBand>
                {
                    new PlotBand(0, 33, Constants.color_1, "50%"),
                    new PlotBand(33, 67, Constants.color_light_blue, "50%"),
                    new PlotBand(67, 100, "#E8E8E8", "50%")
                }
            };
            Series = new List<Serie>();
        }

        public void AddData(string name, List<double> data, string color = Constants.color_dark_gray)
        {
            List<Data> datalist = new List<Data>();
            foreach (double value in data) { datalist.Add(new Data() { Y = value }); }

            DataLabels dataLabels = new DataLabels()
            {
                Format = "<div style='text-align:center'><span style='font-size:25px;color:' + ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '>{y}</span></div>"
            };

            Series.Add(new Serie() { Name = name, Data = datalist, DataLabels = dataLabels });
        }
    }

    public class TorusChart : BaseChart
    {
        public Title Title { get; set; }
        public Subtitle Subtitle { get; set; }
        public Chart Chart { get; set; }
        public Tooltip Tooltip { get; set; }
        public Exporting Exporting { get; set; }
        public Pane Pane { get; set; }
        public PlotOptions PlotOptions { get; set; }
        public Axis YAxis { get; set; }
        public List<Serie> Series { get; set; }

        public TorusChart(string name)
        {
            Script = "";
            Html = "";
            Name = name;
            Title = new Title();
            Subtitle = new Subtitle();
            Chart = new Chart("solidgauge") { Height = "70%" };
            Tooltip = new Tooltip();
            Exporting = new Exporting();
            Pane = new Pane()
            {
                StartAngle = 0,
                EndAngle = 360,
                Background = new List<Background> { new Background() {
                    OuterRadius = "100%",
                    InnerRadius ="75%",
                    BackgroundColor = Constants.color_dark_gray,
                    BorderWidth = 0
                } }
            };
            PlotOptions = new PlotOptions()
            {
                Solidgauge = new SolidgaugeOptions()
                {
                    DataLabels = new DataLabels()
                    {
                        Enabled = true,
                        Y = -40,
                        BorderWidth = 0,
                        BackgroundColor = "none",
                        UseHTML = true,
                        Shadow = false,
                        Style = new Style() { FontSize = "16px" },
                        Formatter = @"function () { return '<div style=""width:100%;text-align:center;""><span style=""font-size:1.2em;color: black;font-weight:bold;"">' + Highcharts.numberFormat(this.y, 0) + '%</span>';}"
                    },
                    Linecap = "round",
                    StickyTracking = false,
                    Rounded = true
                }
            };
            YAxis = new Axis()
            {
                Min = 0,
                Max = 100,
                LineWidth = 0,
                TickPositions = new List<int>()
            };
            Series = new List<Serie>();
        }

        public void AddData(string name, double data)
        {
            Series.Add(new Serie()
            {
                Name = name,
                ColorByPoint = true,
                Data = new List<Data> { new Data() {
                Color = Constants.color_light_blue,
                Radius  =  "100%",
                InnerRadius = "75%",
                Y = data
            } }
            });
        }
    }
    
    // ***********************************************

    public class PlainJsonStringConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(string);
        }
        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            return reader.Value;
        }
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            writer.WriteRawValue((string)value);
        }
    }

    // ***********************************************

    [JsonObject(ItemNullValueHandling = NullValueHandling.Ignore)]
    public class Title
    {
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Text { get; set; }

        public Title()
        {
            Text = "";
        }
    }

    [JsonObject(ItemNullValueHandling = NullValueHandling.Ignore)]
    public class Chart
    {
        public Chart(string type = null)
        {
            Type = type;
        }

        public string Type { get; set; }
        public string Height { get; set; }
        public string Width { get; set; }
        public string ZoomType { get; set; }
        public ButtonOptions ResetZoomButton { get; set; }
        public string BackgroundColor { get; set; }
        public string PlotBackgroundColor { get; set; }
        public string PlotBackgroundImage { get; set; }
        public int? PlotBorderWidth { get; set; }
        public bool? PlotShadow { get; set; }
        public List<int> Margin { get; set; }
    }

    [JsonObject(ItemNullValueHandling = NullValueHandling.Ignore)]
    public class ButtonOptions
    {
        public ButtonOptions() { }

        public Theme Theme { get; set; }
    }

    [JsonObject(ItemNullValueHandling = NullValueHandling.Ignore)]
    public class Theme
    {
        public Theme() { }

        public string Fill { get; set; }
        public string Stroke { get; set; }
        public int R { get; set; }
        public States States { get; set; }
    }

    [JsonObject(ItemNullValueHandling = NullValueHandling.Ignore)]
    public class States
    {
        public States() { }

        public Hover Hover { get; set; }
    }

    [JsonObject(ItemNullValueHandling = NullValueHandling.Ignore)]
    public class Hover
    {
        public Hover() { }

        public string Fill { get; set; }
        public Style Style { get; set; }
    }


    [JsonObject(ItemNullValueHandling = NullValueHandling.Ignore)]
    public class Subtitle
    {
        public Subtitle()
        {
            Text = null;
        }

        public string Text { get; set; }
    }

    [JsonObject(ItemNullValueHandling = NullValueHandling.Ignore)]
    public class Exporting
    {
        public Exporting()
        {
            Enabled = false;
        }

        public bool? Enabled { get; set; }
    }

    [JsonObject(ItemNullValueHandling = NullValueHandling.Ignore)]
    public class Pane
    {
        public Pane()
        {
            Center = new List<string>();
            Background = new List<Background>();
        }

        public List<string> Center { get; set; }
        public string Size { get; set; }
        public int? StartAngle { get; set; }
        public int? EndAngle { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
        public List<Background> Background { get; set; }
    }

    [JsonObject(ItemNullValueHandling = NullValueHandling.Ignore)]
    public class Background
    {
        public Background() { }

        public string OuterRadius { get; set; }
        public string InnerRadius { get; set; }
        public string BackgroundColor { get; set; }
        public int? BorderWidth { get; set; }
    }

    [JsonObject(ItemNullValueHandling = NullValueHandling.Ignore)]
    public class PlotOptions
    {
        public PlotOptions() { }

        public SeriesOptions Series { get; set; }
        public GaugeOptions Gauge { get; set; }
        public SolidgaugeOptions Solidgauge { get; set; }
        public SplineOptions Spline { get; set; }
        public ColumnOptions Column { get; set; }
        public BarOptions Bar { get; set; }
        public AreaOptions Area { get; set; }
        public AreaSplineOptions Areaspline { get; set; }
        public PieOptions Pie { get; set; }
    }

    [JsonObject(ItemNullValueHandling = NullValueHandling.Ignore)]
    public class GaugeOptions
    {
        public GaugeOptions() { }

        public DataLabels DataLabels { get; set; }
        public Dial Dial { get; set; }
        public Pivot Pivot { get; set; }
    }

    [JsonObject(ItemNullValueHandling = NullValueHandling.Ignore)]
    public class SolidgaugeOptions
    {
        public SolidgaugeOptions()
        {
            DataLabels = new DataLabels();
        }

        public DataLabels DataLabels { get; set; }
        public string Linecap { get; set; }
        public bool? StickyTracking { get; set; }
        public bool? Rounded { get; set; }
    }

    [JsonObject(ItemNullValueHandling = NullValueHandling.Ignore)]
    public class DataLabels
    {
        public DataLabels()
        {
            Enabled = false;
            Format = null;
        }

        public bool Enabled { get; set; }
        public int? Y { get; set; }
        public int? BorderWidth { get; set; }
        public string BackgroundColor { get; set; }
        public bool? UseHTML { get; set; }
        public bool? Shadow { get; set; }
        public Style Style { get; set; }
        public string Format { get; set; }
        [JsonConverter(typeof(PlainJsonStringConverter))]
        public string Formatter { get; set; }
    }

    [JsonObject(ItemNullValueHandling = NullValueHandling.Ignore)]
    public class Dial
    {
        public Dial()
        {
            BaseLength = "0%";
            Radius = "100%";
            RearLength = "0%";
            TopWidth = 1;
            BaseWidth = 5;
            BorderWidth = 1;
            BorderColor = "#536C75";
            BackgroundColor = "#fff";
        }
        public string BaseLength { get; set; }
        public string Radius { get; set; }
        public string RearLength { get; set; }
        public int? TopWidth { get; set; }
        public int? BaseWidth { get; set; }
        public int? BorderWidth { get; set; }
        public string BorderColor { get; set; }
        public string BackgroundColor { get; set; }
    }

    [JsonObject(ItemNullValueHandling = NullValueHandling.Ignore)]
    public class Pivot
    {
        public Pivot()
        {
            Radius = 3;
            BorderWidth = 1;
            BorderColor = "#536C75";
            BackgroundColor = "#fff";
        }

        public int? Radius { get; set; }
        public int? BorderWidth { get; set; }
        public string BorderColor { get; set; }
        public string BackgroundColor { get; set; }
    }

    [JsonObject(ItemNullValueHandling = NullValueHandling.Ignore)]
    public class Axis
    {
        public Axis()
        {
            Margin = 0;
        }

        public Labels Labels { get; set; }
        public Title Title { get; set; }
        public string Type { get; set; }
        public int? MinorTickLength { get; set; }
        public int? Min { get; set; }
        public int? Max { get; set; }
        public int? Margin { get; set; }
        public int? TickLength { get; set; }
        public List<PlotBand> PlotBands { get; set; }
        public List<PlotLine> PlotLines { get; set; }
        public List<string> Categories { get; set; }
        public bool? Crosshair { get; set; }
        public bool? Visible { get; set; }
        public bool? Opposite { get; set; }
        public int? LineWidth { get; set; }
        public List<int> TickPositions { get; set; }
    }

    [JsonObject(ItemNullValueHandling = NullValueHandling.Ignore)]
    public class Labels
    {
        public Labels() { }

        public bool? Enabled { get; set; }
        public string Format { get; set; }
        public Style Style { get; set; }
    }

    [JsonObject(ItemNullValueHandling = NullValueHandling.Ignore)]
    public class Style
    {
        public Style() { }

        public string Color { get; set; }
        public string FontSize { get; set; }
    }

    [JsonObject(ItemNullValueHandling = NullValueHandling.Ignore)]
    public class Tooltip
    {
        public Tooltip() { }

        public bool? Shared { get; set; }
        public bool? Outside { get; set; }
        public string HeaderFormat { get; set; }
        public string PointFormat { get; set; }
        public string ValueSuffix { get; set; }
        public int? Delay { get; set; }
    }

    [JsonObject(ItemNullValueHandling = NullValueHandling.Ignore)]
    public class SplineOptions
    {
        public SplineOptions() { }

        public Marker Marker { get; set; }
    }

    [JsonObject(ItemNullValueHandling = NullValueHandling.Ignore)]
    public class Marker
    {
        public Marker() { }

        public bool? Enabled { get; set; }
        public int? Radius{ get; set; }
    }

    public class SeriesOptions
    {
        public SeriesOptions() { }

        public bool? Stacking { get; set; }
        public bool? StickyTracking { get; set; }
        public string NegativeColor { get; set; }
        public Marker Marker { get; set; }
    }

    public class AreaOptions
    {
        public AreaOptions() { }

        public double? FillOpacity { get; set; }
    }

    public class AreaSplineOptions : AreaOptions
    {
        public AreaSplineOptions() { }
    }

    public class ColumnOptions
    {
        public ColumnOptions() { }

        public double? GroupPadding { get; set; }
        public double? PointPadding { get; set; }
    }

    public class BarOptions : ColumnOptions
    {
        public BarOptions() { }
    }

    public class PieOptions
    {
        public PieOptions()
        {
            ShowInLegend = true;
        }
        public bool? ShowInLegend { get; set; }
        public bool? ColorByPoint { get; set; }
        public Tooltip Tooltip { get; set; }
        public DataLabels DataLabels { get; set; }
    }

    [JsonObject(ItemNullValueHandling = NullValueHandling.Ignore)]
    public class Legend
    {
        public Legend() { }

        public string Layout { get; set; }
        public string Align { get; set; }
        public int? X { get; set; }
        public string VerticalAlign { get; set; }
        public int? Y { get; set; }
        public bool? Floating { get; set; }
        public bool? Enabled { get; set; }
        public string BackgroundColor { get; set; }
    }

    [JsonObject(ItemNullValueHandling = NullValueHandling.Ignore)]
    public class PlotBand
    {
        public PlotBand(int from, int to, string color, string thickness)
        {
            From = from;
            To = to;
            Color = color;
            Thickness = thickness;
        }

        public int? From { get; set; }
        public int? To { get; set; }
        public string Color { get; set; }
        public string Thickness { get; set; }
    }

    [JsonObject(ItemNullValueHandling = NullValueHandling.Ignore)]
    public class PlotLine
    {
        public PlotLine()
        {
            Width = 3;
        }

        public double Value { get; set; }
        public double? Width { get; set; }
        public string Color { get; set; }
    }

    [JsonObject(ItemNullValueHandling = NullValueHandling.Ignore)]
    public class Serie
    {
        public Serie()
        {
            Data = new List<Data>();
        }

        public string Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string Color { get; set; }
        public string NegativeColor { get; set; }
        public bool? ColorByPoint { get; set; }
        public int YAxis { get; set; }
        public List<Data> Data { get; set; }
        public DataLabels DataLabels { get; set; }
    }

    [JsonObject(ItemNullValueHandling = NullValueHandling.Ignore)]
    public class Drilldown
    {
        public Drilldown()
        {
            Series = new List<Serie>();
        }

        public bool? AllowPointDrilldown { get; set; }
        public ButtonOptions DrillUpButton { get; set; }
        public List<Serie> Series { get; set; }
        public ActiveAxisLabelStyle ActiveAxisLabelStyle { get; set; }
    }

    [JsonObject(ItemNullValueHandling = NullValueHandling.Ignore)]
    public class ActiveAxisLabelStyle
    {
        public ActiveAxisLabelStyle() { }

        public string TextDecoration { get; set; }
        public string FontWeight { get; set; }
        public string Color { get; set; }
    }

    [JsonObject(ItemNullValueHandling = NullValueHandling.Ignore)]
    public class Data
    {
        public Data() { }

        public string Name { get; set; }
        public string Color { get; set; }
        public string Radius { get; set; }
        public string InnerRadius { get; set; }
        public double Y { get; set; }
        public string Drilldown { get; set; }
    }
}