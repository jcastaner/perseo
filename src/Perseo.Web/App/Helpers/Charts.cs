﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Html;
using Newtonsoft.Json;

namespace Perseo.Web.Charts
{

    public interface ICharts
    {
        IHtmlContent RenderScript();

        LineChart LineChart(string name);
    }

    public class Charts : ICharts
    {
        private Dictionary<string, BaseChart> MyCharts;

        public Charts()
        {
            MyCharts = new Dictionary<string, BaseChart>();
        }

        public IHtmlContent RenderScript()
        {
            string script = $@"<script src='/lib/chartjs/dist/Chart.min.js'></script>
                <script>
                    Chart.defaults.global.pointHitDetectionRadius = 1;
                    Chart.defaults.global.tooltips.enabled = true;
                    Chart.defaults.global.tooltips.mode = 'index';
                    Chart.defaults.global.tooltips.position = 'nearest';
                </script>";

            var serializerSettings = new JsonSerializerSettings
            {
                ContractResolver = new Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver()
            };
            foreach (var chart in MyCharts)
            {
                script = $@"var {chart.Key} = new Chart($('#{chart.Key}')," + JsonConvert.SerializeObject(this, serializerSettings) + ");";

            }
            script = $@"<script>{script}</script>";
            return new HtmlString(script.Replace(Environment.NewLine, string.Empty));
        }

        public LineChart LineChart(string name)
        {
            LineChart chart = new LineChart(name);
            MyCharts.Add(name, chart);
            return chart;
        }
    }

    // ***********************************************

    public abstract class BaseChart
    {
        protected string Html;
        protected string Script;
        protected string Name;

        public BaseChart()
        {
            Script = "";
            Html = "";
        }

        public IHtmlContent Render()
        {
            Html = $@"<canvas class='chart chartjs-render-monitor' id='{Name}' height='70' width='388' style='display: block; width: 388px; height: 70px;'></canvas>";
            return new HtmlString(Html.Replace(Environment.NewLine, string.Empty));
        }
    }

    public class LineChart : BaseChart
    {
        public string Type { get; set; }
        public Options Options { get; set; }
        public Data Data { get; set; }

        public LineChart(string name)
        {
            Script = "";
            Html = "";
            Name = name;
            Type = "line";
            Options = new Options() {
                MaintainAspectRatio = false,
                Legend = new Legend() {
                    Display = false
                },
                Scales = new Scales() { 
                    XAxe = new Axe() {
                        Display = false
                    },
                    YAxe = new Axe() {
                        Display = false
                    },
                },
                Elements = new Elements() {
                    Line = new Line() {
                        BorderWidth = 1
                    },
                    Point = new Point() {
                        Radius = 3,
                        HitRadius = 10,
                        HoverRadius = 3
                    }
                }
            };
            Data = new Data();
        }

        public void AddData(string name, List<double> data, string color = "rgb(83, 108, 117)", string backgroundColor = "rgb(83, 108, 117, 0.6)")
        {
            Data.Datasets.Add(new Dataset() { Label = name, Data = data, BorderColor = color, BackgroundColor = backgroundColor });
        }

        public void AddLabels(List<string> labels)
        {
            Data.Labels = labels;
        }
    }

    // ***********************************************

    public class PlainJsonStringConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(string);
        }
        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            return reader.Value;
        }
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            writer.WriteRawValue((string)value);
        }
    }

    // ***********************************************

    [JsonObject(ItemNullValueHandling = NullValueHandling.Ignore)]
    public class Data
    {
        public List<string> Labels { get; set; }
        public List<Dataset> Datasets { get; set; }

        public Data()
        {
            Labels = new List<string>();
            Datasets = new List<Dataset>();
        }
    }

    [JsonObject(ItemNullValueHandling = NullValueHandling.Ignore)]
    public class Dataset
    {
        public string Label { get; set; }
        public string BackgroundColor { get; set; }
        public string BorderColor { get; set; }
        public List<double> Data;

        public Dataset()
        {
                Data = new List<double>();
        }
    }

    [JsonObject(ItemNullValueHandling = NullValueHandling.Ignore)]
    public class Options
    {
        public bool? MaintainAspectRatio { get; set; }
        public Legend Legend { get; set; }
        public Scales Scales { get; set; }
        public Elements Elements { get; set; }

        public Options()
        {
        }
    }

    [JsonObject(ItemNullValueHandling = NullValueHandling.Ignore)]
    public class Legend
    {
        public bool? Display { get; set; }

        public Legend()
        {
        }
    }

    [JsonObject(ItemNullValueHandling = NullValueHandling.Ignore)]
    public class Scales
    {
        public Scales()
        {
        }
        public Axe XAxe { get; set; }
        public Axe YAxe { get; set; }
    }

    [JsonObject(ItemNullValueHandling = NullValueHandling.Ignore)]
    public class Axe
    {
        public bool? Display { get; set; }

        public Axe()
        {
        }
    }

    [JsonObject(ItemNullValueHandling = NullValueHandling.Ignore)]
    public class Elements
    {
        public Line Line { get; set; }
        public Point Point { get; set; }

        public Elements()
        {
        }

    }

    [JsonObject(ItemNullValueHandling = NullValueHandling.Ignore)]
    public class Line
    {
        public Line()
        {
        }
        public int? BorderWidth { get; set; }

    }

    [JsonObject(ItemNullValueHandling = NullValueHandling.Ignore)]
    public class Point
    {
        public Point()
        {
        }
        public int? Radius { get; set; }
        public int? HitRadius { get; set; }
        public int? HoverRadius { get; set; }
    }

}
