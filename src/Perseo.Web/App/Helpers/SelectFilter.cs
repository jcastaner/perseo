﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace Perseo.Web
{
    public class SelectFilter<T> where T : class
    {
        public SelectFilter(List<T> Items, List<int> SelectedItems, string Related = null)
        {
            this.Name = typeof(T).ToString();
            this.Items = Items;
            this.SelectedItems = SelectedItems;
            this.Parent = typeof(T).GetType().GetProperty("ParentId") != null ? true : false;
            this.Related = Related;
        }

        public string Name { get; set; }
        public List<T> Items { get; set; }
        public List<int> SelectedItems { get; set; }
        public bool Parent { get; set; }
        public string Related { get; set; }
    }
}

