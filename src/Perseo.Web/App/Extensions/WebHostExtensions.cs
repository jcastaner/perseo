﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Perseo.Infraestructure;
using Microsoft.EntityFrameworkCore;

namespace Perseo.Web
{
    public static class WebHostExtensions
    {
        public static IWebHost SeedData(this IWebHost host)
        {
            using (var scope = host.Services.CreateScope())
            {
                var services = scope.ServiceProvider;
                var context = services.GetService<ApplicationContext>();

                //context.Database.Migrate();

                new DataSeeder(context).SeedData();
                #if DEBUG
                new DataSeeder(context).SeedDebugData();
                #endif
            }
            return host;
        }
    }
}


