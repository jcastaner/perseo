using Microsoft.AspNetCore.Mvc.Filters;

using System.Linq;


namespace Microsoft.AspNetCore.Mvc
{
    public class ValidateAjaxModelStateAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (!filterContext.HttpContext.Request.IsAjax())
                return;
            var modelState = filterContext.ModelState;
            if (!modelState.IsValid)
            {
                var errorModel =  from x in modelState.Keys where modelState[x].Errors.Count > 0 select new
                        {
                            key = x,
                            errors = modelState[x].Errors.Select(y => y.ErrorMessage).ToArray()
                        };
                filterContext.Result = new JsonResult(
                new {
                    Result = false,
                    Response = "OOOPS! ALGO SALI� MAL (400)",
                    Content = errorModel
                });
                filterContext.HttpContext.Response.StatusCode = 400;
            }
        }
    }
}
