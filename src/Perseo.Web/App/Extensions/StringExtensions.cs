
namespace Microsoft.AspNetCore.Mvc
{
    public static class StringExtensions
    {
        public static string Truncate(this string value, int maxChars)
        {
            if (value == null) return value;
            return value.Length <= maxChars ? value : value.Substring(0, maxChars) + (char)0x2026;
        }
    }
}
