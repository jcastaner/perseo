﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;

namespace Perseo.Web
{
    public static class AppRole
    {
        [Display(Name = "Administración")]
        public const string Administration = "Administration";

        [Display(Name = "Mantenimiento de la apliación")]
        public const string Maintenance = "Maintenance";

        [Display(Name = "Gestión Económica (consulta)")]
        public const string EconomicalManagementShow = "EconomicalManagementShow";
        [Display(Name = "Gestión Económica (edición)")]
        public const string EconomicalManagementEdit = "EconomicalManagementEdit";

        [Display(Name = "Gestión Técnica (consulta)")]
        public const string TecnicalManagementShow = "TecnicalManagementShow";
        [Display(Name = "Gestión Técnica (edición)")]
        public const string TecnicalManagementEdit = "TecnicalManagementEdit";

        [Display(Name = "Gestión Operativa (consulta)")]
        public const string OperationalManagementShow = "OperationalManagementShow";
        [Display(Name = "Gestión Operativa (edición)")]
        public const string OperationalManagementEdit = "OperationalManagementEdit";

        [Display(Name = "Gestión de Calidad (consulta)")]
        public const string QualityManagementShow = "QualityManagementShow";
        [Display(Name = "Gestión de Calidad (edición)")]
        public const string QualityManagementEdit = "QualityManagementEdit";

        [Display(Name = "Gestión de Formación (consulta)")]
        public const string TrainingManagementShow = "TrainingManagementShow";
        [Display(Name = "Gestión de Formación (consulta)")]
        public const string TrainingManagementEdit = "TrainingManagementEdit";

        [Display(Name = "Gestión de Tecnología (consulta)")]
        public const string ItManagementShow = "ItManagementShow";
        [Display(Name = "Gestión de Tecnología (edición)")]
        public const string ItManagementEdit = "ItManagementEdit";

        [Display(Name = "Gestión de Recursos Humanos (consulta)")]
        public const string HHRRManagementShow = "HHRRManagementShow";
        [Display(Name = "Gestión de Recursos Humanos (edición)")]
        public const string HHRRManagementEdit = "HHRRManagementEdit";

        [Display(Name = "Operación")]
        public const string Operation = "Operation";

        public static Dictionary<string, string> Roles()
        {
            Dictionary<string, string>  dic = new Dictionary<string, string>();

            Type type = typeof(AppRole);
            List<FieldInfo> fields = type.GetFields(BindingFlags.Public | BindingFlags.Static | BindingFlags.FlattenHierarchy)
                .Where(fi => fi.IsLiteral && !fi.IsInitOnly).ToList();
            foreach (FieldInfo f in fields)
            {
                var attribute = f.GetCustomAttributes(typeof(DisplayAttribute), true).Cast<DisplayAttribute>().FirstOrDefault();
                string displayName = attribute?.Name;
                dic.Add(f.Name, displayName);
            }
            return dic;
        }

        public static Dictionary<string, string> Roles(List<string> selectedRoles)
        {
            var allRoles = Roles();
            return allRoles.Where(n => selectedRoles.Contains(n.Key)).ToDictionary(v => v.Key, v => v.Value);
        }
    }

    public static class AppProfile
    {
        [Display(Name = "Administrador")]
        public static List<string> Admin;

        [Display(Name = "Superusuario")]
        public static List<string> SuperUser;

        [Display(Name = "Director")]
        public static List<string> Director;
        [Display(Name = "Gerente de Operaciones")]
        public static List<string> UpperManager;
        [Display(Name = "Jefe de Area/Servicio")]
        public static List<string> MiddleManager;
        [Display(Name = "Jefe de Departamento")]
        public static List<string> LowerManager;
        [Display(Name = "Jefe de Equipo")]
        public static List<string> TeamManger;

        [Display(Name = "Personal de Soporte")]
        public static List<string> SupportStaff;
        [Display(Name = "Personal de Reporte")]
        public static List<string> ReportingStaff;
        [Display(Name = "Personal de Calidad")]
        public static List<string> QualityStaff;
        [Display(Name = "Personal de Formación")]
        public static List<string> TrainingStaff;
        [Display(Name = "Personal de Tecnología")]
        public static List<string> ItStaff;
        [Display(Name = "Personal de RRHH")]
        public static List<string> RRHHStaff;

        [Display(Name = "Personal")]
        public static List<string> Staff;

        static AppProfile()
        {
            Admin = new List<string>() {
                AppRole.Administration, AppRole.Maintenance,
                AppRole.EconomicalManagementShow, AppRole.EconomicalManagementEdit,
                AppRole.TecnicalManagementEdit, AppRole.TecnicalManagementShow,
                AppRole.OperationalManagementEdit, AppRole.OperationalManagementShow,
                AppRole.QualityManagementEdit, AppRole.QualityManagementShow,
                AppRole.TrainingManagementEdit, AppRole.TrainingManagementShow,
                AppRole.HHRRManagementEdit, AppRole.HHRRManagementShow, 
                AppRole.ItManagementEdit, AppRole.ItManagementShow,
                AppRole.Operation
            };
            SuperUser = new List<string>() {
                AppRole.Maintenance,
                AppRole.EconomicalManagementShow, AppRole.EconomicalManagementEdit,
                AppRole.TecnicalManagementEdit, AppRole.TecnicalManagementShow,
                AppRole.OperationalManagementEdit, AppRole.OperationalManagementShow,
                AppRole.QualityManagementEdit, AppRole.QualityManagementShow,
                AppRole.TrainingManagementEdit, AppRole.TrainingManagementShow,
                AppRole.HHRRManagementEdit, AppRole.HHRRManagementShow,
                AppRole.ItManagementEdit, AppRole.ItManagementShow,
                AppRole.Operation
            };
            Director = new List<string>() { 
                AppRole.EconomicalManagementShow, AppRole.EconomicalManagementEdit,
                AppRole.TecnicalManagementEdit, AppRole.TecnicalManagementShow,
                AppRole.OperationalManagementEdit, AppRole.OperationalManagementShow,
                AppRole.QualityManagementEdit, AppRole.QualityManagementShow,
                AppRole.TrainingManagementEdit, AppRole.TrainingManagementShow,
                AppRole.HHRRManagementEdit, AppRole.HHRRManagementShow,
                AppRole.ItManagementEdit, AppRole.ItManagementShow,
                AppRole.Operation
            };
            UpperManager = new List<string>() {
                AppRole.EconomicalManagementShow, AppRole.EconomicalManagementEdit,
                AppRole.TecnicalManagementEdit, AppRole.TecnicalManagementShow,
                AppRole.OperationalManagementEdit, AppRole.OperationalManagementShow,
                AppRole.QualityManagementEdit, AppRole.QualityManagementShow,
                AppRole.TrainingManagementEdit, AppRole.TrainingManagementShow,
                AppRole.HHRRManagementEdit, AppRole.HHRRManagementShow,
                AppRole.ItManagementEdit, AppRole.ItManagementShow,
                AppRole.Operation
            };
            MiddleManager = new List<string>() { 
                AppRole.EconomicalManagementShow, AppRole.EconomicalManagementEdit,
                AppRole.TecnicalManagementEdit, AppRole.TecnicalManagementShow,
                AppRole.OperationalManagementEdit, AppRole.OperationalManagementShow,
                AppRole.QualityManagementEdit, AppRole.QualityManagementShow,
                AppRole.TrainingManagementEdit, AppRole.TrainingManagementShow,
                AppRole.HHRRManagementShow,
                AppRole.ItManagementShow,
                AppRole.Operation
            };
            LowerManager = new List<string>() {
                AppRole.EconomicalManagementShow,
                AppRole.TecnicalManagementEdit, AppRole.TecnicalManagementShow,
                AppRole.OperationalManagementEdit, AppRole.OperationalManagementShow,
                AppRole.QualityManagementEdit, AppRole.QualityManagementShow,
                AppRole.TrainingManagementEdit, AppRole.TrainingManagementShow,
                AppRole.ItManagementShow,
                AppRole.Operation
            };
            TeamManger = new List<string>() {
                AppRole.TecnicalManagementShow,
                AppRole.OperationalManagementEdit, AppRole.OperationalManagementShow,
                AppRole.QualityManagementEdit, AppRole.QualityManagementShow,
                AppRole.TrainingManagementEdit, AppRole.TrainingManagementShow,
                AppRole.ItManagementShow,
                AppRole.Operation
            };
            SupportStaff = new List<string>() { 
                AppRole.OperationalManagementShow,
                AppRole.QualityManagementShow,
                AppRole.TrainingManagementShow,
                AppRole.Operation
            };
            ItStaff = new List<string>() {
                AppRole.ItManagementEdit, AppRole.ItManagementShow,
            };
            RRHHStaff = new List<string>() {
                AppRole.OperationalManagementShow,
                AppRole.HHRRManagementEdit, AppRole.HHRRManagementShow,
            };
            ReportingStaff = new List<string>() { 
                AppRole.OperationalManagementShow,
                AppRole.QualityManagementShow,
                AppRole.TrainingManagementShow,
                AppRole.Operation
            };
            QualityStaff = new List<string>() { 
                AppRole.QualityManagementEdit, AppRole.QualityManagementShow,
                AppRole.TrainingManagementShow,
                AppRole.Operation
            };
            TrainingStaff = new List<string>() { 
                AppRole.QualityManagementShow,
                AppRole.TrainingManagementEdit, AppRole.TrainingManagementShow,
                AppRole.Operation
            };
            Staff = new List<string>() {
                AppRole.Operation
            };
        }

        public static Dictionary<string, string> Profiles()
        {
            Dictionary<string, string> dic = new Dictionary<string, string>();
            /*
            Type type = typeof(AppProfile);
            PropertyInfo[] properties = type.GetProperties();
            type.GetFields();
            foreach (PropertyInfo p in properties)
            {
                var attribute = p.GetCustomAttributes(typeof(DisplayAttribute), false).Cast<DisplayAttribute>().FirstOrDefault();
                string displayName = attribute?.Name;
                dic.Add(p.Name, displayName);                  
            }
            return dic;*/
            
            Type type = typeof(AppProfile);
            List<FieldInfo> fields = type.GetFields(BindingFlags.Public | BindingFlags.Static | BindingFlags.FlattenHierarchy).ToList();
            foreach (FieldInfo f in fields)
            {
                var attribute = f.GetCustomAttributes(typeof(DisplayAttribute), true).Cast<DisplayAttribute>().FirstOrDefault();
                string displayName = attribute?.Name;
                dic.Add(f.Name, displayName);
            }
            return dic;
        }

        public static Dictionary<string, string> Profiles(List<string> selectedRoles)
        {
            var allRoles = Profiles();
            return allRoles.Where(n => selectedRoles.Contains(n.Key)).ToDictionary(v => v.Key, v => v.Value);
        }

        public static Dictionary<string, Dictionary<string, string>> ProfilesRoles()
        {
            Dictionary<string, Dictionary<string, string>> dic = new Dictionary<string, Dictionary<string, string>>();

            //Type type = typeof(AppRole);
            //PropertyInfo[] properties = type.GetProperties();
            //foreach (PropertyInfo p in properties)
            //{
            //    var attribute = p.GetCustomAttributes(typeof(DisplayAttribute), false).Cast<DisplayAttribute>().FirstOrDefault();
            //    string displayName = attribute?.Name;
            //    var value = p;
            //    dic.Add(p.Name, AppRole.Roles((List<string>)p.GetValue(null,null)));
            //}
            //return dic;

            Type type = typeof(AppProfile);
            List<FieldInfo> fields = type.GetFields(BindingFlags.Public | BindingFlags.Static | BindingFlags.FlattenHierarchy).ToList();
            foreach (FieldInfo f in fields)
            {
                var attribute = f.GetCustomAttributes(typeof(DisplayAttribute), true).Cast<DisplayAttribute>().FirstOrDefault();
                string displayName = attribute?.Name;
                dic.Add(f.Name, AppRole.Roles((List<string>)f.GetValue(null)));
            }
            return dic;
        }

        public static Dictionary<string, Dictionary<string, string>> ProfilesRoles(List<string> selectedProfiles)
        {
            var allProfiles = ProfilesRoles();
            return allProfiles.Where(n => selectedProfiles.Contains(n.Key)).ToDictionary(v => v.Key, v => v.Value);
        }
    }

    public static class AppPolicy
    {
        public const string Administration = "Administration";
        public const string Maintenance = "Maintenance";

        public const string EconomicalShow = "EconomicalShow";
        public const string Economical = "Economical";

        public const string TecnicalShow = "TecnicalShow";
        public const string Tecnical = "Tecnical";

        public const string OperationalShow = "OperationalShow";
        public const string Operational = "Operational";

        public const string QualityShow = "QualityShow";
        public const string Quality = "Quality";

        public const string TrainingShow = "TrainingShow";
        public const string Training = "Training";

        public const string DocumentationShow = "DocumentationShow";
        public const string Documentation = "Documentation";

        public const string Staff = "Staff";

    }
}
