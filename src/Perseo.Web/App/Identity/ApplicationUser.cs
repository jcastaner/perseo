﻿using Microsoft.AspNetCore.Identity;
using Perseo.Core;
using System;

namespace Perseo.Web.Identity
{
    public class ApplicationUser : IdentityUser<int>
    {
        public string MobileNumber { get; set; }
        public string NetworkUser { get; set; }
        public int? EmployeeId { get; set; }
        public string FirstName { get; set; }
        public string MainSurname { get; set; }
        public string SecondSurname { get; set; }

        public AcademicDegree? AcademicDegree { get; set; }
        public string Nationality { get; set; }
        public string NationalId { get; set; }
        public DateTime? BirthDate { get; set; }
        public string BirthPlace { get; set; }
        public string PersonalPhoneNumber { get; set; }
        public string PersonalMobileNumber { get; set; }
        public string PersonalEmail { get; set; }
        public string Address { get; set; }
        public string Zip { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }

        public bool? Flag { get; set; }
        public int Alerts { get; set; }
        public int Messages { get; set; }
        public int Tasks { get; set; }
        public bool? ChangePassword {get; set;}
        public bool? RefreshCookie { get; set; }
        public DateTime? LastPasswordChangedDate { get; set; }
        public DateTime? LastLoginDate { get; set; }

    }
}
