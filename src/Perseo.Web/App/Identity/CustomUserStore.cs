﻿using Perseo.Core;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;

namespace Perseo.Web.Identity
{
    public class CustomUserStore :
        IUserStore<ApplicationUser>,
        IUserPasswordStore<ApplicationUser>,
        IUserEmailStore<ApplicationUser>,
        IUserRoleStore<ApplicationUser>,
        IUserSecurityStampStore<ApplicationUser>,
        IUserLockoutStore<ApplicationUser>,
        IQueryableUserStore<ApplicationUser>
    {

        private readonly IUserService _userService;

        public CustomUserStore(IUserService userService)
        {
            _userService = userService;
        }

        #region IQueryableUserStore<ApplicationUser> Members
        public IQueryable<ApplicationUser> Users
        {
            get
            {
                return _userService.GetMany()
                    .Select(x => getApplicationUser(x))
                    .AsQueryable();
            }
        }
        #endregion

        #region IUserStore<ApplicationUser> Members

        public Task<IdentityResult> CreateAsync(ApplicationUser user, CancellationToken cancellationToken)
        {
            try
            {
                if (cancellationToken != null)
                    cancellationToken.ThrowIfCancellationRequested();

                if (user == null)
                    throw new ArgumentNullException(nameof(user));

                var userEntity = getUserEntity(user);

                _userService.Add(userEntity);
                //_unitOfWork.Commit();

                return Task.FromResult(IdentityResult.Success);
            }
            catch (Exception ex)
            {
                return Task.FromResult(IdentityResult.Failed(new IdentityError { Code = ex.Message, Description = ex.Message }));
            }
        }

        public Task<IdentityResult> DeleteAsync(ApplicationUser user, CancellationToken cancellationToken)
        {
            try
            {
                if (cancellationToken != null)
                    cancellationToken.ThrowIfCancellationRequested();

                if (user == null)
                    throw new ArgumentNullException(nameof(user));

                _userService.Delete(user.Id);
                //_userService.Remove(user.Id);
               // _unitOfWork.Commit();

                return Task.FromResult(IdentityResult.Success);
            }
            catch (Exception ex)
            {
                return Task.FromResult(IdentityResult.Failed(new IdentityError { Code = ex.Message, Description = ex.Message }));
            }
        }

        public Task<ApplicationUser> FindByIdAsync(string userId, CancellationToken cancellationToken)
        {
            if (cancellationToken != null)
                cancellationToken.ThrowIfCancellationRequested();

            if (string.IsNullOrWhiteSpace(userId))
                throw new ArgumentNullException(nameof(userId));
            /*
            if (!Guid.TryParse(userId, out Guid id))
                throw new ArgumentOutOfRangeException(nameof(userId), $"{nameof(userId)} is not a valid GUID");
            */
            var userEntity = _userService.Find(Int32.Parse(userId));

            return Task.FromResult(getApplicationUser(userEntity));
        }

        public Task<ApplicationUser> FindByNameAsync(string normalizedUserName, CancellationToken cancellationToken)
        {
            if (cancellationToken != null)
                cancellationToken.ThrowIfCancellationRequested();

            var userEntity = _userService.FindByNormalizedUserName(normalizedUserName);

            return Task.FromResult(getApplicationUser(userEntity));
        }

        public Task<string> GetNormalizedUserNameAsync(ApplicationUser user, CancellationToken cancellationToken)
        {
            if (cancellationToken != null)
                cancellationToken.ThrowIfCancellationRequested();

            if (user == null)
                throw new ArgumentNullException(nameof(user));

            return Task.FromResult(user.NormalizedUserName);
        }

        public Task<string> GetUserIdAsync(ApplicationUser user, CancellationToken cancellationToken)
        {
            if (cancellationToken != null)
                cancellationToken.ThrowIfCancellationRequested();

            if (user == null)
                throw new ArgumentNullException(nameof(user));

            return Task.FromResult(user.Id.ToString());
        }

        public Task<string> GetUserNameAsync(ApplicationUser user, CancellationToken cancellationToken)
        {
            if (cancellationToken != null)
                cancellationToken.ThrowIfCancellationRequested();

            if (user == null)
                throw new ArgumentNullException(nameof(user));

            return Task.FromResult(user.UserName);
        }

        public Task SetNormalizedUserNameAsync(ApplicationUser user, string normalizedName, CancellationToken cancellationToken)
        {
            if (cancellationToken != null)
                cancellationToken.ThrowIfCancellationRequested();

            if (user == null)
                throw new ArgumentNullException(nameof(user));

            user.NormalizedUserName = normalizedName;

            return Task.CompletedTask;
        }

        public Task SetUserNameAsync(ApplicationUser user, string userName, CancellationToken cancellationToken)
        {
            if (cancellationToken != null)
                cancellationToken.ThrowIfCancellationRequested();

            if (user == null)
                throw new ArgumentNullException(nameof(user));

            user.UserName = userName;

            return Task.CompletedTask;
        }

        public Task<IdentityResult> UpdateAsync(ApplicationUser user, CancellationToken cancellationToken)
        {
            try
            {
                if (cancellationToken != null)
                    cancellationToken.ThrowIfCancellationRequested();

                if (user == null)
                    throw new ArgumentNullException(nameof(user));

                var userEntity = getUserEntity(user);

                _userService.Update(userEntity);
                //_unitOfWork.Commit();

                return Task.FromResult(IdentityResult.Success);
            }
            catch (Exception ex)
            {
                return Task.FromResult(IdentityResult.Failed(new IdentityError { Code = ex.Message, Description = ex.Message }));
            }
        }

        public void Dispose()
        {
            // Lifetimes of dependencies are managed by the IoC container, so disposal here is unnecessary.
        }
        #endregion

        #region IUserPasswordStore<ApplicationUser> Members
        public Task SetPasswordHashAsync(ApplicationUser user, string passwordHash, CancellationToken cancellationToken)
        {
            if (cancellationToken != null)
                cancellationToken.ThrowIfCancellationRequested();

            if (user == null)
                throw new ArgumentNullException(nameof(user));

            user.PasswordHash = passwordHash;

            return Task.CompletedTask;
        }

        public Task<string> GetPasswordHashAsync(ApplicationUser user, CancellationToken cancellationToken)
        {
            if (cancellationToken != null)
                cancellationToken.ThrowIfCancellationRequested();

            if (user == null)
                throw new ArgumentNullException(nameof(user));

            return Task.FromResult(user.PasswordHash);
        }

        public Task<bool> HasPasswordAsync(ApplicationUser user, CancellationToken cancellationToken)
        {
            if (cancellationToken != null)
                cancellationToken.ThrowIfCancellationRequested();

            if (user == null)
                throw new ArgumentNullException(nameof(user));

            return Task.FromResult(!string.IsNullOrWhiteSpace(user.PasswordHash));
        }
        #endregion
        
        #region IUserEmailStore<ApplicationUser> Members
        public Task SetEmailAsync(ApplicationUser user, string email, CancellationToken cancellationToken)
        {
            if (cancellationToken != null)
                cancellationToken.ThrowIfCancellationRequested();

            if (user == null)
                throw new ArgumentNullException(nameof(user));

            user.Email = email;

            return Task.CompletedTask;
        }

        public Task<string> GetEmailAsync(ApplicationUser user, CancellationToken cancellationToken)
        {
            if (cancellationToken != null)
                cancellationToken.ThrowIfCancellationRequested();

            if (user == null)
                throw new ArgumentNullException(nameof(user));

            return Task.FromResult(user.Email);
        }

        public Task<bool> GetEmailConfirmedAsync(ApplicationUser user, CancellationToken cancellationToken)
        {
            if (cancellationToken != null)
                cancellationToken.ThrowIfCancellationRequested();

            if (user == null)
                throw new ArgumentNullException(nameof(user));

            return Task.FromResult(user.EmailConfirmed);
        }

        public Task SetEmailConfirmedAsync(ApplicationUser user, bool confirmed, CancellationToken cancellationToken)
        {
            if (cancellationToken != null)
                cancellationToken.ThrowIfCancellationRequested();

            if (user == null)
                throw new ArgumentNullException(nameof(user));

            user.EmailConfirmed = confirmed;

            return Task.CompletedTask;
        }

        public Task<ApplicationUser> FindByEmailAsync(string normalizedEmail, CancellationToken cancellationToken)
        {
            if (string.IsNullOrWhiteSpace(normalizedEmail))
                throw new ArgumentNullException(nameof(normalizedEmail));

            var userEntity = _userService.FindByNormalizedEmail(normalizedEmail);

            return Task.FromResult(getApplicationUser(userEntity));
        }

        public Task<string> GetNormalizedEmailAsync(ApplicationUser user, CancellationToken cancellationToken)
        {
            if (cancellationToken != null)
                cancellationToken.ThrowIfCancellationRequested();

            if (user == null)
                throw new ArgumentNullException(nameof(user));

            return Task.FromResult(user.NormalizedEmail);
        }

        public Task SetNormalizedEmailAsync(ApplicationUser user, string normalizedEmail, CancellationToken cancellationToken)
        {
            if (cancellationToken != null)
                cancellationToken.ThrowIfCancellationRequested();

            if (user == null)
                throw new ArgumentNullException(nameof(user));

            user.NormalizedEmail = normalizedEmail;

            return Task.CompletedTask;
        }
        #endregion

        #region IUserRoleStore<ApplicationUser> Members
        public Task AddToRoleAsync(ApplicationUser user, string roleName, CancellationToken cancellationToken)
        {
            if (cancellationToken != null)
                cancellationToken.ThrowIfCancellationRequested();

            if (user == null)
                throw new ArgumentNullException(nameof(user));

            if (string.IsNullOrWhiteSpace(roleName))
                throw new ArgumentNullException(nameof(roleName));

            _userService.AddRole(user.Id, roleName);
           // _unitOfWork.Commit();

            return Task.CompletedTask;
        }

        public Task RemoveFromRoleAsync(ApplicationUser user, string roleName, CancellationToken cancellationToken)
        {
            if (cancellationToken != null)
                cancellationToken.ThrowIfCancellationRequested();

            if (user == null)
                throw new ArgumentNullException(nameof(user));

            if (string.IsNullOrWhiteSpace(roleName))
                throw new ArgumentNullException(nameof(roleName));

            _userService.RemoveRole(user.Id, roleName);

            //_unitOfWork.Commit();

            return Task.CompletedTask;
        }

        public Task<IList<string>> GetRolesAsync(ApplicationUser user, CancellationToken cancellationToken)
        {
            if (cancellationToken != null)
                cancellationToken.ThrowIfCancellationRequested();

            if (user == null)
                throw new ArgumentNullException(nameof(user));

            IList<string> result = _userService.GetRoleNamesByUserId(user.Id)
                .ToList();

            return Task.FromResult(result);
        }

        public Task<bool> IsInRoleAsync(ApplicationUser user, string roleName, CancellationToken cancellationToken)
        {
            if (cancellationToken != null)
                cancellationToken.ThrowIfCancellationRequested();

            if (user == null)
                throw new ArgumentNullException(nameof(user));

            if (string.IsNullOrWhiteSpace(roleName))
                throw new ArgumentNullException(nameof(roleName));

            var result = _userService.GetRoleNamesByUserId(user.Id).Any(x => x == roleName);

            return Task.FromResult(result);
        }

        public Task<IList<ApplicationUser>> GetUsersInRoleAsync(string roleName, CancellationToken cancellationToken)
        {
            if (cancellationToken != null)
                cancellationToken.ThrowIfCancellationRequested();

            if (string.IsNullOrWhiteSpace(roleName))
                throw new ArgumentNullException(nameof(roleName));

            IList<ApplicationUser> result = _userService.GetUsersByRoleName(roleName)
                .Select(x => getApplicationUser(x))
                .ToList();

            return Task.FromResult(result);
        }
        #endregion

        #region IUserSecurityStampStore<ApplicationUser> Members
        public Task SetSecurityStampAsync(ApplicationUser user, string stamp, CancellationToken cancellationToken)
        {
            if (cancellationToken != null)
                cancellationToken.ThrowIfCancellationRequested();

            if (user == null)
                throw new ArgumentNullException(nameof(user));

            user.SecurityStamp = stamp;

            return Task.CompletedTask;
        }

        public Task<string> GetSecurityStampAsync(ApplicationUser user, CancellationToken cancellationToken)
        {
            if (cancellationToken != null)
                cancellationToken.ThrowIfCancellationRequested();

            if (user == null)
                throw new ArgumentNullException(nameof(user));

            return Task.FromResult(user.SecurityStamp);
        }
        #endregion

        #region IUserLockoutStore<ApplicationUser> Members
        public Task<DateTimeOffset?> GetLockoutEndDateAsync(ApplicationUser user, CancellationToken cancellationToken)
        {
            if (cancellationToken != null)
                cancellationToken.ThrowIfCancellationRequested();

            if (user == null)
                throw new ArgumentNullException(nameof(user));

            return Task.FromResult(user.LockoutEnd);
        }

        public Task SetLockoutEndDateAsync(ApplicationUser user, DateTimeOffset? lockoutEnd, CancellationToken cancellationToken)
        {
            if (cancellationToken != null)
                cancellationToken.ThrowIfCancellationRequested();

            if (user == null)
                throw new ArgumentNullException(nameof(user));

            user.LockoutEnd = lockoutEnd;

            return Task.CompletedTask;
        }

        public Task<int> IncrementAccessFailedCountAsync(ApplicationUser user, CancellationToken cancellationToken)
        {
            if (cancellationToken != null)
                cancellationToken.ThrowIfCancellationRequested();

            if (user == null)
                throw new ArgumentNullException(nameof(user));

            return Task.FromResult(++user.AccessFailedCount);
        }

        public Task ResetAccessFailedCountAsync(ApplicationUser user, CancellationToken cancellationToken)
        {
            if (cancellationToken != null)
                cancellationToken.ThrowIfCancellationRequested();

            if (user == null)
                throw new ArgumentNullException(nameof(user));

            user.AccessFailedCount = 0;

            return Task.CompletedTask;
        }

        public Task<int> GetAccessFailedCountAsync(ApplicationUser user, CancellationToken cancellationToken)
        {
            if (cancellationToken != null)
                cancellationToken.ThrowIfCancellationRequested();

            if (user == null)
                throw new ArgumentNullException(nameof(user));

            return Task.FromResult(user.AccessFailedCount);
        }

        public Task<bool> GetLockoutEnabledAsync(ApplicationUser user, CancellationToken cancellationToken)
        {
            if (cancellationToken != null)
                cancellationToken.ThrowIfCancellationRequested();

            if (user == null)
                throw new ArgumentNullException(nameof(user));

            return Task.FromResult(user.LockoutEnabled);
        }

        public Task SetLockoutEnabledAsync(ApplicationUser user, bool enabled, CancellationToken cancellationToken)
        {
            if (cancellationToken != null)
                cancellationToken.ThrowIfCancellationRequested();

            if (user == null)
                throw new ArgumentNullException(nameof(user));

            user.LockoutEnabled = enabled;

            return Task.CompletedTask;
        }
        #endregion

        #region Private Methods
        private User getUserEntity(ApplicationUser ApplicationUser)
        {
            if (ApplicationUser == null)
                return null;

            var result = new User();
            populateUserEntity(result, ApplicationUser);

            return result;
        }

        private void populateUserEntity(User entity, ApplicationUser ApplicationUser)
        {
            entity.AccessFailedCount = ApplicationUser.AccessFailedCount;
            entity.Email = ApplicationUser.Email;
            entity.EmailConfirmed = ApplicationUser.EmailConfirmed;
            entity.Id = ApplicationUser.Id;
            entity.LockoutEnabled = ApplicationUser.LockoutEnabled;
            entity.LockoutEnd = ApplicationUser.LockoutEnd;
            entity.NormalizedEmail = ApplicationUser.NormalizedEmail;
            entity.NormalizedUserName = ApplicationUser.NormalizedUserName;
            entity.PasswordHash = ApplicationUser.PasswordHash;
            entity.UserName = ApplicationUser.UserName;
            entity.ConcurrencyStamp = ApplicationUser.ConcurrencyStamp;
            entity.SecurityStamp = ApplicationUser.SecurityStamp;

            entity.MobileNumber = ApplicationUser.MobileNumber;
            entity.NetworkUser = ApplicationUser.NetworkUser;
            entity.EmployeeId = ApplicationUser.EmployeeId;
            entity.FirstName = ApplicationUser.FirstName;
            entity.MainSurname = ApplicationUser.MainSurname;
            entity.SecondSurname = ApplicationUser.SecondSurname;
            entity.AcademicDegree = ApplicationUser.AcademicDegree;
            entity.Nationality = ApplicationUser.Nationality;
            entity.NationalId = ApplicationUser.NationalId;
            entity.BirthDate = ApplicationUser.BirthDate;
            entity.BirthPlace = ApplicationUser.BirthPlace;
            entity.PersonalPhoneNumber = ApplicationUser.PersonalPhoneNumber;
            entity.PersonalMobileNumber = ApplicationUser.PersonalMobileNumber;
            entity.PersonalEmail = ApplicationUser.PersonalEmail;
            entity.Address = ApplicationUser.Address;
            entity.Zip = ApplicationUser.Zip;
            entity.City = ApplicationUser.City;
            entity.State = ApplicationUser.State;
            entity.Country = ApplicationUser.Country;
            entity.Flag = ApplicationUser.Flag;
            entity.Alerts = ApplicationUser.Alerts;
            entity.Messages = ApplicationUser.Messages;
            entity.Tasks = ApplicationUser.Tasks;
            entity.RefreshCookie = ApplicationUser.RefreshCookie;
            entity.ChangePassword = ApplicationUser.ChangePassword;
            entity.LastPasswordChangedDate = ApplicationUser.LastPasswordChangedDate;
            entity.LastLoginDate = ApplicationUser.LastLoginDate;


        }

        private ApplicationUser getApplicationUser(User entity)
        {
            if (entity == null)
                return null;

            var result = new ApplicationUser();
            populateApplicationUser(result, entity);

            return result;
        }

        private void populateApplicationUser(ApplicationUser ApplicationUser, User entity)
        {
            ApplicationUser.AccessFailedCount = entity.AccessFailedCount;
            ApplicationUser.Id = entity.Id;
            ApplicationUser.UserName = entity.UserName;
            ApplicationUser.NormalizedUserName = entity.NormalizedUserName;
            ApplicationUser.PasswordHash = entity.PasswordHash;
            ApplicationUser.LockoutEnabled = entity.LockoutEnabled;
            ApplicationUser.LockoutEnd = entity.LockoutEnd;
            ApplicationUser.Email = entity.Email;
            ApplicationUser.NormalizedEmail = entity.NormalizedEmail;
            ApplicationUser.EmailConfirmed = entity.EmailConfirmed;
            ApplicationUser.ConcurrencyStamp = entity.ConcurrencyStamp;
            ApplicationUser.SecurityStamp = entity.SecurityStamp;

            ApplicationUser.MobileNumber = entity.MobileNumber;
            ApplicationUser.NetworkUser = entity.NetworkUser;
            ApplicationUser.EmployeeId = entity.EmployeeId;
            ApplicationUser.FirstName = entity.FirstName;
            ApplicationUser.MainSurname = entity.MainSurname;
            ApplicationUser.SecondSurname = entity.SecondSurname;
            ApplicationUser.AcademicDegree = entity.AcademicDegree;
            ApplicationUser.Nationality = entity.Nationality;
            ApplicationUser.NationalId = entity.NationalId;
            ApplicationUser.BirthDate = entity.BirthDate;
            ApplicationUser.BirthPlace = entity.BirthPlace;
            ApplicationUser.PersonalPhoneNumber = entity.PersonalPhoneNumber;
            ApplicationUser.PersonalMobileNumber = entity.PersonalMobileNumber;
            ApplicationUser.PersonalEmail = entity.PersonalEmail;
            ApplicationUser.Address = entity.Address;
            ApplicationUser.Zip = entity.Zip;
            ApplicationUser.City = entity.City;
            ApplicationUser.State = entity.State;
            ApplicationUser.Country = entity.Country;
            ApplicationUser.Flag = entity.Flag;
            ApplicationUser.Alerts = entity.Alerts;
            ApplicationUser.Messages = entity.Messages;
            ApplicationUser.Tasks = entity.Tasks;
            ApplicationUser.RefreshCookie = entity.RefreshCookie;
            ApplicationUser.ChangePassword = entity.ChangePassword;
            ApplicationUser.LastPasswordChangedDate = entity.LastPasswordChangedDate;
            ApplicationUser.LastLoginDate = entity.LastLoginDate;
        }


        #endregion
    }
}

