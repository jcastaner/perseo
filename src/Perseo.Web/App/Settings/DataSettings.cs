﻿using Perseo.Core;

namespace Perseo.Web
{
    public class DataSettings
    {
        public string ConnectionString { get; set; }
        public string ServerVersion { get; set; }
    }
}
