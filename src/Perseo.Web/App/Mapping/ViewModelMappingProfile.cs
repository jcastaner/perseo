﻿using AutoMapper;
using Perseo.Core;
using Perseo.Web.Models;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;

public class ViewModelMappingProfile : Profile
{
    public static string MemberExclusionKey { get; } = "exclude";

    public ViewModelMappingProfile()
    {
        CreateMap(typeof(PagedResult<>), typeof(PaginationModel));

        #region AREA

        CreateMap<AreaListNode, AreaListNodeModel>();

        #endregion

        #region USER

        CreateMap<User, UserViewModel>()
            .ForMember(dest => dest.Office, opt => opt.MapFrom(user => user.LastUserOffice != null ? user.LastUserOffice.Office.Name : ""))
            .ForMember(dest => dest.Position, opt => opt.MapFrom(user => user.LastUserPosition != null ? user.LastUserPosition.Name : ""))
            .ForMember(dest => dest.Category, opt => opt.MapFrom(user => user.LastUserCategory != null ? user.LastUserCategory.Category.Name : ""))
            .ForMember(dest => dest.Collective, opt => opt.MapFrom(user => user.LastUserCollective != null ? user.LastUserCollective.Collective.Name : ""))
            .ForMember(dest => dest.CollectiveCategory, opt => opt.MapFrom(user => user.LastUserCollectiveCategory != null ? user.LastUserCollectiveCategory.Category : ""))
            .ForMember(dest => dest.Area, opt => opt.MapFrom(user => user.LastUserArea != null ? user.LastUserArea.Area.Name : ""))
            .ForMember(dest => dest.Rate, opt => opt.MapFrom(user => user.LastUserRate != null ? user.LastUserRate.Rate.Value.ToString() + " - " + user.LastUserRate.Rate.Name : ""))
            .ForMember(dest => dest.Contract, opt => opt.MapFrom(user => user.LastUserContract != null ? user.LastUserContract.ContractPeriod : default(ContractPeriodType)))
            .ForMember(dest => dest.Company, opt => opt.MapFrom(user => user.LastUserContract != null ? user.LastUserContract.Company.Name : ""))
            .ForMember(dest => dest.Shift, opt => opt.MapFrom(user => user.LastUserShift != null ? user.LastUserShift.Description : ""))
            .ForMember(dest => dest.Roles, opt => opt.MapFrom(user => user.Roles))
            .ForMember(dest => dest.UserRoles, opt => opt.MapFrom(user => user.UserRole))
            .ForMember(dest => dest.UserOperational, opt => opt.MapFrom(user => user.UserOperational.Where(uo => uo.Active)))
            .ForMember(dest => dest.OwnedAreas, opt => opt.MapFrom(user => string.Join("; ", user.OwnedUserAreas.Select(ua => ua.Area.Name))))
            .ForMember(dest => dest.WeekWorkHours, opt => opt.MapFrom(user => user.LastUserWorkConditions != null ? user.LastUserWorkConditions.WeekWorkHours : 0))
            .ForMember(dest => dest.YearWorkHours, opt => opt.MapFrom(user => user.LastUserWorkConditions != null ? user.LastUserWorkConditions.YearWorkHours : 0))
            .ForMember(dest => dest.YearSpareDays, opt => opt.MapFrom(user => user.LastUserWorkConditions != null ? user.LastUserWorkConditions.YearSpareDays : 0))
            .ForMember(dest => dest.YearVacationDays, opt => opt.MapFrom(user => user.LastUserWorkConditions != null ? user.LastUserWorkConditions.YearVacationDays : 0))
            .ForMember(dest => dest.Salary, opt => opt.MapFrom(user => user.LastUserSalary != null ? user.LastUserSalary.Salary + " ("+ user.LastUserSalary.Currency + ")" : ""))
            .ForMember(dest => dest.Shifts, opt => opt.Ignore())
            .ReverseMap()
            .ForMember(dest => dest.Active, opt => opt.Ignore())
            .ForMember(dest => dest.UserOperational, opt => opt.Ignore())
            .ForMember(dest => dest.UserRole, opt => opt.Ignore())
            .ForMember(dest => dest.OwnedAreas, opt => opt.Ignore())
            .ForMember(dest => dest.OwnedJobs, opt => opt.Ignore());

        CreateMap<UserListNode, UserListNodeModel>()
            .ForMember(dest => dest.FullFamilyName, opt => opt.MapFrom(u => u.FirstName + " " + u.MainSurname + " " + u.SecondSurname));

        CreateMap<QualityExamUser, UserExamPendingViewModel>()
            .ForMember(dest => dest.Quality, opt => opt.MapFrom(qeu => qeu.Exam.Quality))
            .ForMember(dest => dest.Exam, opt => opt.MapFrom(qeu => qeu.Exam))
            .ForMember(dest => dest.DoneAttempts, opt => opt.MapFrom(qeu => qeu.Takes.Count));

        CreateMap<QualityExamUserTake, UserExamTakeViewModel>()
            .ForMember(dest => dest.Quality, opt => opt.MapFrom(qeut => qeut.ExamUser.Exam.Quality))
            .ForMember(dest => dest.Exam, opt => opt.MapFrom(qeut => qeut.ExamUser.Exam))
            .ForMember(dest => dest.Take, opt => opt.MapFrom(qeut => qeut));

        CreateMap<IEnumerable<QualityExamUser>, QualityViewModel>()
            .ForMember(dest => dest.ExamPending, opt => opt.MapFrom(qeus => qeus.Where(qeu => (qeu.Takes.Count < qeu.Exam.Attempts) && (qeu.Takes.All(qeut => qeut.Pass == false)))))
            .ForMember(dest => dest.ExamTakes, opt => opt.MapFrom(qeus => qeus.SelectMany(qeu => qeu.Takes)))
            .ForMember(dest => dest.AuditTakes, opt => opt.Ignore());

        CreateMap<QualityAuditUserTake, UserAuditTakeViewModel>()
            .ForMember(dest => dest.Quality, opt => opt.MapFrom(qaut => qaut.AuditUser.Audit.Quality))
            .ForMember(dest => dest.Audit, opt => opt.MapFrom(qaut => qaut.AuditUser.Audit))
            .ForMember(dest => dest.AuditOwner, opt => opt.MapFrom(qaut => qaut.AuditUser.AuditOwner.User))
            .ForMember(dest => dest.Take, opt => opt.MapFrom(qaut => qaut));

        CreateMap<IEnumerable<QualityAuditUser>, QualityViewModel>()
            .ForMember(dest => dest.AuditTakes, opt => opt.MapFrom(qaus => qaus.SelectMany(qau => qau.Takes)))
            .ForMember(dest => dest.ExamPending, opt => opt.Ignore())
            .ForMember(dest => dest.ExamTakes, opt => opt.Ignore());

        CreateMap<TrainingTestUser, UserTestPendingViewModel>()
            .ForMember(dest => dest.Training, opt => opt.MapFrom(ttu => ttu.Test.Training))
            .ForMember(dest => dest.Test, opt => opt.MapFrom(ttu => ttu.Test))
            .ForMember(dest => dest.DoneAttempts, opt => opt.MapFrom(ttu => ttu.Takes.Count));

        CreateMap<TrainingTestUserTake, UserTestTakeViewModel>()
            .ForMember(dest => dest.Training, opt => opt.MapFrom(ttut => ttut.TestUser.Test.Training))
            .ForMember(dest => dest.Test, opt => opt.MapFrom(ttut => ttut.TestUser.Test))
            .ForMember(dest => dest.Take, opt => opt.MapFrom(ttut => ttut));

        CreateMap<IEnumerable<Training>, TrainingViewModel>()
            .ForMember(dest => dest.UserTrainings, opt => opt.MapFrom(ts => ts))
            .ForMember(dest => dest.TestPending, opt => opt.Ignore())
            .ForMember(dest => dest.TestTakes, opt => opt.Ignore());

        CreateMap<IEnumerable<TrainingTestUser>, TrainingViewModel>()
            .ForMember(dest => dest.TestPending, opt => opt.MapFrom(ttus => ttus.Where(ttu => (ttu.Takes.Count < ttu.Test.Attempts) && (ttu.Takes.All(ttut => ttut.Pass == false)))))
            .ForMember(dest => dest.TestTakes, opt => opt.MapFrom(ttus => ttus.SelectMany(ttu => ttu.Takes)))
            .ForMember(dest => dest.UserTrainings, opt => opt.Ignore());

        #endregion

        #region QUALITY

        CreateMap<Quality, QualityListViewModel>()
            .ForMember(dest => dest.Id, opt => opt.MapFrom(q => q.Id))
            .ForMember(dest => dest.Name, opt => opt.MapFrom(q => q.Name))
            .ForMember(dest => dest.AreaName, opt => opt.MapFrom(q => q.Area.Name))
            .ForMember(dest => dest.StartDate, opt => opt.MapFrom(q => q.StartDate))
            .ForMember(dest => dest.EndDate, opt => opt.MapFrom(q => q.EndDate))
            .ForMember(dest => dest.CountAudit, opt => opt.MapFrom(q => q.Audits.Count()))
            .ForMember(dest => dest.CountExam, opt => opt.MapFrom(q => q.Exams.Count()))
            .ForMember(dest => dest.CountUserQuality, opt => opt.MapFrom(q => q.Users.Count()))
            .ForMember(dest => dest.CountUserQualityCompleted, opt => opt.MapFrom(q => q.Users.Count(qu => qu.QualityCompleted)));

        CreateMap<Quality, ShowQualityViewModel>()
            .ForMember(dest => dest.Quality, opt => opt.MapFrom(q => q))
            .ForMember(dest => dest.AreaName, opt => opt.MapFrom(q => q.Area.Name))
            .ForMember(dest => dest.Owners, opt => opt.MapFrom(q => q.Owners.Select(qo => qo.Owner).ToList()))
            .ForMember(dest => dest.Users, opt => opt.MapFrom(q => q.Users.Select(qo => qo.User).ToList()))
            .ForMember(dest => dest.Audits, opt => opt.MapFrom(q => q.Audits.ToList()))
            .ForMember(dest => dest.Exams, opt => opt.MapFrom(q => q.Exams.ToList()));

        CreateMap<QualityExamUserTake, StateExamTakeViewModel>()
            .ForMember(dest => dest.Id, opt => opt.MapFrom(qeut => qeut.Id))
            .ForMember(dest => dest.Attempt, opt => opt.MapFrom(qeut => qeut.Attempt))
            .ForMember(dest => dest.Active, opt => opt.MapFrom(qeut => qeut.Active))
            .ForMember(dest => dest.Pass, opt => opt.MapFrom(qeut => qeut.Pass))
            .ForMember(dest => dest.Exam, opt => opt.MapFrom(qeut => qeut.ExamUser.Exam))
            .ForMember(dest => dest.User, opt => opt.MapFrom(qeut => qeut.ExamUser.User));

        CreateMap<QualityExamUser, StateExamPendingViewModel>()
            .ForMember(dest => dest.Exam, opt => opt.MapFrom(qeu => qeu.Exam))
            .ForMember(dest => dest.User, opt => opt.MapFrom(qeu => qeu.User));

        CreateMap<QualityAuditUserTake, StateAuditTakeViewModel>()
            .ForMember(dest => dest.Id, opt => opt.MapFrom(qaut => qaut.Id))
            .ForMember(dest => dest.Active, opt => opt.MapFrom(qaut => qaut.Active))
            .ForMember(dest => dest.Pass, opt => opt.MapFrom(qaut => qaut.Pass))
            .ForMember(dest => dest.Audit, opt => opt.MapFrom(qaut => qaut.AuditUser.Audit))
            .ForMember(dest => dest.Owner, opt => opt.MapFrom(qaut => qaut.AuditUser.AuditOwner.User))
            .ForMember(dest => dest.User, opt => opt.MapFrom(qaut => qaut.AuditUser.User));

        CreateMap<QualityAuditUser, StateAuditPendingViewModel>()
            .ForMember(dest => dest.Audit, opt => opt.MapFrom(qau => qau.Audit))
            .ForMember(dest => dest.User, opt => opt.MapFrom(qau => qau.User))
            .ForMember(dest => dest.Owner, opt => opt.MapFrom(qau => qau.AuditOwner.User));

        CreateMap<Quality, StateQualityViewModel>()
            .ForMember(dest => dest.Quality, opt => opt.MapFrom(q => q))
            .ForMember(dest => dest.AreaName, opt => opt.MapFrom(q => q.Area.Name))
            .ForMember(dest => dest.ExamTakes, opt => opt.MapFrom(q => q.Exams.SelectMany(qe => qe.Users.SelectMany(qeu => qeu.Takes)).ToList()))
            .ForMember(dest => dest.ExamPending, opt => opt.MapFrom(q => q.Exams.SelectMany(qe => qe.Users).Where(qeu => !(qeu.Takes.Count > 0)).ToList()))
            .ForMember(dest => dest.AuditTakes, opt => opt.MapFrom(q => q.Audits.SelectMany(qa => qa.Users.SelectMany(qau => qau.Takes)).ToList()))
            .ForMember(dest => dest.AuditPending, opt => opt.MapFrom(q => q.Audits.SelectMany(qa => qa.Users).Where(qau => !(qau.Takes.Count > 0)).ToList()));

        CreateMap<Quality, EditQualityViewModel>()
            .ForMember(dest => dest.Quality, opt => opt.MapFrom(q => q))
            .ForMember(dest => dest.AreaName, opt => opt.MapFrom(q => q.Area.Name))
            .ForMember(dest => dest.Owners, opt => opt.MapFrom(q => q.Owners.Select(qo => qo.Owner).ToList()))
            .ForMember(dest => dest.Users, opt => opt.MapFrom(q => q.Users.Select(qo => qo.User).ToList()))
            .ForMember(dest => dest.Audits, opt => opt.MapFrom(q => q.Audits.ToList()))
            .ForMember(dest => dest.Exams, opt => opt.MapFrom(q => q.Exams.ToList()))
            .ReverseMap()
            .ForMember(dest => dest.Name, opt => opt.MapFrom(m => m.Quality.Name))
            .ForMember(dest => dest.AreaId, opt => opt.MapFrom(m => m.Quality.AreaId))
            .ForMember(dest => dest.Description, opt => opt.MapFrom(m => m.Quality.Description))
            .ForMember(dest => dest.StartDate, opt => opt.MapFrom(m => m.Quality.StartDate))
            .ForMember(dest => dest.EndDate, opt => opt.MapFrom(m => m.Quality.EndDate))
            .ForMember(dest => dest.Type, opt => opt.Ignore())
            .ForMember(dest => dest.CreatorId, opt => opt.Ignore())
            .ForMember(dest => dest.Area, opt => opt.Ignore())
            .ForMember(dest => dest.Owners, opt => opt.Ignore())
            .ForMember(dest => dest.Users, opt => opt.Ignore())
            .ForMember(dest => dest.Audits, opt => opt.Ignore())
            .ForMember(dest => dest.Exams, opt => opt.Ignore())
            .ForMember(dest => dest.Active, opt => opt.Ignore());

        CreateMap<QualityAuditSection, QualityAuditSectionViewModel>().ReverseMap();
        CreateMap<QualityAuditSectionField, QualityAuditSectionFieldViewModel>().ReverseMap();
        CreateMap<QualityAuditSectionFieldOption, QualityAuditSectionFieldOptionViewModel>().ReverseMap();

        CreateMap<QualityAudit, QualityAuditViewModel>()
            .ForMember(dest => dest.QualityName, opt => opt.MapFrom(qe => qe.Quality.Name))
            .ForMember(dest => dest.AuditUsers, opt => opt.MapFrom(qe => qe.Users.ToList()))
            .ForMember(dest => dest.QualityOwners, opt => opt.MapFrom(qe => qe.Quality.Owners.Select(qo => qo.Owner).ToList()))
            .ReverseMap();

        CreateMap<JObject, QualityAudit>().ConvertUsing(new JsonConverter<QualityAudit>());

        CreateMap<QualityAuditSection, QualityAuditTakeSectionViewModel>().ReverseMap();
        CreateMap<QualityAuditSectionField, QualityAuditTakeSectionFieldViewModel>().ReverseMap();
        CreateMap<QualityAuditSectionFieldOption, QualityAuditTakeSectionFieldOptionViewModel>().ReverseMap();
        CreateMap<QualityAudit, QualityAuditTakeViewModel>()
            .ForMember(dest => dest.QualityName, opt => opt.MapFrom(qe => qe.Quality.Name))
            .ReverseMap();

        CreateMap<QualityAudit, QualityAuditTakeRequestViewModel>().ReverseMap();

        CreateMap<JObject, QualityAuditUserTake>().ConvertUsing(new JsonConverter<QualityAuditUserTake>());

        CreateMap<QualityExamSection, QualityExamSectionViewModel>().ReverseMap();
        CreateMap<QualityExamSectionField, QualityExamSectionFieldViewModel>().ReverseMap();
        CreateMap<QualityExamSectionFieldOption, QualityExamSectionFieldOptionViewModel>().ReverseMap();

        CreateMap<QualityExam, QualityExamViewModel>()
            .ForMember(dest => dest.QualityName, opt => opt.MapFrom(qe => qe.Quality.Name))
            .ForMember(dest => dest.ExamUsers, opt => opt.MapFrom(qe => qe.Users.ToList()))
            .ForMember(dest => dest.QualityOwners, opt => opt.MapFrom(qe => qe.Quality.Owners.Select(qo => qo.Owner).ToList()))
            .ReverseMap();

        CreateMap<JObject, QualityExam>().ConvertUsing(new JsonConverter<QualityExam>());

        CreateMap<QualityExam, ConfirmExamTakeViewModel>()
            .ForMember(dest => dest.QualityId, opt => opt.MapFrom(qe => qe.Quality.Id))
            .ForMember(dest => dest.ExamId, opt => opt.MapFrom(qe => qe.Id))
            .ForMember(dest => dest.ExamName, opt => opt.MapFrom(qe => qe.Name))
            .ForMember(dest => dest.ExamAttempts, opt => opt.MapFrom(qe => qe.Attempts))
            .ForMember(dest => dest.ExamTime, opt => opt.MapFrom(qe => qe.Time))
            .ForMember(dest => dest.ExamPassMark, opt => opt.MapFrom(qe => qe.PassMark))
            .ReverseMap();

        CreateMap<QualityExamSection, QualityExamTakeSectionViewModel>().ReverseMap();
        CreateMap<QualityExamSectionField, QualityExamTakeSectionFieldViewModel>().ReverseMap();
        CreateMap<QualityExamSectionFieldOption, QualityExamTakeSectionFieldOptionViewModel>().ReverseMap();
        CreateMap<QualityExam, QualityExamTakeViewModel>()
            .ForMember(dest => dest.QualityName, opt => opt.MapFrom(qe => qe.Quality.Name))
            .ReverseMap();

        CreateMap<QualityExamSection, QualityExamUserTakeSection>()
            .ForMember(dest => dest.ExamSectionId, opt => opt.MapFrom(qes => qes.Id))
            .ForMember(dest => dest.Id, opt => opt.Ignore())
            .ForMember(dest => dest.Active, opt => opt.Ignore())
            .ForMember(dest => dest.Mark, opt => opt.Ignore())
            .ForMember(dest => dest.TakeId, opt => opt.Ignore())
            .ReverseMap();

        CreateMap<QualityExamSectionField, QualityExamUserTakeSectionField>()
            .ForMember(dest => dest.ExamFieldId, opt => opt.MapFrom(qesf => qesf.Id))
            .ForMember(dest => dest.Id, opt => opt.Ignore())
            .ForMember(dest => dest.Active, opt => opt.Ignore())
            .ForMember(dest => dest.Mark, opt => opt.Ignore())
            .ForMember(dest => dest.Answer, opt => opt.Ignore())
            .ForMember(dest => dest.SectionId, opt => opt.Ignore())
            .ReverseMap();

        CreateMap<QualityExamSectionFieldOption, QualityExamUserTakeSectionFieldOption>()
            .ForMember(dest => dest.ExamOptionId, opt => opt.MapFrom(qesfo => qesfo.Id))
            .ForMember(dest => dest.Id, opt => opt.Ignore())
            .ForMember(dest => dest.Active, opt => opt.Ignore())
            .ForMember(dest => dest.Mark, opt => opt.Ignore())
            .ForMember(dest => dest.FieldId, opt => opt.Ignore())
            .ReverseMap();

        CreateMap<JObject, QualityExamUserTake>().ConvertUsing(new JsonConverter<QualityExamUserTake>());

        #endregion

        #region TRAINING

        CreateMap<Training, ShowTrainingViewModel>()
            .ForMember(dest => dest.Training, opt => opt.MapFrom(t => t))
            .ForMember(dest => dest.AreaName, opt => opt.MapFrom(t => t.Area.Name))
            .ForMember(dest => dest.Trainers, opt => opt.MapFrom(t => t.Users.Where(tu => tu.Trainer).Select(tu => tu.User).ToList()))
            .ForMember(dest => dest.Attendants, opt => opt.MapFrom(t => t.Users.Where(tu => tu.Attendant).Select(tu => tu.User).ToList()))
            .ForMember(dest => dest.Tests, opt => opt.MapFrom(t => t.Tests.ToList()));

        CreateMap<TrainingTestUserTake, StateTestTakeViewModel>()
            .ForMember(dest => dest.Id, opt => opt.MapFrom(ttut => ttut.Id))
            .ForMember(dest => dest.Attempt, opt => opt.MapFrom(ttut => ttut.Attempt))
            .ForMember(dest => dest.Active, opt => opt.MapFrom(ttut => ttut.Active))
            .ForMember(dest => dest.Pass, opt => opt.MapFrom(ttut => ttut.Pass))
            .ForMember(dest => dest.Test, opt => opt.MapFrom(ttut => ttut.TestUser.Test))
            .ForMember(dest => dest.User, opt => opt.MapFrom(ttut => ttut.TestUser.User));

        CreateMap<TrainingTestUser, StateTestPendingViewModel>()
            .ForMember(dest => dest.Test, opt => opt.MapFrom(ttu => ttu.Test))
            .ForMember(dest => dest.User, opt => opt.MapFrom(ttu => ttu.User));

        CreateMap<Training, StateTrainingViewModel>()
            .ForMember(dest => dest.Training, opt => opt.MapFrom(t => t))
            .ForMember(dest => dest.AreaName, opt => opt.MapFrom(t => t.Area.Name))
            .ForMember(dest => dest.Takes, opt => opt.MapFrom(t => t.Tests.SelectMany(tt => tt.Users.SelectMany(ttu => ttu.Takes)).ToList()))
            .ForMember(dest => dest.Pending, opt => opt.MapFrom(t => t.Tests.SelectMany(tt => tt.Users).Where(ttu => !(ttu.Takes.Count > 0)).ToList()));

        CreateMap<Training, EditTrainingViewModel>()
            .ForMember(dest => dest.Training, opt => opt.MapFrom(t => t))
            .ForMember(dest => dest.AreaName, opt => opt.MapFrom(t => t.Area.Name))
            .ForMember(dest => dest.Trainers, opt => opt.MapFrom(t => t.Users.Where(tu => tu.Trainer).Select(tu => tu.User).ToList()))
            .ForMember(dest => dest.Attendants, opt => opt.MapFrom(t => t.Users.Where(tu => tu.Attendant).Select(tu => tu.User).ToList()))
            .ForMember(dest => dest.Tests, opt => opt.MapFrom(t => t.Tests.ToList()))
            .ForMember(dest => dest.OwnedAreas, opt => opt.Ignore())
            .ReverseMap()
            .ForMember(dest => dest.Name, opt => opt.MapFrom(m => m.Training.Name))
            .ForMember(dest => dest.AreaId, opt => opt.MapFrom(m => m.Training.AreaId))
            .ForMember(dest => dest.Description, opt => opt.MapFrom(m => m.Training.Description))
            .ForMember(dest => dest.StartDate, opt => opt.MapFrom(m => m.Training.StartDate))
            .ForMember(dest => dest.EndDate, opt => opt.MapFrom(m => m.Training.EndDate))
            .ForMember(dest => dest.Area, opt => opt.Ignore())
            .ForMember(dest => dest.Users, opt => opt.Ignore())
            .ForMember(dest => dest.Tests, opt => opt.Ignore())
            .ForMember(dest => dest.Active, opt => opt.Ignore());

        CreateMap<TrainingTestSection, TrainingTestSectionViewModel>().ReverseMap();
        CreateMap<TrainingTestSectionField, TrainingTestSectionFieldViewModel>().ReverseMap();
        CreateMap<TrainingTestSectionFieldOption, TrainingTestSectionFieldOptionViewModel>().ReverseMap();

        CreateMap<TrainingTest, TrainingTestViewModel>()
            .ForMember(dest => dest.TrainingName, opt => opt.MapFrom(tt => tt.Training.Name))
            .ReverseMap();

        CreateMap<JObject, TrainingTest>().ConvertUsing(new JsonConverter<TrainingTest>());

        CreateMap<TrainingTest, ConfirmTestTakeViewModel>()
            .ForMember(dest => dest.TrainingId, opt => opt.MapFrom(tt => tt.Training.Id))
            .ForMember(dest => dest.TestId, opt => opt.MapFrom(tt => tt.Id))
            .ForMember(dest => dest.TestName, opt => opt.MapFrom(tt => tt.Name))
            .ForMember(dest => dest.TestAttempts, opt => opt.MapFrom(tt => tt.Attempts))
            .ForMember(dest => dest.TestTime, opt => opt.MapFrom(tt => tt.Time))
            .ForMember(dest => dest.TestPassMark, opt => opt.MapFrom(tt => tt.PassMark))
            .ReverseMap();

        CreateMap<TrainingTestSection, TrainingTestTakeSectionViewModel>().ReverseMap();
        CreateMap<TrainingTestSectionField, TrainingTestTakeSectionFieldViewModel>().ReverseMap();
        CreateMap<TrainingTestSectionFieldOption, TrainingTestTakeSectionFieldOptionViewModel>().ReverseMap();
        CreateMap<TrainingTest, TrainingTestTakeViewModel>()
            .ForMember(dest => dest.TrainingName, opt => opt.MapFrom(tt => tt.Training.Name))
            .ReverseMap();

        CreateMap<TrainingTestSection, TrainingTestUserTakeSection>()
            .ForMember(dest => dest.TestSectionId, opt => opt.MapFrom(tts => tts.Id))
            .ForMember(dest => dest.Id, opt => opt.Ignore())
            .ForMember(dest => dest.Active, opt => opt.Ignore())
            .ForMember(dest => dest.Mark, opt => opt.Ignore())
            .ForMember(dest => dest.TakeId, opt => opt.Ignore())
            .ReverseMap();

        CreateMap<TrainingTestSectionField, TrainingTestUserTakeSectionField>()
            .ForMember(dest => dest.TestFieldId, opt => opt.MapFrom(ttsf => ttsf.Id))
            .ForMember(dest => dest.Id, opt => opt.Ignore())
            .ForMember(dest => dest.Active, opt => opt.Ignore())
            .ForMember(dest => dest.Mark, opt => opt.Ignore())
            .ForMember(dest => dest.Answer, opt => opt.Ignore())
            .ForMember(dest => dest.SectionId, opt => opt.Ignore())
            .ReverseMap();

        CreateMap<TrainingTestSectionFieldOption, TrainingTestUserTakeSectionFieldOption>()
            .ForMember(dest => dest.TestOptionId, opt => opt.MapFrom(ttsfo => ttsfo.Id))
            .ForMember(dest => dest.Id, opt => opt.Ignore())
            .ForMember(dest => dest.Active, opt => opt.Ignore())
            .ForMember(dest => dest.Mark, opt => opt.Ignore())
            .ForMember(dest => dest.FieldId, opt => opt.Ignore())
            .ReverseMap();

        CreateMap<JObject, TrainingTestUserTake>().ConvertUsing(new JsonConverter<TrainingTestUserTake>());

        #endregion

    }

    public class JsonConverter<T> : ITypeConverter<JObject, T>
    {
        public T Convert(JObject source, T destination, ResolutionContext context)
        {
            if (destination == null)
            {
                destination = (T)Activator.CreateInstance(typeof(T));
            }
            JsonSerializer serializer = new JsonSerializer();
            serializer.Populate(source.CreateReader(), destination);
            return destination;
        }
    }

}

