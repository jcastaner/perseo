﻿using AutoMapper;
using Perseo.Core;
using Perseo.Web.Models;
using System.Collections.Generic;
using System.Linq;

public class ModelMappingProfile : Profile
{
    public ModelMappingProfile()
    {

        CreateMap<AreaCode, AreaCodeModel>().ReverseMap();
        CreateMap<AreaCodeWbs, AreaCodeWbsModel>().ReverseMap();
        CreateMap<Area, AreaModel>().ReverseMap();

        CreateMap<Forecast, ForecastModel>().ReverseMap();

        CreateMap<JobConfig, JobConfigModel>().ReverseMap();
        CreateMap<JobForecast, JobForecastModel>().ReverseMap();
        CreateMap<JobKpi, JobKpiModel>().ReverseMap();
        CreateMap<Job, JobModel>().ReverseMap();
        CreateMap<JobSla, JobSlaModel>().ReverseMap();

        CreateMap<Bookmark, BookmarkModel>().ReverseMap();
        CreateMap<Notification, NotificationModel>().ReverseMap();

        CreateMap<NonConformityCard, NonConformityCardModel>().ReverseMap();
        CreateMap<NonConformityCardSign, NonConformityCardSignModel>().ReverseMap();
        CreateMap<QualityAudit, QualityAuditModel>().ReverseMap();
        CreateMap<QualityAuditOwner, QualityAuditOwnerModel>().ReverseMap();
        CreateMap<QualityAuditSectionField, QualityAuditSectionFieldModel>().ReverseMap();
        CreateMap<QualityAuditSectionFieldOption, QualityAuditSectionFieldOptionModel>().ReverseMap();
        CreateMap<QualityAuditSection, QualityAuditSectionModel>().ReverseMap();
        CreateMap<QualityAuditUser, QualityAuditUserModel>().ReverseMap();
        CreateMap<QualityAuditUserTake, QualityAuditUserTakeModel>().ReverseMap();
        CreateMap<QualityAuditUserTakeSectionField, QualityAuditUserTakeSectionFieldModel>().ReverseMap();
        CreateMap<QualityAuditUserTakeSectionFieldOption, QualityAuditUserTakeSectionFieldOptionModel>().ReverseMap();
        CreateMap<QualityAuditUserTakeSection, QualityAuditUserTakeSectionModel>().ReverseMap();
        CreateMap<QualityExam, QualityExamModel>().ReverseMap();
        CreateMap<QualityExamOwner, QualityExamOwnerModel>().ReverseMap();
        CreateMap<QualityExamSectionField, QualityExamSectionFieldModel>().ReverseMap();
        CreateMap<QualityExamSectionFieldOption, QualityExamSectionFieldOptionModel>().ReverseMap();
        CreateMap<QualityExamSection, QualityExamSectionModel>().ReverseMap();
        CreateMap<QualityExamUser, QualityExamUserModel>().ReverseMap();
        CreateMap<QualityExamUserTake, QualityExamUserTakeModel>().ReverseMap();
        CreateMap<Quality, QualityModel>().ReverseMap();
        CreateMap<QualityOwner, QualityOwnerModel>().ReverseMap();
        CreateMap<QualityUser, QualityUserModel>().ReverseMap();

        CreateMap<RequestHistory, RequestHistoryModel>().ReverseMap();
        CreateMap<Request, RequestModel>().ReverseMap();

        CreateMap<Recruit, RecruitModel>().ReverseMap();

        CreateMap<ScheduleDate, ScheduleDateModel>().ReverseMap();
        CreateMap<ScheduleDateTermJob, ScheduleDateTermJobModel>().ReverseMap();
        CreateMap<ScheduleDateTerm, ScheduleDateTermModel>().ReverseMap();

        CreateMap<Training, TrainingModel>().ReverseMap();
        CreateMap<TrainingTest, TrainingTestModel>().ReverseMap();
        CreateMap<TrainingTestOwner, TrainingTestOwnerModel>().ReverseMap();
        CreateMap<TrainingTestSectionField, TrainingTestSectionFieldModel>().ReverseMap();
        CreateMap<TrainingTestSectionFieldOption, TrainingTestSectionFieldOptionModel>().ReverseMap();
        CreateMap<TrainingTestSection, TrainingTestSectionModel>().ReverseMap();
        CreateMap<TrainingTestUser, TrainingTestUserModel>().ReverseMap();
        CreateMap<TrainingTestUserTake, TrainingTestUserTakeModel>().ReverseMap();
        CreateMap<TrainingTestUserTakeSectionField, TrainingTestUserTakeSectionFieldModel>().ReverseMap();
        CreateMap<TrainingTestUserTakeSectionFieldOption, TrainingTestUserTakeSectionFieldOptionModel>().ReverseMap();
        CreateMap<TrainingTestUserTakeSection, TrainingTestUserTakeSectionModel>().ReverseMap();
        CreateMap<TrainingUser, TrainingUserModel>().ReverseMap();

        CreateMap<Calendar, CalendarModel>().ReverseMap();
        CreateMap<Category, CategoryModel>().ReverseMap();
        CreateMap<Collective, CollectiveModel>().ReverseMap();
        CreateMap<Company, CompanyModel>().ReverseMap();
        CreateMap<Office, OfficeModel>().ReverseMap();
        CreateMap<Rate, RateModel>().ReverseMap();
        CreateMap<Role, RoleModel>().ReverseMap();
        CreateMap<Shift, ShiftModel>().ReverseMap();
        CreateMap<UserArea, UserAreaModel>().ReverseMap();
        CreateMap<UserCategory, UserCategoryModel>().ReverseMap();
        CreateMap<UserCollectiveCategory, UserCollectiveCategoryModel>().ReverseMap();
        CreateMap<UserCollective, UserCollectiveModel>().ReverseMap();
        CreateMap<UserContract, UserContractModel>().ReverseMap();
        CreateMap<User, UserModel>().ReverseMap();
        CreateMap<UserOffice, UserOfficeModel>().ReverseMap();
        CreateMap<UserOperational, UserOperationalModel>().ReverseMap();
        CreateMap<UserPasswordHistory, UserPasswordHistoryModel>().ReverseMap();
        CreateMap<UserPosition, UserPositionModel>().ReverseMap();
        CreateMap<UserRate, UserRateModel>().ReverseMap();
        CreateMap<UserRole, UserRoleModel>().ReverseMap();
        CreateMap<UserSalary, UserSalaryModel>().ReverseMap();
        CreateMap<UserShift, UserShiftModel>().ReverseMap();
        CreateMap<UserWorkConditions, UserWorkConditionsModel>().ReverseMap();

    }

}