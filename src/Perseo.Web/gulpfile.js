﻿'use strict';

var gulp = require('gulp'),
    sass = require('gulp-sass'),
    concat = require('gulp-concat'),
    cssmin = require('gulp-cssmin'),
    htmlmin = require('gulp-htmlmin'),
    uglify = require('gulp-uglify'),
    merge = require('merge-stream'),
    del = require('del'),
    bundleconfig = [
        {
            "outputFileName": "wwwroot/css/site.min.css",
            "inputFiles": [
                "wwwroot/css/site.css"
            ]
        },
        {
            "outputFileName": "wwwroot/js/site.min.js",
            "inputFiles": [

                "wwwroot/lib/jquery/dist/jquery.js",
                "wwwroot/lib/popper.js/dist/umd/popper.js",
                "wwwroot/lib/bootstrap/dist/js/bootstrap.js",
                "wwwroot/lib/pace-progress/pace.js",
                "wwwroot/lib/perfect-scrollbar/dist/perfect-scrollbar.js",
                "wwwroot/lib/toastr/toastr.js",
                "wwwroot/lib/coreui/dist/js/coreui.js",

                "wwwroot/lib/bootstrap-multiselect/dist/js/bootstrap-multiselect.js",

                "wwwroot/lib/datatables/DataTables-1.10.18/js/jquery.dataTables.min.js",
                "wwwroot/lib/datatables/DataTables-1.10.18/js/dataTables.bootstrap4.min.js",
                "wwwroot/lib/datatables/Responsive-2.2.2/js/dataTables.responsive.min.js",
                "wwwroot/lib/datatables/Responsive-2.2.2/js/responsive.bootstrap4.min.js",
                "wwwroot/lib/datatables/Select-1.3.0/js/dataTables.select.min.js",
                "wwwroot/lib/datatables/checkboxes-1.2.11/js/dataTables.checkboxes.min.js",
                "wwwroot/lib/datatables/Buttons-1.5.6/js/dataTables.buttons.min.js",
                "wwwroot/lib/datatables/Buttons-1.5.6/js/buttons.html5.min.js",
                "wwwroot/lib/datatables/JSZip-2.5.0/jszip.min.js",

                "wwwroot/lib/jquery-sortable/source/js/jquery-sortable-min.js",

                "wwwroot/js/site.js"
            ],
            "minify": {
                "enabled": true,
                "renameLocals": true
            },
            "sourceMap": false
        }
    ]

const regex = {
    css: /\.css$/,
    html: /\.(html|htm)$/,
    js: /\.js$/,
    sass: /\.scss$/
};

gulp.task('sass', function () {
    return gulp.src('wwwroot/scss/site.scss')
        .pipe(sass())
        .pipe(gulp.dest('wwwroot/css/'))
});

gulp.task('min:js',
    () => merge(getBundles(regex.js).map(bundle => {
        return gulp.src(bundle.inputFiles, { base: '.' })
            .pipe(concat(bundle.outputFileName))
            .pipe(uglify())
            .pipe(gulp.dest('.'));
    })));

gulp.task('min:css',
    () => merge(getBundles(regex.css).map(bundle => {
        return gulp.src(bundle.inputFiles, { base: '.' })
            .pipe(concat(bundle.outputFileName))
            .pipe(cssmin())
            .pipe(gulp.dest('.'));
    })));
/*
gulp.task('min:html',
    () => merge(getBundles(regex.html).map(bundle => {
        return gulp.src(bundle.inputFiles, { base: '.' })
            .pipe(concat(bundle.outputFileName))
            .pipe(htmlmin({ collapseWhitespace: true, minifyCSS: true, minifyJS: true }))
            .pipe(gulp.dest('.'));
    })));
*/
gulp.task('min', gulp.series(['min:js', 'min:css'/*, 'min:html'*/]));

gulp.task('clean', () => {
    return del(bundleconfig.map(bundle => bundle.outputFileName));
});

gulp.task('watch', () => {

    getBundles(regex.js).forEach(
        bundle => gulp.watch(bundle.inputFiles, gulp.series(["min:js"])));

    getBundles(regex.css).forEach(
        bundle => gulp.watch(bundle.inputFiles, gulp.series(["min:css"])));
    /*
    getBundles(regex.html).forEach(
        bundle => gulp.watch(bundle.inputFiles, gulp.series(['min:html'])));*/

});

const getBundles = (regexPattern) => {
    return bundleconfig.filter(bundle => {
        return regexPattern.test(bundle.outputFileName);
    });
};

gulp.task('default', gulp.series(['min']));