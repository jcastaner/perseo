﻿
namespace Perseo.Web
{
    public static class Constants
    {

        public const string color_1 = "#536c75";
        public const string color_10 = "#8bc34a";
        public const string color_11 = "#ff83cf";
        public const string color_12 = "#2a80d8";
        public const string color_13 = "#a9380f";
        public const string color_14 = "#009688";
        public const string color_2 = "#536c75";
        public const string color_3 = "#ffc107";
        public const string color_4 = "#8d9da3";
        public const string color_5 = "#4dbd74";
        public const string color_6 = "#ce1987";
        public const string color_7 = "#ce1987";
        public const string color_8 = "#6d93f7";
        public const string color_9 = "#ff5722";
        public const string color_blue = "#6d93f7";
        public const string color_brown = "#a9380f";
        public const string color_dark_blue = "#2a80d8";
        public const string color_dark_gray = "#536c75";
        public const string color_dark_green = "#009688";
        public const string color_gray = "#8d9da3";
        public const string color_green = "#4dbd74";
        public const string color_light_blue = "#92aef9";
        public const string color_light_gray = "#d2d2d2";
        public const string color_light_green = "#8bc34a";
        public const string color_orange = "#ff5722";
        public const string color_pink = "#ff83cf";
        public const string color_purple = "#ce1987";
        public const string color_yellow = "#ffc107";
        public const string color_grey95 = "#f2f2f2";

    }
}
