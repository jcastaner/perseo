﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Globalization;

namespace Perseo.Web.ViewComponents
{
    public class MonthGridStaffing : ViewComponent
    {
        public IViewComponentResult Invoke(int Year, int Month,Object Rows,  List<String> Slots)
        {
            var MonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(Month);
            int DaysOfMonth = DateTime.DaysInMonth(Year, Month);
            int FirstWeekDayOfMonth = (int)(new DateTime(Year, Month, 1)).AddDays(-1).DayOfWeek;
            var WeekDays3 = CultureInfo.CurrentCulture.DateTimeFormat.DayNames;
            string[] WeekDays = { "L", "M", "X", "J", "V", "S", "D" };
            var MaxMonthDays = DaysOfMonth + FirstWeekDayOfMonth <= 35 ? 35 : 42;

            string MonthTrail = "";
            string MonthHead = "";
            for (var i = 0; i < FirstWeekDayOfMonth; i++)
            {
                MonthHead += "<div class='col d-lg-none'></div>";
            }

            for (var i = DaysOfMonth + FirstWeekDayOfMonth; i < MaxMonthDays; i++)
            {
                MonthTrail += "<div class='col d-lg-none'></div>";
            }

            ViewBag.HeaderCols = 2;
            ViewBag.ContentCols = 10;
            ViewBag.TimeSlots  = Slots;
            ViewBag.MonthHead = MonthHead;
            ViewBag.MonthTrail = MonthTrail;
            ViewBag.DaysOfMonth = DaysOfMonth;
            ViewBag.FirstWeekDayOfMonth = FirstWeekDayOfMonth;
            ViewBag.WeekDays = WeekDays;
            ViewBag.MaxMonthDays = MaxMonthDays;

            ViewBag.Year = Year;
            ViewBag.Month = Month;
            ViewBag.Rows = Rows;

            return View();
        }
    }
}
