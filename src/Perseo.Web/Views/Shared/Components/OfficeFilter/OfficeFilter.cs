﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Perseo.Web.Models;

namespace Perseo.Web.ViewComponents
{
    public class OfficeFilter : ViewComponent
    {
        public IViewComponentResult Invoke( List<OfficeModel> selectedOffices, List<OfficeModel> areasOffices)
        {
            ViewBag.SelectedOffices = selectedOffices;
            ViewBag.AreasOffices = areasOffices;
            return View();
        }
    }
}
