﻿using Microsoft.AspNetCore.Mvc;

namespace Perseo.Web.ViewComponents
{
    public class YearFilterViewComponent : ViewComponent
    {
        public IViewComponentResult Invoke(int Year, string Url) 
        {
            ViewBag.Year = Year;
            ViewBag.Url = Url;
            return View();
        }
    }
}
