﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Globalization;

namespace Perseo.Web.ViewComponents
{
    public class MonthGridSchedule : ViewComponent
    {
        public IViewComponentResult Invoke(int Year, int Month, List<Models.ScheduleRow> Rows) 
        {

            var MonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(Month);
            int DaysOfMonth = DateTime.DaysInMonth(Year, Month);
            int FirstWeekDayOfMonth = (int)(new DateTime(Year, Month, 1)).AddDays(-1).DayOfWeek;
            List<string> WeekDays = new List<string>();
            for (int wd = 0; wd < 7; wd++)
            {
                string name = CultureInfo.CurrentCulture.DateTimeFormat.AbbreviatedDayNames[(wd + 1) % 7];
                name = char.ToUpper(name[0]) + name.Substring(1, 1).ToLower();
                WeekDays.Add(name);
            }
            var MaxMonthDays = DaysOfMonth + FirstWeekDayOfMonth <= 35 ? 35 : 42;

            string MonthTrail = "";
            string MonthHead = "";
            for (var i = 0; i < FirstWeekDayOfMonth; i++)
            {
                MonthHead += "<div class='col d-lg-none'></div>";
            }

            for (var i = DaysOfMonth + FirstWeekDayOfMonth; i < MaxMonthDays; i++)
            {
                MonthTrail += "<div class='col d-lg-none'></div>";
            }

            ViewBag.HeaderCols = 3;
            ViewBag.ContentCols = 9;
            ViewBag.MonthHead = MonthHead;
            ViewBag.MonthTrail = MonthTrail;
            ViewBag.DaysOfMonth = DaysOfMonth;
            ViewBag.FirstWeekDayOfMonth = FirstWeekDayOfMonth;
            ViewBag.WeekDays = WeekDays;
            ViewBag.MaxMonthDays = MaxMonthDays;

            ViewBag.Year = Year;
            ViewBag.Month = Month;
            ViewBag.Rows = Rows;

            return View();
        }
    }
}
