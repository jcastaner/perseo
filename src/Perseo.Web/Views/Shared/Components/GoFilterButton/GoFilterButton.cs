﻿using Microsoft.AspNetCore.Mvc;

namespace Perseo.Web.ViewComponents
{
    public class GoFilterButton : ViewComponent
    {
        public IViewComponentResult Invoke(string BaseUrl = null)
        {
            ViewBag.BaseUrl = BaseUrl;
            return View();
        }
    }
}
