﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Globalization;

namespace Perseo.Web.ViewComponents
{
    public class MonthGrid : ViewComponent
    {
        public IViewComponentResult Invoke(int Year, int Month, object Rows, List<String> Header, bool Editable)
        {
            var MonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(Month);
            int DaysOfMonth = DateTime.DaysInMonth(Year, Month);
            int FirstWeekDayOfMonth = (int)(new DateTime(Year, Month, 1)).AddDays(-1).DayOfWeek;
            List<string> WeekDays = new List<string>();
            for (int wd = 0; wd < 7; wd++)
            {
                string name = CultureInfo.CurrentCulture.DateTimeFormat.AbbreviatedDayNames[(wd + 1) % 7];
                name = char.ToUpper(name[0]) + name.Substring(1, 1).ToLower();
                WeekDays.Add(name);
            }
            var MaxMonthDays = DaysOfMonth + FirstWeekDayOfMonth <= 35 ? 35 : 42;
            string MonthTrail = "";
            string MonthHead = "";
            for (var i = 0; i < FirstWeekDayOfMonth; i++)
            {
                MonthHead += "<div class='col d-lg-none'></div>";
            }

            for (var i = DaysOfMonth + FirstWeekDayOfMonth; i < MaxMonthDays; i++)
            {
                MonthTrail += "<div class='col d-lg-none'></div>";
            }
            ViewBag.Editable = Editable;
            ViewBag.MonthHead = MonthHead;
            ViewBag.MonthTrail = MonthTrail;
            ViewBag.DaysOfMonth = DaysOfMonth;
            ViewBag.FirstWeekDayOfMonth = FirstWeekDayOfMonth;
            ViewBag.WeekDays = WeekDays;
            ViewBag.MaxMonthDays = MaxMonthDays;
            ViewBag.Year = Year;
            ViewBag.Month = Month;

            ViewBag.Rows = Rows;
            if (Rows.GetType().Equals(typeof(List<Models.ScheduleRow>))){
                ViewBag.HeaderCols = 3;
                ViewBag.ContentCols = 9;
                return View("~/Views/Shared/Components/MonthGridSchedule/Default.cshtml");
            }
            else if (Rows.GetType().Equals(typeof(Dictionary<int, List<double>>)))
            {
                ViewBag.TimeSlots = Header;
                ViewBag.HeaderCols = 2;
                ViewBag.ContentCols = 10;
                return View("~/Views/Shared/Components/MonthGridStaffing/Default.cshtml");

            }
            return null;
        }
    }
}
