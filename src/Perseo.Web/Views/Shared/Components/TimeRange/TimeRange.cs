﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Perseo.Core;

namespace Perseo.Web.ViewComponents
{
    public class TimeRange : ViewComponent
    {
        public IViewComponentResult Invoke(string Pattern)
        {
            ViewBag.Pattern = Pattern;
            return View();
        }
    }
}
