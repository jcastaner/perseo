﻿using Microsoft.AspNetCore.Mvc;

namespace Perseo.Web.ViewComponents
{
    public class PageTitleViewComponent : ViewComponent
    {
        public IViewComponentResult Invoke(string Title, string Subtitle, string Avatar) 
        {
            ViewBag.Title = Title;
            ViewBag.SubtTitle = Subtitle;
            ViewBag.Avatar = Avatar;
            return View();
        }
    }
}
