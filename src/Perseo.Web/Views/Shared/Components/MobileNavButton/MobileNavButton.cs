﻿using Microsoft.AspNetCore.Mvc;

namespace Perseo.Web.ViewComponents
{
    public class MobileNavButton : ViewComponent
    {
        public IViewComponentResult Invoke() 
        {
            return View();
        }
    }
}
