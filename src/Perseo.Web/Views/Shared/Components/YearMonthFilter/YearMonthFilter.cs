﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Reflection;

namespace Perseo.Web.ViewComponents
{
    public class YearMonthFilter : ViewComponent
    {
        public IViewComponentResult Invoke(int Year, int Month, string Url)
        {
            ViewBag.Year = Year;
            ViewBag.Month = Month;
            ViewBag.Url = Url;
            return View();
        }
    }
}
