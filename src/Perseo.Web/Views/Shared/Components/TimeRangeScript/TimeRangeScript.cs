﻿using Microsoft.AspNetCore.Mvc;

namespace Perseo.Web.ViewComponents
{
    public class TimeRangeScript : ViewComponent
    {
        public IViewComponentResult Invoke()
        {
            return View("~/App/ViewComponents/TimeRange/TimeRangeScriptView.cshtml");
        }
    }
}
