﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Perseo.Core;
using System.Linq;

namespace Perseo.Web.ViewComponents
{
    public class JobNavigator : ViewComponent
    {
        public IViewComponentResult Invoke(List<Area> Areas, List<int> SelectedAreas, List<Job> Jobs, List<int> SelectedJobs, bool Multiple = false)
        {
            ViewBag.Jobs = Jobs.Where(x => x.Type == JobType.Productive || x.Type == JobType.Organizational || x.Type == JobType.Productive).ToList();
            ViewBag.SelectedJobs = SelectedJobs;
            ViewBag.Areas = Areas.Where(x => x.Type != AreaType.General).ToList();
            ViewBag.SelectedAreas = SelectedAreas;
            ViewBag.Multiple = Multiple;
            return View();
        }
    }
}
