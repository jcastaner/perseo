﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Perseo.Core;
using System.Linq;
using Perseo.Web.Models;

namespace Perseo.Web.ViewComponents
{
    public class AreaJobFilter: ViewComponent
    {
        public IViewComponentResult Invoke(List<AreaModel> OwnedAreas, List<int> SelectedAreasIds, List<JobModel> OwnedJobs, List<int> SelectedJobsIds, bool Multiple = false)
        {
            ViewBag.OwnedJobs = OwnedJobs.Where(x => x.Type == JobType.Productive || x.Type == JobType.Organizational || x.Type == JobType.Productive).ToList();
            ViewBag.SelectedJobsIds = SelectedJobsIds;
            ViewBag.OwnedAreas = OwnedAreas.Where(x => x.Type != AreaType.General).ToList();
            ViewBag.SelectedAreasIds = SelectedAreasIds;
            ViewBag.Multiple = Multiple;
            return View();
        }
    }
}
