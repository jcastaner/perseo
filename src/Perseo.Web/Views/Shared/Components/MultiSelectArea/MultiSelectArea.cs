﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Perseo.Core;

namespace Perseo.Web.ViewComponents
{
    public class MultiSelectArea : ViewComponent
    {
        public IViewComponentResult Invoke(List<Area> Items, List<int> SelectedItems, string Id = null, string Name = null, bool Multiple = false)
        {
            ViewBag.Items = Items;
            ViewBag.SelectedItems = SelectedItems;
            ViewBag.Id = Id;
            ViewBag.Name = Name;
            ViewBag.Multiple = Multiple;
            return View();
        }
    }
}
