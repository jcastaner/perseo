﻿using Perseo.Core;
using Microsoft.AspNetCore.Mvc;

namespace Perseo.Web.ViewComponents
{
    public class ActiveFilter : ViewComponent
    {
        public IViewComponentResult Invoke( ActiveType? state)
        {
            ViewBag.State = state;
            return View();
        }
    }
}
