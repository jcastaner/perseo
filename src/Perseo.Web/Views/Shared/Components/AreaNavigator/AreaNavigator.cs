﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Perseo.Core;
using System.Linq;

namespace Perseo.Web.ViewComponents
{
    public class AreaNavigator : ViewComponent
    {
        public IViewComponentResult Invoke(List<Area> Areas, List<int> SelectedAreas, bool Multiple = false)
        {
            ViewBag.Areas = Areas.Where(x=>x.Type != AreaType.General).ToList();
            ViewBag.SelectedAreas = SelectedAreas;
            ViewBag.Multiple = Multiple;
            return View();
        }
    }
}
