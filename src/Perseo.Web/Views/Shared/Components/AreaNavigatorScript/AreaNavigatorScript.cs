﻿using Microsoft.AspNetCore.Mvc;

namespace Perseo.Web.ViewComponents
{
    public class AreaNavigatorScript : ViewComponent
    {
        public IViewComponentResult Invoke()
        {
            return View();
        }
    }
}
