﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Perseo.Core;
using System.Linq;
using Perseo.Web.Models;

namespace Perseo.Web.ViewComponents
{
    public class AreaFilter : ViewComponent
    {
        public IViewComponentResult Invoke(List<AreaModel> OwnedAreas, List<int> SelectedAreasIds, bool Multiple = false)
        {
            ViewBag.OwnedAreas = OwnedAreas.Where(x=>x.Type != AreaType.General).ToList();
            ViewBag.SelectedAreasIds = SelectedAreasIds;
            ViewBag.Multiple = Multiple;
            return View();
        }
    }
}
