
namespace Perseo.Web.Models
{  
    public class TrainingTestUserTakeSectionFieldOptionModel : Model
    {
		
        public bool? Mark { get; set; }
        
        public int TestOptionId { get; set; }

        public int FieldId { get; set; }

    }  
}  