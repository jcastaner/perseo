
namespace Perseo.Web.Models
{  
    public class TrainingTestUserTakeSectionModel : Model
    {
        public double Mark { get; set; }

        public int TestSectionId { get; set; }

        public int TakeId { get; set; }
		
    }  
}  