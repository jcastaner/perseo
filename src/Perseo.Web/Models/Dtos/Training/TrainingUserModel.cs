
namespace Perseo.Web.Models
{  
    public class TrainingUserModel : Model
    {
		
        public int TrainingId { get; set; }

        public int UserId { get; set; }

        public bool Trainer { get; set; }
		
        public bool Attendant { get; set; }
		
        public bool TrainingCompleted { get; set; }

    }  
}  