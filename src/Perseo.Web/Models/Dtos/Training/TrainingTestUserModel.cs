
namespace Perseo.Web.Models
{  
    public class TrainingTestUserModel : Model
    {
        public int TestId { get; set; }

        public int UserId { get; set; }

    }
}  