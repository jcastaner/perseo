using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace Perseo.Web.Models
{  
    public class TrainingTestModel : ModelDateRange
    {
        [Display(Name = "Prueba")]
        [Required(ErrorMessage = "El campo Nombre es obligatorio.")]
        public string Name { get; set; }

        [Display(Name = "Descripci�n")]
        [Required(ErrorMessage = "El campo Descripci�n es obligatorio.")]
        public string Description { get; set; }

        [Display(Name = "Nota de Corte")]
        public double PassMark { get; set; }

        [Display(Name = "Intentos M�ximos")]
        public int Attempts { get; set; }

        [Display(Name = "Tiempo Disponible")]
        public int? Time { get; set; }

        [HiddenInput]
        public int TrainingId { get; set; }
    }  
}  