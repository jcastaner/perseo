
namespace Perseo.Web.Models
{  
    public class TrainingTestSectionFieldOptionModel : Model
    {
		
        public int Order { get; set; }
		
        public string Content { get; set; }
		
        public bool? Mark { get; set; }
		
        public double Value { get; set; }
        
        public int FieldId { get; set; }

    }  
}  