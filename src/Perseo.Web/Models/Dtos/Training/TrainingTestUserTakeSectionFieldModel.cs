
namespace Perseo.Web.Models
{  
    public class TrainingTestUserTakeSectionFieldModel : Model
    {
        public double Mark { get; set; }
		
        public string Answer { get; set; }

        public int TestFieldId { get; set; }
	
        public int SectionId { get; set; }
		
    }  
}  