using System.ComponentModel.DataAnnotations;

namespace Perseo.Web.Models
{  
    public class TrainingModel : ModelDateRange
    {
        [Display(Name = "Acci�n Formativa")]
        [Required(ErrorMessage = "El campo Nombre es obligatorio.")]
        public string Name { get; set; }

        [Display(Name = "Descripci�n")]
        [Required(ErrorMessage = "El campo Descripci�n es obligatorio.")]
        public string Description { get; set; }

        [Display(Name = "�rea")]
        [Required(ErrorMessage = "El campo �rea es obligatorio.")]
        public int AreaId { get; set; }
    }  
}  