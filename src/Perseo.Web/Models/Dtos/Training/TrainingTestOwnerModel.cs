
namespace Perseo.Web.Models
{  
    public class TrainingTestOwnerModel : Model
    {
		
        public int TestId { get; set; }

        public int UserId { get; set; }

    }
}  