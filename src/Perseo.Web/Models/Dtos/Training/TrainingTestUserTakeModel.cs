
namespace Perseo.Web.Models
{  
    public class TrainingTestUserTakeModel : ModelDateRange
    {
        public int TestUserId { get; set; }

        public int Attempt { get; set; }

        public double Mark { get; set; }
		
        public bool? Pass { get; set; }

        public string Content { get; set; }
    }
}  