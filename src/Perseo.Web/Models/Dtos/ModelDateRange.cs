using System;
using System.ComponentModel.DataAnnotations;

namespace Perseo.Web.Models
{  
    public abstract class ModelDateRange : Model
    {
        [Display(Name = "Fecha Inicio")]
        public DateTime StartDate { get; set; }

        [Display(Name = "Fecha Fin")]
        public DateTime? EndDate { get; set; } 
    }  
}  