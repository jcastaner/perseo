﻿  
namespace Perseo.Web.Models
{
	public abstract class Model 
	{
        public int Id { get; set; }

        public bool Active { get; set; }
      
	}
}
