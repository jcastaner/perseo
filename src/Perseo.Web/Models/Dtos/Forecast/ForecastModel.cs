﻿using System;

namespace Perseo.Web.Models
{
    public class ForecastModel : Model
    {
		
        public DateTime Date { get; set; }

        public int Year { get; set; }
		
        public int Month { get; set; }
		
        public double Events { get; set; }

        public int JobId { get; set; }
        
    }
}
