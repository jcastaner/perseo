﻿using Perseo.Core;
using System;

namespace Perseo.Web.Models
{
    public class RequestHistoryModel : Model
    {

        public DateTime EntryDate { get; set; }

        public int RequestId { get; set; }

        public string Comment { get; set; }

        public RequestState State { get; set; }

        public int UserId { get; set; }

    }
}
