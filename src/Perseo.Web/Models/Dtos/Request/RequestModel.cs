﻿using System;
using Perseo.Core;

namespace Perseo.Web.Models
{
    public class RequestModel : Model
    {

        public RequestType Type { get; set; }

        public DateTime RequestDate { get; set; }

        public PriorityType Priority { get; set; }
		
        public string Summary { get; set; }
		
        public string Description { get; set; }

        public int UserId { get; set; }
		
        public int? AssignedUserId { get; set; }

        public string AdditionalData { get; set; }

    }
}
