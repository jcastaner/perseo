
namespace Perseo.Web.Models
{
    public class RateModel : Model
    {
		
        public int Year { get; set; }
		
        public string Name { get; set; }
		
        public string Description { get; set; }
		
        public double Value { get; set; }
		
        public string Currency { get; set; }
		
    } 

}
