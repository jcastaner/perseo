namespace Perseo.Web.Models
{  
    public class UserCategoryModel : ModelDateRange
    {
		
        public int UserId { get; set; }

        public int CategoryId { get; set; }
        
    }  
}  