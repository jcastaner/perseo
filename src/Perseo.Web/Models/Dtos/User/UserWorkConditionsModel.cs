namespace Perseo.Web.Models
{
    public class UserWorkConditionsModel : ModelDateRange
    {
		
        public int UserId { get; set; }
		
        public double WeekWorkHours { get; set; }
		
        public double YearWorkHours { get; set; }
		
        public double YearDoctorHours { get; set; }
		
        public double YearVacationDays { get; set; }
		
        public double YearSpareDays { get; set; }

    }

}