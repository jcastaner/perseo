namespace Perseo.Web.Models
{
    public class UserCollectiveModel : ModelDateRange
    {
		
        public int UserId { get; set; }

        public int CollectiveId { get; set; }

    } 

}