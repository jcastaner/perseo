namespace Perseo.Web.Models
{  
    public class UserPasswordHistoryModel : ModelDateRange
    {
        public int UserId { get; set; }
				
        public string PasswordHash { get; set; }

    }  
}  