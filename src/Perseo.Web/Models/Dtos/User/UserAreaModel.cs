
namespace Perseo.Web.Models
{  
    public class UserAreaModel : ModelDateRange
    {
		
        public bool? Owner { get; set; }
		
        public bool? Authorized { get; set; }
		
        public int UserId { get; set; }

        public int AreaId { get; set; }

    }  
}  