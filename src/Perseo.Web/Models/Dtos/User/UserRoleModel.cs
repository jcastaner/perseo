﻿namespace Perseo.Web.Models
{
    public class UserRoleModel : Model
    {
		
        public int UserId { get; set; }
		
        public int RoleId { get; set; }

    }
}
