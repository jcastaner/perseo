namespace Perseo.Web.Models
{  
    public class UserCollectiveCategoryModel : ModelDateRange
    {  
        public string Category { get; set; }

        public int UserId { get; set; }

    }  
}  