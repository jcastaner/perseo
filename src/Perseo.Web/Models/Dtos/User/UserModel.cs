using System;
using System.ComponentModel.DataAnnotations;
using Perseo.Core;

namespace Perseo.Web.Models
{
    public class UserModel : Model
    {
        [Display(Name = "Usuario")]
        public string UserName { get; set; }

        [Display(Name = "Correo")]
        public string Email { get; set; }

        [Display(Name = "Tel�fono")]
        public string PhoneNumber { get; set; }

        [Display(Name = "M�vil")]
        public string MobileNumber { get; set; }

        [Display(Name = "Usuario de red")]
        public string NetworkUser { get; set; }

        [Display(Name = "Empleado")]
        public int? EmployeeId { get; set; }

        [Display(Name = "Nombre")]
        public string FirstName { get; set; }

        [Display(Name = "Primer Apellido")]
        public string MainSurname { get; set; }

        [Display(Name = "Segundo Apeliido")]
        public string SecondSurname { get; set; }

        [Display(Name = "Nivel de estudios")]
        public AcademicDegree? AcademicDegree { get; set; }
		
        public string Nationality { get; set; }
		
        public string NationalId { get; set; }
		
        public DateTime? BirthDate { get; set; }
		
        public string BirthPlace { get; set; }
		
        public string PersonalPhoneNumber { get; set; }
		
        public string PersonalMobileNumber { get; set; }
		
        public string PersonalEmail { get; set; }
		
        public string Address { get; set; }
		
        public string Zip { get; set; }
		
        public string City { get; set; }
		
        public string State { get; set; }
		
        public string Country { get; set; }

        [Display(Name = "Avatar")]
        public string Avatar { get; set; }


        //public bool EmailConfirmed { get; set; }

        //public int AccessFailedCount { get; set; }

        //public bool LockoutEnabled { get; set; }

        //public DateTimeOffset? LockoutEnd { get; set; }

        //public string ConcurrencyStamp { get; set; }

        //public string SecurityStamp { get; set; }

        //public bool? Flag { get; set; }

        //public int Alerts { get; set; }

        //public int Messages { get; set; }

        //public int Tasks { get; set; }

        //public bool? RefreshCookie { get; set; }

        //public bool? ChangePassword { get; set; }

        //public string PasswordHash { get; set; }

        //public DateTime? LastPasswordChangedDate { get; set; }

        //public DateTime? LastLoginDate { get; set; }


    }
}  