namespace Perseo.Web.Models
{  
    public class UserOfficeModel : ModelDateRange
    {
		
        public int UserId { get; set; }
		
        public int OfficeId { get; set; }
		
    }  
}  