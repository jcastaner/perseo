namespace Perseo.Web.Models
{  
    public class UserSalaryModel : ModelDateRange
    {  
	
        public float Salary { get; set; }  
		
        public float? Variable { get; set; } 
		
        public string Currency { get; set; }

        public int UserId { get; set; }
		
    }  
}  