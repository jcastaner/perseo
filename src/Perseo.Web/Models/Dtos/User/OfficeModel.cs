
namespace Perseo.Web.Models
{  
    public class OfficeModel : Model
    {          

        public string Name { get; set; } 

        public int? CalendarId { get; set; }

    }  
}  