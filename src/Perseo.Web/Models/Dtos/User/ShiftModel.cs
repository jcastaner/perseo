
namespace Perseo.Web.Models
{  
    public class ShiftModel : Model
    {
		
        public int Week { get; set; }
		
        public int WeekDay { get; set; }
		
        public string Pattern { get; set; }
		
        public int UserShiftId { get; set; }
		
    }  
}  