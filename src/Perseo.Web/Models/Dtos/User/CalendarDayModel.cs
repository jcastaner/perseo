using Perseo.Core;

namespace Perseo.Web.Models
{  
    public class CalendarDayModel : Model
    {
        public int CalendarId { get; set; }

        public int Year { get; set; }
        public int Month { get; set; }
        public int Day { get; set; }

        public string Name { get; set; }
        public DayType Type { get; set; }
    }  
}  