namespace Perseo.Web.Models
{  
    public class UserPositionModel : ModelDateRange
    {
		
        public int UserId { get; set; }

        public string Name { get; set; }

    }  
}  