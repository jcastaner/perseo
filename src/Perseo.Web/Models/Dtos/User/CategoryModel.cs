
namespace Perseo.Web.Models
{  
    public class CategoryModel : Model
    {          
        public int Level { get; set; }
		
        public string Name { get; set; } 

    }  
}  