using System.Collections.Generic;

namespace Perseo.Web.Models
{  
    public class UserShiftModel : ModelDateRange
    {
		
        public string Description { get; set; }
		
        public int UserId { get; set; }

    }  
}  