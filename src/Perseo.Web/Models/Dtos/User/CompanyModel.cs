
namespace Perseo.Web.Models
{
    public class CompanyModel : Model
    {
		
        public string Name { get; set; }
		
        public string Description { get; set; }
		
        public bool External { get; set; }

    } 

}
