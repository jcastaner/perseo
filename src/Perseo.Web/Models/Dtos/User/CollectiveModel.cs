
namespace Perseo.Web.Models
{
    public class CollectiveModel : ModelDateRange
    {  
        public string Name { get; set; }
		
        public double YearWorkHours { get; set; }
		
        public double YearDoctorHours { get; set; }
		
        public double YearVacationDays { get; set; }
		
        public double YearSpareDays { get; set; }
		
        public int Other { get; set; }

    } 

}