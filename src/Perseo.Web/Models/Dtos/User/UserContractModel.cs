using Perseo.Core;

namespace Perseo.Web.Models
{
    public class UserContractModel : ModelDateRange
    {

        public int UserId { get; set; }

        public int CompanyId { get; set; }

        public bool External { get; set; }

        public ContractPeriodType ContractPeriod { get; set; }

        public bool? WorkLocalHolydays { get; set; }
		
        public bool? WorkNationalHolidays { get; set; }
		
        public bool? WorkWeekends { get; set; }

    }

}