using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace Perseo.Web.Models
{  
    public class UserOperationalModel : Model
    {
        [Display(Name = "Nombre")]
        public string Key { get; set; }

        [Display(Name = "Valor")]
        public string Value { get; set; }

        [HiddenInput]
        public int UserId { get; set; }
		
    }  
}  