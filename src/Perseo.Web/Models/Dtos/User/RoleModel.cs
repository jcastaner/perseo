﻿
namespace Perseo.Web.Models
{
    public class RoleModel : Model
    {
        public string ConcurrencyStamp { get; set; }
		
        public string Name { get; set; }
		
        public string NormalizedName { get; set; }

    }
}
