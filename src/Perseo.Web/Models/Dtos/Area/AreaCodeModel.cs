﻿
namespace Perseo.Web.Models
{
    public class AreaCodeModel : ModelDateRange
    {

        public string Name { get; set; }
		
        public string Description { get; set; }

        public int AreaId { get; set; }
		
    }
}
