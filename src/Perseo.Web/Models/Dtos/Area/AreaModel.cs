using Perseo.Core;

namespace Perseo.Web.Models
{
    public class AreaModel : ModelDateRange
    {

        public string Name { get; set; }
		
        public string Description { get; set; }
		
        public AreaType Type{ get; set; }

        public int? ParentId { get; set; }
		
        public string Lineage { get; set; }
		
        public int Depth { get; set; }

    }  
}  