﻿
namespace Perseo.Web.Models
{
    public class AreaCodeWbsModel : ModelDateRange
    {

        public string Name { get; set; }
		
        public string Description { get; set; }

        public int? ParentId { get; set; }

        public bool? HasChildren { get; set; }

        public int AreaCodeId { get; set; }

        public int? AreaId { get; set; }

    }
}
