using System;

namespace Perseo.Web.Models
{
    public class ScheduleDateModel : Model
    {
        public DateTime? Date { get; set; }

        public int Year { get; set; }
		
        public int Month { get; set; }
		
        public int Day { get; set; }

        public double TotalHours { get; set; }
		
        public double WorkingHours { get; set; }
		
        public double OtherHours { get; set; }

        public string Acronym { get; set; }
		
        public string Comment { get; set; }

        public int UserId { get; set; }

    } 

}
