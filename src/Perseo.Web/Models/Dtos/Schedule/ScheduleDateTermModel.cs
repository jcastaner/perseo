using Perseo.Core;

namespace Perseo.Web.Models
{
    public class ScheduleDateTermModel : Model
    {

        public string Pattern { get; set; }
		
        public double Hours { get; set; }
		
        public WorkHoursType? HoursType { get; set; }

        public int ScheduleDateId { get; set; }

    } 
}
