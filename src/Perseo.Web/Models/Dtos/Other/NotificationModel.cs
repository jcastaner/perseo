﻿
using Perseo.Core;

namespace Perseo.Web.Models
{
    public class NotificationModel : ModelDateRange
    {

        public int UserId { get; set; }
      
        public NotificationType Type { get; set; }
		
        public bool Priority { get; set; }
		
        public string Summary { get; set; }
		
        public string Description { get; set; }
		
        public string Url { get; set; }
    }
}
