
namespace Perseo.Web.Models
{  
    public class BookmarkModel : Model
    {    
	
        public string Name { get; set; }
		
        public string Url { get; set; }
		
        public bool Local { get; set; }
		
        public bool Group { get; set; }
		
        public int Order { get; set; }

        public int? ParentId { get; set; }
		
        public int UserId { get; set; }
		        
    }  
}  