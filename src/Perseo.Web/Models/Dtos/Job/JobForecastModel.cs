﻿using System;

namespace Perseo.Web.Models
{
    public class JobForecastModel : Model
    {
		
        public DateTime? Date { get; set; }

        public int Year { get; set; }
		
        public int Month { get; set; }
		
        public int Day { get; set; }

        public double Forecast { get; set; }

        public int JobId { get; set; }
		        
    }
}
