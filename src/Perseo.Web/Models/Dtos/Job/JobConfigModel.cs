﻿using Perseo.Core;

namespace Perseo.Web.Models
{
    public class JobConfigModel : ModelDateRange
    {
		
        public BillingType BillingType { get; set; }
		
        public TaskType TaskType { get; set; }
		
        public int JobId { get; set; }
		
    }
}
