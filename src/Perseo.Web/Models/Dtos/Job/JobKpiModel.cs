﻿
namespace Perseo.Web.Models
{
    public class JobKpiModel : ModelDateRange
    {
		
        public int Type { get; set; }
		
        public float? Description { get; set; }
		
        public float? Comparator { get; set; }
		
        public float? Target { get; set; }
		
        public float? Moficator { get; set; }

        public int BonusType { get; set; }
		
        public string BonusDescription { get; set; }
		
        public float? Bonus { get; set; }

        public int MalusType { get; set; }
		
        public string MalusDescription { get; set; }
		
        public float? Malus { get; set; }

        public int JobId { get; set; }
		
    }
}
