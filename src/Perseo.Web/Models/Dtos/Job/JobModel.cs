using Perseo.Core;

namespace Perseo.Web.Models
{  
    public class JobModel : ModelDateRange
    {          

        public string Name { get; set; }
		
        public string Description { get; set; }
		
        public JobType Type { get; set; }
		
        public int AreaId { get; set; }
		
        public bool? Default { get; set; }
		
        public string Acronym { get; set; }

    }  
}  