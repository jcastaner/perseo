using System;

namespace Perseo.Web.Models
{  
    public class RecruitModel : Model
    {          

        public DateTime RequestDate { get; set; } 
		
        public string PositionName { get; set; } 
		
        public string PositionDescription { get; set; }
		
        public string RequestMotivation { get; set; }
		
        public int Vacancies { get; set; }
		
        public bool Mobility { get; set; }
		
        public DateTime IncorporationDate { get; set; }
		
        public string Functions { get; set; }
		
        public string RequiredAdacemicDegree { get; set; }
		
        public string RequiredLanguages { get; set; }
		
        public string RequiredExperience { get; set; }
		
        public string RequiredSkills { get; set; }
		
        public string RequiredKnowloedge { get; set; }
		
        public string RelevantInformation { get; set; }

        public string ContractType { get; set; }
		
        public string ContractModel { get; set; }
		
        public string ContractDuration { get; set; }
		
        public string AcceptedContract { get; set; }
		
        public string Salary { get; set; }
		
        public string Rate { get; set; }
		
        public string BillingType { get; set; }
		
        public float MarginImpact { get; set; }
		
        public string BenefitJustification { get; set; }

    }  
}  