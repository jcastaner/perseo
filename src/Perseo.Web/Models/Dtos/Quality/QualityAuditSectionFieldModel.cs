using Perseo.Core;

namespace Perseo.Web.Models
{  
    public class QualityAuditSectionFieldModel : Model
    {
        public AuditFieldType Type { get; set; }

        public int Order { get; set; }
		
        public string Name { get; set; }

        public bool? Reasoned { get; set; }
		
        public double Weighing { get; set; }

        public int SectionId { get; set; }
		
    }  
}  