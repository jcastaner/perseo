
namespace Perseo.Web.Models
{  
    public class QualityAuditOwnerModel : Model
    {
        public int AuditId { get; set; }
		
        public int UserId { get; set; }

    }
}  