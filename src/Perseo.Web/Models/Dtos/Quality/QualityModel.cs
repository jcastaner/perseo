using System.ComponentModel.DataAnnotations;

namespace Perseo.Web.Models
{  
    public class QualityModel : ModelDateRange
    {
        [Display(Name = "Campa�a")]
        [Required(ErrorMessage = "El campo Nombre es obligatorio.")]
        public string Name { get; set; }

        [Display(Name = "Tipo de Campa�a")]
        [Required(ErrorMessage = "El campo Tipo de Campa�a es obligatorio.")]
        public int Type { get; set; }

        [Display(Name = "Descripci�n")]
        [Required(ErrorMessage = "El campo Descripci�n es obligatorio.")]
        public string Description { get; set; }

        [Display(Name = "�rea")]
        [Required(ErrorMessage = "El campo �rea es obligatorio.")]
        public int AreaId { get; set; }

        public int CreatorId { get; set; }
    }  
}  