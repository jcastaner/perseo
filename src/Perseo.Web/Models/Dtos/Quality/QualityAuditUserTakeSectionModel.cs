
namespace Perseo.Web.Models
{  
    public class QualityAuditUserTakeSectionModel : Model
    {
        public double Mark { get; set; }

        public int AuditSectionId { get; set; }
		
        public int TakeId { get; set; }
		
    }  
}  