﻿using Perseo.Core;

namespace Perseo.Web.Models
{
    public partial class NonConformityCardModel : ModelDateRange
    {
        public string InternalReference { get; set; }

        public bool External { get; set; }
		
        public string ExternalStaff { get; set; }

        public int OriginUserId { get; set; }
		
        public int JobId { get; set; }

        public int TargetUserId { get; set; }

        public string Description { get; set; }
		
        public int? Quantityincidences { get; set; }

        public IncidentTypology? IncidentTypology { get; set; }
		
        public MinuteCost? MinuteCost { get; set; }
		
        public HumanErrorSubtype? HumanErrorSubtype { get; set; }
		
        public RequestState? State { get; set; }

        public string Cause { get; set; }
		
        public string ImmediateAction { get; set; }
		
        public string DefinitiveAction { get; set; }
		
        public string MonitoringEfficiency{ get; set; }

    }
}
