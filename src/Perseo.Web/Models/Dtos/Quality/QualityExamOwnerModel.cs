
namespace Perseo.Web.Models
{  
    public class QualityExamOwnerModel : Model
    {
        public int ExamId { get; set; }
		
        public int UserId { get; set; }
    }
}  