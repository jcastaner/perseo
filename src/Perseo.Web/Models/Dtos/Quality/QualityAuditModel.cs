using System.ComponentModel.DataAnnotations;

namespace Perseo.Web.Models
{  
    public class QualityAuditModel : ModelDateRange
    {
        [Display(Name = "Auditoria")]
        [Required(ErrorMessage = "El campo Nombre es obligatorio.")]
        public string Name { get; set; }

        [Display(Name = "Descripción")]
        [Required(ErrorMessage = "El campo Descripción es obligatorio.")]
        public string Description { get; set; }

        [Display(Name = "Nota de Corte")]
        public double PassMark { get; set; }

        public int QualityId { get; set; }
		
    }  
}  