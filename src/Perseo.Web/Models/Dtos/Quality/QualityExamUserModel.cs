namespace Perseo.Web.Models
{  
    public class QualityExamUserModel : Model
    {
        public int ExamId { get; set; }
		
        public int UserId { get; set; }

        public UserModel User { get; set; }

        public int ExamOwnerId { get; set; }
        public QualityExamOwnerModel ExamOwner { get; set; }
    }
}  