
namespace Perseo.Web.Models
{  
    public class QualityAuditUserTakeModel : ModelDateRange
    {
        public int AuditUserId { get; set; }

        public int AuditorId { get; set; }
		
        public double Mark { get; set; }
		
        public bool? Pass { get; set; }

        public string Content { get; set; }

    }
}  