
namespace Perseo.Web.Models
{  
    public class QualityOwnerModel : Model
    {
        public int QualityId { get; set; }

        public int OwnerId { get; set; }

        public UserModel Owner { get; set; }
    }
}  