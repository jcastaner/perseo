
namespace Perseo.Web.Models
{  
    public class QualityAuditUserTakeSectionFieldModel : Model
    {
        public double Mark { get; set; }
		
        public string Comment { get; set; }

        public int AuditFieldId { get; set; }

        public int SectionId { get; set; }

    }  
}  