
namespace Perseo.Web.Models
{  
    public class QualityAuditSectionModel : Model
    {
		
        public int Order { get; set; }
		
        public string Name { get; set; }
		
        public string Description { get; set; }
		
        public double Weighing { get; set; }

        public int AuditId { get; set; }
		
    }  
}  