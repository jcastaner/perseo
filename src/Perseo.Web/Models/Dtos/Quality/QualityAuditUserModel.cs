namespace Perseo.Web.Models
{  
    public class QualityAuditUserModel : Model
    {
        public int AuditId { get; set; }
		
        public int UserId { get; set; }

        public UserModel User { get; set; }

        public int AuditOwnerId { get; set; }
        public QualityAuditOwnerModel AuditOwner { get; set; }
    }
}  