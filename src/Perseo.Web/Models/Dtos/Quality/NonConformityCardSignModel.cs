﻿namespace Perseo.Web.Models
{
    public class NonConformityCardSignModel : ModelDateRange
    {
		
        public int NonConformityCardId { get; set; }
		
        public string Commentary { get; set; }
		
        public int UserId { get; set; }
		
    }
}
