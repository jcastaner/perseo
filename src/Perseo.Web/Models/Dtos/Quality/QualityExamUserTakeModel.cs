
namespace Perseo.Web.Models
{  
    public class QualityExamUserTakeModel : ModelDateRange
    {
        public int ExamUserId { get; set; }

        public int Attempt { get; set; }
		
        public int Mark { get; set; }
		
        public bool? Pass { get; set; }

        public string Content { get; set; }

    }
}  