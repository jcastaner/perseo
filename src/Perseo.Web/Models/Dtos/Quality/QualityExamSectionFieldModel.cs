using Perseo.Core;

namespace Perseo.Web.Models
{  
    public class QualityExamSectionFieldModel : Model
    {
        public AuditFieldType Type { get; set; }
		
        public int Order { get; set; }

        public string Name { get; set; }

        public string Content { get; set; }
		
        public bool? Reasoned { get; set; }
		
        public double Weighing { get; set; }

        public int SectionId { get; set; }

    }  
}  