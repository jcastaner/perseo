
namespace Perseo.Web.Models
{  
    public class QualityAuditUserTakeSectionFieldOptionModel : Model
    {
		
        public bool? Mark { get; set; }
        
        public int AuditOptionId { get; set; }

        public int FieldId { get; set; }
		
    }  
}  