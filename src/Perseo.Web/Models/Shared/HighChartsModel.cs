using System.Collections.Generic;

namespace Perseo.Web.Models
{
    public class ChartViewModel
    {
        public string Name { get; set; }

        public List<string> Labels { get; set; }

        public List<Serie> Series { get; set; }
        public List<ComplexSerie> ComplexSeries { get; set; }
        public List<ComplexSerie> DrillDowns { get; set; }

        public class Serie
        {
            public string Name { get; set; }
            public List<double> Data { get; set; }
        }
        public class ComplexSerie
        {
            public string Name { get; set; }
            public List<HighCharts.Data> Data { get; set; }
            public string DrillDown { get; set; }
        }

    }

    public class GaugeChartViewModel : ChartViewModel
    {
        new public GaugeSerie Serie { get; set; }

        public class GaugeSerie
        {
            public string Name { get; set; }
            public List<double> Data { get; set; }
        }
    }

    public class TorusChartViewModel : ChartViewModel
    {
        new public TorusSerie Serie { get; set; }

        public class TorusSerie
        {
            public string Name { get; set; }
            public double Data { get; set; }
        }
    }

    public class CardData
    {
        public string Label { get; set; }

        public string MainValue { get; set; }
        public string SecondaryValue { get; set; }

        public int BarValue { get; set; }
    }
}  