
namespace Perseo.Web.Models
{
    public class PaginationModel
    {
        public int RowCount { get; set; }

        public bool ServerSidePagination { get; set; }

        public int DefaultPageSize { get; set; }
    }
}  