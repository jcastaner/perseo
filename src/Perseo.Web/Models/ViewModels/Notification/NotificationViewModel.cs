using Perseo.Core;
using Microsoft.AspNetCore.Mvc;
using System;
using System.ComponentModel.DataAnnotations;

namespace Perseo.Web.Models
{
    public class NotificationViewModel
    {
        [HiddenInput]
        public int Id { get; set; }

        public int UserId { get; set; }

        public string Summary { get; set; }

        public string Description { get; set; }

        public NotificationType? Type { get; set; }

        public bool Active { get; set; }

        public bool Priority { get; set; }

        public string Url { get; set; }

        [DataType(DataType.DateTime)]
        [Display(Name = "Fecha Notificación")]
        public DateTime StartDate { get; set; }
  
        [DataType(DataType.DateTime)]
        [Display(Name = "Fecha Lectura")]
        public DateTime EndDate { get; set; }

    }
}