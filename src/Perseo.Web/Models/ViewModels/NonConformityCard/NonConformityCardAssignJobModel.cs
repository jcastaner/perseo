﻿using Perseo.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Perseo.Web.Models
{
    public class NonConformityCardAssignJobModel
    {
        
        public int JobId { get; set; }
        [Display(Name = "Tarea")]
        public string JobDescription { get; set; }

        public int? Id { get; set; }

        public int NonConformityCardID { get; set; }

    }
}
