﻿using Perseo.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Perseo.Web
{
    public class NonConformityCardViewModel
    {
        [Display(Name = "Referencia interna")]
        public string InternalReference { get; set; }

        [Display(Name = "Origen Externo")]
        public bool External { get; set; }


        public int OriginUserId { get; set; }
        [Display(Name = "Usuario Origen")]
        public string OriginUserName { get; set; }

        public int TargetUserId { get; set; }
        [Display(Name = "Usuario Destino")]
        public string TargetUserName { get; set; }

        public int JobId { get; set; }
        [Display(Name = "Tarea")]
        public string JobDescription { get; set; }


        [Display(Name = "Descripción")]
        public string Description { get; set; }

        [Display(Name = "Estado")]
        public RequestState? State { get; set; }
        

        [Display(Name = "Nº Ficha")]
        public int Id { get; set; }

        [Display(Name = "Fecha Alta")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime StartDate { get; set; }

        [Display(Name = "Fecha Cierre")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? EndDate { get; set; }

        [Display(Name = "Causa")]
        public string Cause  { get; set; }

        [Display(Name = "Acción Inmediata")]
        public string ImmediateAction { get; set; }

        [Display(Name = "Acción definitiva")]
        public string DefinitiveAction { get; set; }

        [Display(Name = "Seguimiento OMC")]
        public string MonitoringEfficiency { get; set; }

        [Display(Name = "Personal Externo")]
        public string ExternalStaff { get; set; }

        public bool Active { get; set; }

        [Display(Name = "Cantidad de incidencias")]
        public int? QuantityIncidences { get; set; }

        [Display(Name = "Tipología")]
        public IncidentTypology? IncidentTypology { get; set; }

        [Display(Name = "Coste en minutos")]
        public MinuteCost? MinuteCost { get; set; }

        [Display(Name = "Subtipo error humano")]
        public HumanErrorSubtype? HumanErrorSubtype { get; set; }

       
    }
}
