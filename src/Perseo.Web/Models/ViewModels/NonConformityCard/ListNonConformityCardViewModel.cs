using System;
using System.Collections.Generic;

namespace Perseo.Web.Models
{
    public class ListNonConformityCardViewModel
    {
        public ChartViewModel AreaChart1 { get; set; }
        public ChartViewModel AreaChart2 { get; set; }
        public ChartViewModel AreaChart3 { get; set; }
        public ChartViewModel AreaChart4 { get; set; }

        public List<NonConformityCardViewModel> Data { get; set; }
    }
}
