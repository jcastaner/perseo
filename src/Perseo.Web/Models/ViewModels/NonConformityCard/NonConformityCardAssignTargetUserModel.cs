﻿using Perseo.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Perseo.Web.Models
{
    public class NonConformityCardAssignTargetUserModel
    {
        
        public int TargetUserId { get; set; }
        [Display(Name = "Usuario")]
        public string TargetUserName { get; set; }

        public int? Id { get; set; }

        public int NonConformityCardID { get; set; }

    }
}
