using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;

namespace Perseo.Web.Models
{
    public class QualityAuditTakeRequestViewModel : QualityAuditModel
    {
        [HiddenInput]
        public int TakeId { get; set; }

        [Required(ErrorMessage = "El campo Resumen es obligatorio.")]
        [Display(Name = "Resumen")]
        public string Summary { get; set; }
    }
}