using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;

namespace Perseo.Web.Models
{
    public class QualityExamTakeViewModel : QualityExamModel
    {
        [Display(Name = "Acci�n Formativa")]
        public string QualityName { get; set; }

        [HiddenInput]
        public int TakeId { get; set; }

        [HiddenInput]
        public int ExamUserId { get; set; }

        public int Attempt { get; set; }

        [Display(Name = "Superado")]
        public bool? Pass { get; set; }

        public ICollection<QualityExamTakeSectionViewModel> Sections { get; set; }

        public QualityExamTakeViewModel()
        {
            Sections = new HashSet<QualityExamTakeSectionViewModel>();
        }
    }

    public class QualityExamTakeSectionViewModel : QualityExamSectionModel
    {
        public new int? Id { get; set; }
        public int? ExamSectionId { get; set; }
        public ICollection<QualityExamTakeSectionFieldViewModel> Fields { get; set; }

        public QualityExamTakeSectionViewModel()
        {
            Fields = new HashSet<QualityExamTakeSectionFieldViewModel>();
        }
    }

    public class QualityExamTakeSectionFieldViewModel : QualityExamSectionFieldModel
    {
        public new int? Id { get; set; }
        public int? ExamFieldId { get; set; }
        public new int Type { get; set; }

        [Display(Name = "Puntuaci�n")]
        public double Mark { get; set; }

        [Display(Name = "Respuesta")]
        public string Answer { get; set; }

        public ICollection<QualityExamTakeSectionFieldOptionViewModel> Options { get; set; }

        public QualityExamTakeSectionFieldViewModel()
        {
            Options = new HashSet<QualityExamTakeSectionFieldOptionViewModel>();
        }
    }

    public class QualityExamTakeSectionFieldOptionViewModel: QualityExamSectionFieldOptionModel
    {
        public new int? Id { get; set; }
        public int? ExamOptionId { get; set; }
        public bool? Checked { get; set; }
    }
}