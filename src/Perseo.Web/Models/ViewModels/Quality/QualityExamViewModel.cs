using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Perseo.Web.Models
{
    public class QualityExamViewModel : QualityExamModel
    {
        [Display(Name = "Campaña")]
        public string QualityName { get; set; }

        public List<UserModel> QualityOwners { get; set; }
        public List<QualityExamUserModel> ExamUsers { get; set; }

        public ICollection<QualityExamSectionViewModel> Sections { get; set; }

        public QualityExamViewModel()
        {
            Sections = new HashSet<QualityExamSectionViewModel>();
        }
    }

    public class QualityExamSectionViewModel : QualityExamSectionModel
    {
        public new int? Id { get; set; }
        public ICollection<QualityExamSectionFieldViewModel> Fields { get; set; }

        public QualityExamSectionViewModel()
        {
            Fields = new HashSet<QualityExamSectionFieldViewModel>();
        }
    }

    public class QualityExamSectionFieldViewModel : QualityExamSectionFieldModel
    {
        public new int? Id { get; set; }
        public new int Type { get; set; }
        public ICollection<QualityExamSectionFieldOptionViewModel> Options { get; set; }

        public QualityExamSectionFieldViewModel()
        {
            Options = new HashSet<QualityExamSectionFieldOptionViewModel>();
        }
    }

    public class QualityExamSectionFieldOptionViewModel : QualityExamSectionFieldOptionModel
    {
        public new int? Id { get; set; }
    }
}