using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;

namespace Perseo.Web.Models
{
    public class ConfirmExamTakeViewModel
    {
        [HiddenInput]
        public int QualityId { get; set; }
        [HiddenInput]
        public int ExamId { get; set; }


        [Display(Name = "Prueba")]
        public string ExamName { get; set; }

        [Display(Name = "Intentos Máximos")]
        public int ExamAttempts { get; set; }

        [Display(Name = "Tiempo Disponible")]
        public int? ExamTime { get; set; }

        [Display(Name = "Nota de Corte")]
        public double ExamPassMark { get; set; }

        [Display(Name = "Intento")]
        public int Attempt { get; set; }

        [HiddenInput]
        public bool AttemptsExceeded { get; set; }
    }
}
