using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Perseo.Web.Models
{
    public class StateQualityViewModel
    {
        public QualityModel Quality { get; set; }

        [Display(Name = "�rea")]
        public string AreaName { get; set; }

        public List<StateExamTakeViewModel> ExamTakes { get; set; }
        public List<StateExamPendingViewModel> ExamPending { get; set; }

        public List<StateAuditTakeViewModel> AuditTakes { get; set; }
        public List<StateAuditPendingViewModel> AuditPending { get; set; }
        public ICollection<StateAuditPendingViewModel> AuthUserAuditPending { get; set; }

        public StateQualityViewModel() : base()
        {
            AuthUserAuditPending = new HashSet<StateAuditPendingViewModel>();
        }
    }

    public class StateFormTakeViewModel
    {
        public int Id { get; set; }

        public bool Active { get; set; }

        public UserModel Owner { get; set; }
        public UserModel User { get; set; }

        public bool? Pass { get; set; }
    }

    public class StateFormPendingViewModel
    {
        public UserModel User { get; set; }
    }

    public class StateExamTakeViewModel : StateFormTakeViewModel
    {
        public QualityExamModel Exam { get; set; }

        public int Attempt { get; set; }
    }

    public class StateExamPendingViewModel : StateFormPendingViewModel
    {
        public QualityExamModel Exam { get; set; }
    }

    public class StateAuditTakeViewModel : StateFormTakeViewModel
    {
        public QualityAuditModel Audit { get; set; }
    }

    public class StateAuditPendingViewModel : StateFormPendingViewModel
    {
        public QualityAuditModel Audit { get; set; }
        public UserModel Owner { get; set; }
    }
}