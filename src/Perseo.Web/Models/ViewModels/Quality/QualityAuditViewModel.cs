using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Perseo.Web.Models
{
    public class QualityAuditViewModel : QualityAuditModel
    {
        [Display(Name = "Campa�a")]
        public string QualityName { get; set; }

        public List<UserModel> QualityOwners { get; set; }
        public List<QualityAuditUserModel> AuditUsers { get; set; }

        public ICollection<QualityAuditSectionViewModel> Sections { get; set; }

        public QualityAuditViewModel()
        {
            Sections = new HashSet<QualityAuditSectionViewModel>();
        }
    }

    public class QualityAuditSectionViewModel : QualityAuditSectionModel
    {
        public new int? Id { get; set; }
        public ICollection<QualityAuditSectionFieldViewModel> Fields { get; set; }

        public QualityAuditSectionViewModel()
        {
            Fields = new HashSet<QualityAuditSectionFieldViewModel>();
        }
    }

    public class QualityAuditSectionFieldViewModel : QualityAuditSectionFieldModel
    {
        public new int? Id { get; set; }
        public new int Type { get; set; }
        public ICollection<QualityAuditSectionFieldOptionViewModel> Options { get; set; }

        public QualityAuditSectionFieldViewModel()
        {
            Options = new HashSet<QualityAuditSectionFieldOptionViewModel>();
        }
    }

    public class QualityAuditSectionFieldOptionViewModel : QualityAuditSectionFieldOptionModel
    {
        public new int? Id { get; set; }
    }
}