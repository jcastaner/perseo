using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Perseo.Web.Models
{  
    public class ShowQualityViewModel
    {
        public QualityModel Quality { get; set; }

        [Display(Name = "�rea")]
        public string AreaName { get; set; }

        [Display(Name = "Auditores")]
        public List<UserModel> Owners { get; set; }
        [Display(Name = "Auditados")]
        public List<UserModel> Users { get; set; }
        [Display(Name = "Modelos de Auditor�a")]
        public List<QualityAuditModel> Audits { get; set; }
        [Display(Name = "Modelos de Examen")]
        public List<QualityExamModel> Exams { get; set; }
    }
} 