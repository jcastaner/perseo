using System;
using System.Collections.Generic;

namespace Perseo.Web.Models
{
    public class ListQualityViewModel
    {
        public ChartViewModel AreaChart1 { get; set; }
        public ChartViewModel AreaChart2 { get; set; }
        public ChartViewModel AreaChart3 { get; set; }
        public ChartViewModel AreaChart4 { get; set; }

        public List<QualityListViewModel> Data { get; set; }
        public PaginationModel Pagination { get; set; }

        public List<AreaModel> OwnedAreas { get; set; }
        public List<int> SelectedAreasIds { get; set; }

        public int Month { get; set; }
        public int Year { get; set; }
    }

    public class QualityListViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string AreaName { get; set; }

        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        
        public int CountAudit { get; set; }
        public int CountExam { get; set; }
        public int CountUserQuality { get; set; }
        public int CountUserQualityCompleted { get; set; }
    }
    
}
