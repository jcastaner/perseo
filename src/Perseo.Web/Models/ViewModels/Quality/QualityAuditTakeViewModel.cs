using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;

namespace Perseo.Web.Models
{
    public class QualityAuditTakeViewModel : QualityAuditModel
    {
        [Display(Name = "Acci�n Formativa")]
        public string QualityName { get; set; }

        [HiddenInput]
        public int TakeId { get; set; }

        [HiddenInput]
        public int AuditUserId { get; set; }

        [HiddenInput]
        public bool? isAuditUser { get; set; }

        [Display(Name = "Superado")]
        public bool? Pass { get; set; }

        public ICollection<QualityAuditTakeSectionViewModel> Sections { get; set; }

        public QualityAuditTakeViewModel()
        {
            Sections = new HashSet<QualityAuditTakeSectionViewModel>();
        }
    }

    public class QualityAuditTakeSectionViewModel : QualityAuditSectionModel
    {
        public new int? Id { get; set; }
        public int? AuditSectionId { get; set; }
        public ICollection<QualityAuditTakeSectionFieldViewModel> Fields { get; set; }

        public QualityAuditTakeSectionViewModel()
        {
            Fields = new HashSet<QualityAuditTakeSectionFieldViewModel>();
        }
    }

    public class QualityAuditTakeSectionFieldViewModel : QualityAuditSectionFieldModel
    {
        public new int? Id { get; set; }
        public int? AuditFieldId { get; set; }
        public new int Type { get; set; }

        [Display(Name = "Puntuaci�n")]
        public double Mark { get; set; }

        [Display(Name = "Comentario del auditor")]
        public string Comment { get; set; }

        public ICollection<QualityAuditTakeSectionFieldOptionViewModel> Options { get; set; }

        public QualityAuditTakeSectionFieldViewModel()
        {
            Options = new HashSet<QualityAuditTakeSectionFieldOptionViewModel>();
        }
    }

    public class QualityAuditTakeSectionFieldOptionViewModel: QualityAuditSectionFieldOptionModel
    {
        public new int? Id { get; set; }
        public int? AuditOptionId { get; set; }
        public bool? Checked { get; set; }
    }
}