using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Perseo.Core;

namespace Perseo.Web.Models
{
    public class QualityAssociateUserViewModel
    {
        [HiddenInput]
        public int QualityId { get; set; }
        [HiddenInput]
        public int FormId { get; set; }
        [HiddenInput]
        public string FormType { get; set; }
        [HiddenInput]
        public int OwnerId { get; set; }
        [HiddenInput]
        public int OwnerUserId { get; set; }
        [HiddenInput]
        public string UsersToAddIds { get; set; }

        public ICollection<User> AddibleUsers { get; set; }
        public QualityAssociateUserViewModel()
        {
            AddibleUsers = new HashSet<User>();
        }
    }
}
