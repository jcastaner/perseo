using System.Collections.Generic;

namespace Perseo.Web.Models
{  
    public class EditQualityViewModel : ShowQualityViewModel
    {
        public List<AreaModel> OwnedAreas { get; set; }
    }
} 