using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;

namespace Perseo.Web.Models
{
    public class AddQualityUserViewModel
    {
        [HiddenInput]
        public int QualityId { get; set; }
        [HiddenInput]
        public string UserType { get; set; }
        [HiddenInput]
        public int? Role { get; set; }
        [HiddenInput]
        public string UsersToAddIds { get; set; }

        public List<UserModel> AddibleUsers { get; set; }
    }
}
