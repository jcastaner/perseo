using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using Perseo.Core;

namespace Perseo.Web.Models
{
    public class BookmarkViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
        public bool Local { get; set; }
        public bool Group { get; set; }
        public int? ParentId { get; set; }
        public int Order { get; set; }

    }
}
