using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Perseo.Web.Models
{
    public class JobForecastViewModel
    {
        [HiddenInput]
        public int Id { get; set; }

        [Display(Name = "A�o")]
        public int Year { get; set; }

        [Display(Name = "Mes")]
        public int Month { get; set; }

        [Display(Name = "D�a")]
        public int Day { get; set; }

        [Display(Name = "Fecha")]
        public DateTime? Date { get; set; }

        [Display(Name = "Eventos")]
        public IEnumerable<double> Events { get; set; }


    }
}