using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Perseo.Web.Models
{
    public class StaffingDayViewModel
    {
        [HiddenInput]
        public int Id { get; set; }

        [Display(Name = "A�o")]
        public int Year { get; set; }

        [Display(Name = "Mes")]
        public int Month { get; set; }

        [Display(Name = "D�a")]
        public int Day { get; set; }

        [Display(Name = "Fecha")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? Date { get; set; }

        [Display(Name = "Eventos")]
        public IEnumerable<double> Events { get; set; }
        [Display(Name = "Agentes Requeridos")]
        public IEnumerable<double> RequiredAgents { get; set; }
        [Display(Name = "Agentes Disponibles")]
        public IEnumerable<double> ActualAgents { get; set; }
    }
}