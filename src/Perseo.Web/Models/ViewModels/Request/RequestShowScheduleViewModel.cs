﻿using Perseo.Core;
using System.Collections.Generic;

namespace Perseo.Web.Models
{ 
    public class RequestShowScheduleViewModel
    {
        public int Id { get; set; }

        public RequestState State { get; set; }

        public int UserId { get; set; }
        public bool IsAreaOwner { get; set; }

        public string AdditionalData { get; set; }
        
        public List<ScheduleViewModel> MonthModels { get; set; }

    }
}
