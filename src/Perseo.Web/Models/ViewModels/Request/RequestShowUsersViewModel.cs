﻿using Perseo.Core;
using System.Collections.Generic;

namespace Perseo.Web.Models
{ 
    public class RequestShowUsersViewModel
    {
        public int Id { get; set; }

        public RequestState State { get; set; }

        public int UserId { get; set; }
        public bool AlreadyReassigned { get; set; }
        public bool IsOriginAreaOwner { get; set; }

        public string AdditionalData { get; set; }

        public AreaModel OriginArea { get; set; }
        public AreaModel DestinationArea { get; set; }
        public List<UserModel> UsersToReassign { get; set; }
    }
}
