using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Perseo.Core;

namespace Perseo.Web.Models
{
    public class AddRequestViewModel
    {

        [Display(Name = "Id")]
        public int Id { get; set; }

        [Required(ErrorMessage = "El campo Resumen es obligatorio.")]
        [Display(Name = "Resumen")]
        public string Summary { get; set; }

        [Required(ErrorMessage = "El campo Descripción es obligatorio.")]
        [Display(Name = "Descripción")]
        public string Description { get; set; }

        [Required(ErrorMessage = "El campo Tipo es obligatorio.")]
        [Display(Name = "Tipo")]
        public RequestType Type { get; set; }

        [Display(Name = "Estado")]
        public RequestState State { get; set; }

        [Display(Name = "Prioridad")]
        public PriorityType Priority { get; set; }

        public int UserId { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "Fecha")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime CreationDate { get; set; }

        [HiddenInput]
        public string AdditionalData { get; set; }

        public List<AssignOptions> AreasOptions { get; set; }
        public List<AddibleUser> AddibleUsers { get; set; }
    }

    public class AddibleUser
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string FullName { get; set; }
        public int AreaId { get; set; }
    }
}