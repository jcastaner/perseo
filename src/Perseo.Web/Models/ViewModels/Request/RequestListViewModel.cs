﻿using Perseo.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Perseo.Web.Models
{ 
    public class RequestListViewModel
    {
        public ChartViewModel AreaChart1 { get; set; }
        public ChartViewModel AreaChart2 { get; set; }
        public ChartViewModel AreaChart3 { get; set; }
        public ChartViewModel AreaChart4 { get; set; }

        [Display(Name = "Solicitudes")]
        public List<RequestListItemViewModel> Requests { get; set; }

        public List<RequestType> Types { get; set; }

        public List<RequestState> States { get; set; }

        public List<Area> Areas { get; set; }

        public List<int> AreasIds { get; set; }


    }
}
