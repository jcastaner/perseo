using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Perseo.Core;

namespace Perseo.Web.Models
{
    public class RequestViewModel
    {
        [Display(Name = "Id")]
        public int Id { get; set; }

        [Display(Name = "Asunto")]
        public string Summary { get; set; }

        [Display(Name = "Descripci�n")]
        public string Description { get; set; }

        [Display(Name = "Tipo")]
        public RequestType Type { get; set; }

        [Display(Name = "Estado")]
        public RequestState State { get; set; }

        [Display(Name = "Prioridad")]
        public PriorityType Priority { get; set; }

        [Display(Name = "Solicitante")]
        public string UserName { get; set; }

        [Display(Name = "Usuario Asignado")]
        public string AssignedUserName { get; set; }

        [HiddenInput]
        public string AdditionalData { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "Fecha Creaci�n")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime RequestDate { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "Fecha Modificaci�n")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime LastAssignedDate { get; set; }

        [Display(Name = "�ltimo Comentario")]
        public string LastComment { get; set; }

        [Display(Name = "Estado")]
        public List<RequestState> AllowedStates { get; set; }

        public bool IsCreator { get; set; }

        public bool IsAssigned { get; set; }

        public bool IsOriginAreaOwner { get; set; }

        [Required(ErrorMessage = "El campo Comentario es obligatorio.")]
        [Display(Name = "Comentario")]
        public string NewComment { get; set; }

        [Required(ErrorMessage = "El campo Estado es obligatorio.")]
        [Display(Name = "Estado")]
        public RequestState NewState { get; set; }

        public List<RequestHistoryListNode> RequestHistory { get; set; }
    }
}