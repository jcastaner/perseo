﻿using Perseo.Core;
using System;
using System.ComponentModel.DataAnnotations;

namespace Perseo.Web.Models
{ 
    public class RequestListItemViewModel
    {
        [Display(Name = "Id")]
        public int Id { get; set; }

        [Display(Name = "Resumen")]
        public string Summary { get; set; }

        [Display(Name = "Descripción")]
        public string Description { get; set; }

        [Display(Name = "Tipo")]
        public RequestType Type { get; set; }

        [Display(Name = "Estado")]
        public RequestState State { get; set; }

        [Display(Name = "Prioridad")]
        public PriorityType Priority { get; set; }

        [Display(Name = "Colaborador")]
        public string User { get; set; }

        [Display(Name = "Asignado a")]
        public string AssignedUser { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "Fecha Creación")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime CreationDate { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "Ultima Modificaión")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime LastChangeDate { get; set; }

    }
}
