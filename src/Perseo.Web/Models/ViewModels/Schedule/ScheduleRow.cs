using Perseo.Core;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Perseo.Web.Models
{
    public class ScheduleRow
    {
        [HiddenInput]
        public int UserId { get; set; }
        [Display(Name = "Nombre")]
        public string FullName { get; set; }
        public Dictionary<String, ScheduleDate> Columns { get; set; }
    }
}
