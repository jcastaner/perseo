using Perseo.Core;
using System.Collections.Generic;

namespace Perseo.Web.Models
{
    public class ScheduleDashBoardViewModel
    {
        public List<CardData> UpperCards { get; set; }
        public ChartViewModel BarChart1 { get; set; }
        public List<CardData> CardsChart1 { get; set; }
        public ChartViewModel ColumnChart1 { get; set; }

        public Dictionary<JobType, GeneralJobType> GeneralJobTypes { get; set; }

        public ScheduleDashBoardViewModel()
        {
            GeneralJobTypes = new Dictionary<JobType, GeneralJobType>();
        }
    }
}
