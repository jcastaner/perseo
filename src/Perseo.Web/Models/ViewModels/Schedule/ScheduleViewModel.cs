﻿using Perseo.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Perseo.Web.Models
{ 
    public class ScheduleViewModel
    {
       
        [Display(Name = "Año")]
        public int Year { get; set; }

        [Display(Name = "Mes")]
        public int Month { get; set; }

        [Display(Name = "Número de días del mes")]
        public int DaysOfMonth
        {
            get
            {
                return DateTime.DaysInMonth(Year, Month);
            }
        }

        [Display(Name = "Primer día del mes")]
        public int FirstWeekDayOfMonth
        {
            get
            {
                return (int)(new DateTime(Year, Month, 1)).AddDays(-1).DayOfWeek;
            }
        }

        [Display(Name = "Maximo número de días")]
        public int MaxMonthDays
        {
            get
            {
                return DaysOfMonth + FirstWeekDayOfMonth <= 35 ? 35 : 42;
            }
        }

        [Display(Name = "Dias de la semana")]
        public readonly IList<string> WeekDays = new List<string> { "L", "M", "X", "J", "V", "S", "D" };

        [Display(Name = "Meses")]
        public readonly IList<string> Months = new List<string> { "", "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" };

        [Display(Name = "Nombre del mes")]
        public string MonthName
        {
            get
            {
                return Months[Month];
            }
        }

        [Display(Name = "Fecha")]
        public DateTime? Date { get; set; }

        [Display(Name = "Areas")]
        public ICollection<Area> Areas { get; set; }

        [Display(Name = "AreasIds")]
        public ICollection<int> AreasIds { get; set; }

        [Display(Name = "Franjas")]
        public IList<string> TimeSlots { get; set; }

        [Display(Name = "Filas")]
        public ICollection<ScheduleRow> Rows { get; set; }

        [Display(Name = "Areas")]
        public List<AreaModel> OwnedAreas { get; set; }

        [Display(Name = "AreasIds")]
        public List<int> SelectedAreasIds { get; set; }


    }
}
