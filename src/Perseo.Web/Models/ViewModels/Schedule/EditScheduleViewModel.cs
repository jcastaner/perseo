using Perseo.Core;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Perseo.Web.Models
{
    public class EditScheduleDateViewModel
    {

        public List<Job> ProductiveJobs { get; set; }

        public List<Job> GeneralJobs { get; set; }

        public List<ScheduleDateTerm> Terms { get; set; }

        public Dictionary<string, List<int>> UserDates = new Dictionary<string, List<int>>();

        public List<int> DatesIds = new List<int>();

        public User User;

        public int Year { get; set; }

        public int Month { get; set; }

        public int Day { get; set; }

        public string Comment { get; set; }

        public string Ids { get; set; }

        public string Days { get; set; }

        public string Assignment { get; set; }

        public bool Multiple { get; set; }

        public readonly IList<string> Months = new List<string> { "", "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" };

        public string MonthName
        {
            get
            {
                return Months[Month];
            }
        }

    }
}
