using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Perseo.Web.Models
{
    public class AssignAreaOptions
    {
        public int? Parent { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool? Productive { get; set; }
        public int Value { get; set; }
    }
}
