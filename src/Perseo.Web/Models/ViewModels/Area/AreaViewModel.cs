using Microsoft.AspNetCore.Mvc;
using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using Perseo.Core;

namespace Perseo.Web.Models
{
    public class AreaViewModel
    {
        [HiddenInput]
        public int Id { get; set; }

        [Display(Name = "Fecha Inicio")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime StartDate { get; set; }

        [Display(Name = "Fecha Fin")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? EndDate { get; set; }

        [Display(Name = "Nombre")]
        [Required(ErrorMessage = "El campo Nombre es obligatorio.")]
        public string Name { get; set; }

        [Display(Name = "Descripcion")]
        [Required(ErrorMessage = "El campo Descripcion es obligatorio.")]
        public string Description { get; set; }

        [Display(Name = "Tipo")]
        [Required(ErrorMessage = "El campo Tipo es obligatorio.")]
        public AreaType Type { get; set; }

        [Display(Name = "Area Padre")]
        public string Parent { get; set; }

        [Display(Name = "Area Padre")]
        [Required(ErrorMessage = "El campo Area Padre es obligatorio.")]
        public int? ParentId { get; set; }
        
        [Display(Name = "Responsable")]
        public string Owner { get; set; }

        [Display(Name = "Responsable")]
        public int? OwnerId { get; set; }


        [Display(Name = "Estado")]
        public bool? Active { get; set; }


        [Display(Name = "C�digo de Proyecto")]
        public string Code { get; set; }

        [Display(Name = "C�digo de Proyecto")]
        public int? CodeId { get; set; }

        [Display(Name = "EDT")]
        public string Wbs { get; set; }

        [Display(Name = "EDT")]
        public int? WbsId { get; set; }

        [Display(Name = "EDT")]
        public string FullWbs { get; set; }

        [Display(Name = "C�digo/Edt")]
        public string FullCode { get; set; }


        public ICollection<Job> Jobs { get; set; }

        public ICollection<User> Users { get; set; }


        public ICollection<Area> ParentAreas { get; set; }

        public ICollection<User> OwnedUsers { get; set; }

        public ICollection<Area> OwnedAreas { get; set; }

        [Display(Name = "Usuarios")]
        public int UserCount { get; set; }

        public string Lineage { get; set; }
        public int Depth { get; set; }

    }
}
