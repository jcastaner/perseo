using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using Perseo.Core;

namespace Perseo.Web.Models
{
    public class AddAreaViewModel
    {
        [Display(Name = "Nombre")]
        [Required(ErrorMessage = "El campo Nombre es obligatorio.")]
        public string Name { get; set; }

        [Display(Name = "Descripción")]
        [Required(ErrorMessage = "El campo Descripcion es obligatorio.")]
        public string Description { get; set; }

        [Display(Name = "Tipo")]
        [Required(ErrorMessage = "El campo Tipo es obligatorio.")]
        public AreaType Type { get; set; }

        [Display(Name = "Area Padre")]
        [Required(ErrorMessage = "El campo Area Padre es obligatorio.")]
        public int ParentId { get; set; }
        public ICollection<Area> ParentAreas { get; set; }

    }
}
