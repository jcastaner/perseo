using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Perseo.Web.Models
{
    public class AssignAreaViewModel
    {
        public int AreaId { get; set; }
        public string Entity { get; set; }
        
        public List<AssignAreaOptions> Options { get; set; }
        public List<AssignAreaHistory> History { get; set; }

        public int AreaCodId { get; set; }

        public int AssignmentId { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime AssignmentStartDate { get; set; }
        [Display(Name = "Nombre")]
        public string NCode { get; set; }
        [Display(Name = "Descripción")]
        public string DesCode { get; set; }        

    }
}
