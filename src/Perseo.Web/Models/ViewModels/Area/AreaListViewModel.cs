using Perseo.Core;
using System.Collections.Generic;

namespace Perseo.Web.Models
{
    public class AreaListViewModel
    {
        public List<AreaListNodeModel> Data { get; set; }

        public PaginationModel Pagination { get; set; }

        public List<AreaModel> OwnedAreas { get; set; }

        public List<int> SelectedAreasIds { get; set; }

        public List<AreaType> AreaTypes { get; set; }

        public ActiveType? Active { get; set; }

    }
}  