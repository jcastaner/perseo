using System;
using Perseo.Core;

namespace Perseo.Web.Models
{
    public class AreaListNodeModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool Active { get; set; }
        public bool? Dedication { get; set; }
        public bool? Productive { get; set; }
        public int? ParentId { get; set; }
        public int? OwnerId { get; set; }
        public int? Users { get; set; }
        public string Code { get; set; }
        public int? CodeId { get; set; }
        public string Wbs { get; set; }
        public int? WbsId { get; set; }
        public AreaType Type { get; set; }
        public string OwnerFullName { get; set; }
        public string Lineage { get; set; }
        public int Depth { get; set; }
        public string FullCode { get; set; }
        public string OwnerAvatar { get; set; }
    }
}  