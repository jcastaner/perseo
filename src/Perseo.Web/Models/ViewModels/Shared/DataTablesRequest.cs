using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using Perseo.Core;
using System;

namespace Perseo.Web.Models
{
    public class DataTablesRequest
    {
        public int draw { get; set; }
        public int start { get; set; }
        public int length { get; set; }
        public IDictionary<string, string> search { get; set; }
        public IList<IDictionary<string, string>> order { get; set; }

        public int page {
            get
            {
                return (int)(start / length) + 1;
            }
        }
        public int pageSize
        {
            get
            {
                return length;
            }
        }

        private int OrderColumn()
        {
            return Convert.ToInt32(this.order[0]["column"]);
        }
        private string OrderDirection()
        {
            return order[0]["dir"] == "asc" ? " ASC" : " DESC";
        }
        private string OrderField(List<string> ColumnNames)
        {
            var orderColumn = OrderColumn();
            return orderColumn > 0 ? ColumnNames[orderColumn - 1] : null;
        }
        public string Order(List<string> ColumnNames)
        {
            var orderField = OrderField(ColumnNames);
            return orderField != null ? orderField + OrderDirection() : null;
        }
    }
}
