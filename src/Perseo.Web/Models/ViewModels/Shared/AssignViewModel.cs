using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Perseo.Web.Models
{
    public class AssignViewModel
    {
        public int Id { get; set; }
        public string Entity { get; set; }
        public List<AssignOptions> Options { get; set; }
        public List<AssignHistory> History { get; set; }

        public string Assignment { get; set; }
        public int AssignmentId { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime AssignmentStartDate { get; set; }

    }
}
