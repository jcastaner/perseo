using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using Perseo.Core;

namespace Perseo.Web.Models
{
    public class DataTablesResponse
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public ICollection<IDictionary<dynamic,dynamic>> data { get; set; }
        public int error { get; set; }


        public void Add(int id, params dynamic[] args) {
            Dictionary<dynamic, dynamic> target = new Dictionary<dynamic, dynamic>();
            for (int i = 0; i < args.Length; i++)
            {
                target.Add(i + 1, args[i]);
            }
            target.Add("DT_RowData", new { id });
            data.Add(target);
        }

        public void Add(int id, List<dynamic> list)
        {
            Dictionary<dynamic, dynamic> target = new Dictionary<dynamic, dynamic>();
            for (int i = 0; i < list.Count; i++)
            {
                target.Add(i + 1, list[i]);
            }
            target.Add("DT_RowData", new { id });
            data.Add(target);
        }

        public void Add(int id, Dictionary<dynamic, dynamic> dic)
        {
            Dictionary<dynamic, dynamic> target = new Dictionary<dynamic, dynamic>();
            int e = 0;
            foreach (var item in dic)
            {
                target.Add(e + 1, item.Value);
                e++;
            }
            target.Add("DT_RowData", new { id });
            data.Add(target);
        }

        public DataTablesResponse(int draw = 0, int recordsTotal = 0, int recordsFiltered = 0)
        {
            this.draw = draw;
            this.recordsTotal = recordsTotal;
            this.recordsFiltered = recordsFiltered;
            data = new HashSet<IDictionary<dynamic, dynamic>>();
        }
    }
}
