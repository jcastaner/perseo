using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Perseo.Web.Models
{
    public class ChangePasswordViewModel
    {
        [HiddenInput]
        public int Id { get; set; }

        [Required(ErrorMessage = "La contrase�a antigua es obligatoria")]
        [DataType(DataType.Password)]
        [Display(Name = "Anterior Contrase�a")]
        public string OldPassword { get; set; }

        [Required(ErrorMessage = "La contrase�a es obligatoria")]
        [StringLength(255, ErrorMessage = "La contrase�a ha de tener 6 o m�s caracteres", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [NotEqual("OldPassword", ErrorMessage = "Las nueva contrase�a no puede ser igual que la anterior")]
        [Display(Name = "Nueva Contrase�a")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Confirm Password is required")]
        [StringLength(255, ErrorMessage = "La contrase�a ha de tener 6 o m�s caracteres", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Las contrase�as no coinciden")]
        [Display(Name = "Confirmar Contrase�a")]
        public string ConfirmPassword { get; set; }

    }
}