using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Perseo.Web.Models
{
    public class AssignJobSlaViewModel
    {
        [HiddenInput]
        public int Id { get; set; }
        [HiddenInput]
        public int JobId { get; set; }
        public string Entity { get; set; }
        public List<AssignJobOptions> Options { get; set; }
        public List<AssignJobHistory> History { get; set; }

        public int AssignmentId { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "Fecha de Modificación")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime AssignmentStartDate { get; set; }

        [Display(Name = "Descripcion SLA")]
        [Required(ErrorMessage = "El campo Descripcion SLA es obligatorio.")]
        public string Sla_Description { get; set; }
        [Display(Name = "Comparador SLA")]
        [Required(ErrorMessage = "El campo Comparador SLA es obligatorio.")]
        public string Sla_Comparator { get; set; }
        [Display(Name = "Sla")]
        public double? SLA { get; set; }
        [Display(Name = "Tarifa")]
        public double? Rate { get; set; }
        [Display(Name = "Tmo")]
        public double? TMO { get; set; }
        [Display(Name = "Espera")]
        public double? Wait { get; set; }
        [Display(Name = "Cola")]
        public double? Queue { get; set; }

        [Display(Name = "Nombre Completo")]
        public string FullName { get; set; }
    }
}
