using Microsoft.AspNetCore.Mvc;
using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using Perseo.Core;

namespace Perseo.Web.Models
{
    public class JobViewModel
    {
        [HiddenInput]
        public int Id { get; set; }

        [Display(Name = "Nombre")]
        [Required(ErrorMessage = "El campo Nombre es obligatorio.")]
        public string Name { get; set; }
        [Display(Name = "Descripción")]
        [Required(ErrorMessage = "El campo Descripción es obligatorio.")]
        public string Description { get; set; }
        [Display(Name = "Activo")]
        public bool? Active { get; set; }
        [Display(Name = "Fecha Inicio")]
        [DisplayFormat(DataFormatString = "{0:dd'/'MM'/'yyyy}")]
        public DateTime StartDate { get; set; }
        [Display(Name = "Fecha Fin")]
        [DisplayFormat(DataFormatString = "{0:dd'/'MM'/'yyyy}")]
        public DateTime? EndDate { get; set; }
        [Display(Name = "Por Defecto")]
        public bool Default { get; set; }
        [Display(Name = "Por Defecto")]
        public string DefaultYesNo { get; set; }
        [Display(Name = "Predeterminada")]
        public string DefStr { get; set; }
        [Display(Name = "Tipo")]
        public JobType Type { get; set; }
        [Display(Name = "Area")]
        public int AreaId { get; set; }
        [Display(Name = "Area")]
        public string AreaName { get; set; }
        [Display(Name = "Acronimo")]
        public string Acronym { get; set; }
        [Display(Name = "Nombre Completo")]
        public string FullName { get; set; }
        [Display(Name = "Avatar")]
        public string Avatar { get; set; }

        /// <summary>
        ///Job Config fields //
        /// </summary>        
        [Display(Name = "Tipo Facturación")]
        [Required(ErrorMessage = "El campo Tipo Facturación es obligatorio.")]
        public BillingType Billing_type { get; set; }
        [Display(Name = "Tipo Tarea")]
        [Required(ErrorMessage = "El campo Tipo Tarea es obligatorio.")]
        public TaskType Task_type { get; set; }        
        [Display(Name = "Descripcion SLA")]
        public string Sla_Des { get; set; }
        [Display(Name = "Comparador SLA")]
        public char Sla_Comp { get; set; }                
        [Display(Name = "Fecha Incio")]
        public DateTime Conf_SDate { get; set; }
        [Display(Name = "Fecha Fin")]
        public DateTime? Conf_EDate { get; set; }

        [Display(Name = "Codigo")]
        public string NCode { get; set; }
        [Display(Name = "EDT")]
        public string NEdt { get; set; }

        /// <summary>
        ///Job SLA fields //
        /// </summary>       
        [Display(Name = "Fecha Inicio")]
        [DisplayFormat(DataFormatString = "{0:dd'/'MM'/'yyyy}")]
        public DateTime Sla_StartDate { get; set; }
        [Display(Name = "Fecha Fin")]
        [DisplayFormat(DataFormatString = "{0:dd'/'MM'/'yyyy}")]
        public DateTime? Sla_EndDate { get; set; }        
        [Display(Name = "Descripcion SLA")]
        public string Sla_Description { get; set; }
        [Display(Name = "Comparador SLA")]
        public string Sla_Comparator { get; set; }        
        [Display(Name = "Sla")]
        public double? SLA { get; set; }
        [Display(Name = "Tarifa")]
        public double? Rate { get; set; }
        [Display(Name = "Tmo")]
        public double? TMO { get; set; }
        //[Display(Name = "Aht")]
        //public double? AHT { get; set; }
        [Display(Name = "Espera")]
        public double? Wait { get; set; }
        [Display(Name = "Cola")]
        public double? Queue { get; set; }

        /// <summary>
        ///Job kpi fiels //
        /// </summary>
        [Required]
        [Display(Name = "Fecha Incio")]
        public DateTime Kpi_SDate { get; set; }
        [Display(Name = "Fecha Fin")]
        public DateTime? Kpi_EDate { get; set; }
        [Required]
        [Display(Name = "Tipo")]
        public int Kpi_type { get; set; }
        [Display(Name = "Descripcion")]
        public float? KpiDescription { get; set; }
        [Display(Name = "Comparator")]
        public float? KpiComparator { get; set; }
        [Display(Name = "Target")]
        public float? KpiTarget { get; set; }
        [Display(Name = "Moficator")]
        public float? KpiMoficator { get; set; }
        [Required]
        [Display(Name = "Tipo Bonus")]
        public int BonusT { get; set; }
        [Display(Name = "Descripcion Bonus")]
        public string BonusD { get; set; }
        [Display(Name = "Bonus")]
        public float? Bonus { get; set; }
        [Required]
        [Display(Name = "Tipo Malus")]
        public int MalusT { get; set; }
        [Display(Name = "Malus Description")]
        public string MalusD { get; set; }
        [Display(Name = "Malus")]
        public float? Malus { get; set; }

        public ICollection<JobKpi> JobKpi { get; set; }
        public ICollection<JobSla> JobSla { get; set; }

    }
}