﻿using Perseo.Core;
using System;
using System.ComponentModel.DataAnnotations;

namespace Perseo.Web.Models
{ 
    public class ForecastMonthViewModel
    {

        [Display(Name = "Fecha")]
        public DateTime Date { get; set; }

        [Display(Name = "Id de Tarea")]
        public int JobId { get; set; }

        [Display(Name = "Año")]
        public int Year { get; set; }

        [Display(Name = "Mes")]
        public int Month { get; set; }

        [Display(Name = "Tarea")]
        public string Job { get; set; }

        [Display(Name = "Area")]
        public string Area { get; set; }

        [Display(Name = "Eventos")]
        public double Events { get; set; }


    }
}
