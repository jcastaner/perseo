﻿using Perseo.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Perseo.Web.Models
{ 
    public class AddForecastViewModel
    {
        [Display(Name = "Id de Tarea")]
        public int JobId { get; set; }

        [Display(Name = "Año")]
        public int Year { get; set; }

        [Display(Name = "Mes")]
        public int Month { get; set; }

        [Display(Name = "Previsión")]
        public string Forecast { get; set; }

    }
}
