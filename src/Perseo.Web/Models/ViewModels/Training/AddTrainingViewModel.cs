using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace Perseo.Web.Models
{
    public class AddTrainingViewModel
    {
        [Display(Name = "Acción Formativa")]
        [Required(ErrorMessage = "El campo Nombre es obligatorio.")]
        public string Name { get; set; }

        [Display(Name = "Descripción")]
        [Required(ErrorMessage = "El campo Descripción es obligatorio.")]
        public string Description { get; set; }

        [Display(Name = "Área")]
        [Required(ErrorMessage = "El campo Área es obligatorio.")]
        public int AreaId { get; set; }

        public List<AreaModel> OwnedAreas { get; set; }
    }
}
