using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Perseo.Web.Models
{  
    public class ShowTrainingViewModel
    {
        public TrainingModel Training { get; set; }

        [Display(Name = "�rea")]
        public string AreaName { get; set; }

        [Display(Name = "Formadores")]
        public List<UserModel> Trainers { get; set; }
        [Display(Name = "Asistentes")]
        public List<UserModel> Attendants { get; set; }
        [Display(Name = "Modelos de Prueba")]
        public List<TrainingTestModel> Tests { get; set; }
    }
} 