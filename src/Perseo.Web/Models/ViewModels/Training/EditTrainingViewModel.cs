using System.Collections.Generic;

namespace Perseo.Web.Models
{  
    public class EditTrainingViewModel : ShowTrainingViewModel
    {
        public List<AreaModel> OwnedAreas { get; set; }
    }
} 