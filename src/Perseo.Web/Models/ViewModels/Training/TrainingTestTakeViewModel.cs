using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;

namespace Perseo.Web.Models
{
    public class TrainingTestTakeViewModel : TrainingTestModel
    {
        [Display(Name = "Acci�n Formativa")]
        public string TrainingName { get; set; }

        [HiddenInput]
        public int TakeId { get; set; }

        [HiddenInput]
        public int TestUserId { get; set; }

        public int Attempt { get; set; }

        [Display(Name = "Superado")]
        public bool? Pass { get; set; }

        public ICollection<TrainingTestTakeSectionViewModel> Sections { get; set; }

        public TrainingTestTakeViewModel()
        {
            Sections = new HashSet<TrainingTestTakeSectionViewModel>();
        }
    }

    public class TrainingTestTakeSectionViewModel : TrainingTestSectionModel
    {
        public new int? Id { get; set; }
        public int? TestSectionId { get; set; }
        public ICollection<TrainingTestTakeSectionFieldViewModel> Fields { get; set; }

        public TrainingTestTakeSectionViewModel()
        {
            Fields = new HashSet<TrainingTestTakeSectionFieldViewModel>();
        }
    }

    public class TrainingTestTakeSectionFieldViewModel : TrainingTestSectionFieldModel
    {
        public new int? Id { get; set; }
        public int? TestFieldId { get; set; }
        public new int Type { get; set; }

        [Display(Name = "Puntuaci�n")]
        public double Mark { get; set; }

        [Display(Name = "Respuesta")]
        public string Answer { get; set; }

        public ICollection<TrainingTestTakeSectionFieldOptionViewModel> Options { get; set; }

        public TrainingTestTakeSectionFieldViewModel()
        {
            Options = new HashSet<TrainingTestTakeSectionFieldOptionViewModel>();
        }
    }

    public class TrainingTestTakeSectionFieldOptionViewModel: TrainingTestSectionFieldOptionModel
    {
        public new int? Id { get; set; }
        public int? TestOptionId { get; set; }
        public bool? Checked { get; set; }
    }
}