using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;

namespace Perseo.Web.Models
{
    public class AddTrainingUserViewModel
    {
        [HiddenInput]
        public int TrainingId { get; set; }
        [HiddenInput]
        public string UserType { get; set; }
        [HiddenInput]
        public string UsersToAddIds { get; set; }

        public List<UserModel> AddibleUsers { get; set; }
    }
}
