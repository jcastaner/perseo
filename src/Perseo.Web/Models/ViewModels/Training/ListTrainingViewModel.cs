using System.Collections.Generic;

namespace Perseo.Web.Models
{
    public class ListTrainingViewModel
    {
        public ChartViewModel AreaChart1 { get; set; }
        public ChartViewModel AreaChart2 { get; set; }
        public ChartViewModel AreaChart3 { get; set; }
        public ChartViewModel AreaChart4 { get; set; }

        public List<TrainingModel> Data { get; set; }

        public PaginationModel Pagination { get; set; }

        public List<AreaModel> OwnedAreas { get; set; }

        public List<int> SelectedAreasIds { get; set; }

        public int Month { get; set; }
        public int Year { get; set; }
    }
}  