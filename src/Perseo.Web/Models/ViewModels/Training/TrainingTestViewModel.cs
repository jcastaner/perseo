using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Perseo.Web.Models
{
    public class TrainingTestViewModel : TrainingTestModel
    {
        [Display(Name = "Acción Formativa")]
        public string TrainingName { get; set; }

        public ICollection<TrainingTestSectionViewModel> Sections { get; set; }

        public TrainingTestViewModel()
        {
            Sections = new HashSet<TrainingTestSectionViewModel>();
        }
    }

    public class TrainingTestSectionViewModel : TrainingTestSectionModel
    {
        public new int? Id { get; set; }
        public ICollection<TrainingTestSectionFieldViewModel> Fields { get; set; }

        public TrainingTestSectionViewModel()
        {
            Fields = new HashSet<TrainingTestSectionFieldViewModel>();
        }
    }

    public class TrainingTestSectionFieldViewModel : TrainingTestSectionFieldModel
    {
        public new int? Id { get; set; }
        public new int Type { get; set; }
        public ICollection<TrainingTestSectionFieldOptionViewModel> Options { get; set; }

        public TrainingTestSectionFieldViewModel()
        {
            Options = new HashSet<TrainingTestSectionFieldOptionViewModel>();
        }
    }

    public class TrainingTestSectionFieldOptionViewModel : TrainingTestSectionFieldOptionModel
    {
        public new int? Id { get; set; }
    }
}