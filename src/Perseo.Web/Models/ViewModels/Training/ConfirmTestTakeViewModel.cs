using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;

namespace Perseo.Web.Models
{
    public class ConfirmTestTakeViewModel
    {
        [HiddenInput]
        public int TrainingId { get; set; }
        [HiddenInput]
        public int TestId { get; set; }


        [Display(Name = "Prueba")]
        public string TestName { get; set; }

        [Display(Name = "Intentos Máximos")]
        public int TestAttempts { get; set; }

        [Display(Name = "Tiempo Disponible")]
        public int? TestTime { get; set; }

        [Display(Name = "Nota de Corte")]
        public double TestPassMark { get; set; }

        [Display(Name = "Intento")]
        public int Attempt { get; set; }

        [HiddenInput]
        public bool AttemptsExceeded { get; set; }

    }
}
