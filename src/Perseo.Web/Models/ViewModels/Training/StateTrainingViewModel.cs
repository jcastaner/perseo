using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Perseo.Web.Models
{  
    public class StateTrainingViewModel
    {
        public TrainingModel Training { get; set; }

        [Display(Name = "�rea")]
        public string AreaName { get; set; }

        public List<StateTestTakeViewModel> Takes { get; set; }
        public List<StateTestPendingViewModel> Pending { get; set; }
    }

    public class StateTestTakeViewModel
    {
        public int Id { get; set; }

        public TrainingTestModel Test { get; set; }

        public bool Active { get; set; }

        public UserModel User { get; set; }

        public int Attempt { get; set; }

        public bool? Pass { get; set; }
    }

    public class StateTestPendingViewModel
    {
        public UserModel User { get; set; }
        public TrainingTestModel Test { get; set; }
    }
} 