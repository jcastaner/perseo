using Perseo.Core;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Perseo.Web.Models
{
    public class AssignContractViewModel
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string Entity { get; set; }
        public List<AssignOptions> Options { get; set; }
        public List<AssignHistory> History { get; set; }

        public int AssignmentId { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime AssignmentStartDate { get; set; }

        public List<Company> CompanyList { get; set; }
        [Display(Name = "Empresa")]
        public int CompanyId { get; set; }
        [Display(Name = "Externo")]
        public bool External { get; set; }
        [Display(Name = "Duraci�n")]
        public ContractPeriodType ContractPeriod { get; set; }
        [Display(Name = "Trabaja festivos locales")]
        public bool? WorkLocalHolydays { get; set; }
        [Display(Name = "Trabaja festivos nacaionales")]
        public bool? WorkNationalHolidays { get; set; }
        [Display(Name = "Trabaja fines de semana")]
        public bool? WorkWeekends { get; set; }

    }
}
