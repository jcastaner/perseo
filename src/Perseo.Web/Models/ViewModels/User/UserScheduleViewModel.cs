using Perseo.Core;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace Perseo.Web.Models
{
    public class UserScheduleViewModel
    {
        [HiddenInput]
        public List<ScheduleDate> ScheduleList { get; set; }
    }
}
