using Perseo.Core;
using System.Collections.Generic;

namespace Perseo.Web.Models
{
    public class UserListViewModel
    {
        public List<CardData> Cards { get; set; }

        public List<UserListNodeModel> Data { get; set; }

        public PaginationModel Pagination { get; set; }

        public List<AreaModel> OwnedAreas { get; set; }

        public List<int> SelectedAreasIds { get; set; }
		
        public ActiveType? Active { get; set; }
    }
}  