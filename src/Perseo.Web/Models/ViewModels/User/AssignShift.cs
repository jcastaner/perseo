using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Perseo.Web.Models
{
    public class AssignShift
    {
        public int UserId { get; set; }
        public string Entity { get; set; }
        public List<AssignOptions> Options { get; set; }
        public List<AssignHistory> History { get; set; }

        public int AssignmentId { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime AssignmentStartDate { get; set; }

        [Display(Name = "Descripción")]
        public string Description { get; set; }
        [Display(Name = "Patrones")]
        public Dictionary<int,string> Patterns { get; set; }
        [Display(Name = "Generar")]
        public bool Generate { get; set; }


        public AssignShift()
        {
            Options = new List<AssignOptions>();
            History = new List<AssignHistory>();
            Patterns = new Dictionary<int, string>();
        }

    }
}
