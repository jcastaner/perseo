using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Perseo.Web.Models
{
    public class AssignSalary
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string Entity { get; set; }
        public List<AssignHistory> History { get; set; }

        public int AssignmentId { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime AssignmentStartDate { get; set; }

        [Display(Name = "Salario Anual")]
        public float? Salary { get; set; }
        [Display(Name = "Variable Anual")]
        public float? Variable { get; set; }
        [Display(Name = "Moneda")]
        public string Currency { get; set; }
    }
}
