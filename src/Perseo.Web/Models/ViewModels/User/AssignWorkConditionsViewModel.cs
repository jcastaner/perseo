using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Perseo.Web.Models
{
    public class AssignWorkConditionsViewModel
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string Entity { get; set; }
        public List<AssignOptions> Options { get; set; }
        public List<AssignHistory> History { get; set; }

        public int AssignmentId { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime AssignmentStartDate { get; set; }

        [Display(Name = "Empresa")]
        public string Company { get; set; }
        [Display(Name = "Jornada Semanal")]
        public double? WeekWorkHours { get; set; }
        [Display(Name = "Horas Anuales (Jor.Comp.)")]
        public double? YearWorkHours { get; set; }
        [Display(Name = "Horas M�dico")]
        public double? YearDoctorHours { get; set; }
        [Display(Name = "D�as de Vacaciones")]
        public double? YearVacationDays { get; set; }
        [Display(Name = "D�as de Libre Disposici�n")]
        public double? YearSpareDays { get; set; }

    }
}
