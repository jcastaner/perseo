using System.Collections.Generic;

namespace Perseo.Web.Models
{
    public class TrainingViewModel
    {
        public ICollection<TrainingModel> UserTrainings { get; set; }
        public ICollection<UserTestPendingViewModel> TestPending { get; set; }
        public ICollection<UserTestTakeViewModel> TestTakes { get; set; }

        public TrainingViewModel() : base()
        {
            UserTrainings = new HashSet<TrainingModel>();
            TestPending = new HashSet<UserTestPendingViewModel>();
            TestTakes = new HashSet<UserTestTakeViewModel>();
        }
    }

    public class UserTestPendingViewModel
    {
        public TrainingModel Training { get; set; }
        public TrainingTestModel Test { get; set; }
        public int DoneAttempts { get; set; }
    }

    public class UserTestTakeViewModel
    {
        public TrainingModel Training { get; set; }
        public TrainingTestModel Test { get; set; }
        public TrainingTestUserTakeModel Take { get; set; }
    }

}
