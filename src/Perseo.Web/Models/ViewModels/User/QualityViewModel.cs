using System.Collections.Generic;

namespace Perseo.Web.Models
{
    public class QualityViewModel
    {
        public ICollection<UserExamPendingViewModel> ExamPending { get; set; }
        public ICollection<UserAuditTakeViewModel> AuditTakes { get; set; }
        public ICollection<UserExamTakeViewModel> ExamTakes { get; set; }

        public QualityViewModel() : base()
        {
            ExamPending = new HashSet<UserExamPendingViewModel>();
            AuditTakes = new HashSet<UserAuditTakeViewModel>();
            ExamTakes = new HashSet<UserExamTakeViewModel>();
        }
    }

    public class UserExamPendingViewModel
    {
        public QualityModel Quality { get; set; }
        public QualityExamModel Exam { get; set; }
        public int DoneAttempts { get; set; }
    }

    public class UserAuditTakeViewModel
    {
        public QualityModel Quality { get; set; }
        public QualityAuditModel Audit { get; set; }
        public UserModel AuditOwner { get; set; }
        public QualityAuditUserTakeModel Take { get; set; }
    }

    public class UserExamTakeViewModel
    {
        public QualityModel Quality { get; set; }
        public QualityExamModel Exam { get; set; }
        public QualityExamUserTakeModel Take { get; set; }
    }
}
