using System;
using Perseo.Core;

namespace Perseo.Web.Models
{
    public class UserListNodeModel
    {

        public int Id { get; set; }

        public string Avatar { get; set; }

        public string FullFamilyName { get; set; }

        public string Email { get; set; }

        public string UserName { get; set; }

        public int? EmployeeId { get; set; }
		
        public string Area { get; set; }

    }
}  