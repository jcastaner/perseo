using Perseo.Core;
using System.Collections.Generic;

namespace Perseo.Web.Models
{
    public class UserDashBoardViewModel
    {
        public List<CardData> Cards { get; set; }

        public Dictionary<JobType, GeneralJobType> GeneralJobTypes { get; set; }

        public UserDashBoardViewModel()
        {
            GeneralJobTypes = new Dictionary<JobType, GeneralJobType>();
        }
    }

    public class GeneralJobType
    {
        public string Acronym { get; set; }
        public List<double> Hours { get; set; }
        public List<double> Days { get; set; }
        public List<GeneralJob> GeneralJobs { get; set; }

        public GeneralJobType()
        {
            GeneralJobs = new List<GeneralJob>();
        }
    }

    public class GeneralJob
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<double> Hours { get; set; }
        public List<double> Days { get; set; }
    }

}
