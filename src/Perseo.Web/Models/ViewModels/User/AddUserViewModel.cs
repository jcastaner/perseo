using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Perseo.Web.Models
{
    public class AddUserViewModel
    {
        [Required(ErrorMessage = "El campo Nombre es obligatorio.")]
        [Display(Name = "Nombre")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "El campo Apellido es obligatorio.")]
        [Display(Name = "Apellido")]
        public string MainSurname { get; set; }

        [Display(Name = "Seg. apellido")]
        public string SecondSurname { get; set; }

        [Required(ErrorMessage = "El campo Usuario es obligatorio.")]
        [Remote(action: "VerifyUserName", controller: "User")]
        [Display(Name = "Usuario")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "La contrase�a es obligatoria")]
        [StringLength(255, ErrorMessage = "La contrase�a ha de tener 6 o m�s caracteres", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Contrase�a")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Confirm Password is required")]
        [StringLength(255, ErrorMessage = "La contrase�a ha de tener 6 o m�s caracteres", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Las contrase�as no coinciden")]
        [Display(Name = "Confirmar")]
        public string ConfirmPassword { get; set; }

        [Required(ErrorMessage = "El campo Email es obligatorio.")]
        [Remote(action: "VerifyEmail", controller: "User")]
        [EmailAddress(ErrorMessage = "La direcci�n introducida no es v�lida.")]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "El campo Area es obligatorio.")]
        [Display(Name = "Area")]
        public int AreaId { get; set; }

        [Required(ErrorMessage = "El campo Fecha de Asignaci�n al Area es obligatorio.")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Fecha de Alta")]
        public DateTime AreaStartDate { get; set; }

        public List<AssignOptions> Options { get; set; }
    }
}