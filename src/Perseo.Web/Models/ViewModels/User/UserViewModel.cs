using Perseo.Core;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Perseo.Web.Models
{
    public class UserViewModel : UserModel
    {
        [Display(Name = "Nombre Completo")]
        public string FullName { get; set; }

        [Display(Name = "Apellidos, Nombre")]
        public string FullFamilyName { get; set; }

        [Display(Name = "Contrase�a")]
        public string Password { get; set; }

        [Display(Name = "Area o Servicio")]
        public string Area { get; set; }

        [Display(Name = "Areas a cargo")]
        public string OwnedAreas { get; set; }

        [Display(Name = "Oficina")]
        public string Office { get; set; }

        [Display(Name = "Cargo")]
        public string Position { get; set; }

        [Display(Name = "Rol")]
        public string Role { get; set; }

        [Display(Name = "Turno")]
        public string Shift { get; set; }

        [Display(Name = "Turnos")]
        public IEnumerable<Shift> Shifts { get; set; }

        [Display(Name = "Contrato")]
        public ContractPeriodType? Contract { get; set; }

        [Display(Name = "Empresa")]
        public bool External { get; set; }

        [Display(Name = "Empresa")]
        public string Company { get; set; }

        [Display(Name = "Jornada")]
        public double? WeekWorkHours { get; set; }

        [Display(Name = "Horas Anuales")]
        public double? YearWorkHours { get; set; }

        [Display(Name = "Horas M�dico")]
        public double? YearDoctorHours { get; set; }

        [Display(Name = "Vacaciones")]
        public double? YearVacationDays { get; set; }

        [Display(Name = "Libre disposici�n")]
        public double? YearSpareDays { get; set; }

        [Display(Name = "Convenio")]
        public string Collective { get; set; }

        [Display(Name = "Categoria Profesional")]
        public string CollectiveCategory { get; set; }

        [Display(Name = "Categoria Interna")]
        public string Category { get; set; }

        [Display(Name = "Salario")]
        public string Salary { get; set; }

        [Display(Name = "Tasa")]
        public string Rate { get; set; }

        [Display(Name = "Roles")]
        public virtual IEnumerable<RoleModel> Roles { get; set; }

        [Display(Name = "Roles")]
        public IEnumerable<UserRoleModel> UserRoles { get; set; }

        [Display(Name = "Operacional")]
        public IEnumerable<UserOperationalModel> UserOperational { get; set; }

        [Display(Name = "Roles")]
        public List<string> RoleList { get; set; }

        [Display(Name = "Roles Disponibles")]
        public Dictionary<string, string> AvailableRoles { get; set; }

        [Display(Name = "Roles Asignables")]
        public List<string> AssignableRoles { get; set; }

        [Display(Name = "Perfiles Disponibles")]
        public Dictionary<string, string> AvailableProfiles { get; set; }

        [Display(Name = "Roles por Perfil")]
        public Dictionary<string, Dictionary<string, string>> ProfilesRoles { get; set; }


        public UserViewModel()
        {
         //   Roles = new HashSet<RoleModel>();
           // Shifts = new HashSet<Shift>();
        }

    }
}
