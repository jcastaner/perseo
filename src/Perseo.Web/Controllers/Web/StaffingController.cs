﻿using AutoMapper;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Newtonsoft.Json;
using Perseo.Core;
using Perseo.Web.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Perseo.Web.Controllers
{
    [Route("[controller]/[action]/{id:int?}")]
    //[Authorize(Policy = AppPolicy.Operational)]
    public class StaffingController : BaseController
    {
        IStaffingService StaffingService;

        public StaffingController(
            IHttpContextAccessor contextAccessor,
            IUserService userService,
            IHostingEnvironment enviroment,
            IStringLocalizer<SharedResources> localizer,
            IStaffingService staffingService,
            IMapper mapper) : base(contextAccessor, enviroment, userService, localizer, mapper)
        {
            StaffingService = staffingService;
        }

        [HttpGet]
        //[Authorize(Policy = "Staff")]
        [Route("/[controller]")]
        public IActionResult Index()
        {
            return Dashboard();
        }

        [HttpGet]
        //[Authorize(Policy = "Staff")]
        [Route("/[controller]/[action]/{year:int?}/{month:int?}/{selectedjobs?}/{availability?}")]
        public IActionResult Dashboard(int? year = null, int? month = null, string selectedjobs = null, string availability = "")
        {
            year = year ?? DateTime.Now.Year;
            month = month ?? DateTime.Now.Month;
            bool full = (availability.Contains("full")) ? true : false;

            List<Area> authUserOwnedAreas = AuthUser.OwnedAreas.ToList();
            List<Job> authUserOwnedJobs = AuthUser.OwnedJobs.ToList();
            List<int> jobFilterIds = JobFilter(authUserOwnedJobs, selectedjobs);
            List<int> areasIdsToShow = selectedjobs == null ? authUserOwnedAreas.Select(a => a.Id).ToList() : authUserOwnedJobs.Where(y => jobFilterIds.Contains(y.Id)).Select(x => x.AreaId).ToList();

            List<JobConfig> JobsToShowConfig = StaffingService.GetJobsConfiguration(jobFilterIds, year.Value, month.Value).ToList();

            ViewBag.Tab = "dashboard";
            ViewBag.Year = year.Value;
            ViewBag.Month = month.Value;
            ViewBag.SelectedAvailability = availability;
            ViewBag.MyOwnedJobs = Mapper.Map<List<JobModel>>(authUserOwnedJobs);
            ViewBag.MyOwnedAreas = Mapper.Map<List<AreaModel>>(authUserOwnedAreas);
            ViewBag.JobsIdsToShow = jobFilterIds;
            ViewBag.AreasIdsToShow = areasIdsToShow;
            ViewBag.JobsToShowConfig = JobsToShowConfig;
            ViewBag.Timeslots = StaffingService.GetTimeSlots();

            #region CHARTS

            IDictionary<int, List<double>> Agents = StaffingService.GetActualAgents(jobFilterIds, year.Value, month.Value);
            IDictionary<int, List<double>> Required = StaffingService.GetRequiredAgents(jobFilterIds, year.Value, month.Value);

            if(Agents.Count() + Required.Count() == 0)
            {
                return View("DashboardNoData");
            }

            IDictionary<int, Dictionary<int, List<double>>> CapacityByJob = StaffingService.GetCapacityByJob(jobFilterIds, year.Value, month.Value, full);

            IDictionary<int, List<double>> Capacity = new Dictionary<int, List<double>>();
            foreach (int jobid in jobFilterIds)
            {
                IDictionary<int, List<double>> JobCapacity = CapacityByJob[jobid];
                for (int daynum = 1; daynum <= DateTime.DaysInMonth(year.Value, month.Value); daynum++)
                {
                    if(JobCapacity.ContainsKey(daynum))
                    {
                        if (!Capacity.ContainsKey(daynum)) { Capacity[daynum] = Enumerable.Repeat(0.0, 48).ToList(); }
                        for (int i = 0; i < 48; i++) { Capacity[daynum][i] += JobCapacity[daynum][i]; }
                    }
                }
            }

            double TodayMaxOverstaffing = 0;
            double TodayMinUndertaffing = 0;
            if (Capacity.ContainsKey(DateTime.Now.Day) && Agents.ContainsKey(DateTime.Now.Day))
            {
                TodayMaxOverstaffing = Capacity[DateTime.Now.Day].Max() / Agents[DateTime.Now.Day].Max() * 100;
                TodayMinUndertaffing = Capacity[DateTime.Now.Day].Min() / Agents[DateTime.Now.Day].Max() * 100;
            }
            ViewBag.MaxOverstaffing = TodayMaxOverstaffing > 0 ? TodayMaxOverstaffing : 0;
            ViewBag.MinUndertaffing = TodayMinUndertaffing < 0 ? TodayMinUndertaffing : 0;

                #region GAUGE

            double TodayCapacity = 0;
            double TodayDisponibility = 0;        
            if (Capacity.ContainsKey(DateTime.Now.Day))
            {
                TodayCapacity = Math.Round(((double)Capacity[DateTime.Now.Day].Where(v => v >= 0).Count() / 48) * 100, 2);
                TodayDisponibility = Math.Round(((double)Capacity[DateTime.Now.Day].Where(v => v > 0).Count() / 48) * 100, 2);
            }

            GaugeChartViewModel Gauge1 = new GaugeChartViewModel()
            {
                Name = Localizer["Capacidad Hoy"],
                Serie = new GaugeChartViewModel.GaugeSerie() { Name = Localizer["Capacidad"], Data = new List<double>() { TodayCapacity } }
            };
            ViewBag.Gauge1 = Gauge1;
            
            GaugeChartViewModel Gauge2 = new GaugeChartViewModel()
            {
                Name = Localizer["Disponibilidad Hoy"],
                Serie = new GaugeChartViewModel.GaugeSerie() { Name = Localizer["Disponibilidad"], Data = new List<double>() { TodayDisponibility } }
            };
            ViewBag.Gauge2 = Gauge2;

            double MonthCapacity = Capacity.Count() > 0 ? Math.Round(((double)Capacity.SelectMany(p => p.Value.Where(v => v >= 0)).Count() / (Capacity.Count() * 48)) * 100, 2) : 0;
            double MonthDisponibility = Capacity.Count() > 0 ? Math.Round(((double)Capacity.SelectMany(p => p.Value.Where(v => v > 0)).Count() / (Capacity.Count() * 48)) * 100, 2): 0;

            GaugeChartViewModel Gauge3 = new GaugeChartViewModel()
            {
                Name = Localizer["Capacidad Mes"],
                Serie = new GaugeChartViewModel.GaugeSerie() { Name = Localizer["Capacidad"], Data = new List<double>() { MonthCapacity } }
            };
            ViewBag.Gauge3 = Gauge3;

            GaugeChartViewModel Gauge4 = new GaugeChartViewModel()
            {
                Name = Localizer["Disponibilidad Mes"],
                Serie = new GaugeChartViewModel.GaugeSerie() { Name = Localizer["Disponibilidad"], Data = new List<double>() { MonthDisponibility } }
            };
            ViewBag.Gauge4 = Gauge4;

                #endregion
                #region COLUMNS AND LINE
            
            List<string> labels = new List<string>();
            for (int daynum = 1; daynum <= DateTime.DaysInMonth(year.Value, month.Value); daynum++) { labels.Add(daynum.ToString()); }

            ChartViewModel ColumnLine1 = new ChartViewModel()
            {
                Name = Localizer["Requerido vs Disponible por Día"],
                Series = new List<ChartViewModel.Serie>() {
                    new ChartViewModel.Serie() { Name = Localizer["Disponible"], Data = new List<double>() },
                    new ChartViewModel.Serie() { Name = Localizer["Requerido"], Data = new List<double>() }
                },
                Labels = labels
            };

            for (int daynum = 1; daynum <= DateTime.DaysInMonth(year.Value, month.Value); daynum++)
            {
                ColumnLine1.Series[0].Data.Add(Math.Round(Agents.ContainsKey(daynum) ? Agents[daynum].Sum() : 0, 2));
                ColumnLine1.Series[1].Data.Add(Math.Round(Required.ContainsKey(daynum) ? - Required[daynum].Sum() : 0, 2));
            }

            ViewBag.ColumnLine1 = ColumnLine1;

            labels = new List<string> { "00:00", "00:30", "01:00", "01:30", "02:00", "02:30", "03:00", "03:30", "04:00", "04:30", "05:00", "05:30", "06:00", "06:30", "07:00", "07:30", "08:00", "08:30", "09:00", "09:30", "10:00", "10:30", "11:00", "11:30", "12:00", "12:30", "13:00", "13:30", "14:00", "14:30", "15:00", "15:30", "16:00", "16:30", "17:00", "17:30", "18:00", "18:30", "19:00", "19:30", "20:00", "20:30", "21:00", "21:30", "22:00", "22:30", "23:00", "23:30" };

            ChartViewModel ColumnLine2 = new ChartViewModel()
            {
                Name = Localizer["Requerido vs Disponible por Franja"],
                Series = new List<ChartViewModel.Serie>() {
                    new ChartViewModel.Serie() { Name = Localizer["Disponible"], Data = new List<double>() },
                    new ChartViewModel.Serie() { Name = Localizer["Requerido"], Data = new List<double>() }
                },
                Labels = labels
            };

            for (int index = 0; index < 48; index++)
            {
                ColumnLine2.Series[0].Data.Add(Math.Round(Agents.Select(p => p.Value.ElementAt(index)).Sum(), 2));
                ColumnLine2.Series[1].Data.Add(Math.Round(- Required.Select(p => p.Value.ElementAt(index)).Sum(), 2));
            }

            ViewBag.ColumnLine2 = ColumnLine2;

                #endregion
                #region COLUMNS
            
            ChartViewModel ColumnLine3 = new ChartViewModel()
            {
                ComplexSeries = new List<ChartViewModel.ComplexSerie>() {
                    new ChartViewModel.ComplexSerie() { Name = Localizer["Disponibilidad"], Data = new List<HighCharts.Data>() }
                },
                DrillDowns = new List<ChartViewModel.ComplexSerie>() { }
            };

            if (availability.Contains("shared"))
            {
                ColumnLine3.Name = Localizer["Dimensionamiento"];

                for (int daynum = 1; daynum <= DateTime.DaysInMonth(year.Value, month.Value); daynum++)
                {
                    ColumnLine3.ComplexSeries[0].Data.Add(new HighCharts.Data
                    {
                        Name = daynum.ToString(),
                        Y = (Math.Round(Capacity.ContainsKey(daynum) ? Capacity[daynum].Sum() : 0, 2)),
                        Drilldown = daynum.ToString()
                    });

                    List<HighCharts.Data> DrillDownData = new List<HighCharts.Data>();
                    for (int index = 0; index < 48; index++)
                    {
                        DrillDownData.Add(new HighCharts.Data
                        {
                            Name = labels[index],
                            Y = (Math.Round(Capacity.ContainsKey(daynum) ? Capacity[daynum].ElementAt(index) : 0, 2))
                        });
                    }

                    ColumnLine3.DrillDowns.Add(new ChartViewModel.ComplexSerie() { Name = daynum.ToString(), Data = DrillDownData });
                }
            }
            else
            {
                ColumnLine3.Name = Localizer["Dimensionamiento por Tarea"];

                foreach (int jobid in jobFilterIds)
                {
                    ChartViewModel.ComplexSerie JobSerie = new ChartViewModel.ComplexSerie()
                    {
                        Name = authUserOwnedJobs.Single(j => j.Id == jobid).Name,
                        Data = new List<HighCharts.Data>()
                    };
                    IDictionary<int, List<double>> JobCapacity = CapacityByJob[jobid];

                    for (int daynum = 1; daynum <= DateTime.DaysInMonth(year.Value, month.Value); daynum++)
                    {
                        JobSerie.Data.Add(new HighCharts.Data
                        {
                            Name = daynum.ToString(),
                            Y = (Math.Round(JobCapacity.ContainsKey(daynum) ? JobCapacity[daynum].Sum() : 0, 2)),
                            Drilldown = jobid.ToString() + "|" +  daynum.ToString()
                        });

                        List<HighCharts.Data> DrillDownData = new List<HighCharts.Data>();
                        for (int index = 0; index < 48; index++)
                        {
                            DrillDownData.Add(new HighCharts.Data
                            {
                                Name = labels[index],
                                Y = (Math.Round(JobCapacity.ContainsKey(daynum) ? JobCapacity[daynum].ElementAt(index) : 0, 2))
                            });
                        }

                        ColumnLine3.DrillDowns.Add(new ChartViewModel.ComplexSerie() { Name = jobid.ToString() + "|" + daynum.ToString(), Data = DrillDownData });
                    }
                    ColumnLine3.ComplexSeries.Add(JobSerie);
                }
            }

            ViewBag.ColumnLine3 = ColumnLine3;

                #endregion
            #endregion

            return View("Dashboard");
        }

        [HttpGet]
        //[Authorize(Policy = "Staff")]
        [Route("/[controller]/[action]/{year:int?}/{month:int?}/{selectedjobs?}/{availability?}")]
        public IActionResult Capacity(int? year = null, int? month = null, string selectedjobs = null, string availability = "")
        {
            return Calculate("capacity", year, month, selectedjobs, availability);
        }

        [HttpGet]
        //[Authorize(Policy = "Staff")]
        [Route("/[controller]/[action]/{year:int?}/{month:int?}/{selectedjobs?}/{availability?}")]
        public IActionResult Events(int? year = null, int? month = null, string selectedjobs = null, string availability = "")
        {
            return Calculate("events", year, month, selectedjobs, availability);
        }

        [HttpGet]
        //[Authorize(Policy = "Staff")]
        [Route("/[controller]/[action]/{year:int?}/{month:int?}/{selectedjobs?}/{availability?}")]
        public IActionResult Required(int? year = null, int? month = null, string selectedjobs = null, string availability = "")
        {
            return Calculate("required", year, month, selectedjobs, availability);
        }

        [HttpGet]
        //[Authorize(Policy = "Staff")]
        [Route("/[controller]/[action]/{year:int?}/{month:int?}/{selectedjobs?}/{availability?}")]
        public IActionResult Staffed(int? year = null, int? month = null, string selectedjobs = null, string availability = "")
        {
            return Calculate("staffed", year, month, selectedjobs, availability);
        }

        [HttpGet]
        //[Authorize(Policy = "Staff")]
        [Route("/[controller]/[action]/{year:int?}/{month:int?}/{selectedjobs?}/{availability?}")]
        public IActionResult Available(int? year = null, int? month = null, string selectedjobs = null, string availability = "")
        {
            return Calculate("available", year, month, selectedjobs, availability);
        }

        private IActionResult Calculate(string tab, int? year = null, int? month = null, string selectedjobs = null, string availability = "")
        {
            year = year ?? DateTime.Now.Year;
            month = month ?? DateTime.Now.Month;
            bool full = (availability.Contains("full")) ? true : false;

            List<Area> authUserOwnedAreas = AuthUser.OwnedAreas.ToList();
            List<Job> authUserOwnedJobs = AuthUser.OwnedJobs.ToList();
            List<int> jobFilterIds = JobFilter(authUserOwnedJobs, selectedjobs);
            List<int> areasIdsToShow = selectedjobs == null ? authUserOwnedAreas.Select(a => a.Id).ToList() : authUserOwnedJobs.Where(y => jobFilterIds.Contains(y.Id)).Select(x => x.AreaId).ToList();

            List<JobConfig> JobsToShowConfig = StaffingService.GetJobsConfiguration(jobFilterIds, year.Value, month.Value).ToList();

            IDictionary<int, List<double>> result = new Dictionary<int, List<double>>();
            if (jobFilterIds.Count > 0)
            {
                switch (tab)
                {
                    case "capacity":
                        result = StaffingService.GetCapacity(jobFilterIds, year.Value, month.Value, full);
                        break;
                    case "events":
                        result = StaffingService.GetRequiredEvents(jobFilterIds, year.Value, month.Value);
                        break;
                    case "required":
                        result = StaffingService.GetRequiredAgents(jobFilterIds, year.Value, month.Value);
                        foreach (var pair in result) { for (int index = 0; index < 48; index++) { pair.Value[index] = - pair.Value[index]; } }
                        break;
                    case "staffed":
                        result = StaffingService.GetActualAgents(jobFilterIds, year.Value, month.Value);
                        break;
                    case "available":
                        result = StaffingService.GetAvailableAgents(jobFilterIds, year.Value, month.Value);
                        break;
                    default:
                        result = StaffingService.GetCapacity(jobFilterIds, year.Value, month.Value, full);
                        break;
                }
            }

            var (start, end) = StaffingService.GetRealTimeSlots(result);
            ViewBag.StartSlot = start;
            ViewBag.EndSlot = end;
            ViewBag.Tab = tab;
            ViewBag.Year = year.Value;
            ViewBag.Month = month.Value;
            ViewBag.SelectedAvailability = availability;
            ViewBag.MyOwnedJobs = Mapper.Map<List<JobModel>>(authUserOwnedJobs);
            ViewBag.MyOwnedAreas = Mapper.Map<List<AreaModel>>(authUserOwnedAreas);
            ViewBag.JobsIdsToShow = jobFilterIds;
            ViewBag.AreasIdsToShow = areasIdsToShow;
            ViewBag.JobsToShowConfig = JobsToShowConfig;
            ViewBag.Timeslots = StaffingService.GetTimeSlots();
            ViewBag.Result = result;

            return View("Staffing");
        }

        [HttpGet]
        //[Authorize(Policy = "Staff")]
        [Route("/[controller]/[action]")]
        public IActionResult Forecast(int? jobId = null)
        {
            string tab = "";
            switch (tab)
            {
                case "show": break;
                case "upload": break;
                case "generate": break;
                default: tab = "show";  break;
            }
            List<Area> MyOwnedAreas = CurrentUserOwnedAreas().ToList();
            List<Job> MyOwnedJobs = CurrentUserOwnedJobs().ToList();
            List<int> MyJobsIds = MyOwnedJobs.Select(x => x.Id).ToList();

            ViewBag.Tab = tab;
            ViewBag.Year = DateTime.Now.Year;
            ViewBag.Month = DateTime.Now.Month;
            ViewBag.MyOwnedJobs = MyOwnedJobs;
            ViewBag.MyOwnedAreas = MyOwnedAreas;
            ViewBag.Timeslots = StaffingService.GetTimeSlots();
            return View("Forecast");
        }

        [HttpPost]
        //[Authorize(Policy = "Staff")]
        [Route("/[controller]/[action]")]
        public async Task<IActionResult> Upload(IFormFile file, int year, int month, int jobId)
        {
            List<Job> MyOwnedJobs = CurrentUserOwnedJobs().ToList();
            List<int> MyJobsIds = MyOwnedJobs.Select(x => x.Id).ToList();
            if (MyOwnedJobs.Any(x => x.Id == jobId))
            {
                IDictionary<int, List<double>> Result = new Dictionary<int, List<double>>();
                var filePath = Path.GetTempFileName();
                if (file.Length > 0)
                {
                    using (var stream = new FileStream(filePath, FileMode.Create))
                    {
                        await file.CopyToAsync(stream);
                        Result = StaffingService.GetRequiredEventsFromFile(stream, 0, jobId, year, month);
                    }
                }
                ViewBag.serialized = JsonConvert.SerializeObject(Result);
                ViewBag.jobId = jobId;
                ViewBag.Year = year;
                ViewBag.Month = month;
                ViewBag.MyOwnedJobs = MyOwnedJobs;
                ViewBag.Timeslots = StaffingService.GetTimeSlots();
                ViewBag.Result = Result;
                return View("ConfirmForecast");
            }
            else
            {
                string res = "mal";
                return Ok(new { res });
            }
        }

        [HttpPost]
        //[Authorize(Policy = "Staff")]
        [Route("/[controller]/[action]")]
        public async Task<IActionResult> Upload2(IFormFile file, int year, int month, int jobId)
        {
            List<Job> MyOwnedJobs = CurrentUserOwnedJobs().ToList();
            if (MyOwnedJobs.Any(x => x.Id == jobId))
            {
                var filePath = Path.GetTempFileName();
                if (file.Length > 0)
                {
                    using (var stream = new FileStream(filePath, FileMode.Create))
                    {
                        await file.CopyToAsync(stream);
                        StaffingService.LoadForeacast(stream, 0, jobId, year, month);
                    }
                }
                return Ok(new { filePath, year, month, jobId });
            }
            else
            {
                string res = "mal";
                return Ok(new { res });
            }
        }
    }
}
