using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Perseo.Core;
using Perseo.Web.Helpers;
using Perseo.Web.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Perseo.Web.Controllers
{
    [Route("quality/[controller]/[action]/{id:int?}")]
    public class NonConformityCardController : BaseController
    {

        #region CONSTRUCTOR
        private readonly IService<NonConformityCard> NonConformityCardService;
        private readonly IService<Quality> QualityService;

        public NonConformityCardController(
            IHttpContextAccessor contextAccessor,
            IHostingEnvironment enviroment,
            IUserService userService,
            IStringLocalizer<SharedResources> localizer,
            IService<Quality> qualityService,
            IService<NonConformityCard> nonConformityCardService,
            IMapper mapper) : base(contextAccessor, enviroment, userService, localizer, mapper)
        {
            NonConformityCardService = nonConformityCardService;
            QualityService = qualityService;
        }

        #endregion

        #region HELPERS        

        private bool AuthorizeOrigin(NonConformityCard NonConformityCard)
        {
            if (NonConformityCard == null) { AddError(StatusCodes.Status404NotFound); return false; }
            if (!(AuthUser.OwnedAreas.Any(a => a.Id == NonConformityCard.Job.AreaId))) { AddError(StatusCodes.Status401Unauthorized); return false; }
            return true;
        }

        private bool AuthorizeAjaxOrigin(NonConformityCard NonConformityCard)
        {
            if (!IsAjax) { AddError(StatusCodes.Status400BadRequest); return false; }
            return AuthorizeOrigin(NonConformityCard);
        }

        private bool AuthorizeTarget(NonConformityCard NonConformityCard)
        {
            if (NonConformityCard == null) { AddError(StatusCodes.Status404NotFound); return false; }
            if (!(AuthUser.OwnedAreas.Any(a => a.Id == NonConformityCard.Job.AreaId))) { AddError(StatusCodes.Status401Unauthorized); return false; }
            return true;
        }

        private bool AuthorizeAjaxTarget(NonConformityCard NonConformityCard)
        {
            if (!IsAjax) { AddError(StatusCodes.Status400BadRequest); return false; }
            return AuthorizeTarget(NonConformityCard);
        }

        private NonConformityCardViewModel MapNonConformityCardToModel(NonConformityCard NonConformityCard)
        {
            NonConformityCardViewModel model = new NonConformityCardViewModel
            {
                Id = NonConformityCard.Id,
                StartDate = NonConformityCard.StartDate, 
                Description = NonConformityCard.Description,
                EndDate = NonConformityCard.EndDate,
                Active = NonConformityCard.Active,
                InternalReference = NonConformityCard.InternalReference,
                State = NonConformityCard.State,
                External = NonConformityCard.External,
                ExternalStaff = NonConformityCard.ExternalStaff,
                OriginUserId = NonConformityCard.OriginUserId,
                JobId = NonConformityCard.JobId,
                TargetUserId = NonConformityCard.TargetUserId,
                QuantityIncidences = NonConformityCard.Quantityincidences,  
                IncidentTypology = NonConformityCard.IncidentTypology,
                MinuteCost = NonConformityCard.MinuteCost, 
                HumanErrorSubtype = NonConformityCard.HumanErrorSubtype, 
                Cause = NonConformityCard.Cause,
                ImmediateAction = NonConformityCard.ImmediateAction,
                DefinitiveAction = NonConformityCard.DefinitiveAction,
                MonitoringEfficiency = NonConformityCard.MonitoringEfficiency,
                OriginUserName=NonConformityCard.OriginUser.FullFamilyName,
                TargetUserName = NonConformityCard.TargetUser.FullFamilyName,
                JobDescription=NonConformityCard.Job.Name
            };
            return model;
        }

        private bool Authorize(NonConformityCard NonConformityCard)
        {
            if (NonConformityCard == null) { AddError(StatusCodes.Status404NotFound); return false; }
            if (!(AuthUser.OwnedAreas.Any(a => a.Id == NonConformityCard.Id))) { AddError(StatusCodes.Status401Unauthorized); return false; }
            return true;
        }

        #endregion

        #region CRUD

        #region List
        [HttpGet]
        [Route("/[controller]")]
        [AuthorizeRoles(AppRole.OperationalManagementEdit, AppRole.OperationalManagementShow)]
        public IActionResult Index()
        {
            var pepe = Mapper.Map<NonConformityCardModel>(NonConformityCardService.GetOne(x => x.Id == 17626));
            return new JsonResult(pepe);
        }

        [HttpGet]
        [AuthorizeRoles(AppRole.OperationalManagementEdit, AppRole.OperationalManagementShow)]
        public IActionResult List()
        {
            ViewBag.Tab = "nonconformity";

            List<Area> authUserOwnedAreas = AuthUser.OwnedAreas.ToList();
            List<int> areaFilterIds = AreaFilter(authUserOwnedAreas, null);

            #region CHARTS

            List<string> labels = new List<string>();
            DateTime BaseDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            for (int i = 11; i >= 0; i--) { labels.Add(CultureInfo.CurrentCulture.TextInfo.ToTitleCase((BaseDate.AddMonths(-i)).ToString("MMMM"))); }

            ChartViewModel Area1 = new ChartViewModel()
            {
                Name = Localizer["Valoraci�n de Auditor�as"],
                Series = new List<ChartViewModel.Serie>() { new ChartViewModel.Serie() { Name = Localizer["Auditor�as"], Data = Enumerable.Repeat(0.0, labels.Count()).ToList() } },
                Labels = labels
            };
            ChartViewModel Area2 = new ChartViewModel()
            {
                Name = Localizer["Nivel de Conocimiento"],
                Series = new List<ChartViewModel.Serie>() { new ChartViewModel.Serie() { Name = Localizer["Conocimiento"], Data = Enumerable.Repeat(0.0, labels.Count()).ToList() } },
                Labels = labels
            };
            ChartViewModel Area3 = new ChartViewModel()
            {
                Name = Localizer["Evoluci�n de Indicadores"],
                Series = new List<ChartViewModel.Serie>() { new ChartViewModel.Serie() { Name = Localizer["Indicadores"], Data = new List<double>() { 74.23, 77.6, 76.4, 84.56, 82.27, 83.67, 84.24, 79.84, 80.23, 76.97, 75.34, 80.58 } } },
                Labels = labels
            };
            ChartViewModel Area4 = new ChartViewModel()
            {
                Name = Localizer["No Conformidades"],
                Series = new List<ChartViewModel.Serie>() { new ChartViewModel.Serie() { Name = Localizer["No Conformidad"], Data = Enumerable.Repeat(0.0, labels.Count()).ToList() } },
                Labels = labels
            };

            List<Quality> qualitys = QualityService.GetMany(q => ((q.StartDate >= BaseDate.AddYears(-1)) && (q.StartDate < BaseDate.AddMonths(1).AddSeconds(-1)) && (areaFilterIds.Any(i => i == q.Area.Id)))).ToList();
            List<double> UsersExams = Enumerable.Repeat(0.0, labels.Count()).ToList();
            List<double> UsersPassExams = Enumerable.Repeat(0.0, labels.Count()).ToList();
            List<double> UsersAudits = Enumerable.Repeat(0.0, labels.Count()).ToList();
            List<double> UsersPassAudits = Enumerable.Repeat(0.0, labels.Count()).ToList();
            foreach (Quality quality in qualitys)
            {
                int index = (((12 - BaseDate.Month) + (quality.StartDate.Month) - 1) % 12);

                switch (quality.Type)
                {
                    case CampaignType.KnowledgeEvaluation:
                        UsersExams[index] += quality.Users.Count();
                        UsersPassExams[index] += quality.Users.Where(qu => qu.QualityCompleted).Count();
                        break;
                    case CampaignType.PerformanceEvaluation:
                        UsersAudits[index] += quality.Users.Count();
                        UsersPassAudits[index] += quality.Users.Where(qu => qu.QualityCompleted).Count();
                        break;
                    default: return Error();
                }
            }

            for (int index = 0; index < labels.Count(); index++)
            {
                Area1.Series[0].Data[index] = UsersAudits[index] > 0 ? Math.Round((UsersPassAudits[index] / UsersAudits[index]) * 100, 2) : 0;
                Area2.Series[0].Data[index] = UsersExams[index] > 0 ? Math.Round((UsersPassExams[index] / UsersExams[index]) * 100, 2) : 0;
            }

            List<NonConformityCard> nonconformitys = NonConformityCardService.GetMany(c => ((c.StartDate >= BaseDate.AddYears(-1)) && (c.StartDate < BaseDate.AddMonths(1).AddSeconds(-1)) && (areaFilterIds.Any(i => i == c.Job.Area.Id)))).ToList();

            foreach (NonConformityCard card in nonconformitys)
            {
                int index = (((12 - BaseDate.Month) + (card.StartDate.Month) - 1) % 12);
                Area4.Series[0].Data[index] += 1;
            }

            #endregion

            var pagedResult = NonConformityCardService.GetManyPaged(null, "StartDate ASC");

            ViewBag.ServerSidePagination = pagedResult.ServerSidePagination;
            ViewBag.RowCount = pagedResult.RowCount;

            List<NonConformityCard> NonConformityCardList = pagedResult.Results.ToList();
            ListNonConformityCardViewModel model = new ListNonConformityCardViewModel()
            {
                AreaChart1 = Area1,
                AreaChart2 = Area2,
                AreaChart3 = Area3,
                AreaChart4 = Area4,
                Data = new List<NonConformityCardViewModel>()
            };
            NonConformityCardList.ForEach(x =>
            {
                NonConformityCardViewModel CardModel = new NonConformityCardViewModel
                {
                    Id = x.Id,
                    Description = x.Description.Truncate(110),
                    State = x.State,
                    StartDate = x.StartDate
                };
                model.Data.Add(CardModel);
            });
            return View("List", model);
        }

        [HttpGet]
        [AuthorizeRoles(AppRole.OperationalManagementEdit, AppRole.OperationalManagementShow)]
        public JsonResult ListData(DataTablesRequest request)
        {
            string search = null;

            var order = request.Order(new List<string> { "Description", "Id", "StartDate", "State" });

            List<AreaViewModel> model = new List<AreaViewModel>();

            var areaIds = AuthUser.OwnedAreas.Select(x => x.Id);

            var pagedResult = NonConformityCardService.GetManyPaged(null, order, request.page, request.pageSize);

            pagedResult.FilteredRowCount = search != null ? pagedResult.Results.Count() : pagedResult.RowCount;

            DataTablesResponse response = new DataTablesResponse(request.draw, pagedResult.RowCount, pagedResult.FilteredRowCount);

            pagedResult.Results.ToList().ForEach(area =>
            {
                response.Add(
                    area.Id,
                    area.Description.Truncate(110),
                    area.Id,
                    area.StartDate,
                    area.State.GetDisplayName()                    
                    );
            });

            return Json(response);
        }

        #endregion

        #region Edit

        [HttpGet]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        public IActionResult Edit(int id)
        {
            NonConformityCard nonConformityCard = NonConformityCardService.Get(id);

            NonConformityCardViewModel model = new NonConformityCardViewModel
            {
                Id = nonConformityCard.Id,
                StartDate = nonConformityCard.StartDate,
                Description = nonConformityCard.Description,
                EndDate = nonConformityCard.EndDate,
                InternalReference = nonConformityCard.InternalReference,
                State = nonConformityCard.State,
                External = nonConformityCard.External,
                ExternalStaff = nonConformityCard.ExternalStaff,
                OriginUserId = nonConformityCard.OriginUserId,
                JobId = nonConformityCard.JobId,
                TargetUserId = nonConformityCard.TargetUserId,
                QuantityIncidences = nonConformityCard.Quantityincidences,
                IncidentTypology = nonConformityCard.IncidentTypology,
                MinuteCost = nonConformityCard.MinuteCost,
                HumanErrorSubtype = nonConformityCard.HumanErrorSubtype,
                Cause = nonConformityCard.Cause,
                ImmediateAction = nonConformityCard.ImmediateAction,
                DefinitiveAction = nonConformityCard.DefinitiveAction,
                MonitoringEfficiency = nonConformityCard.MonitoringEfficiency,
                OriginUserName = nonConformityCard.OriginUser.FullFamilyName,
                TargetUserName = nonConformityCard.TargetUser.FullFamilyName,
                JobDescription = nonConformityCard.Job.Name
            };

            return View("Edit", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateAjaxModelState]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        public IActionResult Edit(NonConformityCardViewModel model)
        {
            NonConformityCard nonConformityCard = NonConformityCardService.Get(model.Id);

            nonConformityCard.StartDate = model.StartDate;
            nonConformityCard.Description = model.Description;
            nonConformityCard.EndDate = model.EndDate;
            nonConformityCard.InternalReference = model.InternalReference;
            nonConformityCard.State = model.State;
            nonConformityCard.External = model.External;
            nonConformityCard.ExternalStaff = model.ExternalStaff;
            nonConformityCard.JobId = model.JobId;
            nonConformityCard.TargetUserId = model.TargetUserId;
            nonConformityCard.Quantityincidences = model.QuantityIncidences;
            nonConformityCard.IncidentTypology = model.IncidentTypology;    
            nonConformityCard.MinuteCost = model.MinuteCost;
            nonConformityCard.HumanErrorSubtype = model.HumanErrorSubtype;
            nonConformityCard.Cause = model.Cause;
            nonConformityCard.ImmediateAction = model.ImmediateAction;
            nonConformityCard.DefinitiveAction = model.DefinitiveAction;
            nonConformityCard.MonitoringEfficiency = model.MonitoringEfficiency;
            
            if ( NonConformityCardService.Update(nonConformityCard) > 0 )
            {
                return Json(new JsonResponse() { Result = true, Response = "El cambio se ha realizado con �xito" });
            }
            return Error();
        }

        #endregion

        #region Show

        [HttpGet]
        [AuthorizeRoles(AppRole.OperationalManagementEdit, AppRole.OperationalManagementShow)]
        public IActionResult Show(int id)
        {
            NonConformityCard nonConformityCard = NonConformityCardService.Get(id);
            NonConformityCardViewModel model = MapNonConformityCardToModel(nonConformityCard);
            return View("Show", model);
        }

        #endregion

        #region AssignJob
      
        [HttpGet]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        public IActionResult AssignJob(int id)
        {
            NonConformityCard nonConformityCard = NonConformityCardService.Get(id);

            List<Job> nonConformityCardJobList = UserService.GetOwnedJobs(AuthUser, null, null).ToList();
            List<NonConformityCardAssignJobModel> modelList = new List<NonConformityCardAssignJobModel>();
            nonConformityCardJobList.ForEach(x =>
            {
                NonConformityCardAssignJobModel model = new NonConformityCardAssignJobModel
                {
                   JobId = x.Id,
                   JobDescription = x.Description,
                   NonConformityCardID = nonConformityCard.Id,
                   Id = nonConformityCard.Id
                };
                modelList.Add(model);
            });
                       
            return View("_AssignJob", modelList);
        }
        #endregion
        #region AssignTargetUser

        [HttpGet]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        public IActionResult AssignTargetUser(int id)
        {
            NonConformityCard nonConformityCard = NonConformityCardService.Get(id);

            List<User> nonConformityCardTargetUserList = UserService.GetMany().ToList();
            List<NonConformityCardAssignTargetUserModel> modelList = new List<NonConformityCardAssignTargetUserModel>();
            nonConformityCardTargetUserList.ForEach(x =>
            {
                NonConformityCardAssignTargetUserModel model = new NonConformityCardAssignTargetUserModel
                {
                    TargetUserId = x.Id,
                    TargetUserName = x.FullName,
                    NonConformityCardID = nonConformityCard.Id,
                    Id = nonConformityCard.Id
                };
                modelList.Add(model);
            });

            return View("_AssignTargetUser", modelList);
        }
        #endregion
    }
    #endregion
}
