using System.Collections.Generic;
using System.Linq;
using Perseo.Core;
using Microsoft.AspNetCore.Mvc;
using Perseo.Web.Models;
using Perseo.Web.Helpers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Localization;
using AutoMapper;

namespace Perseo.Web.Controllers
{
    [Route("[controller]/[action]/{id:int?}")]
    public class BookmarkController : BaseController
    {

        #region CONSTRUCTOR

        private readonly IService<Bookmark> BookmarkService;

        public BookmarkController(
            IHttpContextAccessor contextAccessor,
            IHostingEnvironment enviroment,
            IStringLocalizer<SharedResources> localizer,
            IUserService userService, IService<Bookmark> bookmarkService,
            IMapper mapper) : base(contextAccessor, enviroment, userService, localizer, mapper)
        {
            BookmarkService = bookmarkService;
        }

        #endregion

        #region CRUD

        #region List
        [HttpGet]
        [Route("/[controller]")]
        public IActionResult Index()
        {
            return List();
        }

        [HttpGet]
        [Route("/[controller]/[action]/{active?}")]
        public IActionResult List(bool? active = null)
        {
            List<BookmarkViewModel> model = new List<BookmarkViewModel>();
            AuthUser.Bookmark.ToList().ForEach(bookmark => {
                BookmarkViewModel item = new BookmarkViewModel
                {
                    Id = bookmark.Id,
                    Name = bookmark.Name,
                    Url = bookmark.Url,
                    Local = bookmark.Local,
                    Group = bookmark.Group,
                    ParentId = bookmark.ParentId,
                    Order = bookmark.Order
                };
                model.Add(item);
            });
            return View("List", model);
        }

        #endregion

        #region Show

        [Route("~/[controller]/[action]/{id}")]
        [HttpGet]
        public IActionResult Show(int id)
        {
            Bookmark bookmark = AuthUser.Bookmark.FirstOrDefault(x => x.Id == id);
            if(bookmark.Id > 0)
            {
                BookmarkViewModel model = new BookmarkViewModel
                {
                    Id = bookmark.Id,
                    Name = bookmark.Name,
                    Url = bookmark.Url,
                    Local = bookmark.Local,
                    Group = bookmark.Group,
                    ParentId = bookmark.ParentId,
                    Order = bookmark.Order
                };
                return View("Show", model);
            }
            return Error(404);
        }

        #endregion

        #region Add
        
        [HttpGet]
        public IActionResult Add(int? id)
        {
            BookmarkViewModel model = new BookmarkViewModel();
            if (id.HasValue)
            {
                Bookmark parent = AuthUser.Bookmark.FirstOrDefault(x => x.Id == id && x.Group == true);
                if (parent.Id > 0)
                {
                    model.ParentId = parent.Id;
                }
            }
            return PartialView("_Add", model);
        }
      
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateAjaxModelState]
        public IActionResult Add(BookmarkViewModel model)
        {
            Bookmark bookmark = new Bookmark
            {
                Name = model.Name,
                Url = model.Url,
                Local = model.Local,
                Group = model.Group,
                ParentId = model.ParentId,
                Order = model.Order
            };
            AuthUser.Bookmark.Add(bookmark);
            if (bookmark.Id > 0)
            {
                JsonResponse response = new JsonResponse() { Result = true, Response = "A�adido con �xito", Content = new { Id = bookmark.Id } };
                return Json(response);
            }
            return Error();
        }

        #endregion

        #region Edit

        [Route("~/[controller]/[action]/{id}")]
        [HttpGet]
        public IActionResult Edit(int id)
        {
            Bookmark bookmark = AuthUser.Bookmark.FirstOrDefault(x => x.Id == id);
            if (bookmark.Id > 0)
            {
                BookmarkViewModel model = new BookmarkViewModel
                {
                    Id = bookmark.Id,
                    Name = bookmark.Name,
                    Url = bookmark.Url,
                    Local = bookmark.Local,
                    Group = bookmark.Group,
                    ParentId = bookmark.ParentId,
                    Order = bookmark.Order
                };
                return PartialView("_Edit", model);
            }
            return Error(404);
                    
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateAjaxModelState]
        public IActionResult Edit(BookmarkViewModel model)
        {
            Bookmark bookmark = AuthUser.Bookmark.FirstOrDefault(x => x.Id == model.Id);
            if (bookmark.Id > 0)
            {
                bookmark.Name = model.Name;
                bookmark.Url = model.Url;
                bookmark.Local = model.Local;
                bookmark.Group = model.Group;
                bookmark.ParentId = model.ParentId;
                bookmark.Order = model.Order;
            }
            if(BookmarkService.Update(bookmark) > 0)
            {
                JsonResponse response = new JsonResponse() { Result = true, Response = "A�adido con �xito", Content = new { Id = bookmark.Id } };
                return Json(response);
            }
            return Error();
        }

        #endregion

        #region Delete

        [Route("~/[controller]/[action]/{id}")]
        [HttpGet]
        public IActionResult Delete(int id)
        {
            Bookmark bookmark = AuthUser.Bookmark.FirstOrDefault(x => x.Id == id);
            if (bookmark.Id > 0)
            {
                BookmarkViewModel model = new BookmarkViewModel
                {
                    Id = bookmark.Id,
                    Name = bookmark.Name,
                    Url = bookmark.Url,
                    Local = bookmark.Local,
                    Group = bookmark.Group,
                    ParentId = bookmark.ParentId,
                    Order = bookmark.Order
                };
                return PartialView("_Delete", model);
            }
            return Error(404);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateAjaxModelState]
        public IActionResult Delete(BookmarkViewModel model)
        {
            Bookmark bookmark = AuthUser.Bookmark.FirstOrDefault(x => x.Id == model.Id);
            if (bookmark.Id > 0)
            {
                if (BookmarkService.Delete(bookmark) > 0)
                {
                    JsonResponse response = new JsonResponse() { Result = true, Response = "Eliminado con �xito", Content = new { Id = model.Id } };
                    return Json(response);
                }
            }
            return Error();
        }

        #endregion

        #endregion

    }
}
