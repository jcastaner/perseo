using System;
using System.Collections.Generic;
using System.Linq;
using Perseo.Core;
using Microsoft.AspNetCore.Mvc;
using Perseo.Web.Models;
using Perseo.Web.Helpers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Localization;
using AutoMapper;

namespace Perseo.Web.Controllers
{
    [Route("[controller]/[action]/{id:int?}")]
    public class JobController : BaseController
    {

        #region CONSTRUCTOR

        private readonly IAreaService AreaService;
        private readonly IJobService JobService;
        private readonly IService<JobSla> SlaService;

        public JobController(
            IHttpContextAccessor contextAccessor,
            IHostingEnvironment enviroment,
            IUserService userService,
            IAreaService areaService,
            IService<JobSla> slaService,
            IStringLocalizer<SharedResources> localizer,
            IJobService jobService,
            IMapper mapper) : base(contextAccessor, enviroment, userService, localizer, mapper)
        {
            AreaService = areaService;
            JobService = jobService;
            SlaService = slaService;
        }

        #endregion

        #region HELPERS        

        private Job MapModeltoJob(JobViewModel model, Job job)
        {
            job.Name = model.Name;
            job.Description = model.Description;
            job.Type = model.Type;
            job.AreaId = model.AreaId;
            job.Default = model.Default;
            job.Acronym = string.IsNullOrEmpty(model.Acronym) ? "T" : model.Acronym;

            JobConfig jobconfig = job.JobConfig.SingleOrDefault(jc => jc.Active);
            if (jobconfig != null)
            {
                jobconfig.BillingType = model.Billing_type;
                jobconfig.TaskType = model.Task_type;
            }
            else
            {
                JobConfig newJobConfig = new JobConfig { StartDate = DateTime.Now };
                newJobConfig.BillingType = model.Billing_type;
                newJobConfig.TaskType = model.Task_type;
                job.JobConfig.Add(newJobConfig);
            }
                    
            return job;
        }

        private JobViewModel MapJobToModel(Job job)
        {
            JobConfig jobconfig = job.JobConfig.Where(x => x.JobId == job.Id).LastOrDefault();            
            JobKpi jobkpi = job.JobKpi.Where(x => x.JobId == job.Id).LastOrDefault();
            JobSla jobsla = job.JobSla.Where (x => x.JobId == job.Id).LastOrDefault();

            JobViewModel model = new JobViewModel
            {
                Id = job.Id,
                Name = job.Name,
                Description = job.Description,
                Type = job.Type,
                Acronym = job.Acronym,        
                AreaId = job.AreaId,
                AreaName = job.Area.Name
            };

            if (jobconfig != null)
            {
                model.Billing_type = jobconfig.BillingType;
                model.Task_type = jobconfig.TaskType;                                               
                model.Conf_SDate = jobconfig.StartDate;
                model.Conf_EDate = jobconfig.EndDate;                
            }

            if (jobsla != null)
            {
                model.Sla_Description = jobsla.SlaDescription;                
                model.Sla_Comparator = jobsla.SlaComparator.ToString();
                model.SLA = jobsla.Sla;
                model.TMO = jobsla.Tmo;
                model.Wait = jobsla.Wait;
                model.Queue = jobsla.Queue;
                model.Rate = jobsla.Rate;
                model.Sla_StartDate = jobsla.StartDate;
                model.Sla_EndDate = jobsla.EndDate;
            }
            else
            {
                model.Sla_Comp = '0';
            }
            
            if (job.Default.HasValue)
            {                
                model.Default = job.Default.Value;
                model.DefaultYesNo = job.Default.Value == false ? Localizer["No"] : Localizer["S�"];
            }
            else
            {
                model.Default = false;
                model.DefaultYesNo = Localizer["No"];                
            }

            return model;
        }

        private bool Authorize(Job job)
        {
            if (job == null) { AddError(StatusCodes.Status404NotFound); return false; }
            if (!(AuthUser.OwnedAreas.Any(a => a.Id == job.AreaId))) { AddError(StatusCodes.Status401Unauthorized); return false; }
            return true;
        }

        private bool AuthorizeAjax(Job job)
        {
            if (!IsAjax) { AddError(StatusCodes.Status400BadRequest); return false; }
            return Authorize(job);
        }

        #endregion

        #region CRUD
            #region List

        [HttpGet]
        [Route("/[controller]")]
        [AuthorizeRoles(AppRole.OperationalManagementEdit, AppRole.OperationalManagementShow)]
        public IActionResult Index()
        {
            return List();
        }

        [HttpGet]
        [AuthorizeRoles(AppRole.OperationalManagementEdit, AppRole.OperationalManagementShow)]
        public IActionResult List()
        {
            List<AreaViewModel> modelList = new List<AreaViewModel>();
          
            return View("List", modelList);
        }

            #endregion
            #region Show

        [HttpGet]
        [AuthorizeRoles(AppRole.OperationalManagementEdit, AppRole.OperationalManagementShow)]
        public IActionResult Show(int id)
        {
            Job job = JobService.Get(id);
            if (job == null) return Error(StatusCodes.Status406NotAcceptable);

            JobViewModel model = MapJobToModel(job);
            model.JobSla = job.JobSla.Where(a => a.JobId == job.Id).ToList();
            model.JobKpi = job.JobKpi.Where(a => a.JobId == job.Id).ToList();
            model.FullName = job.Area.Name;
            ViewBag.Avatar = "";

            return View("Show", model);
        }

        [HttpGet]
        [AuthorizeRoles(AppRole.OperationalManagementEdit, AppRole.OperationalManagementShow)]
        public IActionResult Show_Tab(int id, string tab = null)
        {          
            Job job = JobService.Get(id);
            
            ViewBag.JobId = job.Id;
            ViewBag.Name = job.Name;
            ViewBag.FullName = job.Area.Name;
            ViewBag.Avatar = "";
             tab = tab ?? "general";
            tab = tab.ToLower();
            ViewBag.Tab = tab;
            switch (tab)
            {
                case "general": return ShowGeneral(job);
                case "kpi": return ShowKpi(job);                
                default: return Error(StatusCodes.Status404NotFound);
            }
        }

        private IActionResult ShowGeneral(Job job)
        {
            JobViewModel model = MapJobToModel(job);            
            model.JobSla = job.JobSla.Where(a => a.JobId == job.Id).ToList();
            model.JobKpi = job.JobKpi.Where(a => a.JobId == job.Id).ToList();
            model.FullName = job.Area.Name;
            return View("ShowGeneral", model);
        }

        private IActionResult ShowKpi(Job job)
        {
            JobViewModel model = MapJobToModel(job);
            model.JobKpi = job.JobKpi.Where(a => a.JobId == job.Id).ToList();
            model.FullName = job.Area.Name;
            return View("ShowKpi", model);
        }

            #endregion
            #region Add

        [HttpGet]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        public IActionResult Add(int? id)
        {
            if (!id.HasValue) return Error();
            JobViewModel model = new JobViewModel
            {
                AreaId =  id.Value
            };
            return PartialView("_Add", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateAjaxModelState]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        public IActionResult Add(JobViewModel model)
        {
            Area area = AreaService.Get(model.AreaId);
            switch (area.Type)
            {
                case AreaType.Organizational: model.Type = JobType.Organizational; break;
                case AreaType.Project: model.Type = JobType.Project; break;
                case AreaType.Productive: model.Type = JobType.Productive; break;
                case AreaType.General: model.Type = JobType.General; break;
                default: model.Type = JobType.General; break;
            }
            Job job = MapModeltoJob(model, new Job());
            job.StartDate = DateTime.Now;

            if (!Authorize(job)) return Error();
            
            if (JobService.Add(job) > 0)
            {
                JsonResponse response = new JsonResponse() { Result = true, Response = "A�adido con �xito", Content = new { job.Id } };
                return Json(response);
            }
            return Error();
        }

            #endregion
            #region Edit

        [HttpGet]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        public IActionResult Edit(int id)
        {
            Job  job= JobService.Get(id);
            if (job == null) return Error(StatusCodes.Status406NotAcceptable);

            JobViewModel model = MapJobToModel(job);
            model.JobSla = job.JobSla.Where(a => a.JobId == job.Id).ToList();
            model.JobKpi = job.JobKpi.Where(a => a.JobId == job.Id).ToList();
            model.FullName = job.Area.Name;

            return View("Edit", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateAjaxModelState]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        public IActionResult Edit(JobViewModel model)
        {
            Job job = JobService.Get(model.Id);

            switch (job.Area.Type)
            {
                case AreaType.Organizational: model.Type = JobType.Organizational; break;
                case AreaType.Project: model.Type = JobType.Project; break;
                case AreaType.Productive: model.Type = JobType.Productive; break;
                case AreaType.General: model.Type = JobType.General; break;
                default: model.Type = JobType.General; break;
            }

            job = MapModeltoJob(model, job);
            
            if (model.Default)
            {
                foreach (Job j in job.Area.Job.Where(j => j.Default == true))
                {
                    j.Default = false;
                }
                job.Default = true;
            }

            if (JobService.Update(job) > 0)
            {
                return RedirectToAction("Show", new { id = job.Id });
            }
            return Error();
        }

            #endregion
            #region Delete

        [HttpGet]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        public IActionResult Delete(int id)
        {
            UserViewModel model = new UserViewModel();
            return View("Delete", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateAjaxModelState]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        public IActionResult DeleteConfirm(int id)
        {
            UserService.Delete(id);
            return RedirectToAction("Index");
        }

           #endregion
        #endregion

        #region JobSLA

        [Route("~/[controller]/show/{id}/[action]")]
        [HttpGet]
        public IActionResult JobSlaHistory(int id)
        {            
            ViewBag.Accion = "Show";         
            JobSla jobSla = SlaService.Get(id);
            ViewBag.Name = Localizer["Informaci�n de SLA"];
            return AssignJobSla(jobSla);
        }

        [Route("~/[controller]/edit/{id}/[action]")]
        [HttpGet]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        public IActionResult EditJobSlaHistory(int id)
        {            
            ViewBag.Accion = "Edit";
            JobSla jobSla = SlaService.Get(id);
            ViewBag.Name = Localizer["Modificaci�n de SLA"];
            return AssignJobSla(jobSla);
        }

        [Route("~/[controller]/edit/{id}/[action]")]
        [HttpGet]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        public IActionResult AddJobSlaHistory(int id)
        {
            ViewBag.Accion = "Add";
            Job job = JobService.Get(id);

            AssignJobSlaViewModel Model = new AssignJobSlaViewModel
            {
                JobId = job.Id,
                Entity = Localizer["SLA"],
                AssignmentStartDate = DateTime.Now,
                History = job.JobSla.Select(i =>
                    new AssignJobHistory
                    {                       
                        StartDate = i.StartDate,
                        EndDate = i.EndDate
                    }).OrderByDescending(i => i.StartDate).ToList()
            };
            ViewBag.Name = Localizer["Nuevo SLA"];
            return PartialView("_AssignJobSla", Model);
        }

        [HttpGet]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        protected IActionResult AssignJobSla(JobSla jobSla)
        {                        
            AssignJobSlaViewModel Model = new AssignJobSlaViewModel
            {
                Id = jobSla.Id,
                Entity = Localizer["SLA"],
                SLA = jobSla.Sla,
                Sla_Description = jobSla.SlaDescription,
                Sla_Comparator = jobSla.SlaComparator.ToString(),
                Rate = jobSla.Rate,
                TMO = jobSla.Tmo,
                Queue = jobSla.Queue,
                Wait = jobSla.Wait,
                AssignmentStartDate = jobSla.StartDate,
                JobId = jobSla.JobId,
            };
            return PartialView("_AssignJobSla", Model);
        }

        [Route("~/[controller]/edit/[action]")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateAjaxModelState]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        public IActionResult EditJobSlaHistory(AssignJobSlaViewModel Model)
        {            
            JobSla jobSla = new JobSla()
            {
                Id = Model.Id,
                SlaComparator = Convert.ToChar(Model.Sla_Comparator),
                SlaDescription = Model.Sla_Description,
                Sla = Model.SLA,
                Rate = Model.Rate,
                Tmo = Model.TMO,
                Wait = Model.Wait,
                Queue = Model.Queue,     
                JobId = Model.JobId,
                StartDate = Model.AssignmentStartDate
            };            
            SlaService.Update(jobSla);

            JsonResponse response = new JsonResponse() { Result = true, Response = "El cambio se ha realizado con �xito" };
            return Json(response);
        }

        [Route("~/[controller]/edit/[action]")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateAjaxModelState]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        public IActionResult AddJobSlaHistory(AssignJobSlaViewModel Model)
        {
            Job job = JobService.Get(Model.JobId);
            JobSla lastSla = job.LastJobSla;

            if(job.LastJobSla != null)
            {
                job.LastJobSla.EndDate = Model.AssignmentStartDate.AddDays(-1);
            }

            job.JobSla.Add(new JobSla
            {
                JobId = Model.JobId,
                SlaComparator = Convert.ToChar(Model.Sla_Comparator),
                SlaDescription = Model.Sla_Description,
                Sla = Model.SLA,
                Rate = Model.Rate,
                Tmo = Model.TMO,
                Wait = Model.Wait,
                Queue = Model.Queue,
                StartDate = Model.AssignmentStartDate
            });

            JobService.Update(job);

            JsonResponse response = new JsonResponse() { Result = true, Response = "El cambio se ha realizado con �xito" };
            return Json(response);
        }

        #endregion
    }
}