using AutoMapper;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Perseo.Core;

namespace Perseo.Web.Controllers
{
    [Route("[controller]/[action]/{id:int?}")]
    public class SuppliesController : BaseController
    {
        public SuppliesController( IHttpContextAccessor contextAccessor,  IHostingEnvironment enviroment, IStringLocalizer<SharedResources> localizer,
            IUserService userService,
            IMapper mapper) : base(contextAccessor, enviroment, userService, localizer, mapper)
        { }

        [HttpGet]
        [Route("/[controller]")]
        public IActionResult Index()
        {
            return List();
        }

        [HttpGet]
        public IActionResult List()
        {
            return View("List");
        }

    }
}
