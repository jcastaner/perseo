using Perseo.Core;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Localization;
using AutoMapper;

namespace Perseo.Web.Controllers
{
    [Route("[controller]/[action]/{id:int?}")]
    public class IncidentController : BaseController
    {
        public IncidentController( IHttpContextAccessor contextAccessor,  IHostingEnvironment enviroment, IStringLocalizer<SharedResources> localizer,
            IUserService userService, IMapper mapper) : base(contextAccessor, enviroment, userService, localizer, mapper)
        { }

        [HttpGet]
        [Route("/[controller]")]
        [AuthorizeRoles(AppRole.TecnicalManagementEdit, AppRole.TecnicalManagementShow)]
        public IActionResult Index()
        {
            return List();

        }

        [HttpGet]
        [AuthorizeRoles(AppRole.TecnicalManagementEdit, AppRole.TecnicalManagementShow)]
        public IActionResult List()
        {
            return View("List");
        }

    }
}
