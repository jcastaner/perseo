﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Perseo.Web.Models;
using Perseo.Web.Identity;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using Perseo.Core;
using Perseo.Web.Helpers;
using Microsoft.Extensions.Localization;
using Microsoft.AspNetCore.Localization;
using AutoMapper;

namespace Perseo.Web.Controllers
{
    [Route("[controller]/[action]/{id:int?}")]
    public class AccountController : BaseController
    {
        private readonly UserManager<ApplicationUser> UserManager;
        private readonly SignInManager<ApplicationUser> SignInManager;
        private readonly IEmailService EmailService;
        private readonly ILogger _logger;

        public AccountController(
            IHttpContextAccessor contextAccessor,
            IHostingEnvironment enviroment,
            IUserService userService,
            IStringLocalizer<SharedResources> localizer,
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            IEmailService emailService,
            ILogger<HomeController> logger,
            IMapper mapper) : base(contextAccessor, enviroment, userService, localizer, mapper)
        {
            UserManager = userManager;
            SignInManager = signInManager;
            EmailService = emailService;
            _logger = logger;

        }

        [TempData]
        public string ErrorMessage { get; set; }

        [HttpGet]
        [Route("~/[controller]/changepassword/mandatory")]
        public IActionResult ChangePasswordMandatory()
        {
            ChangePasswordViewModel model = new ChangePasswordViewModel();
            return PartialView("ChangePasswordMandatory", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("~/[controller]/changepassword/mandatory")]
        public async Task<IActionResult> ChangePasswordMandatory(ChangePasswordViewModel model)
        {;
            ApplicationUser user = await UserManager.FindByNameAsync(AuthUser.UserName);
            bool goodOldPassword = await SignInManager.UserManager.CheckPasswordAsync(user, model.OldPassword);
            if (goodOldPassword)
            {
                if (ModelState.IsValid)
                {
                    var Hasher = new PasswordHasher<User>();
                    AuthUser.PasswordHash = Hasher.HashPassword(AuthUser, model.Password);
                    AuthUser.SecurityStamp = Guid.NewGuid().ToString();
                    AuthUser.ChangePassword = false;
                    AuthUser.RefreshCookie = false;
                    if (UserService.Update(AuthUser) > 0)
                    {
                        var task = ContextAccessor.HttpContext.RefreshLoginAsync();
                        return RedirectToLocal("");
                    }               
                }
            }
            else
            {
                ModelState.AddModelError("OldPassword", Localizer["El password introducido es incorrecto"]);
            }
            return View(model);
        }

        [HttpGet]
        [Route("~/[controller]/[action]")]
        public IActionResult ChangePassword()
        {
            ChangePasswordViewModel model = new ChangePasswordViewModel() { Id = AuthUser.Id };
            return PartialView("_ChangePassword", model);
        }

        [HttpPost]
        [ValidateAjaxModelState]
        [ValidateAntiForgeryToken]
        [Route("~/[controller]/[action]")]
        public async Task<IActionResult> ChangePassword(ChangePasswordViewModel model)
        {
            ApplicationUser user = await UserManager.FindByNameAsync(AuthUser.UserName);
            bool goodOldPassword = await SignInManager.UserManager.CheckPasswordAsync(user, model.OldPassword);
            if (goodOldPassword)
            {
                var Hasher = new PasswordHasher<User>();
                AuthUser.PasswordHash = Hasher.HashPassword(AuthUser, model.Password);
                AuthUser.SecurityStamp = Guid.NewGuid().ToString();
                AuthUser.ChangePassword = false;
                AuthUser.RefreshCookie = false;
                if (UserService.Update(AuthUser) > 0)
                {
                    var task = ContextAccessor.HttpContext.RefreshLoginAsync();
                    JsonResponse response = new JsonResponse() { Result = true, Response = "El cambio se ha realizado con éxito" };
                    return Json(response);
                }
            }
            else
            {
                Response.StatusCode = 400;
                return new JsonResult(  new
                {
                    Result = false,
                    Response = "OOOPS! ALGO SALIÓ MAL (400)",
                    Content = new object[]{ new { key = "OldPassword", errors = new string[] { "El password es incorrecto" } } }
                });
            }
            return Error(StatusCodes.Status400BadRequest);
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("~/[controller]/[action]/{returnUrl?}")]
        public async Task<IActionResult> Login(string returnUrl = null)
        {
            
            await HttpContext.SignOutAsync(IdentityConstants.ExternalScheme);
            LoginViewModel model = new LoginViewModel() { ReturnUrl = System.Net.WebUtility.UrlDecode(returnUrl) };
            return View("Login", model);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        [Route("~/[controller]/[action]")]
        public async Task<IActionResult> Login(LoginViewModel model)
        {

            if (ModelState.IsValid)
            {
                var result = await SignInManager.PasswordSignInAsync(model.UserName, model.Password, model.RememberMe, lockoutOnFailure: true);

                if (result.Succeeded)
                {
                    _logger.LogInformation("User logged in.");
                    return RedirectToLocal(model.ReturnUrl);
                }
                if (result.IsLockedOut)
                {
                    _logger.LogWarning("User account locked out.");
                    return RedirectToAction(nameof(Lockout));
                }
                else
                {
                    ModelState.AddModelError(string.Empty, Localizer["Usuario y/o Contraseña incorrectos"]);
                    return View(model);
                }
            }
            return View(model);
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("~/[controller]/[action]")]
        public IActionResult Lockout()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("~/[controller]/[action]")]
        public async Task<IActionResult> Logout()
        {
            await SignInManager.SignOutAsync();
            //_logger.LogInformation("User logged out.");
            return RedirectToAction("Login", "Account");
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("~/[controller]/[action]")]
        public IActionResult SignedOut()
        {
            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("~/[controller]/[action]")]
        public IActionResult ForgotPassword()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        [Route("~/[controller]/[action]")]
        public async Task<IActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindByEmailAsync(model.Email);
                if (user == null || !(await UserManager.IsEmailConfirmedAsync(user)))
                {
                    return RedirectToAction(nameof(ForgotPasswordConfirmation));
                }
                var code = await UserManager.GeneratePasswordResetTokenAsync(user);
                var callbackUrl = Url.ResetPasswordCallbackLink(user.Id.ToString(), code, Request.Scheme);
                await EmailService.SendAsync(model.Email, Localizer["Recuperación de contraseña"],null,
                    Localizer["Por favor, cambie su contraseña pinchando aquí: "] + "<a href='"+ callbackUrl + "'>link</a>");
                return RedirectToAction(nameof(ForgotPasswordConfirmation));
            }
            return View(model);
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("~/[controller]/[action]")]
        public IActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("~/[controller]/[action]/{code?}")]
        public IActionResult ResetPassword(string code = null)
        {
            if (code == null)
            {
                throw new ApplicationException("A code must be supplied for password reset.");
            }
            var model = new ResetPasswordViewModel { Code = code };
            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        [Route("~/[controller]/[action]")]
        public async Task<IActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = await UserManager.FindByEmailAsync(model.Email);

            if (user == null)
            {
                return RedirectToAction(nameof(ResetPasswordConfirmation));
            }
            string resetToken = await UserManager.GeneratePasswordResetTokenAsync(user);
            var result = await UserManager.ResetPasswordAsync(user, model.Code, model.Password);
            if (result.Succeeded)
            {
                return RedirectToAction(nameof(ResetPasswordConfirmation));
            }
            AddErrors(result);
            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("~/[controller]/[action]")]
        public IActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        [HttpGet]
        [Route("~/[controller]/[action]")]
        public IActionResult AccessDenied()
        {
            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("~/[controller]/[action]/{culture}/{url?}")]
        public IActionResult SetLanguage(string culture, string url = null)
        {
            Response.Cookies.Append(
                CookieRequestCultureProvider.DefaultCookieName,
                CookieRequestCultureProvider.MakeCookieValue(new RequestCulture(culture)),
                new CookieOptions { Expires = DateTimeOffset.UtcNow.AddYears(1), HttpOnly = true, Secure = true, SameSite = SameSiteMode.Lax}
            );
            var returnUrl = Request.Headers["Referer"].ToString();
            if (String.IsNullOrEmpty(returnUrl))
            {
                return String.IsNullOrEmpty(url) ? Redirect("/") : Redirect(url)
;            }
            else
            {
                return Redirect(returnUrl);
            }
        }

        #region Helpers

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }
        }

        private IActionResult RedirectToLocal(string returnUrl)
        {

            returnUrl = System.Net.WebUtility.UrlDecode(returnUrl);
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return Redirect("/");
            }
        }

        #endregion
    }
}