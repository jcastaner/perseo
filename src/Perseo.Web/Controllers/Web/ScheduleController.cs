using System;
using System.Collections.Generic;
using System.Linq;
using Perseo.Core;
using Microsoft.AspNetCore.Mvc;
using Perseo.Web.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Perseo.Web.Helpers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Localization;
using AutoMapper;

namespace Perseo.Web.Controllers
{
    [Route("[controller]/[action]/{id:int?}")]
    public class ScheduleController : BaseController
    {
        private readonly IScheduleService ScheduleService;
        private readonly IJobService JobService;
        private readonly IRequestService RequestService;
        private readonly IAreaService AreaService;

        public ScheduleController(IScheduleService scheduleService, 
            IHttpContextAccessor contextAccessor,
            IHostingEnvironment enviroment,
            IJobService jobService,
            IRequestService requestService,
            IUserService userService,
            IStringLocalizer<SharedResources> localizer,
            IAreaService areaService,
            IMapper mapper) : base(contextAccessor, enviroment, userService, localizer, mapper)
        {
            ScheduleService = scheduleService;
            AreaService = areaService;
            JobService = jobService;
            RequestService = requestService;
        }

        [HttpGet]
        [Route("/[controller]")]
        [AuthorizeRoles(AppRole.OperationalManagementEdit, AppRole.OperationalManagementShow)]
        public IActionResult Index()
        {
            return Show(null, null, null, null,null);
        }

        [Route("~/[controller]/[action]/{active?}/{year:int?}/{month:int?}/{selectedareas?}/{selectedoffice?}")]
        [HttpGet]
        [AuthorizeRoles(AppRole.OperationalManagementEdit, AppRole.OperationalManagementShow)]
        public IActionResult Show(string active, int? year, int? month, string selectedareas, string selectedoffice)
        {
            if (year == null) year = DateTime.Now.Year;
            if (month == null) month = DateTime.Now.Month;

            if (active != null && active != "all" && active != "active" && active != "inactive")
            {
                return Error(StatusCodes.Status404NotFound);
            }
            ViewBag.Active = active == "" || active == null ? "active" : active;
            bool? Active = active == "" || active == null || active == "active" ? true : active == "inactive" ? false : (bool?)null;

            List<Area> authUserOwnedAreas = AuthUser.OwnedAreas.ToList();
            List<int> areaFilterIds = AreaFilter(authUserOwnedAreas, selectedareas);

            List<Office> areasOffices = AreaService.GetMany(a => areaFilterIds.Contains(a.Id)).SelectMany(a => a.UserArea).Where(ua => ua.Active).Select(ua => ua.User).SelectMany(u => u.UserOffice).Where(uo => uo.Active).Select(uo => uo.Office).Distinct().ToList();
            bool nullOffices = AreaService.GetMany(a => areaFilterIds.Contains(a.Id)).SelectMany(a => a.UserArea).Where(ua => ua.Active).Select(ua => ua.User).Any(u => !(u.UserOffice.Any(uo => uo.Active)));
            if (nullOffices)
            {
                areasOffices.Insert(0, new Office() { Id = 0, Name = Localizer["No informada"] });
            }
            List<Office> selectedOffices = new List<Office>();

            IEnumerable<int> officeFilterIds = new List<int> ();
            if (selectedoffice != null)
            {
                officeFilterIds = selectedoffice.Split(';').Select(int.Parse);
                selectedOffices = areasOffices.Where(o => officeFilterIds.Contains(o.Id)).ToList();
            }

            ViewBag.SelectedOffices = Mapper.Map<List<OfficeModel>>(selectedOffices);
            ViewBag.AreasOffices = Mapper.Map<List<OfficeModel>>(areasOffices);

            ScheduleViewModel model = new ScheduleViewModel
            {
                Year = year.Value,
                Month = month.Value,
                Areas = authUserOwnedAreas,
                AreasIds = areaFilterIds,
                OwnedAreas = Mapper.Map<List<AreaModel>>(authUserOwnedAreas),
                SelectedAreasIds = areaFilterIds,
                Rows = new List<ScheduleRow>()
            };
            if (areaFilterIds.Count <= 0) return View("Show", model);

            UserService.GetAllUserSchedule(model.Year, model.Month, areaFilterIds, Active, officeFilterIds.ToList()).ToList().ForEach(user => 
            {
                Dictionary<string, ScheduleDate> scheduleDict = new Dictionary<string, ScheduleDate>();
                foreach (ScheduleDate scheduleDay in user.ScheduleDate)
                {
                    if (scheduleDay != null)  scheduleDict.Add("D" + scheduleDay.Day, scheduleDay);
                }
                ScheduleRow scheduleRow = new ScheduleRow
                {
                    UserId = user.Id,
                    FullName = user.FullName,
                    Columns = scheduleDict
                };
                model.Rows.Add(scheduleRow);
            });
            return View("Show", model);
        }

        [Route("~/[controller]/[action]/{year:int?}/{month:int?}/{selectedareas?}/{selectedoffice?}")]
        [HttpGet]
        [AuthorizeRoles(AppRole.OperationalManagementEdit, AppRole.OperationalManagementShow)]
        public IActionResult ShowDashboard(int? year, int? month, string selectedareas, string selectedoffice)
        {
            year = year ?? DateTime.Now.Year;
            month = month ?? DateTime.Now.Month;

            List<Area> authUserOwnedAreas = AuthUser.OwnedAreas.ToList();
            List<int> areaFilterIds = AreaFilter(authUserOwnedAreas, selectedareas);

            List<Area> areas = AreaService.GetAreasToSchedule(areaFilterIds).ToList();
            if (areas.Count() < 1) return Error(StatusCodes.Status406NotAcceptable);

            IEnumerable<Job> productiveJobs = areas.SelectMany(a => a.Job).Where(j => j.Type == JobType.Productive);
            IEnumerable<User> users = areas.SelectMany(a => a.UserArea).Where(ua => ua.Active).Select(ua => ua.User).Distinct();

            List<Office> areasOffices = users.SelectMany(u => u.UserOffice.Where(uo => uo.Active).Select(uo => uo.Office)).Distinct().ToList();
            bool nullOffices = users.Any(u => !(u.UserOffice.Any(uo => uo.Active)));
            if (nullOffices)
            {
                areasOffices.Insert(0, new Office() { Id = 0, Name = Localizer["No informada"] });
            }

            List<Office> selectedOffices = new List<Office>();

            if (selectedoffice != null)
            {
                IEnumerable<int> officeFilterIds = selectedoffice.Split(';').Select(int.Parse);
                selectedOffices = areasOffices.Where(o => officeFilterIds.Contains(o.Id)).ToList();
                users = users.Where(u => officeFilterIds.Any(i => i.Equals(0)) ? u.UserOffice.Any(uo => uo.Active && officeFilterIds.Contains(uo.OfficeId)) || !(u.UserOffice.Any()) : u.UserOffice.Any(uo => uo.Active && officeFilterIds.Contains(uo.OfficeId))).Distinct();
            }

            ScheduleDashBoardViewModel model = new ScheduleDashBoardViewModel();

            #region TABLE

            IEnumerable<ScheduleDateTermJob> sdtjs = users.SelectMany(u => u.ScheduleDate).Where(sd => sd.Year == year.Value).SelectMany(sd => sd.ScheduleDateTerm).SelectMany(sdt => sdt.ScheduleDateTermJob);
            IEnumerable<Job> generalJobs = sdtjs.Select(s => s.Job).Where(j => j.Type != JobType.Productive && j.Type != JobType.General && j.Type != JobType.Organizational && j.Type != JobType.Project).Distinct();
            IEnumerable<JobType> JobTypes = generalJobs.Select(j => j.Type).Distinct();

            foreach (JobType type in JobTypes)
            {
                GeneralJobType generalJobType = new GeneralJobType
                {
                    Acronym = generalJobs.First(x => x.Type == type).Acronym,
                    Days = Enumerable.Repeat(0.0, 13).ToList(),
                    Hours = Enumerable.Repeat(0.0, 13).ToList()
                };
                foreach (Job item in generalJobs.Where(x => x.Type == type))
                {
                    GeneralJob generalJob = new GeneralJob() { Id = item.Id, Name = item.Name };
                    generalJob.Days = Enumerable.Repeat(0.0, 13).ToList();
                    generalJob.Hours = Enumerable.Repeat(0.0, 13).ToList();

                    double days = sdtjs.Where(sdtj => sdtj.JobId == item.Id).Count();
                    double hours = sdtjs.Where(sdtj => sdtj.JobId == item.Id).Select(sdtj => sdtj.ScheduleDateTerm).Select(sdt => sdt.Hours).Sum();

                    generalJobType.Days[0] += days;
                    generalJobType.Hours[0] += hours;
                    generalJob.Days[0] = days;
                    generalJob.Hours[0] = hours;

                    for (int i = 1; i <= 12; i++)
                    {
                        days = sdtjs.Where(sdtj => sdtj.JobId == item.Id).Where(sdtj => sdtj.ScheduleDateTerm.ScheduleDate.Month == i).Count();
                        hours = sdtjs.Where(sdtj => sdtj.JobId == item.Id).Where(sdtj => sdtj.ScheduleDateTerm.ScheduleDate.Month == i).Select(sdtj => sdtj.ScheduleDateTerm).Select(sdt => sdt.Hours).Sum();

                        generalJobType.Days[i] += days;
                        generalJobType.Hours[i] += hours;
                        generalJob.Days[i] = days;
                        generalJob.Hours[i] = hours;
                    }
                    generalJobType.GeneralJobs.Add(generalJob);
                }
                model.GeneralJobTypes.Add(type, generalJobType);
            }

            GeneralJobType productiveJobType = new GeneralJobType
            {
                Acronym = "T",
                Days = Enumerable.Repeat(0.0, 13).ToList(),
                Hours = Enumerable.Repeat(0.0, 13).ToList()
            };
            foreach (Job item in productiveJobs)
            {
                GeneralJob productiveJob = new GeneralJob() { Id = item.Id, Name = item.Name };
                productiveJob.Days = Enumerable.Repeat(0.0, 13).ToList();
                productiveJob.Hours = Enumerable.Repeat(0.0, 13).ToList();

                double days = sdtjs.Where(sdtj => sdtj.JobId == item.Id).Count();
                double hours = sdtjs.Where(sdtj => sdtj.JobId == item.Id).Select(sdtj => sdtj.ScheduleDateTerm).Select(sdt => sdt.Hours).Sum();

                productiveJobType.Days[0] += days;
                productiveJobType.Hours[0] += hours;
                productiveJob.Days[0] = days;
                productiveJob.Hours[0] = hours;

                for (int i = 1; i <= 12; i++)
                {
                    days = sdtjs.Where(sdtj => sdtj.JobId == item.Id).Where(sdtj => sdtj.ScheduleDateTerm.ScheduleDate.Month == i).Count();
                    hours = sdtjs.Where(sdtj => sdtj.JobId == item.Id).Where(sdtj => sdtj.ScheduleDateTerm.ScheduleDate.Month == i).Select(sdtj => sdtj.ScheduleDateTerm).Select(sdt => sdt.Hours).Sum();

                    productiveJobType.Days[i] += days;
                    productiveJobType.Hours[i] += hours;
                    productiveJob.Days[i] = days;
                    productiveJob.Hours[i] = hours;
                }
                productiveJobType.GeneralJobs.Add(productiveJob);
            }
            model.GeneralJobTypes.Add(JobType.Productive, productiveJobType);

            #endregion

            ViewBag.Tab = "tab-dashboard";
            ViewBag.Year = year;
            ViewBag.Month = month;
            ViewBag.SelectedAreasIds = areaFilterIds;
            ViewBag.SelectedOffices = Mapper.Map<List<OfficeModel>>(selectedOffices);
            ViewBag.AreasOffices = Mapper.Map<List<OfficeModel>>(areasOffices);
            ViewBag.OwnedAreas = Mapper.Map<List<AreaModel>>(authUserOwnedAreas);

            #region CHARTS

            IEnumerable<ScheduleDate> scheduleDates = users.SelectMany(u => u.ScheduleDate).Where(sd => sd.Month == month.Value && sd.Year == year.Value);
       
            if (scheduleDates.Count() < 1)
            {
                return View("DashboardNoData", model);
            }

                #region BARS

            ChartViewModel Bars1 = new ChartViewModel()
            {
                Name = Localizer["Dedicaci�n por Tarea"],
                ComplexSeries = new List<ChartViewModel.ComplexSerie>() {
                    new ChartViewModel.ComplexSerie() { Name = Localizer["Horas"], Data = new List<HighCharts.Data>(){ } }
                }
            };

            foreach (Job job in productiveJobs)
            {
                List<ScheduleDateTermJob> scheduleDateTermJobs = scheduleDates.SelectMany(sd => sd.ScheduleDateTerm.SelectMany(sdt => sdt.ScheduleDateTermJob).Where(sdtj => sdtj.Job == job)).ToList();
                double jobhours = 0;
                foreach (ScheduleDateTermJob sdtj in scheduleDateTermJobs) { jobhours += sdtj.ScheduleDateTerm.Hours; }
                Bars1.ComplexSeries.First().Data.Add(new HighCharts.Data() { Name = job.Name, Y = jobhours });
            }

                #endregion
                #region COLUMN

            ChartViewModel Column1 = new ChartViewModel()
            {
                Name = Localizer["Resumen Diario"],
                ComplexSeries = new List<ChartViewModel.ComplexSerie>() {
                    new ChartViewModel.ComplexSerie() { Name = Localizer["Trabajo"], Data = new List<HighCharts.Data>() },
                    new ChartViewModel.ComplexSerie() { Name = Localizer["Vacaciones"], Data = new List<HighCharts.Data>() },
                    new ChartViewModel.ComplexSerie() { Name = Localizer["Baja"], Data = new List<HighCharts.Data>() },
                    new ChartViewModel.ComplexSerie() { Name = Localizer["Ausencia"], Data = new List<HighCharts.Data>() },
                    new ChartViewModel.ComplexSerie() { Name = Localizer["Permiso"], Data = new List<HighCharts.Data>() }
                },
                DrillDowns = new List<ChartViewModel.ComplexSerie>() { }
            };

            for (int daynum = 1; daynum <= DateTime.DaysInMonth(year.Value, month.Value); daynum++)
            {
                Column1.ComplexSeries[0].Data.Add(new HighCharts.Data() { Name = daynum.ToString(), Y = 0 });
                Column1.ComplexSeries[1].Data.Add(new HighCharts.Data() { Name = daynum.ToString(), Y = 0, Drilldown = "V" + daynum.ToString() });
                Column1.ComplexSeries[2].Data.Add(new HighCharts.Data() { Name = daynum.ToString(), Y = 0 });
                Column1.ComplexSeries[3].Data.Add(new HighCharts.Data() { Name = daynum.ToString(), Y = 0, Drilldown = "A" + daynum.ToString() });
                Column1.ComplexSeries[4].Data.Add(new HighCharts.Data() { Name = daynum.ToString(), Y = 0 });

                Column1.DrillDowns.Add(new ChartViewModel.ComplexSerie()
                {
                    Name = "V" + daynum.ToString(),
                    Data = new List<HighCharts.Data>()
                    {
                        new HighCharts.Data() { Name = Localizer["Vacaciones"], Y = 0 },
                        new HighCharts.Data() { Name = Localizer["Vacaciones A�o Anterior"], Y = 0 },
                        new HighCharts.Data() { Name = Localizer["Libre Disposici�n"], Y = 0 }
                    }
                });

                Column1.DrillDowns.Add(new ChartViewModel.ComplexSerie()
                {
                    Name = "A" + daynum.ToString(),
                    Data = new List<HighCharts.Data>()
                    {
                        new HighCharts.Data() { Name = Localizer["Ausencia injustificada"], Y = 0 },
                        new HighCharts.Data() { Name = Localizer["Horas Sindicales"], Y = 0 },
                        new HighCharts.Data() { Name = Localizer["Visita M�dica"], Y = 0 }
                    }
                });

                foreach (ScheduleDate sd in scheduleDates.Where(sd => sd.Day == daynum))
                {
                    switch (sd.Acronym)
                    {
                        case "T":
                            Column1.ComplexSeries[0].Data.Last().Y += sd.TotalHours;
                            break;
                        case "V":
                            Column1.ComplexSeries[1].Data.Last().Y += sd.TotalHours;
                            Column1.DrillDowns.ElementAt(Column1.DrillDowns.Count() - 2).Data[0].Y += sd.TotalHours;
                            break;
                        case "Y":
                            Column1.ComplexSeries[1].Data.Last().Y += sd.TotalHours;
                            Column1.DrillDowns.ElementAt(Column1.DrillDowns.Count() - 2).Data[1].Y += sd.TotalHours;
                            break;
                        case "L":
                            Column1.ComplexSeries[1].Data.Last().Y += sd.TotalHours;
                            Column1.DrillDowns.ElementAt(Column1.DrillDowns.Count() - 2).Data[2].Y += sd.TotalHours;
                            break;
                        case "B":
                            Column1.ComplexSeries[2].Data.Last().Y += sd.TotalHours;
                            break;
                        case "A":
                            Column1.ComplexSeries[3].Data.Last().Y += sd.TotalHours;
                            Column1.DrillDowns.ElementAt(Column1.DrillDowns.Count() - 1).Data[0].Y += sd.TotalHours;
                            break;
                        case "S":
                            Column1.ComplexSeries[3].Data.Last().Y += sd.TotalHours;
                            Column1.DrillDowns.ElementAt(Column1.DrillDowns.Count() - 1).Data[1].Y += sd.TotalHours;
                            break;
                        case "M":
                            Column1.ComplexSeries[3].Data.Last().Y += sd.TotalHours;
                            Column1.DrillDowns.ElementAt(Column1.DrillDowns.Count() - 1).Data[2].Y += sd.TotalHours;
                            break;
                        case "P":
                            Column1.ComplexSeries[4].Data.Last().Y += sd.TotalHours;
                            break;
                        default:
                            return Error();
                    }
                }
            }

                #endregion
                #region UPPERCARDS

            double TotalHours = Math.Max(Column1.ComplexSeries.SelectMany(s => s.Data).Select(d => d.Y).Sum(), 1);
            double WorkHours = Column1.ComplexSeries[0].Data.Select(d => d.Y).Sum();
            double HolydayHours = Column1.ComplexSeries[1].Data.Select(d => d.Y).Sum();
            double SickHours = Column1.ComplexSeries[2].Data.Select(d => d.Y).Sum();
            double AbsenteeismHours = Column1.ComplexSeries[3].Data.Select(d => d.Y).Sum();
            double SparedHours = Column1.ComplexSeries[4].Data.Select(d => d.Y).Sum();
            double ExtraHours = scheduleDates.SelectMany(sd => sd.ScheduleDateTerm).Where(sdt => sdt.HoursType == WorkHoursType.Extra).Select(sdt => sdt.Hours).Sum();

            CardData UpperCard1 = new CardData()
            {
                Label = Localizer["Trabajo"],
                MainValue = Math.Round(WorkHours / TotalHours * 100, 2).ToString(),
                SecondaryValue = WorkHours.ToString() + " h",
                BarValue = (int)(WorkHours / TotalHours * 100)
            };
            CardData UpperCard2 = new CardData()
            {
                Label = Localizer["Horas Extra"],
                MainValue = Math.Round(ExtraHours / TotalHours * 100, 2).ToString(),
                SecondaryValue = ExtraHours.ToString() + " h",
                BarValue = Math.Min((int)(ExtraHours / TotalHours * 100 * 4), 100)
            };
            CardData UpperCard3 = new CardData()
            {
                Label = Localizer["Ausencias"],
                MainValue = Math.Round(AbsenteeismHours / TotalHours * 100, 2).ToString(),
                SecondaryValue = AbsenteeismHours.ToString() + " h",
                BarValue = Math.Min((int)(AbsenteeismHours / TotalHours * 100 * 4), 100)
            };
            CardData UpperCard4 = new CardData()
            {
                Label = Localizer["Permiso Especial"],
                MainValue = Math.Round(SparedHours / TotalHours * 100, 2).ToString(),
                SecondaryValue = SparedHours.ToString() + " h",
                BarValue = Math.Min((int)(SparedHours / TotalHours * 100 * 4), 100)
            };
            CardData UpperCard5 = new CardData()
            {
                Label = Localizer["Baja M�dica"],
                MainValue = Math.Round(SickHours / TotalHours * 100, 2).ToString(),
                SecondaryValue = SickHours.ToString() + " h",
                BarValue = Math.Min((int)(SickHours / TotalHours * 100 * 4), 100)
            };
            CardData UpperCard6 = new CardData()
            {
                Label = Localizer["Vacaciones"],
                MainValue = Math.Round(HolydayHours / TotalHours * 100, 2).ToString(),
                SecondaryValue = HolydayHours.ToString() + " h",
                BarValue = Math.Min((int)(HolydayHours / TotalHours * 100 * 4), 100)
            };

                #endregion
                #region CARDSCHART

            IEnumerable<User> ActiveUsers = users.Where(u => u.Active = true);
            IEnumerable<User> ExternalUsers = ActiveUsers.Where(u => u.UserContract.Any(uc => uc.Active && uc.External));
            IEnumerable<User> AreasInternalUsers = users.Where(u => u.UserContract.Any(uc => uc.Active && uc.External == false));
            IEnumerable<User> PermanentEmployees = ActiveUsers.Where(u => u.UserContract.Any(uc => uc.Active && uc.ContractPeriod == ContractPeriodType.Permanent));

            double MonthRegistrations = AreasInternalUsers.SelectMany(u => u.UserContract).Where(uc => uc.StartDate >= DateTime.Now.AddMonths(-1)).Count();
            double MonthWithdrawals = AreasInternalUsers.SelectMany(u => u.UserContract).Where(uc => uc.EndDate >= DateTime.Now.AddMonths(-1)).Count();
            double MonthAverageEmployees = ((ActiveUsers.Count() - MonthRegistrations + MonthWithdrawals) + ActiveUsers.Count()) / 2;
            double MonthStaffTurnover = (((MonthRegistrations + MonthWithdrawals) / 2) * 100) / MonthAverageEmployees;

            CardData ChartCard1 = new CardData()
            {
                Label = Localizer["Improductividad"],
                MainValue = "27%",
                BarValue = 27
            };
            CardData ChartCard2 = new CardData()
            {
                Label = Localizer["Absentismo"],
                MainValue = Math.Round(AbsenteeismHours / TotalHours * 100, 2).ToString() + "%",
                BarValue = (int)Math.Round(AbsenteeismHours / TotalHours * 100, 2)
            };
            CardData ChartCard3 = new CardData()
            {
                Label = Localizer["Rotaci�n"],
                MainValue = Math.Round(MonthStaffTurnover, 2).ToString() + "%",
                BarValue = (int)MonthStaffTurnover
            };
            CardData ChartCard4 = new CardData()
            {
                Label = Localizer["Personal Externo"],
                MainValue = Math.Round((double)ExternalUsers.Count() / ActiveUsers.Count() * 100, 2).ToString() + "%",
                BarValue = (int)Math.Round((double)ExternalUsers.Count() / ActiveUsers.Count() * 100, 2)
            };
            CardData ChartCard5 = new CardData()
            {
                Label = Localizer["Personal Indefinido"],
                MainValue = Math.Round((double)PermanentEmployees.Count() / ActiveUsers.Count() * 100, 2).ToString() + "%",
                BarValue = (int)Math.Round((double)PermanentEmployees.Count() / ActiveUsers.Count() * 100, 2)
            };

            #endregion
            
            model.BarChart1 = Bars1;
            model.ColumnChart1 = Column1;
            model.CardsChart1 = new List<CardData>() { ChartCard1, ChartCard2, ChartCard3, ChartCard4, ChartCard5 };
            model.UpperCards = new List<CardData>() { UpperCard1, UpperCard2, UpperCard3, UpperCard4, UpperCard5, UpperCard6 };
            
            #endregion

            return View("ShowDashboard", model);
        }

        [HttpGet]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        public IActionResult Edit(EditScheduleDateViewModel model)
        {
            bool Multiple = false;
            Dictionary<string, List<int>> UserDates = new Dictionary<string, List<int>>();
            List<int> DatesIds = new List<int>();
            try
            {
                DatesIds = JsonConvert.DeserializeObject<List<int>>(model.Ids);
            }
            catch { }
            try
            {
                UserDates = JsonConvert.DeserializeObject<Dictionary<string, List<int>>>(model.Days);
            }
            catch { }
            if ((UserDates.Count + DatesIds.Count) > 1)
            {
                Multiple = true;
            }
            else
            {
                if (UserDates.Any()) {
                    User user = UserService.Get(Int32.Parse(UserDates.First().Key));
                    model.User = user;
                    if (UserDates.First().Value.Count > 1)
                        Multiple = true;
                }
            }
            if (!Multiple && DatesIds.Any())
            {
                ScheduleDate scheduleDate = ScheduleService.Get(DatesIds.First());
                model.User = scheduleDate.User;
                if (scheduleDate != null)
                {
                    if (scheduleDate.ScheduleDateTerm != null) model.Terms = scheduleDate.ScheduleDateTerm.OrderByDescending(t=>t.Hours).ToList();
                    model.Comment = scheduleDate.Comment;
                    UserDates.Add(scheduleDate.UserId.ToString(), new List<int>{scheduleDate.Day});
                }
            }
            model.Multiple = Multiple;
            model.DatesIds = DatesIds;
            model.UserDates = UserDates;
            var areas = AuthUser.OwnedAreas;
            model.ProductiveJobs = AuthUser.OwnedJobs.Where(x=>x.Type == JobType.Productive).ToList();
            model.GeneralJobs = ScheduleService.GetGeneralJobs(model.Year).ToList();
            return PartialView("_Edit", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        public JsonResult Edit([FromBody] dynamic data)
        {
            List<Dictionary<string, string>> arr_response = new List<Dictionary<string, string>>();
            int Year = data.year.ToObject<int>();
            int Month = data.month.ToObject<int>();
            string Acronym = data.acronym.ToObject<string>();
            string Comment = data.comment.ToObject<string>();
            double TotalHours = data.hours.ToObject<double>();
            bool Multiple = data.multiple.ToObject<bool>();


            List<int> Ids = data.ids.ToObject<List<int>>();
            Dictionary<string, List<int>> Days = data.days.ToObject<Dictionary<string, List<int>>>();

            List<Dictionary<string, dynamic>> Add = data.add.ToObject<List<Dictionary<string,dynamic>>>();
            List<Dictionary<string, dynamic>> Edit = data.edit.ToObject<List<Dictionary<string, dynamic>>>();
            List<Dictionary<string, dynamic>> Remove = data.remove.ToObject<List<Dictionary<string, dynamic>>>();

            if ((Add.Count + Edit.Count + Remove.Count) == 0) return Json(new JsonResponse() { Result = false, Response = "Se ha producido un error" });

            if (Ids.Any())
            {
                if (Multiple)
                {
                    if (Add.Count < 1)
                    {
                        List<ScheduleDate> scheduleDateList = ScheduleService.GetMany(x=> Ids.Contains(x.Id)).ToList();
                        ScheduleService.RemoveRange(scheduleDateList);
                        foreach (var item in scheduleDateList)
                        {
                            arr_response.Add(new Dictionary<string, string> {
                                { "user_id",item.UserId.ToString()},
                                { "day", item.Day.ToString()},
                                { "id", ""},
                                { "total_hours", ""},
                                { "acronym", ""}
                            });
                        }
                    }
                    else
                    {
                        List<ScheduleDate> scheduleDateList = new List<ScheduleDate>();
                        foreach (var id in Ids)
                        {
                            ScheduleDate scheduleDate = ScheduleService.Get(id);
                            scheduleDate.Acronym = data.acronym;
                            scheduleDate.Comment = data.comment;
                            scheduleDate.TotalHours = data.hours;
                            scheduleDate.WorkingHours = 0;
                            scheduleDate.OtherHours = 0;

                            ScheduleService.RemoveTermRange(scheduleDate.ScheduleDateTerm);

                            foreach (var term in Add)
                            {
                                Enum.TryParse(term["hours_type"], out WorkHoursType hoursType);
                                ScheduleDateTerm scheduleDateTerm = new ScheduleDateTerm
                                {
                                    Pattern = term["pattern"],
                                    Hours = term["hours"],
                                    HoursType = hoursType
                                };

                                if (hoursType == WorkHoursType.Normal)
                                {
                                    scheduleDate.WorkingHours += term["hours"];
                                }
                                else
                                {
                                    scheduleDate.OtherHours += term["hours"];
                                }

                                var JobIds = term["job_id"].ToObject<List<int>>();
                                foreach (var jobid in JobIds)
                                {
                                    scheduleDateTerm.ScheduleDateTermJob.Add(new ScheduleDateTermJob
                                    {
                                        Job = JobService.Get(jobid)
                                    });
                                }
                                scheduleDate.ScheduleDateTerm.Add(scheduleDateTerm);
                            }
                            scheduleDateList.Add(scheduleDate);
                        }
                        ScheduleService.UpdateRange(scheduleDateList);
                        foreach (var item in scheduleDateList)
                        {
                            arr_response.Add(new Dictionary<string, string> {
                                { "user_id",item.UserId.ToString()},
                                { "day", item.Day.ToString()},
                                { "id", item.Id.ToString()},
                                { "total_hours", item.TotalHours.ToString()},
                                { "acronym", item.Acronym},
                                { "comment", item.Comment.Length > 1 ? "true" : "false" }
                            });
                        }
                    }
                }
                else
                {
                    ScheduleDate scheduleDate = ScheduleService.Get(Ids.First());
                    scheduleDate.Acronym = data.acronym;
                    scheduleDate.Comment = data.comment;
                    scheduleDate.TotalHours = data.hours;
                    scheduleDate.WorkingHours = 0;
                    scheduleDate.OtherHours = 0;
                    if (Remove.Any())
                    {
                        List<ScheduleDateTerm> scheduleDateTermList = new List<ScheduleDateTerm>();
                        foreach (var item in Remove)
                        {
                            var term = scheduleDate.ScheduleDateTerm.FirstOrDefault(t => t.Id == (int)item["term_id"]);
                            if (term.Id > 0) scheduleDateTermList.Add(term);
                        }
                        ScheduleService.RemoveTermRange(scheduleDateTermList);
                    }
                    if (Edit.Any())
                    {
                        List<ScheduleDateTerm> scheduleDateTermList = new List<ScheduleDateTerm>();
                        foreach (var term in Edit)
                        {
                            if (term["hours"] > 0)
                            {
                                Enum.TryParse(term["hours_type"], out WorkHoursType hoursType);
                                ScheduleDateTerm scheduleDateTerm = ScheduleService.GetTerm((int)term["term_id"]);

                                scheduleDateTerm.Pattern = term["pattern"];
                                scheduleDateTerm.Hours = term["hours"];
                                scheduleDateTerm.HoursType = hoursType;
                                
                                if (hoursType == WorkHoursType.Normal)
                                {
                                    scheduleDate.WorkingHours += term["hours"];
                                }
                                else
                                {
                                    scheduleDate.OtherHours += term["hours"];
                                }

                                List<int> JobIds = term["job_id"].ToObject<List<int>>();
                                foreach (var jobid in JobIds)
                                {
                                    if (!scheduleDateTerm.ScheduleDateTermJob.Any(x => x.JobId == jobid))
                                    {
                                        scheduleDateTerm.ScheduleDateTermJob.Add(new ScheduleDateTermJob
                                        {
                                            Job = JobService.Get(jobid)
                                        });
                                    }
                                }
                                for (int i = 0; i < scheduleDateTerm.ScheduleDateTermJob.Count; i++)
                                {
                                    ScheduleDateTermJob termJobToDelete = scheduleDateTerm.ScheduleDateTermJob.ElementAt(i);
                                    if (!JobIds.Any(a => a == termJobToDelete.JobId))
                                    {
                                        ScheduleService.RemoveTermJob(termJobToDelete);
                                    }
                                }
                                foreach (var jobid in JobIds)
                                {
                                    if (!scheduleDateTerm.ScheduleDateTermJob.Any(x => x.JobId == jobid))
                                        scheduleDateTerm.ScheduleDateTermJob.Add(new ScheduleDateTermJob
                                        {
                                            Job = JobService.Get(jobid)
                                        });
                                }
                                ScheduleService.UpdateTerm(scheduleDateTerm);
                            }
                            else
                            {
                                ScheduleDateTerm termToRemove = ScheduleService.GetTerm((int)term["term_id"]);
                                ScheduleService.RemoveTerm(termToRemove);
                            }
                        }
                        arr_response.Add(new Dictionary<string, string> {
                            { "user_id",scheduleDate.UserId.ToString()},
                            { "day", scheduleDate.Day.ToString()},
                            { "id", scheduleDate.Id.ToString()},
                            { "total_hours", scheduleDate.TotalHours.ToString()},
                            { "acronym", scheduleDate.Acronym},
                            { "comment", scheduleDate.Comment.Length > 1 ? "true" : "false" }
                        });
                    }
                    if (Add.Any())
                    {
                        foreach (var term in Add)
                        {
                            if (term["hours"] > 0)
                            {
                                Enum.TryParse(term["hours_type"], out WorkHoursType hoursType);
                                ScheduleDateTerm scheduleDateTerm = new ScheduleDateTerm
                                {
                                    Pattern = term["pattern"],
                                    Hours = term["hours"],
                                    HoursType = hoursType
                                };

                                if (hoursType == WorkHoursType.Normal)
                                {
                                    scheduleDate.WorkingHours += term["hours"];
                                }
                                else
                                {
                                    scheduleDate.OtherHours += term["hours"];
                                }

                                var JobIds = term["job_id"].ToObject<List<int>>();
                                foreach (var jobid in JobIds)
                                {
                                    scheduleDateTerm.ScheduleDateTermJob.Add(new ScheduleDateTermJob
                                    {
                                        Job = JobService.Get(jobid)
                                    });
                                }
                                scheduleDate.ScheduleDateTerm.Add(scheduleDateTerm);
                            }
                        }
                        ScheduleService.Update(scheduleDate);

                        arr_response.Add(new Dictionary<string, string> {
                            { "user_id",scheduleDate.UserId.ToString()},
                            { "day", scheduleDate.Day.ToString()},
                            { "id", scheduleDate.Id.ToString()},
                            { "total_hours", scheduleDate.TotalHours.ToString()},
                            { "acronym", scheduleDate.Acronym},
                            { "comment", scheduleDate.Comment.Length > 1 ? "true" : "false" }
                        });
                    }

                    if (scheduleDate.ScheduleDateTerm.Count < 1 && scheduleDate.Comment == "")
                    {
                        ScheduleService.Remove(scheduleDate);
                        arr_response.Add(new Dictionary<string, string> {
                            { "user_id",scheduleDate.UserId.ToString()},
                            { "day", scheduleDate.Day.ToString()},
                            { "id", ""},
                            { "total_hours", ""},
                            { "acronym", ""}
                        });
                    }
                    else
                    {
                        arr_response.Add(new Dictionary<string, string> {
                            { "user_id",scheduleDate.UserId.ToString()},
                            { "day", scheduleDate.Day.ToString()},
                            { "id", scheduleDate.Id.ToString()},
                            { "total_hours", scheduleDate.TotalHours.ToString()},
                            { "acronym", scheduleDate.Acronym },
                            { "comment", scheduleDate.Comment.Length > 1 ? "true" : "false" }
                        });
                    }

                }
            }

            if (Days.Any())
            {
                List<ScheduleDate> scheduleDateList = new List<ScheduleDate>();
                foreach (var userdays in Days)
                {
                    int UserId = int.Parse(userdays.Key);
                    foreach (var Day in userdays.Value)
                    {
                        User user = UserService.Get(UserId);
                        ScheduleDate scheduleDate = new ScheduleDate
                        {
                            Year = Year,
                            Month = Month,
                            Day = Day,
                            Acronym = Acronym,
                            Comment = Comment,
                            TotalHours = TotalHours,
                            User = user
                        };
                        List<ScheduleDateTerm> scheduleDateTermList = new List<ScheduleDateTerm>();
                        foreach (var term in Add)
                        {
                            Enum.TryParse(term["hours_type"], out WorkHoursType hoursType);
                            ScheduleDateTerm scheduleDateTerm = new ScheduleDateTerm {
                                Pattern = term["pattern"],
                                Hours = term["hours"],
                                HoursType = hoursType
                            };

                            if (hoursType == WorkHoursType.Normal)
                            {
                                scheduleDate.WorkingHours += term["hours"];
                            }
                            else
                            {
                                scheduleDate.OtherHours += term["hours"];
                            }

                            var JobIds = term["job_id"].ToObject<List<int>>();
                            foreach (var jobid in JobIds)
                            {
                                scheduleDateTerm.ScheduleDateTermJob.Add(new ScheduleDateTermJob {
                                    Job = JobService.Get(jobid)
                                });
                            }
                            scheduleDateTermList.Add(scheduleDateTerm);
                        }
                        scheduleDate.ScheduleDateTerm = scheduleDateTermList;
                        scheduleDateList.Add(scheduleDate);
                    }
                }
                ScheduleService.AddRange(scheduleDateList);
                foreach (var item in scheduleDateList)
                {
                    arr_response.Add(new Dictionary<string, string> {
                        { "user_id",item.UserId.ToString()},
                        { "day", item.Day.ToString()},
                        { "id", item.Id.ToString()},
                        { "total_hours", item.TotalHours.ToString()},
                        { "acronym", item.Acronym},
                        { "comment", item.Comment.Length > 1 ? "true" : "false" }
                });
                }
            }

            return Json(new JsonResponse() { Result = true, Response = "OK", Content = arr_response });
        }

        [HttpGet]
        [Route("~/[controller]/[action]/{userId}/{year?}")]
        [AuthorizeRoles(AppRole.OperationalManagementEdit, AppRole.OperationalManagementShow)]
        public IActionResult ShowUser(int userId, int? year = null)
        {
            if (year == null) year = DateTime.Now.Year;
            List<ScheduleDate> scheduleDate = ScheduleService.GetUserSchedule(year.Value, userId).ToList();
            List<Job> generalJobs = ScheduleService.GetGeneralJobs(year.Value).ToList();
            if (generalJobs.Count() > 0)
            {
                generalJobs.Add(new Job()
                {
                    Acronym = "T",
                    Name = Localizer["Trabajo"],
                    Description = Localizer["Trabajo"]
                });
            }

            List<dynamic> data = new List<dynamic>();
            foreach (ScheduleDate item in scheduleDate)
            {
                data.Add(new
                {
                    id = item.Id,
                    hours = item.TotalHours,
                    acronym = item.Acronym,
                    year = item.Year,
                    month = item.Month,
                    day = item.Day,
                    comment = item.Comment,
                    workinghours = item.WorkingHours,
                    otherhours = item.OtherHours
                });
            }
            return Json(new { data, jobs = generalJobs });
        }

        [HttpGet]
        [Route("~/[controller]/[action]/{id}")]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        public IActionResult GenerateConfirmation(int id)
        {
            GenerateScheduleViewModel Model = new GenerateScheduleViewModel();
            Model.UserId = id;
            Model.ProductiveJobs = AuthUser.OwnedJobs.Where(x => x.Type == JobType.Productive).ToList();
            return PartialView("_Generate", Model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateAjaxModelState]
        [Route("~/[controller]/[action]")]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        public IActionResult Generate(GenerateScheduleViewModel Model)
        {
            List<DayType> hollidays = new List<DayType>() { DayType.NationalHoliday, DayType.RegionalHoliday, DayType.LocalHoliday };

            User user = UserService.Get(Model.UserId);
            UserShift lastUserShift = user.LastUserShift ?? new UserShift();
            List<CalendarDay> officeCalendar = user.LastUserOffice == null ? new List<CalendarDay>() : user.LastUserOffice.Office.Calendar.Days.ToList();

            Dictionary<int, string> patterns = new Dictionary<int, string>();
            lastUserShift.Shift.ToList().ForEach(i =>
            {
                patterns.Add(i.WeekDay, i.Pattern);
            });
            int year = lastUserShift.StartDate.Year;
            int month = lastUserShift.StartDate.Month;
            int day = lastUserShift.StartDate.Day;

            IEnumerable<ScheduleDate> firstMonth = user.ScheduleDate.Where(x => x.Year == year && x.Month == month && x.Day >= day);
            IEnumerable<ScheduleDate> restOfMonths = user.ScheduleDate.Where(x => x.Year == year && x.Month > month);
            IEnumerable<ScheduleDate> scheduleDates = firstMonth.Concat(restOfMonths);
            ScheduleService.DeleteRange(scheduleDates);

            Job job = JobService.Get(Model.JobId);
            List<ScheduleDate> scheduleDateList = new List<ScheduleDate>();

            for (int m = month; m <= 12; m++)
            {
                int firstDay = m == month ? day : 1;
                int days = DateTime.DaysInMonth(year, m);
                for (int d = firstDay; d <= days; d++)
                {
                    int dayOfWeek = (int)(new DateTime(year, m, d).AddDays(-1).DayOfWeek);
                    if (patterns.ContainsKey(dayOfWeek))
                    {
                        string pattern = patterns.Single(p => p.Key == dayOfWeek).Value;
                        int hours = pattern.Count(x => x == '1') / 2;

                        CalendarDay calendarDay = officeCalendar.SingleOrDefault(cd => cd.Year == year && cd.Month == m && cd.Day == d);
                        
                        if (calendarDay == null  || !(hollidays.Contains(calendarDay.Type)))
                        {
                            ScheduleDateTerm scheduleDateTerm = new ScheduleDateTerm()
                            {
                                Pattern = pattern,
                                Hours = hours,
                                HoursType = WorkHoursType.Normal
                            };
                            scheduleDateTerm.ScheduleDateTermJob.Add(new ScheduleDateTermJob() { Job = job });
                            ScheduleDate scheduleDate = new ScheduleDate()
                            {
                                User = user,
                                Year = year,
                                Month = m,
                                Day = d,
                                TotalHours = hours,
                                WorkingHours = hours,
                                Acronym = "T"
                            };

                            scheduleDate.ScheduleDateTerm.Add(scheduleDateTerm);
                            scheduleDateList.Add(scheduleDate);
                        }
                    }
                }
            }

            JsonResponse response = new JsonResponse() { Result = false, Response = "Se ha producido un error" };
            if (ScheduleService.AddRange(scheduleDateList)>0)
            {
                response = new JsonResponse() { Result = true, Response = "El cambio se ha realizado con �xito" };
            }
            return Json(response);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("~/[controller]/[action]/{requestid:int}/{option}")]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        public IActionResult ValidateRequest(int requestid, string option)
        {
            Request request = RequestService.Get(requestid);
            if (request == null) return Error(StatusCodes.Status406NotAcceptable);
            if (request.AdditionalData == null) return Error(StatusCodes.Status406NotAcceptable);

            JObject AdditionalDataObject = JObject.Parse(request.AdditionalData);
            List<DateTime> RequestDays = new List<DateTime>();
            string optionName = "";
            if (option == "main")
            {
                foreach (JToken dateString in AdditionalDataObject["DaysMain"].ToArray())
                {
                    DateTime date = DateTime.Parse((string)dateString);
                    RequestDays.Add(date);
                    optionName = Localizer["Primera opci�n"];
                }
            }
            else if (option == "second")
            {
                foreach (JToken dateString in AdditionalDataObject["DaysSecond"].ToArray())
                {
                    DateTime date = DateTime.Parse((string)dateString);
                    RequestDays.Add(date);
                    optionName = Localizer["Segunda opci�n"];
                }
            }
            else { return Error(StatusCodes.Status406NotAcceptable); }
            if(RequestDays.Count < 1) return Error(StatusCodes.Status406NotAcceptable);


            int jobid = 0;
            string acronym = "";
            switch (request.Type)
            {
                case RequestType.Hollydays:
                    jobid = 17;
                    acronym = "V";
                    break;
                case RequestType.SpecialLeave:
                    jobid = 26;
                    acronym = "P";
                    break;
                case RequestType.CompensationHours:
                    jobid = 20;
                    acronym = "L";
                    break;
                default: return Error(StatusCodes.Status406NotAcceptable);
            }
            Job job = JobService.Get(jobid);

            User user = request.User;
            UserShift lastUserShift = user.LastUserShift ?? new UserShift();
            Dictionary<int, string> patterns = new Dictionary<int, string>();
            lastUserShift.Shift.ToList().ForEach(i =>
            {
                patterns.Add(i.WeekDay, i.Pattern);
            });

            IEnumerable<ScheduleDate> scheduleDatesToDelete = new List<ScheduleDate>();
            List<ScheduleDate> scheduleDatesToAdd = new List<ScheduleDate>();

            foreach (DateTime date in RequestDays)
            {
                scheduleDatesToDelete = scheduleDatesToDelete.Concat(user.ScheduleDate.Where(x => x.Year == date.Year && x.Month == date.Month && x.Day == date.Day));

                int dayOfWeek = (int)(date.AddDays(-1).DayOfWeek);
                if (patterns.ContainsKey(dayOfWeek))
                {
                    string pattern = patterns.Single(p => p.Key == dayOfWeek).Value;
                    int hours = pattern.Count(x => x == '1') / 2;
                    ScheduleDateTerm scheduleDateTerm = new ScheduleDateTerm()
                    {
                        Pattern = pattern,
                        Hours = hours,
                        HoursType = WorkHoursType.Normal
                    };
                    scheduleDateTerm.ScheduleDateTermJob.Add(new ScheduleDateTermJob() { Job = job });
                    ScheduleDate scheduleDate = new ScheduleDate()
                    {
                        User = user,
                        Year = date.Year,
                        Month = date.Month,
                        Day = date.Day,
                        TotalHours = hours,
                        WorkingHours = hours,
                        Acronym = acronym
                    };
                    scheduleDate.ScheduleDateTerm.Add(scheduleDateTerm);
                    scheduleDatesToAdd.Add(scheduleDate);
                }
            }
            int result = ScheduleService.DeleteRange(scheduleDatesToDelete);
            result = result > 0 ? ScheduleService.AddRange(scheduleDatesToAdd) : result;
            
            if (result > 0)
            {
                request.RequestHistory.Add(new RequestHistory
                {
                    EntryDate = DateTime.Now,
                    State = RequestState.Accepted,
                    Comment = optionName + " " + Localizer["aplicada en calendario"],
                    User = AuthUser
                });
                if (RequestService.Update(request) > 0)
                {
                    return Json(new JsonResponse() { Result = true, Response = "El cambio se ha realizado con �xito" });
                }
            }
            return Json(new JsonResponse() { Result = false, Response = "Se ha producido un error" });
        }

    }
}
