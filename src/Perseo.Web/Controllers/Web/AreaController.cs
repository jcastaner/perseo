using System.Collections.Generic;
using System.Linq;
using Perseo.Core;
using Microsoft.AspNetCore.Mvc;
using Perseo.Web.Models;
using Perseo.Web.Helpers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Authorization;
using System;
using Microsoft.Extensions.Localization;
using AutoMapper;

namespace Perseo.Web.Controllers
{
    [Route("[controller]/[action]/{id:int?}")]
    public class AreaController : BaseController
    {

        #region CONSTRUCTOR

        private readonly IAreaService AreaService;
        private readonly IJobService JobService;
        private readonly IFileService FileService;
        private readonly IStaffingService StaffingService;

        public AreaController(
            IHttpContextAccessor contextAccessor,
            IHostingEnvironment enviroment,
            IUserService userService,
            IAreaService areaService,
            IStringLocalizer<SharedResources> localizer,
            IJobService jobService,
            IFileService fileService,
            IStaffingService staffingService,
            IMapper mapper) : base(contextAccessor, enviroment, userService, localizer, mapper)
        {
            AreaService = areaService;
            JobService = jobService;
            FileService = fileService;
            StaffingService = staffingService;
        }

        #endregion

        #region HELPERS               

        private AreaViewModel MapAreaToModel(Area area)
        {
            User owner = area.Owner ?? new User();
            Area parent = area.Parent ?? new Area();
            AreaCode code = area.Code ?? new AreaCode();
            AreaCodeWbs wbs = area.Wbs ?? new AreaCodeWbs();
            AreaViewModel model = new AreaViewModel
            {
                Id = area.Id,
                Name = area.Name,
                Description = area.Description,
                Type = area.Type,
                Active = area.Active,
                StartDate = area.StartDate,
                EndDate = area.EndDate,
                Owner = owner.FullName,
                OwnerId = owner.Id,
                Parent = parent.Name,
                ParentId = area.ParentId,
                Code = code.Name,
                CodeId = code.Id,
                FullCode = area.FullCode,
                Wbs = wbs.Name,
                WbsId = wbs.Id,
                FullWbs = area.FullWbs,
                Jobs = area.Job
            };
            return model;
        }

        private bool Authorize(Area area)
        {
            if (area == null) { AddError(StatusCodes.Status404NotFound); return false; }
            if (!(AuthUser.OwnedAreas.Any(a => a.Id == area.Id))) { AddError(StatusCodes.Status401Unauthorized); return false; }
            return true;
        }

        private bool AuthorizeAjax(Area area)
        {
            if (!IsAjax) { AddError(StatusCodes.Status400BadRequest); return false; }
            return Authorize(area);
        }

        #endregion

        #region CRUD

        #region List

        [HttpGet]
        [Route("/[controller]")]
        [AuthorizeRoles(AppRole.OperationalManagementEdit, AppRole.OperationalManagementShow)]
        public IActionResult Index()
        {
            return List();
        }

        [HttpGet]
        [Route("/[controller]/[action]/{active?}/{selectedtypes?}")]
        [AuthorizeRoles(AppRole.OperationalManagementEdit, AppRole.OperationalManagementShow)]
        public IActionResult List(ActiveType? active = null, string selectedtypes = null)
        {
            List<Area> authUserOwnedAreas = AuthUser.OwnedAreas.ToList();
            if (authUserOwnedAreas.Count == 1)
            {
                return Show(authUserOwnedAreas.FirstOrDefault().Id);
            }
            else
            {
                List<int> areaFilterIds = authUserOwnedAreas.Select(x=>x.Id).ToList();

                List<AreaType> types = selectedtypes != null 
                    ? selectedtypes.Split(";").Select(s => (AreaType)int.Parse(s)).ToList() 
                    : Enum.GetValues(typeof(AreaType)).Cast<AreaType>().Where(at => at != AreaType.General).ToList();

                PagedResult<AreaListNode> pagedResult = AreaService.ListPaged(areaFilterIds, active, types);

                List<AreaListNode> UpperAreas = pagedResult.Results.ToList();
                foreach (AreaListNode aln in pagedResult.Results)
                {
                    string idInLineage = "-" + aln.Id + "-";
                    if (UpperAreas.Any(a => a.Lineage.Contains(idInLineage)))
                    {
                        UpperAreas.RemoveAll(a => a.Lineage.Contains(idInLineage));
                        UpperAreas.Add(aln);
                    }
                }

                List<List<AreaListNode>> UpperTrees = new List<List<AreaListNode>>();
                foreach (AreaListNode aln in UpperAreas)
                {
                    List<AreaListNode> AreaTree = AreaService.GetTree(aln.Id).ToList();
                    UpperTrees.Add(AreaTree);
                }

                ViewBag.UpperTrees = UpperTrees;

                AreaListViewModel model = new AreaListViewModel
                {
                    Data = Mapper.Map<List<AreaListNodeModel>>(pagedResult.Results.ToList()),
                    Pagination = Mapper.Map<PaginationModel>(pagedResult),
                    OwnedAreas = Mapper.Map<List<AreaModel>>(authUserOwnedAreas),
                    Active = active,
                    AreaTypes = types
                };

                return View("List", model);
            }
        }

        [HttpGet]
        [Route("/[controller]/[action]/{active?}/{selectedtypes?}")]
        [AuthorizeRoles(AppRole.OperationalManagementEdit, AppRole.OperationalManagementShow)]
        public JsonResult ListData(DataTablesRequest request, ActiveType? active = null, string selectedtypes = null )
        {

            List<Area> authUserOwnedAreas = AuthUser.OwnedAreas.ToList();
            List<int> areaFilterIds = authUserOwnedAreas.Select(x => x.Id).ToList();

            List<AreaType> types = null;
            if (selectedtypes != null)
            {
                try
                {
                    types = selectedtypes.Split(";").Select(s => (AreaType)Int32.Parse(s)).ToList();
                }
                catch (Exception)
                {
                    return null;
                }
            }
            else
            {
                types = Enum.GetValues(typeof(AreaType)).Cast<AreaType>().ToList();
                types.Remove(AreaType.General);
            }

            List<string> fields = new List<string> { "Name", "Description", "Type", "OwnerFullName", "FullCode" };
            var orderColumn = Convert.ToInt32(request.order[0]["column"]);
            var orderField = orderColumn > 0 ? fields[orderColumn - 1] : null;
            var orderDirection = request.order[0]["dir"] == "asc" ? " ASC" : " DESC";
            var order = orderField != null ? orderField + orderDirection : "Lineage ASC, Depth ASC";
  
            var pagedResult = AreaService.ListPaged(areaFilterIds, active, types, request.page, request.pageSize);

            DataTablesResponse response = new DataTablesResponse(request.draw, pagedResult.RowCount, pagedResult.FilteredRowCount);

            pagedResult.Results.ToList().ForEach(area =>
            {
                response.Add(
                    area.Id,
                    area.Name,
                    area.Description,
                    area.Type.GetDisplayName(),
                    area.OwnerFullName,
                    area.Users,
                    area.FullCode,
                    area.Lineage,
                    area.Depth
                    );
            });

            return Json(response);
        }

        #endregion

        #region Show

        [Route("~/[controller]/[action]/{id}")]
        [HttpGet]
        [AuthorizeRoles(AppRole.OperationalManagementEdit, AppRole.OperationalManagementShow)]
        public IActionResult Show(int id)
        {
            ViewBag.Id = id;
            ViewBag.Tab = "tab-main";
            Area area = AreaService.Get(id);
            if (area == null) return Error(StatusCodes.Status406NotAcceptable);
            if (!Authorize(area)) return Error();
            List<AreaListNode> FlatTree = AreaService.GetTree(id).ToList();
            ViewBag.FlatTree = FlatTree;
            AreaViewModel model = MapAreaToModel(area);
            model.Jobs = JobService.GetJobsFromAreas(new Area[] { area }).ToList();            
            model.OwnedUsers = UserService.GetUsersFromAreas(new int[] { id }).ToList();
            return View("Show", model);
        }

        [Route("~/[controller]/show/{id:int}/dashboard/{year:int?}/{month:int?}/{viewtype?}")]
        [HttpGet]
        [AuthorizeRoles(AppRole.OperationalManagementEdit, AppRole.OperationalManagementShow)]
        public IActionResult ShowDashboard(int id,int? year = null, int? month = null, string viewtype = null)
        {
            viewtype = viewtype ?? "month";
            year = year ?? DateTime.Now.Year;
            month = month ?? DateTime.Now.Month;

            ViewBag.ViewType = viewtype;

            ViewBag.Id = id;
            ViewBag.Tab = "tab-dashboard";
            ViewBag.Month = month.Value;
            ViewBag.Year = year.Value;

            List<Area> areas = AreaService.GetChildren(id).ToList();
            if (areas.Count() < 1) return Error(StatusCodes.Status406NotAcceptable);
            Area area = areas.Single(a => a.Id == id);
            if (!Authorize(area)) return Error();

            AreaViewModel model = MapAreaToModel(area);
            model.Jobs = areas.SelectMany(a => a.Job).ToList();
            model.OwnedUsers = areas.SelectMany(a => a.UserArea).Select(ua => ua.User).ToList();

            #region CHARTS

            ChartViewModel Pie1 = new ChartViewModel()
            {
                Name = Localizer["Recursos por Horario"],
                ComplexSeries = new List<ChartViewModel.ComplexSerie>() {
                    new ChartViewModel.ComplexSerie() { Data = new List<HighCharts.Data>(){
                        new HighCharts.Data() { Name = Localizer["Trabajo"], Y = 0 },
                        new HighCharts.Data() { Name = Localizer["Vacaciones"], Y = 0, Drilldown = "V" },
                        new HighCharts.Data() { Name = Localizer["Baja"], Y = 0 },
                        new HighCharts.Data() { Name = Localizer["Ausencia"], Y = 0, Drilldown = "A" },
                    } } },
                DrillDowns = new List<ChartViewModel.ComplexSerie>() {
                    new ChartViewModel.ComplexSerie() { Name = "V", Data = new List<HighCharts.Data>(){
                        new HighCharts.Data() { Name = Localizer["Vacaciones"], Y = 0 },
                        new HighCharts.Data() { Name = Localizer["Vacaciones A�o Anterior"], Y = 0 },
                        new HighCharts.Data() { Name = Localizer["Libre Disposici�n"], Y = 0 },
                        new HighCharts.Data() { Name = Localizer["Permiso Retribuido"], Y = 0 }
                    } },
                    new ChartViewModel.ComplexSerie() { Name = "A", Data = new List<HighCharts.Data>(){
                        new HighCharts.Data() { Name = Localizer["Ausencia Injustificada"], Y = 0 },
                        new HighCharts.Data() { Name = Localizer["Horas Sindicales"], Y = 0 },
                        new HighCharts.Data() { Name = Localizer["Visita M�dica"], Y = 0 }
                    } },
                }
            };

            List<ScheduleDate> scheduleDates = model.OwnedUsers.SelectMany(u => u.ScheduleDate).Where(sd => /*sd.Day == DateTime.Now.Day &&*/ (viewtype == "month" ? sd.Month == month.Value : true) && sd.Year == year.Value).ToList();
            foreach (ScheduleDate sd in scheduleDates)
            {
                switch (sd.Acronym)
                {
                    case "T":
                        Pie1.ComplexSeries.First().Data[0].Y += sd.TotalHours;
                        break;
                    case "V":
                        Pie1.ComplexSeries.First().Data[1].Y += sd.TotalHours;
                        Pie1.DrillDowns[0].Data[0].Y += sd.TotalHours;
                        break;
                    case "Y":
                        Pie1.ComplexSeries.First().Data[1].Y += sd.TotalHours;
                        Pie1.DrillDowns[0].Data[1].Y += sd.TotalHours;
                        break;
                    case "L":
                        Pie1.ComplexSeries.First().Data[1].Y += sd.TotalHours;
                        Pie1.DrillDowns[0].Data[2].Y += sd.TotalHours;
                        break;
                    case "P":
                        Pie1.ComplexSeries.First().Data[1].Y += sd.TotalHours;
                        Pie1.DrillDowns[0].Data[3].Y += sd.TotalHours;
                        break;
                    case "B":
                        Pie1.ComplexSeries.First().Data[2].Y += sd.TotalHours;
                        break;
                    case "A":
                        Pie1.ComplexSeries.First().Data[3].Y += sd.TotalHours;
                        Pie1.DrillDowns[1].Data[0].Y += sd.TotalHours;
                        break;
                    case "S":
                        Pie1.ComplexSeries.First().Data[3].Y += sd.TotalHours;
                        Pie1.DrillDowns[1].Data[1].Y += sd.TotalHours;
                        break;
                    case "M":
                        Pie1.ComplexSeries.First().Data[3].Y += sd.TotalHours;
                        Pie1.DrillDowns[1].Data[2].Y += sd.TotalHours;
                        break;
                    default:
                        return Error();
                }
            }
            ViewBag.Pie1 = Pie1;

            ChartViewModel Bars2 = new ChartViewModel()
            {
                Name = Localizer["Dedicaci�n por Tarea"],
                ComplexSeries = new List<ChartViewModel.ComplexSerie>() {
                    new ChartViewModel.ComplexSerie() { Name = Localizer["Horas"], Data = new List<HighCharts.Data>(){ } }
                }
            };

            foreach (Job job in model.Jobs.Where(j => j.Type == JobType.Productive))
            {
                List<ScheduleDateTermJob> scheduleDateTermJobs = scheduleDates.SelectMany(sd => sd.ScheduleDateTerm.SelectMany(sdt => sdt.ScheduleDateTermJob).Where(sdtj => sdtj.Job == job)).ToList();
                double jobhours = 0;
                foreach (ScheduleDateTermJob sdtj in scheduleDateTermJobs) { jobhours += sdtj.ScheduleDateTerm.Hours; }
                Bars2.ComplexSeries.First().Data.Add( new HighCharts.Data() { Name = job.Name, Y = jobhours } );
            }

            ViewBag.Bars2 = Bars2;

            if(viewtype == "month")
            {
                IDictionary<int, List<double>> Agents = StaffingService.GetActualAgents(model.Jobs.Select(j => j.Id).ToList(), year.Value, month.Value);
                IDictionary<int, List<double>> Required = StaffingService.GetRequiredAgents(model.Jobs.Select(j => j.Id).ToList(), year.Value, month.Value);
                List<string> labels = new List<string> { "00:00", "00:30", "01:00", "01:30", "02:00", "02:30", "03:00", "03:30", "04:00", "04:30", "05:00", "05:30", "06:00", "06:30", "07:00", "07:30", "08:00", "08:30", "09:00", "09:30", "10:00", "10:30", "11:00", "11:30", "12:00", "12:30", "13:00", "13:30", "14:00", "14:30", "15:00", "15:30", "16:00", "16:30", "17:00", "17:30", "18:00", "18:30", "19:00", "19:30", "20:00", "20:30", "21:00", "21:30", "22:00", "22:30", "23:00", "23:30" };

                ChartViewModel Line1 = new ChartViewModel()
                {
                    Name = Localizer["Capacidad Mensual por D�a"],
                    ComplexSeries = new List<ChartViewModel.ComplexSerie>() {
                        new ChartViewModel.ComplexSerie() { Name = Localizer["Disponible"], Data = new List<HighCharts.Data>(){ } },
                        new ChartViewModel.ComplexSerie() { Name = Localizer["Requerido"], Data = new List<HighCharts.Data>(){ } }
                    },
                    DrillDowns = new List<ChartViewModel.ComplexSerie>() { }
                };

                for (int daynum = 1; daynum <= DateTime.DaysInMonth(year.Value, month.Value); daynum++)
                {
                    Line1.ComplexSeries[0].Data.Add(new HighCharts.Data() { Name = daynum.ToString(), Y = Math.Round(Agents.ContainsKey(daynum) ? Agents[daynum].Sum() : 0, 2), Drilldown = "A" + daynum.ToString() });
                    Line1.ComplexSeries[1].Data.Add(new HighCharts.Data() { Name = daynum.ToString(), Y = Math.Round(Required.ContainsKey(daynum) ? -Required[daynum].Sum() : 0, 2), Drilldown = "R" + daynum.ToString() });

                    List<HighCharts.Data> DrillDownDataA = new List<HighCharts.Data>();
                    List<HighCharts.Data> DrillDownDataR = new List<HighCharts.Data>();
                    for (int index = 0; index < 48; index++)
                    {
                        DrillDownDataA.Add(new HighCharts.Data
                        {
                            Name = labels[index],
                            Y = (Math.Round(Agents.ContainsKey(daynum) ? Agents[daynum].ElementAt(index) : 0, 2))
                        });
                        DrillDownDataR.Add(new HighCharts.Data
                        {
                            Name = labels[index],
                            Y = (Math.Round(Required.ContainsKey(daynum) ? -Required[daynum].ElementAt(index) : 0, 2))
                        });
                    }
                    Line1.DrillDowns.Add(new ChartViewModel.ComplexSerie() { Name = "A" + daynum.ToString(), Data = DrillDownDataA });
                    Line1.DrillDowns.Add(new ChartViewModel.ComplexSerie() { Name = "R" + daynum.ToString(), Data = DrillDownDataR });
                }

                ViewBag.Line1 = Line1;
            }

            #endregion

            return View("ShowDashboard", model);
        }

        [Route("~/[controller]/show/{id:int}/staffing/{year:int?}/{month:int?}/{selectedjobs?}")]
        [HttpGet]
        [AuthorizeRoles(AppRole.OperationalManagementEdit, AppRole.OperationalManagementShow)]
        public IActionResult ShowStaffing(int id, int? year, int? month, string selectedjobs = null)
        {
            ViewBag.Id = id;
            ViewBag.Tab = "tab-staffing";
            Area area = AreaService.Get(id);
            if (!Authorize(area)) return Error();
            AreaViewModel model = MapAreaToModel(area);
            var areas = AreaService.GetChildren(id).ToList();
            model.Jobs = JobService.GetJobsFromAreas(areas).ToList();
            var JobsIdsToShow = model.Jobs.Select(x => x.Id).ToList();

            if (year == null) year = DateTime.Now.Year;
            if (month == null) month = DateTime.Now.Month;

            var result = StaffingService.GetCapacity(JobsIdsToShow, year.Value, month.Value);
            var (start, end) = StaffingService.GetRealTimeSlots(result);

            List<Area> authUserOwnedAreas = AuthUser.OwnedAreas.ToList();
            List<Job> authUserOwnedJobs = AuthUser.OwnedJobs.ToList();
            List<int> jobFilterIds = JobFilter(authUserOwnedJobs, selectedjobs);
            List<int> areasIdsToShow = selectedjobs == null ? authUserOwnedAreas.Select(a => a.Id).ToList() : authUserOwnedJobs.Where(y => jobFilterIds.Contains(y.Id)).Select(x => x.AreaId).ToList();
            List<JobConfig> JobsToShowConfig = StaffingService.GetJobsConfiguration(jobFilterIds, year.Value, month.Value).ToList();

            ViewBag.StartSlot = start;
            ViewBag.EndSlot = end;
            ViewBag.AreaName = area.Name;
            ViewBag.AreaType = area.Type.GetDisplayName();
            ViewBag.MyOwnedJobs = Mapper.Map<List<JobModel>>(authUserOwnedJobs);
            ViewBag.MyOwnedAreas = Mapper.Map<List<AreaModel>>(authUserOwnedAreas);
            ViewBag.JobsIdsToShow = jobFilterIds;
            ViewBag.AreasIdsToShow = areasIdsToShow;
            ViewBag.JobsToShowConfig = JobsToShowConfig;
            ViewBag.Year = year;
            ViewBag.Month = month;
            ViewBag.Timeslots = StaffingService.GetTimeSlots();
            ViewBag.Result = result;

            return View("ShowStaffing");
        }

        [Route("~/[controller]/show/{id:int}/users/{state?}/{selectedoffice?}")]
        [HttpGet]
        [AuthorizeRoles(AppRole.OperationalManagementEdit, AppRole.OperationalManagementShow)]
        public IActionResult ShowUsers(int id, string state = null, string selectedoffice = null)
        {
            ViewBag.Id = id;
            ViewBag.Tab = "tab-users";

            Area area = AreaService.Get(id);
            if (!Authorize(area)) return Error();

            List<Area> ChildrenAreas = AreaService.GetChildren(id).ToList();

            List<Office> areasOffices = ChildrenAreas.SelectMany(a => a.UserArea).Where(ua => ua.Active).Select(ua => ua.User).SelectMany(u => u.UserOffice).Where(uo => uo.Active).Select(uo => uo.Office).Distinct().ToList();
            bool nullOffices = ChildrenAreas.SelectMany(a => a.UserArea).Where(ua => ua.Active).Select(ua => ua.User).Any(u => !(u.UserOffice.Any(uo => uo.Active)));
            if (nullOffices)
            {
                areasOffices.Insert(0, new Office() { Id = 0, Name = Localizer["No informada"] });
            }

            List<Office> selectedOffices = new List<Office>();

            bool? Active = null;
            switch (state)
            {
                case "Active": Active = true; ViewBag.State = ActiveType.Active; ViewBag.ActiveName = " " + Localizer["Activos"].ToString().ToLower(); break;
                case "Inactive": Active = false; ViewBag.State = ActiveType.Inactive; ViewBag.ActiveName = " " + Localizer["Dados de baja"].ToString().ToLower(); break;
                case "All": Active = null; ViewBag.State = ActiveType.All; break;
                default: Active = true; ViewBag.State = ActiveType.Active; ViewBag.ActiveName = " " + Localizer["Activos"].ToString().ToLower(); break;
            }

            AreaViewModel model = MapAreaToModel(area);

            List<int> areasIds = ChildrenAreas.Select(x => x.Id).ToList();
            List<User> users = UserService.GetUsersFromAreas(areasIds).ToList();

            if (Active.HasValue)
            {
                users = users.Where(u => u.Active == Active).ToList();
            }
            if (selectedoffice != null)
            {
                IEnumerable<int> officeFilterIds = selectedoffice.Split(';').Select(int.Parse);
                selectedOffices = areasOffices.Where(o => officeFilterIds.Contains(o.Id)).ToList();
                users = users.Where(u => officeFilterIds.Any(i => i.Equals(0)) ? u.UserOffice.Any(uo => uo.Active && officeFilterIds.Contains(uo.OfficeId)) || !(u.UserOffice.Any()) : u.UserOffice.Any(uo => uo.Active && officeFilterIds.Contains(uo.OfficeId)) ).Distinct().ToList();
            }

            ViewBag.SelectedOffices = Mapper.Map<List<OfficeModel>>(selectedOffices); ;
            ViewBag.AreasOffices = Mapper.Map<List<OfficeModel>>(areasOffices); ;

            model.Jobs = JobService.GetJobsFromAreas(new Area[] { area }).ToList();
            model.OwnedUsers = users;

            return View("ShowUsers", model);
        }

        [Route("~/[controller]/show/{id:int}/schedule/{year:int?}/{month:int?}/{active?}")]
        [HttpGet]
        [AuthorizeRoles(AppRole.OperationalManagementEdit, AppRole.OperationalManagementShow)]
        public IActionResult ShowSchedule(int id, int? year, int? month, string active = null)
        {
            Area area = AreaService.Get(id);

            ViewBag.Tab = "tab-schedule";
            ViewBag.Id = id;
            ViewBag.AreaId = id;
            ViewBag.AreaName = area.Name;
            ViewBag.AreaType = area.Type.GetDisplayName();

            if (year == null) year = DateTime.Now.Year;
            if (month == null) month = DateTime.Now.Month;
            if (active != null && active != "all" && active != "active" && active != "inactive")
            {
                return Error(StatusCodes.Status404NotFound);
            }
            ViewBag.Active = active == "" || active == null ? "active" : active;
            bool? Active = active == "" || active == null || active == "active" ? true : active == "inactive" ? false : (bool?)null;

            List<Area> Areas = AuthUser.OwnedAreas.ToList();
            List<int> AreasIds = AreaService.GetChildren(id).Select(a => a.Id).ToList();
            ScheduleViewModel model = new ScheduleViewModel
            {
                Year = year.Value,
                Month = month.Value,
                Areas = Areas,
                AreasIds = AreasIds,
                Rows = new List<ScheduleRow>()
            };
            if (AreasIds.Count <= 0) return View("ShowSchedule", model);

            UserService.GetAllUserSchedule(model.Year, model.Month, AreasIds, Active).ToList().ForEach(user =>
            {
                Dictionary<string, ScheduleDate> scheduleDict = new Dictionary<string, ScheduleDate>();
                foreach (ScheduleDate scheduleDay in user.ScheduleDate)
                {
                    if (scheduleDay != null) scheduleDict.Add("D" + scheduleDay.Day, scheduleDay);
                }
                ScheduleRow scheduleRow = new ScheduleRow
                {
                    UserId = user.Id,
                    FullName = user.FullName,
                    Columns = scheduleDict
                };
                model.Rows.Add(scheduleRow);
            });
            
            return View("ShowSchedule", model);
        }

        #endregion

        #region Add

        [HttpGet]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        [Route("~/[controller]/add/{id:int?}")]
        public IActionResult Add(int? id)
        {
            AddAreaViewModel model = new AddAreaViewModel
            {
                ParentAreas = AuthUser.OwnedAreas
            };
            if (id.HasValue)
            {
                model.ParentId = id.Value;
            }
            return PartialView("_Add", model);
        }
      
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateAjaxModelState]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        public IActionResult Add(AddAreaViewModel model)
        {
            Area area = new Area
            {
                Name = model.Name,
                Description = model.Description,
                Type = model.Type,
                ParentId = model.ParentId,
                StartDate = DateTime.Now
            };
            JobType jobType;
            string acronym = "T";
            switch (model.Type)
            {
                case AreaType.Organizational: jobType = JobType.Organizational;  break;
                case AreaType.Project: jobType = JobType.Project;  break;
                case AreaType.Productive: jobType = JobType.Productive; break;
                case AreaType.General: jobType = JobType.General; break;
                default: jobType = JobType.General; break;
            }
            area.Job.Add(new Job
            {
                Name = model.Name,
                Description = Localizer["Tarea por defecto"],
                Default = true,
                Type = jobType,
                Acronym = acronym
            });

            if (AreaService.Insert(area) > 0)
            {
                JsonResponse response = new JsonResponse() { Result = true, Response = "A�adido con �xito", Content = new { area.Id } };
                return Json(response);
            }
            return Error();
        }

        #endregion

        #region Edit

        [HttpGet]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        public IActionResult Edit(int id)
        {           
            Area area = AreaService.Get(id);
            if (area == null) return Error(StatusCodes.Status406NotAcceptable);

            AreaViewModel model = MapAreaToModel(area);
            model.UserCount = area.UserArea.Where(x => x.Active == true && x.Owner == false && x.Authorized == false).Count();
            model.OwnedAreas = AuthUser.OwnedAreas;
            model.OwnedUsers = UserService.GetSubordinates(AuthUser.Id).ToList();
            return View("Edit", model);            
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateAjaxModelState]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        public IActionResult Edit(AreaViewModel model)
        {
            Area area = AreaService.Get(model.Id);
            
            area.Name = model.Name;
            area.Description = model.Description;
            area.Type = model.Type;
            area.ParentId = model.ParentId;
            area.StartDate = model.StartDate;
            if (model.EndDate.HasValue)
            {
                foreach (Area a in AreaService.GetChildren(model.Id))
                {
                    a.EndDate = model.EndDate;
                    AreaCode lastAreaCode = a.AreaCode.Where(ac => !ac.EndDate.HasValue).FirstOrDefault();
                    lastAreaCode.EndDate = model.EndDate;
                    foreach (AreaCodeWbs acw in lastAreaCode.AreaCodeWbs)
                    {
                        acw.EndDate = model.EndDate;
                    }
                    foreach (Job j in a.Job.Where(j => !j.EndDate.HasValue))
                    {
                        j.EndDate = model.EndDate;
                        foreach (JobConfig jc in j.JobConfig.Where(jc => !jc.EndDate.HasValue))
                        {
                            jc.EndDate = model.EndDate;
                        }
                    }
                }
            }

            if (AreaService.Update(area) > 0)
            {
                return RedirectToAction("Show", new { id = area.Id });
            }
            return Error();
        }

        #endregion

        #region Assign

            #region AssignOwner

        [Route("~/[controller]/edit/{id}/[action]")]
        [HttpGet]
        [AuthorizeRoles(AppRole.OperationalManagementShow)]
        public IActionResult OwnerHistory(int id)
        {
            ViewBag.Editable = false;
            Area area = AreaService.Get(id);
            if (!AuthorizeAjax(area)) return Error();
            return AssignOwner(area);
        }

        [Route("~/[controller]/edit/{id}/[action]")]
        [HttpGet]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        public IActionResult EditOwnerHistory(int id)
        {
            ViewBag.Editable = true;
            Area area = AreaService.Get(id);
            if (!AuthorizeAjax(area)) return Error();
            return AssignOwner(area);
        }

        protected IActionResult AssignOwner(Area area)
        {
            UserArea lastUserArea = area.UserArea.Where(x => x.Owner.GetValueOrDefault() == true && !x.EndDate.HasValue).FirstOrDefault();
            var AssignmentStartDate = lastUserArea != null ? lastUserArea.StartDate : DateTime.Parse("1900-01-01 00:00:00");
            AssignViewModel Model = new AssignViewModel
            {
                Id = area.Id,
                Entity = Localizer["Responsable"],
                AssignmentStartDate = AssignmentStartDate,
                Options = UserService.GetSubordinates(AuthUser.Id).Select(x => new AssignOptions() { Value = x.Id, Name = x.FullName }).ToList(),
                History = area.UserArea.Where(x => x.Owner.GetValueOrDefault() == true).Select(i =>
                    new AssignHistory
                    {
                        Name = i.User.FullName,
                        StartDate = i.StartDate,
                        EndDate = i.EndDate
                    }).OrderByDescending(i => i.StartDate).ToList()
            };
            return PartialView("_Assign", Model);
        }

        [Route("~/[controller]/edit/[action]")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateAjaxModelState]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        public IActionResult EditOwnerHistory(AssignViewModel Model)
        {
            Area area = AreaService.Get(Model.Id);
            if (!AuthorizeAjax(area)) return Error();
            UserArea lastUserArea = area.UserArea.Where(x => x.Owner.GetValueOrDefault() == true && !x.EndDate.HasValue).FirstOrDefault();
            if (lastUserArea != null)
            {
                lastUserArea.EndDate = Model.AssignmentStartDate.AddDays(-1);
            }
            User user = UserService.Get(Model.AssignmentId);
            if (user.Id > 0)
            {
                UserArea userArea = new UserArea()
                {
                    User = user,
                    StartDate = Model.AssignmentStartDate,
                    Owner = true,
                    Authorized = false
                };
                area.UserArea.Add(userArea);
                AreaService.Update(area);
                JsonResponse response = new JsonResponse() { Result = true, Response = "El cambio se ha realizado con �xito" };
                return Json(response);
            }
            return Error();
        }

            #endregion
            #region AssignCode

        [Route("~/[controller]/edit/{id}/[action]")]
        [HttpGet]
        [AuthorizeRoles(AppRole.OperationalManagementShow)]
        public IActionResult CodeHistory(int id)
        {
            ViewBag.Editable = false;
            Area area = AreaService.Get(id);
            if (!AuthorizeAjax(area)) return Error();
            return AssignCode(area);
        }

        [Route("~/[controller]/edit/{id}/[action]")]
        [HttpGet]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        public IActionResult EditCodeHistory(int id)
        {
            ViewBag.Editable = true;
            Area area = AreaService.Get(id);
            if (!AuthorizeAjax(area)) return Error();
            return AssignCode(area);
        }

        protected IActionResult AssignCode(Area area)
        {
            AreaCode lastAreaCode = area.AreaCode.Where(x => !x.EndDate.HasValue).FirstOrDefault();
            DateTime AssignmentStartDate = lastAreaCode != null ? lastAreaCode.StartDate : DateTime.Parse("1900-01-01 00:00:00");
            AssignViewModel Model = new AssignViewModel
            {
                Id = area.Id,
                Entity = Localizer["C�digo de Proyecto"],
                AssignmentStartDate = AssignmentStartDate,
                History = area.AreaCode.Select(i =>
                    new AssignHistory
                    {
                        Name = i.Name,
                        StartDate = i.StartDate,
                        EndDate = i.EndDate
                    }).OrderByDescending(i => i.StartDate).ToList()
            };
            return PartialView("_Assign", Model);
        }

        [Route("~/[controller]/edit/[action]")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateAjaxModelState]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        public IActionResult EditCodeHistory(AssignViewModel Model)
        {
            Area area = AreaService.Get(Model.Id);
            if (!AuthorizeAjax(area)) return Error();
            AreaCode lastAreaCode = area.AreaCode.Where(x => !x.EndDate.HasValue).FirstOrDefault();
            if (lastAreaCode != null)
            {
                lastAreaCode.EndDate = Model.AssignmentStartDate.AddDays(-1);
                foreach (AreaCodeWbs acw in lastAreaCode.AreaCodeWbs)
                {
                    acw.EndDate = lastAreaCode.EndDate;
                }
            }

            AreaCode areaCode = new AreaCode()
            {
                Area = area,
                Name = Model.Assignment,
                StartDate = Model.AssignmentStartDate
            };
            area.AreaCode.Add(areaCode);

            AreaService.Update(area);
            JsonResponse response = new JsonResponse() { Result = true, Response = "El cambio se ha realizado con �xito" };
            return Json(response);
        }

            #endregion
            #region AssignCodeWbs
    
        [Route("~/[controller]/edit/{id}/[action]")]
        [HttpGet]
        [AuthorizeRoles(AppRole.OperationalManagementShow)]
        public IActionResult CodeWbsHistory(int id)
        {
            ViewBag.Editable = false;
            Area area = AreaService.Get(id);
            if (!AuthorizeAjax(area)) return Error();
            return AssignCodeWbs(area);
        }

        [Route("~/[controller]/edit/{id}/[action]")]
        [HttpGet]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        public IActionResult EditCodeWbsHistory(int id)
        {
            ViewBag.Editable = true;
            Area area = AreaService.Get(id);
            if (!AuthorizeAjax(area)) return Error();
            return AssignCodeWbs(area);
        }

        protected IActionResult AssignCodeWbs(Area area)
        {
            AreaCodeWbs lastAreaCodeWbs = area.AreaCodeWbs.Where(x => !x.EndDate.HasValue).FirstOrDefault();
            var AssignmentStartDate = lastAreaCodeWbs != null ? lastAreaCodeWbs.StartDate : DateTime.Parse("1900-01-01 00:00:00");
            AssignViewModel Model = new AssignViewModel
            {
                Id = area.Id,
                Entity = Localizer["EDT"],
                AssignmentStartDate = AssignmentStartDate,
                History = area.AreaCodeWbs.Select(i =>
                    new AssignHistory
                    {
                        Name = i.Name,
                        StartDate = i.StartDate,
                        EndDate = i.EndDate
                    }).OrderByDescending(i => i.StartDate).ToList()
            };
           

            AreaCode code = area.AreaCode.Where(x => !x.EndDate.HasValue).FirstOrDefault();
            while (area.ParentId != null && code == null)
            {
                area = area.Parent;
                code = area.AreaCode.Where(x => !x.EndDate.HasValue).FirstOrDefault();
            }
            return PartialView("_Assign", Model);
        }

        [Route("~/[controller]/edit/[action]")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateAjaxModelState]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        public IActionResult EditCodeWbsHistory(AssignViewModel Model)
        {
            Area area = AreaService.Get(Model.Id);
            if (!AuthorizeAjax(area)) return Error();
            AreaCodeWbs lastAreaCodeWbs = area.AreaCodeWbs.Where(x => !x.EndDate.HasValue).FirstOrDefault();
            if (lastAreaCodeWbs != null)
            {
                lastAreaCodeWbs.EndDate = Model.AssignmentStartDate.AddDays(-1);
            }
            Area tempArea = area;
            AreaCode code = tempArea.AreaCode.Where(x => !x.EndDate.HasValue).FirstOrDefault();
            while (tempArea.ParentId != null && code == null)
            {
                tempArea = tempArea.Parent;
                code = tempArea.AreaCode.Where(x => !x.EndDate.HasValue).FirstOrDefault();
            }
            if (code != null)
            {
                AreaCodeWbs areaCodeWbs = new AreaCodeWbs()
                {
                    Area = area,
                    AreaCode = code,
                    Name = Model.Assignment,
                    StartDate = Model.AssignmentStartDate
                };
                area.AreaCodeWbs.Add(areaCodeWbs);
                AreaService.Update(area);
                JsonResponse response = new JsonResponse() { Result = true, Response = "El cambio se ha realizado con �xito" };
                return Json(response);
            }
            return Error();
            
        }

            #endregion

        #endregion

        #endregion

        #region Code

        [Route("~/[controller]/edit/{id}/[action]")]
        [HttpGet]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        public IActionResult EditCode(int id)
        {
            Area area = AreaService.Get(id);
            AssignAreaViewModel Model = new AssignAreaViewModel
            {
                AreaId = id,
                Entity = Localizer["C�digo"],
            };
            return PartialView("_AssignA", Model);
        }

        [Route("~/[controller]/edit/[action]")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateAjaxModelState]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        public IActionResult EditCode(AssignAreaViewModel model)
        {        
            Area area = AreaService.Get(model.AreaId);
            AreaService.Update(area);

            JsonResponse response = new JsonResponse() { Result = true, Response = "El cambio se ha realizado con �xito" };
            return Json(response);
        }

        #endregion

        #region EDT

        [Route("~/[controller]/edit/{id}/[action]")]
        [HttpGet]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        public IActionResult EditEdt(int id)
        {
            Area area = AreaService.Get(id);
            AssignAreaViewModel Model = new AssignAreaViewModel
            {
                AreaId = id,
                Entity = Localizer["EDT"],
            };
            return PartialView("_AssignA", Model);
        }

        [Route("~/[controller]/edit/[action]")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateAjaxModelState]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        public IActionResult EditEdt(AssignAreaViewModel model)
        {
            Area area = AreaService.Get(model.AreaId);
            if (area == null) return Error(StatusCodes.Status406NotAcceptable);
            AreaCodeWbs areaCodeEdt = new AreaCodeWbs
            {
                Name = model.NCode,
                Description = model.DesCode,
                StartDate = model.AssignmentStartDate,
            };
            AreaService.Update(area);
            JsonResponse response = new JsonResponse() { Result = true, Response = "El cambio se ha realizado con �xito" };
            return Json(response);
        }

        #endregion
    }
}
