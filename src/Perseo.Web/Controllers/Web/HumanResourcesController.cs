using Perseo.Core;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Localization;
using AutoMapper;

namespace Perseo.Web.Controllers
{
    [Route("[controller]/[action]/{id:int?}")]
    public class HumanResourcesController : BaseController
    {
        public HumanResourcesController( IHttpContextAccessor contextAccessor,  IHostingEnvironment enviroment, IStringLocalizer<SharedResources> localizer,
            IUserService userService,
            IMapper mapper) : base(contextAccessor, enviroment, userService, localizer, mapper)
        { }

        [HttpGet]
        [Route("/[controller]")]
        [ValidateAjaxModelState]
        [AuthorizeRoles(AppRole.HHRRManagementEdit, AppRole.HHRRManagementShow)]
        public IActionResult Index()
        {
            return List();
        }

        [HttpGet]
        [AuthorizeRoles(AppRole.HHRRManagementEdit, AppRole.HHRRManagementShow)]
        public IActionResult List()
        {
            return View("List");
        }

        [HttpGet]
        [AuthorizeRoles(AppRole.HHRRManagementEdit)]
        public IActionResult Recruit()
        {
            return View("List");
        }

        [HttpGet]
        [AuthorizeRoles(AppRole.HHRRManagementEdit)]
        public IActionResult Dismiss()
        {
            return View("List");
        }
    }
}
