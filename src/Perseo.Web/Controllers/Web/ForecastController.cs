﻿using Perseo.Core;
using Perseo.Web.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Localization;
using AutoMapper;

namespace Perseo.Web.Controllers
{
    [Route("/staffing/[controller]/[action]/{id:int?}")]
    public class ForecastController : BaseController
    {
        private readonly IStaffingService StaffingService;
        private readonly IService<Forecast> ForecastService;

        public ForecastController(
            IHttpContextAccessor contextAccessor,
            IUserService userService,
            IStringLocalizer<SharedResources> localizer,
            IStaffingService staffingService,
            IService<Forecast> forecastService,
            IHostingEnvironment enviroment,
            IMapper mapper) : base(contextAccessor, enviroment, userService, localizer, mapper)
        {
            StaffingService = staffingService;
            ForecastService = forecastService;
        }

        [HttpGet]
        [AuthorizeRoles(AppRole.OperationalManagementEdit, AppRole.OperationalManagementShow)]
        [Route("/staffing/[controller]/{year:int?}/{jobs?}")]
        public IActionResult Index(int? year, string jobs = null)
        {
            if (year == null) year = DateTime.Now.Year;
            return List(null);
        }

        [HttpGet]
        [AuthorizeRoles(AppRole.OperationalManagementEdit, AppRole.OperationalManagementShow)]
        [Route("/staffing/[controller]/[action]/{year:int?}/{jobs?}")]
        public IActionResult List(int? year, string jobs = null)
        {
            if (year == null) year = DateTime.Now.Year;
            List<AreaModel> MyOwnedAreas = Mapper.Map<List<AreaModel>>(AuthUser.OwnedAreas.ToList());
            List<JobModel> MyOwnedJobs = Mapper.Map<List<JobModel>>(AuthUser.OwnedJobs.ToList());
            List<int> MyJobsIds = MyOwnedJobs.Select(x => x.Id).ToList();
            List<int> JobsIdsToShow = jobs == null ? MyJobsIds : jobs.Split(";").Select(Int32.Parse).ToList();
            JobsIdsToShow.RemoveAll(j => !MyJobsIds.Contains(j));
            List<int> AreasIdsToShow = jobs == null ? MyOwnedAreas.Select(a => a.Id).ToList() : MyOwnedJobs.Where(y => JobsIdsToShow.Contains(y.Id)).Select(x=>x.AreaId).ToList();

            List<ForecastMonthViewModel> modelList = new List<ForecastMonthViewModel>();
            ForecastService.GetMany(x=>x.Year == year && JobsIdsToShow.Any(y=>y == x.JobId), null, true).ToList().ForEach(item =>
            {
                ForecastMonthViewModel model = new ForecastMonthViewModel
                {
                    Date = item.Date,
                    Area = item.Job.Area.Name,
                    Job = item.Job.Name,
                    Year = item.Year,
                    Month = item.Month,
                    Events = item.Events
                };
                modelList.Add(model);
            });

            ViewBag.Year = year;
            ViewBag.MyOwnedJobs = MyOwnedJobs;
            ViewBag.MyOwnedAreas = MyOwnedAreas;
            ViewBag.JobsIdsToShow = JobsIdsToShow;
            ViewBag.AreasIdsToShow = AreasIdsToShow;
            return View("list", modelList);
        }

        [HttpGet]
        [AuthorizeRoles(AppRole.OperationalManagementEdit, AppRole.OperationalManagementShow)]
        [Route("/staffing/[controller]/[action]")]
        public IActionResult Show(int? jobId = null)
        {
            List<Area> MyOwnedAreas = AuthUser.OwnedAreas.ToList();
            List<Job> MyOwnedJobs = AuthUser.OwnedJobs.ToList();
            List<int> MyJobsIds = MyOwnedJobs.Select(x => x.Id).ToList();

            ViewBag.Year = DateTime.Now.Year;
            ViewBag.Month = DateTime.Now.Month;
            ViewBag.MyOwnedJobs = MyOwnedJobs;
            ViewBag.MyOwnedAreas = MyOwnedAreas;
            ViewBag.Timeslots = StaffingService.GetTimeSlots();
            return View("Show");
        }

        [HttpGet]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        [Route("/staffing/[controller]/[action]")]
        public IActionResult Generate(int? jobId = null)
        {
            ForecastViewModel Model = new ForecastViewModel
            {
                Year = DateTime.Now.Year,
                Month = DateTime.Now.Month,
                Areas = AuthUser.OwnedAreas,
                Jobs = AuthUser.OwnedJobs,
                TimeSlots = StaffingService.GetTimeSlots(),
            };
            return View("Generate", Model);
        }

        [HttpGet]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        [Route("/staffing/[controller]/[action]")]
        public IActionResult GeneratePattern(int? year = null, int? month = null, int? jobId = null)
        {
            if (year == null) year = DateTime.Now.Year;
            if (month == null) month = DateTime.Now.Month;

            IDictionary<int, List<double>> Pattern = StaffingService.GetRequiredEventsMonthPattern(year.Value, month.Value);

            ForecastPatternViewModel Model = new ForecastPatternViewModel
            {
                Year = DateTime.Now.Year,
                Month = DateTime.Now.Month,
                Areas = AuthUser.OwnedAreas,
                Jobs = AuthUser.OwnedJobs,
                TimeSlots = StaffingService.GetTimeSlots(),
                Forecast = Pattern
            };
            return PartialView("_GenerateForecastPattern", Model);
        }

        [HttpPost]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        [Route("/staffing/[controller]/[action]")]
        [ValidateAntiForgeryToken]
        [ValidateAjaxModelState]
        public IActionResult Generate()
        {
            if (ModelState.IsValid)
            {
                ForecastViewModel Model = new ForecastViewModel
                {
                    Year = DateTime.Now.Year,
                    Month = DateTime.Now.Month,
                    Areas = AuthUser.OwnedAreas,
                    Jobs = AuthUser.OwnedJobs,
                    TimeSlots = StaffingService.GetTimeSlots(),
                };
                return View("Generate");
            }
            string res = "mal";
            return Ok(new { res });
        }

        [HttpGet]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        [Route("/staffing/[controller]/[action]")]
        public IActionResult Upload(int? jobId = null)
        {
            ForecastViewModel Model = new ForecastViewModel
            {
                Year = DateTime.Now.Year,
                Month = DateTime.Now.Month,
                Areas = AuthUser.OwnedAreas,
                Jobs = AuthUser.OwnedJobs,
                TimeSlots = StaffingService.GetTimeSlots(),
            };
            return View("Upload", Model);
        }


        [HttpPost]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        [Route("/staffing/[controller]/[action]")]
        [ValidateAntiForgeryToken]
        [ValidateAjaxModelState]
        public async Task<IActionResult> Upload(IFormFile File, ForecastViewModel Model)
        {
            if (AuthUser.OwnedJobs.Any(x => x.Id == Model.JobId))
            {
                IDictionary<int, List<double>> Forecast = new Dictionary<int, List<double>>();
                var filePath = Path.GetTempFileName();
                if (File.Length > 0)
                {
                    using (var stream = new FileStream(filePath, FileMode.Create))
                    {
                        await File.CopyToAsync(stream);
                        Forecast = StaffingService.GetRequiredEventsFromFile(stream, 0, Model.JobId, Model.Year, Model.Month);
                    }
                }
                Model.TimeSlots = StaffingService.GetTimeSlots();
                Model.Forecast = Forecast;
                return PartialView("_ConfirmForecast", Model);
            }
            return StatusCode(StatusCodes.Status400BadRequest);
        }

        [HttpPost]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        [Route("/staffing/[controller]/[action]")]
        [ValidateAntiForgeryToken]
        [ValidateAjaxModelState]
        public IActionResult Add(AddForecastViewModel Model)
        {
            if (AuthUser.OwnedJobs.Any(x => x.Id == Model.JobId))
            {
                IDictionary<int, List<double>> Forecast = new Dictionary<int, List<double>>();
                try {
                    Forecast = JsonConvert.DeserializeObject<Dictionary<int, List<double>>>(Model.Forecast);
                    StaffingService.SaveForeacast(Forecast, Model.JobId, Model.Year, Model.Month);
                    Forecast forecast = new Forecast() {
                        Date = DateTime.Now,
                        Year = Model.Year,
                        Month = Model.Month,
                        JobId = Model.JobId,
                        Events = Forecast.Values.Sum(d => d.Sum())
                    };
                    ForecastService.Add(forecast);
                }
                catch
                {
                    return StatusCode(StatusCodes.Status400BadRequest);
                }
                return StatusCode(StatusCodes.Status200OK);
            }
            return StatusCode(StatusCodes.Status400BadRequest);
        }
    }
}
