using System;
using System.Linq;
using System.Globalization;
using System.Collections.Generic;
using Perseo.Core;
using Perseo.Web.Models;
using Perseo.Web.Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Localization;
using AutoMapper;

namespace Perseo.Web.Controllers
{
    [Route("[controller]/[action]/{id:int?}")]
    public class TrainingController : BaseController
    {
        #region CONSTRUCTOR
        private readonly IService<Training> TrainingService;
        private readonly IAreaService AreaService;
        public TrainingController(
            IHttpContextAccessor contextAccessor,
            IHostingEnvironment enviroment,
            IUserService userService,
            IStringLocalizer<SharedResources> localizer,
            IAreaService areaService,
            IService<Training> trainingService,
            IMapper mapper) : base(contextAccessor, enviroment, userService, localizer, mapper)
        {
            TrainingService = trainingService;
            AreaService = areaService;
        }
        #endregion

        #region LIST

        [HttpGet]
        [Route("/[controller]")]
        [AuthorizeRoles(AppRole.OperationalManagementEdit, AppRole.OperationalManagementShow)]
        public IActionResult Index()
        {
            return List();
        }

        [HttpGet]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        [Route("/[controller]/[action]/{year:int?}/{month:int?}/{selectedareas?}")]
        public IActionResult List(int? year = null, int? month = null, string selectedareas = null)
        {
            year = year ?? DateTime.Now.Year;
            month = month ?? DateTime.Now.Month;

            List<Area> authUserOwnedAreas = AuthUser.OwnedAreas.ToList();
            List<int> areaFilterIds = AreaFilter(authUserOwnedAreas, selectedareas);

            #region CHARTS
            List<string> labels = new List<string>();
            DateTime BaseDate = new DateTime((int)year, (int)month, 1);
            for (int i = 11; i >= 0; i--) { labels.Add(CultureInfo.CurrentCulture.TextInfo.ToTitleCase((BaseDate.AddMonths(-i)).ToString("MMMM"))); }

            ChartViewModel Area1 = new ChartViewModel() {
                Name = Localizer["Acciones Formativas"],
                Series = new List<ChartViewModel.Serie>() { new ChartViewModel.Serie() { Name = Localizer["Formaciones"], Data = Enumerable.Repeat(0.0, labels.Count()).ToList() } },
                Labels = labels
            };
            ChartViewModel Area2 = new ChartViewModel() {
                Name = Localizer["Colaboradores Formados"],
                Series = new List<ChartViewModel.Serie>() { new ChartViewModel.Serie() { Name = Localizer["Colaboradores"], Data = Enumerable.Repeat(0.0, labels.Count()).ToList() } },
                Labels = labels
            };
            ChartViewModel Area3 = new ChartViewModel() {
                Name = Localizer["Formaciones por Colaborador"],
                Series = new List<ChartViewModel.Serie>() { new ChartViewModel.Serie() { Name = Localizer["Formaciones"], Data = Enumerable.Repeat(0.0, labels.Count()).ToList() } },
                Labels = labels
            };
            ChartViewModel Area4 = new ChartViewModel() {
                Name = Localizer["Grado de Aprovechamiento"],
                Series = new List<ChartViewModel.Serie>() { new ChartViewModel.Serie() { Name = Localizer["Aprovechamiento"], Data = Enumerable.Repeat(0.0, labels.Count()).ToList() } },
                Labels = labels
            };

            List<Training> trainings = TrainingService.GetMany(t => ((t.StartDate >= BaseDate.AddYears(-1)) && (t.StartDate < BaseDate.AddMonths(1).AddSeconds(-1)) && (areaFilterIds.Any(i => i == t.Area.Id)))).ToList();

            int UsersInAreas = UserService.GetMany(u => u.UserArea.Any(ua => areaFilterIds.Contains(ua.AreaId))).Count();
            List<double> UsersPass = Enumerable.Repeat(0.0, labels.Count()).ToList();
            foreach (Training training in trainings)
            {
                int index = (((12 - BaseDate.Month) + (training.StartDate.Month) - 1) % 12);
                Area1.Series[0].Data[index] += 1;
                Area2.Series[0].Data[index] += training.Users.Where(tu => tu.Attendant).Count();
                Area3.Series[0].Data[index] = Math.Round(Area2.Series[0].Data[index] / UsersInAreas, 2);
                UsersPass[index] += training.Users.Where(tu => tu.Attendant && tu.TrainingCompleted).Count();
            }

            for (int index = 0; index < Area4.Series[0].Data.Count(); index++)
            {
                Area4.Series[0].Data[index] = Area2.Series[0].Data[index] > 0 ? Math.Round((UsersPass[index] / Area2.Series[0].Data[index]) * 100, 2) : 0;
            }
            #endregion

            PagedResult<Training> pagedResult = TrainingService.GetManyPaged(t => ((t.StartDate.Year == (int)year && t.StartDate.Month == (int)month) || (t.Active)) && areaFilterIds.Any(i => i == t.Area.Id), "Active DESC, StartDate");

            ListTrainingViewModel model = new ListTrainingViewModel()
            {
                AreaChart1 = Area1,
                AreaChart2 = Area2,
                AreaChart3 = Area3,
                AreaChart4 = Area4,
                Data = Mapper.Map<List<TrainingModel>>(pagedResult.Results.ToList()),
                Pagination = Mapper.Map<PaginationModel>(pagedResult),
                OwnedAreas = Mapper.Map<List<AreaModel>>(authUserOwnedAreas),
                SelectedAreasIds = areaFilterIds,
                Month = month.Value,
                Year = year.Value
            };
                                 
            return View("List", model);
        }

        #endregion

        #region SHOW

        [HttpGet]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        [Route("~/[controller]/[action]/{trainingid:int}")]
        public IActionResult Show(int trainingid)
        {
            Training training = TrainingService.Get(trainingid);
            if (training == null) return Error(StatusCodes.Status406NotAcceptable);

            ShowTrainingViewModel model = Mapper.Map<ShowTrainingViewModel>(training);

            return View("Show", model);
        }

        [HttpGet]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        [Route("~/[controller]/[action]/{trainingid:int}")]
        public IActionResult State(int trainingid)
        {
            Training training = TrainingService.Get(trainingid);
            if (training == null) return Error(StatusCodes.Status406NotAcceptable);
          
            StateTrainingViewModel model = Mapper.Map<StateTrainingViewModel>(training);

            return View("State", model);
        }

        #endregion

        #region EDIT
            #region BASE

        [HttpGet]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        [Route("~/[controller]/[action]/{trainingid:int}")]
        public IActionResult Edit(int trainingid)
        {
            Training training = TrainingService.Get(trainingid);
            if (training == null) return Error(StatusCodes.Status406NotAcceptable);

            EditTrainingViewModel model = Mapper.Map<EditTrainingViewModel>(training);
            model.OwnedAreas = Mapper.Map<List<AreaModel>>(AuthUser.OwnedAreas.ToList());

            return View("Edit", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateAjaxModelState]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        public IActionResult Edit(EditTrainingViewModel model)
        {
            Training training = TrainingService.Get(model.Training.Id);
            if (training == null) return Error(StatusCodes.Status406NotAcceptable);

            Mapper.Map(model, training);

            if (TrainingService.Update(training) > 0)
            {
                return Json(new JsonResponse() { Result = true, Response = "El cambio se ha realizado con éxito" });
            }
            return Error();
        }

            #endregion
            #region USER
                #region ADD

        [HttpGet]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        [Route("~/[controller]/[action]/{trainingid:int}/{usertype}")]
        public IActionResult AddUser(int trainingid, string usertype)
        {
            Training training = TrainingService.Get(trainingid);
            if (training == null) return Error(StatusCodes.Status406NotAcceptable);
            List<int> AreasIds = AreaService.GetChildren(training.AreaId).Select(a => a.Id).ToList();

            AddTrainingUserViewModel model = new AddTrainingUserViewModel
            {
                TrainingId = trainingid,
                UserType = usertype,
                AddibleUsers = Mapper.Map<List<UserModel>>(UserService.GetMany(u => u.UserArea.Any(ua => AreasIds.Contains(ua.AreaId)) && !u.TrainingUser.Any(tu => tu.TrainingId == training.Id), null, false).ToList())
            };
            return PartialView("_AddUser", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateAjaxModelState]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        [Route("~/[controller]/[action]/{trainingid:int}/{usertype}")]
        public IActionResult AddUser(AddTrainingUserViewModel model)
        {
            List<int> UsersToAddIdInt = model.UsersToAddIds.Split(";").Select(int.Parse).ToList();

            Training training = TrainingService.Get(model.TrainingId);

            UsersToAddIdInt.ToList().ForEach(userid =>
            {
                TrainingUser traininguser = new TrainingUser
                {
                    UserId = userid,
                    Trainer = model.UserType == "trainer" ? true : false,
                    Attendant = model.UserType == "attendant" ? true : false
                };

                training.Users.Add(traininguser);

                if (traininguser.Attendant)
                {
                    foreach (TrainingTest tt in training.Tests)
                    {
                        tt.Users.Add(new TrainingTestUser { UserId = traininguser.UserId });
                    }
                }
            });

            if (TrainingService.Update(training) > 0)
            {
                return Json(data: new JsonResponse() { Result = true, Response = "Añadido/s con éxito", Content = new { Id = model.TrainingId } });
            }
            return Error();
        }

                #endregion
                #region DELETE

        [HttpGet]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        [Route("~/[controller]/[action]/{trainingid:int}/{userid:int}")]
        public JsonResult DeleteUserCheck(int trainingid, int userid)
        {
            string response = "";
            bool ActionAllowed = true;

            Training training = TrainingService.Get(trainingid);

            if (training.Users.Where(tu => tu.UserId == userid).Any(tu => tu.TrainingCompleted))
            {
                ActionAllowed = false; response = Localizer["No es posible eliminar un usuario que ha finalizado la acción formativa."];
            }
            else if (training.Tests.SelectMany(tt => tt.Users).Where(ttu => ttu.UserId == userid).Any(ttu => ttu.Takes.Count > 0))
            {
                ActionAllowed = false; response = Localizer["No es posible eliminar un usuario con pruebas realizadas."];
            }

            return Json(data: new JsonResponse() { Result = ActionAllowed, Response = response });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateAjaxModelState]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        [Route("~/[controller]/[action]/{trainingid:int}/{userid:int}")]
        public IActionResult DeleteUser(int trainingid, int userid)
        {
            Training training = TrainingService.Get(trainingid);
            if (training == null) return Error(StatusCodes.Status406NotAcceptable);

            TrainingUser trainingUser = training.Users.SingleOrDefault(tu => tu.UserId == userid);
            if (trainingUser == null) return Error(StatusCodes.Status406NotAcceptable);

            foreach (TrainingTest tt in training.Tests.ToList())
            {
                foreach (TrainingTestUser ttu in tt.Users.ToList()) { if (ttu.UserId == userid) { tt.Users.Remove(ttu); } }
            }
            
            training.Users.Remove(trainingUser);

            if (TrainingService.Update(training) > 0)
            {
                return Json(data: new JsonResponse() { Result = true, Response = "Eliminado con éxito", Content = new { Id = trainingid } });
            }
            return Error();
        }

                #endregion
            #endregion
        #endregion

        #region ADD

        [HttpGet]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        [Route("~/[controller]/[action]")]
        public IActionResult Add()
        {
            AddTrainingViewModel model = new AddTrainingViewModel
            {
                OwnedAreas = Mapper.Map<List<AreaModel>>(AuthUser.OwnedAreas.ToList())
            };
            return PartialView("_Add", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateAjaxModelState]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        public IActionResult Add(AddTrainingViewModel model)
        {
            Training training = new Training
            {
                Name = model.Name,
                Description = model.Description,
                AreaId = model.AreaId,
                StartDate = DateTime.Now
            };

            if (TrainingService.Add(training) > 0)
            {
                return Json(new JsonResponse() { Result = true, Response = "Añadido con éxito", Content = new { training.Id } });
            }
            return Error();
        }

        #endregion

        #region DELETE

        [HttpGet]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        [Route("~/[controller]/[action]/{trainingid:int}")]
        public JsonResult DeleteCheck(int trainingid)
        {
            string response = "";
            bool ActionAllowed = true;

            Training training = TrainingService.Get(trainingid);

            if (training.Users.Any(tu => tu.TrainingCompleted))
            {
                ActionAllowed = false; response = Localizer["No es posible eliminar una acción formativa que haya sido finalizada por algún usuario."];
            }else if (training.Tests.SelectMany(tt => tt.Users).Any(ttu => ttu.Takes.Count > 0))
            {
                ActionAllowed = false; response = Localizer["No es posible eliminar una acción formativa con pruebas realizadas."];
            }

            return Json(data: new JsonResponse() { Result = ActionAllowed, Response = response});
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateAjaxModelState]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        [Route("~/[controller]/[action]/{trainingid:int}")]
        public IActionResult Delete(int trainingid)
        {
            Training training = TrainingService.Get(trainingid);
            if (training == null) return Error(StatusCodes.Status406NotAcceptable);

            if (TrainingService.Remove(training) > 0)
            {
                return Json(data: new JsonResponse() { Result = true, Response = "Eliminado con éxito" });
            }
            return Error();
        }

        #endregion

        #region TEST
            #region SHOW

        [HttpGet]
        [Route("/[controller]/{trainingid:int}/test/show/{testid:int}")]
        public IActionResult ShowTest(int trainingid, int testid)
        {
            Training training = TrainingService.Get(trainingid);
            if (training == null) return Error(StatusCodes.Status406NotAcceptable);

            TrainingTest test = training.Tests.SingleOrDefault(tt => tt.Id == testid);
            if (test == null) return Error(StatusCodes.Status406NotAcceptable);

            TrainingTestViewModel model = Mapper.Map<TrainingTestViewModel>(test);

            return View("ShowTest", model);
        }

            #endregion
            #region EDIT
        
        [HttpGet]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        [Route("/[controller]/{trainingid:int}/test/editcheck/{testid:int}")]
        public JsonResult EditTestCheck(int trainingid, int testid)
        {
            string response = "";
            bool ActionAllowed = true;

            Training training = TrainingService.Get(trainingid);
            TrainingTest test = training.Tests.SingleOrDefault(tt => tt.Id == testid);
            if (test.Users.Any(ttu => ttu.Takes.Count > 0))
            {
                ActionAllowed = false; response = Localizer["No es posible editar una prueba que ha sido realizada por un usuario."];
            }

            return Json(data: new JsonResponse() { Result = ActionAllowed, Response = response });
        }

        [HttpGet]
        [Route("/[controller]/{trainingid:int}/test/edit/{testid:int}")]
        public IActionResult EditTest(int trainingid, int testid)
        {
            Training training = TrainingService.Get(trainingid);
            if (training == null) return Error(StatusCodes.Status406NotAcceptable);

            TrainingTest test = training.Tests.SingleOrDefault(tt => tt.Id == testid);
            if (test == null) return Error(StatusCodes.Status406NotAcceptable);
            if (test.Users.Any(ttu => ttu.Takes.Count > 0)) return Error(StatusCodes.Status406NotAcceptable);

            TrainingTestViewModel model = Mapper.Map<TrainingTestViewModel>(test);

            return View("EditTest", model);
        }

        [HttpPost]
        [Route("/[controller]/{trainingid:int}/test/edit/{testid:int}")]
        [ValidateAntiForgeryToken]
        public JsonResult EditTest([FromBody] dynamic data)
        {
            int trainingid = data.TrainingId.ToObject<int>();
            int testid = data.Id.ToObject<int>();

            Training training = TrainingService.Get(trainingid);
            TrainingTest test = training.Tests.SingleOrDefault(tt => tt.Id == testid);

            test.Sections.Clear();
            if (TrainingService.Update(training) > 0)
            {
                Mapper.Map(data, test);
                if (TrainingService.Update(training) > 0)
                {
                    return Json(data: new JsonResponse() { Result = true, Response = "OK", Content = data });
                }
            }            
            return Json(data: new JsonResponse() { Result = false, Response = "KO", Content = data });
        }

            #endregion
            #region ADD

        [HttpGet]
        [Route("/[controller]/{trainingid:int}/test/add")]
        public IActionResult AddTest(int? trainingid)
        {
            Training training = TrainingService.Get(trainingid);
            if (training == null) return Error(StatusCodes.Status406NotAcceptable);

            TrainingTestViewModel model = new TrainingTestViewModel
            {
                TrainingId = training.Id,
                TrainingName = training.Name,
                StartDate = DateTime.Now
            };
            return View("EditTest", model);
        }

        [HttpPost]
        [Route("/[controller]/{trainingid:int}/test/add")]
        [ValidateAntiForgeryToken]
        public IActionResult AddTest([FromBody] dynamic data)
        {
            int trainingid = data.TrainingId.ToObject<int>();
            Training training = TrainingService.Get(trainingid);

            TrainingTest test = Mapper.Map<TrainingTest>(data);
            test.Owners.Add(new TrainingTestOwner { User = AuthUser});

            foreach (TrainingUser tu in training.Users)
            {
                if (tu.Attendant) {
                    test.Users.Add(new TrainingTestUser { User = tu.User});
                    tu.TrainingCompleted = false;
                }
            }

            training.Tests.Add(test);

            if (TrainingService.Update(training) > 0)
            {
                return Json(data: new JsonResponse() { Result = true, Response = "OK", Content = data });
            }
            return Json(data: new JsonResponse() { Result = false, Response = "KO", Content = data });
        }

            #endregion
            #region DELETE

        [HttpGet]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        [Route("/[controller]/{trainingid:int}/test/deletecheck/{testid:int}")]
        public JsonResult DeleteTestCheck(int trainingid, int testid)
        {
            string response = "";
            bool ActionAllowed = true;

            Training training = TrainingService.Get(trainingid);
            TrainingTest test = training.Tests.SingleOrDefault(tt => tt.Id == testid);

            if (test.Users.Any(ttu => ttu.Takes.Count > 0))
            {
                ActionAllowed = false; response = Localizer["No es posible eliminar una prueba que ha sido realizada por un usuario."];
            }

            return Json(data: new JsonResponse() { Result = ActionAllowed, Response = response });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateAjaxModelState]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        [Route("/[controller]/{trainingid:int}/test/delete/{testid:int}")]
        public IActionResult DeleteTest(int trainingid, int testid)
        {
            Training training = TrainingService.Get(trainingid);
            if (training == null) return Error(StatusCodes.Status406NotAcceptable);

            TrainingTest test = training.Tests.SingleOrDefault(tt => tt.Id == testid);
            if (test == null) return Error(StatusCodes.Status406NotAcceptable);

            training.Tests.Remove(test);

            if (TrainingService.Update(training) > 0)
            {
                return Json(data: new JsonResponse() { Result = true, Response = "Eliminado con éxito", Content = new { Id = trainingid } });
            }
            return Error();
        }
    
            #endregion
            #region TAKE

        [HttpGet]
        [Route("/[controller]/{trainingid:int}/test/{testid:int}/take/confirm")]
        public IActionResult ConfirmTestTake(int trainingid, int testid)
        {
            Training training = TrainingService.Get(trainingid);
            if (training == null) return Error(StatusCodes.Status406NotAcceptable);

            TrainingTest test = training.Tests.SingleOrDefault(tt => tt.Id == testid);
            if (test == null) return Error(StatusCodes.Status406NotAcceptable);

            TrainingUser trainingUser = training.Users.SingleOrDefault(tu => tu.UserId == AuthUser.Id && tu.Attendant);
            if (trainingUser == null) return Error();

            TrainingTestUser testUser = test.Users.SingleOrDefault(ttu => ttu.UserId == AuthUser.Id);
            if (testUser == null) return Error();
            if (!(testUser.Takes.All(ttut => ttut.Pass == false))) { return Error(); }

            ConfirmTestTakeViewModel model = Mapper.Map<ConfirmTestTakeViewModel>(test);
            model.Attempt = testUser.Takes.Count() + 1;
            model.AttemptsExceeded = (model.Attempt > test.Attempts);

            return PartialView("_ConfirmTestTake", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateAjaxModelState]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        [Route("/[controller]/{trainingid:int}/test/{testid:int}/take/confirm")]
        public IActionResult ConfirmTestTake(ConfirmTestTakeViewModel model)
        {
            Training training = TrainingService.Get(model.TrainingId);
            if (training == null) return Error(StatusCodes.Status406NotAcceptable);

            TrainingTest test = training.Tests.SingleOrDefault(tt => tt.Id == model.TestId);
            if (test == null) return Error(StatusCodes.Status406NotAcceptable);

            TrainingUser trainingUser = training.Users.SingleOrDefault(tu => tu.UserId == AuthUser.Id && tu.Attendant);
            if (trainingUser == null) return Error();

            TrainingTestUser testUser = test.Users.SingleOrDefault(ttu => ttu.UserId == AuthUser.Id);
            if (testUser == null) return Error();

            TrainingTestUserTake take = new TrainingTestUserTake { StartDate = DateTime.Now };
            Mapper.Map(test.Sections, take.Sections);
            testUser.Takes.Add(take);
            
            if (TrainingService.Update(training) > 0)
                { return Json(new JsonResponse() { Result = true, Response = "Añadido con éxito", Content = new { take.Id } }); }
            return Error();
        }

        [HttpGet]
        [Route("/[controller]/{trainingid:int}/test/{testid:int}/take/{takeid:int}")]
        public IActionResult TakeTest(int trainingid, int testid, int takeid)
        {
            Training training = TrainingService.Get(trainingid);
            if (training == null) return Error(StatusCodes.Status406NotAcceptable);

            TrainingTest test = training.Tests.SingleOrDefault(tt => tt.Id == testid);
            if (test == null) return Error(StatusCodes.Status406NotAcceptable);

            TrainingTestUser testUser = test.Users.SingleOrDefault(ttu => ttu.UserId == AuthUser.Id);
            if (testUser == null) return Error(StatusCodes.Status406NotAcceptable);

            TrainingTestUserTake take = test.Users.SelectMany(ttu => ttu.Takes).SingleOrDefault(ttut => ttut.Id == takeid);
            if (take == null) return Error(StatusCodes.Status406NotAcceptable);
            if (!take.Active || take.Attempt > 0) return Error();

            take.Attempt = testUser.Takes.Count();

            if (!(TrainingService.Update(training) > 0)) return Error();

            TrainingTestTakeViewModel model = Mapper.Map<TrainingTestTakeViewModel>(test);
            model.TestUserId = testUser.Id;
            model.Attempt = testUser.Takes.Count();

            model.TakeId = take.Id;
            model.StartDate = take.StartDate;
            model.EndDate = take.EndDate;
            model.Pass = take.Pass;
            model.TestUserId = take.TestUserId;
            model.Attempt = take.Attempt;

            model.Sections.ToList().ForEach(section =>
            {
                section.Fields.ToList().ForEach(field =>
                {
                    field.Options.ToList().ForEach(option =>
                    {
                        option.TestOptionId = take.Sections.FirstOrDefault(s => s.TestSectionId == section.Id).Fields.FirstOrDefault(f => f.TestFieldId == field.Id).Options.FirstOrDefault(o => o.TestOptionId == option.Id).TestOptionId;
                        option.Id = take.Sections.FirstOrDefault(s => s.TestSectionId == section.Id).Fields.FirstOrDefault(f => f.TestFieldId == field.Id).Options.FirstOrDefault(o => o.TestOptionId == option.Id).Id;
                    });

                    field.TestFieldId = take.Sections.FirstOrDefault(s => s.TestSectionId == section.Id).Fields.FirstOrDefault(f => f.TestFieldId == field.Id).TestFieldId;
                    field.Id = take.Sections.FirstOrDefault(s => s.TestSectionId == section.Id).Fields.FirstOrDefault(f => f.TestFieldId == field.Id).Id;
                });

                section.TestSectionId = take.Sections.FirstOrDefault(s => s.TestSectionId == section.Id).TestSectionId;
                section.Id = take.Sections.FirstOrDefault(s => s.TestSectionId == section.Id).Id;
            });

            return View("TakeTest", model);
        }

        [HttpPost]
        [Route("/[controller]/{trainingid:int}/test/{testid:int}/take/{takeid:int}")]
        [ValidateAntiForgeryToken]
        public IActionResult TakeTest([FromBody] dynamic data)
        {
            int trainingid = data.TrainingId.ToObject<int>();
            int testid = data.TestId.ToObject<int>();
            int takeid = data.Id.ToObject<int>();

            Training training = TrainingService.Get(trainingid);
            TrainingTest test = training.Tests.SingleOrDefault(tt => tt.Id == testid);
            TrainingTestUserTake take = test.Users.SelectMany(ttu => ttu.Takes).Single(ttut => ttut.Id == takeid);

            take.Sections.Clear();
            if (TrainingService.Update(training) > 0)
            {
                Mapper.Map(data, take);
                if (TrainingService.Update(training) > 0)
                {
                    return Json(data: new JsonResponse() { Result = true, Response = "OK", Content = new { take.Id } });
                }
            }
            return Error();
        }
            #endregion
            #region SHOW_TAKE

        [HttpGet]
        [Route("/[controller]/{trainingid:int}/test/show/{testid:int}/take/{takeid:int}")]
        public IActionResult ShowTestTake(int trainingid, int testid, int takeid)
        {
            Training training = TrainingService.Get(trainingid);
            if (training == null) return Error(StatusCodes.Status406NotAcceptable);

            TrainingTest test = training.Tests.SingleOrDefault(tt => tt.Id == testid);
            if (test == null) return Error(StatusCodes.Status406NotAcceptable);

            TrainingTestUserTake take = test.Users.SelectMany(ttu => ttu.Takes).SingleOrDefault(ttut => ttut.Id == takeid);
            if (take == null || take.Active) return Error(StatusCodes.Status406NotAcceptable);

            TrainingTestTakeViewModel model = Mapper.Map<TrainingTestTakeViewModel>(test);

            model.TakeId = take.Id;
            model.StartDate = take.StartDate;
            model.EndDate = take.EndDate;
            model.Pass = take.Pass;

            model.Sections.ToList().ForEach(section =>
            {
                section.Fields.ToList().ForEach(field =>
                {
                    field.Mark = take.Sections.FirstOrDefault(s => s.TestSectionId == section.Id).Fields.FirstOrDefault(f => f.TestFieldId == field.Id).Mark;
                    field.Answer = take.Sections.FirstOrDefault(s => s.TestSectionId == section.Id).Fields.FirstOrDefault(f => f.TestFieldId == field.Id).Answer;

                    field.Options.ToList().ForEach(option =>
                    {
                        option.Checked = take.Sections.FirstOrDefault(s => s.TestSectionId == section.Id).Fields.FirstOrDefault(f => f.TestFieldId == field.Id).Options.FirstOrDefault(o => o.TestOptionId == option.Id).Mark;
                    });
                });
            });

            return View("ShowTestTake", model);
        }

            #endregion
            #region MARK_TAKE

        [HttpGet]
        [Route("/[controller]/{trainingid:int}/test/mark/{testid:int}/take/{takeid:int}")]
        public IActionResult MarkTestTake(int trainingid, int testid, int takeid)
        {
            Training training = TrainingService.Get(trainingid);
            if (training == null) return Error(StatusCodes.Status406NotAcceptable);

            TrainingTest test = training.Tests.SingleOrDefault(tt => tt.Id == testid);
            if (test == null) return Error(StatusCodes.Status406NotAcceptable);

            TrainingTestUserTake take = test.Users.SelectMany(ttu => ttu.Takes).SingleOrDefault(ttut => ttut.Id == takeid);
            if (take == null) return Error(StatusCodes.Status406NotAcceptable);

            if (take.Active)
            {
                if ((DateTime.Now - take.StartDate).TotalMinutes > (test.Time ?? 120))
                {
                    take.Active = false;
                    take.Content = "Finalizada por exceso de tiempo";
                    if (!(TrainingService.Update(training) > 0)) { return Error(); }
                } else { return Error(StatusCodes.Status406NotAcceptable); }
            }

            TrainingTestTakeViewModel model = Mapper.Map<TrainingTestTakeViewModel>(test);

            model.TakeId = take.Id;
            model.StartDate = take.StartDate;
            model.EndDate = take.EndDate;
            model.Pass = take.Pass;
            model.TestUserId = take.TestUserId;
            model.Attempt = take.Attempt;

            model.Sections.ToList().ForEach(section =>
            {
                section.Fields.ToList().ForEach(field =>
                {
                    field.Options.ToList().ForEach(option =>
                    {
                        option.Checked = take.Sections.FirstOrDefault(s => s.TestSectionId == section.Id).Fields.FirstOrDefault(f => f.TestFieldId == field.Id).Options.FirstOrDefault(o => o.TestOptionId == option.Id).Mark;
                        option.TestOptionId = take.Sections.FirstOrDefault(s => s.TestSectionId == section.Id).Fields.FirstOrDefault(f => f.TestFieldId == field.Id).Options.FirstOrDefault(o => o.TestOptionId == option.Id).TestOptionId;
                        option.Id = take.Sections.FirstOrDefault(s => s.TestSectionId == section.Id).Fields.FirstOrDefault(f => f.TestFieldId == field.Id).Options.FirstOrDefault(o => o.TestOptionId == option.Id).Id;
                    });

                    field.Mark = take.Sections.FirstOrDefault(s => s.TestSectionId == section.Id).Fields.FirstOrDefault(f => f.TestFieldId == field.Id).Mark;
                    field.Answer = take.Sections.FirstOrDefault(s => s.TestSectionId == section.Id).Fields.FirstOrDefault(f => f.TestFieldId == field.Id).Answer;
                    field.TestFieldId = take.Sections.FirstOrDefault(s => s.TestSectionId == section.Id).Fields.FirstOrDefault(f => f.TestFieldId == field.Id).TestFieldId;
                    field.Id = take.Sections.FirstOrDefault(s => s.TestSectionId == section.Id).Fields.FirstOrDefault(f => f.TestFieldId == field.Id).Id;
                });

                section.TestSectionId = take.Sections.FirstOrDefault(s => s.TestSectionId == section.Id).TestSectionId;
                section.Id = take.Sections.FirstOrDefault(s => s.TestSectionId == section.Id).Id;
            });

            return View("MarkTestTake", model);
        }

        [HttpPost]
        [Route("/[controller]/{trainingid:int}/test/mark/{testid:int}/take/{takeid:int}")]
        [ValidateAntiForgeryToken]
        public IActionResult MarkTestTake([FromBody] dynamic data)
        {
            int trainingid = data.TrainingId.ToObject<int>();
            int testid = data.TestId.ToObject<int>();
            int takeid = data.Id.ToObject<int>();

            Training training = TrainingService.Get(trainingid);
            TrainingTest test = training.Tests.SingleOrDefault(tt => tt.Id == testid);
            TrainingTestUserTake take = test.Users.SelectMany(ttu => ttu.Takes).Single(ttut => ttut.Id == takeid);

            take.Sections.Clear();
            if (TrainingService.Update(training) > 0)
            {
                Mapper.Map(data, take);
                take.Mark = take.Sections.Sum(ts => ts.Mark);
                take.Pass = take.Mark >= data.PassMark.ToObject<double>();

                User user = take.TestUser.User;
                IEnumerable<TrainingTestUser> usertests = training.Tests.SelectMany(tt => tt.Users).Where(ttu => ttu.User == user);
                if (usertests.All(ttu => ttu.Takes.Any(ttut => ttut.Pass == true)))
                {
                    training.Users.Single(tu => tu.User == user).TrainingCompleted = true;
                }

                if (TrainingService.Update(training) > 0)
                {
                    return Json(data: new JsonResponse() { Result = true, Response = "OK", Content = new { take.Id } });
                }
            }
            return Error();
        }

            #endregion
        #endregion
    }
}

