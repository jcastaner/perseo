﻿using AutoMapper;
using Perseo.Core;
using Perseo.Web.Helpers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Localization;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Perseo.Web.Controllers
{

    [Authorize(AuthenticationSchemes = "Identity.Application")]
    public abstract class BaseController : Controller
    {
        protected readonly bool IsAjax;
        protected readonly int AuthUserId;
        protected readonly User AuthUser;

        protected readonly string WebRootPath;
        protected readonly string ContentRootPath;
        protected readonly string StaticPath;

        protected readonly IHostingEnvironment Enviroment;
        protected readonly IHttpContextAccessor ContextAccessor;
        protected readonly IUserService UserService;
        protected readonly IStringLocalizer<SharedResources> Localizer;
        protected readonly IMapper Mapper;

        protected int ErrorCode { get; set; }
        
        protected BaseController( IHttpContextAccessor contextAccessor, IHostingEnvironment enviroment, IUserService userService, IStringLocalizer<SharedResources> localizer, IMapper mapper)
        {
            ErrorCode = 500;
            Mapper = mapper;
            Localizer = localizer;
            Enviroment = enviroment;
            WebRootPath = enviroment.WebRootPath;
            ContentRootPath = enviroment.ContentRootPath;
            StaticPath = ContentRootPath + "/App/Static/";
            UserService = userService;
            ContextAccessor = contextAccessor;
            IsAjax = ContextAccessor.HttpContext.Request.IsAjax();
            if (ContextAccessor.HttpContext.User.Identity.IsAuthenticated)
            {
                AuthUser = UserService.GetAuthUser(ContextAccessor.HttpContext.User.Identity.Name);
                if (AuthUser.RefreshCookie.HasValue && AuthUser.RefreshCookie.Value)
                {
                    AuthUser.SecurityStamp = Guid.NewGuid().ToString();
                    AuthUser.RefreshCookie = false;
                    UserService.Update(AuthUser);
                    var task = ContextAccessor.HttpContext.RefreshLoginAsync();
                }
            }
        }
        
        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            base.OnActionExecuted(filterContext);

            if (AuthUser != null && AuthUser.ChangePassword.HasValue && AuthUser.ChangePassword.Value == true)
            {
                string actionName = ControllerContext.RouteData.Values["action"].ToString();
                string controllerName = ControllerContext.RouteData.Values["controller"].ToString();
                if (!(controllerName == "Account" && actionName == "ChangePasswordMandatory"))
                {
                    filterContext.Result = new RedirectResult("/account/changepassword/mandatory");
                    return;
   
                }
            }
            ViewBag._Enviroment = Enviroment.EnvironmentName;
            ViewBag._Area = "";
            ViewBag._Action = filterContext.ActionDescriptor.RouteValues["action"].ToLower();
            ViewBag._Controller = filterContext.ActionDescriptor.RouteValues["controller"].ToLower();

            if (ContextAccessor.HttpContext.User.Identity.IsAuthenticated)
            {
                ViewBag._AuthUser = AuthUser;
                ViewBag._AuthUserIsSigned = true;
                ViewBag._AuthUserId = AuthUser.Id;
                ViewBag._AuthUserName = AuthUser.FirstName + " " + AuthUser.MainSurname;
                ViewBag._AuthUserAvatarUrl = "/static/avatars/" + AuthUser.Avatar;
                ViewBag._AuthUserAlerts = AuthUser.Alerts;
                ViewBag._AuthUserMessages = AuthUser.Messages;
                ViewBag._AuthUserTasks = AuthUser.Tasks;
                ViewBag._AuthUserRoles = AuthUser.Roles.Select(x => x.Name).Distinct();
            }
        }

        #region HELPERS

        protected List<int> AreaFilter(IEnumerable<Area> areas, string selectedareastring)
        {
            List<int> MyAreasIds = areas.Select(a => a.Id).ToList();
            List<int> AreasIds = MyAreasIds;
            if (selectedareastring != null)
            {
                try
                {
                    AreasIds = selectedareastring.Split(";").Select(Int32.Parse).ToList();
                    AreasIds.RemoveAll(a => !MyAreasIds.Contains(a));
                }
                catch (Exception)
                {
                    AddError(StatusCodes.Status404NotFound);
                }
            }
            return AreasIds;
        }

        protected List<int> JobFilter(IEnumerable<Job> jobs, string selectedjobstring)
        {
            List<int> MyJobsIds = jobs.Select(a => a.Id).ToList();
            List<int> JobsIds = MyJobsIds;
            if (selectedjobstring != null)
            {
                try
                {
                    JobsIds = selectedjobstring.Split(";").Select(Int32.Parse).ToList();
                    JobsIds.RemoveAll(a => !MyJobsIds.Contains(a));
                }
                catch (Exception)
                {
                    AddError(StatusCodes.Status404NotFound);
                }
            }
            return JobsIds;
        }

        protected IEnumerable<Area> CurrentUserOwnedAreas(DateTime? StartDate = null, DateTime? EndDate = null)
        {
            if (AuthUserId > 0)
            {
                return UserService.GetOwnedAreas(AuthUser, StartDate, EndDate);
            }
            return Enumerable.Empty<Area>();
        }

        protected IEnumerable<Job> CurrentUserOwnedJobs(DateTime? StartDate = null, DateTime? EndDate = null)
        {
            if (AuthUserId > 0)
            {
                return UserService.GetOwnedJobs(AuthUser, StartDate, EndDate);
            }
            return Enumerable.Empty<Job>();
        }

        protected void AddError(int code)
        {
            ErrorCode = code;
        }

        protected IActionResult Error(int errorCode = 0, string customMessage = "")
        {
            errorCode = errorCode == 0 ? ErrorCode : errorCode;
            string errorString = customMessage == "" ? ErrorString(errorCode) : customMessage;
            if (IsAjax) {
                JsonResponse response = new JsonResponse() { Result = false, Response = errorString, Content = null };
                return new JsonResult(response) { StatusCode = errorCode };
            }
            else
            {
                Response.StatusCode = errorCode;
                ViewBag.ErrorString = errorString;
                ViewBag.ErrorCustomMessage = customMessage;
                return View("~/Views/Shared/Error.cshtml");
            }
        }

        protected JsonResult JsonError(int errorCode = 0, string customMessage = "")
        {
            errorCode = errorCode == 0 ? ErrorCode : errorCode;
            string errorString = customMessage == "" ? ErrorString(errorCode) : customMessage;
            Response.StatusCode = errorCode;
            JsonResponse response = new JsonResponse() { Result = false, Response = errorString, Content = null };
            return Json(response);
          
        }

        private string ErrorString(int errorCode)
        {
            string errorString = "";
            switch (errorCode)
            {
                case 400: errorString = Localizer["La petición realizada al servidor es incorrecta"] + " (400)"; break;
                case 401: errorString = Localizer["No está autorizado a acceder al recurso solicitado"] + " (401)"; break;
                case 403: errorString = Localizer["No tiene los privilegios para acceder al recurso solicitado"] + " (403)"; break;
                case 404: errorString = Localizer["No se ha encontrado el recurso solicitado"] + " (404)"; break;
                case 405: errorString = Localizer["El recurso no admite el método utilizado"] + " (405)"; break;
                case 406: errorString = Localizer["La petición realizada no es aceptable y no puede ser atendida"] + " (406)"; break;
                case 408: errorString = Localizer["Se ha agotado el tiempo máximo para atender la solicitud"] + " (408)"; break;
                case 410: errorString = Localizer["El recurso solicitado no está disponible"] + " (410)"; break;
                case 418: errorString = Localizer["¡Has roto Internet!"] + " (418)"; break;
                case 500: errorString = Localizer["Se ha producido un error interno del servidor"] + " (500)"; break;
                default: errorString = Localizer["Se ha producido un error"] + " (" + errorCode.ToString() + ")"; break;
            }
            return errorString;
        }

        #endregion
    }
}
