using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Perseo.Core;
using Perseo.Web.Helpers;
using Perseo.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Perseo.Web.Controllers
{
    [Route("[controller]/[action]/{id:int?}")]
    public class NotificationController : BaseController
    {
        private readonly INotificationService NotificationService;

        public NotificationController(IHttpContextAccessor contextAccessor, IHostingEnvironment enviroment, IStringLocalizer<SharedResources> localizer,
            IUserService userService, INotificationService notificationService,
            IMapper mapper) : base(contextAccessor, enviroment, userService, localizer, mapper)
        {
            NotificationService = notificationService;
        }

        [HttpGet]
        [Route("/[controller]")]
        public IActionResult Index()
        {
            return List();
        }

        [HttpGet]
        [Route("/[controller]/[action]/{type?}")]
        public IActionResult List(NotificationType? type = null)
        {
            List<NotificationViewModel> modelList = new List<NotificationViewModel>();
            List<Notification> notifications = NotificationService.GetAllFromUser(AuthUser.Id, true, type).ToList();
            foreach(var notification in notifications)
            {
                NotificationViewModel model = new NotificationViewModel()
                {
                    Id = notification.Id,
                    Summary = notification.Summary,
                    Description = notification.Description,
                    Type = notification.Type,
                    Priority = notification.Priority,
                    Active = notification.Active,
                    Url = notification.Url
                };
                modelList.Add(model);
            }
            ViewBag.Type = type;
            return PartialView("_List", modelList);
        }

        [HttpGet]
        public IActionResult Dismiss(int id)
        {
            if (id > 0)
            {
                Notification notification = NotificationService.Get(id);
                if (notification.UserId == AuthUser.Id)
                {
                    if (NotificationService.Dismiss(notification))
                    {
                        return StatusCode(StatusCodes.Status200OK);
                    }
                    return StatusCode(StatusCodes.Status500InternalServerError);
                }
                return Error(StatusCodes.Status401Unauthorized);
            }
            return Error(StatusCodes.Status404NotFound);
        }

        [HttpGet]
        [AuthorizeRoles(AppRole.Administration, AppRole.Maintenance)]
        public IActionResult Add()
        {
            NotificationViewModel model = new NotificationViewModel();
            return View("Add", model);
        }

        [HttpPost]
        [AuthorizeRoles(AppRole.Administration, AppRole.Maintenance)]
        [ValidateAntiForgeryToken]
        public IActionResult Add(NotificationViewModel model)
        {
            if (!ModelState.IsValid) return Error(StatusCodes.Status406NotAcceptable);
            User user = UserService.Get(model.UserId);
            if (user == null) return Error(StatusCodes.Status404NotFound);
            if (user.LastUserArea == null) return Error(StatusCodes.Status500InternalServerError); 
            if (!(AuthUser.OwnedAreas.Any(a => a.Id == user.LastUserArea.AreaId))) return Error(StatusCodes.Status401Unauthorized);
            Notification notification = new Notification()
            {
                User = user,
                UserId = user.Id,
                Description = model.Description,
                Summary = model.Summary,
                StartDate = DateTime.Now,
                Active = true,
                Priority = model.Priority,
                Url = model.Url
            };
            NotificationService.Add(notification);
            if (notification.Id > 0)
            {
                switch (notification.Type)
                {
                    case NotificationType.alert: user.Alerts++; break;
                    case NotificationType.task: user.Tasks++; break;
                    case NotificationType.message: user.Messages++; break;
                }
            }
            UserService.Update(user);
            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult Query()
        {
            if (AuthUser.Alerts + AuthUser.Tasks + AuthUser.Messages >= 1)
            {
                return Json(new JsonResponse() { Result = true, Response = "Hay nuevas notificaciones", Content = new { tasks = AuthUser.Tasks, messages = AuthUser.Messages, alerts = AuthUser.Alerts} } );
            }
            return Json(new JsonResponse() { Result = false, Response = "No hay nuevas notificaciones", Content = new { task = 0, message = 0, alert = 0 } });
        }
    }
}
