using Perseo.Core;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Localization;
using AutoMapper;

namespace Perseo.Web.Controllers
{
    [Route("[controller]/[action]/{id:int?}")]
    public class AssetController : BaseController
    {
        public AssetController( IHttpContextAccessor contextAccessor,  IHostingEnvironment enviroment,
            IUserService userService, IStringLocalizer<SharedResources> localizer,
            IMapper mapper) : base(contextAccessor, enviroment, userService, localizer, mapper) { }

        [HttpGet]
        [Route("/[controller]")]
        public IActionResult Index()
        {
            return List();
        }

        [HttpGet]
        public IActionResult List()
        {
            return View("List");
        }

    }
}
