using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Perseo.Core;
using Perseo.Web.Helpers;
using Perseo.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Perseo.Web.Controllers
{
    [Route("[controller]/[action]/{id:int?}")]
    public class UserController : BaseController
    {
        #region CONSTRUCTOR

        private readonly IService<Company> CompanyService;
        private readonly IService<Office> OfficeService;
        private readonly IService<Category> CategoryService;
        private readonly IService<Collective> CollectiveService;
        private readonly IService<Shift> ShiftService;
        private readonly IService<Rate> RateService;
        private readonly IService<Quality> QualityService;
        private readonly IService<Training> TrainingService;
        private readonly IAreaService AreaService;
        private readonly IScheduleService ScheduleService;
        private readonly IRequestService RequestService;
        private readonly IRoleService RoleService;

        public UserController(
            IHttpContextAccessor contextAccessor, 
            IHostingEnvironment enviroment,
            IStringLocalizer<SharedResources> localizer,
            IService<Company> companyService,
            IService<Office> officeService,
            IService<Category> categoryService,
            IService<Collective> collectiveService,
            IService<Shift> shiftService,
            IService<Rate> rateService,
            IService<Quality> qualityService,
            IService<Training> trainingService,
            IScheduleService scheduleService,
            IRoleService roleService,
            IAreaService areaService,
            IRequestService requestService,
            IUserService userService,
            IMapper mapper) : base(contextAccessor, enviroment, userService, localizer, mapper)
        {
            CompanyService = companyService;
            OfficeService = officeService;
            CategoryService = categoryService;
            CollectiveService = collectiveService;
            RoleService = roleService;
            ShiftService = shiftService;
            RateService = rateService;
            QualityService = qualityService;
            TrainingService = trainingService;
            RequestService = requestService;
            ScheduleService = scheduleService;
            AreaService = areaService;
        }

        #endregion

        #region HELPERS

        protected bool Authorize(User user)
        {
            if (!ModelState.IsValid) { AddError(StatusCodes.Status406NotAcceptable); return false; }
            if (user == null) { AddError(StatusCodes.Status404NotFound); return false; }
            if (user.LastUserArea == null) { AddError(StatusCodes.Status500InternalServerError); return false; }
            if (!(AuthUser.OwnedAreas.Any(a => a.Id == user.LastUserArea.AreaId))) { AddError(StatusCodes.Status401Unauthorized); return false; }
            return true;
        }

        protected bool AuthorizeAjax(User user)
        {
            if (!IsAjax) { AddError(StatusCodes.Status400BadRequest); return false; }
            return Authorize(user);
        }

        #endregion

        #region CRUD

        #region List

        [HttpGet]
        [Route("/[controller]")]
        [AuthorizeRoles(AppRole.OperationalManagementEdit, AppRole.OperationalManagementShow)]
        public IActionResult Index()
        {
            return List();
        }

        [HttpGet]
        [Route("/[controller]/[action]/{active?}/{selectedareas?}/{selectedoffice?}")]
        [AuthorizeRoles(AppRole.OperationalManagementEdit, AppRole.OperationalManagementShow)]
        public IActionResult List(ActiveType? active = null, string selectedareas = null, string selectedoffice = null)
        {
            List<Area> authUserOwnedAreas = AuthUser.OwnedAreas.ToList();
            List<int> areaFilterIds = AreaFilter(authUserOwnedAreas, selectedareas);

            List<Office> areasOffices = AreaService.GetMany(a => areaFilterIds.Contains(a.Id)).SelectMany(a => a.UserArea).Where(ua => ua.Active).Select(ua => ua.User).SelectMany(u => u.UserOffice).Where(uo => uo.Active).Select(uo => uo.Office).Distinct().ToList();
            bool nullOffices = AreaService.GetMany(a => areaFilterIds.Contains(a.Id)).SelectMany(a => a.UserArea).Where(ua => ua.Active).Select(ua => ua.User).Any(u => !(u.UserOffice.Any(uo => uo.Active)));
            if (nullOffices)
            {
                areasOffices.Insert(0, new Office() { Id = 0, Name = Localizer["No informada"] });
            }
            List<Office> selectedOffices = new List<Office>().ToList();

            PagedResult<UserListNode> pagedResult = new PagedResult<UserListNode>();

            if (selectedoffice != null)
            {
                IEnumerable<int> officeFilterIds = selectedoffice.Split(';').Select(int.Parse);
                selectedOffices = areasOffices.Where(o => officeFilterIds.Contains(o.Id)).ToList();
                pagedResult = UserService.ListPaged(areaFilterIds, active, null, null, null, null, officeFilterIds.ToList());
            }
            else
            {
                pagedResult = UserService.ListPaged(areaFilterIds, active);
            }
            
            #region CARD DATA

            IEnumerable<User> UsersFromAreas = UserService.GetMany(u => (u.UserArea.Select(ua => ua.AreaId)).Intersect(areaFilterIds).Any());
            IEnumerable<User> ActiveUsers = UsersFromAreas.Where(u => u.Active = true).Distinct();
            IEnumerable<User> ManagementUsers = ActiveUsers.Where(u => u.UserCategory.Any(uc => uc.Active && uc.CategoryId >= 7));
            IEnumerable<User> ExternalUsers = ActiveUsers.Where(u => u.UserContract.All(uc => uc.Active && uc.External));
            IEnumerable<User> ActiveInternalUsers = ActiveUsers.Where(u => u.UserContract.Any(uc => uc.Active && uc.External == false));
            double totalSeniority = 0;
            foreach (User employee in ActiveInternalUsers)
            {
                totalSeniority += DateTime.Now.Subtract(employee.LastUserContract.StartDate).TotalDays / 365;
            }

            IEnumerable<User> AreasInternalUsers = UsersFromAreas.Where(u => u.UserContract.Any(uc => uc.Active && uc.External == false));
            double MonthRegistrations = AreasInternalUsers.SelectMany(u => u.UserContract).Where(uc => uc.StartDate >= DateTime.Now.AddMonths(-1)).Count();
            double MonthWithdrawals = AreasInternalUsers.SelectMany(u => u.UserContract).Where(uc => uc.EndDate >= DateTime.Now.AddMonths(-1)).Count();
            double MonthAverageEmployees = ((ActiveUsers.Count() - MonthRegistrations + MonthWithdrawals) + ActiveUsers.Count()) / 2;
            double MonthStaffTurnover = (((MonthRegistrations + MonthWithdrawals) / 2) * 100) / MonthAverageEmployees;
            
            CardData Card1 = new CardData()
            {
                Label = Localizer["Equipo Total"],
                MainValue = ActiveUsers.Count().ToString(),
                SecondaryValue = Localizer["Colaboradores"]
            };
            CardData Card2 = new CardData()
            {
                Label = Localizer["Equipo de Gestión"],
                MainValue = ManagementUsers.Count().ToString(),
                SecondaryValue = Math.Round((((double)ManagementUsers.Count() / ActiveUsers.Count()) * 100), 2).ToString() + "%"
            };
            CardData Card3 = new CardData()
            {
                Label = Localizer["Equipo de Producción"],
                MainValue = (ActiveUsers.Count() - ManagementUsers.Count()).ToString(),
                SecondaryValue = Math.Round(((1 - ((double)ManagementUsers.Count() / ActiveUsers.Count())) * 100), 2).ToString() + "%"
            };
            CardData Card4 = new CardData()
            {
                Label = Localizer["Personal Externo"],
                MainValue = ExternalUsers.Count().ToString(),
                SecondaryValue = Math.Round((((double)ExternalUsers.Count() / ActiveUsers.Count()) * 100), 2).ToString() + "%"
            };
            CardData Card5 = new CardData()
            {
                Label = Localizer["Antigüedad Media"],
                MainValue = Math.Round(totalSeniority / ActiveInternalUsers.Count(), 2) .ToString(),
                SecondaryValue = Localizer["Años"]
            };
            CardData Card6 = new CardData()
            {
                Label = Localizer["Rotación Mensual"],
                MainValue = Math.Round(MonthStaffTurnover, 2).ToString() + "%",
                SecondaryValue = ""
            };

            #endregion

            UserListViewModel model = new UserListViewModel
            {
                Cards = new List<CardData>() { Card1, Card2, Card3, Card4, Card5, Card6},
                Data = Mapper.Map<List<UserListNodeModel>>(pagedResult.Results.ToList()),
                Pagination = Mapper.Map<PaginationModel>(pagedResult),
                OwnedAreas = Mapper.Map<List<AreaModel>>(authUserOwnedAreas),
                SelectedAreasIds = areaFilterIds,
                Active = active
            };

            ViewBag.SelectedOffices = Mapper.Map<List<OfficeModel>>(selectedOffices);
            ViewBag.AreasOffices = Mapper.Map<List<OfficeModel>>(areasOffices);

            return View("List", model);
        }

        [HttpGet]
        [Route("/[controller]/[action]/{active?}/{selectedareas?}")]
        [AuthorizeRoles(AppRole.OperationalManagementEdit, AppRole.OperationalManagementShow)]
        public JsonResult ListData(DataTablesRequest request, ActiveType? active = null, string selectedareas = null)
        {
            List<Area> authUserOwnedAreas = AuthUser.OwnedAreas.ToList();
            List<int> areaFilterIds = AreaFilter(authUserOwnedAreas, selectedareas);

            string search = request.search["value"];
            string order = request.Order(new List<string> { "avatar", "name", "email", "username", "employee_id", "area" });

            var pagedResult = UserService.ListPaged(areaFilterIds, active, request.page, request.pageSize, search, order);

            pagedResult.FilteredRowCount = search != null ? pagedResult.Results.Count() : pagedResult.RowCount;

            DataTablesResponse response = new DataTablesResponse(request.draw, pagedResult.RowCount, pagedResult.FilteredRowCount);
          
            pagedResult.Results.ToList().ForEach(u =>
            {
                response.Add(
                   u.Id,
                   "<img class='avatar img-circle' rel='popover' src='/static/avatars/" + u.Avatar + "' data-original-title='' title=''>",
                   u.FirstName + " " + u.MainSurname + " " + u.SecondSurname,
                   u.Email,
                   u.UserName,
                   u.EmployeeId,
                   u.Area );
            });

            return Json(response);
        }

        #endregion

        #region Show

        [HttpGet]
        [Route("~/account/profile/{tab?}/{year?}/{month?}")]
        public IActionResult Profile(string tab = null, int? year = null, int? month = null)
        {
            return Show(null, tab, year, month);
        }

        [HttpGet]
        [Route("~/[controller]/[action]/{id}/{tab?}/{year?}/{month?}")]
        [AuthorizeRoles(AppRole.OperationalManagementEdit, AppRole.OperationalManagementShow)]
        public IActionResult Show(int? id = null, string tab = null, int? year = null, int? month = null)
        {
            User user;
            if (id != null)
            {
                user = UserService.Get(id);
                if (!Authorize(user)) return Error();
                ViewBag._Profile = false;
            }
            else
            {
                user = AuthUser;
                ViewBag._Profile = true;
            }

            year = year ?? DateTime.Now.Year;
            month = month ?? DateTime.Now.Month;

            string SubTitle = user.LastUserArea != null ? user.LastUserArea.Area.Name : "";
            SubTitle += user.LastUserPosition != null ? " - " + user.LastUserPosition.Name : "";
            SubTitle += user.LastUserCategory != null ? " ( " + user.LastUserCategory.Category.Name + " )" : "";
            tab = tab ?? "main";
            tab = tab.ToLower();

            ViewBag.SubTitle = SubTitle.Trim();
            ViewBag.Avatar = user.Avatar;
            ViewBag.UserId = user.Id;
            ViewBag.FullName = user.FullName;
            ViewBag.Year = year;
            ViewBag.Month = month;
            ViewBag.Tab = tab;

            switch (tab)
            {
                case "main": return ShowMain(user);
                case "dashboard": return ShowDashboard(user, year);
                case "schedule": return ShowSchedule(user, year);
                case "requests": return ShowRequests(user, year);
                case "training": return ShowTraining(user, year);
                case "quality": return ShowQuality(user, year);
                case "dedications": return ShowDedications(user, year, month);
                case "operational": return ShowOperational(user);
                default: return Error(StatusCodes.Status404NotFound);
            }
        }

        private IActionResult ShowMain(User user)
        {
            UserViewModel model = Mapper.Map<UserViewModel>(user);
            return View("ShowMain", model);
        }

        private IActionResult ShowDashboard(User user, int? year = null)
        {
            // dashboard
            year = year ?? DateTime.Now.Year;

            #region TABLE

            UserDashBoardViewModel model = new UserDashBoardViewModel() { };
            IEnumerable<Job> productiveJobs = user.OwnedAreas.SelectMany(a => a.Job).Where(j => j.Type == JobType.Productive);

            IEnumerable<ScheduleDateTermJob> sdtjs = user.ScheduleDate.Where(sd => sd.Year == year.Value).SelectMany(sd => sd.ScheduleDateTerm).SelectMany(sdt => sdt.ScheduleDateTermJob);
            IEnumerable<Job> generalJobs = sdtjs.Select(s => s.Job).Where(j => j.Type != JobType.Productive && j.Type != JobType.General && j.Type != JobType.Organizational && j.Type != JobType.Project).Distinct();
            IEnumerable<JobType> JobTypes = generalJobs.Select(j => j.Type).Distinct();

            foreach (JobType type in JobTypes)
            {
                GeneralJobType generalJobType = new GeneralJobType
                {
                    Acronym = generalJobs.First(x => x.Type == type).Acronym,
                    Days = Enumerable.Repeat(0.0, 13).ToList(),
                    Hours = Enumerable.Repeat(0.0, 13).ToList()
                };
                foreach (Job item in generalJobs.Where(x => x.Type == type))
                {
                    GeneralJob generalJob = new GeneralJob() { Id = item.Id, Name = item.Name };
                    generalJob.Days = Enumerable.Repeat(0.0, 13).ToList();
                    generalJob.Hours = Enumerable.Repeat(0.0, 13).ToList();

                    double days = sdtjs.Where(sdtj => sdtj.JobId == item.Id).Count();
                    double hours = sdtjs.Where(sdtj => sdtj.JobId == item.Id).Select(sdtj => sdtj.ScheduleDateTerm).Select(sdt => sdt.Hours).Sum();

                    generalJobType.Days[0] += days;
                    generalJobType.Hours[0] += hours;
                    generalJob.Days[0] = days;
                    generalJob.Hours[0] = hours;

                    for (int i = 1; i <= 12; i++)
                    {
                        days = sdtjs.Where(sdtj => sdtj.JobId == item.Id).Where(sdtj => sdtj.ScheduleDateTerm.ScheduleDate.Month == i).Count();
                        hours = sdtjs.Where(sdtj => sdtj.JobId == item.Id).Where(sdtj => sdtj.ScheduleDateTerm.ScheduleDate.Month == i).Select(sdtj => sdtj.ScheduleDateTerm).Select(sdt => sdt.Hours).Sum();

                        generalJobType.Days[i] += days;
                        generalJobType.Hours[i] += hours;
                        generalJob.Days[i] = days;
                        generalJob.Hours[i] = hours;
                    }
                    generalJobType.GeneralJobs.Add(generalJob);
                }
                model.GeneralJobTypes.Add(type, generalJobType);
            }

            GeneralJobType productiveJobType = new GeneralJobType
            {
                Acronym = "T",
                Days = Enumerable.Repeat(0.0, 13).ToList(),
                Hours = Enumerable.Repeat(0.0, 13).ToList()
            };
            foreach (Job item in productiveJobs)
            {
                GeneralJob productiveJob = new GeneralJob() { Id = item.Id, Name = item.Name };
                productiveJob.Days = Enumerable.Repeat(0.0, 13).ToList();
                productiveJob.Hours = Enumerable.Repeat(0.0, 13).ToList();

                double days = sdtjs.Where(sdtj => sdtj.JobId == item.Id).Count();
                double hours = sdtjs.Where(sdtj => sdtj.JobId == item.Id).Select(sdtj => sdtj.ScheduleDateTerm).Select(sdt => sdt.Hours).Sum();

                productiveJobType.Days[0] += days;
                productiveJobType.Hours[0] += hours;
                productiveJob.Days[0] = days;
                productiveJob.Hours[0] = hours;

                for (int i = 1; i <= 12; i++)
                {
                    days = sdtjs.Where(sdtj => sdtj.JobId == item.Id).Where(sdtj => sdtj.ScheduleDateTerm.ScheduleDate.Month == i).Count();
                    hours = sdtjs.Where(sdtj => sdtj.JobId == item.Id).Where(sdtj => sdtj.ScheduleDateTerm.ScheduleDate.Month == i).Select(sdtj => sdtj.ScheduleDateTerm).Select(sdt => sdt.Hours).Sum();

                    productiveJobType.Days[i] += days;
                    productiveJobType.Hours[i] += hours;
                    productiveJob.Days[i] = days;
                    productiveJob.Hours[i] = hours;
                }
                productiveJobType.GeneralJobs.Add(productiveJob);
            }
            model.GeneralJobTypes.Add(JobType.Productive, productiveJobType);

            #endregion
            
            #region CARD DATA

            double YearWorkHours = user.LastUserWorkConditions != null ? user.LastUserWorkConditions.YearWorkHours : 0;
            double YearExtraHoursLimit = 80;
            double YearExtraHours = user.ScheduleDate.Where(sd => sd.Year == year.Value).SelectMany(sd => sd.ScheduleDateTerm).Where(sdt => sdt.HoursType == WorkHoursType.Extra).Select(sdt => sdt.Hours).Sum();
            double YearWorkedHours = model.GeneralJobTypes.Single(j => j.Key == JobType.Productive).Value.Hours.First() - YearExtraHours;

            double YearTotalDays = model.GeneralJobTypes.Select(j => j.Value).Select(p => p.Days).Select(a => a.ElementAt(0)).Sum();
            double YearSpareDays = user.LastUserWorkConditions != null ? user.LastUserWorkConditions.YearSpareDays : 0;
            double YearVacationDays = user.LastUserWorkConditions != null ? user.LastUserWorkConditions.YearVacationDays : 0;

            double YearAbsenteeismDays = model.GeneralJobTypes.SingleOrDefault(j => j.Key == JobType.UnjustifiedAbsence).Value != null ? model.GeneralJobTypes.Single(j => j.Key == JobType.UnjustifiedAbsence).Value.Days.First() : 0;
            double YearSparedDays = model.GeneralJobTypes.SingleOrDefault(j => j.Key == JobType.FreeDays).Value != null ? model.GeneralJobTypes.Single(j => j.Key == JobType.FreeDays).Value.Days.First() : 0;
            double YearSickDays = model.GeneralJobTypes.SingleOrDefault(j => j.Key == JobType.SickLeave).Value != null ? model.GeneralJobTypes.Single(j => j.Key == JobType.SickLeave).Value.Days.First() : 0;
            double YearHolyDays = model.GeneralJobTypes.SingleOrDefault(j => j.Key == JobType.Hollydays).Value != null ? model.GeneralJobTypes.Single(j => j.Key == JobType.Hollydays).Value.Days.First() : 0;

            CardData Card1 = new CardData()
            {
                Label = Localizer["Horas Efectivas"],
                MainValue = Math.Round((YearWorkedHours / YearWorkHours) * 100, 2).ToString(),
                SecondaryValue = Math.Round(YearWorkedHours, 2).ToString() + " " + Localizer["De"].ToString().ToLower() + " " + YearWorkHours.ToString() + " h",
                BarValue = (int)Math.Min(100, (YearWorkedHours / YearWorkHours) * 100)
            };
            CardData Card2 = new CardData()
            {
                Label = Localizer["Horas Extra"],
                MainValue = Math.Round((YearExtraHours / YearExtraHoursLimit) * 100, 2).ToString(),
                SecondaryValue = Math.Round(YearExtraHours, 2).ToString() + " " + Localizer["De"].ToString().ToLower() + " " + YearExtraHoursLimit.ToString() + " h",
                BarValue = (int)Math.Min(100, (YearExtraHours / YearExtraHoursLimit) * 100)
            };
            CardData Card3 = new CardData()
            {
                Label = Localizer["Absentismo"],
                MainValue = Math.Round((YearAbsenteeismDays / YearTotalDays) * 100, 2).ToString(),
                SecondaryValue = Math.Round(YearAbsenteeismDays, 2).ToString() + " " + Localizer["días"],
                BarValue = (int)Math.Min(100, Math.Round((YearAbsenteeismDays / YearTotalDays) * 100, 2))
            };
            CardData Card4 = new CardData()
            {
                Label = Localizer["Libre Disposición"],
                MainValue = Math.Round((YearSparedDays / YearSpareDays) * 100, 2).ToString(),
                SecondaryValue = Math.Round(YearSparedDays, 2).ToString() + " " + Localizer["De"].ToString().ToLower() + " " + YearSpareDays.ToString() + " " + Localizer["días"],
                BarValue = (int)Math.Min(100, Math.Round((YearSparedDays / YearSpareDays) * 100, 2))
            };
            CardData Card5 = new CardData()
            {
                Label = Localizer["Baja Médica"],
                MainValue = Math.Round((YearSickDays / YearTotalDays) * 100, 2).ToString(),
                SecondaryValue = Math.Round(YearSickDays, 2).ToString() + " " + Localizer["días"],
                BarValue = (int)Math.Min(100, Math.Round((YearSickDays / YearTotalDays) * 100, 2))
            };
            CardData Card6 = new CardData()
            {
                Label = Localizer["Vacaciones Disfrutadas"],
                MainValue = Math.Round((YearHolyDays / YearVacationDays) * 100, 2).ToString(),
                SecondaryValue = Math.Round(YearHolyDays, 2).ToString() + " " + Localizer["De"].ToString().ToLower() + " " + YearVacationDays.ToString() + " " + Localizer["días"],
                BarValue = (int)Math.Min(100, Math.Round((YearHolyDays / YearVacationDays) * 100, 2))
            };

            #endregion

            model.Cards = new List<CardData>() { Card1, Card2, Card3, Card4, Card5, Card6 };

            ViewBag.Year = year.Value;

            //timeline
            double dt = DateTime.Now.GetUnixEpoch();
            ViewBag.UserCategory = user.UserCategory.ToList();
            ViewBag.UserArea = user.UserArea.ToList();
            ViewBag.UserCollective = user.UserCollective.ToList();
            ViewBag.UserCollectiveCategory = user.UserCollectiveCategory.ToList();
            ViewBag.UserOffice = user.UserOffice.ToList();
            ViewBag.UserPosition = user.UserPosition.ToList();
            ViewBag.UserRate = user.UserRate.ToList();
            ViewBag.UserShift = user.UserShift.ToList();
            ViewBag.UserSalary = user.UserSalary.ToList();
            ViewBag.UserWorkConditions = user.UserWorkConditions.ToList();
            ViewBag.UserContract = user.UserContract.ToList();
            ViewBag.dt = dt;
                                          
            return View("ShowDashboard", model);
        }

        private IActionResult ShowSchedule(User user, int? year = null)
        {
            year = year ?? DateTime.Now.Year;

            return View("ShowSchedule");
        }

        private IActionResult ShowRequests(User user, int? year = null)
        {
            RequestListViewModel model = new RequestListViewModel
            {
                Requests = new List<RequestListItemViewModel>(),
            };
            RequestService.GetRequestsCreatedByUser(user.Id).ToList().ForEach(request =>
            {
                RequestListItemViewModel requestModel = new RequestListItemViewModel
                {
                    Id = request.Id,
                    CreationDate = request.CreationDate,
                    LastChangeDate = request.LastChangeDate,
                    Priority = request.Priority,
                    User = request.UserName,
                    AssignedUser = request.AssignedUserName,
                    Type = request.Type,
                    Summary = request.Summary,
                    State = request.State
                };
                model.Requests.Add(requestModel);
            });
            return View("ShowRequests", model);
        }

        private IActionResult ShowQuality(User user, int? year = null)
        {
            IEnumerable<Quality> qualitys = QualityService.GetMany(q => q.Active);

            IEnumerable<QualityExamUser> userExams = qualitys.SelectMany(q => q.Exams).Where(qe => qe.Active).SelectMany(qeu => qeu.Users).Where(qeu => qeu.User == user);

            IEnumerable<QualityAuditUser> userAudits = qualitys.SelectMany(q => q.Audits).Where(qa => qa.Active).SelectMany(qau => qau.Users).Where(qau => qau.User == user);

            QualityViewModel model = Mapper.Map<QualityViewModel>(userAudits);
            Mapper.Map(userExams,model);

            return View("ShowQuality", model);
        }

        private IActionResult ShowTraining(User user, int? year = null)
        {
            IEnumerable<Training> userTrainings = TrainingService.GetMany(t => t.Users.Any(tu => tu.User == user && tu.Attendant));
            IEnumerable<TrainingTestUser> userTests = userTrainings.SelectMany(t => t.Tests).SelectMany(tt => tt.Users).Where(ttu => ttu.User == user);

            TrainingViewModel model = Mapper.Map<TrainingViewModel>(userTrainings);
            Mapper.Map(userTests, model);

            return View("ShowTraining", model);
        }

        private IActionResult ShowDedications(User user, int? year = null, int? month = null)
        {
            if (year == null) year = DateTime.Now.Year;
            if (month == null) month = DateTime.Now.Month;
            ViewBag.Year = year.Value;
            ViewBag.Month = month.Value;
            List<UserDedication> dedications = ScheduleService.GetUserDedications(user.Id, year.Value, month.Value);
            ViewBag.dedications = dedications;
            return View("ShowDedications");
        }

        private IActionResult ShowOperational(User user)
        {
            UserViewModel model = Mapper.Map<UserViewModel>(user);
            return View("ShowOperational", model);
        }

        [HttpGet]
        [Route("~/[controller]/[action]/{userId}/{year}/{month?}")]
        [AuthorizeRoles(AppRole.OperationalManagementEdit, AppRole.OperationalManagementShow)]
        public IActionResult Dedications(int userId, int year, int? month = null)
        {
            User user = UserService.Get(userId);
            if (!Authorize(user)) return Error();
            List<UserDedication> dedications = ScheduleService.GetUserDedications(userId, year, month);
            JsonResponse response = new JsonResponse() { Result = true, Response = "OK", Content = dedications };
            return Json(response);
        }

        #endregion

        #region Add

        [HttpGet]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        public IActionResult Add()
        {
            AddUserViewModel model = new AddUserViewModel {
                Options = AuthUser.OwnedAreas.ToList().Where(x=>x.Type != AreaType.General).Select(u =>
                        new AssignOptions { Name = u.Name, Value = u.Id, Parent = u.ParentId }).ToList()
            };
            return PartialView("_Add", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateAjaxModelState]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        public IActionResult Add(AddUserViewModel model)
        {
            if (!(AuthUser.OwnedAreas.Any(a => a.Id == model.AreaId))) return Error(StatusCodes.Status401Unauthorized);

            var Hasher = new PasswordHasher<User>();
            User user = new User
            {
                UserName = model.UserName,
                Email = model.Email,
                FirstName = model.FirstName,
                MainSurname = model.MainSurname,
                SecondSurname = model.SecondSurname
            };
            user.PasswordHash = Hasher.HashPassword(user, model.Password);
            user.SecurityStamp = Guid.NewGuid().ToString();
            Area area = AreaService.Get(model.AreaId);
            if (area == null) return Error(StatusCodes.Status406NotAcceptable);
            UserArea userArea = new UserArea
            {
                Area = area,
                StartDate = model.AreaStartDate,
                Owner = false
            };
            user.UserArea.Add(userArea);
            UserService.Add(user);
            if (user.Id > 0)
            {
                JsonResponse response = new JsonResponse() { Result = true, Response = "El usuario se ha creado con éxito", Content = new { Id = user.Id } };
                return Json(response);
            }
            return Error();
        }

        #endregion

        #region Edit

        [HttpGet]
        [Route("~/[controller]/[action]/{id}/{tab?}")]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        public IActionResult Edit(int id, string tab = null)
        {
            User user = UserService.Get(id);
            if (!Authorize(user)) return Error();
            if (user == null) return Error(StatusCodes.Status406NotAcceptable);

            ViewBag.Tab = tab;

            UserViewModel model = Mapper.Map<UserViewModel>(user);
            model.AvailableRoles = AppRole.Roles(AuthUser.Roles.Select(x => x.Name).ToList());
            model.AvailableProfiles = AppProfile.Profiles();
            model.ProfilesRoles = AppProfile.ProfilesRoles();

            return View("Edit", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateAjaxModelState]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        public IActionResult Edit(UserViewModel model)
        {
            User user = UserService.Get(model.Id);
            if (!Authorize(user)) return Error();
            Mapper.Map(model, user);

            bool roleChange = false;
            IEnumerable<Role> roles = RoleService.GetMany();
            AuthUser.Roles.Select(x => x.Name).ToList().ForEach(roleName => { // loop auth user roles
                Role role = roles.FirstOrDefault(x => x.NormalizedName == roleName.ToUpper());
                if (role != null && model.RoleList != null)
                {
                    if (model.RoleList.Contains(roleName)) //checked in model
                    {
                        if (!user.Roles.Contains(role)) // doesn't exist in user
                        {
                            user.UserRole.Add(new UserRole() { User = user, Role = role }); //add to user
                            roleChange = true;
                        }
                    }
                    else //not checked in model
                    {
                        if (user.Roles.Contains(role)) //  exist in user
                        {
                            user.UserRole.Remove(user.UserRole.Single(x => x.Role == role)); //remove from user
                            roleChange = true;
                        }
                    }
                }
            });
            if (roleChange)
            {
                user.RefreshCookie = true;
            }
            if (UserService.Update(user) < 1)
            {
                return Error();
            }
            return Json(new JsonResponse() { Result = true, Response = "El cambio se ha realizado con éxito" });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateAjaxModelState]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        [Route("/[controller]/edit/{userid:int}/[action]/{operationalid:int}")]
        public IActionResult DeleteOperational(int userid, int operationalid)
        {
            User user = UserService.Get(userid);
            if (user == null) return Error(StatusCodes.Status406NotAcceptable);

            UserOperational operational = user.UserOperational.SingleOrDefault(uo => uo.Id == operationalid);
            if (operational == null) return Error(StatusCodes.Status406NotAcceptable);

            operational.Active = false;
            
            if (UserService.Update(user) > 0)
            {
                return Json(data: new JsonResponse() { Result = true, Response = "Eliminado con éxito", Content = new { user.Id } });
            }
            return Error();
        }

        [HttpGet]
        [Route("~/[controller]/edit/{id:int}/[action]")]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        public IActionResult ResetPassword(int id)
        {
            User user = UserService.Get(id);
            if (!Authorize(user)) return Error();
            PasswordViewModel model = new PasswordViewModel() { Id = id };
            return PartialView("_ResetPassword", model);
        }

        [HttpPost]
        [Route("~/[controller]/edit/[action]")]
        [ValidateAntiForgeryToken]
        [ValidateAjaxModelState]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        public IActionResult ResetPassword(PasswordViewModel model)
        {
            User user = UserService.Get(model.Id);
            if (!Authorize(user)) return Error();
            var Hasher = new PasswordHasher<User>();
            user.PasswordHash = Hasher.HashPassword(user, model.Password);
            user.SecurityStamp = Guid.NewGuid().ToString();
            user.RefreshCookie = true;
            user.ChangePassword = true;
            UserService.Update(user);
            JsonResponse response = new JsonResponse() { Result = true, Response = "El cambio se ha realizado con éxito" };
            return Json(response);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("~/[controller]/[action]/{requestid:int}")]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        public IActionResult ValidateRequest(int requestid)
        {
            Request request = RequestService.Get(requestid);
            if (request == null) return Error(StatusCodes.Status406NotAcceptable);
            if (request.AdditionalData == null) return Error(StatusCodes.Status406NotAcceptable);

            JObject AdditionalDataObject = JObject.Parse(request.AdditionalData);
            Area DestinationArea = AreaService.Get((int)AdditionalDataObject["DestinationArea"]);
            if (DestinationArea == null) return Error(StatusCodes.Status406NotAcceptable);
            List<int> UserIds = new List<int>();
            foreach (JToken userIdString in AdditionalDataObject["UserIds"].ToArray())
            {
                int userId = int.Parse((string)userIdString);
                UserIds.Add(userId);
            }
            if (UserIds.Count < 1) return Error(StatusCodes.Status406NotAcceptable);

            List<User> usersToReassign = UserService.GetMany(u => UserIds.Contains(u.Id)).ToList();
            DateTime AssignmentStartDate = DateTime.Now.AddDays(1);

            foreach (User user in usersToReassign)
            {
                if (user.LastUserArea != null) user.LastUserArea.EndDate = AssignmentStartDate.AddDays(-1);
                UserArea userArea = new UserArea
                {
                    User = user,
                    Area = DestinationArea,
                    Owner = false,
                    StartDate = AssignmentStartDate
                };
                user.UserArea.Add(userArea);
            }
            if (UserService.UpdateRange(usersToReassign) > 0)
            {
                request.RequestHistory.Add(new RequestHistory
                {
                    EntryDate = DateTime.Now,
                    State = RequestState.Accepted,
                    Comment = Localizer["Reasignación realizada"],
                    User = AuthUser
                });
                if (RequestService.Update(request) > 0)
                {
                    return Json(new JsonResponse() { Result = true, Response = "El cambio se ha realizado con éxito" });
                }
            }
            return Json(new JsonResponse() { Result = false, Response = "Se ha producido un error" });
        }

        #endregion

        #region Delete

        [HttpGet]
        [AuthorizeRoles(AppRole.Administration, AppRole.Maintenance)]
        public IActionResult Delete(int id)
        {
            User user = UserService.Get(id);
            if(!Authorize(user)) return Error();
            UserViewModel model = new UserViewModel();
            user.FirstName = model.FirstName;
            user.MainSurname = model.MainSurname;
            user.SecondSurname = model.SecondSurname;
            user.UserName = model.UserName;
            return View("Delete", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateAjaxModelState]
        [AuthorizeRoles(AppRole.Administration, AppRole.Maintenance)]
        public IActionResult DeleteConfirm(int id)
        {
            User user = UserService.Get(id);
            if (!Authorize(user)) return Error();
            UserService.Delete(id);
            return RedirectToAction("Index");
        }

        #endregion

        #region Verification

        //[AllowAnonymous]
        [AcceptVerbs("Get", "Post")]
        public IActionResult VerifyUserName(string userName)
        {
            if (UserService.ExistsUserName(userName))
            {
                return Json($"El usuario {userName} ya consta en el sistema");
            }
            return Json(true);
        }

        //[AllowAnonymous]
        [AcceptVerbs("Get", "Post")]
        public IActionResult VerifyEmail(string email)
        {
            if (UserService.ExistsEmail(email))
            {
                return Json($"La dirección {email} ya consta en el sistema");
            }
            return Json(true);
        }

        //[AllowAnonymous]
        [AcceptVerbs("Get", "Post")]
        public IActionResult VerifyEmployeeId(int employeeId)
        {
            if (UserService.ExistsEmployeeId(employeeId))
            {
                return Json($"El id de empleado {employeeId} ya consta en el sistema");
            }
            return Json(true);
        }
        
        #endregion

        #endregion

        #region ASSIGNMENTS

            #region Avatar

        [Route("~/[controller]/edit/{id:int}/[action]")]
        [HttpPost]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        public IActionResult Avatar(int id, List<string> File)
        {
            User user = UserService.Get(id);
            if (user != null)
            {
                if (user.LastUserArea != null)
                {
                    if (AuthUser.OwnedAreas.Any(a => a.Id == user.LastUserArea.AreaId))
                    {
                        try
                        {
                            dynamic file = JsonConvert.DeserializeObject(File[0]);
                            string image = file.output.image;
                            string base64image = image.Split(',')[1];
                            FileHelper.SaveAvatar(Enviroment, base64image, id + ".jpg");
                            user.Avatar = user.Id + ".jpg";
                        }
                        catch (Exception)
                        {
                            FileHelper.DeleteAvatar(Enviroment, id + ".jpg");
                            user.Avatar = null;
                        }
                        UserService.Update(user);
                        var response = new { status = "success", name = id+".jpg", path = "/static/avatars/"+id+".jpg" };
                        return Json(response);
                    }
                    return StatusCode(StatusCodes.Status401Unauthorized);
                }
                return StatusCode(StatusCodes.Status418ImATeapot);
            }
            return StatusCode(StatusCodes.Status400BadRequest);
        }

            #endregion

            #region Area

        [Route("~/account/profile/[action]")]
        [HttpGet]
        public IActionResult AreaHistory()
        {
            ViewBag.Editable = false;
            return AssignArea(AuthUser);
        }

        [Route("~/[controller]/show/{id}/[action]")]
        [HttpGet]
        [AuthorizeRoles(AppRole.OperationalManagementEdit, AppRole.OperationalManagementShow)]
        public IActionResult AreaHistory(int id)
        {
            ViewBag.Editable = false;
            User user = UserService.Get(id);
            if (!AuthorizeAjax(user)) return Error();
            return AssignArea(user);
        }

        [Route("~/[controller]/edit/{id}/[action]")]
        [HttpGet]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        public IActionResult EditAreaHistory(int id)
        {
            ViewBag.Editable = true;
            User user = UserService.Get(id);
            if (!AuthorizeAjax(user)) return Error();
            return AssignArea(user);
        }

        protected IActionResult AssignArea(User user)
        {
            UserArea lastUserArea = user.LastUserArea;
            var assignmentId = lastUserArea != null ? lastUserArea.AreaId : 0;
            var AssignmentStartDate = lastUserArea != null ? lastUserArea.StartDate : DateTime.Parse("1900-01-01 00:00:00");
            AssignViewModel Model = new AssignViewModel
            {
                Id = user.Id,
                Entity = "Area",
                AssignmentId = assignmentId,
                AssignmentStartDate = AssignmentStartDate,
                Options = AuthUser.OwnedAreas.ToList().Select(u =>
                    new AssignOptions { Name = u.Name, Value = u.Id, Parent = u.ParentId }).ToList(),
                History = user.UserArea.Where(a => a.Owner != true).Select(i =>
                    new AssignHistory { Name = i.Area.Name, StartDate = i.StartDate, EndDate = i.EndDate })
                        .OrderByDescending(i => i.StartDate).ToList()
            };
            return PartialView("_AssignArea", Model);
        }

        [Route("~/[controller]/edit/[action]")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateAjaxModelState]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        public IActionResult EditAreaHistory(AssignViewModel Model)
        {
            User user = UserService.Get(Model.Id);
            if (!AuthorizeAjax(user)) return Error();

            Area area = AreaService.Get(Model.AssignmentId);
            if (area == null) return Error(StatusCodes.Status406NotAcceptable);

            if (user.LastUserArea != null) user.LastUserArea.EndDate = Model.AssignmentStartDate.AddDays(-1);
            UserArea userArea = new UserArea {
                User = user,
                Area = area,
                Owner = false,
                StartDate = Model.AssignmentStartDate
            };
            user.UserArea.Add(userArea);
            UserService.Update(user);
            JsonResponse response = new JsonResponse() { Result = true, Response = "El cambio se ha realizado con éxito" };
            return Json(response);
        }

            #endregion

            #region OwnedAreas

        [Route("~/account/profile/[action]")]
        [HttpGet]
        public IActionResult OwnedAreasHistory()
        {
            ViewBag.Editable = false;
            return AssignOwnedAreas(AuthUser);
        }

        [Route("~/[controller]/show/{id}/[action]")]
        [HttpGet]
        [AuthorizeRoles(AppRole.OperationalManagementEdit, AppRole.OperationalManagementShow)]
        public IActionResult OwnedAreasHistory(int id)
        {
            ViewBag.Editable = false;
            User user = UserService.Get(id);
            if (!AuthorizeAjax(user)) return Error();
            return AssignOwnedAreas(user);
        }

        [Route("~/[controller]/edit/{id}/[action]")]
        [HttpGet]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        public IActionResult EditOwnedAreasHistory(int id)
        {
            ViewBag.Editable = true;
            User user = UserService.Get(id);
            if (!AuthorizeAjax(user)) return Error();
            return AssignOwnedAreas(user);
        }

        protected IActionResult AssignOwnedAreas(User user)
        {
            UserArea lastUserArea = user.LastUserArea;
            var assignmentId = lastUserArea != null ? lastUserArea.AreaId : 0;
            var AssignmentStartDate = lastUserArea != null ? lastUserArea.StartDate : DateTime.Parse("1900-01-01 00:00:00");
            AssignViewModel Model = new AssignViewModel {
                Id = user.Id,
                Entity = Localizer["Áreas a su cargo"],
                AssignmentId = assignmentId,
                AssignmentStartDate = AssignmentStartDate,
                Options = AuthUser.OwnedAreas.ToList().Select(u =>
                    new AssignOptions { Name = u.Name, Value = u.Id, Parent = u.ParentId }).ToList(),
                History = user.OwnedUserAreas.Select(i =>
                    new AssignHistory { Name = i.Area.Name, StartDate = i.StartDate, EndDate = i.EndDate })
                        .OrderByDescending(i => i.EndDate).ToList()
            };
            return PartialView("_AssignArea", Model);
        }

        [Route("~/[controller]/edit/[action]")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateAjaxModelState]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        public IActionResult EditOwnedAreasHistory(AssignViewModel Model)
        {
            User user = UserService.Get(Model.Id);
            if (!AuthorizeAjax(user)) return Error();

            Area area = AreaService.Get(Model.AssignmentId);
            if (area == null) return Error(StatusCodes.Status406NotAcceptable);

            UserArea userArea = new UserArea {
                User = user,
                Area = area,
                Owner = true,
                StartDate = Model.AssignmentStartDate
            };
            user.UserArea.Add(userArea);
            UserService.Update(user);
            JsonResponse response = new JsonResponse() { Result = true, Response = "El cambio se ha realizado con éxito" };
            return Json(response);
        }

            #endregion

            #region Category

        [Route("~/account/profile/[action]")]
        [HttpGet]
        public IActionResult CategoryHistory()
        {
            ViewBag.Editable = false;
            return AssignCategory(AuthUser);
        }

        [Route("~/[controller]/show/{id}/[action]")]
        [HttpGet]
        [AuthorizeRoles(AppRole.OperationalManagementEdit, AppRole.OperationalManagementShow)]
        public IActionResult CategoryHistory(int id)
        {
            ViewBag.Editable = false;
            User user = UserService.Get(id);
            if (!AuthorizeAjax(user)) return Error();
            return AssignCategory(user);
        }

        [Route("~/[controller]/edit/{id}/[action]")]
        [HttpGet]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        public IActionResult EditCategoryHistory(int id)
        {
            ViewBag.Editable = true;
            User user = UserService.Get(id);
            if (!AuthorizeAjax(user)) return Error();
            return AssignCategory(user);
        }

        protected IActionResult AssignCategory(User user)
        {

            UserCategory lastUserCategory = user.LastUserCategory ?? new UserCategory();
            var assignmentId = lastUserCategory != null ? lastUserCategory.CategoryId : 0;
            var AssignmentStartDate = lastUserCategory != null ? lastUserCategory.StartDate : DateTime.Parse("1900-01-01 00:00:00");
            AssignViewModel Model = new AssignViewModel
            {
                Id = user.Id,
                Entity = Localizer["Categoría"],
                AssignmentId = assignmentId,
                AssignmentStartDate = AssignmentStartDate,
                Options = CategoryService.GetMany().ToList().Select(u =>
                    new AssignOptions { Name = u.Name, Value = u.Id }).ToList(),
                History = user.UserCategory.Select(i =>
                    new AssignHistory { Name = i.Category.Name, StartDate = i.StartDate, EndDate = i.EndDate })
                        .OrderByDescending(i => i.StartDate).ToList()
            };
            return PartialView("_Assign", Model);


        }

        [Route("~/[controller]/edit/[action]")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateAjaxModelState]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        public IActionResult EditCategoryHistory(AssignViewModel Model)
        {
            User user = UserService.Get(Model.Id);
            if (!AuthorizeAjax(user)) return Error();
            Category category = CategoryService.Get(Model.AssignmentId);
            if (category == null) return Error(StatusCodes.Status406NotAcceptable);

            if (user.LastUserCategory != null)
            {
                user.LastUserCategory.EndDate = Model.AssignmentStartDate.AddDays(-1);
            }
            UserCategory userCategory = new UserCategory()
            {
                User = user,
                Category = category,
                StartDate = Model.AssignmentStartDate
            };
            user.UserCategory.Add(userCategory);
            UserService.Update(user);
            JsonResponse response = new JsonResponse() { Result = true, Response = "El cambio se ha realizado con éxito" };
            return Json(response);
        }

            #endregion

            #region Collective

        [Route("~/account/profile/[action]")]
        [HttpGet]
        public IActionResult CollectiveHistory()
        {
            ViewBag.Editable = false;
            return AssignCollective(AuthUser);
        }

        [Route("~/[controller]/show/{id}/[action]")]
        [HttpGet]
        [AuthorizeRoles(AppRole.OperationalManagementEdit, AppRole.OperationalManagementShow)]
        public IActionResult CollectiveHistory(int id)
        {
            ViewBag.Editable = false;
            User user = UserService.Get(id);
            if (!AuthorizeAjax(user)) return Error();
            return AssignCollective(user);
        }

        [Route("~/[controller]/edit/{id}/[action]")]
        [HttpGet]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        public IActionResult EditCollectiveHistory(int id)
        {
            ViewBag.Editable = true;
            User user = UserService.Get(id);
            if (!AuthorizeAjax(user)) return Error();
            return AssignCollective(user);
        }

        protected IActionResult AssignCollective(User user)
        {

            UserCollective lastUserCollective = user.LastUserCollective ?? new UserCollective();
            var assignmentId = lastUserCollective != null ? lastUserCollective.CollectiveId : 0;
            var AssignmentStartDate = lastUserCollective != null ? lastUserCollective.StartDate : DateTime.Parse("1900-01-01 00:00:00");
            AssignViewModel Model = new AssignViewModel
            {
                Id = user.Id,
                Entity = Localizer["Convenio"],
                AssignmentId = assignmentId,
                AssignmentStartDate = AssignmentStartDate,
                Options = CollectiveService.GetMany().ToList().Select(u =>
                    new AssignOptions { Name = u.Name, Value = u.Id }).ToList(),
                History = user.UserCollective.Select(i =>
                    new AssignHistory { Name = i.Collective.Name, StartDate = i.StartDate, EndDate = i.EndDate })
                        .OrderByDescending(i => i.StartDate).ToList()
            };
            return PartialView("_Assign", Model);
        }

        [Route("~/[controller]/edit/[action]")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateAjaxModelState]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        public IActionResult EditCollectiveHistory(AssignViewModel Model)
        {
            User user = UserService.Get(Model.Id);
            if (!AuthorizeAjax(user)) return Error();

            Collective collective = CollectiveService.Get(Model.AssignmentId);
            if (collective == null) return Error(StatusCodes.Status406NotAcceptable);

            if (user.LastUserCollective != null)
            {
                user.LastUserCollective.EndDate = Model.AssignmentStartDate.AddDays(-1);
            }
            UserCollective userCollective = new UserCollective()
            {
                User = user,
                Collective = collective,
                StartDate = Model.AssignmentStartDate
            };
            user.UserCollective.Add(userCollective);
            UserService.Update(user);
            JsonResponse response = new JsonResponse() { Result = true, Response = "El cambio se ha realizado con éxito" };
            return Json(response);
        }

            #endregion

            #region CollectiveCategory

        [Route("~/account/profile/[action]")]
        [HttpGet]
        public IActionResult CollectiveCategoryHistory()
        {
            ViewBag.Editable = false;
            return AssignCollectiveCategory(AuthUser);
        }

        [Route("~/[controller]/show/{id}/[action]")]
        [HttpGet]
        [AuthorizeRoles(AppRole.OperationalManagementEdit, AppRole.OperationalManagementShow)]
        public IActionResult CollectiveCategoryHistory(int id)
        {
            ViewBag.Editable = false;
            User user = UserService.Get(id);
            if (!AuthorizeAjax(user)) return Error();
            return AssignCollectiveCategory(user);
        }

        [Route("~/[controller]/edit/{id}/[action]")]
        [HttpGet]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        public IActionResult EditCollectiveCategoryHistory(int id)
        {
            ViewBag.Editable = true;
            User user = UserService.Get(id);
            if (!AuthorizeAjax(user)) return Error();
            return AssignCollectiveCategory(user);
        }

        protected IActionResult AssignCollectiveCategory(User user)
        {
            UserCollectiveCategory lastUserCollectiveCategory = user.LastUserCollectiveCategory ?? new UserCollectiveCategory();
            var Assignment = lastUserCollectiveCategory != null ? lastUserCollectiveCategory.Category : "";
            var AssignmentStartDate = lastUserCollectiveCategory != null ? lastUserCollectiveCategory.StartDate : DateTime.Parse("1900-01-01 00:00:00");
            AssignViewModel Model = new AssignViewModel
            {
                Id = user.Id,
                Entity = Localizer["Categoría Externa"],
                Assignment = Assignment,
                AssignmentStartDate = AssignmentStartDate,
                History = user.UserCollectiveCategory.Select(i =>
                    new AssignHistory { Name = i.Category, StartDate = i.StartDate, EndDate = i.EndDate })
                        .OrderByDescending(i => i.StartDate).ToList()
            };
            return PartialView("_Assign", Model);
        }

        [Route("~/[controller]/edit/[action]")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateAjaxModelState]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        public IActionResult EditCollectiveCategoryHistory(AssignViewModel Model)
        {
            User user = UserService.Get(Model.Id);
            if (!AuthorizeAjax(user)) return Error();

            if (user.LastUserCollectiveCategory != null)
            {
                user.LastUserCollectiveCategory.EndDate = Model.AssignmentStartDate.AddDays(-1);
            }
            UserCollectiveCategory userCollectiveCategory = new UserCollectiveCategory()
            {
                User = user,
                Category = Model.Assignment,
                StartDate = Model.AssignmentStartDate
            };
            user.UserCollectiveCategory.Add(userCollectiveCategory);
            UserService.Update(user);
            JsonResponse response = new JsonResponse() { Result = true, Response = "El cambio se ha realizado con éxito" };
            return Json(response);
        }

            #endregion

            #region Office

        [Route("~/account/profile/[action]")]
        [HttpGet]
        public IActionResult OfficeHistory()
        {
            ViewBag.Editable = false;
            return AssignOffice(AuthUser);
        }

        [Route("~/[controller]/show/{id}/[action]")]
        [HttpGet]
        [AuthorizeRoles(AppRole.OperationalManagementEdit, AppRole.OperationalManagementShow)]
        public IActionResult OfficeHistory(int id)
        {
            ViewBag.Editable = false;
            User user = UserService.Get(id);
            if (!AuthorizeAjax(user)) return Error();
            return AssignOffice(user);
        }

        [Route("~/[controller]/edit/{id}/[action]")]
        [HttpGet]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        public IActionResult EditOfficeHistory(int id)
        {
            ViewBag.Editable = true;
            User user = UserService.Get(id);
            if (!AuthorizeAjax(user)) return Error();
            return AssignOffice(user);
        }

        protected IActionResult AssignOffice(User user)
        {
            UserOffice lastUserOffice = user.LastUserOffice ?? new UserOffice();
            var assignmentId = lastUserOffice != null ? lastUserOffice.OfficeId : 0;
            var AssignmentStartDate = lastUserOffice != null ? lastUserOffice.StartDate : DateTime.Parse("1900-01-01 00:00:00");
            AssignViewModel Model = new AssignViewModel {
                Id = user.Id,
                Entity = Localizer["Oficina"],
                AssignmentId = assignmentId,
                AssignmentStartDate = AssignmentStartDate,
                Options = OfficeService.GetMany().ToList().Select(u =>
                    new AssignOptions { Name = u.Name, Value = u.Id }).ToList(),
                History = user.UserOffice.Select(i =>
                    new AssignHistory { Name = i.Office.Name, StartDate = i.StartDate, EndDate = i.EndDate })
                        .OrderByDescending(i => i.StartDate).ToList()
            };
            return PartialView("_Assign", Model);
        }

        [Route("~/[controller]/edit/[action]")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateAjaxModelState]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        public IActionResult EditOfficeHistory(AssignViewModel Model)
        {
            User user = UserService.Get(Model.Id);
            if (!AuthorizeAjax(user)) return Error();

            Office office = OfficeService.Get(Model.AssignmentId);
            if (office == null) return Error(StatusCodes.Status406NotAcceptable);

            if (user.LastUserOffice != null)
            {
                user.LastUserOffice.EndDate = Model.AssignmentStartDate.AddDays(-1);
            }
            UserOffice userOffice = new UserOffice {
                User = user,
                Office = office,
                StartDate = Model.AssignmentStartDate
            };
            user.UserOffice.Add(userOffice);
            UserService.Update(user);
            JsonResponse response = new JsonResponse() { Result = true, Response = "El cambio se ha realizado con éxito" };
            return Json(response);
            
        }

            #endregion

            #region Position

        [Route("~/account/profile/[action]")]
        [HttpGet]
        public IActionResult PositionHistory()
        {
            ViewBag.Editable = false;
            return AssignPosition(AuthUser);
        }

        [Route("~/[controller]/show/{id}/[action]")]
        [HttpGet]
        [AuthorizeRoles(AppRole.OperationalManagementEdit, AppRole.OperationalManagementShow)]
        public IActionResult PositionHistory(int id)
        {
            ViewBag.Editable = false;
            User user = UserService.Get(id);
            if (!AuthorizeAjax(user)) return Error();
            return AssignPosition(user);
        }

        [Route("~/[controller]/edit/{id}/[action]")]
        [HttpGet]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        public IActionResult EditPositionHistory(int id)
        {
            ViewBag.Editable = true;
            User user = UserService.Get(id);
            if (!AuthorizeAjax(user)) return Error();
            return AssignPosition(user);
        }

        protected IActionResult AssignPosition(User user)
        {
            UserPosition lastUserPosition = user.LastUserPosition ?? new UserPosition();
            string position = lastUserPosition != null ? lastUserPosition.Name : "";
            var AssignmentStartDate = lastUserPosition != null ? lastUserPosition.StartDate : DateTime.Parse("1900-01-01 00:00:00");
            AssignViewModel Model = new AssignViewModel
            {
                Id = user.Id,
                Entity = Localizer["Cargo"],
                Assignment = position,
                AssignmentStartDate = AssignmentStartDate,
                History = user.UserPosition.Select(i =>
                    new AssignHistory { Name = i.Name, StartDate = i.StartDate, EndDate = i.EndDate })
                        .OrderByDescending(i => i.StartDate).ToList()
            };
            return PartialView("_Assign", Model);
        }

        [Route("~/[controller]/edit/[action]")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateAjaxModelState]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        public IActionResult EditPositionHistory(AssignViewModel Model)
        {
            User user = UserService.Get(Model.Id);
            if (!AuthorizeAjax(user)) return Error();
            if (user.LastUserPosition != null)
            {
                user.LastUserPosition.EndDate = Model.AssignmentStartDate.AddDays(-1);
            }
            UserPosition userPosition = new UserPosition()
            {
                User = user,
                Name = Model.Assignment,
                StartDate = Model.AssignmentStartDate
            };
            user.UserPosition.Add(userPosition);
            UserService.Update(user);
            JsonResponse response = new JsonResponse() { Result = true, Response = "El cambio se ha realizado con éxito" };
            return Json(response);
        }

            #endregion

            #region Rate

        [Route("~/account/profile/[action]")]
        [HttpGet]
        public IActionResult RateHistory()
        {
            ViewBag.Editable = false;
            return AssignRate(AuthUser);
        }

        [Route("~/[controller]/show/{id}/[action]")]
        [HttpGet]
        [AuthorizeRoles(AppRole.EconomicalManagementEdit, AppRole.EconomicalManagementShow)]
        public IActionResult RateHistory(int id)
        {
            ViewBag.Editable = false;
            User user = UserService.Get(id);
            if (!AuthorizeAjax(user)) return Error();
            return AssignRate(user);
        }

        [Route("~/[controller]/edit/{id}/[action]")]
        [HttpGet]
        [AuthorizeRoles(AppRole.EconomicalManagementEdit)]
        public IActionResult EditRateHistory(int id)
        {
            ViewBag.Editable = true;
            User user = UserService.Get(id);
            if (!AuthorizeAjax(user)) return Error();
            return AssignRate(user);
        }

        protected IActionResult AssignRate(User user)
        {
            UserRate lastUserRate = user.LastUserRate ?? new UserRate();
            var assignmentId = lastUserRate != null ? lastUserRate.RateId : 0;
            var AssignmentStartDate = lastUserRate != null ? lastUserRate.StartDate : DateTime.Parse("1900-01-01 00:00:00");
            AssignViewModel Model = new AssignViewModel {
                Id = user.Id,
                Entity = Localizer["Tasa"],
                AssignmentId = assignmentId,
                AssignmentStartDate = AssignmentStartDate,
                Options = RateService.GetMany().ToList().Select(u =>
                    new AssignOptions { Name = u.Name + " - " + u.Value + " " + u.Currency, Value = u.Id }).ToList(),
                History = user.UserRate.Select(i =>
                    new AssignHistory { Name = i.Rate.Name, StartDate = i.StartDate, EndDate = i.EndDate })
                        .OrderByDescending(i => i.StartDate).ToList()
            };
            return PartialView("_Assign", Model);
        }

        [Route("~/[controller]/edit/[action]")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateAjaxModelState]
        [AuthorizeRoles(AppRole.EconomicalManagementEdit)]
        public IActionResult EditRateHistory(AssignViewModel Model)
        {
            User user = UserService.Get(Model.Id);
            if (!AuthorizeAjax(user)) return Error();

            Rate rate = RateService.Get(Model.AssignmentId);
            if (rate == null) return Error(StatusCodes.Status406NotAcceptable);

            if (user.LastUserRate != null)
            {
                user.LastUserRate.EndDate = Model.AssignmentStartDate.AddDays(-1);
            }
            UserRate userRate = new UserRate()
            {
                User = user,
                Rate = rate,
                StartDate = Model.AssignmentStartDate
            };
            user.UserRate.Add(userRate);
            UserService.Update(user);
            JsonResponse response = new JsonResponse() { Result = true, Response = "El cambio se ha realizado con éxito" };
            return Json(response);
        }

            #endregion

            #region Shift

        [Route("~/account/profile/[action]")]
        [HttpGet]
        public IActionResult ShiftHistory()
        {
            ViewBag.Editable = false;
            return AssignShift(AuthUser);
        }

        [Route("~/[controller]/show/{id}/[action]")]
        [HttpGet]
        [AuthorizeRoles(AppRole.OperationalManagementEdit, AppRole.OperationalManagementShow)]
        public IActionResult ShiftHistory(int id)
        {
            ViewBag.Editable = false;
            User user = UserService.Get(id);
            if (!AuthorizeAjax(user)) return Error();
            return AssignShift(user);
        }

        [Route("~/[controller]/edit/{id}/[action]")]
        [HttpGet]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        public IActionResult EditShiftHistory(int id)
        {
            ViewBag.Editable = true;
            User user = UserService.Get(id);
            if (!AuthorizeAjax(user)) return Error();
            return AssignShift(user);
        }

        protected IActionResult AssignShift(User user)
        {
            UserShift lastUserShift = user.LastUserShift ?? new UserShift();
            var AssignmentStartDate = lastUserShift != null ? lastUserShift.StartDate : DateTime.Parse("1900-01-01 00:00:00");

            List<Shift> Shifts = user.LastUserShift == null ? new List<Shift>() : user.LastUserShift.Shift.OrderBy(x => x.WeekDay).ToList();
            ViewBag.Shifts = Shifts;

            AssignShift Model = new AssignShift
            {
                UserId = user.Id,
                Entity = Localizer["Turno"],
                AssignmentStartDate = AssignmentStartDate,
                History = user.UserShift.Select(i =>
                    new AssignHistory
                    {
                        Name = i.Description,
                        StartDate = i.StartDate,
                        EndDate = i.EndDate
                    }).OrderByDescending(i => i.StartDate).ToList(),
            };
            lastUserShift.Shift.ToList().ForEach(i =>
            {
                Model.Patterns.Add(i.WeekDay, i.Pattern);
            });
            return PartialView("_AssignShift", Model);
        }

        [Route("~/[controller]/edit/[action]")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateAjaxModelState]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        public IActionResult EditShiftHistory(AssignShift Model)
        {
            User user = UserService.Get(Model.UserId);
            if (!AuthorizeAjax(user)) return Error();

            if (user.LastUserShift != null)
            {
                user.LastUserShift.EndDate = Model.AssignmentStartDate.AddDays(-1);
            }
            UserShift userShift = new UserShift()
            {
                User = user,
                Description = Model.Description,
                StartDate = Model.AssignmentStartDate
            };
            foreach (var item in Model.Patterns)
            {
                Shift shift = new Shift()
                {
                    Week = 1,
                    WeekDay = item.Key,
                    Pattern = item.Value
                };
                userShift.Shift.Add(shift);
            }
            user.UserShift.Add(userShift);
            JsonResponse response = new JsonResponse() { Result = false, Response = "Se ha producido un error" };
            if (UserService.Update(user) > 0)
            {
                response = new JsonResponse() { Result = true, Response = "El cambio se ha realizado con éxito" };
            }
            return Json(response);
        }

            #endregion

            #region Salary

        [Route("~/account/profile/[action]")]
        [HttpGet]
        public IActionResult SalaryHistory()
        {
            ViewBag.Editable = false;
            return AssignSalary(AuthUser);
        }

        [Route("~/[controller]/show/{id}/[action]")]
        [HttpGet]
        [AuthorizeRoles(AppRole.EconomicalManagementEdit, AppRole.EconomicalManagementShow)]
        public IActionResult SalaryHistory(int id)
        {
            ViewBag.Editable = false;
            User user = UserService.Get(id);
            if (!AuthorizeAjax(user)) return Error();
            return AssignSalary(user);
        }

        [Route("~/[controller]/edit/{id}/[action]")]
        [HttpGet]
        [AuthorizeRoles(AppRole.EconomicalManagementEdit)]
        public IActionResult EditSalaryHistory(int id)
        {
            ViewBag.Editable = true;
            User user = UserService.Get(id);
            if (!AuthorizeAjax(user)) return Error();
            return AssignSalary(user);
        }

        protected IActionResult AssignSalary(User user)
        {
            UserSalary lastUserSalary = user.LastUserSalary ?? new UserSalary();
            string currency = lastUserSalary != null ? lastUserSalary.Currency : "Euro";
            var AssignmentStartDate = lastUserSalary != null ? lastUserSalary.StartDate : DateTime.Parse("1900-01-01 00:00:00");
            AssignSalary Model = new AssignSalary
            {
                UserId = user.Id,
                Entity = Localizer["Salario"],
                Salary = lastUserSalary.Salary != 0 ? lastUserSalary.Salary : new Nullable<float>(),
                Variable = lastUserSalary.Variable,
                Currency = currency,
                AssignmentStartDate = AssignmentStartDate,
                History = user.UserSalary.Select(i =>
                    new AssignHistory { Name = i.Salary.ToString() + " " + i.Currency, StartDate = i.StartDate, EndDate = i.EndDate })
                        .OrderByDescending(i => i.StartDate).ToList()
            };
            return PartialView("_AssignSalary", Model);
        }

        [Route("~/[controller]/edit/[action]")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateAjaxModelState]
        [AuthorizeRoles(AppRole.EconomicalManagementEdit)]
        public IActionResult EditSalaryHistory(AssignSalary Model)
        {
            User user = UserService.Get(Model.UserId);
            if (!AuthorizeAjax(user)) return Error();

            if (user.LastUserSalary != null)
            {
                user.LastUserSalary.EndDate = Model.AssignmentStartDate.AddDays(-1);
            }
            UserSalary userSalary = new UserSalary()
            {
                User = user,
                Salary = Model.Salary ?? 0,
                Variable = Model.Variable,
                Currency = Model.Currency,
                StartDate = Model.AssignmentStartDate
            };
            user.UserSalary.Add(userSalary);
            UserService.Update(user);
            JsonResponse response = new JsonResponse() { Result = true, Response = "El cambio se ha realizado con éxito" };
            return Json(response);
        }

            #endregion

            #region WorkConditions

        [Route("~/account/profile/[action]")]
        [HttpGet]
        public IActionResult WorkConditionsHistory()
        {
            ViewBag.Editable = false;
            return AssignWorkConditions(AuthUser);
        }

        [Route("~/[controller]/show/{id}/[action]")]
        [HttpGet]
        [AuthorizeRoles(AppRole.OperationalManagementEdit, AppRole.OperationalManagementShow)]
        public IActionResult WorkConditionsHistory(int id)
        {
            ViewBag.Editable = false;
            User user = UserService.Get(id);
            if (!AuthorizeAjax(user)) return Error();
            return AssignWorkConditions(user);
        }

        [Route("~/[controller]/edit/{id}/[action]")]
        [HttpGet]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        public IActionResult EditWorkConditionsHistory(int id)
        {
            ViewBag.Editable = true;
            User user = UserService.Get(id);
            if (!AuthorizeAjax(user)) return Error();
            return AssignWorkConditions(user);
        }

        protected IActionResult AssignWorkConditions(User user)
        {
            UserWorkConditions lastUserWorkConditions = user.LastUserWorkConditions ?? new UserWorkConditions();
            var AssignmentStartDate = lastUserWorkConditions != null ? lastUserWorkConditions.StartDate : DateTime.Parse("1900-01-01 00:00:00");
            AssignWorkConditionsViewModel Model = new AssignWorkConditionsViewModel
            {
                UserId = user.Id,
                Entity = Localizer["Condiciones Laborales"],
                WeekWorkHours = lastUserWorkConditions.WeekWorkHours != 0 ? lastUserWorkConditions.WeekWorkHours : new Nullable<double>(),
                YearWorkHours = lastUserWorkConditions.YearWorkHours != 0 ? lastUserWorkConditions.YearWorkHours : new Nullable<double>(),
                YearVacationDays = lastUserWorkConditions.YearVacationDays != 0 ? lastUserWorkConditions.YearVacationDays : new Nullable<double>(),
                YearSpareDays = lastUserWorkConditions.YearSpareDays != 0 ? lastUserWorkConditions.YearSpareDays : new Nullable<double>(),
                AssignmentStartDate = AssignmentStartDate,
                History = user.UserWorkConditions.Select(i =>
                    new AssignHistory {
                        Name = i.WeekWorkHours.ToString() + "h / " + i.YearWorkHours.ToString()
                            + "h / " + i.YearVacationDays.ToString() + "d / " + i.YearSpareDays.ToString() + "d",
                        StartDate = i.StartDate,
                        EndDate = i.EndDate
                    }).OrderByDescending(i => i.StartDate).ToList()
            };
            return PartialView("_AssignWorkConditions", Model);
        }

        [Route("~/[controller]/edit/[action]")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateAjaxModelState]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        public IActionResult EditWorkConditionsHistory(AssignWorkConditionsViewModel Model)
        {
            User user = UserService.Get(Model.UserId);
            if (!AuthorizeAjax(user)) return Error();

            if (user.LastUserWorkConditions != null)
            {
                user.LastUserWorkConditions.EndDate = Model.AssignmentStartDate.AddDays(-1);
            }
            UserWorkConditions userWorkConditions = new UserWorkConditions()
            {
                User = user,
                WeekWorkHours = Model.WeekWorkHours ?? 0,
                YearWorkHours = Model.YearWorkHours ?? 0,
                YearVacationDays = Model.YearVacationDays ?? 0,
                YearSpareDays = Model.YearSpareDays ?? 0,
                StartDate = Model.AssignmentStartDate
            };
            user.UserWorkConditions.Add(userWorkConditions);
            UserService.Update(user);
            JsonResponse response = new JsonResponse() { Result = true, Response = "El cambio se ha realizado con éxito" };
            return Json(response);
        }

            #endregion

            #region Contract

        [Route("~/account/profile/[action]")]
        [HttpGet]
        public IActionResult ContractHistory()
        {
            ViewBag.Editable = false;
            return AssignContract(AuthUser);
        }

        [Route("~/[controller]/show/{id}/[action]")]
        [HttpGet]
        [AuthorizeRoles(AppRole.OperationalManagementEdit, AppRole.OperationalManagementShow)]
        public IActionResult ContractHistory(int id)
        {
            ViewBag.Editable = false;
            User user = UserService.Get(id);
            if (!AuthorizeAjax(user)) return Error();
            return AssignContract(user);
        }

        [Route("~/[controller]/edit/{id}/[action]")]
        [HttpGet]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        public IActionResult EditContractHistory(int id)
        {
            ViewBag.Editable = true;
            User user = UserService.Get(id);
            if (!AuthorizeAjax(user)) return Error();
            return AssignContract(user);
        }

        protected IActionResult AssignContract(User user)
        {

            UserContract lastUserContract = user.LastUserContract ?? new UserContract();
            var AssignmentStartDate = lastUserContract != null ? lastUserContract.StartDate : DateTime.Parse("1900-01-01 00:00:00");
            AssignContractViewModel Model = new AssignContractViewModel
            {
                UserId = user.Id,
                Entity = Localizer["Contrato"],

                External = lastUserContract.External,
                CompanyId = lastUserContract.CompanyId,
                WorkLocalHolydays = lastUserContract.WorkLocalHolydays,
                WorkNationalHolidays = lastUserContract.WorkLocalHolydays,
                WorkWeekends = lastUserContract.WorkWeekends,
                ContractPeriod = lastUserContract.ContractPeriod,
                AssignmentStartDate = lastUserContract.StartDate,
                CompanyList = CompanyService.GetMany().ToList(),
                History = user.UserContract.Select(i =>
                    new AssignHistory
                    {
                        Name = i.ContractPeriod.GetDisplayName() + " - " + i.Company.Name,
                        StartDate = i.StartDate,
                        EndDate = i.EndDate
                    }).OrderByDescending(i => i.StartDate).ToList(),

                
            };
            return PartialView("_AssignContract", Model);
        }

        [Route("~/[controller]/edit/[action]")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateAjaxModelState]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        public IActionResult EditContractHistory(AssignContractViewModel Model)
        {
            User user = UserService.Get(Model.UserId);
            if (!AuthorizeAjax(user)) return Error();

            if (user.LastUserContract != null)
            {
                user.LastUserContract.EndDate = Model.AssignmentStartDate.AddDays(-1);
            }
            Company Company = CompanyService.Get(Model.CompanyId);
            UserContract userContract = new UserContract()
            {
                User = user,
                External = Model.ContractPeriod == ContractPeriodType.External ? true : false,
                Company = Company,
                WorkLocalHolydays = Model.WorkLocalHolydays,
                WorkNationalHolidays = Model.WorkLocalHolydays,
                WorkWeekends = Model.WorkWeekends,
                ContractPeriod = Model.ContractPeriod,

                StartDate = Model.AssignmentStartDate
            };
            user.UserContract.Add(userContract);
            UserService.Update(user);
            JsonResponse response = new JsonResponse() { Result = true, Response = "El cambio se ha realizado con éxito" };
            return Json(response);
        }

            #endregion

            #region Operational

        [HttpGet]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        [Route("~/[controller]/edit/{userid}/[action]/{operationalid?}")]
        public IActionResult AssignOperational (int userid, int? operationalid)
        {
            User user = UserService.Get(userid);
            if (user == null) return Error(StatusCodes.Status406NotAcceptable);

            UserOperational operational = new UserOperational() { UserId = userid };
            if (operationalid != null)
            {
                operational = user.UserOperational.SingleOrDefault(uo => uo.Id == operationalid);
                if (operational == null) return Error(StatusCodes.Status406NotAcceptable);
            }

            UserOperationalModel model = Mapper.Map<UserOperationalModel>(operational);

            return PartialView("_AssignOperational", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateAjaxModelState]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        [Route("~/[controller]/edit/{userid}/[action]/{operationalid?}")]
        public IActionResult AssignOperational(UserOperationalModel model)
        {
            User user = UserService.Get(model.UserId);

            if (model.Id > 0)
            {
                UserOperational operational = user.UserOperational.SingleOrDefault(uo => uo.Id == model.Id);
                operational.Key = model.Key;
                operational.Value = model.Value;
            }
            else
            {
                UserOperational operational = new UserOperational() {
                    Key = model.Key,
                    Value = model.Value
                };
                user.UserOperational.Add(operational);
            }

            if (UserService.Update(user) > 0)
            {
                return RedirectToAction("Edit", new { id = model.UserId, tab = "operational" });
            }
            return Error();
        }

            #endregion

        #endregion
    }
}