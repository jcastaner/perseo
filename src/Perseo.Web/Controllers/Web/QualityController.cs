using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Newtonsoft.Json.Linq;
using Perseo.Core;
using Perseo.Web.Helpers;
using Perseo.Web.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Perseo.Web.Controllers
{
    [Route("[controller]/[action]/{id:int?}")]
    public class QualityController : BaseController
    {
        #region CONSTRUCTOR
        private readonly IService<Quality> QualityService;
        private readonly IEmailService EmailService;
        private readonly IAreaService AreaService;
        private readonly IRequestService RequestService;
        private readonly INotificationService NotificationService;
        private readonly IService<NonConformityCard> NonConformityCardService;
        public QualityController(
            IEmailService emailService,
            IHttpContextAccessor contextAccessor,
            IHostingEnvironment enviroment,
            IUserService userService,
            IStringLocalizer<SharedResources> localizer,
            IAreaService areaService,
            IRequestService requestService,
            INotificationService notificationService,
            IService<Quality> qualityService,
            IService<NonConformityCard> nonConformityCardService,
            IMapper mapper) : base(contextAccessor, enviroment, userService, localizer, mapper)
        {
            QualityService = qualityService;
            EmailService = emailService;
            AreaService = areaService;
            RequestService = requestService;
            NotificationService = notificationService;
            NonConformityCardService = nonConformityCardService;
        }
        #endregion

        #region LIST

        [HttpGet]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        [Route("/[controller]")]
        public IActionResult Index()
        {
            return List();
        }

        [HttpGet]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        [Route("/[controller]/[action]/{year:int?}/{month:int?}/{selectedareas?}")]
        public IActionResult List(int? year = null, int? month = null, string selectedareas = null)
        {
            year = year ?? DateTime.Now.Year;
            month = month ?? DateTime.Now.Month;

            List<Area> authUserOwnedAreas = AuthUser.OwnedAreas.ToList();
            List<int> areaFilterIds = AreaFilter(authUserOwnedAreas, selectedareas);

            #region CHARTS

            List<string> labels = new List<string>();
            DateTime BaseDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            for (int i = 11; i >= 0; i--) { labels.Add(CultureInfo.CurrentCulture.TextInfo.ToTitleCase((BaseDate.AddMonths(-i)).ToString("MMMM"))); }

            ChartViewModel Area1 = new ChartViewModel()
            {
                Name = Localizer["Valoraci�n de Auditor�as"],
                Series = new List<ChartViewModel.Serie>() { new ChartViewModel.Serie() { Name = Localizer["Auditor�as"], Data = Enumerable.Repeat(0.0, labels.Count()).ToList() } },
                Labels = labels
            };
            ChartViewModel Area2 = new ChartViewModel()
            {
                Name = Localizer["Nivel de Conocimiento"],
                Series = new List<ChartViewModel.Serie>() { new ChartViewModel.Serie() { Name = Localizer["Conocimiento"], Data = Enumerable.Repeat(0.0, labels.Count()).ToList() } },
                Labels = labels
            };
            ChartViewModel Area3 = new ChartViewModel()
            {
                Name = Localizer["Evoluci�n de Indicadores"],
                Series = new List<ChartViewModel.Serie>() { new ChartViewModel.Serie() { Name = Localizer["Indicadores"], Data = new List<double>() { 74.23, 77.6, 76.4, 84.56, 82.27, 83.67, 84.24, 79.84, 80.23, 76.97, 75.34, 80.58 } } },
                Labels = labels
            };
            ChartViewModel Area4 = new ChartViewModel()
            {
                Name = Localizer["No Conformidades"],
                Series = new List<ChartViewModel.Serie>() { new ChartViewModel.Serie() { Name = Localizer["No Conformidad"], Data = Enumerable.Repeat(0.0, labels.Count()).ToList() } },
                Labels = labels
            };

            List<Quality> qualitys = QualityService.GetMany(q => ((q.StartDate >= BaseDate.AddYears(-1)) && (q.StartDate < BaseDate.AddMonths(1).AddSeconds(-1)) && (areaFilterIds.Any(i => i == q.Area.Id)))).ToList();
            List<double> UsersExams = Enumerable.Repeat(0.0, labels.Count()).ToList();
            List<double> UsersPassExams = Enumerable.Repeat(0.0, labels.Count()).ToList();
            List<double> UsersAudits = Enumerable.Repeat(0.0, labels.Count()).ToList();
            List<double> UsersPassAudits = Enumerable.Repeat(0.0, labels.Count()).ToList();
            foreach (Quality quality in qualitys)
            {
                int index = (((12 - BaseDate.Month) + (quality.StartDate.Month) - 1) % 12);

                switch (quality.Type)
                {
                    case CampaignType.KnowledgeEvaluation:
                        UsersExams[index] += quality.Users.Count();
                        UsersPassExams[index] += quality.Users.Where(qu => qu.QualityCompleted).Count();
                        break;
                    case CampaignType.PerformanceEvaluation:
                        UsersAudits[index] += quality.Users.Count();
                        UsersPassAudits[index] += quality.Users.Where(qu => qu.QualityCompleted).Count();
                        break;
                    default: return Error();
                }
            }

            for (int index = 0; index < labels.Count(); index++)
            {
                Area1.Series[0].Data[index] = UsersAudits[index] > 0 ? Math.Round((UsersPassAudits[index] / UsersAudits[index]) * 100, 2): 0;
                Area2.Series[0].Data[index] = UsersExams[index] > 0 ? Math.Round((UsersPassExams[index] / UsersExams[index]) * 100, 2) : 0;
            }

            List<NonConformityCard> nonconformitys = NonConformityCardService.GetMany(c => ((c.StartDate >= BaseDate.AddYears(-1)) && (c.StartDate < BaseDate.AddMonths(1).AddSeconds(-1)) && (areaFilterIds.Any(i => i == c.Job.Area.Id)))).ToList();

            foreach (NonConformityCard card in nonconformitys)
            {
                int index = (((12 - BaseDate.Month) + (card.StartDate.Month) - 1) % 12);
                Area4.Series[0].Data[index] += 1;
            }

            #endregion

            PagedResult<Quality> pagedResult = QualityService.GetManyPaged(t => ((t.StartDate.Year == (int)year && t.StartDate.Month == (int)month) || (t.Active)) && areaFilterIds.Any(i => i == t.Area.Id), "Active DESC, StartDate");

            ListQualityViewModel model = new ListQualityViewModel()
            {
                AreaChart1 = Area1,
                AreaChart2 = Area2,
                AreaChart3 = Area3,
                AreaChart4 = Area4,
                Data = Mapper.Map<List<QualityListViewModel>>(pagedResult.Results.ToList()),
                Pagination = Mapper.Map<PaginationModel>(pagedResult),
                OwnedAreas = Mapper.Map<List<AreaModel>>(authUserOwnedAreas),
                SelectedAreasIds = areaFilterIds,
                Month = month.Value,
                Year = year.Value
            };

            return View("List", model);
        }

        #endregion

        #region SHOW

        [HttpGet]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        [Route("~/[controller]/[action]/{qualityid:int}")]
        public IActionResult Show(int? qualityid)
        {
            Quality quality = QualityService.Get(qualityid);
            if (quality == null) return Error(StatusCodes.Status406NotAcceptable);

            ShowQualityViewModel model = Mapper.Map<ShowQualityViewModel>(quality);

            return View("Show", model);
        }

        [HttpGet]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        [Route("~/[controller]/[action]/{qualityid:int}")]
        public IActionResult State(int qualityid)
        {
            Quality quality = QualityService.Get(qualityid);
            if (quality == null) return Error(StatusCodes.Status406NotAcceptable);

            StateQualityViewModel model = Mapper.Map<StateQualityViewModel>(quality);

            List<QualityAuditUser> authUserPendings = quality.Audits.SelectMany(qa => qa.Users).Where(qau => qau.AuditOwner.User == AuthUser && !(qau.Takes.Count > 0)).ToList();

            foreach (QualityAuditUser qau in authUserPendings)
                { model.AuthUserAuditPending.Add(Mapper.Map<StateAuditPendingViewModel>(qau));}

            return View("State", model);
        }

        #endregion

        #region EDIT
            #region BASE

        [HttpGet]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        [Route("~/[controller]/[action]/{qualityid:int}")]
        public IActionResult Edit(int qualityid)
        {
            Quality quality = QualityService.Get(qualityid);
            if (quality == null) return Error(StatusCodes.Status406NotAcceptable);

            EditQualityViewModel model = Mapper.Map<EditQualityViewModel>(quality);
            model.OwnedAreas = Mapper.Map<List<AreaModel>>(AuthUser.OwnedAreas.ToList());

            return View("Edit", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateAjaxModelState]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        public IActionResult Edit(EditQualityViewModel model)
        {
            Quality quality = QualityService.Get(model.Quality.Id);
            if (quality == null) return Error(StatusCodes.Status406NotAcceptable);

            Mapper.Map(model, quality);

            if (QualityService.Update(quality) > 0)
            {
                return Json(new JsonResponse() { Result = true, Response = "El cambio se ha realizado con �xito" });
            }
            return Error();
        }

            #endregion
            #region USER
                #region ADD

        [HttpGet]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        [Route("~/[controller]/[action]/{qualityid:int}/{usertype}/{role:int?}")]
        public IActionResult AddUser(int qualityid, string usertype, int? role = 17)
        {
            Quality quality = QualityService.Get(qualityid);
            if (quality == null) return Error(StatusCodes.Status406NotAcceptable);
            List<int> AreasIds = AreaService.GetChildren(quality.AreaId).Select(a => a.Id).ToList();
            List<User> AddibleUsers = new List<User>();

            switch (usertype)
            {
                case "owner":
                    AddibleUsers = UserService.GetMany(u => u.UserRole.Any(ur => ur.RoleId == role) && !u.QualityUser.Any(tu => tu.QualityId == quality.Id) && !u.QualityOwner.Any(tu => tu.QualityId == quality.Id), null, false).ToList();
                    break;
                case "user":
                    AddibleUsers = UserService.GetMany(u => u.UserArea.Any(ua => AreasIds.Contains(ua.AreaId)) && !u.QualityUser.Any(tu => tu.QualityId == quality.Id) && !u.QualityOwner.Any(tu => tu.QualityId == quality.Id), null, false).ToList();
                    break;
                default: return Error(StatusCodes.Status406NotAcceptable);
            }

            AddQualityUserViewModel model = new AddQualityUserViewModel
            {
                QualityId = qualityid,
                UserType = usertype,
                Role = role,
                AddibleUsers = Mapper.Map<List<UserModel>>(AddibleUsers)
            };

            return PartialView("_AddUser", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateAjaxModelState]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        [Route("~/[controller]/[action]/{qualityid:int}/{usertype}/{role:int?}")]
        public IActionResult AddUser(AddQualityUserViewModel model)
        {
            List<int> UsersToAddIdInt = model.UsersToAddIds.Split(";").Select(int.Parse).ToList();
            Quality quality = QualityService.Get(model.QualityId);

            switch (model.UserType)
            {
                case "owner":
                    UsersToAddIdInt.ToList().ForEach(userid => { quality.Owners.Add(new QualityOwner { OwnerId = userid }); });
                    break;
                case "user":
                    UsersToAddIdInt.ToList().ForEach(userid => { quality.Users.Add(new QualityUser { UserId = userid }); });
                    break;
                default: return Error(StatusCodes.Status406NotAcceptable);
            }

            if (QualityService.Update(quality) > 0)
            {
                return Json(data: new JsonResponse() { Result = true, Response = "A�adido/s con �xito", Content = new { Id = model.QualityId } });
            }
            return Error();
        }

                #endregion
                #region DELETE

        [HttpGet]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        [Route("~/[controller]/[action]/{QualityId:int}/{UserId:int}")]
        public JsonResult DeleteUserCheck(int qualityid, int userid)
        {
            string response = "";
            bool ActionAllowed = true;

            Quality quality = QualityService.Get(qualityid);

            QualityOwner qualityOwner = quality.Owners.SingleOrDefault(qo => qo.OwnerId == userid);
            if (qualityOwner != null)
            {
                if (quality.Exams.Any(qe => qe.Owners.Any(qeo => qeo.UserId == userid))
                    || quality.Audits.Any(qa => qa.Owners.Any(qao => qao.UserId == userid)))
                {
                    ActionAllowed = false; response = Localizer["No es posible eliminar un usuario con pruebas asociadas."];
                }
            }

            QualityUser qualityUser = quality.Users.SingleOrDefault(qu => qu.UserId == userid);
            if (qualityUser != null)
            {

                if (quality.Users.Where(qu => qu.UserId == userid).Any(qu => qu.QualityCompleted))
                {
                    ActionAllowed = false; response = Localizer["No es posible eliminar un usuario que ha finalizado la campa�a de calidad."];
                }
                else if (quality.Exams.Any(qe => qe.Users.Any(qeu => qeu.UserId == userid))
                         || quality.Audits.Any(qa => qa.Users.Any(qau => qau.UserId == userid)))
                {
                    ActionAllowed = false; response = Localizer["No es posible eliminar un usuario con pruebas asociadas."];
                }
            }

            return Json(data: new JsonResponse() { Result = ActionAllowed, Response = response });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateAjaxModelState]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        [Route("~/[controller]/[action]/{qualityid:int}/{userid:int}")]
        public IActionResult DeleteUser(int qualityid, int userid)
        {
            Quality quality = QualityService.Get(qualityid);
            if (quality == null) return Error(StatusCodes.Status406NotAcceptable);
            QualityOwner qualityOwner = quality.Owners.SingleOrDefault(qo => qo.OwnerId == userid);
            QualityUser qualityUser = quality.Users.SingleOrDefault(qu => qu.UserId == userid);
            if ((qualityOwner == null) && (qualityUser == null)) return Error(StatusCodes.Status406NotAcceptable);

            if (qualityOwner != null) { quality.Owners.Remove(qualityOwner); }
            if (qualityUser != null) { quality.Users.Remove(qualityUser); }

            if (QualityService.Update(quality) > 0)
            {
                return Json(data: new JsonResponse() { Result = true, Response = "Eliminado con �xito", Content = new { Id = qualityid } });
            }

            return Error();
        }

                #endregion
            #endregion
        #endregion

        #region ADD

        [HttpGet]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        [Route("~/[controller]/[action]")]
        public IActionResult Add()
        {
            AddQualityViewModel model = new AddQualityViewModel
            {
                OwnedAreas = Mapper.Map<List<AreaModel>>(AuthUser.OwnedAreas.ToList())
            };
            return PartialView("_Add", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateAjaxModelState]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        public IActionResult Add(AddQualityViewModel model)
        {
            Quality quality = new Quality
            {
                Name = model.Name,
                Type = model.Type,
                Description = model.Description,
                AreaId = model.AreaId,
                CreatorId = AuthUser.Id,
                StartDate = DateTime.Now
            };

            quality.Owners.Add(new QualityOwner { OwnerId = AuthUserId, Owner = AuthUser });

            if (QualityService.Add(quality) > 0)
            {
                return Json(new JsonResponse() { Result = true, Response = "A�adido con �xito", Content = new { quality.Id } });
            }
            return Error();
        }

        #endregion

        #region DELETE

        [HttpGet]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        [Route("~/[controller]/[action]/{qualityid:int}")]
        public JsonResult DeleteCheck(int qualityid)
        {
            string response = "";
            bool ActionAllowed = true;

            Quality quality = QualityService.Get(qualityid);

            if (quality.Users.Any(qu => qu.QualityCompleted))
            {
                ActionAllowed = false; response = Localizer["No es posible eliminar una campa�a que haya sido finalizada por alg�n usuario."];
            }else if (quality.Audits.Any(qa => qa.Users.Any())
                      || (quality.Exams.Any(qe => qe.Users.Any())))
            {
                ActionAllowed = false; response = Localizer["No es posible eliminar una campa�a con usuarios asignados."];
            }

            return Json(data: new JsonResponse() { Result = ActionAllowed, Response = response });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateAjaxModelState]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        [Route("~/[controller]/[action]/{qualityid:int}")]
        public IActionResult Delete(int qualityid)
        {
            Quality quality = QualityService.Get(qualityid);
            if (quality == null) return Error(StatusCodes.Status406NotAcceptable);

            if (QualityService.Remove(quality) > 0)
            {
                return Json(data: new JsonResponse() { Result = true, Response = "Eliminado con �xito" });
            }
            return Error();
        }

        #endregion

        #region EXAM
            #region SHOW

        [HttpGet]
        [Route("/[controller]/{qualityid:int}/exam/show/{examid:int}")]
        public IActionResult ShowExam(int qualityid, int examid)
        {
            Quality quality = QualityService.Get(qualityid);
            if (quality == null) return Error(StatusCodes.Status406NotAcceptable);

            QualityExam exam = quality.Exams.SingleOrDefault(qe => qe.Id == examid);
            if (exam == null) return Error(StatusCodes.Status406NotAcceptable);

            QualityExamViewModel model = Mapper.Map<QualityExamViewModel>(exam);

            return View("ShowExam", model);
        }

            #endregion
            #region EDIT

        [HttpGet]
        [Route("/[controller]/{qualityid:int}/exam/edit/{examid:int}")]
        public IActionResult EditExam(int qualityid, int examid)
        {
            Quality quality = QualityService.Get(qualityid);
            if (quality == null) return Error(StatusCodes.Status406NotAcceptable);

            QualityExam exam = quality.Exams.SingleOrDefault(qe => qe.Id == examid);
            if (exam == null) return Error(StatusCodes.Status406NotAcceptable);
            if (exam.Users.Any(qeu => qeu.Takes.Count > 0)) return Error(StatusCodes.Status406NotAcceptable);

            QualityExamViewModel model = Mapper.Map<QualityExamViewModel>(exam);

            return View("EditExam", model);
        }

        [HttpPost]
        [Route("/[controller]/{qualityid:int}/exam/edit/{examid:int}")]
        [ValidateAntiForgeryToken]
        public JsonResult EditExam([FromBody] dynamic data)
        {
            int qualityid = data.QualityId.ToObject<int>();
            int examid = data.ExamId.ToObject<int>();

            Quality quality = QualityService.Get(qualityid);
            QualityExam exam = quality.Exams.SingleOrDefault(qe => qe.Id == examid);

            exam.Sections.Clear();
            if (QualityService.Update(quality) > 0)
            {
                Mapper.Map(data, exam);
                if (QualityService.Update(quality) > 0)
                {
                    return Json(data: new JsonResponse() { Result = true, Response = "OK", Content = data });
                }
            }
            return Json(data: new JsonResponse() { Result = false, Response = "KO", Content = data });
        }

            #endregion
            #region ADD

        [HttpGet]
        [Route("/[controller]/{qualityid:int}/exam/add")]
        public IActionResult AddExam(int? qualityid)
        {
            Quality quality = QualityService.Get(qualityid);
            if (quality == null) return Error(StatusCodes.Status406NotAcceptable);

            QualityExamViewModel model = new QualityExamViewModel
            {
                QualityId = quality.Id,
                QualityName = quality.Name,
                StartDate = DateTime.Now
            };
            return View("EditExam", model);
        }

        [HttpPost]
        [Route("/[controller]/{id:int}/exam/add")]
        [ValidateAntiForgeryToken]
        public IActionResult AddExam([FromBody] dynamic data)
        {
            int qualityid = data.QualityId.ToObject<int>();
            int examid = data.ExamId.ToObject<int>();

            Quality quality = QualityService.Get(qualityid);
            QualityExam exam = quality.Exams.SingleOrDefault(qe => qe.Id == examid);

            exam = Mapper.Map<QualityExam>(data);
            exam.Owners.Add(new QualityExamOwner { UserId = AuthUser.Id, Exam = exam });
            quality.Exams.Add(exam);

            if (QualityService.Update(quality) > 0)
            {
                return Json(data: new JsonResponse() { Result = true, Response = "OK", Content = data });
            }
            return Json(data: new JsonResponse() { Result = false, Response = "KO", Content = data });
        }

            #endregion
            #region TAKE

        [HttpGet]
        [Route("/[controller]/{qualityid:int}/exam/{examid:int}/take/confirm")]
        public IActionResult ConfirmExamTake(int qualityid, int examid)
        {
            Quality quality = QualityService.Get(qualityid);
            if (quality == null) return Error(StatusCodes.Status406NotAcceptable);

            QualityExam exam = quality.Exams.SingleOrDefault(qe => qe.Id == examid);
            if (exam == null) return Error(StatusCodes.Status406NotAcceptable);

            QualityUser QualityUser = quality.Users.SingleOrDefault(qu => qu.UserId == AuthUser.Id);
            if (QualityUser == null) return Error();

            QualityExamUser ExamUser = exam.Users.SingleOrDefault(qeu => qeu.UserId == AuthUser.Id);
            if (ExamUser == null) return Error();
            if (!(ExamUser.Takes.All(qeut => qeut.Pass == false))) { return Error(); }

            ConfirmExamTakeViewModel model = Mapper.Map<ConfirmExamTakeViewModel>(exam);
            model.Attempt = ExamUser.Takes.Count() + 1;
            model.AttemptsExceeded = (model.Attempt > exam.Attempts);

            return PartialView("_ConfirmExamTake", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateAjaxModelState]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        [Route("/[controller]/{qualityid:int}/exam/{examid:int}/take/confirm")]
        public IActionResult ConfirmExamTake(ConfirmExamTakeViewModel model)
        {
            Quality quality = QualityService.Get(model.QualityId);
            if (quality == null) return Error(StatusCodes.Status406NotAcceptable);

            QualityExam exam = quality.Exams.SingleOrDefault(qe => qe.Id == model.ExamId);
            if (exam == null) return Error(StatusCodes.Status406NotAcceptable);

            QualityUser QualityUser = quality.Users.SingleOrDefault(qu => qu.UserId == AuthUser.Id);
            if (QualityUser == null) return Error();

            QualityExamUser ExamUser = exam.Users.SingleOrDefault(qeu => qeu.UserId == AuthUser.Id);
            if (ExamUser == null) return Error();

            QualityExamUserTake Take = new QualityExamUserTake { StartDate = DateTime.Now };
            Mapper.Map(exam.Sections, Take.Sections);
            ExamUser.Takes.Add(Take);

            if (QualityService.Update(quality) > 0)
            {
                return Json(new JsonResponse() { Result = true, Response = "A�adido con �xito", Content = new { Take.Id } });
            }
            return Error();
        }

        [HttpGet]
        [Route("/[controller]/{qualityid:int}/exam/{examid:int}/take/{takeid:int}")]
        public IActionResult TakeExam(int qualityid, int examid, int takeid)
        {
            Quality quality = QualityService.Get(qualityid);
            if (quality == null) return Error(StatusCodes.Status406NotAcceptable);

            QualityExam exam = quality.Exams.SingleOrDefault(qe => qe.Id == examid);
            if (exam == null) return Error(StatusCodes.Status406NotAcceptable);

            QualityExamUser ExamUser = exam.Users.SingleOrDefault(qeu => qeu.UserId == AuthUser.Id);
            if (ExamUser == null) return Error(StatusCodes.Status406NotAcceptable);

            QualityExamUserTake take = exam.Users.SelectMany(qeu => qeu.Takes).SingleOrDefault(qeut => qeut.Id == takeid);
            if (take == null) return Error(StatusCodes.Status406NotAcceptable);
            if (!take.Active || take.Attempt > 0) return Error();

            take.Attempt = ExamUser.Takes.Count();

            if (!(QualityService.Update(quality) > 0)) return Error();
           
            QualityExamTakeViewModel model = Mapper.Map<QualityExamTakeViewModel>(exam);
            model.ExamUserId = ExamUser.Id;
            model.Attempt = ExamUser.Takes.Count();

            model.TakeId = take.Id;
            model.StartDate = take.StartDate;
            model.EndDate = take.EndDate;
            model.Pass = take.Pass;
            model.ExamUserId = take.ExamUserId;
            model.Attempt = take.Attempt;

            model.Sections.ToList().ForEach(section =>
            {
                section.Fields.ToList().ForEach(field =>
                {
                    field.Options.ToList().ForEach(option =>
                    {
                        option.ExamOptionId = take.Sections.FirstOrDefault(s => s.ExamSectionId == section.Id).Fields.FirstOrDefault(f => f.ExamFieldId == field.Id).Options.FirstOrDefault(o => o.ExamOptionId == option.Id).ExamOptionId;
                        option.Id = take.Sections.FirstOrDefault(s => s.ExamSectionId == section.Id).Fields.FirstOrDefault(f => f.ExamFieldId == field.Id).Options.FirstOrDefault(o => o.ExamOptionId == option.Id).Id;
                    });

                    field.ExamFieldId = take.Sections.FirstOrDefault(s => s.ExamSectionId == section.Id).Fields.FirstOrDefault(f => f.ExamFieldId == field.Id).ExamFieldId;
                    field.Id = take.Sections.FirstOrDefault(s => s.ExamSectionId == section.Id).Fields.FirstOrDefault(f => f.ExamFieldId == field.Id).Id;
                });

                section.ExamSectionId = take.Sections.FirstOrDefault(s => s.ExamSectionId == section.Id).ExamSectionId;
                section.Id = take.Sections.FirstOrDefault(s => s.ExamSectionId == section.Id).Id;
            });
                                                        
            return View("TakeExam", model);
        }

        [HttpPost]
        [Route("/[controller]/{qualityid:int}/exam/{examid:int}/take/{takeid:int}")]
        [ValidateAntiForgeryToken]
        public IActionResult TakeExam([FromBody] dynamic data)
        {
            int qualityid = data.QualityId.ToObject<int>();
            int examid = data.ExamId.ToObject<int>();
            int takeid = data.TakeId.ToObject<int>();

            Quality quality = QualityService.Get(qualityid);
            QualityExam exam = quality.Exams.SingleOrDefault(qe => qe.Id == examid);
            QualityExamUserTake take = exam.Users.SelectMany(ttu => ttu.Takes).Single(ttut => ttut.Id == takeid);

            take.Sections.Clear();
            if (QualityService.Update(quality) > 0)
            {
                Mapper.Map(data, take);
                if (QualityService.Update(quality) > 0)
                {
                    return Json(data: new JsonResponse() { Result = true, Response = "OK", Content = new { take.Id } });
                }
            }
            return Error();
        }

            #endregion
            #region SHOW_TAKE

        [HttpGet]
        [Route("/[controller]/{qualityid:int}/exam/show/{examid:int}/take/{takeid:int}")]
        public IActionResult ShowExamTake(int qualityid, int examid, int takeid)
        {
            Quality quality = QualityService.Get(qualityid);
            if (quality == null) return Error(StatusCodes.Status406NotAcceptable);

            QualityExam exam = quality.Exams.SingleOrDefault(qe => qe.Id == examid);
            if (exam == null) return Error(StatusCodes.Status406NotAcceptable);

            QualityExamUserTake take = exam.Users.SelectMany(qeu => qeu.Takes).SingleOrDefault(qeut => qeut.Id == takeid);
            if (take == null || take.Active) return Error(StatusCodes.Status406NotAcceptable);

            QualityExamTakeViewModel model = Mapper.Map<QualityExamTakeViewModel>(exam);

            model.TakeId = take.Id;
            model.StartDate = take.StartDate;
            model.EndDate = take.EndDate;
            model.Pass = take.Pass;

            model.Sections.ToList().ForEach(section =>
            {
                section.Fields.ToList().ForEach(field =>
                {
                    field.Mark = take.Sections.FirstOrDefault(s => s.ExamSectionId == section.Id).Fields.FirstOrDefault(f => f.ExamFieldId == field.Id).Mark;
                    field.Answer = take.Sections.FirstOrDefault(s => s.ExamSectionId == section.Id).Fields.FirstOrDefault(f => f.ExamFieldId == field.Id).Answer;

                    field.Options.ToList().ForEach(option =>
                    {
                        option.Checked = take.Sections.FirstOrDefault(s => s.ExamSectionId == section.Id).Fields.FirstOrDefault(f => f.ExamFieldId == field.Id).Options.FirstOrDefault(o => o.ExamOptionId == option.Id).Mark;
                    });
                });
            });

            return View("ShowExamTake", model);
        }

            #endregion
            #region MARK_TAKE

        [HttpGet]
        [Route("/[controller]/{qualityid:int}/exam/mark/{examid:int}/take/{takeid:int}")]
        public IActionResult MarkExamTake(int qualityid, int examid, int takeid)
        {
            Quality quality = QualityService.Get(qualityid);
            if (quality == null) return Error(StatusCodes.Status406NotAcceptable);

            QualityExam exam = quality.Exams.SingleOrDefault(qe => qe.Id == examid);
            if (exam == null) return Error(StatusCodes.Status406NotAcceptable);

            QualityExamUserTake take = exam.Users.SelectMany(qeu => qeu.Takes).SingleOrDefault(qeut => qeut.Id == takeid);
            if (take == null) return Error(StatusCodes.Status406NotAcceptable);

            if (take.Active)
            {
                if ((DateTime.Now - take.StartDate).TotalMinutes > (exam.Time ?? 120))
                {
                    take.Active = false;
                    take.Content = "Finalized at marking";
                    if (!(QualityService.Update(quality) > 0)) { return Error(); }
                }
                else { return Error(StatusCodes.Status406NotAcceptable); }
            }

            QualityExamTakeViewModel model = Mapper.Map<QualityExamTakeViewModel>(exam);

            model.TakeId = take.Id;
            model.StartDate = take.StartDate;
            model.EndDate = take.EndDate;
            model.Pass = take.Pass;
            model.ExamUserId = take.ExamUserId;
            model.Attempt = take.Attempt;

            model.Sections.ToList().ForEach(section =>
            {
                section.Fields.ToList().ForEach(field =>
                {
                    field.Options.ToList().ForEach(option =>
                    {
                        option.Checked = take.Sections.FirstOrDefault(s => s.ExamSectionId == section.Id).Fields.FirstOrDefault(f => f.ExamFieldId == field.Id).Options.FirstOrDefault(o => o.ExamOptionId == option.Id).Mark;
                        option.ExamOptionId = take.Sections.FirstOrDefault(s => s.ExamSectionId == section.Id).Fields.FirstOrDefault(f => f.ExamFieldId == field.Id).Options.FirstOrDefault(o => o.ExamOptionId == option.Id).ExamOptionId;
                        option.Id = take.Sections.FirstOrDefault(s => s.ExamSectionId == section.Id).Fields.FirstOrDefault(f => f.ExamFieldId == field.Id).Options.FirstOrDefault(o => o.ExamOptionId == option.Id).Id;
                    });

                    field.Mark = take.Sections.FirstOrDefault(s => s.ExamSectionId == section.Id).Fields.FirstOrDefault(f => f.ExamFieldId == field.Id).Mark;
                    field.Answer = take.Sections.FirstOrDefault(s => s.ExamSectionId == section.Id).Fields.FirstOrDefault(f => f.ExamFieldId == field.Id).Answer;
                    field.ExamFieldId = take.Sections.FirstOrDefault(s => s.ExamSectionId == section.Id).Fields.FirstOrDefault(f => f.ExamFieldId == field.Id).ExamFieldId;
                    field.Id = take.Sections.FirstOrDefault(s => s.ExamSectionId == section.Id).Fields.FirstOrDefault(f => f.ExamFieldId == field.Id).Id;
                });

                section.ExamSectionId = take.Sections.FirstOrDefault(s => s.ExamSectionId == section.Id).ExamSectionId;
                section.Id = take.Sections.FirstOrDefault(s => s.ExamSectionId == section.Id).Id;
            });

            return View("MarkExamTake", model);
        }

        [HttpPost]
        [Route("/[controller]/{qualityid:int}/exam/mark/{examid:int}/take/{takeid:int}")]
        [ValidateAntiForgeryToken]
        public IActionResult MarkExamTake([FromBody] dynamic data)
        {
            int qualityid = data.QualityId.ToObject<int>();
            int examid = data.ExamId.ToObject<int>();
            int takeid = data.Id.ToObject<int>();

            Quality quality = QualityService.Get(qualityid);
            QualityExam exam = quality.Exams.SingleOrDefault(tt => tt.Id == examid);
            QualityExamUserTake take = exam.Users.SelectMany(ttu => ttu.Takes).Single(ttut => ttut.Id == takeid);

            take.Sections.Clear();
            if (QualityService.Update(quality) > 0)
            {
                Mapper.Map(data, take);
                take.Mark = take.Sections.Sum(ts => ts.Mark);
                take.Pass = take.Mark >= data.PassMark.ToObject<double>();

                User user = take.ExamUser.User;
                IEnumerable<QualityExamUser> userexams = quality.Exams.SelectMany(qe => qe.Users).Where(qeu => qeu.User == user);
                if (userexams.All(qeu => qeu.Takes.Any(qeut => qeut.Pass == true)))
                {
                    quality.Users.Single(qu => qu.User == user).QualityCompleted = true;
                }

                if (QualityService.Update(quality) > 0)
                {
                    return Json(data: new JsonResponse() { Result = true, Response = "OK", Content = new { take.Id } });
                }
            }
            return Error();
        }

        #endregion
            #region ASSOCIATE_USERS

        [HttpGet]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        [Route("/[controller]/{qualityid:int}/exam/associate/{examid:int}")]
        public IActionResult ExamAssociateUsers(int qualityid, int examid)
        {
            Quality quality = QualityService.Get(qualityid);
            if (quality == null) return Error(StatusCodes.Status406NotAcceptable);

            QualityExam exam = quality.Exams.SingleOrDefault(qe => qe.Id == examid);
            if (exam == null) return Error(StatusCodes.Status406NotAcceptable);

            QualityExamViewModel model = Mapper.Map<QualityExamViewModel>(exam);

            return View("ExamAssociateUsers", model);
        }

            #endregion
        #endregion

        #region AUDIT
            #region SHOW

        [HttpGet]
        [Route("/[controller]/{qualityid:int}/audit/show/{auditid:int}")]
        public IActionResult ShowAudit(int qualityid, int auditid)
        {
            Quality quality = QualityService.Get(qualityid);
            if (quality == null) return Error(StatusCodes.Status406NotAcceptable);

            QualityAudit audit = quality.Audits.SingleOrDefault(qa => qa.Id == auditid);
            if (audit == null) return Error(StatusCodes.Status406NotAcceptable);

            QualityAuditViewModel model = Mapper.Map<QualityAuditViewModel>(audit);

            return View("ShowAudit", model);
        }

            #endregion
            #region EDIT

        [HttpGet]
        [Route("/[controller]/{qualityid:int}/audit/edit/{auditid:int}")]
        public IActionResult EditAudit(int qualityid, int auditid)
        {
            Quality quality = QualityService.Get(qualityid);
            if (quality == null) return Error(StatusCodes.Status406NotAcceptable);

            QualityAudit audit = quality.Audits.SingleOrDefault(qa => qa.Id == auditid);
            if (audit == null) return Error(StatusCodes.Status406NotAcceptable);

            if (audit.Users.Any(qau => qau.Takes.Count > 0)) return Error(StatusCodes.Status406NotAcceptable);

            QualityAuditViewModel model = Mapper.Map<QualityAuditViewModel>(audit);

            return View("EditAudit", model);
        }

        [HttpPost]
        [Route("/[controller]/{qualityid:int}/audit/edit/{auditid:int}")]
        [ValidateAntiForgeryToken]
        public JsonResult EditAudit([FromBody] dynamic data)
        {
            int qualityid = data.QualityId.ToObject<int>();
            int auditid = data.AuditId.ToObject<int>();

            Quality quality = QualityService.Get(qualityid);
            QualityAudit audit = quality.Audits.SingleOrDefault(qa => qa.Id == auditid);

            audit.Sections.Clear();
            if (QualityService.Update(quality) > 0)
            {
                Mapper.Map(data, audit);
                if (QualityService.Update(quality) > 0)
                {
                    return Json(data: new JsonResponse() { Result = true, Response = "OK", Content = data });
                }
            }
            return Json(data: new JsonResponse() { Result = false, Response = "KO", Content = data });
        }

            #endregion
            #region ADD

        [HttpGet]
        [Route("/[controller]/{qualityid:int}/audit/add")]
        public IActionResult AddAudit(int? qualityid)
        {
            Quality quality = QualityService.Get(qualityid);
            if (quality == null) return Error(StatusCodes.Status406NotAcceptable);

            QualityAuditViewModel model = new QualityAuditViewModel
            {
                QualityId = quality.Id,
                QualityName = quality.Name,
                StartDate = DateTime.Now
            };
            return View("EditAudit", model);
        }

        [HttpPost]
        [Route("/[controller]/{qualityid:int}/audit/add")]
        [ValidateAntiForgeryToken]
        public IActionResult AddAudit([FromBody] dynamic data)
        {
            int qualityid = data.QualityId.ToObject<int>();
            Quality quality = QualityService.Get(qualityid);

            QualityAudit audit = Mapper.Map<QualityAudit>(data);
            audit.Owners.Add(new QualityAuditOwner { UserId = AuthUser.Id, Audit = audit });
            quality.Audits.Add(audit);

            if (QualityService.Update(quality) > 0)
            {
                return Json(data: new JsonResponse() { Result = true, Response = "OK", Content = data });
            }
            return Json(data: new JsonResponse() { Result = false, Response = "KO", Content = data });
        }

            #endregion
            #region TAKE

        [HttpGet]
        [Route("/[controller]/{qualityid:int}/audit/{auditid:int}/take/{userid:int}")]
        public IActionResult TakeAudit(int qualityid, int auditid, int userid)
        {
            Quality quality = QualityService.Get(qualityid);
            if (quality == null) return Error(StatusCodes.Status406NotAcceptable);

            QualityAudit audit = quality.Audits.SingleOrDefault(qa => qa.Id == auditid);
            if (audit == null) return Error(StatusCodes.Status406NotAcceptable);

            QualityOwner qualityOwner = quality.Owners.SingleOrDefault(qu => qu.OwnerId == AuthUser.Id);
            if (qualityOwner == null) return Error();

            QualityUser qualityUser = quality.Users.SingleOrDefault(qu => qu.UserId == userid);
            if (qualityUser == null) return Error();

            QualityAuditUser auditUser = audit.Users.SingleOrDefault(qau => qau.UserId == userid);
            if (auditUser == null) return Error();

            if (auditUser.Takes.Count() > 0) return Error();
            if (auditUser.AuditOwner.User != AuthUser) return Error();

            QualityAuditTakeViewModel model = Mapper.Map<QualityAuditTakeViewModel>(audit);
            model.AuditUserId = auditUser.Id;

            return View("TakeAudit", model);
        }

        [HttpPost]
        [Route("/[controller]/{qualityid:int}/audit/{auditid:int}/take/{userid:int}")]
        [ValidateAntiForgeryToken]
        public IActionResult TakeAudit([FromBody] dynamic data)
        {
            int qualityid = data.QualityId.ToObject<int>();
            int auditid = data.AuditId.ToObject<int>();
            int userid = data.AuditUserId.ToObject<int>();

            Quality quality = QualityService.Get(qualityid);
            if (quality == null) return Error(StatusCodes.Status406NotAcceptable);

            QualityAudit audit = quality.Audits.SingleOrDefault(qa => qa.Id == auditid);
            if (audit == null) return Error(StatusCodes.Status406NotAcceptable);

            QualityAuditUser auditUser = audit.Users.SingleOrDefault(qau => qau.Id == userid);
            if (auditUser == null) return Error(StatusCodes.Status406NotAcceptable);

            QualityAuditUserTake take = Mapper.Map<QualityAuditUserTake>(data);
            take.AuditorId = AuthUser.Id;

            double TakeMark = 0;
            QualityAuditTakeViewModel model = Mapper.Map<QualityAuditTakeViewModel>(audit);
            
            model.Sections.ToList().ForEach(section =>
            {
                double SectionMark = 0;

                section.Fields.ToList().ForEach(field =>
                {
                    double FieldMark = take.Sections.FirstOrDefault(s => s.AuditSectionId == section.Id).Fields.FirstOrDefault(f => f.AuditFieldId == field.Id).Mark;
                    bool CorrectAnswer = true;

                    field.Options.ToList().ForEach(option =>
                    {
                        bool? AuditChecked = option.Mark;
                        bool? TakeChecked = take.Sections.FirstOrDefault(s => s.AuditSectionId == section.Id).Fields.FirstOrDefault(f => f.AuditFieldId == field.Id).Options.FirstOrDefault(o => o.AuditOptionId == option.Id).Mark;
                        if (AuditChecked != TakeChecked) { CorrectAnswer = false; }
                    });

                    FieldMark = ((FieldMark == 0) && (CorrectAnswer)) ? field.Weighing : FieldMark;
                    take.Sections.FirstOrDefault(s => s.AuditSectionId == section.Id).Fields.FirstOrDefault(f => f.AuditFieldId == field.Id).Mark = FieldMark;

                    SectionMark += FieldMark;
                });

                take.Sections.FirstOrDefault(s => s.AuditSectionId == section.Id).Mark = SectionMark;
                TakeMark += SectionMark;
            });

            take.Mark = TakeMark;
            take.Pass = take.Mark >= audit.PassMark;
            auditUser.Takes.Add(take);

            IEnumerable<QualityAuditUser> useraudits = quality.Audits.SelectMany(qa => qa.Users).Where(qau => qau.User.Id == auditUser.User.Id);
            if (useraudits.All(qau => qau.Takes.Any(qaut => qaut.Pass == true)))
            {
                quality.Users.Single(qu => qu.User.Id == auditUser.User.Id).QualityCompleted = true;
            }

            if (QualityService.Update(quality) > 0)
            {
                return Json(new JsonResponse() { Result = true, Response = "Actualizado con �xito", Content = new { take.Id } });
            }
            return Error();
        }

            #endregion
            #region SHOW_TAKE

        [HttpGet]
        [Route("/[controller]/{qualityid:int}/audit/show/{auditid:int}/take/{takeid:int}")]
        public IActionResult ShowAuditTake(int qualityid, int auditid, int takeid)
        {
            Quality quality = QualityService.Get(qualityid);
            if (quality == null) return Error(StatusCodes.Status406NotAcceptable);

            QualityAudit audit = quality.Audits.SingleOrDefault(qa => qa.Id == auditid);
            if (audit == null) return Error(StatusCodes.Status406NotAcceptable);

            QualityAuditUserTake take = audit.Users.SelectMany(qau => qau.Takes).SingleOrDefault(qaut => qaut.Id == takeid);
            if (take == null || take.Active) return Error(StatusCodes.Status406NotAcceptable);

            QualityAuditTakeViewModel model = Mapper.Map<QualityAuditTakeViewModel>(audit);

            model.TakeId = take.Id;
            model.StartDate = take.StartDate;
            model.EndDate = take.EndDate;
            model.Pass = take.Pass;
            model.isAuditUser = take.AuditUser.User.Id == AuthUser.Id;

            model.Sections.ToList().ForEach(section =>
            {
                section.Fields.ToList().ForEach(field =>
                {
                    field.Mark = take.Sections.FirstOrDefault(s => s.AuditSectionId == section.Id).Fields.FirstOrDefault(f => f.AuditFieldId == field.Id).Mark;
                    field.Comment = take.Sections.FirstOrDefault(s => s.AuditSectionId == section.Id).Fields.FirstOrDefault(f => f.AuditFieldId == field.Id).Comment;

                    field.Options.ToList().ForEach(option =>
                    {
                        option.Checked = take.Sections.FirstOrDefault(s => s.AuditSectionId == section.Id).Fields.FirstOrDefault(f => f.AuditFieldId == field.Id).Options.FirstOrDefault(o => o.AuditOptionId == option.Id).Mark;
                    });
                });
            });

            return View("ShowAuditTake", model);
        }

            #endregion
            #region ASSOCIATE_USERS

        [HttpGet]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        [Route("/[controller]/{qualityid:int}/audit/associate/{auditid:int}")]
        public IActionResult AuditAssociateUsers(int qualityid, int auditid)
        {
            Quality quality = QualityService.Get(qualityid);
            if (quality == null) return Error(StatusCodes.Status406NotAcceptable);

            QualityAudit audit = quality.Audits.SingleOrDefault(qa => qa.Id == auditid);
            if (audit == null) return Error(StatusCodes.Status406NotAcceptable);

            QualityAuditViewModel model = Mapper.Map<QualityAuditViewModel>(audit);

            return View("AuditAssociateUsers", model);
        }

            #endregion
            #region REQUEST_REVIEW

        [HttpGet]
        [Route("/[controller]/{qualityid:int}/audit/show/{auditid:int}/take/{takeid:int}/requestreview")]
        public IActionResult RequestReview(int qualityid, int auditid, int takeid)
        {
            Quality quality = QualityService.Get(qualityid);
            if (quality == null) return Error(StatusCodes.Status406NotAcceptable);

            QualityAudit audit = quality.Audits.SingleOrDefault(qa => qa.Id == auditid);
            if (audit == null) return Error(StatusCodes.Status406NotAcceptable);

            QualityAuditUserTake take = audit.Users.SelectMany(qau => qau.Takes).SingleOrDefault(qaut => qaut.Id == takeid);
            if (take == null || take.Active) return Error(StatusCodes.Status406NotAcceptable);

            QualityAuditTakeRequestViewModel model = Mapper.Map<QualityAuditTakeRequestViewModel>(audit);
            model.TakeId = take.Id;

            return PartialView("_RequestReview", model);
        }

        [HttpPost]
        [Route("/[controller]/{qualityid:int}/audit/show/{auditid:int}/take/{takeid:int}/requestreview")]
        [ValidateAntiForgeryToken]
        public IActionResult RequestReview(QualityAuditTakeRequestViewModel model)
        {
            Quality quality = QualityService.Get(model.QualityId);
            if (quality == null) return Error(StatusCodes.Status406NotAcceptable);

            QualityAudit audit = quality.Audits.SingleOrDefault(qa => qa.Id == model.Id);
            if (audit == null) return Error(StatusCodes.Status406NotAcceptable);

            QualityAuditUserTake take = audit.Users.SelectMany(qau => qau.Takes).SingleOrDefault(qaut => qaut.Id == model.TakeId);
            if (take == null || take.Active) return Error(StatusCodes.Status406NotAcceptable);

            Request request = new Request
            {
                Type = RequestType.Other,
                RequestDate = DateTime.Now,
                Priority = PriorityType.Low,
                Summary = Localizer["Revisi�n de Auditor�a"] + " " + model.Name,
                Description = model.Summary,
                AdditionalData = "{\"QualityId\":\"" + quality.Id + "\", \"AuditId\":\"" + audit.Id + "\", \"TakeId\":\"" + take.Id + "\"}",
                User = AuthUser
            };
            request.RequestHistory.Add(new RequestHistory
            {
                EntryDate = request.RequestDate,
                State = RequestState.Created,
                Comment = Localizer["Solicitud Creada"],
                User = AuthUser
            });
            request.RequestHistory.Add(new RequestHistory
            {
                EntryDate = DateTime.Now,
                State = RequestState.Pending,
                Comment = Localizer["Pendiente de Asignaci�n"],
                User = AuthUser
            });

            request.AssignedUser = take.Auditor;
            request.RequestHistory.Add(new RequestHistory
            {
                EntryDate = DateTime.Now,
                State = RequestState.InProgress,
                Comment = Localizer["Asignaci�n autom�tica a"] + " " + take.Auditor.FullName,
                User = AuthUser
            });

            RequestService.Add(request);
            if (request.Id > 0)
            {
                User supervisor = request.User.Supervisor;
                if (supervisor != null)
                {
                    Notification notification = new Notification()
                    {
                        User = supervisor,
                        Summary = "Nueva solicitud de Revisi�n de Auditor�a: " + request.Id,
                        Description = request.User.FullName + " ha creado una solicitud de Revisi�n de Auditor�a con identificador " + request.Id + " el " + request.RequestDate,
                        StartDate = DateTime.Now,
                        Active = true,
                        Priority = false,
                        Url = "/request/show/" + request.Id,
                        Type = NotificationType.alert
                    };
                    NotificationService.Add(notification);
                    if (notification.Id > 0)
                    {
                        supervisor.Alerts++;
                        UserService.Update(supervisor);
                    }
                }

                if (request.AssignedUser != null)
                {
                    Notification notification = new Notification()
                    {
                        User = request.AssignedUser,
                        Summary = "Asignada nueva solicitud de Revisi�n de Auditor�a: " + request.Id,
                        Description = "Se le ha asignado de manera autom�tica una solicitud de Revisi�n de Auditor�a con identificador " + request.Id + " creada por " + request.User.FullName + " el " + request.RequestDate,
                        StartDate = DateTime.Now,
                        Active = true,
                        Priority = false,
                        Url = "/request/show/" + request.Id,
                        Type = NotificationType.alert
                    };
                    NotificationService.Add(notification);
                    if (notification.Id > 0)
                    {
                        request.AssignedUser.Alerts++;
                        UserService.Update(request.AssignedUser);
                    }
                }

                EmailService.SendAsync(AuthUser.Email, "Solicitud S00" + request.Id + " creada", "Se ha creado la solicitud S00" + request.Id);
                JsonResponse response = new JsonResponse() { Result = true, Response = "La petici�n se ha creado con �xito", Content = new { Id = request.Id } };
                return Json(response);
            }
            return Error();
        }

           #endregion
        #endregion

        #region FORMS
            #region DELETE
        [HttpGet]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        [Route("/[controller]/{qualityid:int}/{formtype}/deletecheck/{formid:int}")]
        public JsonResult DeleteFormCheck(int qualityid, string formtype, int formid)
        {
            string response = "";
            bool ActionAllowed = true;

            Quality quality = QualityService.Get(qualityid);

            if (formtype == "audit")
            {
                if (quality.Audits.SingleOrDefault(qa =>qa.Id == formid).Users.Count > 0)
                {
                    ActionAllowed = false; response = Localizer["No es posible eliminar el modelo de una auditor�a con usuarios asignados."];
                }
            }
            if (formtype == "exam")
            {
                if (quality.Exams.SingleOrDefault(qe => qe.Id == formid).Users.Count > 0)
                {
                    ActionAllowed = false; response = Localizer["No es posible eliminar el modelo de un examen con usuarios asignados."];
                }
            }
            
            return Json(data: new JsonResponse() { Result = ActionAllowed, Response = response });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateAjaxModelState]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        [Route("/[controller]/{qualityid:int}/{formtype}/delete/{formid:int}")]
        public IActionResult DeleteForm(int qualityid, string formtype, int formid)
        {
            Quality quality = QualityService.Get(qualityid);
            if (quality == null) return Error(StatusCodes.Status406NotAcceptable);

            switch (formtype)
            {
                case "audit":
                    QualityAudit audit = quality.Audits.SingleOrDefault(qa => qa.Id == formid);
                    if (audit == null) return Error(StatusCodes.Status406NotAcceptable);
                    quality.Audits.Remove(audit);
                    break;
                case "exam":
                    QualityExam exam = quality.Exams.SingleOrDefault(qe => qe.Id == formid);
                    if (exam == null) return Error(StatusCodes.Status406NotAcceptable);
                    quality.Exams.Remove(exam);
                    break;
                default: return Error(StatusCodes.Status406NotAcceptable);
            }

            if (QualityService.Update(quality) > 0)
            {
                return Json(data: new JsonResponse() { Result = true, Response = "Eliminado con �xito", Content = new { quality.Id } });
            }
            return Error();
        }

            #endregion
            #region EDIT_CHECK

        [HttpGet]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        [Route("/[controller]/{qualityid:int}/{formtype}/editcheck/{formid:int}")]
        public JsonResult EditTestCheck(int qualityid, string formtype, int formid)
        {
            string response = "";
            bool ActionAllowed = true;

            Quality quality = QualityService.Get(qualityid);

            if (formtype == "audit")
            {
                if (quality.Audits.SingleOrDefault(qa => qa.Id == formid).Users.Any(qau => qau.Takes.Count > 0))
                {
                    ActionAllowed = false; response = Localizer["No es posible editar el modelo de una auditor�a que ha sido realizada por un usuario."];
                }
            }
            if (formtype == "exam")
            {
                if (quality.Exams.SingleOrDefault(qe => qe.Id == formid).Users.Any(qeu => qeu.Takes.Count > 0))
                {
                    ActionAllowed = false; response = Localizer["No es posible editar el modelo de un examen que ha sido realizado por un usuario."];
                }
            }

            return Json(data: new JsonResponse() { Result = ActionAllowed, Response = response });
        }

            #endregion
            #region ASSOCIATE_USERS

        [HttpGet]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        [Route("/[controller]/{qualityid:int}/{formtype}/associate/{formid:int}/{owneruserid:int}")]
        public IActionResult AssociateUsers(int qualityid, string formtype, int formid, int owneruserid)
        {
            Quality quality = QualityService.Get(qualityid);
            if (quality == null) return Error(StatusCodes.Status406NotAcceptable);
            List<User> AddibleUsers = new List<User>();
            dynamic form;

            switch (formtype)
            {
                case "audit":
                    form = quality.Audits.SingleOrDefault(qa => qa.Id == formid);
                    if (form == null) return Error(StatusCodes.Status406NotAcceptable);

                    AddibleUsers = UserService.GetMany(u => u.QualityUser.Any(qu => qu.QualityId == qualityid) && !u.QualityAuditUser.Any(qau => qau.AuditId == formid), null, false).ToList();
                    break;
                case "exam":
                    form = quality.Exams.SingleOrDefault(qe => qe.Id == formid);
                    if (form == null) return Error(StatusCodes.Status406NotAcceptable);

                    AddibleUsers = UserService.GetMany(u => u.QualityUser.Any(qu => qu.QualityId == qualityid) && !u.QualityExamUser.Any(qeu => qeu.ExamId == formid), null, false).ToList();
                    break;
                default: return Error(StatusCodes.Status406NotAcceptable);
            }

            QualityAssociateUserViewModel model = new QualityAssociateUserViewModel
            {
                QualityId = qualityid,
                FormId = formid,
                FormType = formtype,
                OwnerUserId = owneruserid,
                AddibleUsers = AddibleUsers
            };

            return PartialView("_AssociateUser", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateAjaxModelState]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        [Route("/[controller]/{qualityid:int}/{formtype}/associate/{formid:int}/{owneruserid:int}")]
        public IActionResult AssociateUsers(QualityAssociateUserViewModel model)
        {
            List<int> UsersToAddIdInt = model.UsersToAddIds.Split(";").Select(Int32.Parse).ToList();
            dynamic form;

            Quality quality = QualityService.Get(model.QualityId);
            if (quality == null) return Error(StatusCodes.Status406NotAcceptable);

            switch (model.FormType)
            {
                case "audit":
                    form = quality.Audits.SingleOrDefault(qa => qa.Id == model.FormId);
                    if (form == null) return Error(StatusCodes.Status406NotAcceptable);

                    QualityAuditOwner auditowner = quality.Audits.Single(qa => qa.Id == model.FormId).Owners.SingleOrDefault(qao => qao.UserId == model.OwnerUserId);
                    if (auditowner == null)
                    {
                        auditowner = new QualityAuditOwner { UserId = model.OwnerUserId };
                        form.Owners.Add(auditowner);
                    }

                    UsersToAddIdInt.ToList().ForEach(userid =>
                    {
                        form.Users.Add(new QualityAuditUser { UserId = userid, AuditOwner = auditowner });
                    });
                    break;
                case "exam":
                    form = quality.Exams.SingleOrDefault(qe => qe.Id == model.FormId);
                    if (form == null) return Error(StatusCodes.Status406NotAcceptable);

                    QualityExamOwner examowner = quality.Exams.Single(qe => qe.Id == model.FormId).Owners.SingleOrDefault(qeo => qeo.UserId == model.OwnerUserId);
                    if (examowner == null)
                    {
                        examowner = new QualityExamOwner { UserId = model.OwnerUserId };
                        form.Owners.Add(examowner);
                    }

                    UsersToAddIdInt.ToList().ForEach(userid =>
                    {
                        form.Users.Add(new QualityExamUser { UserId = userid, ExamOwner = examowner });
                        quality.Users.Single(qu => qu.UserId == userid).QualityCompleted = false;
                    });
                    break;
                default: return Error(StatusCodes.Status406NotAcceptable);
            }

            if (QualityService.Update(quality) > 0)
            {
                return Json(data: new JsonResponse() { Result = true, Response = "A�adido/s con �xito", Content = new { Id = model.QualityId } });
            }
            return Error();
        }

            #endregion
            #region DISSOCIATE_USERS

        [HttpGet]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        [Route("/[controller]/{qualityid:int}/{formtype}/dissociatecheck/{formid:int}/{userid:int}")]
        public JsonResult DissociateCheck(int qualityid, string formtype, int formid, int userid)
        {
            string response = "";
            bool ActionAllowed = true;
            dynamic formuser;

            Quality quality = QualityService.Get(qualityid);

            switch (formtype)
            {
                case "audit":
                    formuser = quality.Audits.SingleOrDefault(qa => qa.Id == formid).Users.SingleOrDefault(qau => qau.UserId == userid);
                    if (formuser.Takes.Count > 0)
                    {
                        ActionAllowed = false; response = Localizer["No es posible separar un usuario con auditor�as realizadas."];
                    }
                    break;
                case "exam":
                    formuser = quality.Exams.SingleOrDefault(qe => qe.Id == formid).Users.SingleOrDefault(qeu => qeu.UserId == userid);
                    if (formuser.Takes.Count > 0)
                    {
                        ActionAllowed = false; response = Localizer["No es posible separar un usuario con ex�menes realizados."];
                    }
                    break;
            }

            return Json(data: new JsonResponse() { Result = ActionAllowed, Response = response });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateAjaxModelState]
        [AuthorizeRoles(AppRole.OperationalManagementEdit)]
        [Route("/[controller]/{qualityid:int}/{formtype}/dissociate/{formid:int}/{userid:int}")]
        public IActionResult Dissociate(int qualityid, string formtype, int formid, int userid)
        {
            dynamic formuser;
            User user;

            Quality quality = QualityService.Get(qualityid);
            if (quality == null) return Error(StatusCodes.Status406NotAcceptable);

            switch (formtype)
            {
                case "audit":
                    formuser = quality.Audits.SingleOrDefault(qa => qa.Id == formid).Users.SingleOrDefault(qau => qau.UserId == userid);
                    if (formuser == null) { return Error(StatusCodes.Status406NotAcceptable); }

                    quality.Audits.Single(qa => qa.Id == formid).Users.Remove(formuser);
                    formuser.AuditOwner.Users.Remove(formuser);
                    if (!(formuser.AuditOwner.Users.Count > 0))
                    {
                        quality.Audits.Single(qa => qa.Id == formid).Owners.Remove(formuser.AuditOwner);
                    }

                    user = formuser.User;
                    IEnumerable<QualityAuditUser> useraudits = quality.Audits.SelectMany(qa => qa.Users).Where(qau => qau.User == user);
                    if (useraudits.All(qau => qau.Takes.Any(qaut => qaut.Pass == true)))
                    {
                        quality.Users.Single(qu => qu.User == user).QualityCompleted = true;
                    }
                    break;
                case "exam":
                    formuser = quality.Exams.SingleOrDefault(qe => qe.Id == formid).Users.SingleOrDefault(qeu => qeu.UserId == userid);
                    if (formuser == null) { return Error(StatusCodes.Status406NotAcceptable); }

                    quality.Exams.Single(qe => qe.Id == formid).Users.Remove(formuser);
                    formuser.AuditOwner.Users.Remove(formuser);
                    if (!(formuser.AuditOwner.Users.Count > 0))
                    {
                        quality.Exams.Single(qe => qe.Id == formid).Owners.Remove(formuser.AuditOwner);
                    }

                    user = formuser.User;
                    IEnumerable<QualityExamUser> userexams = quality.Exams.SelectMany(qe => qe.Users).Where(qeu => qeu.User == user);
                    if (userexams.All(qeu => qeu.Takes.Any(qeut => qeut.Pass == true)))
                    {
                        quality.Users.Single(qu => qu.User == user).QualityCompleted = true;
                    }
                    break;
                default: return Error(StatusCodes.Status406NotAcceptable);
            }

            if (QualityService.Update(quality) > 0)
            {
                return Json(data: new JsonResponse() { Result = true, Response = "Eliminado con �xito" });
            }
            return Error();
        }

            #endregion
        #endregion
    }
}