﻿using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using Perseo.Core;
using Microsoft.Extensions.Localization;
using AutoMapper;

namespace Perseo.Web.Controllers
{
    public class HomeController : BaseController
    {
        public HomeController(IHttpContextAccessor contextAccessor, IHostingEnvironment enviroment,
            IUserService userService, IStringLocalizer<SharedResources> localizer,
            IMapper mapper) : base(contextAccessor, enviroment, userService, localizer, mapper)
        { }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Error()
        {
            var exceptionFeature = HttpContext.Features.Get<IExceptionHandlerPathFeature>();

            if (exceptionFeature != null)
            {
                // Get which route the exception occurred at
                string routeWhereExceptionOccurred = exceptionFeature.Path;
                // Get the exception that occurred
                Exception exceptionThatOccurred = exceptionFeature.Error;
                // TODO: Do something with the exception
                // Log it with Serilog?
                // Send an e-mail, text, fax, or carrier pidgeon?  Maybe all of the above?
                // Whatever you do, be careful to catch any exceptions, otherwise you'll end up with a blank page and throwing a 500
            }
            return StatusCode(StatusCodes.Status500InternalServerError);
        }
    }
}
