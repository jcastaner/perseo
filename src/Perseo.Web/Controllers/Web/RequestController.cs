using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Newtonsoft.Json.Linq;
using Perseo.Core;
using Perseo.Web.Helpers;
using Perseo.Web.Models;
using Perseo.Web.Services;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Perseo.Web.Controllers
{
    [Route("[controller]/[action]/{id:int?}")]
    public class RequestController : BaseController
    {
        private readonly IRequestService RequestService;
        private readonly IEmailService EmailService;
        private readonly INotificationService NotificationService;
        private readonly IViewRenderService ViewRenderService;
        private readonly IAreaService AreaService;
        private readonly IService<QualityAuditUserTake> AuditTakeService;

        public RequestController(
            IEmailService emailService,
            IHttpContextAccessor contextAccessor, 
            IHostingEnvironment enviroment,
            IStringLocalizer<SharedResources> localizer,
            IUserService userService,
            IRequestService requestService,
            INotificationService notificationService,
            IViewRenderService viewRenderService,
            IAreaService areaService,
            IService<QualityAuditUserTake> auditTakeService,
            IMapper mapper) : base(contextAccessor, enviroment, userService, localizer, mapper)
        {
            RequestService = requestService;
            EmailService = emailService;
            NotificationService = notificationService;
            ViewRenderService = viewRenderService;
            AreaService = areaService;
            AuditTakeService = auditTakeService;
        }

        [HttpGet]
        [AuthorizeRoles(AppRole.OperationalManagementEdit, AppRole.OperationalManagementShow)]
        [Route("/[controller]")]
        public IActionResult Index()
        {
            return List();
        }

        [HttpGet]
        [Route("/[controller]/[action]/{year:int?}/{month:int?}/{selectedtypes?}/{selectedstates?}")]
        [AuthorizeRoles(AppRole.OperationalManagementEdit, AppRole.OperationalManagementShow)]
        public IActionResult List(int? year = null, int? month = null, string selectedtypes = null, string selectedstates = null)
        {
            year = year ?? DateTime.Now.Year;
            month = month ?? DateTime.Now.Month;

            List<RequestType> Types = selectedtypes != null ? selectedtypes.Split(";").Select(s => (RequestType)int.Parse(s)).ToList() : Types = Enum.GetValues(typeof(RequestType)).Cast<RequestType>().ToList();
            List<RequestState> States = selectedstates != null ? States = selectedstates.Split(";").Select(s => (RequestState)int.Parse(s)).ToList() : new List<RequestState> { RequestState.Created, RequestState.InProgress, RequestState.Pending };

            List<Area> Areas = AuthUser.OwnedAreas.ToList();
            List<int> AreasIds = Areas.Select(a => a.Id).ToList();

            ViewBag.Month = month.Value;
            ViewBag.Year = year.Value;
            ViewBag.SelectedTypes = string.Join(";", Types.Select(t => t.GetHashCode()));
            ViewBag.SelectedStates = string.Join(";", States.Select(s => s.GetHashCode()));

            #region CHARTS

            List<string> labels = new List<string>();
            DateTime BaseDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            for (int i = 11; i >= 0; i--) { labels.Add(CultureInfo.CurrentCulture.TextInfo.ToTitleCase((BaseDate.AddMonths(-i)).ToString("MMMM"))); }

            ChartViewModel Area1 = new ChartViewModel()
            {
                Name = Localizer["Peticiones Atendidas"],
                Series = new List<ChartViewModel.Serie>() { new ChartViewModel.Serie() { Name = Localizer["Peticiones"], Data = Enumerable.Repeat(0.0, labels.Count()).ToList() } },
                Labels = labels
            };
            ChartViewModel Area2 = new ChartViewModel()
            {
                Name = Localizer["Gestionadas" + " < 24h"],
                Series = new List<ChartViewModel.Serie>() { new ChartViewModel.Serie() { Name = Localizer["Peticiones"], Data = Enumerable.Repeat(0.0, labels.Count()).ToList() } },
                Labels = labels
            };
            ChartViewModel Area3 = new ChartViewModel()
            {
                Name = Localizer["Gestionadas" + " < 48h"],
                Series = new List<ChartViewModel.Serie>() { new ChartViewModel.Serie() { Name = Localizer["Peticiones"], Data = Enumerable.Repeat(0.0, labels.Count()).ToList() } },
                Labels = labels
            };
            ChartViewModel Area4 = new ChartViewModel()
            {
                Name = Localizer["Gestionadas" + " < 72h"],
                Series = new List<ChartViewModel.Serie>() { new ChartViewModel.Serie() { Name = Localizer["Peticiones"], Data = Enumerable.Repeat(0.0, labels.Count()).ToList() } },
                Labels = labels
            };

            List<Request> requests = RequestService.GetMany(r => ((r.RequestDate >= BaseDate.AddYears(-1)) && (r.RequestDate < BaseDate.AddMonths(1).AddSeconds(-1)) && (Types.Any(t => t == r.Type)))).ToList();

            List<double> Total = Enumerable.Repeat(0.0, labels.Count()).ToList();
            List<double> Period24 = Enumerable.Repeat(0.0, labels.Count()).ToList();
            List<double> Period48 = Enumerable.Repeat(0.0, labels.Count()).ToList();
            List<double> Period72 = Enumerable.Repeat(0.0, labels.Count()).ToList();

            foreach (Request request in requests)
            {
                int index = (((12 - BaseDate.Month) + (request.RequestDate.Month) - 1) % 12);
                Total[index] += 1;

                DateTime StartDate = request.RequestHistory.Single(rh => rh.State == RequestState.Created).EntryDate;
                if (request.RequestHistory.SingleOrDefault(rh => rh.State == RequestState.Closed) != null)
                {
                    DateTime EndDate =  request.RequestHistory.Single(rh => rh.State == RequestState.Closed).EntryDate;
                    double ProcessingHours = (EndDate - StartDate).TotalHours;
                    if (ProcessingHours < 24) { Period24[index] += 1; }
                    else if (ProcessingHours < 48) { Period48[index] += 1; }
                    else if (ProcessingHours < 72) { Period72[index] += 1; }
                }
            }

            for (int index = 0; index < labels.Count(); index++)
            {
                Area1.Series[0].Data[index] = Total[index];
                Area2.Series[0].Data[index] = Total[index] > 0 ? Math.Round((Period24[index] / Total[index]) * 100, 2) : 0;
                Area3.Series[0].Data[index] = Total[index] > 0 ? Math.Round(((Period24[index] + Period48[index]) / Total[index]) * 100, 2) : 0;
                Area4.Series[0].Data[index] = Total[index] > 0 ? Math.Round(((Period24[index] + Period48[index] + Period72[index]) / Total[index]) * 100, 2) : 0;
            }

            #endregion

            RequestListViewModel model = new RequestListViewModel
            {
                AreaChart1 = Area1,
                AreaChart2 = Area2,
                AreaChart3 = Area3,
                AreaChart4 = Area4,
                Requests = new List<RequestListItemViewModel>(),
                States = States,
                Types = Types,
                Areas = Areas,
                AreasIds = AreasIds
            };

            IEnumerable<RequestListNode> assigned = RequestService.GetRequestsAssignedToUser(AuthUser.Id, Types, States);
            assigned.ToList().ForEach(request =>
            {
                RequestListItemViewModel requestModel = new RequestListItemViewModel
                {
                    Id = request.Id,
                    CreationDate = request.CreationDate,
                    LastChangeDate = request.LastChangeDate,
                    Priority = request.Priority,
                    User = request.UserName,
                    AssignedUser = request.AssignedUserName,
                    Type = request.Type,
                    State = request.State,
                    Summary = request.Summary
                };
                model.Requests.Add(requestModel);
            });

            PagedResult<RequestListNode> pagedResult = RequestService.GetRequestsFromAreasPaged(AreasIds, Types, States, year, month);

            ViewBag.RowCount = pagedResult.RowCount;
            ViewBag.ServerSidePagination = pagedResult.ServerSidePagination;
            ViewBag.DefaultPageSize = pagedResult.DefaultPageSize;

            pagedResult.Results.ToList().ForEach(request =>
            {
                RequestListItemViewModel requestModel = new RequestListItemViewModel
                {
                    Id = request.Id,
                    CreationDate = request.CreationDate,
                    LastChangeDate = request.LastChangeDate,
                    Priority = request.Priority,
                    User = request.UserName,
                    AssignedUser = request.AssignedUserName,
                    Type = request.Type,
                    State = request.State,
                    Summary = request.Summary
                };
                model.Requests.Add(requestModel);
            });

            model.Requests = model.Requests.GroupBy(r => r.Id).Select(r => r.FirstOrDefault()).ToList();

            return View("List", model);
        }

        [HttpGet]
        [Route("/[controller]/[action]/{selectedtypes?}/{selectedstates?}")]
        [AuthorizeRoles(AppRole.OperationalManagementEdit, AppRole.OperationalManagementShow)]
        public JsonResult ListData(DataTablesRequest request, string selectedtypes = null, string selectedstates = null)
        {
            List<RequestType> Types = null;
            List<RequestState> States = null;
            if (selectedtypes != null)
            {
                try
                {
                    Types = selectedtypes.Split(";").Select(s => (RequestType)Int32.Parse(s)).ToList();
                }
                catch (Exception)
                {
                    return null;
                }
            }
            else
            {
                Types = Enum.GetValues(typeof(RequestType)).Cast<RequestType>().ToList();
            }
            if (selectedstates != null)
            {
                try
                {
                    States = selectedstates.Split(";").Select(s => (RequestState)Int32.Parse(s)).ToList();
                }
                catch (Exception)
                {
                    return null;
                }
            }
            else
            {
                States = new List<RequestState> { RequestState.Created, RequestState.InProgress, RequestState.Pending };
            }
            List<Area> Areas = AuthUser.OwnedAreas.ToList();
            List<int> AreasIds = Areas.Select(a => a.Id).ToList();

            var search = request.search["value"];

            List<string> fileds = new List<string> { "cretion_date",  "user_name", "type", "priority", "state" };
            var orderColumn = Convert.ToInt32(request.order[0]["column"]);
            var orderField = orderColumn > 0 ? fileds[orderColumn - 1] : null;
            var orderDirection = request.order[0]["dir"] == "asc" ? "asc" : "desc";

            List<AreaViewModel> model = new List<AreaViewModel>();

            var resultsPaged = RequestService.GetRequestsFromAreasPaged(AreasIds, Types, States, null, null, request.page, request.pageSize, search, orderField, orderDirection);

            resultsPaged.FilteredRowCount = search != null ? resultsPaged.Results.Count() : resultsPaged.RowCount;

            DataTablesResponse response = new DataTablesResponse(request.draw, resultsPaged.RowCount, resultsPaged.FilteredRowCount);

            resultsPaged.Results.ToList().ForEach(item =>
            {
                response.Add(
                    item.Id,
                    item.CreationDate,
                    item.UserName,
                    item.Type.GetDisplayName(),
                    item.Priority.GetDisplayName(),
                    item.State.GetDisplayName()
                    );
            });

            return Json(response);
        }

        [HttpGet]
        public IActionResult Add()
        {
            IEnumerable<Area> areas = AreaService.GetMany(a => a.Active && a.Type != AreaType.General);
            IEnumerable<User> users = areas.SelectMany(a => a.UserArea).Where(ua => ua.Active).Select(ua => ua.User).Where(u => u.Active).Distinct();
            AddRequestViewModel model = new AddRequestViewModel() {
                UserId = AuthUser.Id,
                AreasOptions = areas.Select(a => new AssignOptions { Name = a.Name, Value = a.Id, Parent = a.ParentId }).ToList(),
                AddibleUsers = users.Select(u => new AddibleUser { Id = u.Id, UserName = u.UserName, AreaId = u.LastUserArea.AreaId, FullName = u.FullName }).ToList()
            };
            return PartialView("_Add", model);
        }

        [HttpPost]
        [Route("/[controller]/[action]")]
        [ValidateAntiForgeryToken]
        [ValidateAjaxModelState]
        public IActionResult Add(AddRequestViewModel model)
        {
            Request request = new Request
            {
                Type = model.Type,
                RequestDate = DateTime.Now,
                Priority = model.Priority,
                Summary = model.Summary,
                Description = model.Description,
                AdditionalData = model.AdditionalData,
                User = AuthUser                
            };
            request.RequestHistory.Add(new RequestHistory {
                EntryDate = request.RequestDate,
                State = RequestState.Created,
                Comment = Localizer["Solicitud Creada"],
                User = AuthUser
            });
            request.RequestHistory.Add(new RequestHistory
            {
                EntryDate = DateTime.Now,
                State = RequestState.Pending,
                Comment = Localizer["Pendiente de Asignaci�n"],
                User = AuthUser
            });

            if (request.Type == RequestType.PositionChange && request.AdditionalData != null)
            {
                JObject AdditionalDataObject = JObject.Parse(request.AdditionalData);
                Area OriginArea = AreaService.Get((int)AdditionalDataObject["OriginArea"]);
                Area DestinationArea = AreaService.Get((int)AdditionalDataObject["DestinationArea"]);
                List<int> UserIds = new List<int>();
                foreach (JToken userIdString in AdditionalDataObject["UserIds"].ToArray())
                {
                    int userId = int.Parse((string)userIdString);
                    UserIds.Add(userId);
                }

                User AssignationUser = new User();

                if (AuthUser.OwnedAreas.Select(a => a.Id).Contains(OriginArea.Id))
                {
                    AssignationUser = DestinationArea.Owner;
                }
                else if (AuthUser.OwnedAreas.Select(a => a.Id).Contains(DestinationArea.Id))
                {
                    AssignationUser = OriginArea.Owner;
                }
                else if (!(UserIds.Contains(AuthUser.Id)))
                {
                    return Error(StatusCodes.Status401Unauthorized);
                }

                if (AssignationUser.Id > 0)
                {
                    request.AssignedUser = AssignationUser;
                    request.RequestHistory.Add(new RequestHistory
                    {
                        EntryDate = DateTime.Now,
                        State = RequestState.InProgress,
                        Comment = Localizer["Asignaci�n autom�tica a"] + " " + AssignationUser.FullName,
                        User = AuthUser
                    });
                }
            }

            RequestService.Add(request);
            if (request.Id > 0)
            {
                User supervisor = request.User.Supervisor;
                if (supervisor != null)
                {
                    Notification notification = new Notification()
                    {
                        User = supervisor,
                        Summary = "Nueva solicitud de " + request.Type.GetDisplayName() + ": " + request.Id,
                        Description = request.User.FullName + " ha creado una solicitud de " + request.Type.GetDisplayName() + " con identificador " + request.Id + " el " + request.RequestDate,
                        StartDate = DateTime.Now,
                        Active = true,
                        Priority = false,
                        Url = "/request/show/" + request.Id,
                        Type = NotificationType.alert
                    };
                    NotificationService.Add(notification);
                    if (notification.Id > 0)
                    {
                        supervisor.Alerts++;
                        UserService.Update(supervisor);
                    }
                }

                if(request.AssignedUser != null)
                {
                    Notification notification = new Notification()
                    {
                        User = request.AssignedUser,
                        Summary = "Asignada nueva solicitud de " + request.Type.GetDisplayName() + ": " + request.Id,
                        Description = "Se le ha asignado de manera autom�tica una solicitud de " + request.Type.GetDisplayName() + " con identificador " + request.Id + " creada por " + request.User.FullName + " el " + request.RequestDate,
                        StartDate = DateTime.Now,
                        Active = true,
                        Priority = false,
                        Url = "/request/show/" + request.Id,
                        Type = NotificationType.alert
                    };
                    NotificationService.Add(notification);
                    if (notification.Id > 0)
                    {
                        request.AssignedUser.Alerts++;
                        UserService.Update(request.AssignedUser);
                    }
                }

                EmailService.SendAsync(AuthUser.Email, "Solicitud S00" + request.Id + " creada", "Se ha creado la solicitud S00" + request.Id);
                JsonResponse response = new JsonResponse() { Result = true, Response = "La petici�n se ha creado con �xito", Content = new { Id = request.Id } };
                return Json(response);
            }
            return Error();
        }

        [HttpGet]
        public IActionResult Show(int id)
        {
            Request request = RequestService.Get(id);

            if (!(request.UserId == AuthUser.Id
                || request.AssignedUserId == AuthUser.Id
                || AuthUser.OwnedAreas.Any(a => a.Id == request.User.LastUserArea.AreaId)
                || AuthUser.OwnedAreas.Any(a => a.Id == request.AssignedUser.LastUserArea.AreaId)))
            {
                return Error(StatusCodes.Status401Unauthorized);
            }

            List<RequestState> AllowedStates = new List<RequestState>();

            if (request.State == RequestState.Created || request.State == RequestState.Pending)
            {
                AllowedStates = new List<RequestState> { RequestState.Closed };
            }
            else if (request.State == RequestState.InProgress)
            {
                AllowedStates = new List<RequestState> { RequestState.Accepted, RequestState.Rejected, RequestState.Returned };
            }
            else if (request.State == RequestState.Returned || request.State == RequestState.Rejected || request.State == RequestState.Accepted)
            {
                AllowedStates = new List<RequestState> { RequestState.Pending, RequestState.Closed };
            }

            var requestHistory = RequestService.GetRequestHistory(id).ToList();
            RequestViewModel model = new RequestViewModel
            {
                Id = request.Id,
                AllowedStates = AllowedStates.ToList(),
                Type = request.Type,
                State = request.State,
                RequestDate = request.RequestDate,
                LastAssignedDate = requestHistory.Max(x => x.EntryDate),
                LastComment = requestHistory.OrderByDescending(t => t.EntryDate).FirstOrDefault().Comment,
                Priority = request.Priority,
                Summary = request.Summary,
                Description = request.Description,
                UserName = request.User.FirstName + " " + request.User.MainSurname,
                AssignedUserName = request.AssignedUser != null ? request.AssignedUser.FirstName + " " + request.AssignedUser.MainSurname : "",
                AdditionalData = request.AdditionalData,
                RequestHistory = requestHistory,
                IsCreator = request.UserId == AuthUser.Id ? true : false,
                IsAssigned = request.AssignedUser == null ? false : request.AssignedUser.Id == AuthUser.Id ? true : false,
            };
            return View("Show", model);
        }

        [Route("~/[controller]/[action]/{requestid:int}")]
        [HttpGet]
        [AuthorizeRoles(AppRole.OperationalManagementEdit, AppRole.OperationalManagementShow)]
        public IActionResult ShowSchedule(int requestid)
        {
            Request request = RequestService.Get(requestid);
            if (request == null) return Error(StatusCodes.Status406NotAcceptable);
            if (request.AdditionalData == null) return Error(StatusCodes.Status406NotAcceptable);

            List<DateTime> MainDays = new List<DateTime>();
            List<DateTime> SecondDays = new List<DateTime>();

            JObject AdditionalDataObject = JObject.Parse(request.AdditionalData);

            foreach (JToken dateString in AdditionalDataObject["DaysMain"].ToArray())
            {
                DateTime date = DateTime.Parse((string)dateString);
                MainDays.Add(date);
            }
            foreach (JToken dateString in AdditionalDataObject["DaysSecond"].ToArray())
            {
                DateTime date = DateTime.Parse((string)dateString);
                SecondDays.Add(date);
            }

            DateTime minDay = MainDays.Concat(SecondDays).Min();
            DateTime maxDay = MainDays.Concat(SecondDays).Max();

            RequestShowScheduleViewModel model = new RequestShowScheduleViewModel
            {
                Id = request.Id,
                State = request.State,
                UserId = request.UserId,
                IsAreaOwner = (AuthUser.OwnedAreas.Select(a => a.Id).Contains(request.User.LastUserArea.AreaId)),
                AdditionalData = request.AdditionalData,
                MonthModels = new List<ScheduleViewModel>()
            };

            List<int> areasIds = request.User.UserArea.Where(ua => ua.Active).Select(ua => ua.Area).Select(a => a.Id).Distinct().ToList();

            for(DateTime indexDate = minDay.AddDays(1 - minDay.Day); indexDate <= maxDay ; indexDate = indexDate.AddMonths(1))
            {
                int year = indexDate.Year;
                int month = indexDate.Month;

                ScheduleViewModel monthModel = new ScheduleViewModel
                {
                    Year = year,
                    Month = month,
                    Rows = new List<ScheduleRow>()
                };

                UserService.GetAllUserSchedule(monthModel.Year, monthModel.Month, areasIds).ToList().ForEach(user =>
                {
                    Dictionary<string, ScheduleDate> scheduleDict = new Dictionary<string, ScheduleDate>();
                    foreach (ScheduleDate scheduleDay in user.ScheduleDate)
                    {
                        if (scheduleDay != null) scheduleDict.Add("D" + scheduleDay.Day, scheduleDay);
                    }
                    ScheduleRow scheduleRow = new ScheduleRow
                    {
                        UserId = user.Id,
                        FullName = user.FullName,
                        Columns = scheduleDict
                    };
                    monthModel.Rows.Add(scheduleRow);
                });

                model.MonthModels.Add(monthModel);
            }

            return PartialView("_ShowSchedule", model);
        }

        [Route("~/[controller]/[action]/{requestid:int}")]
        [HttpGet]
        [AuthorizeRoles(AppRole.OperationalManagementEdit, AppRole.OperationalManagementShow)]
        public IActionResult ShowUsers(int requestid)
        {
            Request request = RequestService.Get(requestid);
            if (request == null) return Error(StatusCodes.Status406NotAcceptable);
            if (request.AdditionalData == null) return Error(StatusCodes.Status406NotAcceptable);

            JObject AdditionalDataObject = JObject.Parse(request.AdditionalData);
            Area OriginArea = AreaService.Get((int)AdditionalDataObject["OriginArea"]);
            Area DestinationArea = AreaService.Get((int)AdditionalDataObject["DestinationArea"]);
            List<int> UserIds = new List<int>();
            foreach (JToken userIdString in AdditionalDataObject["UserIds"].ToArray())
            {
                int userId = int.Parse((string)userIdString);
                UserIds.Add(userId);
            }
            List<User> usersToReassign = UserService.GetMany(u => UserIds.Contains(u.Id)).ToList();

            RequestShowUsersViewModel model = new RequestShowUsersViewModel
            {
                Id = request.Id,
                State = request.State,
                UserId = request.UserId,
                IsOriginAreaOwner = (AuthUser.OwnedAreas.Select(a => a.Id).Contains(OriginArea.Id)),
                AlreadyReassigned = usersToReassign.All(u => u.LastUserArea.AreaId == DestinationArea.Id),
                AdditionalData = request.AdditionalData,
                OriginArea = Mapper.Map<AreaModel>(OriginArea),
                DestinationArea = Mapper.Map<AreaModel>(DestinationArea),
                UsersToReassign = Mapper.Map<List<UserModel>>(usersToReassign)
            };
            
            return PartialView("_ShowUsers", model);
        }

        [HttpGet]
        [Route("/[controller]/[action]/{id:int}/{state}")]
        public IActionResult Edit(int id, RequestState state)
        {
            Request request = RequestService.Get(id);

            if (!(request.UserId == AuthUser.Id
                || request.AssignedUserId == AuthUser.Id
                || AuthUser.OwnedAreas.Any(a => a.Id == request.User.LastUserArea.AreaId)
                || AuthUser.OwnedAreas.Any(a => a.Id == request.AssignedUser.LastUserArea.AreaId)))
            {
                return Error(StatusCodes.Status401Unauthorized);
            }

            bool IsCreator = request.UserId == AuthUser.Id ? true : false;
            bool IsAssigned = request.AssignedUser == null ? false : request.AssignedUser.Id == AuthUser.Id ? true : false;
            bool IsOriginAreaOwner = false;

            if (request.Type == RequestType.PositionChange && request.AdditionalData == null)
            {
                JObject AdditionalDataObject = JObject.Parse(request.AdditionalData);
                Area OriginArea = AreaService.Get((int)AdditionalDataObject["OriginArea"]);
                IsOriginAreaOwner = (AuthUser.OwnedAreas.Select(a => a.Id).Contains(OriginArea.Id));
            }

            RequestViewModel model = new RequestViewModel
            {
                Id = request.Id,
                AdditionalData = request.AdditionalData,
                State = request.State,
                NewState = state,
                IsCreator = IsCreator,
                IsAssigned = IsAssigned,
                IsOriginAreaOwner = IsOriginAreaOwner
            };

            return PartialView("_Edit", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateAjaxModelState]
        public IActionResult Edit(RequestViewModel model)
        {
            Request request = RequestService.Get(model.Id);

            if (!(request.UserId == AuthUser.Id
                || request.AssignedUserId == AuthUser.Id
                || AuthUser.OwnedAreas.Any(a => a.Id == request.User.LastUserArea.AreaId)
                || AuthUser.OwnedAreas.Any(a => a.Id == request.AssignedUser.LastUserArea.AreaId)))
            {
                return Error(StatusCodes.Status401Unauthorized);
            }

            request.RequestHistory.Add(new RequestHistory
            {
                EntryDate = DateTime.Now,
                State = model.NewState,
                Comment = model.NewComment,
                User = AuthUser
            });

            bool IsCreator = request.UserId == AuthUser.Id ? true : false;
            bool IsAssigned = request.AssignedUser == null ? false : request.AssignedUser.Id == AuthUser.Id ? true : false;
            
            if (model.NewState == RequestState.Pending)
            {
                User AssignationUser = new User();

                if (request.Type == RequestType.PositionChange && request.AdditionalData != null)
                {
                    JObject AdditionalDataObject = JObject.Parse(request.AdditionalData);
                    Area OriginArea = AreaService.Get((int)AdditionalDataObject["OriginArea"]);
                    if (OriginArea == null) return Error(StatusCodes.Status406NotAcceptable);
                    Area DestinationArea = AreaService.Get((int)AdditionalDataObject["DestinationArea"]);
                    if (DestinationArea == null) return Error(StatusCodes.Status406NotAcceptable);
                    List<int> UserIds = new List<int>();
                    foreach (JToken userIdString in AdditionalDataObject["UserIds"].ToArray())
                    {
                        int userId = int.Parse((string)userIdString);
                        UserIds.Add(userId);
                    }
                    if (UserIds.Count == 0) return Error(StatusCodes.Status406NotAcceptable);

                    if (AuthUser.OwnedAreas.Select(a => a.Id).Contains(OriginArea.Id))
                    {
                        AssignationUser = DestinationArea.Owner;
                    }
                    else if (AuthUser.OwnedAreas.Select(a => a.Id).Contains(DestinationArea.Id))
                    {
                        AssignationUser = OriginArea.Owner;
                    }
                    else if (!(UserIds.Contains(AuthUser.Id)))
                    {
                        return Error(StatusCodes.Status401Unauthorized);
                    }
                }

                if (request.Type == RequestType.Other && request.AdditionalData != null)
                {
                    JObject AdditionalDataObject = JObject.Parse(request.AdditionalData);
                    QualityAuditUserTake Take = AuditTakeService.Get((int)AdditionalDataObject["TakeId"]);
                    if (Take == null) return Error(StatusCodes.Status406NotAcceptable);

                    AssignationUser = Take.Auditor;
                }

                if (AssignationUser.Id > 0)
                {
                    request.AssignedUser = AssignationUser;
                    request.RequestHistory.Add(new RequestHistory
                    {
                        EntryDate = DateTime.Now,
                        State = RequestState.InProgress,
                        Comment = Localizer["Asignaci�n autom�tica a"] + " " + AssignationUser.FullName,
                        User = AuthUser
                    });
                }
                else
                {
                    request.AssignedUser = null;
                    request.AssignedUserId = null;
                }

            }
            else if (model.NewState == RequestState.InProgress)
            {
                request.AssignedUser = AuthUser;
                request.AssignedUserId = AuthUser.Id;
            }
            else if (model.NewState == RequestState.Returned)
            {
                request.AssignedUser = request.User;
                request.AssignedUserId = request.User.Id;
            }
            else if (model.NewState == RequestState.Accepted || model.NewState == RequestState.Rejected)
            {
                request.AssignedUser = request.User;
                request.AssignedUserId = request.User.Id;
            }

            if (RequestService.Update(request) > 0)
            {
                if(model.NewState == RequestState.Accepted || model.NewState == RequestState.Rejected || model.NewState == RequestState.Returned)
                {
                    Notification notification = new Notification()
                    {
                        User = request.User,
                        UserId = request.User.Id,
                        Summary = "Cambios en su solicitud " + request.Id,
                        Description = "La solicitud de " + request.Type.GetDisplayName() + " con identificador " + request.Id + " creada el " + request.RequestDate + " ha sido " + model.NewState.GetDisplayName() + " por " + AuthUser.FullName + " con el sigiente comentario: " + model.NewComment,
                        StartDate = DateTime.Now,
                        Active = true,
                        Priority = false,
                        Url = "/request/show/"+ request.Id,
                        Type = NotificationType.task
                    };
                    NotificationService.Add(notification);
                    if (notification.Id > 0)
                    {
                        request.User.Tasks++;
                        UserService.Update(request.User);
                    }
                }

                if (model.NewState == RequestState.Pending && request.AssignedUser != null)
                {
                    Notification notification = new Notification()
                    {
                        User = request.AssignedUser,
                        Summary = "Asignada nueva solicitud de " + request.Type.GetDisplayName() + ": " + request.Id,
                        Description = "Se le ha asignado de manera autom�tica una solicitud de " + request.Type.GetDisplayName() + " con identificador " + request.Id + " creada por " + request.User.FullName + " el " + request.RequestDate,
                        StartDate = DateTime.Now,
                        Active = true,
                        Priority = false,
                        Url = "/request/show/" + request.Id,
                        Type = NotificationType.alert
                    };
                    NotificationService.Add(notification);
                    if (notification.Id > 0)
                    {
                        request.AssignedUser.Alerts++;
                        UserService.Update(request.AssignedUser);
                    }
                }

                JsonResponse response = new JsonResponse() { Result = true, Response = "La solicitud se ha gestionado con �xito", Content = new { Id = request.Id } };
                return Json(response);
            }
            return Error();
        }

    }
}
