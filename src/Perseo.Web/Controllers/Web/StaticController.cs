﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.IO;

namespace Perseo.Web.Controllers
{
    [Route("[controller]/[action]/{filename}")]
    //[Authorize(Policy = AppPolicy.Staff)]
    public class StaticController : Controller
    {
        private readonly IHostingEnvironment env;
        private readonly string WebRootPath;
        private readonly string ContentRootPath;
        private readonly string StaticPath;

        public StaticController(IHostingEnvironment env)
        {
            this.env = env ?? throw new ArgumentNullException(nameof(env));
            WebRootPath = env.WebRootPath;
            ContentRootPath = env.ContentRootPath;
            StaticPath = ContentRootPath + "/App/Static/";
        }

        [HttpGet]
        public IActionResult Avatars(string filename)
        {
            var path = Path.Combine(StaticPath, "Avatars", filename);
            if (System.IO.File.Exists(path))
            {
                return PhysicalFile(@path, "image/jpg");
            }
            else
            {
                //path = Path.Combine(StaticPath, "Avatars", "0.jpg");
                //return PhysicalFile(@path, "image/jpg");
                return StatusCode(StatusCodes.Status404NotFound);
            }
        }

        [HttpGet]
        public IActionResult Templates(string filename)
        {
            var path = Path.Combine(StaticPath, "Templates", filename);
            if (System.IO.File.Exists(path))
            {
                return PhysicalFile(@path, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            }
            else
            {
                return StatusCode(StatusCodes.Status404NotFound);
            }
        }

    }
}