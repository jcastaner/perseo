using Perseo.Core;
using Microsoft.AspNetCore.Mvc;
using System.Net.Mime;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;

namespace Perseo.Web.ApiControllers
{
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]  
    // Add header to http request  "Authorization : Bearer whatever-token"  
    // Or add token to url parameter "...?token=whatever-token"
    // To get token post "username" and "password" as Json to url "/api/1/request-token" 
    public abstract class BaseApiController : ControllerBase
    {

        protected readonly IUserService UserService;
        protected readonly IMapper Mapper;

        #region CONSTRUCTOR

        public BaseApiController(IUserService userService, IMapper mapper) {
            UserService = userService;
            Mapper = mapper;
        }

        #endregion
       

    }

    
}
