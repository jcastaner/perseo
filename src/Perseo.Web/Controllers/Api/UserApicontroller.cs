using Perseo.Core;
using Microsoft.AspNetCore.Mvc;
using Perseo.Web.Models;
using AutoMapper;
using System.Collections.Generic;
using System.Linq;

namespace Perseo.Web.ApiControllers
{

    public class UserController : BaseApiController
    {
        public UserController(IUserService userService, IMapper mapper) : base (userService, mapper) { }


        [HttpGet]
        [Route("api/{ver:int}/[controller]s")]
        public ActionResult Get()
        {
            List<User> users = UserService.GetMany().ToList();
            var model = Mapper.Map<List<UserViewModel>>(users);
            return new JsonResult(model);
        }

        [HttpGet]
        [Route("api/{ver:int}/[controller]s/{id:int}")]
        public ActionResult Get(int id)
        {
            var user = UserService.Get(id);
            if (user == null) return NotFound();
            var model = Mapper.Map<UserViewModel>(user);
            return new JsonResult(model);
        }


        [HttpPost]
        [Route("api/{ver:int}/[controller]s")]
        public ActionResult Add(UserViewModel model)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            var user = Mapper.Map<User>(model);
            UserService.Add(user);
            if (user.Id > 0) return CreatedAtAction(nameof(Get), new { id = user.Id }, user);
            else return Conflict();
        }


        [HttpPut]
        [Route("api/{ver:int}/[controller]s/{id:int}")]
        public ActionResult Edit(UserViewModel model)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            var user = UserService.Get(model.Id);
            if (user == null) return NotFound();
            Mapper.Map(model, user);
            UserService.Update(user);
            if (UserService.Update(user) > 0) return Ok();
            return Conflict();
        }


        //[HttpDelete]
        //[Route("api/{version:int}/[controller]s/{id:int}")]
        //public ActionResult Delete(int id)
        //{
        //    var user = UserService.Get(id);
        //    if (user.Id > 0)
        //    {
        //        if (UserService.Delete(user) > 0) return Ok();
        //        return Conflict();
        //    }
        //    return NotFound();
        //}



    }
}
