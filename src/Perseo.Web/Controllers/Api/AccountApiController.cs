using Perseo.Core;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Perseo.Web.App.Identity;
using Microsoft.AspNetCore.Identity;
using Perseo.Web.Identity;
using System.Threading.Tasks;
using System.Security.Claims;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.Extensions.Options;
using System;

namespace Perseo.Web.ApiControllers
{
     public class AccountController : BaseApiController
    {
        private TokenManagement _tokenManagement { get; }
        private readonly UserManager<ApplicationUser> UserManager;
        private readonly SignInManager<ApplicationUser> SignInManager;

        public AccountController(IUserService userService, 
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            IOptions<TokenManagement> TokenManagement,
            IMapper mapper) : base (userService, mapper)
        {
            UserManager = userManager;
            SignInManager = signInManager;
            _tokenManagement = TokenManagement.Value;
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("api/{ver:int}/request-token")]
        public async Task<IActionResult> RequestToken([FromBody] TokenRequest request)
        {
            string token = string.Empty;
            var user = await UserManager.FindByNameAsync(request.UserName);
            if (user == null)
            {
                return null;
            }
            var roles = await UserManager.GetRolesAsync(user);
            var result = await SignInManager.CheckPasswordSignInAsync(user, request.Password, lockoutOnFailure: true);

            if (result.Succeeded)
            {
                List<Claim> claims = new List<Claim>
                {
                    new Claim(ClaimTypes.Name, request.UserName),
                };
                foreach (var role in roles)
                {
                    claims.Add(new Claim(ClaimTypes.Role, role));
                }

                var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_tokenManagement.Secret));
                var credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

                var jwtToken = new JwtSecurityToken(
                    _tokenManagement.Issuer,
                    _tokenManagement.Audience,
                    claims,
                    expires: DateTime.Now.AddMinutes(_tokenManagement.AccessExpiration),
                    signingCredentials: credentials
            );
            token = new JwtSecurityTokenHandler().WriteToken(jwtToken);
            return Ok(token);

            }
            if (result.IsLockedOut)
            {
                return BadRequest("User is Locked");
            }
            return BadRequest("Invalid Request");
        }

    }
}
